# Hoteles D-apos
## Tecnologias Instaladas 
- Instalar Xampp vr de 2.3 en adelante
- PHP vr 7.0 en adelante 
- GIT 
- SourceTreeSetup se recomienda para administrar de una mejor forma el proyecto, sin embargo no es necesaria. 
### Creacion de base de datos
- Creamos una base de datos dentro de nuestro MySQL
- Damos en importar base de datos
- El Script que debemos importar a la base de datos se encuentra en la carpeta DB 
- Actualizamos el archivo Database.php encontrado en (Hoteleria/core/controller/Database.php)
- Cambiamos los datos requeridos en caso de que le hayamos puesto contraseña al acceso de la base de datos.
#### Correr Proyecto
Abrimos cualquier navegador e ingresamos a nuestra carpeta de proyecto xampp ejemplo  (http://localhost/nombre_de_la_carpeta)

#####
- Usuario :admin
- Contraseña :123

![alt text](https://gitlab.com/GaboQA/functionalities_for_d_apos/-/raw/master/img/Readme.jpeg)
