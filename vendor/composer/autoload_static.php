<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite781e22c6755ca899cc79352238f85d8
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Twilio\\' => 7,
        ),
        'I' => 
        array (
            'Ifsnop\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Twilio\\' => 
        array (
            0 => __DIR__ . '/..' . '/twilio/sdk/src/Twilio',
        ),
        'Ifsnop\\' => 
        array (
            0 => __DIR__ . '/..' . '/ifsnop/mysqldump-php/src/Ifsnop',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite781e22c6755ca899cc79352238f85d8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite781e22c6755ca899cc79352238f85d8::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite781e22c6755ca899cc79352238f85d8::$classMap;

        }, null, ClassLoader::class);
    }
}
