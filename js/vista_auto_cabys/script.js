$(window).load(function () {

    $('.footable').footable();

});




function actualizar(id) {



    var descripcion = document.getElementById("descripcion").value;

    var parametros = {
        "id": id,
        "descripcion": descripcion,
        "et": 1
    };
    $.ajax({
        type: "POST",
        url: 'index.php?action=action_auto_cabys',
        data: parametros,
        success: function (data) {
            window.location.reload(); // Recargar página   
        }
    });
    
}


function actualizar_estado(id) {

    var parametros = {
        "id": id,
        "et": 2
    };
    $.ajax({
        type: "POST",
        url: 'index.php?action=action_auto_cabys',
        data: parametros,
        success: function (data) {  
            console.log("exitoso");
        }
    });
}

function del(id) {
    Swal.fire({
        title: 'Estas seguro?',
        text: "",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar'
    }).then((result) => {
        if (result.isConfirmed) {
            var parametros = {
                "id": id,
                "et": 3
            };
            $('#tipo_cliente').html("Por favor espera un momento");
            $.ajax({
                type: "POST",
                url: 'index.php?action=action_auto_cabys',
                data: parametros,
                success: function (data) {
                    window.location.reload(); // Recargar página
                }
            });
        }

    })
}

//Clientes
function load_c() {
    var parametros = {
        "page": 1
    };
    $("#loader_c").fadeIn('slow');
    $.ajax({
        url: './?action=actions_buscar_cabys',
        data: parametros,
        beforeSend: function (objeto) {

        },
        success: function (data) {
            $(".outer_div_c").html(data).fadeIn('slow');
            $('#loader_c').html('');

        }
    })
}
