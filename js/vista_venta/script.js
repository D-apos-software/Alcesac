
  //Productos
  function load(page) {
    var q = $("#q").val();
    var parametros = {
      "page": page,
      "q": q
    };
    $("#loader").fadeIn('slow');
    $.ajax({
      url: './?action=buscar_producto',
      data: parametros,
      beforeSend: function(objeto) {
        $('#loader').html('<img src="core/pedidos/img/ajax-loader.gif"> Cargando...');
      },
      success: function(data) {
        $(".outer_div").html(data).fadeIn('slow');
        $('#loader').html('');

      }
    })
  }
  //Clientes
  function load_c(page) {
    var q = $("#q").val();
    var parametros = {
      "page": 1
    };
    $("#loader_c").fadeIn('slow');
    $.ajax({
      url: './?action=actions_buscar_clientesT',
      data: parametros,
      beforeSend: function(objeto) {
        $('#loader_c').html('<img src="core/pedidos/img/ajax-loader.gif"> Cargando...');
      },
      success: function(data) {
        $(".outer_div_c").html(data).fadeIn('slow');
        $('#loader_c').html('');

      }
    })
  }

  function CargarTipoCliente(val) {

    $('#tipo_cliente').html("Por favor espera un momento");
    $.ajax({
      type: "POST",
      url: 'index.php?action=mostrar_cliente',
      data: 'id_mostrar_cliente=' + val,
      success: function(resp) {
        $('#tipo_cliente').html(resp);
      }
    });
  };




  function agregar(id) {
    var precio_venta = $('#precio_venta_' + id).val();
    var precio_venta_exo = $('#precio_venta_exo_' + id).val();
    var cantidad = $('#cantidad_' + id).val();
    var descripcion = $('#descripcion_' + id).val();

    //Inicia validacion
    if (isNaN(cantidad)) {
      alert('Esto no es un numero');
      document.getElementById('cantidad_' + id).focus();
      return false;
    }
    if (isNaN(precio_venta)) {
      alert('Esto no es un numero');
      document.getElementById('precio_venta_' + id).focus();
      return false;
    }

    //Fin validacion
    var parametros = {
      "id": id,
      "precio_venta": precio_venta,
      "precio_venta_exo": precio_venta_exo,
      "cantidad": cantidad
    };
    $.ajax({
      type: "POST",
      url: "./?action=agregar_tmp",
      data: parametros,
      beforeSend: function(objeto) {
        $("#resultados").html("Mensaje: Cargando...");
      },
      success: function(datos) {

        $("#resultados").html(datos);

      }
    });
  }



  function eliminar(id) {

    $.ajax({
      type: "GET",
      url: "./?action=agregar_tmp",
      data: "id=" + id,
      beforeSend: function(objeto) {
        $("#resultados").html("Mensaje: Cargando...");
      },
      success: function(datos) {
        $("#resultados").html(datos);
      }
    });

  }
  



  function CargarNV(val){
    if(val == 0){
      $(function() {
        $("#myModal_NV").modal();
      });
    }


  }



  function valor_moneda(){

    $.ajax({
      "url": "https://api.hacienda.go.cr/indicadores/tc/dolar",
      "method": "GET"
    }).done(function (response) {
      document.getElementById("cambio_mon").value = response.venta.valor;
    });
  }