
function Validacion_cliente_existente(id_usuario) {
    var parametros = {
        "id": id_usuario,
        "fs": '0'
    };
    $.ajax({
        type: "POST",
        url: './?action=action_busqueda_cliente',
        data: parametros,
        success: function (data) {
            //alert(data);

            if (data != null && data != "null") {
                //alert('ID ingresado existe en base de datos.');


                //Si existe hay que validar si se encuentra en una habitacion o si no.
                sim = JSON.parse(data)
                //alert(sim.tipo_documento);
                document.getElementById('tipo_id_cliente').value = 40;
                val = 40;
                $.ajax({
                    type: "POST",
                    url: 'index.php?action=mostrar_cliente',
                    data: 'id_mostrar_cliente=' + val,
                    success: function (resp) {
                        $('#tipo_cliente').html(resp);


                    }
                });
                setTimeout(function () {
                    props = {
                        "TipoDocumentoData": sim.tipo_documento,
                        "fs": '3'
                    }
                    $.ajax({
                        type: "POST",
                        url: 'index.php?action=action_busqueda_cliente',
                        data: props,
                        success: function (resp) {
                            document.getElementById('tipo_documento').value = resp;
                            document.getElementById('documento').value = sim.documento;
                            document.getElementById('nombre_cliente').value = sim.nombre;
                            document.getElementById('email').value = sim.email;
                            document.getElementById('id_tipo_comprobante').value = 3;
                            document.getElementById('periodo').value = 2;
                            document.getElementById('pagado').value = 40;
                            document.getElementById('m_pago').value = '01';
                            
                        }
                    });

                 

                }, 1000);

            } else if (data == null || data == "null") {
                //alert('ID ingresado no existe en base de datos.');
                //Realiazamos consulta directa a EL TRIBUNAL
                val = 40;
                $.ajax({
                    type: "POST",
                    url: 'index.php?action=mostrar_cliente',
                    data: 'id_mostrar_cliente=' + val,
                    success: function (resp) {
                        $('#tipo_cliente').html(resp);
                    }
                });
                document.getElementById('tipo_id_cliente').value = 40;
                setTimeout(function () {
                    $.ajax({
                        type: "GET",
                        url: 'https://api.hacienda.go.cr/fe/ae?identificacion=' + id_usuario,
                        success: function (data) {
                            //window.location.reload(); 
                            console.log(data);
                            document.getElementById('tipo_documento').value = parseInt(data.tipoIdentificacion); 
                            document.getElementById('nombre_cliente').value = data.nombre;
                            document.getElementById('pagado').value = 40;
                            document.getElementById('periodo').value = 2;
                            document.getElementById('id_tipo_comprobante').value = 3;
                            document.getElementById('documento').value = id_usuario; 
                            document.getElementById('m_pago').value = '01';
                        }
                    });
                }, 1000);

            }

        }
    });
}


$(document).ready(function () {
    $('#cd').click(function () {
        id = document.getElementById('cd1').value;
        Validacion_cliente_existente(id);
    });
});