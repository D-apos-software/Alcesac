 //ADD
 function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
      $('#imagePreview').hide();
      $('#imagePreview').fadeIn(650);
    }
    reader.readAsDataURL(input.files[0]);
  }
}
$("#imagen").change(function() {
  readURL(this);
});
//MODIFY
function readURL_m(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      /*$('#imagePreview_m').remove();
      var id = document.getElementById("id_producto").value;
      document.getElementById('avatar-preview_m').innerHTML = '<div id="imagePreview_m" style="background-image: url('+ e.target.result + ');">';*/
      $('#imagePreview_m').css('background-image', 'url(' + e.target.result + ')');
      $('#imagePreview_m').hide();
      $('#imagePreview_m').fadeIn(650);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#imagen_m").change(function() {
  readURL_m(this);
});



  $(".checkbox-dropdown").click(function() {
    $(this).toggleClass("is-active");
  });

  $(".checkbox-dropdown ul").click(function(e) {
    e.stopPropagation();
  });





  (function($) {
    var CheckboxDropdown = function(el) {
      var _this = this;
      this.isOpen = false;
      this.areAllChecked = false;
      this.$el = $(el);
      this.$label = this.$el.find('.dropdown-label');
      this.$checkAll = this.$el.find('[data-toggle="check-all"]').first();
      this.$inputs = this.$el.find('[type="checkbox"]');

      this.onCheckBox();

      this.$label.on('click', function(e) {
        e.preventDefault();
        _this.toggleOpen();
      });

      this.$checkAll.on('click', function(e) {
        e.preventDefault();
        _this.onCheckAll();
      });

      this.$inputs.on('change', function(e) {
        _this.onCheckBox();
      });
    };

    CheckboxDropdown.prototype.onCheckBox = function() {
      this.updateStatus();
    };

    CheckboxDropdown.prototype.updateStatus = function() {
      var checked = this.$el.find(':checked');

      this.areAllChecked = false;
      this.$checkAll.html('Check All');

      if (checked.length <= 0) {
        this.$label.html('Select Options');
      } else if (checked.length === 1) {
        this.$label.html(checked.parent('label').text());
      } else if (checked.length === this.$inputs.length) {
        this.$label.html('All Selected');
        this.areAllChecked = true;
        this.$checkAll.html('Uncheck All');
      } else {
        this.$label.html(checked.length + ' Selected');
      }
    };

    CheckboxDropdown.prototype.onCheckAll = function(checkAll) {
      if (!this.areAllChecked || checkAll) {
        this.areAllChecked = true;
        this.$checkAll.html('Uncheck All');
        this.$inputs.prop('checked', true);
      } else {
        this.areAllChecked = false;
        this.$checkAll.html('Check All');
        this.$inputs.prop('checked', false);
      }

      this.updateStatus();
    };

    CheckboxDropdown.prototype.toggleOpen = function(forceOpen) {
      var _this = this;

      if (!this.isOpen || forceOpen) {
        this.isOpen = true;
        this.$el.addClass('on');
        $(document).on('click', function(e) {
          if (!$(e.target).closest('[data-control]').length) {
            _this.toggleOpen();
          }
        });
      } else {
        this.isOpen = false;
        this.$el.removeClass('on');
        $(document).off('click');
      }
    };

    var checkboxesDropdowns = document.querySelectorAll('[data-control="checkbox-dropdown"]');
    for (var i = 0, length = checkboxesDropdowns.length; i < length; i++) {
      new CheckboxDropdown(checkboxesDropdowns[i]);
    }
  })(jQuery);