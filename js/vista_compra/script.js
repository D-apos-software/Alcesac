function load(page) {
    var q = $("#q").val();
    var parametros = {
        "page": page,
        "q": q
    };
    $("#loader").fadeIn('slow');
    $.ajax({
        url: './?action=buscar_producto_compra',
        data: parametros,
        beforeSend: function (objeto) {
            $('#loader').html('<img src="core/pedidos/img/ajax-loader.gif"> Cargando...');
        },
        success: function (data) {
            $(".outer_div").html(data).fadeIn('slow');
            $('#loader').html('');

        }
    })
}

function agregar(id) {

    //Declaramos funcion principal la cual realiza una consulta al action agregar_tmp_compra para añadir los datos seleccionados a la tabla de la vista compra
    function agregar_cont() {
        var precio_venta = $('#precio_venta_' + id).val();
        var cantidad = $('#cantidad_' + id).val();
        var descripcion = $('#descripcion_' + id).val();
        //Inicia validacion
        if (isNaN(cantidad)) {
            alert('Esto no es un numero');
            document.getElementById('cantidad_' + id).focus();
            return false;
        }
        if (isNaN(precio_venta)) {
            alert('Esto no es un numero');
            document.getElementById('precio_venta_' + id).focus();
            return false;
        }

        //Fin validacion
        var parametros = {
            "id": id,
            "precio_venta": precio_venta,
            "cantidad": cantidad
        };
        $.ajax({
            type: "POST",
            url: "./?action=agregar_tmp_compra",
            data: parametros,
            beforeSend: function (objeto) {
                $("#resultados").html("Mensaje: Cargando...");
            },
            success: function (datos) {
                $("#resultados").html(datos);

            }
        });
    }

    var cantidad_aum = $('#cantidad_' + id).val();
    var stock = document.getElementById("stock").value;
    var parametros = {
        "id_producto": id,
        "cantidad_aum": cantidad_aum,
        "estado": "bodega"

    };
    //alert(stock);
    $.ajax({
        type: "POST",
        url: './?action=validacion-compras',
        data: parametros,
        success: function (data) {
            if (data == 1) {


                //validamos si el stock del producto añadir es mayor o igual al stock maximo realizar aviso antes de realizar la compra

                var cantidad_aum = $('#cantidad_' + id).val();

                var parametros = {
                    "id_producto": id,
                    "cantidad_aum": cantidad_aum,
                    "estado": "cant_maxima",
                    "stock": stock


                };
                $.ajax({
                    type: "POST",
                    url: './?action=validacion-compras',
                    data: parametros,
                    success: function (data) {
                        if (data == 2) {

                            agregar_cont();

                        } else if (data == 1) {

                            const swalWithBootstrapButtons = Swal.mixin({
                                customClass: {
                                    confirmButton: 'btn btn-success',
                                    cancelButton: 'btn btn-danger'
                                },
                                buttonsStyling: false
                            })

                            swalWithBootstrapButtons.fire({
                                title: 'Estas seguro?',
                                text: "Este producto se encuentra al maximo de stock",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Si, agregar',
                                cancelButtonText: 'No, cancelar!',
                                reverseButtons: true
                            }).then((result) => {
                                if (result.isConfirmed) {

                                    agregar_cont();
                                } else if (
                                    /* Read more about handling dismissals below */
                                    result.dismiss === Swal.DismissReason.cancel
                                ) {
                                    $("#myModal").modal();
                                }
                            })

                        }
                    }
                })




            } else {
                swal({
                    title: "¡ERROR!",
                    text: "La cantidad del producto supera la cantidad máxima del almacenamiento de la bodega en donde se encuentra.",
                    type: "error",
                });
            }
        }
    })


}



function eliminar(id) {

    $.ajax({
        type: "GET",
        url: "./?action=agregar_tmp_compra",
        data: "id=" + id,
        beforeSend: function (objeto) {
            $("#resultados").html("Mensaje: Cargando...");
        },
        success: function (datos) {
            $("#resultados").html(datos);
        }
    });

}