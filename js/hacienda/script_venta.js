
//---------- Token -------------//
function GetToken(Lista, ListaV, ListaAdm) {
  axios
    //.post("https://alcesac.com/api_hacienda/www/api.php", {
    .post("http://localhost/Api_haciendav3/www/api.php", {
      w: 'procesos_optimizados',
      r: 'token_acces',
      grant_type: 'password',
      client_id: 'api-stag',
      password: '@W>oMjx#0K#})l0#xL>!',
      username: 'cpf-02-0791-0714@stag.comprobanteselectronicos.go.cr'
    }).then((response) => {
      rest = response.data;
      res3 = rest.replace("{", "*{");
      res2 = res3.split('*')[1]
      res3 = res2.replace("}", "}*");
      var frg = res3.split("*");
      var result_2 = frg[0];
      var tsl = JSON.parse(result_2);
      //console.log(tsl.resp);
      //return tsl.resp;
      var Token = tsl.resp;
      Creacion_ClaveXML_XML_FIRMA(Lista, ListaV, ListaAdm, Token);
    })
    .catch((error) => {
      console.log(error);
    });
}


//---------- CreacionClaveXM, XML , FIRMA XML -------------//
function Creacion_ClaveXML_XML_FIRMA(Lista, ListaV, ListaAdm, Token) {



  // -------------------------------  CREACION DE JSON DETALLES --------------------------------//
  const resultado = [];
  const impuestoResultado = [];
  const descuetoResultado = [];


  total_serv_gravados = 0;
  total_serv_exentos = 0;
  total_merca_gravados = 0;
  total_merca_exentos = 0;
  total_gravados = 0;
  total_exentos = 0;
  total_ventas = 0;
  total_descuentos = 0;
  total_ventas_neta = 0;
  total_impuestos = 0;
  total_comprobante = 0;



  for (var i = 0; i < Lista[1].length; i++) {
    //console.log(Lista[1][i]);
    total_serv_gravados += Lista[1][i][0];
    total_serv_exentos += Lista[1][i][1];
    total_gravados += Lista[1][i][2];
    total_exentos += Lista[1][i][3];
    total_ventas += Lista[1][i][4];
    total_descuentos += Lista[1][i][5];
    total_ventas_neta += Lista[1][i][6];
    total_impuestos += Lista[1][i][7];
    total_comprobante += Lista[1][i][8];
    total_merca_gravados += Lista[1][i][9];
    total_merca_exentos += Lista[1][i][10];
  }

  total_serv_gravados = DistanciaDigito(total_serv_gravados);
  total_serv_exentos = DistanciaDigito(total_serv_exentos);
  total_gravados = DistanciaDigito(total_gravados);
  total_exentos = DistanciaDigito(total_exentos);
  total_ventas = DistanciaDigito(total_ventas);
  total_descuentos = DistanciaDigito(total_descuentos);
  total_ventas_neta = DistanciaDigito(total_ventas_neta);
  total_impuestos = DistanciaDigito(total_impuestos);
  total_comprobante = DistanciaDigito(total_comprobante);
  total_merca_gravados = DistanciaDigito(total_merca_gravados);
  total_merca_exentos = DistanciaDigito(total_merca_exentos);

  Lista[1] =
    [
      total_serv_gravados,
      total_serv_exentos,
      total_gravados,
      total_exentos,
      total_ventas,
      total_descuentos,
      total_ventas_neta,
      total_impuestos,
      total_comprobante,
      total_merca_gravados,
      total_merca_exentos
    ]

  console.log(Lista[1]);


  //--------------------- Ceteamos archivo ----------------//
  for (var i = 0; i < Lista[0].length; i++) {



    //---- CONDICIONES ---//
    var constDescuento = Lista[0][i][5] - Lista[0][i][6];
    var constImpuesto = 0;
    if (Lista[0][i][8][3]) {
      constImpuesto = Lista[0][i][8][3];
    }

    //---- FIN CONDICIONES ---//


    // ----------------------------------------------------------------- CON IMPUESTOS ------------------------------------------------------------------------------------------------
    if (constImpuesto > 0 && constDescuento == 0) {
      console.log(1);
      impuestoResultado.pop();

      impuestoCIM = DistanciaIMDESCDigito(Lista[0][i][8]);

      impuestoResultado.push({
        codigo: impuestoCIM[0],
        codigoTarifa: impuestoCIM[1],
        tarifa: impuestoCIM[2],
        monto: impuestoCIM[3]
      });

      var restimp = Object.assign({}, impuestoResultado);


      precioUnitarioCIM = DistanciaDigito(Lista[0][i][4]);
      montoTotalCIM = DistanciaDigito(Lista[0][i][5]);
      subtotalCIM = DistanciaDigito(Lista[0][i][6]);
      montoTotalLineaCIM = DistanciaDigito(Lista[0][i][7]);




      resultado.push({
        codigo: Lista[0][i][0],
        cantidad: Lista[0][i][1],
        unidadMedida: Lista[0][i][2],
        detalle: Lista[0][i][3],
        precioUnitario: precioUnitarioCIM,
        montoTotal: montoTotalCIM,
        subtotal: subtotalCIM,
        montoTotalLinea: montoTotalLineaCIM,
        impuesto: restimp

      });

    }
    // ----------------------------------------------------------------- FIN CON IMPUESTOS ------------------------------------------------------------------------------------------------

    // ----------------------------------------------------------------- CON DESCUENTO - CON IMPUESTOS ------------------------------------------------------------------------------------------------

    else if (constDescuento > 0 && constImpuesto > 0) {

      console.log(2);
      impuestoResultado.pop();
      descuetoResultado.pop();


      impuestoCIM = DistanciaIMDESCDigito(Lista[0][i][8]);

      impuestoResultado.push({
        codigo: impuestoCIM[0],
        codigoTarifa: impuestoCIM[1],
        tarifa: impuestoCIM[2],
        monto: impuestoCIM[3]
      });

      descuento = Lista[0][i][5] - Lista[0][i][6];
      descuento = DistanciaDigito(descuento);
      console.log(descuento);
      descuetoResultado.push({
        montoDescuento: descuento,
        naturalezaDescuento: 'Descuento'
      });


      var resdesc = Object.assign({}, descuetoResultado);
      var restimp = Object.assign({}, impuestoResultado);



      precioUnitarioCIM = DistanciaDigito(Lista[0][i][4]);
      montoTotalCIM = DistanciaDigito(Lista[0][i][5]);
      subtotalCIM = DistanciaDigito(Lista[0][i][6]);
      montoTotalLineaCIM = DistanciaDigito(Lista[0][i][7]);


      resultado.push({
        codigo: Lista[0][i][0],
        cantidad: Lista[0][i][1],
        unidadMedida: Lista[0][i][2],
        detalle: Lista[0][i][3],
        precioUnitario: precioUnitarioCIM,
        montoTotal: montoTotalCIM,
        subtotal: subtotalCIM,
        montoTotalLinea: montoTotalLineaCIM,
        impuesto: restimp,
        descuento: resdesc
      });


    }
    // ----------------------------------------------------------------- FIN CON DESCUENTO - CON IMPUESTOS ------------------------------------------------------------------------------------------------

    // -----------------------------------------------------------------CON DESCUENTO ------------------------------------------------------------------------------------------------

    else if (constDescuento > 0 && constImpuesto == 0) {

      descuetoResultado.pop();
      descuento = Lista[0][i][5] - Lista[0][i][6];
      descuento = DistanciaDigito(descuento);
      //console.log(descuento);
      descuetoResultado.push({
        montoDescuento: descuento.toString(),
        naturalezaDescuento: 'Descuento'
      });

      var restdesc = Object.assign({}, descuetoResultado);

      resultado.push({
        codigo: Lista[0][i][0],
        cantidad: Lista[0][i][1],
        unidadMedida: Lista[0][i][2],
        detalle: Lista[0][i][3],
        precioUnitario: Lista[0][i][4].toString(),
        montoTotal: Lista[0][i][5].toString(),
        subtotal: Lista[0][i][6].toString(),
        montoTotalLinea: Lista[0][i][7].toString(),
        descuento: restdesc
      });
      console.log(3);

    }

    // ----------------------------------------------------------------- FIN CON DESCUENTO------------------------------------------------------------------------------------------------

    // ----------------------------------------------------------------- SIN DESCUENTO - SIN IMPUESTOS ------------------------------------------------------------------------------------------------
    else {
      console.log(4);

      resultado.push({
        codigo: Lista[0][i][0],
        cantidad: Lista[0][i][1],
        unidadMedida: Lista[0][i][2],
        detalle: Lista[0][i][3],
        precioUnitario: Lista[0][i][4].toString(),
        montoTotal: Lista[0][i][5].toString(),
        subtotal: Lista[0][i][6].toString(),
        montoTotalLinea: Lista[0][i][7].toString(),
      });
    }

  }

  // ----------------------------------------------------------------- FIN SIN DESCUENTO - SIN IMPUESTOS ------------------------------------------------------------------------------------------------

  var rest = Object.assign({}, resultado);
  var productos_listados = JSON.stringify(rest);
  //console.log(productos_listados);

  // ----------------------------------- FIN CREACION DE JSON --------------------------------//

  axios
    //.post("https://alcesac.com/api_hacienda/www/api.php", {
    .post("http://localhost/Api_haciendav3/www/api.php", {
      w: 'procesos_optimizados',
      r: 'Firma_xml_clave',
      client_id: 'api-stag', //Sandbox pasar a produccion 
      grant_type: 'password',

      tipoCedula: 'fisico',
      cedula: '207910714',
      tipoDocumento: 'FE',
      fecha_emision: '2022-01-17T00:46:00-06:00',
      emisor_nombre: 'Gabriel Quesada',
      emisor_tipo_identif: '01',
      emisor_num_identif: '207910714',
      nombre_comercial: 'Gabriel Quesada',
      emisor_provincia: '2',
      emisor_canton: '10',
      emisor_distrito: '01',
      emisor_barrio: '01',
      emisor_otras_senas: 'Frente a la escuela',
      emisor_cod_pais_tel: '506',
      emisor_tel: '61749563',
      emisor_email: 'usuariodapos@gmail.com',

      receptor_nombre: ListaV[0],
      receptor_tipo_identif: ListaV[1],
      receptor_num_identif: ListaV[2],
      receptor_provincia: '2',
      receptor_canton: '10',
      receptor_distrito: '01',
      receptor_barrio: '01',
      receptor_cod_pais_tel: '506',
      receptor_tel: '61749563',
      receptor_email: ListaV[4],
      condicion_venta: '01',
      plazo_credito: '0',
      plazos_credito: '0',
      medio_pago: '01',
      medios_pago: '01',
      cod_moneda: ListaAdm[5],
      tipo_cambio: ListaV[3],
      //-------------------------------- Valores IMP ------------------------//
      total_serv_gravados: Lista[1][0],
      total_serv_exentos: Lista[1][1],
      total_merc_gravada: Lista[1][9],
      total_merc_exenta: Lista[1][10],
      total_gravados: Lista[1][2],
      total_exentos: Lista[1][3],
      total_exento: Lista[1][3],
      total_ventas: Lista[1][4],
      total_descuentos: Lista[1][5],
      total_ventas_neta: Lista[1][6],
      total_impuestos: Lista[1][7],
      total_comprobante: Lista[1][8],
      //-------------------------------- Fin Valores IMP ------------------------//
      detalles: productos_listados,
      otros: 'Muchas Gracias',
      otrosType: 'OtroTexto',
      codigo_actividad: '990001',
      pinP12: '1234',
      omitir_receptor: 'true',
      password_email: 'D-apos2021**',
      token_llave: 'e8323365a649c9da903a2776a9fb586c',
    }).then((response) => {
      rest = response.data;
      res3 = rest.replace("{", "*{");
      res2 = res3.split('*')[1]
      res3 = res2.replace("}", "}*");
      var frg = res3.split("*");
      var result_2 = frg[0];
      var tsl = JSON.parse(result_2);
      //console.log(tsl.resp); 
      var datar = JSON.parse(tsl.resp);
      var clave = datar[1];
      var pdfFirmado = datar[0];
      var consecutivo = datar[2];
      enviar_hacienda(Lista, ListaV, ListaAdm, Token, clave, pdfFirmado, consecutivo, productos_listados,resultado);
    })
    .catch((error) => {
      console.log(error);
    });
}

function enviar_hacienda(Lista, ListaV, ListaAdm, Token, clave, pdfFirmado, consecutivo, productos_listados,resultado) {
  axios
    //.post("https://alcesac.com/api_hacienda/www/api.php", {
    .post("http://localhost/Api_haciendav3/www/api.php", {
      w: 'procesos_optimizados',
      r: 'enviar_hacienda',
      Token_session: Token,
      XmlClave: clave,
      R_XmlFirmado: pdfFirmado,
      client_id: 'api-stag',
      emisor_nombre: 'Gabriel Quesada',
      emisor_tipo_identif: '01',
      emisor_num_identif: '207910714',
      nombre_comercial: 'Gabriel Quesada',
      emisor_provincia: '2',
      emisor_canton: '10',
      emisor_distrito: '01',
      emisor_barrio: '01',
      emisor_otras_senas: 'Frente a la escuela',
      emisor_cod_pais_tel: '506',
      emisor_tel: '61749563',
      emisor_email: 'usuariodapos@gmail.com',
      receptor_nombre: ListaV[0],
      receptor_tipo_identif: ListaV[1],
      receptor_num_identif: ListaV[2],
      receptor_provincia: '2',
      receptor_canton: '10',
      receptor_distrito: '01',
      receptor_barrio: '01',
      receptor_cod_pais_tel: '506',
      receptor_tel: '61749563',
      receptor_email: ListaV[4],
      fecha_emision: '2022-01-17T00:46:00-06:00',
    }).then((response) => {
      rest = response.data;
      /* res3 = rest.replace("{", "*{");
      res2 = res3.split('*')[1]
      res3 = res2.replace("}", "}*");
      var frg = res3.split("*");
      var result_2 = frg[0];
      var tsl = JSON.parse(result_2); */
      /* console.log(response); 
      console.log(clave);
      console.log(Token);   */
      estado_hacienda(Lista, ListaV, ListaAdm, Token, clave, pdfFirmado, consecutivo, productos_listados,resultado);
    })
    .catch((error) => {
      console.log(error);
    });
}

function estado_hacienda(Lista, ListaV, ListaAdm, Token, Clave, pdfFirmado, consecutivo, productos_listados,resultado) {
  ListaRegistroFact = [
    //Emisor
    ListaV[2],
    'FE',
    ListaAdm[18],
    ListaAdm[6],
    ListaAdm[4],
    ListaAdm[8],
    ListaAdm[7],
    ListaAdm[9],
    ListaAdm[10],
    ListaAdm[11],
    ListaAdm[12],
    ListaAdm[13],
    ListaAdm[15],
    'usuariodapos@gmail.com', //Seteamos correo que se utilizará dentro del api (por defecto el que enviara los correos)
    //Receptor
    ListaV[0],
    ListaV[1],
    ListaV[2],
    'ND',
    'ND',
    'ND',
    'ND',
    'ND',
    'ND',
    ListaV[4],
    //Datos Generales
    '01',
    '0',
    ListaV[5],
    ListaAdm[5],
    ListaV[3],
    Lista[1][0],
    Lista[1][1],
    '0',
    '0',
    Lista[1][2],
    Lista[1][3],
    Lista[1][4],
    Lista[1][5],
    Lista[1][6],
    Lista[1][7],
    Lista[1][8],

    ListaAdm[3],
    ListaV[7],
    ListaV[6],
		"true",
  ];


  axios
    //.post("https://alcesac.com/api_hacienda/www/api.php", {
    .post("http://localhost/Api_haciendav3/www/api.php", {
      w: 'procesos_optimizados',
      r: 'estado',
      Token_session: Token,
      XmlClave: Clave,
      client_id: 'api-stag',
    }).then((response) => {
      rest = response.data;
      res3 = rest.replace("{", "*{");
      res2 = res3.split('*')[1]
      res3 = res2.replace("}", "}*");
      var frg = res3.split("*");
      var result_2 = frg[0];
      var tsl = JSON.parse(result_2);

      var datarXM = JSON.parse(tsl.resp);
      var estado_hac = datarXM[1];
      var R_XmlFirmado = datarXM[0];
      console.log(estado_hac);

      if (estado_hac != "aceptado") {

        if (estado_hac == "procesando") {

          EstadoR(Lista, ListaV, ListaAdm, Token, Clave, pdfFirmado, consecutivo, productos_listados, resultado);

        } else {

          Swal.fire({
            icon: 'info',
            title: 'Estado de Factura: ' + estado_hac,
            text: 'Esperemos lo entiendas!',
            footer: '<a href=""></a>'
          }).then((respuesta) => {
            registrar_datos_fact(ListaRegistroFact,resultado,estado_hac);
            window.location = "index.php?view=venta"; //return estado_hac;
          });

        }
      } else {
        registrar_datos_fact(ListaRegistroFact,resultado,estado_hac);
        enviar_Dcs(Lista, ListaV, ListaAdm, R_XmlFirmado, pdfFirmado, Clave, consecutivo, productos_listados);

      }

    })
    .catch((error) => {
      console.log(error);
    });
}


function enviar_Dcs(Lista, ListaV, ListaAdm, R_XmlFirmado, xml_firmado, XmlClave, consecutivo, productos_listados) {

  axios
    //.post("https://alcesac.com/api_hacienda/www/api.php", {
  .post("http://localhost/Api_haciendav3/www/api.php", {
      w: 'procesos_optimizados',
      r: 'enviar_doc',
      client_id: 'api-stag',
      grant_type: 'password',
      tipoCedula: 'fisico',
      cedula: '207910714',
      tipoDocumento: 'FE',
      fecha_emision: '2022-01-17T00:46:00-06:00',
      emisor_nombre: 'Gabriel Quesada',
      emisor_tipo_identif: '01',
      emisor_num_identif: '207910714',
      nombre_comercial: 'Gabriel Quesada',
      emisor_provincia: '2',
      emisor_canton: '10',
      emisor_distrito: '01',
      emisor_barrio: '01',
      emisor_otras_senas: 'Frente a la escuela',
      emisor_cod_pais_tel: '506',
      emisor_tel: '61749563',
      emisor_email: 'usuariodapos@gmail.com',
      receptor_nombre: ListaV[0],
      receptor_tipo_identif: ListaV[1],
      receptor_num_identif: ListaV[2],
      receptor_provincia: '2',
      receptor_canton: '10',
      receptor_distrito: '01',
      receptor_barrio: '01',
      receptor_cod_pais_tel: '506',
      receptor_tel: '61749563',
      receptor_email: ListaV[4],
      condicion_venta: '01',
      plazo_credito: '0',
      plazos_credito: '0',
      medio_pago: '01',
      medios_pago: '01',
      cod_moneda: ListaV[3],
      tipo_cambio: '564.48',
      //-------------------------------- Valores IMP ------------------------//
      total_serv_gravados: Lista[1][0],
      total_serv_exentos: Lista[1][1],
      total_merc_gravada: Lista[1][9],
      total_merc_exenta: Lista[1][10],
      total_gravados: Lista[1][2],
      total_exentos: Lista[1][3],
      total_exento: Lista[1][3],
      total_ventas: Lista[1][4],
      total_descuentos: Lista[1][5],
      total_ventas_neta: Lista[1][6],
      total_impuestos: Lista[1][7],
      total_comprobante: Lista[1][8],
      //-------------------------------- Fin Valores IMP ------------------------//
      detalles: productos_listados,
      otros: 'Muchas Gracias',
      otrosType: 'OtroTexto',
      codigo_actividad: '990001',
      pinP12: '1234',
      omitir_receptor: 'true',
      password_email: 'D-apos2021**',
      token_llave: 'e8323365a649c9da903a2776a9fb586c',

      R_XmlFirmado: R_XmlFirmado,
      xml_firmado: xml_firmado,
      XmlClave: XmlClave,
      consecutivo: consecutivo,

    }).then((response) => {
      rest = response.data;
      res3 = rest.replace("{", "*{");
      res2 = res3.split('*')[1]
      res3 = res2.replace("}", "}*");
      var frg = res3.split("*");
      var result_2 = frg[0];
      var tsl = JSON.parse(result_2);
      console.log(response);
      window.location = "index.php?view=venta"; 

    })
    .catch((error) => {
      console.log(error);
    });
}

function cls(Lista, ListaV, ListaAdm) {
  GetToken(Lista, ListaV, ListaAdm);
}


function Espera() {
  let timerInterval
  Swal.fire({
    icon: 'success',
    title: 'Venta satisfactoria',
    html: 'Enviando documentos de compra <b></b>',
    timerProgressBar: true,
    didOpen: () => {
      Swal.showLoading()
      const b = Swal.getHtmlContainer().querySelector('b')
      timerInterval = setInterval(() => {
        b.textContent = Swal.getTimerLeft()
      })
    },
    willClose: () => {
      clearInterval(timerInterval)
    }
  }).then((result) => {
    /* Read more about handling dismissals below */
    if (result.dismiss === Swal.DismissReason.timer) {
      console.log('I was closed by the timer')
    }
  })
}


function DistanciaDigito(Dg) {
  return Dg.toFixed(5);
}

function DistanciaIMDESCDigito(Dg) {
  list = [];
  for (var i = 0; i < Dg.length; i++) {
    dat = Dg[i];
    //console.log(dat % 1 == 0);
    if (esEntero(dat) == 0) {
      dat = dat.toFixed(5);
      //console.log("entra");
    }
    list.push(dat.toString());
  }
  return list;
}

function esEntero(numero) {
  if (isNaN(numero)) {
    return 3;
  } else {
    if (numero % 1 == 0) {
      return 1;
    } else {
      return 0;
    }
  }
}

function EstadoR(Lista, ListaV, ListaAdm, Token, Clave, pdfFirmado, consecutivo, productos_listados) {
  axios
    //.post("https://alcesac.com/api_hacienda/www/api.php", {
    .post("http://localhost/Api_haciendav3/www/api.php", {
      w: 'procesos_optimizados',
      r: 'estado',
      Token_session: Token,
      XmlClave: Clave,
      client_id: 'api-stag',
    }).then((response) => {
      rest = response.data;
      res3 = rest.replace("{", "*{");
      res2 = res3.split('*')[1]
      res3 = res2.replace("}", "}*");
      var frg = res3.split("*");
      var result_2 = frg[0];
      var tsl = JSON.parse(result_2);

      var datarXM = JSON.parse(tsl.resp);
      var estado_hac = datarXM[1];
      var R_XmlFirmado = datarXM[0];
      console.log(estado_hac);

      if (estado_hac != "aceptado") {

        if (estado_hac == "procesando") {

          estado_hacienda(Lista, ListaV, ListaAdm, Token, Clave, pdfFirmado, consecutivo, productos_listados);

        } else {

          Swal.fire({
            icon: 'info',
            title: 'Estado de Factura: ' + estado_hac,
            text: 'Esperemos lo entiendas!',
            footer: '<a href="">Why do I have this issue?</a>'
          }).then((respuesta) => {
            registrar_datos_fact(ListaRegistroFact,resultado,estado_hac);
            window.location = "index.php?view=venta";
          });

        }
      } else {
        enviar_Dcs(Lista, ListaV, ListaAdm, R_XmlFirmado, pdfFirmado, Clave, consecutivo, productos_listados);
      }

    })
    .catch((error) => {
      console.log(error);
    });
}

function registrar_detalle_prod(detalles,id_fact_hacienda){
  var parametros = {
    "codigo": detalles.codigo,
    "cantidad": detalles.cantidad,
    "unidad_medida": detalles.unidadMedida,
    "detalle": detalles.detalle,
    "precio_unitario": detalles.precioUnitario,
    "monto_total": detalles.montoTotal,
    "subtotal": detalles.subtotal,
    "monto_total_lineal": detalles.montoTotalLinea,
    "d_monto_descuento": detalles.descuento[0].montoDescuento,
    "d_naturaleza_descuento": detalles.descuento[0].naturalezaDescuento,
    "imp_codigo": detalles.impuesto[0].codigo,
    "imp_codigo_Tarifa": detalles.impuesto[0].codigoTarifa,
    "imp_tarifa": detalles.impuesto[0].tarifa,
    "mp_monto": detalles.impuesto[0].monto,
    "id_fact_hacienda": id_fact_hacienda
  };
  $.ajax({
    type: "POST",
    url: "./?action=action_Fac_registro_productos",
    data: parametros,
    success: function (datos) {
      console.log("registro exitoso");
      return datos;
    }
  });
}


function registrar_datos_fact(datos_fact,elementos,estado_hac){
    if(estado_hac == "aceptado"){
      estado_hac = 0;
    }else{
      estado_hac = 1;
    }

    var parametros = {
    "id_cliente": datos_fact[0],
    "tipo_documento": datos_fact[1],
    "tipo_cedula": datos_fact[2],
    "fecha_emision": datos_fact[3],
    "emisor_nombre": datos_fact[4],
    "emisor_tipo_identif": datos_fact[5],
    "emisor_num_identif": datos_fact[6],
    "nombre_comercial": datos_fact[7],
    "emisor_provincia": datos_fact[8],
    "emisor_canton": datos_fact[9],
    "emisor_distrito": datos_fact[10],
    "emisor_barrio": datos_fact[11],
    "emisor_cod_pais_tel": datos_fact[12],
    "emisor_email": datos_fact[13],

    

    "receptor_nombre": datos_fact[14],
    "receptor_tipo_identif": datos_fact[15],
    "receptor_num_identif": datos_fact[16],
    "receptor_provincia": datos_fact[17],

    "receptor_canton": datos_fact[18],
    "receptor_distrito": datos_fact[19],
    "receptor_barrio": datos_fact[20],
    "receptor_cod_pais_tel": datos_fact[21],
    "receptor_tel": datos_fact[22],
    "receptor_email": datos_fact[23],
    
    "condicion_venta": datos_fact[24],
    "plazo_credito": datos_fact[25],
    "medio_pago": datos_fact[26],
    "cod_moneda": datos_fact[27],
    "tipo_cambio": datos_fact[28],
    
    "total_serv_gravados": datos_fact[29],
    "total_serv_exentos": datos_fact[30],
    "total_merc_gravada": datos_fact[31],
    "total_merc_exenta": datos_fact[32],
    "total_gravados": datos_fact[33],
    "total_exentos": datos_fact[34],
    "total_ventas": datos_fact[35],
    "total_descuentos": datos_fact[36],
    "total_ventas_neta": datos_fact[37],
    "total_impuesto": datos_fact[38],
    "total_comprobante": datos_fact[39],
    "token_llave": datos_fact[40],
    "id_caja": datos_fact[41],
    "id_usuario": datos_fact[42],
    "omitir_receptor": datos_fact[43],
    "estado": estado_hac

  };
  $.ajax({
    type: "POST",
    url: "./?action=action_registro_factura",
    data: parametros,
    success: function (datos) {
      elementos.forEach(element => registrar_detalle_prod(element,datos));
    }
  }); 
}
