class Tab {
    /**
     * Create a handler around the given tab group
     *
     * @param {HTMLElement} parent - Parent element
     */
    constructor(parent) {
        this.parent = parent;
        this.labels = Array.from(parent.querySelectorAll("[role=tab]"));
        this.panels = Array.from(parent.querySelectorAll("[role=tabpanel]"));
        this.register();
    }

    /**
     * Register the necessary event handlers
     */
    register() {
        this.parent.classList.add("has-js");
        this.parent.addEventListener("click", (event) => {
            const target = event.target;

            if (target.matches("[role=tab]")) {
                this.navigate(target);
            }
        });
        this.parent.addEventListener("keydown", (event) => {
            const target = event.target;

            if (target.matches("[role=tab]")) {
                this.onKey(target, event.key);
            }
        });
    }

    /**
     * Handle a keyboard event
     *
     * @param {HTMLElement} button - Tab button
     * @param {String} key         - Keyboard key
     */
    onKey(button, key) {
        let index = this.labels.indexOf(button);

        switch (key) {
            case "ArrowRight":
                this.labels[++index] && this.labels[index].focus();
                break;
            case "ArrowLeft":
                this.labels[--index] && this.labels[index].focus();
                break;
            case " ":
            case "Enter":
                this.navigate(button);
                break;
        }
    }

    /**
     * Navigates to a tab and displays its content
     *
     * @param {HTMLElement} button - Tab button
     */
    navigate(button) {
        this.labels.forEach((label) => {
            label.setAttribute("aria-selected", "false");
            label.setAttribute("tabindex", "0");
            label.classList.remove("is-active");
        });
        this.panels.forEach((panel) => {
            panel.setAttribute("aria-expanded", "false");
            panel.setAttribute("hidden", "");
            panel.classList.remove("is-active");
        });

        const panel_id = button.getAttribute("aria-controls");
        button.setAttribute("aria-selected", "true");
        button.setAttribute("tabindex", "-1");
        button.classList.add("is-active");

        const panel = document.getElementById(panel_id);
        panel.setAttribute("aria-expanded", "true");
        panel.removeAttribute("hidden");
        panel.classList.add("is-active");
        panel.focus();
    }
}

// Just pass in the element that contains both the labels and panels
const testing = new Tab(document.querySelector(".tab"));




function actualizar(id) {
    var parametros = {
        "id": id,
        "id_estado": "actualizar"
    };
    $.ajax({
        type: "POST",
        url: 'index.php?action=action_funcionalidades',
        data: parametros,
        success: function(data) {
            Swal.fire({
                icon: 'success',
                title: 'Actulizacion exitosa',
                showConfirmButton: false,
                timer: 1700
            })
        }
    });
}