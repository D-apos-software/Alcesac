


function Validacion_cliente_existente(id_usuario) {
    parametros = {
        "id": id_usuario,
        "fs": '0'
    }
    $.ajax({
        type: "POST",
        url: './?action=action_busqueda_cliente',
        data: parametros,
        success: function (data) {

            //alert(data);

            if (data != null && data != "null") {
                //alert('ID ingresado existe en base de datos.');
                //Si existe hay que validar si se encuentra en una habitacion o si no.
                sim = JSON.parse(data);
                //alert( sim.id_categoria_p);
                if( sim.id_categoria_p != null){
                    document.getElementById('categoria').value = sim.id_categoria_p;
                }else{
                    document.getElementById('categoria').options.selectedIndex = 0;
                }
                
                document.getElementById('pagado').selectedIndex = 0;
                document.getElementById('cantidad').value = 1;
                document.getElementById('id_tarifa').selectedIndex = 1;


                try {
                    //alert(sim.tipo_documento);
                    props = {
                        "TipoDocumentoData": sim.tipo_documento,
                        "fs": '3'
                    }
                    $.ajax({
                        type: "POST",
                        url: 'index.php?action=action_busqueda_cliente',
                        data: props,
                        success: function (resp) {
                            document.getElementById('tipo_documento').value = resp;
                            document.getElementById('documento').value = sim.documento;
                            document.getElementById('nombre').value = sim.nombre;
                            document.getElementById('email').value = sim.email;
                            document.getElementById('giro').value = sim.giro;
                            document.getElementById('telefono').value = sim.telefono;
                            document.getElementById('razon_social').value = sim.razon_social;
                            document.getElementById('direccion').value = sim.direccion;
                            document.getElementById('medio_transporte').value = sim.medio_transporte;
                            document.getElementById('destino').value = sim.destino;
                            document.getElementById('motivo').value = sim.motivo;
                            document.getElementById('ocupacion').value = sim.ocupacion;
                        }
                    });
                    
                }
                catch (exception_var_1) {
                    //alert(exception_var_1);
                }
                setTimeout(function () {
                    
                //Condicion si ha realizado una reserva 
                //Cargamos los campos desplegables
                val = document.getElementById('id_tarifa').value;
                $.ajax({
                    type: "POST",
                    url: 'index.php?action=tarifa',
                    data: 'id=' + val,
                    success: function (resp) {
                        $('#mostrar_precio').html(resp);
                    }
                });
            }, 100);

            } else if (data == null || data == "null") {
                //alert('ID ingresado no existe en base de datos.');
                //Realiazamos consulta directa a EL TRIBUNAL
                    $.ajax({
                        type: "GET",
                        url: 'https://api.hacienda.go.cr/fe/ae?identificacion=' + id_usuario,
                        success: function (data) {
                            console.log(parseInt(data.tipoIdentificacion));
                            document.getElementById('tipo_documento').value = parseInt(data.tipoIdentificacion); 
                            document.getElementById('documento').value = id_usuario;
                            document.getElementById('nombre').value = data.nombre;
                            document.getElementById('pagado').selectedIndex = 0;
                            document.getElementById('cantidad').value = 1;
                            document.getElementById('id_tarifa').selectedIndex = 1;
                            
                            setTimeout(function () { 
                                //Condicion si ha realizado una reserva 
                                //Cargamos los campos desplegables 
                                val = document.getElementById('id_tarifa').value;
                                $.ajax({
                                    type: "POST",
                                    url: 'index.php?action=tarifa',
                                    data: 'id=' + val,
                                    success: function (resp) {
                                        $('#mostrar_precio').html(resp);
                                    }
                                });
                            }, 100);
                        }
                    });
            }


        }
    });
}


$(document).ready(function () {
    $('#cd').click(function () {
        id = document.getElementById('cd1').value;
        Validacion_cliente_existente(id);
    });
});