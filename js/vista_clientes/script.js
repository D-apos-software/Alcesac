
    // Seccion Actualizar
    function ActualizarData(val, id) {
        if (id == 1) {
          document.getElementById('title_c' + val).style.display = 'none';
          document.getElementById('canton' + val).style.display = 'none';
          document.getElementById('title_d' + val).style.display = 'none';
          document.getElementById('distrito' + val).style.display = 'none';
        } else if (id == 2) {
          document.getElementById('title_d' + val).style.display = 'none';
          document.getElementById('distrito' + val).style.display = 'none';
        }
  
      };
  
      function CargarCantonM(val, cl_id) {
        document.getElementById("canton_vm" + cl_id).style.display = 'table-cell';
        parametros = {
          'id': '3',
          'idProvincia': val,
          'cl_id': cl_id
        }
        $.ajax({
          type: "POST",
          url: 'index.php?action=Ubicacion_JSON',
          data: parametros,
          success: function(resp) {
            $('#id_cantonm' + cl_id).html(resp);
          }
        });
      };
  
      function CargarDistritoM(val, id_pro, cl_id) {
        document.getElementById("distrito_vm" + cl_id).style.display = 'table-cell';
        parametros = {
          'id': '4',
          'idProvincia': id_pro,
          'idCanton': val,
          'cl_id': cl_id
        }
        $.ajax({
          type: "POST",
          url: 'index.php?action=Ubicacion_JSON',
          data: parametros,
          success: function(resp) {
            $('#id_distritom' + cl_id).html(resp);
            document.getElementById("cantonMD").value = val;
          }
        });
      };
      // Seccion Agregar 
      function CargarCanton(val) {
        document.getElementById("canton_v").style.display = 'table-cell';
        parametros = {
          'id': '1',
          'idProvincia': val
        }
        $.ajax({
          type: "POST",
          url: 'index.php?action=Ubicacion_JSON',
          data: parametros,
          success: function(resp) {
            $('#id_canton').html(resp);
          }
        });
      };
  
      function CargarDistrito(val, id_pro) {
        document.getElementById("distrito_v").style.display = 'table-cell';
        parametros = {
          'id': '2',
          'idProvincia': id_pro,
          'idCanton': val
        }
        $.ajax({
          type: "POST",
          url: 'index.php?action=Ubicacion_JSON',
          data: parametros,
          success: function(resp) {
            $('#id_distrito').html(resp);
          }
        });
      };
  
      function CargarBarrio(vl) {
       document.getElementById("distritoMD").value = 123;
       document.getElementById("barrio_v").style.display = 'table-cell';
      };
  
      function del(id) {
        Swal.fire({
          title: 'Estas seguro?',
          text: "",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar'
        }).then((result) => {
          if (result.isConfirmed) {
            var parametros = {
              "id": id
            };
            $('#tipo_cliente').html("Por favor espera un momento");
            $.ajax({
              type: "POST",
              url: 'index.php?view=delcliente',
              data: parametros,
              success: function(data) {
                window.location.reload(); // Recargar página
              }
            });
            //window.location="index.php?view=delete_documento&id="+$id
          }
  
        })
      }
