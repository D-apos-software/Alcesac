

$(document).ready(function () {
  $("#submit_fac").click(function () {

    //* Si el campo archivo se encuentra basido se envian solo los campos text */
    const file = document.querySelector("#llave_criptografica");
    var id_config = $('#id_config').val();
    var usuario = $('#usuario_hacienda').val();
    var contrasena = $('#contrasena_hacienda').val();
    var pin = $('#pin').val();
    var cod_moneda = $('#cod_moneda').val();
    var nombre_hac = $('#nombre_hac').val();
    var numero_id = $('#numero_id').val();
    var provincia = $('#provincia').val();
    var canton = $('#canton').val();
    var distrito = $('#distrito').val();
    var barrio = $('#barrio').val();
    var cod_pais_tel_hc = $('#cod_pais_tel_hc').val();
    var tel_hac = $('#tel_hac').val();
    var tipoCed = tipoID(numero_id);

    if (file.files[0] != 'undefined' && file.files[0] != null) {
      update_field_file(id_config, usuario, contrasena, pin, cod_moneda, nombre_hac, numero_id, provincia, canton, distrito, barrio, cod_pais_tel_hc, tel_hac, tipoCed);
    } else {
      update_without_field_file(id_config, usuario, contrasena, pin, cod_moneda, nombre_hac, numero_id, provincia, canton, distrito, barrio, cod_pais_tel_hc, tel_hac, tipoCed);
    }

  });
});



function update_without_field_file(id_config, usuario, contrasena, pin, cod_moneda, nombre_hac, numero_id, provincia, canton, distrito, barrio, cod_pais_tel_hc, tel_hac, tipoCed) {

  parametros = {
    estado: 1,
    id_config: id_config,
    usuario_hacienda: usuario,
    contrasena_hacienda: contrasena,
    pin: pin,
    cod_moneda: cod_moneda,
    nombre_hac: nombre_hac,
    numero_id: numero_id,
    provincia: provincia,
    canton: canton,
    distrito: distrito,
    barrio: barrio,
    cod_pais_tel_hc: cod_pais_tel_hc,
    tel_hac: tel_hac,
    tipoCed: tipoCed
  };

  $.ajax({
    type: "POST",
    url: 'index.php?action=action_update_configuracion',
    data: parametros,
    success: function (data) {
      console.log(data);
      if (data == 'exitoso') {
        Swal.fire({
          icon: 'success',
          title: 'Actualización exitoso',
          showConfirmButton: false,
          timer: 1700
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Error al actualizar los datos'
        })
      }
    }
  });
}



function update_token(token, id_config, usuario, contrasena, pin, cod_moneda, nombre_hac, numero_id, provincia, canton, distrito, barrio, cod_pais_tel_hc, tel_hac, tipoCed) {

  parametros = {
    estado: 0,
    id_config: id_config,
    usuario_hacienda: usuario,
    contrasena_hacienda: contrasena,
    token: token,
    pin: pin,
    cod_moneda: cod_moneda,
    nombre_hac: nombre_hac,
    numero_id: numero_id,
    provincia: provincia,
    canton: canton,
    distrito: distrito,
    barrio: barrio,
    cod_pais_tel_hc: cod_pais_tel_hc,
    tel_hac: tel_hac,
    tipoCed: 2
  };

  $.ajax({
    type: "POST",
    url: 'index.php?action=action_update_configuracion',
    data: parametros,
    success: function (data) {
      console.log(data);
      if (data == 'exitoso') {
        Swal.fire({
          icon: 'success',
          title: 'Actualización exitoso',
          showConfirmButton: false,
          timer: 1700
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Error al actualizar los datos'
        })
      }
    }
  });
}

function update_field_file(id_config, usuario, contrasena, pin, cod_moneda, nombre_hac, numero_id, provincia, canton, distrito, cod_pais_tel_hc, tel_hac, tipoCed) {
  const formData = new FormData();
  const imagefile = document.querySelector("#llave_criptografica");
  //console.log(imagefile.files[0]);
  formData.append("fileToUpload", imagefile.files[0]);
  formData.append("w", "procesos_optimizados");
  formData.append("r", "token");
  formData.append("iam", "GabrielQA");
  axios.post("http://localhost/Api_haciendav3/www/api.php", formData, {
  //axios.post("https://alcesac.com/api_hacienda/www/api.php", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  })
    .then((response) => {
      var frg = response.data.split("<");
      var result_2 = frg[0];
      var token = JSON.parse(result_2);
      token = token.resp.downloadCode;
      // ACTUALIZAMOS LOS DATOS USUARIO Y CONTRASEÑA

      update_token(token, id_config, usuario, contrasena, pin, cod_moneda, nombre_hac, numero_id, provincia, canton, distrito, cod_pais_tel_hc, tel_hac, tipoCed);

    })
    .catch((error) => {
      console.log(error);
    });

}


function file(event) {
  let file = document.getElementById('llave_criptografica');
  let extension = file.files[0].name.split('.')[1];
  let nuevo_nombre = `archivo_nuevo.${extension}`;
  file.files[0].name = nuevo_nombre;
  document.getElementById('llave_criptografica').innerHTML = `el nombre se conserva: ${file.files[0].name}`;
}


function tipoID(id_usuario) {
  $.ajax({
    type: "GET",
    url: 'https://api.hacienda.go.cr/fe/ae?identificacion='+id_usuario,
    success: function (data) {
      localStorage.setItem('tpid', data.tipoIdentificacion);
    }
  });

  return localStorage.getItem('tpid');
}
