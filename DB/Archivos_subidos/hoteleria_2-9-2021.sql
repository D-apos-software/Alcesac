-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-09-2021 a las 01:35:18
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hoteleria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aperturas_cajas`
--

CREATE TABLE `aperturas_cajas` (
  `id` int(11) NOT NULL,
  `id_caja` int(11) NOT NULL,
  `fecha_apertura` datetime NOT NULL,
  `fecha_cierre` datetime NOT NULL,
  `monto_apertura` int(11) NOT NULL,
  `monto_cierre` int(11) NOT NULL,
  `estado` varchar(500) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aperturas_cajas`
--

INSERT INTO `aperturas_cajas` (`id`, `id_caja`, `fecha_apertura`, `fecha_cierre`, `monto_apertura`, `monto_cierre`, `estado`, `id_usuario`, `fecha_creada`) VALUES
(111, 24, '2021-08-23 13:21:51', '2021-08-23 13:22:37', 1000, 2000, '0', 4, '2021-08-23 13:21:55'),
(112, 27, '2021-08-23 13:23:19', '2021-08-23 13:23:57', 1000, 3000, '0', 4, '2021-08-23 13:23:24'),
(113, 27, '2021-08-23 14:22:26', '2021-08-31 15:08:16', 1000, 4000, '0', 4, '2021-08-23 14:42:43'),
(114, 25, '2021-08-31 15:08:17', '2021-08-31 15:08:25', 2000, 2000, '0', 4, '2021-08-31 15:08:25'),
(115, 25, '2021-08-31 15:09:24', '0000-00-00 00:00:00', 2000, 3500, '1', 4, '2021-08-31 15:09:27'),
(116, 27, '2021-08-31 15:21:39', '0000-00-00 00:00:00', 2000, 4000, '1', 1, '2021-08-31 15:21:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `fecha_apertura` datetime DEFAULT NULL,
  `fecha_cierre` datetime DEFAULT NULL,
  `monto_apertura` double DEFAULT NULL,
  `monto_cierre` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`, `fecha_apertura`, `fecha_cierre`, `monto_apertura`, `monto_cierre`, `estado`, `id_usuario`, `fecha_creada`) VALUES
(22, '2021-07-13 12:53:20', '2021-07-13 12:54:10', 2000, 0, 0, 3, '2021-07-13 11:53:24'),
(23, '2021-07-13 12:54:50', '2021-07-13 12:55:02', 2000, 0, 0, 3, '2021-07-13 11:54:53'),
(24, '2021-07-13 12:56:22', '2021-07-13 13:02:04', 2000, 3, 0, 3, '2021-07-13 11:56:25'),
(25, '2021-07-13 13:05:42', '2021-07-13 13:06:06', 300000, 0, 0, 3, '2021-07-13 12:05:45'),
(26, '2021-07-13 17:41:23', '2021-07-13 17:42:04', 50000, 0, 0, 3, '2021-07-13 16:41:25'),
(27, '2021-07-13 17:44:00', '2021-07-13 17:44:22', 50000, 0, 0, 3, '2021-07-13 16:44:08'),
(28, '2021-07-14 14:53:51', '2021-07-14 14:55:16', 2000, -1000, 0, 4, '2021-07-14 13:53:55'),
(29, '2021-07-14 15:19:05', '2021-07-14 15:19:51', 2000, -1000, 0, 4, '2021-07-14 14:19:07'),
(30, '2021-07-15 13:44:07', '2021-07-15 14:49:19', 10000, 0, 0, 1, '2021-07-15 12:44:20'),
(31, '2021-07-15 14:56:22', '2021-07-22 12:11:09', 2000, 165000, 0, 1, '2021-07-15 13:56:24'),
(32, '2021-07-22 12:11:14', '2021-07-22 12:21:05', 2000, 50000, 0, 1, '2021-07-22 11:11:16'),
(33, '2021-07-24 15:46:49', NULL, 2000, NULL, 1, 1, '2021-07-24 14:46:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `imagen`, `fecha_creada`) VALUES
(1, 'Personal', 'conference.jpg', '2018-02-15 09:14:21'),
(2, 'Doble', NULL, '2019-02-26 11:39:02'),
(3, 'Triple', NULL, '2019-02-26 11:39:08'),
(4, 'Cuatruple', NULL, '2019-02-26 11:40:17'),
(5, 'Especial', NULL, '2019-02-26 11:40:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_proceso`
--

CREATE TABLE `cliente_proceso` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_proceso` int(11) DEFAULT NULL,
  `sesion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente_proceso`
--

INSERT INTO `cliente_proceso` (`id`, `id_cliente`, `id_proceso`, `sesion`) VALUES
(271, 527, 282, 'tvqb5am3t444j3prdu918aiovq'),
(272, 528, 283, 'tvqb5am3t444j3prdu918aiovq'),
(273, 532, 284, 'tvqb5am3t444j3prdu918aiovq'),
(274, 533, 285, 'tvqb5am3t444j3prdu918aiovq'),
(275, 534, 286, 'tvqb5am3t444j3prdu918aiovq'),
(276, 535, 287, 'tvqb5am3t444j3prdu918aiovq'),
(277, 537, 288, 'b2cp8u44eifm3s3idd01j10ijb'),
(278, 538, 289, '4kaugkt3hasohagdp15vn1gm98'),
(279, 539, 290, 'b37oics0r0k3ba4ipsjc3fc7eo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `rnc` varchar(25) DEFAULT NULL,
  `registro_empresarial` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `nombre`, `direccion`, `estado`, `telefono`, `fax`, `rnc`, `registro_empresarial`, `ciudad`, `logo`) VALUES
(1, 'HOTEL EDEN', 'DIRECCION EXACTA', 'Alajuela', '+506 61749563', 'NULL', '0038947384786', 'NULL', 'Cartago', 'Captura_de_pantalla__581_-removebg-preview_1.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_persona` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cajas`
--

CREATE TABLE `estado_cajas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_cajas`
--

INSERT INTO `estado_cajas` (`id`, `nombre`, `valor`, `fecha_creada`) VALUES
(3, 'Ocupada\r\n', 0, 2),
(4, 'Disponible', 1, 2),
(5, 'Mantenimiento', 3, 2),
(6, 'Deshabilitada', 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_de_pago`
--

CREATE TABLE `estado_de_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `medio_pago` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_de_pago`
--

INSERT INTO `estado_de_pago` (`id`, `nombre`, `medio_pago`, `fecha_creada`) VALUES
(2, 'Pago realizado', 1, '2021-05-14'),
(8, 'En proceso', 0, '2021-06-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--

CREATE TABLE `gasto` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `capacidad` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id`, `nombre`, `descripcion`, `precio`, `id_categoria`, `estado`, `capacidad`, `fecha_creada`) VALUES
(3, 'Habitacion 1', '1', 0, 1, 3, 1, '0000-00-00 00:00:00'),
(4, 'Habitacion 2', '1', 0, 1, 2, 1, '0000-00-00 00:00:00'),
(5, 'Habitacion 3', '1', 0, 1, 2, 1, '0000-00-00 00:00:00'),
(6, 'Habitacion 4', '1', 0, 1, 2, 1, '0000-00-00 00:00:00'),
(41, 'Habitacion 5', '2', 0, 1, 1, 1, '2021-07-18 19:08:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `compras` int(11) NOT NULL,
  `egresos` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`id`, `nombre`, `valor`, `compras`, `egresos`, `fecha_creada`) VALUES
(1, 'IVA', 0, 1, 0, '0000-00-00 00:00:00'),
(3, 'Impuesto sobre la venta 13%', 0, 0, 0, '0000-00-00 00:00:00'),
(4, 'Impuesto sobre la venta 8%', 0, 0, 0, '0000-00-00 00:00:00'),
(5, 'Impuesto sobre la venta 4%', 0, 0, 0, '0000-00-00 00:00:00'),
(6, 'Impuesto sobre la venta 2%', 1, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimiento_cajas`
--

CREATE TABLE `mantenimiento_cajas` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mantenimiento_cajas`
--

INSERT INTO `mantenimiento_cajas` (`id`, `numero`, `estado`, `fecha_creada`) VALUES
(24, 1, 1, '2021-06-29'),
(25, 2, 2, '2021-06-29'),
(26, 3, 1, '2021-06-29'),
(27, 4, 2, '2021-06-29'),
(28, 5, 1, '2021-07-03'),
(30, 7, 1, '2021-07-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_pago`
--

CREATE TABLE `periodos_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(250) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `periodos_pago`
--

INSERT INTO `periodos_pago` (`id`, `nombre`, `valor`, `fecha_creada`) VALUES
(2, 'CONTADO', 0, '2021-06-05'),
(3, 'Limite de fecha', 0, '2021-06-09'),
(4, 'Por pagos', 0, '2021-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `tipo_documento` int(11) DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `razon_social` varchar(150) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  `tipo` int(11) DEFAULT 1,
  `vip` int(11) NOT NULL DEFAULT 0,
  `contador` int(11) NOT NULL DEFAULT 0,
  `limite` int(11) NOT NULL DEFAULT 7,
  `nacionalidad` varchar(25) DEFAULT NULL,
  `estado_civil` varchar(12) DEFAULT NULL,
  `ocupacion` varchar(255) DEFAULT NULL,
  `medio_transporte` varchar(65) DEFAULT NULL,
  `destino` varchar(55) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `celular` varchar(25) DEFAULT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `tipo_documento`, `documento`, `giro`, `nombre`, `fecha_nac`, `razon_social`, `direccion`, `fecha_creada`, `tipo`, `vip`, `contador`, `limite`, `nacionalidad`, `estado_civil`, `ocupacion`, `medio_transporte`, `destino`, `motivo`, `telefono`, `celular`, `email`) VALUES
(527, 2, '1', '1', '1', NULL, '1', 'Alajuela', '2021-08-23 13:21:58', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '61749563', NULL, '123@123.com'),
(528, 71, '1', '1', '1', NULL, '1', 'Alajuela', '2021-08-23 13:22:34', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '61749563', NULL, '123@123.com'),
(529, NULL, NULL, NULL, '1', NULL, NULL, NULL, '2021-08-23 14:32:44', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(530, NULL, NULL, NULL, '1', NULL, NULL, NULL, '2021-08-23 14:34:12', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(531, NULL, NULL, NULL, '1', NULL, NULL, NULL, '2021-08-23 14:34:36', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(532, 2, '1', '1', '1', NULL, '1', '1', '2021-08-23 14:47:55', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', NULL, '123@123.com'),
(533, 2, '2', '1', '1', NULL, '1', '1', '2021-08-23 15:36:57', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', NULL, '123@123.com'),
(534, 71, '3', '1', '1', NULL, '1', 'Alajuela', '2021-08-23 15:49:05', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '61749563', NULL, '123@123.com'),
(535, 2, '12', '1', '1', NULL, '1', '1', '2021-08-23 15:49:47', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', NULL, '123@123.com'),
(536, NULL, NULL, NULL, '1', NULL, NULL, NULL, '2021-08-31 15:12:32', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(537, 71, '1', '1', '1', NULL, '1', '1', '2021-09-01 10:27:53', 1, 0, 0, 7, NULL, NULL, '1', '1', '123', '1', '1', NULL, '123@123.com'),
(538, 2, '1', '1', '1', NULL, '1', '1', '2021-09-01 15:45:40', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', NULL, 'gabrielqqquesada@gmail.com'),
(539, 2, '5', '5', '5', NULL, '5', '5', '2021-09-01 16:20:42', 1, 0, 0, 7, NULL, NULL, '1', '5', '5', '5', '55', NULL, '123@123.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL,
  `tipo_proceso` int(11) NOT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_apertura` int(11) NOT NULL,
  `precio` double NOT NULL DEFAULT 0,
  `cobro_ext` int(11) NOT NULL,
  `cant_noche` float NOT NULL DEFAULT 1,
  `dinero_dejado` double NOT NULL DEFAULT 0,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `fecha_entrada` datetime DEFAULT NULL,
  `fecha_salida` datetime DEFAULT NULL,
  `total` double NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `cant_personas` double DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `fecha_creada` datetime DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT 1,
  `observacion` varchar(255) DEFAULT NULL,
  `pagado` int(11) DEFAULT NULL,
  `nro_operacion` int(25) DEFAULT NULL,
  `num_tarjeta` int(11) DEFAULT NULL,
  `banco` int(11) DEFAULT NULL,
  `num_aprobacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id`, `tipo_proceso`, `id_habitacion`, `id_tarifa`, `id_cliente`, `id_apertura`, `precio`, `cobro_ext`, `cant_noche`, `dinero_dejado`, `id_tipo_pago`, `fecha_entrada`, `fecha_salida`, `total`, `id_usuario`, `cant_personas`, `id_caja`, `estado`, `fecha_creada`, `cantidad`, `observacion`, `pagado`, `nro_operacion`, `num_tarjeta`, `banco`, `num_aprobacion`) VALUES
(282, 0, 3, 65, 527, 112, 1000, 0, 1, 0, 1, '2021-08-23 13:21:58', '2021-08-24 12:00:00', 0, 4, 1, 112, 1, '2021-08-23 13:21:58', 1, NULL, 1, 0, 0, 1, 0),
(283, 1, 4, 67, 528, 112, 2000, 1000, 1, 0, 1, '2021-08-23 13:22:34', '2021-08-24 12:00:00', 0, 4, 1, 111, 1, '2021-08-23 13:22:34', 1, NULL, 1, 0, 0, 2, 0),
(284, 1, 4, 67, 532, 113, 1000, 0, 1, 0, 1, '2021-08-23 14:47:55', '2021-08-24 12:00:00', 0, 4, 1, 113, 1, '2021-08-23 14:47:55', 1, NULL, 1, 0, 0, 1, 0),
(285, 1, 5, 66, 533, 113, 1000, 0, 1, 0, 1, '2021-08-23 15:36:57', '2021-08-24 12:00:00', 0, 4, 1, 113, 1, '2021-08-23 15:36:57', 1, NULL, 1, 0, 0, 2, 0),
(286, 1, 4, 67, 534, 113, 0, 0, 0, 0, 1, '2021-08-23 15:49:05', '2021-08-24 12:00:00', 0, 4, 1, 113, 1, '2021-08-23 15:49:05', 1, NULL, 1, 0, 0, 0, 0),
(287, 1, 5, 66, 535, 113, 1000, 0, 1, 0, 1, '2021-08-23 15:49:47', '2021-08-24 12:00:00', 0, 4, 1, 113, 1, '2021-08-23 15:49:47', 1, NULL, 1, 0, 0, 2, 0),
(288, 1, 6, 17, 537, 116, 1000, 0, 1, 0, 1, '2021-09-01 10:27:53', '2021-09-02 12:00:00', 0, 1, 1, 116, 0, '2021-09-01 10:27:53', 1, NULL, 1, 0, 0, 2, 0),
(289, 1, 5, 66, 538, 116, 1000, 0, 1, 0, 1, '2021-09-01 15:45:40', '2021-09-02 12:00:00', 0, 1, 1, 116, 0, '2021-09-01 15:45:40', 2, NULL, 1, 121, 0, 2, 0),
(290, 1, 4, 67, 539, 115, 1000, 0, 1, 0, 1, '2021-09-01 16:20:42', '2021-09-02 12:00:00', 0, 4, 1, 115, 0, '2021-09-01 16:20:42', 1, NULL, 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_sueldo`
--

CREATE TABLE `proceso_sueldo` (
  `id` int(11) NOT NULL,
  `id_sueldo` int(11) DEFAULT NULL,
  `monto` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `tipo` int(11) NOT NULL DEFAULT 1,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_venta`
--

CREATE TABLE `proceso_venta` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_operacion` int(11) DEFAULT NULL,
  `id_venta` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso_venta`
--

INSERT INTO `proceso_venta` (`id`, `id_producto`, `id_operacion`, `id_venta`, `cantidad`, `precio`, `tipo_operacion`, `fecha_creada`, `id_caja`, `id_usuario`) VALUES
(259, 3, NULL, 185, 1, 500, 1, '2021-08-31 15:12:33', 115, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `presentacion` varchar(255) DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `stock` double NOT NULL DEFAULT 0,
  `id_proveedor` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `codigo`, `nombre`, `marca`, `descripcion`, `presentacion`, `precio_compra`, `precio_venta`, `stock`, `id_proveedor`, `fecha_creada`) VALUES
(3, 'PRO-0003', 'Gaseosa Coca Cola', 'Coca Cola', 'Personal medio litro', 'Coca', 100, 500, 20, 0, '2018-02-16 20:59:18'),
(4, '157919', 'Fresco de Frutas', 'Natura', 'Natura Fresco', 'Natura Fresco Natural', 2000, 2500, 500, 0, '2021-06-04 09:44:42'),
(5, '1579191', 'Yuquitas ', 'Slice', 'Yucas con sal asadas  ', 'Slice Yuquitas', 1100, 1500, 500, 0, '2021-06-04 09:46:31'),
(6, '1012201', 'Tostadas ', 'Slice Tostadas', 'Slice Tostadas asadas ', 'Slice Tostadas', 800, 1000, 500, 0, '2021-06-04 09:47:17'),
(7, '501423', 'Powerade', 'Powerade', 'Fresco energizante ', 'Powerade', 600, 800, 1000, 0, '2021-06-04 09:48:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldo`
--

CREATE TABLE `sueldo` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `monto` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `dia_pago` int(11) NOT NULL DEFAULT 1,
  `fecha_comienzo` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa`
--

CREATE TABLE `tarifa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa`
--

INSERT INTO `tarifa` (`id`, `nombre`) VALUES
(1, '24 Horas'),
(4, '12 horas'),
(7, 'Doble'),
(8, 'Personal'),
(9, 'Triple'),
(10, 'Cuadruple');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa_habitacion`
--

CREATE TABLE `tarifa_habitacion` (
  `id` int(11) NOT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa_habitacion`
--

INSERT INTO `tarifa_habitacion` (`id`, `id_tarifa`, `id_habitacion`, `precio`) VALUES
(17, 8, 6, 380),
(18, 7, 6, 580),
(19, 9, 6, 700),
(20, 10, 6, 800),
(23, 7, 8, 580),
(24, 7, 7, 580),
(25, 7, 9, 580),
(26, 9, 10, 700),
(27, 7, 11, 580),
(28, 8, 12, 380),
(29, 8, 13, 380),
(30, 8, 14, 500),
(31, 7, 14, 600),
(32, 8, 15, 380),
(33, 7, 16, 580),
(34, 7, 17, 580),
(35, 9, 18, 700),
(36, 7, 19, 580),
(37, 8, 20, 380),
(38, 8, 21, 380),
(39, 9, 22, 700),
(40, 8, 23, 380),
(41, 7, 24, 580),
(42, 7, 25, 580),
(43, 9, 26, 700),
(65, 1, 3, 1000),
(66, 1, 5, 1000),
(67, 1, 4, 1000),
(68, 1, 41, 50000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_banco`
--

CREATE TABLE `tipo_banco` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `numero_cuenta` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_banco`
--

INSERT INTO `tipo_banco` (`id`, `nombre`, `numero_cuenta`, `valor`, `fecha_creada`) VALUES
(1, 'Banco de Costa Rica', 11111, 0, '2021-08-03'),
(2, 'COOCIQUE', 12, 0, '2021-08-03'),
(4, 'Gabriel', 123123, 0, '2021-08-09'),
(10, '213', 123, 0, '2021-08-09'),
(11, '123', 1233, 0, '2021-08-09'),
(12, '3233', 32323, 0, '2021-08-09'),
(13, '23323', 2323, 0, '2021-08-09'),
(14, '2332', 323, 0, '2021-08-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `id` int(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor_extra` int(250) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_cliente`
--

INSERT INTO `tipo_cliente` (`id`, `nombre`, `valor_extra`, `fecha_creada`) VALUES
(39, 'Huesped de habitación', 1, '2021-06-09'),
(40, 'Cliente natural', 0, '2021-06-09'),
(41, 'Empleado de Hotel', 1, '2021-06-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_comprobante`
--

CREATE TABLE `tipo_comprobante` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_comprobante`
--

INSERT INTO `tipo_comprobante` (`id`, `nombre`, `estado`) VALUES
(1, 'TICKET', 1),
(2, 'BOLETA', 2),
(3, 'FACTURA', 3),
(7, 'Factura Electrónica ', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id`, `nombre`, `fecha_creada`) VALUES
(2, 'INE', '2018-02-15 09:24:24'),
(71, 'Cedula', '2021-08-03 16:44:55'),
(72, 'Pasaporte', '2021-08-03 16:45:09'),
(73, '1212', '2021-08-13 15:07:29'),
(74, '21212', '2021-08-13 15:07:32'),
(75, '21212', '2021-08-13 15:07:34'),
(76, '1212', '2021-08-13 15:07:36'),
(77, '12121', '2021-08-13 15:07:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `banco` int(11) NOT NULL,
  `numero_aprobacion` int(11) NOT NULL,
  `numero_tarjeta` int(11) NOT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id`, `nombre`, `valor`, `banco`, `numero_aprobacion`, `numero_tarjeta`, `fecha_creada`) VALUES
(1, 'Transferencia Bancaria', 0, 1, 0, 0, '2018-02-15 09:25:24'),
(2, 'Tarjeta de Debito/Credito', 1, 1, 0, 0, '2018-02-15 09:25:24'),
(3, 'Deposito ', 0, 1, 1, 0, '2018-08-22 00:00:00'),
(25, 'SIMPE', 0, 0, 0, 1, '2021-06-22 11:09:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_proceso`
--

CREATE TABLE `tipo_proceso` (
  `id` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_proceso`
--

INSERT INTO `tipo_proceso` (`id`, `id_tipo_cliente`, `nombre`, `valor`, `fecha_creada`) VALUES
(19, 39, 'Cancelado', 1, '2021-06-09'),
(20, 40, 'Cancelado', 1, '2021-06-09'),
(22, 41, 'Cargar a Habitacion', 0, '2021-06-10'),
(23, 39, 'Cargar a Habitacion', 0, '2021-06-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE `tmp` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad_tmp` int(11) DEFAULT NULL,
  `precio_tmp` double DEFAULT NULL,
  `sessionn_id` varchar(255) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `pago` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `created_at`, `pago`) VALUES
(1, 'Administrador', 'admin', 'admin', 'admin@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2016-12-13 09:08:03', 0),
(2, 'DAPOS', '1', 'DAPOS', 'info@d-apos.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2019-02-26 01:23:42', 0),
(3, 'Gabriel', 'Quesada', 'GabrielQAAdmin', '123@123.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2021-06-24 14:34:52', 0),
(4, 'Gabriel', 'Quesada', 'gabrielqa', 'AmbientePruebas', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2021-06-24 14:49:36', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `id_tipo_comprobante` int(11) DEFAULT NULL,
  `nro_comprobante` varchar(25) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `total` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `id_tipo_comprobante`, `nro_comprobante`, `id_proveedor`, `id_tipo_pago`, `tipo_operacion`, `total`, `id_usuario`, `id_caja`, `fecha_creada`) VALUES
(185, 1, '500', 1, 1, 1, 536, 4, 115, '2021-08-31 15:12:33');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_id_caja` (`id_caja`) USING BTREE;

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_cliente` (`id_tipo_cliente`) USING BTREE;

--
-- Indices de la tabla `tmp`
--
ALTER TABLE `tmp`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=280;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `gasto`
--
ALTER TABLE `gasto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=540;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;

--
-- AUTO_INCREMENT de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `tmp`
--
ALTER TABLE `tmp`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=336;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD CONSTRAINT `aperturas_cajas_ibfk_1` FOREIGN KEY (`id_caja`) REFERENCES `mantenimiento_cajas` (`id`);

--
-- Filtros para la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD CONSTRAINT `tipo_proceso_ibfk_1` FOREIGN KEY (`id_tipo_cliente`) REFERENCES `tipo_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
