DROP TABLE IF EXISTS aperturas_cajas;

CREATE TABLE `aperturas_cajas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(11) NOT NULL,
  `fecha_apertura` datetime NOT NULL,
  `fecha_cierre` datetime NOT NULL,
  `monto_apertura` int(11) NOT NULL,
  `monto_cierre` int(11) NOT NULL,
  `estado` varchar(500) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_id_caja` (`id_caja`) USING BTREE,
  CONSTRAINT `aperturas_cajas_ibfk_1` FOREIGN KEY (`id_caja`) REFERENCES `mantenimiento_cajas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8mb4;

INSERT INTO aperturas_cajas VALUES("123","24","2022-01-29 14:45:27","2022-02-02 15:13:33","12","12","0","4","2022-01-29 14:45:31"),
("124","24","2022-02-02 15:15:05","2022-02-03 12:02:16","100000","100000","0","4","2022-02-02 15:15:14"),
("125","26","2022-02-03 12:02:27","2022-02-03 12:40:04","100000","100123","0","1","2022-02-03 12:02:34"),
("126","30","2022-02-03 12:08:59","2022-02-03 12:42:29","200","1200","0","4","2022-02-03 12:09:05"),
("127","25","2022-02-03 12:42:35","2022-02-03 14:00:53","100","26100","0","1","2022-02-03 12:42:41"),
("128","24","2022-02-03 14:15:19","0000-00-00 00:00:00","100","100","1","1","2022-02-03 14:15:25"),
("129","28","2022-02-03 14:20:43","2022-02-03 15:47:20","200","700","0","4","2022-02-03 14:20:48"),
("130","27","2022-02-03 15:53:04","2022-02-03 17:10:51","200","306224","0","10","2022-02-03 15:53:07"),
("131","28","2022-02-04 13:51:10","0000-00-00 00:00:00","200","200","1","4","2022-02-04 13:51:14");



DROP TABLE IF EXISTS auto_cabys;

CREATE TABLE `auto_cabys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` longtext NOT NULL,
  `cabys` longtext NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `fecha_act` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

INSERT INTO auto_cabys VALUES("32","descripcion","1201000000100","1","2022-02-11","2022-02-11 09:01:56"),
("33","descripcion","2391400009900","0","2022-02-11","2022-02-11 09:06:18"),
("34","descripcion","4141102000000","1","2022-02-11","2022-02-11 09:54:03");



DROP TABLE IF EXISTS bodegas;

CREATE TABLE `bodegas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `cantidad_inicial` int(11) NOT NULL,
  `cantidad_maxima` int(11) NOT NULL,
  `cantidad_minima` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

INSERT INTO bodegas VALUES("6","001","Bodega","100","400","50","1","2021-11-03"),
("7","002","Bodega #2","100","1000","50","1","2021-11-03"),
("8","003","Bodega #3","100","400","100","1","2021-11-03"),
("9","004","Bodega #4","100","5000","100","1","2021-11-03"),
("10","005","Bodega #5","100","1000","100","1","2021-11-03"),
("11","006","Bodega #6","100","900","20","1","2021-11-03");



DROP TABLE IF EXISTS caja;

CREATE TABLE `caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_apertura` datetime DEFAULT NULL,
  `fecha_cierre` datetime DEFAULT NULL,
  `monto_apertura` double DEFAULT NULL,
  `monto_cierre` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS categoria;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO categoria VALUES("1","Personal","CASA-GOAS_30_INDIVIDUAL_1.jpg","2018-02-15 09:14:21"),
("2","Doble","1892_rec.jpg","2019-02-26 11:39:02"),
("3","Triple","triple.jpg","2019-02-26 11:39:08"),
("4","Cuadruple","cuadruple_DSC2747_resize-1024x683.jpg","2019-02-26 11:40:17"),
("5","Especial","habitacion-grande-1.jpg","2019-02-26 11:40:26");



DROP TABLE IF EXISTS categoria_clientes_p;

CREATE TABLE `categoria_clientes_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

INSERT INTO categoria_clientes_p VALUES("2","Bronce","1","2021-12-10"),
("3","Plata","1","2021-12-10"),
("4","Oro","0","2021-12-11"),
("5","Platino","1","2021-12-11"),
("7","Diamante","1","2021-12-11");



DROP TABLE IF EXISTS categoria_venta;

CREATE TABLE `categoria_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `id_tipo_categoria` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tipo_categoria` (`id_tipo_categoria`),
  CONSTRAINT `fk_tipo_categoria` FOREIGN KEY (`id_tipo_categoria`) REFERENCES `tipo_categoria_venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;




DROP TABLE IF EXISTS cliente_proceso;

CREATE TABLE `cliente_proceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `id_proceso` int(11) DEFAULT NULL,
  `sesion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=latin1;

INSERT INTO cliente_proceso VALUES("302","768","318","4hvdrr5r5uanipfq6ufppjsm16"),
("303","769","319","4hvdrr5r5uanipfq6ufppjsm16"),
("304","770","320","4hvdrr5r5uanipfq6ufppjsm16"),
("305","771","321","4hvdrr5r5uanipfq6ufppjsm16"),
("306","772","322","4hvdrr5r5uanipfq6ufppjsm16"),
("307","773","323","4hvdrr5r5uanipfq6ufppjsm16"),
("308","774","324","4hvdrr5r5uanipfq6ufppjsm16"),
("309","775","325","4hvdrr5r5uanipfq6ufppjsm16"),
("310","776","326","4hvdrr5r5uanipfq6ufppjsm16"),
("311","777","327","4hvdrr5r5uanipfq6ufppjsm16"),
("312","778","328","4hvdrr5r5uanipfq6ufppjsm16"),
("313","779","329","4hvdrr5r5uanipfq6ufppjsm16"),
("314","780","330","4hvdrr5r5uanipfq6ufppjsm16"),
("315","781","331","4hvdrr5r5uanipfq6ufppjsm16"),
("316","782","332","4hvdrr5r5uanipfq6ufppjsm16"),
("317","783","333","4hvdrr5r5uanipfq6ufppjsm16"),
("318","784","334","4hvdrr5r5uanipfq6ufppjsm16"),
("319","785","335","4hvdrr5r5uanipfq6ufppjsm16"),
("320","786","336","4hvdrr5r5uanipfq6ufppjsm16"),
("321","787","337","4hvdrr5r5uanipfq6ufppjsm16"),
("322","788","338","4hvdrr5r5uanipfq6ufppjsm16"),
("323","789","339","4hvdrr5r5uanipfq6ufppjsm16"),
("324","790","340","4hvdrr5r5uanipfq6ufppjsm16"),
("325","791","341","4dkbmrbnou42i3h2ufillm1srn"),
("326","792","342","4dkbmrbnou42i3h2ufillm1srn"),
("327","793","343","4dkbmrbnou42i3h2ufillm1srn"),
("328","794","344","4dkbmrbnou42i3h2ufillm1srn"),
("329","795","345","4dkbmrbnou42i3h2ufillm1srn"),
("330","796","346","4dkbmrbnou42i3h2ufillm1srn"),
("331","799","347","4dkbmrbnou42i3h2ufillm1srn"),
("332","800","348","4dkbmrbnou42i3h2ufillm1srn"),
("333","801","349","4dkbmrbnou42i3h2ufillm1srn"),
("334","802","350","4dkbmrbnou42i3h2ufillm1srn"),
("335","803","351","4dkbmrbnou42i3h2ufillm1srn"),
("336","804","352","4dkbmrbnou42i3h2ufillm1srn"),
("337","805","353","4dkbmrbnou42i3h2ufillm1srn"),
("338","806","354","4dkbmrbnou42i3h2ufillm1srn"),
("339","807","355","4dkbmrbnou42i3h2ufillm1srn"),
("340","812","356","4dkbmrbnou42i3h2ufillm1srn"),
("341","814","357","4dkbmrbnou42i3h2ufillm1srn"),
("342","815","358","4dkbmrbnou42i3h2ufillm1srn"),
("343","816","359","4dkbmrbnou42i3h2ufillm1srn"),
("344","818","360","4dkbmrbnou42i3h2ufillm1srn"),
("345","821","361","4dkbmrbnou42i3h2ufillm1srn"),
("346","822","362","4dkbmrbnou42i3h2ufillm1srn"),
("347","828","363","4dkbmrbnou42i3h2ufillm1srn"),
("348","830","364","4dkbmrbnou42i3h2ufillm1srn"),
("349","832","365","4dkbmrbnou42i3h2ufillm1srn"),
("350","833","366","4dkbmrbnou42i3h2ufillm1srn"),
("351","834","367","4dkbmrbnou42i3h2ufillm1srn"),
("352","835","368","4dkbmrbnou42i3h2ufillm1srn"),
("353","836","369","4dkbmrbnou42i3h2ufillm1srn"),
("354","837","370","4dkbmrbnou42i3h2ufillm1srn"),
("355","838","371","4dkbmrbnou42i3h2ufillm1srn"),
("356","841","372","4dkbmrbnou42i3h2ufillm1srn"),
("357","842","373","4dkbmrbnou42i3h2ufillm1srn"),
("358","843","374","4dkbmrbnou42i3h2ufillm1srn"),
("359","844","375","4dkbmrbnou42i3h2ufillm1srn"),
("360","845","376","4dkbmrbnou42i3h2ufillm1srn"),
("361","847","377","4dkbmrbnou42i3h2ufillm1srn"),
("362","848","378","4dkbmrbnou42i3h2ufillm1srn"),
("363","851","379","4dkbmrbnou42i3h2ufillm1srn"),
("364","855","380","4dkbmrbnou42i3h2ufillm1srn"),
("365","857","381","4dkbmrbnou42i3h2ufillm1srn"),
("366","858","382","4dkbmrbnou42i3h2ufillm1srn"),
("367","760","395","4dkbmrbnou42i3h2ufillm1srn"),
("368","760","396","4dkbmrbnou42i3h2ufillm1srn"),
("369","859","397","4dkbmrbnou42i3h2ufillm1srn"),
("370","859","398","4dkbmrbnou42i3h2ufillm1srn"),
("371","760","399","4dkbmrbnou42i3h2ufillm1srn"),
("372","760","400","4dkbmrbnou42i3h2ufillm1srn"),
("373","764","401","4dkbmrbnou42i3h2ufillm1srn"),
("374","764","402","4dkbmrbnou42i3h2ufillm1srn"),
("375","860","403","4dkbmrbnou42i3h2ufillm1srn"),
("376","860","404","4dkbmrbnou42i3h2ufillm1srn"),
("377","760","405","4dkbmrbnou42i3h2ufillm1srn"),
("378","760","406","4dkbmrbnou42i3h2ufillm1srn"),
("379","861","407","4dkbmrbnou42i3h2ufillm1srn"),
("380","862","408","4dkbmrbnou42i3h2ufillm1srn"),
("381","862","409","4dkbmrbnou42i3h2ufillm1srn"),
("382","863","410","4dkbmrbnou42i3h2ufillm1srn"),
("383","863","411","4dkbmrbnou42i3h2ufillm1srn"),
("384","760","412","f3ccs7tr0lrn9p84ct8qv85pe6"),
("385","864","413","7om9i6utbu34l07vpcd9arvnlb"),
("386","865","414","e9uungkrbhfnmbcimjfi52af2p"),
("387","866","415","e9uungkrbhfnmbcimjfi52af2p"),
("388","867","416","e9uungkrbhfnmbcimjfi52af2p"),
("389","868","417","b0al4v6794bvc4c60rj6kjhr5o"),
("390","869","418","b0al4v6794bvc4c60rj6kjhr5o");



DROP TABLE IF EXISTS configuracion;

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `rnc` varchar(25) DEFAULT NULL,
  `registro_empresarial` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `usuario_hacienda` varchar(50) NOT NULL,
  `contrasena_hacienda` varchar(50) NOT NULL,
  `llave_criptografica` varchar(50) NOT NULL,
  `token` varchar(600) NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO configuracion VALUES("1","Alcesac","Costa Rica, Cartago, Rio Cuarto.","Cartago","+50661749563","NULL","0038947384786","NULL","Tres Rios","Alcesac.jpeg","Gabriel","123","file.p12","150cdcae104c97b617c57addb2ad2dc8","2022-02-08");



DROP TABLE IF EXISTS contacto;

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documento` varchar(12) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_persona` int(11) DEFAULT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS descuento_productos;

CREATE TABLE `descuento_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

INSERT INTO descuento_productos VALUES("1","Descuento 12%","12","1","2021-10-27"),
("2","Descuento 15%","15","1","2021-10-27"),
("3","Descuento 20%","20","1","2021-10-27"),
("4","Descuento 5%","5","1","2021-10-27"),
("5","Sin Descuento","0","1","2021-12-03");



DROP TABLE IF EXISTS estado_cajas;

CREATE TABLE `estado_cajas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

INSERT INTO estado_cajas VALUES("3","Ocupada\n\n","0","2"),
("4","Disponible","1","2"),
("5","Mantenimiento","3","2"),
("6","Deshabilitada","4","2");



DROP TABLE IF EXISTS estado_de_pago;

CREATE TABLE `estado_de_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `medio_pago` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

INSERT INTO estado_de_pago VALUES("2","Pago realizado","1","2021-05-14"),
("8","En proceso","0","2021-06-05");



DROP TABLE IF EXISTS funcionalidades;

CREATE TABLE `funcionalidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(110) NOT NULL,
  `estado` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO funcionalidades VALUES("1","Notificacio Email","1"),
("2","Notificaciones SMS","0"),
("3","Promociones","1");



DROP TABLE IF EXISTS gasto;

CREATE TABLE `gasto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS habitacion;

CREATE TABLE `habitacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `capacidad` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

INSERT INTO habitacion VALUES("52","Habitacion 1","123","0","2","2","1","2021-12-04 16:23:49"),
("53","Habitacion 2","123","0","1","2","1","2021-12-07 09:09:45"),
("54","Habitacion 3","21","0","2","1","1","2021-12-07 09:10:26"),
("55","Habitacion 4","q","0","3","4","1","2021-12-07 09:10:36"),
("56","Habitacion 5","4","0","4","1","1","2021-12-07 09:10:47"),
("57","Habitacion 6","5","0","5","1","1","2021-12-07 09:10:57"),
("58","Habitacion 7","12","0","4","1","1","2021-12-07 09:11:05"),
("59","Habitacion 8","21","0","5","2","1","2021-12-07 09:11:14"),
("60","Habitacion 9","2","0","2","1","1","2021-12-07 09:11:21"),
("61","Habitacion 10","231","0","5","2","1","2021-12-07 09:11:29"),
("62","Habitacion 8","nd","0","1","1","1","2022-02-03 12:36:00"),
("63","Habitacion 9","nd","0","1","1","1","2022-02-03 12:36:10"),
("64","Habitacion 10","nd","0","1","1","1","2022-02-03 12:36:19"),
("65","Habitacion 11","nd","0","1","2","1","2022-02-03 12:38:20"),
("66","Habitacion 12","nd","0","1","1","1","2022-02-03 12:38:27"),
("67","Habitacion 13","nd","0","1","1","1","2022-02-03 12:50:57"),
("68","Habitacion 14","nd","0","1","2","1","2022-02-03 12:51:03"),
("69","Habitacion 15","nd","0","1","1","1","2022-02-03 12:51:12"),
("70","Habitacion 16","nd","0","1","1","1","2022-02-03 12:51:19"),
("71","Habitacion 17","nd","0","1","1","1","2022-02-03 12:51:26"),
("72","Habitacion 18","nd","0","1","2","1","2022-02-03 12:51:33"),
("73","Habitacion 19","nd","0","1","1","1","2022-02-03 12:51:39"),
("74","Habitacion 20","nd","0","1","1","1","2022-02-03 12:51:46"),
("75","Habitacion 21","nd","0","1","1","1","2022-02-03 12:52:36");



DROP TABLE IF EXISTS impuestos;

CREATE TABLE `impuestos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `compras` int(11) NOT NULL,
  `egresos` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

INSERT INTO impuestos VALUES("1","IVA","0","1","0","0000-00-00 00:00:00"),
("3","Impuesto sobre la venta 13%","1","1","0","0000-00-00 00:00:00"),
("4","Impuesto sobre la venta 8%","0","0","0","0000-00-00 00:00:00"),
("5","Impuesto sobre la venta 4%","0","0","0","0000-00-00 00:00:00"),
("6","Impuesto sobre la venta 2%","0","0","0","0000-00-00 00:00:00");



DROP TABLE IF EXISTS impuestos_productos;

CREATE TABLE `impuestos_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `descripcion` text NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_tipo_impuesto` int(11) NOT NULL,
  `tipo_impuesto` varchar(30) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

INSERT INTO impuestos_productos VALUES("17","13","13%","TEST","1","6","asd","2021-10-25"),
("18","15","15%","TEST","1","6","asd","2021-10-25"),
("19","10","10%","TEST","1","6","dsa","2021-10-26"),
("20","8","8%","TEST","1","7","asda","2021-10-28");



DROP TABLE IF EXISTS inventario;

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS mantenimiento_cajas;

CREATE TABLE `mantenimiento_cajas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

INSERT INTO mantenimiento_cajas VALUES("24","1","2","2021-06-29"),
("25","2","1","2021-06-29"),
("26","3","1","2021-06-29"),
("27","4","1","2021-06-29"),
("28","5","2","2021-07-03"),
("30","6","1","2021-07-23");



DROP TABLE IF EXISTS notificaciones;

CREATE TABLE `notificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

INSERT INTO notificaciones VALUES("8","gabrielqqquesada@gmail.com","0","0","2021-09-11");



DROP TABLE IF EXISTS notificaciones_stocks;

CREATE TABLE `notificaciones_stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO notificaciones_stocks VALUES("1","10","1","2021-09-14"),
("2","15","0","2021-09-14"),
("3","5","1","2021-09-14");



DROP TABLE IF EXISTS periodos_pago;

CREATE TABLE `periodos_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `dias` int(250) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

INSERT INTO periodos_pago VALUES("2","Contado","0","2021-06-05"),
("3","Limite de fecha","0","2021-06-09"),
("4","Por pagos","0","2021-06-09");



DROP TABLE IF EXISTS persona;

CREATE TABLE `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_documento` int(11) DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `razon_social` varchar(150) DEFAULT NULL,
  `provincia` int(11) NOT NULL,
  `canton` int(11) NOT NULL,
  `distrito` int(11) NOT NULL,
  `barrio` int(11) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  `tipo` int(11) DEFAULT 1,
  `vip` int(11) NOT NULL DEFAULT 0,
  `contador` int(11) NOT NULL DEFAULT 0,
  `limite` int(11) NOT NULL DEFAULT 7,
  `nacionalidad` varchar(25) DEFAULT NULL,
  `estado_civil` varchar(12) DEFAULT NULL,
  `ocupacion` varchar(255) DEFAULT NULL,
  `medio_transporte` varchar(65) DEFAULT NULL,
  `destino` varchar(55) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `telefono_sec` int(11) NOT NULL,
  `celular` varchar(25) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `exonerado` int(11) DEFAULT NULL,
  `id_categoria_p` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria_promo` (`id_categoria_p`)
) ENGINE=InnoDB AUTO_INCREMENT=881 DEFAULT CHARSET=latin1;

INSERT INTO persona VALUES("760","2","207910714","","Gabriel Quesada","","","0","0","0","0","SRM","2022-02-02 14:09:51","1","0","0","7","","","","","","","","0","","","",""),
("761","2","207910715","","Hernando Alonso","","","0","0","0","0","SRM","2022-02-02 14:33:45","1","0","0","7","","","","","","","","0","","","",""),
("762","2","207910451","","Maria Jose Carvajal ","","","0","0","0","0","CR, SRM, AI","2022-02-02 14:45:21","1","0","0","7","","","","","","","","0","","","",""),
("763","2","207910713","","Rodrigo Alejandro Ramirez","","","0","0","0","0","SR","2022-02-02 14:46:34","1","0","0","7","","","","","","","","0","","","",""),
("764","2","207910710","","Daniela Alvarado","","","0","0","0","0","ND","2022-02-02 14:52:10","1","0","0","7","","","","","","","","0","","","",""),
("765","2","207910716","","Francela Arguellas","","","0","0","0","0","ND","2022-02-02 14:55:26","1","0","0","7","","","","","","","","0","","","",""),
("766","2","207910719","","Antonio Francisco Gonzales ","","","0","0","0","0","ND","2022-02-02 15:00:38","1","0","0","7","","","","","","","","0","","","",""),
("864","2","207910718","1","LUISETH FERNANDA BARRANTES ALVARADO","","1","0","0","0","0","1","2022-02-03 12:21:58","1","0","0","7","","","1","1","1","1","1","0","","usuariodapos@gmail.com","0","0"),
("865","71","207910452","1","MARIA AUXILIADORA CASTRO SALAS","","1","0","0","0","0","CR, SRM, AI","2022-02-03 13:56:59","1","0","0","7","","","1","1","1","1","1","0","","gabrielquesadajgqa@gmail.com","0","0"),
("866","2","207910215","1","KRISTEL VANESSA CHACON ESPINO","","1","0","0","0","0","1","2022-02-03 13:59:06","1","0","0","7","","","1","1","1","1","1","0","","gabrielquesadajgqa@gmail.com","0","2"),
("867","2","207910722","1","MARIA JOSE ALFARO RODRIGUEZ","","1","0","0","0","0","1","2022-02-03 14:00:48","1","0","0","7","","","1","1","1","1","1","0","","gabrielquesadajgqa@gmail.com","0","5"),
("868","2","207910478","1","KATHERINE YESENIA GARCIA MEJIA","","1","0","0","0","0","1","2022-02-03 15:45:33","1","0","0","7","","","1","1","1","1","1","0","","usuariodapos@gmail.com","0","0"),
("869","2","207910818","1","ANTHONY PINEDA SANCHEZ","","1","0","0","0","0","1","2022-02-03 15:46:57","1","0","0","7","","","1","1","1","1","1","0","","usuariodapos@gmail.com","0","0"),
("870","2","207910788","","Hernan","","","0","0","0","0","SDR","2022-02-03 16:24:32","1","0","0","7","","","","","","","","0","","","",""),
("871","2","207910744","","Jessica","","","0","0","0","0","SR","2022-02-03 16:25:55","1","0","0","7","","","","","","","","0","","","",""),
("872","2","207810718","","Esteban","","","0","0","0","0","1","2022-02-03 16:31:59","1","0","0","7","","","","","","","","0","","","",""),
("873","2","207918118","","Stiven","","","0","0","0","0","1","2022-02-03 16:33:09","1","0","0","7","","","","","","","","0","","","",""),
("877","2","207910111","","Hernan","","","0","0","0","0","SDR","2022-02-03 16:43:07","1","0","0","7","","","","","","","","0","","","",""),
("878","2","255887411","","Joel","","","0","0","0","0","1","2022-02-03 16:44:11","1","0","0","7","","","","","","","","0","","","",""),
("879","71","207910714","","Gabriel Quesada","","","0","0","0","0","","2022-02-03 17:03:45","3","0","0","7","","","","","","","","0","","gabrielquesada1999@gmail.com","0","0"),
("880","71","207910714","","Gabriel Quesada","","","0","0","0","0","","2022-02-03 17:10:15","3","0","0","7","","","","","","","","0","","usuariodapos@gmail.com","0","2");



DROP TABLE IF EXISTS proceso;

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_proceso` int(11) NOT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_apertura` int(11) NOT NULL,
  `precio` double NOT NULL DEFAULT 0,
  `cobro_ext` int(11) NOT NULL,
  `cant_noche` float NOT NULL DEFAULT 1,
  `dinero_dejado` double NOT NULL DEFAULT 0,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `fecha_entrada` datetime DEFAULT NULL,
  `fecha_salida` datetime DEFAULT NULL,
  `total` double NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `cant_personas` double DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `fecha_creada` datetime DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT 1,
  `observacion` varchar(255) DEFAULT NULL,
  `pagado` int(11) DEFAULT NULL,
  `nro_operacion` int(25) DEFAULT NULL,
  `num_tarjeta` int(11) DEFAULT NULL,
  `banco` int(11) DEFAULT NULL,
  `num_aprobacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=432 DEFAULT CHARSET=latin1;

INSERT INTO proceso VALUES("303","0","52","","760","0","0","0","1","0","1","2022-02-05 10:00:00","2022-02-18 15:10:00","0","4","1","","3","2022-02-02 14:09:51","1","ND","","","","",""),
("304","0","53","","761","0","0","0","1","0","1","2022-02-07 01:28:00","2022-02-08 15:28:00","0","4","1","","3","2022-02-02 14:33:45","1","ND","","","","",""),
("305","0","59","","762","0","0","0","1","0","1","2022-02-11 15:44:00","2022-02-13 15:44:00","0","4","1","","3","2022-02-02 14:45:21","1","ND","","","","",""),
("306","0","55","","763","0","0","0","1","0","1","2022-02-11 00:00:00","2022-02-17 00:00:00","0","4","1","","3","2022-02-02 14:46:34","1","ND","","","","",""),
("307","0","57","","764","0","0","0","1","0","1","2022-02-08 00:00:00","2022-02-15 00:00:00","0","4","1","","3","2022-02-02 14:52:10","1","ND","","","","",""),
("308","0","61","","765","0","0","0","1","0","1","2022-02-06 15:55:00","2022-02-07 15:55:00","0","4","1","","3","2022-02-02 14:55:26","1","ND","","","","",""),
("309","0","60","","766","0","0","0","1","0","1","2022-02-08 00:00:00","2022-02-09 00:00:00","0","4","1","","3","2022-02-02 15:00:38","1","ND","","","","",""),
("412","1","52","69","760","125","123","0","1","0","1","2022-02-03 12:11:30","2022-02-04 12:00:00","0","1","1","125","0","2022-02-03 12:11:30","1","","1","0","0","1","0"),
("413","1","53","70","864","126","1000","0","1","0","1","2022-02-03 12:21:58","2022-02-04 12:00:00","0","4","1","126","0","2022-02-03 12:21:58","1","","1","0","0","1","0"),
("414","1","61","78","865","127","15000","0","1","0","1","2022-02-03 13:56:59","2022-02-04 12:00:00","0","1","1","127","0","2022-02-03 13:56:59","1","","1","0","0","1","0"),
("415","1","72","79","866","127","1000","0","1","0","1","2022-02-03 13:59:06","2022-02-04 12:00:00","0","1","1","127","0","2022-02-03 13:59:06","1","","1","0","0","1","0"),
("416","1","65","80","867","127","10000","0","1","0","1","2022-02-03 14:00:48","2022-02-04 12:00:00","0","1","1","127","0","2022-02-03 14:00:48","1","","1","0","0","1","0"),
("417","1","68","82","868","129","500","0","1","0","2","2022-02-03 15:45:33","2022-02-04 12:00:00","0","4","1","129","0","2022-02-03 15:45:33","1","","1","2010309123","0","4","0"),
("418","0","59","76","869","129","30000","0","1","0","0","2022-02-03 15:46:57","2022-02-04 12:00:00","0","4","1","129","0","2022-02-03 15:46:57","1","","0","0","0","0","0"),
("419","0","63","","870","0","0","0","1","0","1","2022-02-13 00:00:00","2022-02-22 00:00:00","0","10","1","","3","2022-02-03 16:24:32","1","ND","","","","",""),
("420","0","64","","871","0","0","0","1","0","1","2022-02-11 17:25:00","2022-02-12 17:26:00","0","10","1","","3","2022-02-03 16:25:55","1","ND","","","","",""),
("423","0","70","","872","0","0","0","1","0","1","2022-02-13 00:00:00","2022-02-21 00:00:00","0","10","1","","3","2022-02-03 16:31:59","1","1","","","","",""),
("424","0","69","","873","0","0","0","1","0","1","2022-02-11 00:00:00","2022-02-23 00:00:00","0","10","1","","3","2022-02-03 16:33:09","1","1","","","","",""),
("425","0","66","","872","0","0","0","1","0","1","2022-02-09 00:00:00","2022-02-18 00:00:00","0","10","1","","3","2022-02-03 16:33:48","1","1","","","","",""),
("429","0","54","","871","0","0","0","1","0","1","2022-02-17 00:00:00","2022-02-24 00:00:00","0","10","1","","3","2022-02-03 16:42:04","1","1","","","","",""),
("430","0","58","","877","0","0","0","1","0","1","2022-02-24 00:00:00","2022-02-25 00:00:00","0","10","1","","3","2022-02-03 16:43:07","1","ND","","","","",""),
("431","0","62","","878","0","0","0","1","0","1","2022-02-09 00:00:00","2022-02-11 00:00:00","0","10","1","","3","2022-02-03 16:44:11","1","1","","","","","");



DROP TABLE IF EXISTS proceso_sueldo;

CREATE TABLE `proceso_sueldo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_sueldo` int(11) DEFAULT NULL,
  `monto` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `tipo` int(11) NOT NULL DEFAULT 1,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS proceso_venta;

CREATE TABLE `proceso_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) DEFAULT NULL,
  `id_operacion` int(11) DEFAULT NULL,
  `id_venta` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=468 DEFAULT CHARSET=latin1;

INSERT INTO proceso_venta VALUES("463","212","","379","2","5","2","2022-02-03 16:46:11","130","10"),
("464","214","","379","1","123","2","2022-02-03 16:46:11","130","10"),
("465","493","","380","1","1000","2","2022-02-03 16:54:03","130","10"),
("466","212","","381","1","150.35328","1","2022-02-03 17:03:46","130","10"),
("467","504","","382","50","6140.1375","1","2022-02-03 17:10:16","130","10");



DROP TABLE IF EXISTS producto;

CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) DEFAULT NULL,
  `codigo_barra` varchar(50) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `acreditable` tinyint(1) NOT NULL DEFAULT 0,
  `presentacion` varchar(255) DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `stock` double NOT NULL DEFAULT 0,
  `aviso_stock` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `utilidad` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_bodega` int(11) NOT NULL,
  `id_unidad_medida` int(11) NOT NULL,
  `id_descuento` int(11) DEFAULT 0,
  `imagen` varchar(500) NOT NULL,
  `count_mail` int(11) NOT NULL,
  `estado` double NOT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bodega` (`id_bodega`),
  KEY `FK_unidad_de_medida` (`id_unidad_medida`),
  KEY `FK_descuento` (`id_descuento`) USING BTREE,
  KEY `fk_proveedor` (`id_proveedor`),
  CONSTRAINT `fk_bodega` FOREIGN KEY (`id_bodega`) REFERENCES `bodegas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_descuentos_as` FOREIGN KEY (`id_descuento`) REFERENCES `descuento_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_unidad_de_medida` FOREIGN KEY (`id_unidad_medida`) REFERENCES `unidad_de_medida` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=505 DEFAULT CHARSET=latin1;

INSERT INTO producto VALUES("212","20213","20213","Ranchitas","RA","100","1","Natura Fresco Natural","5","100","100","10","105","40","0","7","1","1","img/img_productos/Yummies-Snack-Ranchita-Queso-250-Gr-1-50299.jpg","0","0","2021-11-11 11:45:57"),
("214","20213","20213","Picaronas","TEST","123","1","TEST","123","1000","123","123","1000","32","0","7","1","1","img/img_productos/6529-product-60aea52e3ac4c-150078-1.png","0","0","2021-11-11 11:47:38"),
("215","20213","20213","Nutella","TEST","TEST","0","Nutella","313","5000","400","40","5000","15","0","10","1","4","img/img_productos/81JnoKs70IL._SX425_.jpg","0","0","2021-11-22 16:34:21"),
("493","C013","C013","Fanta","CRD2","TEST","1","Fanta","1000","2000","100","14","200","1","0","8","5","5","img/img_productos/descarga.jpg","0","0","2022-02-03 14:16:03"),
("494","C014","C014","Papiolas MD","Papiolas MD","TEST","1","Papiolas MD","1000","3000","100","15","200","8","0","7","1","4","img/img_productos/2417319.jpg","0","0","2022-02-03 14:25:21"),
("495","C016","C016","Papiolas","Papiolas","TEST","1","Papiolas","1200","4000","100","15","200","12","0","8","1","4","img/img_productos/6529-product-60aea52e3ac4c-150078-1.png","0","0","2022-02-03 14:28:36"),
("496","01923","01923","Sandia","Sandia","nd","1","Sandia","1000","3000","100","15","500","10","0","6","1","4","img/img_productos/sandia.jpg","0","0","2022-02-03 15:02:37"),
("497","021784","021784","Melon","Melon","nd","1","Melon","2000","5200","100","15","200","6","0","11","1","4","img/img_productos/melon-mediano.png","0","0","2022-02-03 15:03:37"),
("498","20213","20213","Papaya","TEST","100","1","Papaya","5","100","90","10","150","10","0","7","1","5","img/img_productos/melon-mediano.png","0","0","2022-02-03 15:10:55"),
("503","20313","20313","Peperoni","Peperoni","ND","1","Peperoni","500","1000","100","10","250","10","0","7","1","5","0","0","0","2022-02-03 15:30:29"),
("504","22813","22813","Chicharrones","Chicharrones","ND","1","Chicharrones","3500","4500","100","10","1000","5","0","7","1","5","0","0","0","2022-02-03 15:30:29");



DROP TABLE IF EXISTS promociones;

CREATE TABLE `promociones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` int(11) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

INSERT INTO promociones VALUES("48","100","Promocion 1","1","2021-12-14");



DROP TABLE IF EXISTS r_impuestos_productos;

CREATE TABLE `r_impuestos_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_impuesto` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_impuesto` (`id_impuesto`),
  KEY `FK_producto` (`id_producto`) USING BTREE,
  CONSTRAINT `fk_impuesto` FOREIGN KEY (`id_impuesto`) REFERENCES `impuestos_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=723 DEFAULT CHARSET=utf8mb4;

INSERT INTO r_impuestos_productos VALUES("705","20","212","2022-02-03"),
("706","18","214","2022-02-03"),
("707","20","493","2022-02-03"),
("708","17","215","2022-02-03"),
("709","20","215","2022-02-03"),
("710","17","494","2022-02-03"),
("711","19","495","2022-02-03"),
("712","20","495","2022-02-03"),
("713","19","496","2022-02-03"),
("714","20","497","2022-02-03"),
("716","18","498","2022-02-03"),
("721","18","503","2022-02-03"),
("722","18","504","2022-02-03");



DROP TABLE IF EXISTS r_plazo_venta;

CREATE TABLE `r_plazo_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) NOT NULL,
  `id_plazo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  `fecha_act` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_venta` (`id_venta`),
  KEY `id_plazo` (`id_plazo`),
  CONSTRAINT `plazo` FOREIGN KEY (`id_plazo`) REFERENCES `periodos_pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

INSERT INTO r_plazo_venta VALUES("8","381","2","0","2022-02-03","2022-02-03"),
("9","382","2","0","2022-02-03","2022-02-03");



DROP TABLE IF EXISTS r_promociones;

CREATE TABLE `r_promociones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT 0,
  `id_categoria` int(11) NOT NULL,
  `id_porcentaje` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `r_producto` (`id_producto`) USING BTREE,
  KEY `r_habitacion` (`id_habitacion`),
  KEY `r_categoria` (`id_categoria`),
  KEY `r_porcentaje` (`id_porcentaje`),
  CONSTRAINT `categoria_promo` FOREIGN KEY (`id_categoria`) REFERENCES `categoria_clientes_p` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `promociones` FOREIGN KEY (`id_porcentaje`) REFERENCES `promociones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4;

INSERT INTO r_promociones VALUES("69","0","55","5","48"),
("70","0","57","5","48"),
("71","0","60","5","48"),
("72","214","0","5","48");



DROP TABLE IF EXISTS r_utilidades_productos;

CREATE TABLE `r_utilidades_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilidad` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_utilidad` (`id_utilidad`),
  KEY `FK_producto` (`id_producto`),
  CONSTRAINT `FK_producto_utilidad` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_utilidad_producto` FOREIGN KEY (`id_utilidad`) REFERENCES `utilidad_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;




DROP TABLE IF EXISTS reservations;

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS rooms;

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS subimpuestos_productos;

CREATE TABLE `subimpuestos_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

INSERT INTO subimpuestos_productos VALUES("6","Cruz roja","2","1","2021-10-25"),
("7","IVA","13","1","2021-10-26");



DROP TABLE IF EXISTS sueldo;

CREATE TABLE `sueldo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `monto` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `dia_pago` int(11) NOT NULL DEFAULT 1,
  `fecha_comienzo` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS tarifa;

CREATE TABLE `tarifa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO tarifa VALUES("1","24 Horas"),
("4","12 horas"),
("7","Doble"),
("8","Personal"),
("9","Triple"),
("10","Cuadruple");



DROP TABLE IF EXISTS tarifa_habitacion;

CREATE TABLE `tarifa_habitacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

INSERT INTO tarifa_habitacion VALUES("17","8","6","380"),
("18","7","6","580"),
("19","9","6","700"),
("20","10","6","800"),
("23","7","8","580"),
("24","7","7","580"),
("25","7","9","580"),
("26","9","10","700"),
("27","7","11","580"),
("28","8","12","380"),
("29","8","13","380"),
("30","8","14","500"),
("31","7","14","600"),
("32","8","15","380"),
("33","7","16","580"),
("34","7","17","580"),
("35","9","18","700"),
("36","7","19","580"),
("37","8","20","380"),
("38","8","21","380"),
("39","9","22","700"),
("40","8","23","380"),
("41","7","24","580"),
("42","7","25","580"),
("43","9","26","700"),
("65","1","3","1000"),
("66","1","5","1000"),
("67","1","4","1000"),
("68","1","41","50000"),
("69","4","52","123"),
("70","1","53","1000"),
("71","4","54","12000"),
("72","8","55","5000"),
("73","7","56","10000"),
("74","10","57","40000"),
("75","1","58","40000"),
("76","9","59","30000"),
("77","1","60","15000"),
("78","9","61","15000"),
("79","4","72","1000"),
("80","8","65","10000"),
("81","9","65","1000"),
("82","8","68","500");



DROP TABLE IF EXISTS tipo_banco;

CREATE TABLE `tipo_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `numero_cuenta` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

INSERT INTO tipo_banco VALUES("1","Banco Costa Rica","102","0","2021-08-03"),
("2","Coocique","103","0","2021-08-03"),
("4","Bac San Jose","104","0","2021-08-09"),
("15","BP","0","0","2021-12-10");



DROP TABLE IF EXISTS tipo_categoria_venta;

CREATE TABLE `tipo_categoria_venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

INSERT INTO tipo_categoria_venta VALUES("8","123","123","1","2021-11-24");



DROP TABLE IF EXISTS tipo_cliente;

CREATE TABLE `tipo_cliente` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `valor_extra` int(250) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4;

INSERT INTO tipo_cliente VALUES("39","Huesped de habitación","1","0","2021-06-09"),
("40","Cliente Habitual","0","0","2021-06-09"),
("51","Cliente Existente","2","0","2021-11-26");



DROP TABLE IF EXISTS tipo_comprobante;

CREATE TABLE `tipo_comprobante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO tipo_comprobante VALUES("1","Ticket","1"),
("2","Boleta","2"),
("3","Factura Fisica","3"),
("7","Factura Electrónica ","3");



DROP TABLE IF EXISTS tipo_documento;

CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

INSERT INTO tipo_documento VALUES("2","NITE","2018-02-15 09:24:24"),
("71","Cedula","2021-08-03 16:44:55"),
("72","Pasaporte","2021-08-03 16:45:09"),
("79","DIMEX","2021-11-09 10:23:52");



DROP TABLE IF EXISTS tipo_pago;

CREATE TABLE `tipo_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `banco` int(11) NOT NULL,
  `numero_aprobacion` int(11) NOT NULL,
  `numero_tarjeta` int(11) NOT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

INSERT INTO tipo_pago VALUES("1","Transferencia Bancaria","0","1","0","0","2018-02-15 09:25:24"),
("2","Tarjeta de Debito/Credito","1","1","0","0","2018-02-15 09:25:24"),
("3","Deposito ","0","1","1","0","2018-08-22 00:00:00"),
("25","SIMPE","0","0","0","1","2021-06-22 11:09:42");



DROP TABLE IF EXISTS tipo_proceso;

CREATE TABLE `tipo_proceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_cliente` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tipo_cliente` (`id_tipo_cliente`) USING BTREE,
  CONSTRAINT `tipo_proceso_ibfk_1` FOREIGN KEY (`id_tipo_cliente`) REFERENCES `tipo_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;

INSERT INTO tipo_proceso VALUES("19","39","Cancelado","1","2021-06-09"),
("20","40","Cancelado","1","2021-06-09"),
("23","39","Cargar a Habitacion","0","2021-06-10"),
("27","51","Cancelado","0","2021-12-06");



DROP TABLE IF EXISTS tmp;

CREATE TABLE `tmp` (
  `id_tmp` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad_tmp` int(11) DEFAULT NULL,
  `precio_tmp` double DEFAULT NULL,
  `precio_tmp_exo` double DEFAULT NULL,
  `sessionn_id` varchar(255) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_tmp`)
) ENGINE=InnoDB AUTO_INCREMENT=780 DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS unidad_de_medida;

CREATE TABLE `unidad_de_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

INSERT INTO unidad_de_medida VALUES("1","Gramos","0","1","2021-10-26"),
("2","Kilogramos","0","1","2021-10-26"),
("3","Decagramo","0","1","2021-10-27"),
("4","Tonelada","0","1","2021-11-03"),
("5","Litros","0","1","2021-11-03");



DROP TABLE IF EXISTS user;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `pago` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("1","Administrador","admin","admin","admin@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","1","2016-12-13 09:08:03","0"),
("4","Gabriel","Quesada","gabrielqa","AmbientePruebas","adcd7048512e64b48da55b027577886ee5a36350","","1","1","2021-06-24 14:49:36","0"),
("5","Esteban ","Alcanzas ","Esteban","gabrielquesadajgqa@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","1","2022-02-03 14:01:26","0"),
("6","Priscilla ","Cordero","Priscilla","gabrielquesadajgqa@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","1","2022-02-03 14:01:41","0"),
("7","Loana ","Rodriguez","Loana","gabrielquesadajgqa@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","1","2022-02-03 14:02:00","0"),
("8","Bryan","Alpizar","Bryan","gabrielquesadajgqa@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","1","2022-02-03 14:02:17","0"),
("9","Josue","Martínez ","Josue","gabrielquesadajgqa@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","0","2022-02-03 14:02:36","0"),
("10","Maria","Carvajal","Maria","gabrielquesadajgqa@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","1","2022-02-03 14:02:52","0"),
("11","Juan","Lopez","Juan","Gabrielqqquesada@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","0","2022-02-03 14:39:14","0"),
("12","Rodrigo","Vega","Rodrigo","usuariodapos@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","0","2022-02-03 14:59:50","0"),
("13","Valeria","Arguellas","Valeria","usuariodapos@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","0","2022-02-03 15:00:08","0"),
("14","Valentina","Romp","Valentina","usuariodapos@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","0","2022-02-03 15:00:26","0"),
("15","Joshua","Vega","Joshua","Gabrielqqquesada@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","0","2022-02-03 15:00:46","0");



DROP TABLE IF EXISTS utilidad_producto;

CREATE TABLE `utilidad_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO utilidad_producto VALUES("1"," 30%","30","1","2021-10-27"),
("2","17%","17","1","2021-10-27");



DROP TABLE IF EXISTS venta;

CREATE TABLE `venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_comprobante` int(11) DEFAULT NULL,
  `nro_comprobante` varchar(25) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `total` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=latin1;

INSERT INTO venta VALUES("371","1","2312","753","1","1","150.35328","4","121","2021-12-07 11:20:28"),
("372","1","12121","754","1","1","1509.4992","4","121","2021-12-07 11:20:46"),
("373","2","12","751","1","1","1509.4992","4","121","2021-12-07 11:21:16"),
("374","1","13321","746","1","1","1509.4992","4","121","2021-12-07 11:29:23"),
("375","1","1231","746","1","1","1509.4992","4","121","2021-12-07 11:29:58"),
("376","1","12121","746","1","1","150.35328","4","121","2021-12-09 15:42:56"),
("377","2","1212","751","1","1","150.35328","4","121","2021-12-09 15:43:48"),
("378","1","123123","751","1","1","150.35328","4","121","2021-12-09 15:45:00"),
("379","2","2001","","1","2","133","10","130","2022-02-03 16:46:11"),
("380","1","1312","","1","2","1000","10","130","2022-02-03 16:54:03"),
("381","3","1","879","2","1","150.35328","10","130","2022-02-03 17:03:46"),
("382","3","1","880","2","1","307006.875","10","130","2022-02-03 17:10:16");



