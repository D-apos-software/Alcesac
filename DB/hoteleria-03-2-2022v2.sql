-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-02-2022 a las 00:18:46
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alcesac_alcesac_pruebas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aperturas_cajas`
--

CREATE TABLE `aperturas_cajas` (
  `id` int(11) NOT NULL,
  `id_caja` int(11) NOT NULL,
  `fecha_apertura` datetime NOT NULL,
  `fecha_cierre` datetime NOT NULL,
  `monto_apertura` int(11) NOT NULL,
  `monto_cierre` int(11) NOT NULL,
  `estado` varchar(500) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aperturas_cajas`
--

INSERT INTO `aperturas_cajas` (`id`, `id_caja`, `fecha_apertura`, `fecha_cierre`, `monto_apertura`, `monto_cierre`, `estado`, `id_usuario`, `fecha_creada`) VALUES
(123, 24, '2022-01-29 14:45:27', '2022-02-02 15:13:33', 12, 12, '0', 4, '2022-01-29 14:45:31'),
(124, 24, '2022-02-02 15:15:05', '2022-02-03 12:02:16', 100000, 100000, '0', 4, '2022-02-02 15:15:14'),
(125, 26, '2022-02-03 12:02:27', '2022-02-03 12:40:04', 100000, 100123, '0', 1, '2022-02-03 12:02:34'),
(126, 30, '2022-02-03 12:08:59', '2022-02-03 12:42:29', 200, 1200, '0', 4, '2022-02-03 12:09:05'),
(127, 25, '2022-02-03 12:42:35', '2022-02-03 14:00:53', 100, 26100, '0', 1, '2022-02-03 12:42:41'),
(128, 24, '2022-02-03 14:15:19', '0000-00-00 00:00:00', 100, 100, '1', 1, '2022-02-03 14:15:25'),
(129, 28, '2022-02-03 14:20:43', '2022-02-03 15:47:20', 200, 700, '0', 4, '2022-02-03 14:20:48'),
(130, 27, '2022-02-03 15:53:04', '2022-02-03 17:10:51', 200, 306224, '0', 10, '2022-02-03 15:53:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bodegas`
--

CREATE TABLE `bodegas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `cantidad_inicial` int(11) NOT NULL,
  `cantidad_maxima` int(11) NOT NULL,
  `cantidad_minima` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bodegas`
--

INSERT INTO `bodegas` (`id`, `codigo`, `nombre`, `cantidad_inicial`, `cantidad_maxima`, `cantidad_minima`, `estado`, `fecha_creada`) VALUES
(6, '001', 'Bodega', 100, 400, 50, 1, '2021-11-03'),
(7, '002', 'Bodega #2', 100, 1000, 50, 1, '2021-11-03'),
(8, '003', 'Bodega #3', 100, 400, 100, 1, '2021-11-03'),
(9, '004', 'Bodega #4', 100, 5000, 100, 1, '2021-11-03'),
(10, '005', 'Bodega #5', 100, 1000, 100, 1, '2021-11-03'),
(11, '006', 'Bodega #6', 100, 900, 20, 1, '2021-11-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `fecha_apertura` datetime DEFAULT NULL,
  `fecha_cierre` datetime DEFAULT NULL,
  `monto_apertura` double DEFAULT NULL,
  `monto_cierre` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `imagen`, `fecha_creada`) VALUES
(1, 'Personal', 'CASA-GOAS_30_INDIVIDUAL_1.jpg', '2018-02-15 09:14:21'),
(2, 'Doble', '1892_rec.jpg', '2019-02-26 11:39:02'),
(3, 'Triple', 'triple.jpg', '2019-02-26 11:39:08'),
(4, 'Cuadruple', 'cuadruple_DSC2747_resize-1024x683.jpg', '2019-02-26 11:40:17'),
(5, 'Especial', 'habitacion-grande-1.jpg', '2019-02-26 11:40:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_clientes_p`
--

CREATE TABLE `categoria_clientes_p` (
  `id` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria_clientes_p`
--

INSERT INTO `categoria_clientes_p` (`id`, `nombre`, `estado`, `fecha`) VALUES
(2, 'Bronce', 1, '2021-12-10'),
(3, 'Plata', 1, '2021-12-10'),
(4, 'Oro', 0, '2021-12-11'),
(5, 'Platino', 1, '2021-12-11'),
(7, 'Diamante', 1, '2021-12-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_venta`
--

CREATE TABLE `categoria_venta` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `id_tipo_categoria` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_proceso`
--

CREATE TABLE `cliente_proceso` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_proceso` int(11) DEFAULT NULL,
  `sesion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente_proceso`
--

INSERT INTO `cliente_proceso` (`id`, `id_cliente`, `id_proceso`, `sesion`) VALUES
(302, 768, 318, '4hvdrr5r5uanipfq6ufppjsm16'),
(303, 769, 319, '4hvdrr5r5uanipfq6ufppjsm16'),
(304, 770, 320, '4hvdrr5r5uanipfq6ufppjsm16'),
(305, 771, 321, '4hvdrr5r5uanipfq6ufppjsm16'),
(306, 772, 322, '4hvdrr5r5uanipfq6ufppjsm16'),
(307, 773, 323, '4hvdrr5r5uanipfq6ufppjsm16'),
(308, 774, 324, '4hvdrr5r5uanipfq6ufppjsm16'),
(309, 775, 325, '4hvdrr5r5uanipfq6ufppjsm16'),
(310, 776, 326, '4hvdrr5r5uanipfq6ufppjsm16'),
(311, 777, 327, '4hvdrr5r5uanipfq6ufppjsm16'),
(312, 778, 328, '4hvdrr5r5uanipfq6ufppjsm16'),
(313, 779, 329, '4hvdrr5r5uanipfq6ufppjsm16'),
(314, 780, 330, '4hvdrr5r5uanipfq6ufppjsm16'),
(315, 781, 331, '4hvdrr5r5uanipfq6ufppjsm16'),
(316, 782, 332, '4hvdrr5r5uanipfq6ufppjsm16'),
(317, 783, 333, '4hvdrr5r5uanipfq6ufppjsm16'),
(318, 784, 334, '4hvdrr5r5uanipfq6ufppjsm16'),
(319, 785, 335, '4hvdrr5r5uanipfq6ufppjsm16'),
(320, 786, 336, '4hvdrr5r5uanipfq6ufppjsm16'),
(321, 787, 337, '4hvdrr5r5uanipfq6ufppjsm16'),
(322, 788, 338, '4hvdrr5r5uanipfq6ufppjsm16'),
(323, 789, 339, '4hvdrr5r5uanipfq6ufppjsm16'),
(324, 790, 340, '4hvdrr5r5uanipfq6ufppjsm16'),
(325, 791, 341, '4dkbmrbnou42i3h2ufillm1srn'),
(326, 792, 342, '4dkbmrbnou42i3h2ufillm1srn'),
(327, 793, 343, '4dkbmrbnou42i3h2ufillm1srn'),
(328, 794, 344, '4dkbmrbnou42i3h2ufillm1srn'),
(329, 795, 345, '4dkbmrbnou42i3h2ufillm1srn'),
(330, 796, 346, '4dkbmrbnou42i3h2ufillm1srn'),
(331, 799, 347, '4dkbmrbnou42i3h2ufillm1srn'),
(332, 800, 348, '4dkbmrbnou42i3h2ufillm1srn'),
(333, 801, 349, '4dkbmrbnou42i3h2ufillm1srn'),
(334, 802, 350, '4dkbmrbnou42i3h2ufillm1srn'),
(335, 803, 351, '4dkbmrbnou42i3h2ufillm1srn'),
(336, 804, 352, '4dkbmrbnou42i3h2ufillm1srn'),
(337, 805, 353, '4dkbmrbnou42i3h2ufillm1srn'),
(338, 806, 354, '4dkbmrbnou42i3h2ufillm1srn'),
(339, 807, 355, '4dkbmrbnou42i3h2ufillm1srn'),
(340, 812, 356, '4dkbmrbnou42i3h2ufillm1srn'),
(341, 814, 357, '4dkbmrbnou42i3h2ufillm1srn'),
(342, 815, 358, '4dkbmrbnou42i3h2ufillm1srn'),
(343, 816, 359, '4dkbmrbnou42i3h2ufillm1srn'),
(344, 818, 360, '4dkbmrbnou42i3h2ufillm1srn'),
(345, 821, 361, '4dkbmrbnou42i3h2ufillm1srn'),
(346, 822, 362, '4dkbmrbnou42i3h2ufillm1srn'),
(347, 828, 363, '4dkbmrbnou42i3h2ufillm1srn'),
(348, 830, 364, '4dkbmrbnou42i3h2ufillm1srn'),
(349, 832, 365, '4dkbmrbnou42i3h2ufillm1srn'),
(350, 833, 366, '4dkbmrbnou42i3h2ufillm1srn'),
(351, 834, 367, '4dkbmrbnou42i3h2ufillm1srn'),
(352, 835, 368, '4dkbmrbnou42i3h2ufillm1srn'),
(353, 836, 369, '4dkbmrbnou42i3h2ufillm1srn'),
(354, 837, 370, '4dkbmrbnou42i3h2ufillm1srn'),
(355, 838, 371, '4dkbmrbnou42i3h2ufillm1srn'),
(356, 841, 372, '4dkbmrbnou42i3h2ufillm1srn'),
(357, 842, 373, '4dkbmrbnou42i3h2ufillm1srn'),
(358, 843, 374, '4dkbmrbnou42i3h2ufillm1srn'),
(359, 844, 375, '4dkbmrbnou42i3h2ufillm1srn'),
(360, 845, 376, '4dkbmrbnou42i3h2ufillm1srn'),
(361, 847, 377, '4dkbmrbnou42i3h2ufillm1srn'),
(362, 848, 378, '4dkbmrbnou42i3h2ufillm1srn'),
(363, 851, 379, '4dkbmrbnou42i3h2ufillm1srn'),
(364, 855, 380, '4dkbmrbnou42i3h2ufillm1srn'),
(365, 857, 381, '4dkbmrbnou42i3h2ufillm1srn'),
(366, 858, 382, '4dkbmrbnou42i3h2ufillm1srn'),
(367, 760, 395, '4dkbmrbnou42i3h2ufillm1srn'),
(368, 760, 396, '4dkbmrbnou42i3h2ufillm1srn'),
(369, 859, 397, '4dkbmrbnou42i3h2ufillm1srn'),
(370, 859, 398, '4dkbmrbnou42i3h2ufillm1srn'),
(371, 760, 399, '4dkbmrbnou42i3h2ufillm1srn'),
(372, 760, 400, '4dkbmrbnou42i3h2ufillm1srn'),
(373, 764, 401, '4dkbmrbnou42i3h2ufillm1srn'),
(374, 764, 402, '4dkbmrbnou42i3h2ufillm1srn'),
(375, 860, 403, '4dkbmrbnou42i3h2ufillm1srn'),
(376, 860, 404, '4dkbmrbnou42i3h2ufillm1srn'),
(377, 760, 405, '4dkbmrbnou42i3h2ufillm1srn'),
(378, 760, 406, '4dkbmrbnou42i3h2ufillm1srn'),
(379, 861, 407, '4dkbmrbnou42i3h2ufillm1srn'),
(380, 862, 408, '4dkbmrbnou42i3h2ufillm1srn'),
(381, 862, 409, '4dkbmrbnou42i3h2ufillm1srn'),
(382, 863, 410, '4dkbmrbnou42i3h2ufillm1srn'),
(383, 863, 411, '4dkbmrbnou42i3h2ufillm1srn'),
(384, 760, 412, 'f3ccs7tr0lrn9p84ct8qv85pe6'),
(385, 864, 413, '7om9i6utbu34l07vpcd9arvnlb'),
(386, 865, 414, 'e9uungkrbhfnmbcimjfi52af2p'),
(387, 866, 415, 'e9uungkrbhfnmbcimjfi52af2p'),
(388, 867, 416, 'e9uungkrbhfnmbcimjfi52af2p'),
(389, 868, 417, 'b0al4v6794bvc4c60rj6kjhr5o'),
(390, 869, 418, 'b0al4v6794bvc4c60rj6kjhr5o');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `rnc` varchar(25) DEFAULT NULL,
  `registro_empresarial` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `nombre`, `direccion`, `estado`, `telefono`, `fax`, `rnc`, `registro_empresarial`, `ciudad`, `logo`, `fecha`) VALUES
(1, 'Hotel Alcesac', 'Costa Rica, Cartago, Rio Cuarto.', 'Cartago', '+50661749563', 'NULL', '0038947384786', 'NULL', 'Tres Rios', 'Captura_de_pantalla__581_-removebg-preview_1.png', '2022-02-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_persona` int(11) DEFAULT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento_productos`
--

CREATE TABLE `descuento_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `descuento_productos`
--

INSERT INTO `descuento_productos` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(1, 'Descuento 12%', 12, 1, '2021-10-27'),
(2, 'Descuento 15%', 15, 1, '2021-10-27'),
(3, 'Descuento 20%', 20, 1, '2021-10-27'),
(4, 'Descuento 5%', 5, 1, '2021-10-27'),
(5, 'Sin Descuento', 0, 1, '2021-12-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cajas`
--

CREATE TABLE `estado_cajas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_cajas`
--

INSERT INTO `estado_cajas` (`id`, `nombre`, `valor`, `fecha_creada`) VALUES
(3, 'Ocupada\r\n\n', 0, 2),
(4, 'Disponible', 1, 2),
(5, 'Mantenimiento', 3, 2),
(6, 'Deshabilitada', 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_de_pago`
--

CREATE TABLE `estado_de_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `medio_pago` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_de_pago`
--

INSERT INTO `estado_de_pago` (`id`, `nombre`, `medio_pago`, `fecha_creada`) VALUES
(2, 'Pago realizado', 1, '2021-05-14'),
(8, 'En proceso', 0, '2021-06-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionalidades`
--

CREATE TABLE `funcionalidades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(110) NOT NULL,
  `estado` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `funcionalidades`
--

INSERT INTO `funcionalidades` (`id`, `nombre`, `estado`) VALUES
(1, 'Notificacio Email', 1),
(2, 'Notificaciones SMS', 0),
(3, 'Promociones', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--

CREATE TABLE `gasto` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `capacidad` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id`, `nombre`, `descripcion`, `precio`, `id_categoria`, `estado`, `capacidad`, `fecha_creada`) VALUES
(52, 'Habitacion 1', '123', 0, 2, 2, 1, '2021-12-04 16:23:49'),
(53, 'Habitacion 2', '123', 0, 1, 2, 1, '2021-12-07 09:09:45'),
(54, 'Habitacion 3', '21', 0, 2, 1, 1, '2021-12-07 09:10:26'),
(55, 'Habitacion 4', 'q', 0, 3, 4, 1, '2021-12-07 09:10:36'),
(56, 'Habitacion 5', '4', 0, 4, 1, 1, '2021-12-07 09:10:47'),
(57, 'Habitacion 6', '5', 0, 5, 1, 1, '2021-12-07 09:10:57'),
(58, 'Habitacion 7', '12', 0, 4, 1, 1, '2021-12-07 09:11:05'),
(59, 'Habitacion 8', '21', 0, 5, 2, 1, '2021-12-07 09:11:14'),
(60, 'Habitacion 9', '2', 0, 2, 1, 1, '2021-12-07 09:11:21'),
(61, 'Habitacion 10', '231', 0, 5, 2, 1, '2021-12-07 09:11:29'),
(62, 'Habitacion 8', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:00'),
(63, 'Habitacion 9', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:10'),
(64, 'Habitacion 10', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:19'),
(65, 'Habitacion 11', 'nd', 0, 1, 2, 1, '2022-02-03 12:38:20'),
(66, 'Habitacion 12', 'nd', 0, 1, 1, 1, '2022-02-03 12:38:27'),
(67, 'Habitacion 13', 'nd', 0, 1, 1, 1, '2022-02-03 12:50:57'),
(68, 'Habitacion 14', 'nd', 0, 1, 2, 1, '2022-02-03 12:51:03'),
(69, 'Habitacion 15', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:12'),
(70, 'Habitacion 16', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:19'),
(71, 'Habitacion 17', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:26'),
(72, 'Habitacion 18', 'nd', 0, 1, 2, 1, '2022-02-03 12:51:33'),
(73, 'Habitacion 19', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:39'),
(74, 'Habitacion 20', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:46'),
(75, 'Habitacion 21', 'nd', 0, 1, 1, 1, '2022-02-03 12:52:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `compras` int(11) NOT NULL,
  `egresos` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`id`, `nombre`, `valor`, `compras`, `egresos`, `fecha_creada`) VALUES
(1, 'IVA', 0, 1, 0, '0000-00-00 00:00:00'),
(3, 'Impuesto sobre la venta 13%', 1, 1, 0, '0000-00-00 00:00:00'),
(4, 'Impuesto sobre la venta 8%', 0, 0, 0, '0000-00-00 00:00:00'),
(5, 'Impuesto sobre la venta 4%', 0, 0, 0, '0000-00-00 00:00:00'),
(6, 'Impuesto sobre la venta 2%', 0, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos_productos`
--

CREATE TABLE `impuestos_productos` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `descripcion` text NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_tipo_impuesto` int(11) NOT NULL,
  `tipo_impuesto` varchar(30) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impuestos_productos`
--

INSERT INTO `impuestos_productos` (`id`, `valor`, `nombre`, `descripcion`, `estado`, `id_tipo_impuesto`, `tipo_impuesto`, `fecha_creada`) VALUES
(17, 13, '13%', 'TEST', 1, 6, 'asd', '2021-10-25'),
(18, 15, '15%', 'TEST', 1, 6, 'asd', '2021-10-25'),
(19, 10, '10%', 'TEST', 1, 6, 'dsa', '2021-10-26'),
(20, 8, '8%', 'TEST', 1, 7, 'asda', '2021-10-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimiento_cajas`
--

CREATE TABLE `mantenimiento_cajas` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mantenimiento_cajas`
--

INSERT INTO `mantenimiento_cajas` (`id`, `numero`, `estado`, `fecha_creada`) VALUES
(24, 1, 2, '2021-06-29'),
(25, 2, 1, '2021-06-29'),
(26, 3, 1, '2021-06-29'),
(27, 4, 1, '2021-06-29'),
(28, 5, 1, '2021-07-03'),
(30, 6, 1, '2021-07-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones`
--

CREATE TABLE `notificaciones` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notificaciones`
--

INSERT INTO `notificaciones` (`id`, `email`, `estado`, `valor`, `fecha_creada`) VALUES
(8, 'gabrielqqquesada@gmail.com', 0, 0, '2021-09-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones_stocks`
--

CREATE TABLE `notificaciones_stocks` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notificaciones_stocks`
--

INSERT INTO `notificaciones_stocks` (`id`, `valor`, `estado`, `fecha_creada`) VALUES
(1, 10, 1, '2021-09-14'),
(2, 15, 0, '2021-09-14'),
(3, 5, 1, '2021-09-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_pago`
--

CREATE TABLE `periodos_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `dias` int(250) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `periodos_pago`
--

INSERT INTO `periodos_pago` (`id`, `nombre`, `dias`, `fecha_creada`) VALUES
(2, 'Contado', 0, '2021-06-05'),
(3, 'Limite de fecha', 0, '2021-06-09'),
(4, 'Por pagos', 0, '2021-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `tipo_documento` int(11) DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `razon_social` varchar(150) DEFAULT NULL,
  `provincia` int(11) NOT NULL,
  `canton` int(11) NOT NULL,
  `distrito` int(11) NOT NULL,
  `barrio` int(11) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  `tipo` int(11) DEFAULT 1,
  `vip` int(11) NOT NULL DEFAULT 0,
  `contador` int(11) NOT NULL DEFAULT 0,
  `limite` int(11) NOT NULL DEFAULT 7,
  `nacionalidad` varchar(25) DEFAULT NULL,
  `estado_civil` varchar(12) DEFAULT NULL,
  `ocupacion` varchar(255) DEFAULT NULL,
  `medio_transporte` varchar(65) DEFAULT NULL,
  `destino` varchar(55) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `telefono_sec` int(11) NOT NULL,
  `celular` varchar(25) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `exonerado` int(11) DEFAULT NULL,
  `id_categoria_p` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `tipo_documento`, `documento`, `giro`, `nombre`, `fecha_nac`, `razon_social`, `provincia`, `canton`, `distrito`, `barrio`, `direccion`, `fecha_creada`, `tipo`, `vip`, `contador`, `limite`, `nacionalidad`, `estado_civil`, `ocupacion`, `medio_transporte`, `destino`, `motivo`, `telefono`, `telefono_sec`, `celular`, `email`, `exonerado`, `id_categoria_p`) VALUES
(760, 1, '207910714', NULL, 'Gabriel Quesada', NULL, NULL, 0, 0, 0, 0, 'SRM', '2022-02-02 14:09:51', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(761, 1, '207910715', NULL, 'Hernando Alonso', NULL, NULL, 0, 0, 0, 0, 'SRM', '2022-02-02 14:33:45', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(762, 1, '207910451', NULL, 'Maria Jose Carvajal ', NULL, NULL, 0, 0, 0, 0, 'CR, SRM, AI', '2022-02-02 14:45:21', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(763, 1, '207910713', NULL, 'Rodrigo Alejandro Ramirez', NULL, NULL, 0, 0, 0, 0, 'SR', '2022-02-02 14:46:34', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(764, 1, '207910710', NULL, 'Daniela Alvarado', NULL, NULL, 0, 0, 0, 0, 'ND', '2022-02-02 14:52:10', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(765, 1, '207910716', NULL, 'Francela Arguellas', NULL, NULL, 0, 0, 0, 0, 'ND', '2022-02-02 14:55:26', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(766, 1, '207910719', NULL, 'Antonio Francisco Gonzales ', NULL, NULL, 0, 0, 0, 0, 'ND', '2022-02-02 15:00:38', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(864, 2, '207910718', '1', 'LUISETH FERNANDA BARRANTES ALVARADO', NULL, '1', 0, 0, 0, 0, '1', '2022-02-03 12:21:58', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', 0, NULL, 'usuariodapos@gmail.com', 0, 0),
(865, 71, '207910452', '1', 'MARIA AUXILIADORA CASTRO SALAS', NULL, '1', 0, 0, 0, 0, 'CR, SRM, AI', '2022-02-03 13:56:59', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', 0, NULL, 'gabrielquesadajgqa@gmail.com', 0, 0),
(866, 2, '207910215', '1', 'KRISTEL VANESSA CHACON ESPINO', NULL, '1', 0, 0, 0, 0, '1', '2022-02-03 13:59:06', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', 0, NULL, 'gabrielquesadajgqa@gmail.com', 0, 2),
(867, 2, '207910722', '1', 'MARIA JOSE ALFARO RODRIGUEZ', NULL, '1', 0, 0, 0, 0, '1', '2022-02-03 14:00:48', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', 0, NULL, 'gabrielquesadajgqa@gmail.com', 0, 5),
(868, 2, '207910478', '1', 'KATHERINE YESENIA GARCIA MEJIA', NULL, '1', 0, 0, 0, 0, '1', '2022-02-03 15:45:33', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', 0, NULL, 'usuariodapos@gmail.com', 0, 0),
(869, 2, '207910818', '1', 'ANTHONY PINEDA SANCHEZ', NULL, '1', 0, 0, 0, 0, '1', '2022-02-03 15:46:57', 1, 0, 0, 7, NULL, NULL, '1', '1', '1', '1', '1', 0, NULL, 'usuariodapos@gmail.com', 0, 0),
(870, 1, '207910788', NULL, 'Hernan', NULL, NULL, 0, 0, 0, 0, 'SDR', '2022-02-03 16:24:32', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(871, 1, '207910744', NULL, 'Jessica', NULL, NULL, 0, 0, 0, 0, 'SR', '2022-02-03 16:25:55', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(872, 1, '207810718', NULL, 'Esteban', NULL, NULL, 0, 0, 0, 0, '1', '2022-02-03 16:31:59', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(873, 1, '207918118', NULL, 'Stiven', NULL, NULL, 0, 0, 0, 0, '1', '2022-02-03 16:33:09', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(877, 1, '207910111', NULL, 'Hernan', NULL, NULL, 0, 0, 0, 0, 'SDR', '2022-02-03 16:43:07', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(878, 1, '255887411', NULL, 'Joel', NULL, NULL, 0, 0, 0, 0, '1', '2022-02-03 16:44:11', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL),
(879, 71, '207910714', NULL, 'Gabriel Quesada', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-03 17:03:45', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'gabrielquesada1999@gmail.com', 0, 0),
(880, 71, '207910714', NULL, 'Gabriel Quesada', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-03 17:10:15', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'usuariodapos@gmail.com', 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL,
  `tipo_proceso` int(11) NOT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_apertura` int(11) NOT NULL,
  `precio` double NOT NULL DEFAULT 0,
  `cobro_ext` int(11) NOT NULL,
  `cant_noche` float NOT NULL DEFAULT 1,
  `dinero_dejado` double NOT NULL DEFAULT 0,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `fecha_entrada` datetime DEFAULT NULL,
  `fecha_salida` datetime DEFAULT NULL,
  `total` double NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `cant_personas` double DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `fecha_creada` datetime DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT 1,
  `observacion` varchar(255) DEFAULT NULL,
  `pagado` int(11) DEFAULT NULL,
  `nro_operacion` int(25) DEFAULT NULL,
  `num_tarjeta` int(11) DEFAULT NULL,
  `banco` int(11) DEFAULT NULL,
  `num_aprobacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id`, `tipo_proceso`, `id_habitacion`, `id_tarifa`, `id_cliente`, `id_apertura`, `precio`, `cobro_ext`, `cant_noche`, `dinero_dejado`, `id_tipo_pago`, `fecha_entrada`, `fecha_salida`, `total`, `id_usuario`, `cant_personas`, `id_caja`, `estado`, `fecha_creada`, `cantidad`, `observacion`, `pagado`, `nro_operacion`, `num_tarjeta`, `banco`, `num_aprobacion`) VALUES
(303, 0, 52, NULL, 760, 0, 0, 0, 1, 0, 1, '2022-02-05 10:00:00', '2022-02-18 15:10:00', 0, 4, 1, NULL, 3, '2022-02-02 14:09:51', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(304, 0, 53, NULL, 761, 0, 0, 0, 1, 0, 1, '2022-02-07 01:28:00', '2022-02-08 15:28:00', 0, 4, 1, NULL, 3, '2022-02-02 14:33:45', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(305, 0, 59, NULL, 762, 0, 0, 0, 1, 0, 1, '2022-02-11 15:44:00', '2022-02-13 15:44:00', 0, 4, 1, NULL, 3, '2022-02-02 14:45:21', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(306, 0, 55, NULL, 763, 0, 0, 0, 1, 0, 1, '2022-02-11 00:00:00', '2022-02-17 00:00:00', 0, 4, 1, NULL, 3, '2022-02-02 14:46:34', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(307, 0, 57, NULL, 764, 0, 0, 0, 1, 0, 1, '2022-02-08 00:00:00', '2022-02-15 00:00:00', 0, 4, 1, NULL, 3, '2022-02-02 14:52:10', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(308, 0, 61, NULL, 765, 0, 0, 0, 1, 0, 1, '2022-02-06 15:55:00', '2022-02-07 15:55:00', 0, 4, 1, NULL, 3, '2022-02-02 14:55:26', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(309, 0, 60, NULL, 766, 0, 0, 0, 1, 0, 1, '2022-02-08 00:00:00', '2022-02-09 00:00:00', 0, 4, 1, NULL, 3, '2022-02-02 15:00:38', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(412, 1, 52, 69, 760, 125, 123, 0, 1, 0, 1, '2022-02-03 12:11:30', '2022-02-04 12:00:00', 0, 1, 1, 125, 0, '2022-02-03 12:11:30', 1, NULL, 1, 0, 0, 1, 0),
(413, 1, 53, 70, 864, 126, 1000, 0, 1, 0, 1, '2022-02-03 12:21:58', '2022-02-04 12:00:00', 0, 4, 1, 126, 0, '2022-02-03 12:21:58', 1, NULL, 1, 0, 0, 1, 0),
(414, 1, 61, 78, 865, 127, 15000, 0, 1, 0, 1, '2022-02-03 13:56:59', '2022-02-04 12:00:00', 0, 1, 1, 127, 0, '2022-02-03 13:56:59', 1, NULL, 1, 0, 0, 1, 0),
(415, 1, 72, 79, 866, 127, 1000, 0, 1, 0, 1, '2022-02-03 13:59:06', '2022-02-04 12:00:00', 0, 1, 1, 127, 0, '2022-02-03 13:59:06', 1, NULL, 1, 0, 0, 1, 0),
(416, 1, 65, 80, 867, 127, 10000, 0, 1, 0, 1, '2022-02-03 14:00:48', '2022-02-04 12:00:00', 0, 1, 1, 127, 0, '2022-02-03 14:00:48', 1, NULL, 1, 0, 0, 1, 0),
(417, 1, 68, 82, 868, 129, 500, 0, 1, 0, 2, '2022-02-03 15:45:33', '2022-02-04 12:00:00', 0, 4, 1, 129, 0, '2022-02-03 15:45:33', 1, NULL, 1, 2010309123, 0, 4, 0),
(418, 0, 59, 76, 869, 129, 30000, 0, 1, 0, 0, '2022-02-03 15:46:57', '2022-02-04 12:00:00', 0, 4, 1, 129, 0, '2022-02-03 15:46:57', 1, NULL, 0, 0, 0, 0, 0),
(419, 0, 63, NULL, 870, 0, 0, 0, 1, 0, 1, '2022-02-13 00:00:00', '2022-02-22 00:00:00', 0, 10, 1, NULL, 3, '2022-02-03 16:24:32', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(420, 0, 64, NULL, 871, 0, 0, 0, 1, 0, 1, '2022-02-11 17:25:00', '2022-02-12 17:26:00', 0, 10, 1, NULL, 3, '2022-02-03 16:25:55', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(423, 0, 70, NULL, 872, 0, 0, 0, 1, 0, 1, '2022-02-13 00:00:00', '2022-02-21 00:00:00', 0, 10, 1, NULL, 3, '2022-02-03 16:31:59', 1, '1', NULL, NULL, NULL, NULL, NULL),
(424, 0, 69, NULL, 873, 0, 0, 0, 1, 0, 1, '2022-02-11 00:00:00', '2022-02-23 00:00:00', 0, 10, 1, NULL, 3, '2022-02-03 16:33:09', 1, '1', NULL, NULL, NULL, NULL, NULL),
(425, 0, 66, NULL, 872, 0, 0, 0, 1, 0, 1, '2022-02-09 00:00:00', '2022-02-18 00:00:00', 0, 10, 1, NULL, 3, '2022-02-03 16:33:48', 1, '1', NULL, NULL, NULL, NULL, NULL),
(429, 0, 54, NULL, 871, 0, 0, 0, 1, 0, 1, '2022-02-17 00:00:00', '2022-02-24 00:00:00', 0, 10, 1, NULL, 3, '2022-02-03 16:42:04', 1, '1', NULL, NULL, NULL, NULL, NULL),
(430, 0, 58, NULL, 877, 0, 0, 0, 1, 0, 1, '2022-02-24 00:00:00', '2022-02-25 00:00:00', 0, 10, 1, NULL, 3, '2022-02-03 16:43:07', 1, 'ND', NULL, NULL, NULL, NULL, NULL),
(431, 0, 62, NULL, 878, 0, 0, 0, 1, 0, 1, '2022-02-09 00:00:00', '2022-02-11 00:00:00', 0, 10, 1, NULL, 3, '2022-02-03 16:44:11', 1, '1', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_sueldo`
--

CREATE TABLE `proceso_sueldo` (
  `id` int(11) NOT NULL,
  `id_sueldo` int(11) DEFAULT NULL,
  `monto` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `tipo` int(11) NOT NULL DEFAULT 1,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_venta`
--

CREATE TABLE `proceso_venta` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_operacion` int(11) DEFAULT NULL,
  `id_venta` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso_venta`
--

INSERT INTO `proceso_venta` (`id`, `id_producto`, `id_operacion`, `id_venta`, `cantidad`, `precio`, `tipo_operacion`, `fecha_creada`, `id_caja`, `id_usuario`) VALUES
(463, 212, NULL, 379, 2, 5, 2, '2022-02-03 16:46:11', 130, 10),
(464, 214, NULL, 379, 1, 123, 2, '2022-02-03 16:46:11', 130, 10),
(465, 493, NULL, 380, 1, 1000, 2, '2022-02-03 16:54:03', 130, 10),
(466, 212, NULL, 381, 1, 150.35328, 1, '2022-02-03 17:03:46', 130, 10),
(467, 504, NULL, 382, 50, 6140.1375, 1, '2022-02-03 17:10:16', 130, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) DEFAULT NULL,
  `codigo_barra` varchar(50) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `acreditable` tinyint(1) NOT NULL DEFAULT 0,
  `presentacion` varchar(255) DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `stock` double NOT NULL DEFAULT 0,
  `aviso_stock` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `utilidad` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_bodega` int(11) NOT NULL,
  `id_unidad_medida` int(11) NOT NULL,
  `id_descuento` int(11) DEFAULT 0,
  `imagen` varchar(500) NOT NULL,
  `count_mail` int(11) NOT NULL,
  `estado` double NOT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `codigo`, `codigo_barra`, `nombre`, `marca`, `descripcion`, `acreditable`, `presentacion`, `precio_compra`, `precio_venta`, `stock`, `aviso_stock`, `stock_max`, `utilidad`, `id_proveedor`, `id_bodega`, `id_unidad_medida`, `id_descuento`, `imagen`, `count_mail`, `estado`, `fecha_creada`) VALUES
(212, '20213', '20213', 'Ranchitas', 'RA', '100', 1, 'Natura Fresco Natural', 5, 100, 100, 10, 105, 40, 0, 7, 1, 1, 'img/img_productos/Yummies-Snack-Ranchita-Queso-250-Gr-1-50299.jpg', 0, 0, '2021-11-11 11:45:57'),
(214, '20213', '20213', 'Picaronas', 'TEST', '123', 1, 'TEST', 123, 1000, 123, 123, 1000, 32, 0, 7, 1, 1, 'img/img_productos/6529-product-60aea52e3ac4c-150078-1.png', 0, 0, '2021-11-11 11:47:38'),
(215, '20213', '20213', 'Nutella', 'TEST', 'TEST', 0, 'Nutella', 313, 5000, 400, 40, 5000, 15, 0, 10, 1, 4, 'img/img_productos/81JnoKs70IL._SX425_.jpg', 0, 0, '2021-11-22 16:34:21'),
(493, 'C013', 'C013', 'Fanta', 'CRD2', 'TEST', 1, 'Fanta', 1000, 2000, 100, 14, 200, 1, 0, 8, 5, 5, 'img/img_productos/descarga.jpg', 0, 0, '2022-02-03 14:16:03'),
(494, 'C014', 'C014', 'Papiolas MD', 'Papiolas MD', 'TEST', 1, 'Papiolas MD', 1000, 3000, 100, 15, 200, 8, 0, 7, 1, 4, 'img/img_productos/2417319.jpg', 0, 0, '2022-02-03 14:25:21'),
(495, 'C016', 'C016', 'Papiolas', 'Papiolas', 'TEST', 1, 'Papiolas', 1200, 4000, 100, 15, 200, 12, 0, 8, 1, 4, 'img/img_productos/6529-product-60aea52e3ac4c-150078-1.png', 0, 0, '2022-02-03 14:28:36'),
(496, '01923', '01923', 'Sandia', 'Sandia', 'nd', 1, 'Sandia', 1000, 3000, 100, 15, 500, 10, 0, 6, 1, 4, 'img/img_productos/sandia.jpg', 0, 0, '2022-02-03 15:02:37'),
(497, '021784', '021784', 'Melon', 'Melon', 'nd', 1, 'Melon', 2000, 5200, 100, 15, 200, 6, 0, 11, 1, 4, 'img/img_productos/melon-mediano.png', 0, 0, '2022-02-03 15:03:37'),
(498, '20213', '20213', 'Papaya', 'TEST', '100', 1, 'Papaya', 5, 100, 90, 10, 150, 10, 0, 7, 1, 5, 'img/img_productos/melon-mediano.png', 0, 0, '2022-02-03 15:10:55'),
(503, '20313', '20313', 'Peperoni', 'Peperoni', 'ND', 1, 'Peperoni', 500, 1000, 100, 10, 250, 10, 0, 7, 1, 5, '0', 0, 0, '2022-02-03 15:30:29'),
(504, '22813', '22813', 'Chicharrones', 'Chicharrones', 'ND', 1, 'Chicharrones', 3500, 4500, 100, 10, 1000, 5, 0, 7, 1, 5, '0', 0, 0, '2022-02-03 15:30:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones`
--

CREATE TABLE `promociones` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `promociones`
--

INSERT INTO `promociones` (`id`, `valor`, `descripcion`, `estado`, `fecha`) VALUES
(48, 100, 'Promocion 1', 1, '2021-12-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_impuestos_productos`
--

CREATE TABLE `r_impuestos_productos` (
  `id` int(11) NOT NULL,
  `id_impuesto` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_impuestos_productos`
--

INSERT INTO `r_impuestos_productos` (`id`, `id_impuesto`, `id_producto`, `fecha_creada`) VALUES
(705, 20, 212, '2022-02-03'),
(706, 18, 214, '2022-02-03'),
(707, 20, 493, '2022-02-03'),
(708, 17, 215, '2022-02-03'),
(709, 20, 215, '2022-02-03'),
(710, 17, 494, '2022-02-03'),
(711, 19, 495, '2022-02-03'),
(712, 20, 495, '2022-02-03'),
(713, 19, 496, '2022-02-03'),
(714, 20, 497, '2022-02-03'),
(716, 18, 498, '2022-02-03'),
(721, 18, 503, '2022-02-03'),
(722, 18, 504, '2022-02-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_plazo_venta`
--

CREATE TABLE `r_plazo_venta` (
  `id` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_plazo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  `fecha_act` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_plazo_venta`
--

INSERT INTO `r_plazo_venta` (`id`, `id_venta`, `id_plazo`, `estado`, `fecha_creada`, `fecha_act`) VALUES
(8, 381, 2, 0, '2022-02-03', '2022-02-03'),
(9, 382, 2, 0, '2022-02-03', '2022-02-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_promociones`
--

CREATE TABLE `r_promociones` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT 0,
  `id_categoria` int(11) NOT NULL,
  `id_porcentaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_promociones`
--

INSERT INTO `r_promociones` (`id`, `id_producto`, `id_habitacion`, `id_categoria`, `id_porcentaje`) VALUES
(69, 0, 55, 5, 48),
(70, 0, 57, 5, 48),
(71, 0, 60, 5, 48),
(72, 214, 0, 5, 48);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_utilidades_productos`
--

CREATE TABLE `r_utilidades_productos` (
  `id` int(11) NOT NULL,
  `id_utilidad` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subimpuestos_productos`
--

CREATE TABLE `subimpuestos_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `subimpuestos_productos`
--

INSERT INTO `subimpuestos_productos` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(6, 'Cruz roja', 2, 1, '2021-10-25'),
(7, 'IVA', 13, 1, '2021-10-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldo`
--

CREATE TABLE `sueldo` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `monto` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `dia_pago` int(11) NOT NULL DEFAULT 1,
  `fecha_comienzo` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa`
--

CREATE TABLE `tarifa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa`
--

INSERT INTO `tarifa` (`id`, `nombre`) VALUES
(1, '24 Horas'),
(4, '12 horas'),
(7, 'Doble'),
(8, 'Personal'),
(9, 'Triple'),
(10, 'Cuadruple');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa_habitacion`
--

CREATE TABLE `tarifa_habitacion` (
  `id` int(11) NOT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa_habitacion`
--

INSERT INTO `tarifa_habitacion` (`id`, `id_tarifa`, `id_habitacion`, `precio`) VALUES
(17, 8, 6, 380),
(18, 7, 6, 580),
(19, 9, 6, 700),
(20, 10, 6, 800),
(23, 7, 8, 580),
(24, 7, 7, 580),
(25, 7, 9, 580),
(26, 9, 10, 700),
(27, 7, 11, 580),
(28, 8, 12, 380),
(29, 8, 13, 380),
(30, 8, 14, 500),
(31, 7, 14, 600),
(32, 8, 15, 380),
(33, 7, 16, 580),
(34, 7, 17, 580),
(35, 9, 18, 700),
(36, 7, 19, 580),
(37, 8, 20, 380),
(38, 8, 21, 380),
(39, 9, 22, 700),
(40, 8, 23, 380),
(41, 7, 24, 580),
(42, 7, 25, 580),
(43, 9, 26, 700),
(65, 1, 3, 1000),
(66, 1, 5, 1000),
(67, 1, 4, 1000),
(68, 1, 41, 50000),
(69, 4, 52, 123),
(70, 1, 53, 1000),
(71, 4, 54, 12000),
(72, 8, 55, 5000),
(73, 7, 56, 10000),
(74, 10, 57, 40000),
(75, 1, 58, 40000),
(76, 9, 59, 30000),
(77, 1, 60, 15000),
(78, 9, 61, 15000),
(79, 4, 72, 1000),
(80, 8, 65, 10000),
(81, 9, 65, 1000),
(82, 8, 68, 500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_banco`
--

CREATE TABLE `tipo_banco` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `numero_cuenta` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_banco`
--

INSERT INTO `tipo_banco` (`id`, `nombre`, `numero_cuenta`, `valor`, `fecha_creada`) VALUES
(1, 'Banco Costa Rica', 102, 0, '2021-08-03'),
(2, 'Coocique', 103, 0, '2021-08-03'),
(4, 'Bac San Jose', 104, 0, '2021-08-09'),
(15, 'BP', 0, 0, '2021-12-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_categoria_venta`
--

CREATE TABLE `tipo_categoria_venta` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_categoria_venta`
--

INSERT INTO `tipo_categoria_venta` (`id`, `nombre`, `codigo`, `estado`, `fecha_creada`) VALUES
(8, '123', '123', 1, '2021-11-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `id` int(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor_extra` int(250) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_cliente`
--

INSERT INTO `tipo_cliente` (`id`, `nombre`, `valor_extra`, `estado`, `fecha_creada`) VALUES
(39, 'Huesped de habitación', 1, 0, '2021-06-09'),
(40, 'Cliente Habitual', 0, 0, '2021-06-09'),
(51, 'Cliente Existente', 2, 0, '2021-11-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_comprobante`
--

CREATE TABLE `tipo_comprobante` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_comprobante`
--

INSERT INTO `tipo_comprobante` (`id`, `nombre`, `estado`) VALUES
(1, 'Ticket', 1),
(2, 'Boleta', 2),
(3, 'Factura Fisica', 3),
(7, 'Factura Electrónica ', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id`, `nombre`, `fecha_creada`) VALUES
(2, 'NITE', '2018-02-15 09:24:24'),
(71, 'Cedula', '2021-08-03 16:44:55'),
(72, 'Pasaporte', '2021-08-03 16:45:09'),
(79, 'DIMEX', '2021-11-09 10:23:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `banco` int(11) NOT NULL,
  `numero_aprobacion` int(11) NOT NULL,
  `numero_tarjeta` int(11) NOT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id`, `nombre`, `valor`, `banco`, `numero_aprobacion`, `numero_tarjeta`, `fecha_creada`) VALUES
(1, 'Transferencia Bancaria', 0, 1, 0, 0, '2018-02-15 09:25:24'),
(2, 'Tarjeta de Debito/Credito', 1, 1, 0, 0, '2018-02-15 09:25:24'),
(3, 'Deposito ', 0, 1, 1, 0, '2018-08-22 00:00:00'),
(25, 'SIMPE', 0, 0, 0, 1, '2021-06-22 11:09:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_proceso`
--

CREATE TABLE `tipo_proceso` (
  `id` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_proceso`
--

INSERT INTO `tipo_proceso` (`id`, `id_tipo_cliente`, `nombre`, `valor`, `fecha_creada`) VALUES
(19, 39, 'Cancelado', 1, '2021-06-09'),
(20, 40, 'Cancelado', 1, '2021-06-09'),
(23, 39, 'Cargar a Habitacion', 0, '2021-06-10'),
(27, 51, 'Cancelado', 0, '2021-12-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE `tmp` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad_tmp` int(11) DEFAULT NULL,
  `precio_tmp` double DEFAULT NULL,
  `precio_tmp_exo` double DEFAULT NULL,
  `sessionn_id` varchar(255) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_de_medida`
--

CREATE TABLE `unidad_de_medida` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `unidad_de_medida`
--

INSERT INTO `unidad_de_medida` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(1, 'Gramos', 0, 1, '2021-10-26'),
(2, 'Kilogramos', 0, 1, '2021-10-26'),
(3, 'Decagramo', 0, 1, '2021-10-27'),
(4, 'Tonelada', 0, 1, '2021-11-03'),
(5, 'Litros', 0, 1, '2021-11-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `pago` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `created_at`, `pago`) VALUES
(1, 'Administrador', 'admin', 'admin', 'admin@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2016-12-13 09:08:03', 0),
(4, 'Gabriel', 'Quesada', 'gabrielqa', 'AmbientePruebas', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2021-06-24 14:49:36', 0),
(5, 'Esteban ', 'Alcanzas ', 'Esteban', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:01:26', 0),
(6, 'Priscilla ', 'Cordero', 'Priscilla', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:01:41', 0),
(7, 'Loana ', 'Rodriguez', 'Loana', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:00', 0),
(8, 'Bryan', 'Alpizar', 'Bryan', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:17', 0),
(9, 'Josue', 'Martínez ', 'Josue', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:02:36', 0),
(10, 'Maria', 'Carvajal', 'Maria', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:52', 0),
(11, 'Juan', 'Lopez', 'Juan', 'Gabrielqqquesada@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:39:14', 0),
(12, 'Rodrigo', 'Vega', 'Rodrigo', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:59:50', 0),
(13, 'Valeria', 'Arguellas', 'Valeria', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:08', 0),
(14, 'Valentina', 'Romp', 'Valentina', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:26', 0),
(15, 'Joshua', 'Vega', 'Joshua', 'Gabrielqqquesada@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:46', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utilidad_producto`
--

CREATE TABLE `utilidad_producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `utilidad_producto`
--

INSERT INTO `utilidad_producto` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(1, ' 30%', 30, 1, '2021-10-27'),
(2, '17%', 17, 1, '2021-10-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `id_tipo_comprobante` int(11) DEFAULT NULL,
  `nro_comprobante` varchar(25) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `total` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `id_tipo_comprobante`, `nro_comprobante`, `id_proveedor`, `id_tipo_pago`, `tipo_operacion`, `total`, `id_usuario`, `id_caja`, `fecha_creada`) VALUES
(371, 1, '2312', 753, 1, 1, 150.35328, 4, 121, '2021-12-07 11:20:28'),
(372, 1, '12121', 754, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:20:46'),
(373, 2, '12', 751, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:21:16'),
(374, 1, '13321', 746, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:29:23'),
(375, 1, '1231', 746, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:29:58'),
(376, 1, '12121', 746, 1, 1, 150.35328, 4, 121, '2021-12-09 15:42:56'),
(377, 2, '1212', 751, 1, 1, 150.35328, 4, 121, '2021-12-09 15:43:48'),
(378, 1, '123123', 751, 1, 1, 150.35328, 4, 121, '2021-12-09 15:45:00'),
(379, 2, '2001', NULL, 1, 2, 133, 10, 130, '2022-02-03 16:46:11'),
(380, 1, '1312', NULL, 1, 2, 1000, 10, 130, '2022-02-03 16:54:03'),
(381, 3, '1', 879, 2, 1, 150.35328, 10, 130, '2022-02-03 17:03:46'),
(382, 3, '1', 880, 2, 1, 307006.875, 10, 130, '2022-02-03 17:10:16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_id_caja` (`id_caja`) USING BTREE;

--
-- Indices de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_clientes_p`
--
ALTER TABLE `categoria_clientes_p`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_categoria` (`id_tipo_categoria`);

--
-- Indices de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descuento_productos`
--
ALTER TABLE `descuento_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `funcionalidades`
--
ALTER TABLE `funcionalidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impuestos_productos`
--
ALTER TABLE `impuestos_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notificaciones_stocks`
--
ALTER TABLE `notificaciones_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria_promo` (`id_categoria_p`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bodega` (`id_bodega`),
  ADD KEY `FK_unidad_de_medida` (`id_unidad_medida`),
  ADD KEY `FK_descuento` (`id_descuento`) USING BTREE,
  ADD KEY `fk_proveedor` (`id_proveedor`);

--
-- Indices de la tabla `promociones`
--
ALTER TABLE `promociones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_impuesto` (`id_impuesto`),
  ADD KEY `FK_producto` (`id_producto`) USING BTREE;

--
-- Indices de la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_venta` (`id_venta`),
  ADD KEY `id_plazo` (`id_plazo`);

--
-- Indices de la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_producto` (`id_producto`) USING BTREE,
  ADD KEY `r_habitacion` (`id_habitacion`),
  ADD KEY `r_categoria` (`id_categoria`),
  ADD KEY `r_porcentaje` (`id_porcentaje`);

--
-- Indices de la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_utilidad` (`id_utilidad`),
  ADD KEY `FK_producto` (`id_producto`);

--
-- Indices de la tabla `subimpuestos_productos`
--
ALTER TABLE `subimpuestos_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_categoria_venta`
--
ALTER TABLE `tipo_categoria_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_cliente` (`id_tipo_cliente`) USING BTREE;

--
-- Indices de la tabla `tmp`
--
ALTER TABLE `tmp`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `unidad_de_medida`
--
ALTER TABLE `unidad_de_medida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `utilidad_producto`
--
ALTER TABLE `utilidad_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categoria_clientes_p`
--
ALTER TABLE `categoria_clientes_p`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=391;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `descuento_productos`
--
ALTER TABLE `descuento_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `funcionalidades`
--
ALTER TABLE `funcionalidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `gasto`
--
ALTER TABLE `gasto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `impuestos_productos`
--
ALTER TABLE `impuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `notificaciones_stocks`
--
ALTER TABLE `notificaciones_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=881;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=432;

--
-- AUTO_INCREMENT de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=468;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=505;

--
-- AUTO_INCREMENT de la tabla `promociones`
--
ALTER TABLE `promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=723;

--
-- AUTO_INCREMENT de la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `subimpuestos_productos`
--
ALTER TABLE `subimpuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tipo_categoria_venta`
--
ALTER TABLE `tipo_categoria_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `tmp`
--
ALTER TABLE `tmp`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=780;

--
-- AUTO_INCREMENT de la tabla `unidad_de_medida`
--
ALTER TABLE `unidad_de_medida`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `utilidad_producto`
--
ALTER TABLE `utilidad_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=383;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD CONSTRAINT `aperturas_cajas_ibfk_1` FOREIGN KEY (`id_caja`) REFERENCES `mantenimiento_cajas` (`id`);

--
-- Filtros para la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  ADD CONSTRAINT `fk_tipo_categoria` FOREIGN KEY (`id_tipo_categoria`) REFERENCES `tipo_categoria_venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_bodega` FOREIGN KEY (`id_bodega`) REFERENCES `bodegas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_descuentos_as` FOREIGN KEY (`id_descuento`) REFERENCES `descuento_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_unidad_de_medida` FOREIGN KEY (`id_unidad_medida`) REFERENCES `unidad_de_medida` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  ADD CONSTRAINT `fk_impuesto` FOREIGN KEY (`id_impuesto`) REFERENCES `impuestos_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  ADD CONSTRAINT `plazo` FOREIGN KEY (`id_plazo`) REFERENCES `periodos_pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  ADD CONSTRAINT `categoria_promo` FOREIGN KEY (`id_categoria`) REFERENCES `categoria_clientes_p` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `promociones` FOREIGN KEY (`id_porcentaje`) REFERENCES `promociones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  ADD CONSTRAINT `FK_producto_utilidad` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_utilidad_producto` FOREIGN KEY (`id_utilidad`) REFERENCES `utilidad_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD CONSTRAINT `tipo_proceso_ibfk_1` FOREIGN KEY (`id_tipo_cliente`) REFERENCES `tipo_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
