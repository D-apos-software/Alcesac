-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-03-2022 a las 01:35:54
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hoteleria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aperturas_cajas`
--

CREATE TABLE `aperturas_cajas` (
  `id` int(11) NOT NULL,
  `id_caja` int(11) NOT NULL,
  `fecha_apertura` datetime NOT NULL,
  `fecha_cierre` datetime NOT NULL,
  `monto_apertura` int(11) NOT NULL,
  `monto_cierre` int(11) NOT NULL,
  `estado` varchar(500) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aperturas_cajas`
--

INSERT INTO `aperturas_cajas` (`id`, `id_caja`, `fecha_apertura`, `fecha_cierre`, `monto_apertura`, `monto_cierre`, `estado`, `id_usuario`, `fecha_creada`) VALUES
(123, 24, '2022-01-29 14:45:27', '2022-02-02 15:13:33', 12, 12, '0', 4, '2022-01-29 14:45:31'),
(124, 24, '2022-02-02 15:15:05', '2022-02-03 12:02:16', 100000, 100000, '0', 4, '2022-02-02 15:15:14'),
(125, 26, '2022-02-03 12:02:27', '2022-02-03 12:40:04', 100000, 100123, '0', 1, '2022-02-03 12:02:34'),
(126, 30, '2022-02-03 12:08:59', '2022-02-03 12:42:29', 200, 1200, '0', 4, '2022-02-03 12:09:05'),
(127, 25, '2022-02-03 12:42:35', '2022-02-03 14:00:53', 100, 26100, '0', 1, '2022-02-03 12:42:41'),
(128, 24, '2022-02-03 14:15:19', '0000-00-00 00:00:00', 100, 100, '1', 1, '2022-02-03 14:15:25'),
(129, 28, '2022-02-03 14:20:43', '2022-02-03 15:47:20', 200, 700, '0', 4, '2022-02-03 14:20:48'),
(130, 27, '2022-02-03 15:53:04', '2022-02-03 17:10:51', 200, 306224, '0', 10, '2022-02-03 15:53:07'),
(131, 28, '2022-02-04 13:51:10', '0000-00-00 00:00:00', 200, 1719343, '1', 4, '2022-02-04 13:51:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_cabys`
--

CREATE TABLE `auto_cabys` (
  `id` int(11) NOT NULL,
  `descripcion` longtext NOT NULL,
  `cabys` longtext NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `fecha_act` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auto_cabys`
--

INSERT INTO `auto_cabys` (`id`, `descripcion`, `cabys`, `estado`, `fecha`, `fecha_act`) VALUES
(33, 'test2', '2391400009900', 0, '2022-02-11', '2022-02-11 11:45:05'),
(34, 'descripcion', '4141102000000', 0, '2022-02-11', '2022-02-11 09:54:03'),
(35, 'descripcion', '2799805000000', 0, '2022-02-11', '2022-02-11 11:22:20'),
(36, 'descripcion', '4299903010100', 0, '2022-02-11', '2022-02-11 11:49:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bodegas`
--

CREATE TABLE `bodegas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `cantidad_inicial` int(11) NOT NULL,
  `cantidad_maxima` int(11) NOT NULL,
  `cantidad_minima` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bodegas`
--

INSERT INTO `bodegas` (`id`, `codigo`, `nombre`, `cantidad_inicial`, `cantidad_maxima`, `cantidad_minima`, `estado`, `fecha_creada`) VALUES
(6, '001', 'Bodega', 100, 400, 50, 1, '2021-11-03'),
(7, '002', 'Bodega #2', 100, 1000, 50, 1, '2021-11-03'),
(8, '003', 'Bodega #3', 100, 400, 100, 1, '2021-11-03'),
(9, '004', 'Bodega #4', 100, 5000, 100, 1, '2021-11-03'),
(10, '005', 'Bodega #5', 100, 1000, 100, 1, '2021-11-03'),
(11, '006', 'Bodega #6', 100, 900, 20, 1, '2021-11-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `fecha_apertura` datetime DEFAULT NULL,
  `fecha_cierre` datetime DEFAULT NULL,
  `monto_apertura` double DEFAULT NULL,
  `monto_cierre` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `imagen`, `fecha_creada`) VALUES
(1, 'Personal', 'CASA-GOAS_30_INDIVIDUAL_1.jpg', '2018-02-15 09:14:21'),
(2, 'Doble', '1892_rec.jpg', '2019-02-26 11:39:02'),
(3, 'Triple', 'triple.jpg', '2019-02-26 11:39:08'),
(4, 'Cuadruple', 'cuadruple_DSC2747_resize-1024x683.jpg', '2019-02-26 11:40:17'),
(5, 'Especial', 'habitacion-grande-1.jpg', '2019-02-26 11:40:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_clientes_p`
--

CREATE TABLE `categoria_clientes_p` (
  `id` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria_clientes_p`
--

INSERT INTO `categoria_clientes_p` (`id`, `nombre`, `estado`, `fecha`) VALUES
(2, 'Bronce', 1, '2021-12-10'),
(3, 'Plata', 1, '2021-12-10'),
(4, 'Oro', 0, '2021-12-11'),
(5, 'Platino', 1, '2021-12-11'),
(7, 'Diamante', 1, '2021-12-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_venta`
--

CREATE TABLE `categoria_venta` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `id_tipo_categoria` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_proceso`
--

CREATE TABLE `cliente_proceso` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_proceso` int(11) DEFAULT NULL,
  `sesion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente_proceso`
--

INSERT INTO `cliente_proceso` (`id`, `id_cliente`, `id_proceso`, `sesion`) VALUES
(302, 768, 318, '4hvdrr5r5uanipfq6ufppjsm16'),
(303, 769, 319, '4hvdrr5r5uanipfq6ufppjsm16'),
(304, 770, 320, '4hvdrr5r5uanipfq6ufppjsm16'),
(305, 771, 321, '4hvdrr5r5uanipfq6ufppjsm16'),
(306, 772, 322, '4hvdrr5r5uanipfq6ufppjsm16'),
(307, 773, 323, '4hvdrr5r5uanipfq6ufppjsm16'),
(308, 774, 324, '4hvdrr5r5uanipfq6ufppjsm16'),
(309, 775, 325, '4hvdrr5r5uanipfq6ufppjsm16'),
(310, 776, 326, '4hvdrr5r5uanipfq6ufppjsm16'),
(311, 777, 327, '4hvdrr5r5uanipfq6ufppjsm16'),
(312, 778, 328, '4hvdrr5r5uanipfq6ufppjsm16'),
(313, 779, 329, '4hvdrr5r5uanipfq6ufppjsm16'),
(314, 780, 330, '4hvdrr5r5uanipfq6ufppjsm16'),
(315, 781, 331, '4hvdrr5r5uanipfq6ufppjsm16'),
(316, 782, 332, '4hvdrr5r5uanipfq6ufppjsm16'),
(317, 783, 333, '4hvdrr5r5uanipfq6ufppjsm16'),
(318, 784, 334, '4hvdrr5r5uanipfq6ufppjsm16'),
(319, 785, 335, '4hvdrr5r5uanipfq6ufppjsm16'),
(320, 786, 336, '4hvdrr5r5uanipfq6ufppjsm16'),
(321, 787, 337, '4hvdrr5r5uanipfq6ufppjsm16'),
(322, 788, 338, '4hvdrr5r5uanipfq6ufppjsm16'),
(323, 789, 339, '4hvdrr5r5uanipfq6ufppjsm16'),
(324, 790, 340, '4hvdrr5r5uanipfq6ufppjsm16'),
(325, 791, 341, '4dkbmrbnou42i3h2ufillm1srn'),
(326, 792, 342, '4dkbmrbnou42i3h2ufillm1srn'),
(327, 793, 343, '4dkbmrbnou42i3h2ufillm1srn'),
(328, 794, 344, '4dkbmrbnou42i3h2ufillm1srn'),
(329, 795, 345, '4dkbmrbnou42i3h2ufillm1srn'),
(330, 796, 346, '4dkbmrbnou42i3h2ufillm1srn'),
(331, 799, 347, '4dkbmrbnou42i3h2ufillm1srn'),
(332, 800, 348, '4dkbmrbnou42i3h2ufillm1srn'),
(333, 801, 349, '4dkbmrbnou42i3h2ufillm1srn'),
(334, 802, 350, '4dkbmrbnou42i3h2ufillm1srn'),
(335, 803, 351, '4dkbmrbnou42i3h2ufillm1srn'),
(336, 804, 352, '4dkbmrbnou42i3h2ufillm1srn'),
(337, 805, 353, '4dkbmrbnou42i3h2ufillm1srn'),
(338, 806, 354, '4dkbmrbnou42i3h2ufillm1srn'),
(339, 807, 355, '4dkbmrbnou42i3h2ufillm1srn'),
(340, 812, 356, '4dkbmrbnou42i3h2ufillm1srn'),
(341, 814, 357, '4dkbmrbnou42i3h2ufillm1srn'),
(342, 815, 358, '4dkbmrbnou42i3h2ufillm1srn'),
(343, 816, 359, '4dkbmrbnou42i3h2ufillm1srn'),
(344, 818, 360, '4dkbmrbnou42i3h2ufillm1srn'),
(345, 821, 361, '4dkbmrbnou42i3h2ufillm1srn'),
(346, 822, 362, '4dkbmrbnou42i3h2ufillm1srn'),
(347, 828, 363, '4dkbmrbnou42i3h2ufillm1srn'),
(348, 830, 364, '4dkbmrbnou42i3h2ufillm1srn'),
(349, 832, 365, '4dkbmrbnou42i3h2ufillm1srn'),
(350, 833, 366, '4dkbmrbnou42i3h2ufillm1srn'),
(351, 834, 367, '4dkbmrbnou42i3h2ufillm1srn'),
(352, 835, 368, '4dkbmrbnou42i3h2ufillm1srn'),
(353, 836, 369, '4dkbmrbnou42i3h2ufillm1srn'),
(354, 837, 370, '4dkbmrbnou42i3h2ufillm1srn'),
(355, 838, 371, '4dkbmrbnou42i3h2ufillm1srn'),
(356, 841, 372, '4dkbmrbnou42i3h2ufillm1srn'),
(357, 842, 373, '4dkbmrbnou42i3h2ufillm1srn'),
(358, 843, 374, '4dkbmrbnou42i3h2ufillm1srn'),
(359, 844, 375, '4dkbmrbnou42i3h2ufillm1srn'),
(360, 845, 376, '4dkbmrbnou42i3h2ufillm1srn'),
(361, 847, 377, '4dkbmrbnou42i3h2ufillm1srn'),
(362, 848, 378, '4dkbmrbnou42i3h2ufillm1srn'),
(363, 851, 379, '4dkbmrbnou42i3h2ufillm1srn'),
(364, 855, 380, '4dkbmrbnou42i3h2ufillm1srn'),
(365, 857, 381, '4dkbmrbnou42i3h2ufillm1srn'),
(366, 858, 382, '4dkbmrbnou42i3h2ufillm1srn'),
(367, 760, 395, '4dkbmrbnou42i3h2ufillm1srn'),
(368, 760, 396, '4dkbmrbnou42i3h2ufillm1srn'),
(369, 859, 397, '4dkbmrbnou42i3h2ufillm1srn'),
(370, 859, 398, '4dkbmrbnou42i3h2ufillm1srn'),
(371, 760, 399, '4dkbmrbnou42i3h2ufillm1srn'),
(372, 760, 400, '4dkbmrbnou42i3h2ufillm1srn'),
(373, 764, 401, '4dkbmrbnou42i3h2ufillm1srn'),
(374, 764, 402, '4dkbmrbnou42i3h2ufillm1srn'),
(375, 860, 403, '4dkbmrbnou42i3h2ufillm1srn'),
(376, 860, 404, '4dkbmrbnou42i3h2ufillm1srn'),
(377, 760, 405, '4dkbmrbnou42i3h2ufillm1srn'),
(378, 760, 406, '4dkbmrbnou42i3h2ufillm1srn'),
(379, 861, 407, '4dkbmrbnou42i3h2ufillm1srn'),
(380, 862, 408, '4dkbmrbnou42i3h2ufillm1srn'),
(381, 862, 409, '4dkbmrbnou42i3h2ufillm1srn'),
(382, 863, 410, '4dkbmrbnou42i3h2ufillm1srn'),
(383, 863, 411, '4dkbmrbnou42i3h2ufillm1srn'),
(384, 760, 412, 'f3ccs7tr0lrn9p84ct8qv85pe6'),
(385, 864, 413, '7om9i6utbu34l07vpcd9arvnlb'),
(386, 865, 414, 'e9uungkrbhfnmbcimjfi52af2p'),
(387, 866, 415, 'e9uungkrbhfnmbcimjfi52af2p'),
(388, 867, 416, 'e9uungkrbhfnmbcimjfi52af2p'),
(389, 868, 417, 'b0al4v6794bvc4c60rj6kjhr5o'),
(390, 869, 418, 'b0al4v6794bvc4c60rj6kjhr5o'),
(391, 881, 432, 'ojkd79kv0jqtu1v436qub65bma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `rnc` varchar(25) DEFAULT NULL,
  `registro_empresarial` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `usuario_hacienda` varchar(50) NOT NULL,
  `contrasena_hacienda` varchar(50) NOT NULL,
  `llave_criptografica` varchar(50) NOT NULL,
  `token` varchar(600) NOT NULL,
  `pin` int(6) NOT NULL,
  `cod_moneda` varchar(20) NOT NULL,
  `nombre_hac` varchar(100) NOT NULL,
  `numero_id` int(60) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `provincia` int(10) NOT NULL,
  `canton` int(60) NOT NULL,
  `distrito` int(60) NOT NULL,
  `barrio` int(60) NOT NULL,
  `cod_pais_tel_hc` int(60) NOT NULL,
  `tel_hac` int(60) NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `nombre`, `direccion`, `estado`, `telefono`, `fax`, `rnc`, `registro_empresarial`, `ciudad`, `logo`, `usuario_hacienda`, `contrasena_hacienda`, `llave_criptografica`, `token`, `pin`, `cod_moneda`, `nombre_hac`, `numero_id`, `tipo_id`, `provincia`, `canton`, `distrito`, `barrio`, `cod_pais_tel_hc`, `tel_hac`, `fecha`) VALUES
(1, 'Alcesac', 'Costa Rica, Cartago, Rio Cuarto.', 'Cartago', '+50661749563', 'NULL', '0038947384786', 'NULL', 'Tres Rios', 'Alcesac.jpeg', 'cpf-02-0791-0714@stag.comprobanteselectronicos.go.', '@W>oMjx#0K#})l0#xL>!', 'file.p12', 'e8323365a649c9da903a2776a9fb586c', 1234, 'CRC', 'Gabriel Quesada Araya', 207910714, 1, 1, 1, 1, 1, 506, 61749563, '2022-02-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_persona` int(11) DEFAULT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_factura`
--

CREATE TABLE `datos_factura` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `tipo_documento` varchar(100) NOT NULL,
  `tipo_cedula` varchar(100) NOT NULL,
  `fecha_emision` date NOT NULL,
  `emisor_nombre` varchar(100) NOT NULL,
  `emisor_tipo_identif` varchar(100) NOT NULL,
  `emisor_num_identif` int(11) NOT NULL,
  `nombre_comercial` varchar(100) NOT NULL,
  `emisor_provincia` int(11) NOT NULL,
  `emisor_canton` int(11) NOT NULL,
  `emisor_distrito` int(11) NOT NULL,
  `emisor_barrio` int(11) NOT NULL,
  `emisor_cod_pais_tel` int(11) NOT NULL,
  `emisor_email` varchar(100) NOT NULL,
  `receptor_nombre` varchar(100) NOT NULL,
  `receptor_tipo_identif` int(11) NOT NULL,
  `receptor_num_identif` int(11) NOT NULL,
  `receptor_provincia` int(11) NOT NULL,
  `receptor_canton` int(11) NOT NULL,
  `receptor_distrito` int(11) NOT NULL,
  `receptor_barrio` int(11) NOT NULL,
  `receptor_cod_pais_tel` int(11) NOT NULL,
  `receptor_tel` int(11) NOT NULL,
  `receptor_email` varchar(100) NOT NULL,
  `condicion_venta` int(11) NOT NULL,
  `plazo_credito` int(11) NOT NULL,
  `medio_pago` int(11) NOT NULL,
  `cod_moneda` varchar(100) NOT NULL,
  `tipo_cambio` int(11) NOT NULL,
  `total_serv_gravados` int(11) NOT NULL,
  `total_serv_exentos` int(11) NOT NULL,
  `total_merc_gravada` int(11) NOT NULL,
  `total_merc_exenta` int(11) NOT NULL,
  `total_gravados` int(11) NOT NULL,
  `total_exentos` int(11) NOT NULL,
  `total_ventas` int(11) NOT NULL,
  `total_descuentos` int(11) NOT NULL,
  `total_ventas_neta` int(11) NOT NULL,
  `total_impuesto` int(11) NOT NULL,
  `total_comprobante` int(11) NOT NULL,
  `token_llave` longtext NOT NULL,
  `id_caja` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `omitir_receptor` varchar(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `datos_factura`
--

INSERT INTO `datos_factura` (`id`, `id_cliente`, `tipo_documento`, `tipo_cedula`, `fecha_emision`, `emisor_nombre`, `emisor_tipo_identif`, `emisor_num_identif`, `nombre_comercial`, `emisor_provincia`, `emisor_canton`, `emisor_distrito`, `emisor_barrio`, `emisor_cod_pais_tel`, `emisor_email`, `receptor_nombre`, `receptor_tipo_identif`, `receptor_num_identif`, `receptor_provincia`, `receptor_canton`, `receptor_distrito`, `receptor_barrio`, `receptor_cod_pais_tel`, `receptor_tel`, `receptor_email`, `condicion_venta`, `plazo_credito`, `medio_pago`, `cod_moneda`, `tipo_cambio`, `total_serv_gravados`, `total_serv_exentos`, `total_merc_gravada`, `total_merc_exenta`, `total_gravados`, `total_exentos`, `total_ventas`, `total_descuentos`, `total_ventas_neta`, `total_impuesto`, `total_comprobante`, `token_llave`, `id_caja`, `id_usuario`, `omitir_receptor`, `estado`) VALUES
(12, 207910714, 'FE', 'fisica', '2022-03-15', 'Gabriel Quesada Araya', '01', 207910714, 'Gabriel Quesada Araya', 1, 1, 1, 1, 506, 'usuariodapos@gmail.com', 'JOSE GABRIEL QUESADA ARAYA', 1, 207910714, 0, 0, 0, 0, 0, 0, 'gabrielqqquesada@gmail.com', 1, 0, 4, 'CRC', 646, 2120, 0, 0, 0, 2120, 0, 2120, 106, 2014, 262, 2276, 'e8323365a649c9da903a2776a9fb586c', 28, 4, '0', 0),
(13, 207910714, 'FE', 'fisica', '2022-03-15', 'Gabriel Quesada Araya', '01', 207910714, 'Gabriel Quesada Araya', 1, 1, 1, 1, 506, 'usuariodapos@gmail.com', 'JOSE GABRIEL QUESADA ARAYA', 1, 207910714, 0, 0, 0, 0, 0, 0, 'gabrielqqquesada@gmail.com', 1, 0, 4, 'CRC', 646, 2120, 0, 0, 0, 2120, 0, 2120, 106, 2014, 262, 2276, 'e8323365a649c9da903a2776a9fb586c', 28, 4, '0', 0),
(14, 207910714, 'FE', 'fisica', '2022-03-16', 'Gabriel Quesada Araya', '01', 207910714, 'Gabriel Quesada Araya', 1, 1, 1, 1, 506, 'usuariodapos@gmail.com', 'JOSE GABRIEL QUESADA ARAYA', 1, 207910714, 0, 0, 0, 0, 0, 0, 'gabrielqqquesada@gmail.com', 1, 0, 4, 'CRC', 644, 2120, 0, 0, 0, 2120, 0, 2120, 106, 2014, 262, 2276, 'e8323365a649c9da903a2776a9fb586c', 28, 4, 'true', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento_productos`
--

CREATE TABLE `descuento_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `descuento_productos`
--

INSERT INTO `descuento_productos` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(1, 'Descuento 12%', 12, 1, '2021-10-27'),
(2, 'Descuento 15%', 15, 1, '2021-10-27'),
(3, 'Descuento 20%', 20, 1, '2021-10-27'),
(4, 'Descuento 5%', 5, 1, '2021-10-27'),
(5, 'Sin Descuento', 0, 1, '2021-12-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_producto_factura`
--

CREATE TABLE `detalles_producto_factura` (
  `id` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `unidad_medida` varchar(100) NOT NULL,
  `detalle` varchar(100) NOT NULL,
  `precio_unitario` int(11) NOT NULL,
  `monto_total` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `monto_total_lineal` int(11) NOT NULL,
  `d_monto_descuento` int(11) NOT NULL,
  `d_naturaleza_descuento` varchar(100) NOT NULL,
  `imp_codigo` int(11) NOT NULL,
  `imp_codigo_Tarifa` int(11) NOT NULL,
  `imp_tarifa` int(11) NOT NULL,
  `mp_monto` int(11) NOT NULL,
  `id_fact_hacienda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalles_producto_factura`
--

INSERT INTO `detalles_producto_factura` (`id`, `codigo`, `cantidad`, `unidad_medida`, `detalle`, `precio_unitario`, `monto_total`, `subtotal`, `monto_total_lineal`, `d_monto_descuento`, `d_naturaleza_descuento`, `imp_codigo`, `imp_codigo_Tarifa`, `imp_tarifa`, `mp_monto`, `id_fact_hacienda`) VALUES
(149, 2147483647, 1, 'Sp', 'Servicios informaticos', 2120, 2120, 2014, 2276, 106, 'Descuento', 1, 8, 13, 262, 12),
(150, 2147483647, 1, 'Sp', 'Servicios informaticos', 2120, 2120, 2014, 2276, 106, 'Descuento', 1, 8, 13, 262, 13),
(151, 2147483647, 1, 'Sp', 'Servicios informaticos', 2120, 2120, 2014, 2276, 106, 'Descuento', 1, 8, 13, 262, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cajas`
--

CREATE TABLE `estado_cajas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_cajas`
--

INSERT INTO `estado_cajas` (`id`, `nombre`, `valor`, `fecha_creada`) VALUES
(3, 'Ocupada\r\n\n', 0, 2),
(4, 'Disponible', 1, 2),
(5, 'Mantenimiento', 3, 2),
(6, 'Deshabilitada', 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_de_pago`
--

CREATE TABLE `estado_de_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `medio_pago` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_de_pago`
--

INSERT INTO `estado_de_pago` (`id`, `nombre`, `medio_pago`, `fecha_creada`) VALUES
(2, 'Pago realizado', 1, '2021-05-14'),
(8, 'En proceso', 0, '2021-06-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionalidades`
--

CREATE TABLE `funcionalidades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(110) NOT NULL,
  `estado` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `funcionalidades`
--

INSERT INTO `funcionalidades` (`id`, `nombre`, `estado`) VALUES
(1, 'Notificacio Email', 0),
(2, 'Notificaciones SMS', 0),
(3, 'Promociones', 0),
(4, 'Facturación Electrónica', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--

CREATE TABLE `gasto` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `capacidad` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id`, `nombre`, `descripcion`, `precio`, `id_categoria`, `estado`, `capacidad`, `fecha_creada`) VALUES
(52, 'Habitacion 1', '123', 0, 2, 1, 1, '2021-12-04 16:23:49'),
(53, 'Habitacion 2', '123', 0, 1, 1, 1, '2021-12-07 09:09:45'),
(54, 'Habitacion 3', '21', 0, 2, 2, 1, '2021-12-07 09:10:26'),
(55, 'Habitacion 4', 'q', 0, 3, 1, 1, '2021-12-07 09:10:36'),
(56, 'Habitacion 5', '4', 0, 4, 1, 1, '2021-12-07 09:10:47'),
(57, 'Habitacion 6', '5', 0, 5, 1, 1, '2021-12-07 09:10:57'),
(58, 'Habitacion 7', '12', 0, 4, 1, 1, '2021-12-07 09:11:05'),
(59, 'Habitacion 8', '21', 0, 5, 1, 1, '2021-12-07 09:11:14'),
(60, 'Habitacion 9', '2', 0, 2, 1, 1, '2021-12-07 09:11:21'),
(61, 'Habitacion 10', '231', 0, 5, 1, 1, '2021-12-07 09:11:29'),
(62, 'Habitacion 8', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:00'),
(63, 'Habitacion 9', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:10'),
(64, 'Habitacion 10', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:19'),
(65, 'Habitacion 11', 'nd', 0, 1, 1, 1, '2022-02-03 12:38:20'),
(66, 'Habitacion 12', 'nd', 0, 1, 1, 1, '2022-02-03 12:38:27'),
(67, 'Habitacion 13', 'nd', 0, 1, 1, 1, '2022-02-03 12:50:57'),
(68, 'Habitacion 14', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:03'),
(69, 'Habitacion 15', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:12'),
(70, 'Habitacion 16', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:19'),
(71, 'Habitacion 17', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:26'),
(72, 'Habitacion 18', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:33'),
(73, 'Habitacion 19', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:39'),
(74, 'Habitacion 20', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:46'),
(75, 'Habitacion 21', 'nd', 0, 1, 1, 1, '2022-02-03 12:52:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `compras` int(11) NOT NULL,
  `egresos` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`id`, `nombre`, `valor`, `compras`, `egresos`, `fecha_creada`) VALUES
(1, 'IVA', 0, 1, 0, '0000-00-00 00:00:00'),
(3, 'Impuesto sobre la venta 13%', 1, 1, 0, '0000-00-00 00:00:00'),
(4, 'Impuesto sobre la venta 8%', 0, 0, 0, '0000-00-00 00:00:00'),
(5, 'Impuesto sobre la venta 4%', 0, 0, 0, '0000-00-00 00:00:00'),
(6, 'Impuesto sobre la venta 2%', 0, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos_productos`
--

CREATE TABLE `impuestos_productos` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `descripcion` text NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_tipo_impuesto` int(11) NOT NULL,
  `tipo_impuesto` varchar(30) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impuestos_productos`
--

INSERT INTO `impuestos_productos` (`id`, `valor`, `nombre`, `descripcion`, `estado`, `id_tipo_impuesto`, `tipo_impuesto`, `fecha_creada`) VALUES
(17, 13, 'Tarifa general 13%', 'TEST', 1, 6, '08', '2021-10-25'),
(18, 8, 'Transitorio 8%', 'TEST', 1, 6, '07', '2021-10-25'),
(19, 4, 'Transitorio 4% ', 'TEST', 1, 6, '06', '2021-10-26'),
(20, 2, '2%', 'TEST', 0, 7, 'asda', '2021-10-28'),
(22, 12, 'NITE', '', 0, 0, '12', '2022-03-12'),
(23, 0, 'Tarifa 0% (Exento)', '', 1, 0, '01', '2022-03-12'),
(24, 1, 'Tarifa 1%', '', 1, 0, '02', '2022-03-12'),
(25, 2, 'Tarifa reducida 2%', '', 1, 0, '03', '2022-03-12'),
(26, 4, 'Tarifa reducida 4%', '', 1, 0, '04', '2022-03-12'),
(27, 5, 'Transitorio 0%', '', 1, 0, '05', '2022-03-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimiento_cajas`
--

CREATE TABLE `mantenimiento_cajas` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mantenimiento_cajas`
--

INSERT INTO `mantenimiento_cajas` (`id`, `numero`, `estado`, `fecha_creada`) VALUES
(24, 1, 2, '2021-06-29'),
(25, 2, 1, '2021-06-29'),
(26, 3, 1, '2021-06-29'),
(27, 4, 1, '2021-06-29'),
(28, 5, 2, '2021-07-03'),
(30, 6, 1, '2021-07-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones`
--

CREATE TABLE `notificaciones` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notificaciones`
--

INSERT INTO `notificaciones` (`id`, `email`, `estado`, `valor`, `fecha_creada`) VALUES
(8, 'gabrielqqquesada@gmail.com', 1, 0, '2021-09-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones_stocks`
--

CREATE TABLE `notificaciones_stocks` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notificaciones_stocks`
--

INSERT INTO `notificaciones_stocks` (`id`, `valor`, `estado`, `fecha_creada`) VALUES
(1, 10, 1, '2021-09-14'),
(2, 15, 0, '2021-09-14'),
(3, 5, 1, '2021-09-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_pago`
--

CREATE TABLE `periodos_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `dias` int(250) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `periodos_pago`
--

INSERT INTO `periodos_pago` (`id`, `nombre`, `dias`, `fecha_creada`) VALUES
(2, 'Contado', 0, '2021-06-05'),
(3, 'Limite de fecha', 0, '2021-06-09'),
(4, 'Por pagos', 0, '2021-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `tipo_documento` int(11) DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `razon_social` varchar(150) DEFAULT NULL,
  `provincia` int(11) NOT NULL,
  `canton` int(11) NOT NULL,
  `distrito` int(11) NOT NULL,
  `barrio` int(11) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  `tipo` int(11) DEFAULT 1,
  `vip` int(11) NOT NULL DEFAULT 0,
  `contador` int(11) NOT NULL DEFAULT 0,
  `limite` int(11) NOT NULL DEFAULT 7,
  `nacionalidad` varchar(25) DEFAULT NULL,
  `estado_civil` varchar(12) DEFAULT NULL,
  `ocupacion` varchar(255) DEFAULT NULL,
  `medio_transporte` varchar(65) DEFAULT NULL,
  `destino` varchar(55) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `telefono_sec` int(11) NOT NULL,
  `celular` varchar(25) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `exonerado` int(11) DEFAULT NULL,
  `id_categoria_p` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `tipo_documento`, `documento`, `giro`, `nombre`, `fecha_nac`, `razon_social`, `provincia`, `canton`, `distrito`, `barrio`, `direccion`, `fecha_creada`, `tipo`, `vip`, `contador`, `limite`, `nacionalidad`, `estado_civil`, `ocupacion`, `medio_transporte`, `destino`, `motivo`, `telefono`, `telefono_sec`, `celular`, `email`, `exonerado`, `id_categoria_p`) VALUES
(881, 71, '207910714', 'NULL', 'JOSE GABRIEL QUESADA ARAYA', '0000-00-00', 'NULL', 4, 5, 1, 122, 'NULL', '2022-02-15 13:15:20', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, '24510254', 24510254, NULL, 'gabrielqqquesada@gmail.com', 0, 0),
(882, 2, '123', '1', '1', '0000-00-00', 'NITE', 2, 2, 7, 232, '123', '2022-02-17 09:53:52', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, '12332312', 12332312, NULL, 'gquesada@d-apos.com', 0, 0),
(885, 71, '207910716', NULL, 'MARIA JOSE CAMPOS BARRANTES', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-17 13:33:01', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'gabrielqqquesada@gmail.com', 0, 0),
(889, 71, '207910715', NULL, 'MARIA CELESTE CAPARROSO ZUÑIGA', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-17 13:40:07', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'gabrielqqquesada@gmail.com', 0, 0),
(904, 71, '207910719', NULL, 'MARCO ANTONIO ALFARO ALVARADO', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-17 17:05:40', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 0),
(930, 71, '208300168', NULL, 'VALERIA BARRANTES HERNANDEZ', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-18 20:32:30', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 0),
(934, 71, '207910415', NULL, 'ANGIE DANIELA FERNANDEZ CHACON', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-19 08:59:42', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL,
  `tipo_proceso` int(11) NOT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_apertura` int(11) NOT NULL,
  `precio` double NOT NULL DEFAULT 0,
  `cobro_ext` int(11) NOT NULL,
  `cant_noche` float NOT NULL DEFAULT 1,
  `dinero_dejado` double NOT NULL DEFAULT 0,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `fecha_entrada` datetime DEFAULT NULL,
  `fecha_salida` datetime DEFAULT NULL,
  `total` double NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `cant_personas` double DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `fecha_creada` datetime DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT 1,
  `observacion` varchar(255) DEFAULT NULL,
  `pagado` int(11) DEFAULT NULL,
  `nro_operacion` int(25) DEFAULT NULL,
  `num_tarjeta` int(11) DEFAULT NULL,
  `banco` int(11) DEFAULT NULL,
  `num_aprobacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id`, `tipo_proceso`, `id_habitacion`, `id_tarifa`, `id_cliente`, `id_apertura`, `precio`, `cobro_ext`, `cant_noche`, `dinero_dejado`, `id_tipo_pago`, `fecha_entrada`, `fecha_salida`, `total`, `id_usuario`, `cant_personas`, `id_caja`, `estado`, `fecha_creada`, `cantidad`, `observacion`, `pagado`, `nro_operacion`, `num_tarjeta`, `banco`, `num_aprobacion`) VALUES
(432, 1, 54, 71, 881, 131, 12000, 0, 1, 0, 1, '2022-02-17 15:03:09', '2022-02-18 12:00:00', 0, 4, 1, 131, 0, '2022-02-17 15:03:09', 1, NULL, 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_sueldo`
--

CREATE TABLE `proceso_sueldo` (
  `id` int(11) NOT NULL,
  `id_sueldo` int(11) DEFAULT NULL,
  `monto` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `tipo` int(11) NOT NULL DEFAULT 1,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_venta`
--

CREATE TABLE `proceso_venta` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_operacion` int(11) DEFAULT NULL,
  `id_venta` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso_venta`
--

INSERT INTO `proceso_venta` (`id`, `id_producto`, `id_operacion`, `id_venta`, `cantidad`, `precio`, `tipo_operacion`, `fecha_creada`, `id_caja`, `id_usuario`) VALUES
(463, 212, NULL, 379, 2, 5, 2, '2022-02-03 16:46:11', 130, 10),
(464, 214, NULL, 379, 1, 123, 2, '2022-02-03 16:46:11', 130, 10),
(465, 493, NULL, 380, 1, 1000, 2, '2022-02-03 16:54:03', 130, 10),
(466, 212, NULL, 381, 1, 150.35328, 1, '2022-02-03 17:03:46', 130, 10),
(467, 504, NULL, 382, 50, 6140.1375, 1, '2022-02-03 17:10:16', 130, 10),
(468, 212, NULL, 383, 1, 150.35328, 1, '2022-02-15 13:15:23', 131, 4),
(469, 538, NULL, 384, 1, 2684.88, 1, '2022-02-17 13:31:59', 131, 4),
(470, 538, NULL, 385, 1, 2684.88, 1, '2022-02-17 13:33:01', 131, 4),
(471, 538, NULL, 386, 1, 2684.88, 1, '2022-02-17 13:33:40', 131, 4),
(472, 548, NULL, 387, 1, 1275.318, 1, '2022-02-17 13:35:06', 131, 4),
(473, 538, NULL, 388, 1, 2684.88, 1, '2022-02-17 13:38:39', 131, 4),
(474, 538, NULL, 389, 1, 2684.88, 1, '2022-02-17 13:40:08', 131, 4),
(475, 548, NULL, 390, 1, 1275.318, 1, '2022-02-17 14:10:37', 131, 4),
(476, 538, NULL, 391, 1, 2684.88, 1, '2022-02-17 14:11:28', 131, 4),
(477, 538, NULL, 392, 1, 2684.88, 1, '2022-02-17 14:24:46', 131, 4),
(478, 538, NULL, 393, 1, 2684.88, 1, '2022-02-17 14:33:15', 131, 4),
(479, 538, NULL, 394, 1, 2684.88, 1, '2022-02-17 14:33:43', 131, 4),
(480, 554, NULL, 395, 1, 2711.7288, 1, '2022-02-17 14:34:19', 131, 4),
(481, 549, NULL, 396, 1, 5843.2752, 1, '2022-02-17 14:45:47', 131, 4),
(482, 548, NULL, 397, 1, 1275.318, 1, '2022-02-17 14:51:28', 131, 4),
(483, 538, NULL, 398, 1, 2684.88, 1, '2022-02-17 14:59:10', 131, 4),
(484, 538, NULL, 399, 1, 2684.88, 1, '2022-02-17 14:59:58', 131, 4),
(485, 538, NULL, 400, 1, 2684.88, 1, '2022-02-17 15:04:22', 131, 4),
(486, 559, NULL, 401, 1, 10739.52, 1, '2022-02-17 16:49:03', 131, 4),
(487, 548, NULL, 402, 1, 1275.318, 1, '2022-02-17 16:51:03', 131, 4),
(488, 549, NULL, 403, 1, 5843.2752, 1, '2022-02-17 17:05:40', 131, 4),
(489, 559, NULL, 404, 1, 10739.52, 1, '2022-02-18 14:56:44', 131, 4),
(490, 549, NULL, 405, 1, 5843.2752, 1, '2022-02-18 15:00:47', 131, 4),
(491, 548, NULL, 406, 1, 1275.318, 1, '2022-02-18 15:08:20', 131, 4),
(492, 554, NULL, 407, 1, 2711.7288, 1, '2022-02-18 15:10:27', 131, 4),
(493, 559, NULL, 408, 1, 10739.52, 1, '2022-02-18 15:12:36', 131, 4),
(494, 538, NULL, 409, 1, 2684.88, 1, '2022-02-18 15:20:56', 131, 4),
(495, 538, NULL, 410, 1, 2684.88, 1, '2022-02-18 15:23:15', 131, 4),
(496, 549, NULL, 411, 1, 5843.2752, 1, '2022-02-18 15:26:03', 131, 4),
(497, 549, NULL, 412, 1, 5843.2752, 1, '2022-02-18 15:31:58', 131, 4),
(498, 562, NULL, 413, 1, 5369.76, 1, '2022-02-18 15:37:21', 131, 4),
(499, 554, NULL, 414, 1, 2711.7288, 1, '2022-02-18 15:39:44', 131, 4),
(500, 548, NULL, 415, 1, 1275.318, 1, '2022-02-18 15:41:40', 131, 4),
(501, 559, NULL, 416, 1, 10739.52, 1, '2022-02-18 15:45:50', 131, 4),
(502, 554, NULL, 417, 1, 2711.7288, 1, '2022-02-18 15:48:19', 131, 4),
(503, 561, NULL, 417, 1, 9397.08, 1, '2022-02-18 15:48:19', 131, 4),
(504, 548, NULL, 418, 1, 1275.318, 1, '2022-02-18 15:53:14', 131, 4),
(505, 561, NULL, 419, 1, 9397.08, 1, '2022-02-18 15:54:26', 131, 4),
(506, 549, NULL, 420, 1, 5843.2752, 1, '2022-02-18 16:04:39', 131, 4),
(507, 549, NULL, 421, 1, 5843.2752, 1, '2022-02-18 16:30:34', 131, 4),
(508, 548, NULL, 421, 1, 1275.318, 1, '2022-02-18 16:30:34', 131, 4),
(509, 561, NULL, 422, 1, 9397.08, 1, '2022-02-18 16:31:53', 131, 4),
(510, 538, NULL, 423, 1, 2684.88, 1, '2022-02-18 16:32:29', 131, 4),
(511, 538, NULL, 424, 1, 2684.88, 1, '2022-02-18 16:33:35', 131, 4),
(512, 548, NULL, 425, 1, 1275.318, 1, '2022-02-18 16:34:10', 131, 4),
(513, 564, NULL, 426, 1, 2684.88, 1, '2022-02-18 20:25:59', 131, 4),
(514, 559, NULL, 427, 1, 10739.52, 1, '2022-02-18 20:32:30', 131, 4),
(515, 548, NULL, 428, 1, 1275.318, 1, '2022-02-19 08:55:21', 131, 4),
(516, 564, NULL, 429, 1, 2684.88, 1, '2022-02-19 08:56:08', 131, 4),
(517, 559, NULL, 430, 1, 10739.52, 1, '2022-02-19 08:58:49', 131, 4),
(518, 549, NULL, 431, 1, 5843.2752, 1, '2022-02-19 08:59:42', 131, 4),
(519, 538, NULL, 432, 1, 2684.88, 1, '2022-02-19 09:06:59', 131, 4),
(520, 549, NULL, 433, 1, 5843.2752, 1, '2022-02-19 09:10:35', 131, 4),
(521, 554, NULL, 434, 1, 2711.7288, 1, '2022-02-19 09:13:57', 131, 4),
(522, 554, NULL, 435, 1, 2711.7288, 1, '2022-02-19 09:16:03', 131, 4),
(523, 548, NULL, 436, 1, 1275.318, 1, '2022-02-19 09:19:35', 131, 4),
(524, 548, NULL, 437, 1, 1275.318, 1, '2022-02-19 09:26:14', 131, 4),
(525, 549, NULL, 438, 1, 5843.2752, 1, '2022-02-19 09:28:33', 131, 4),
(526, 559, NULL, 439, 1, 10739.52, 1, '2022-02-19 09:29:59', 131, 4),
(527, 548, NULL, 439, 1, 1275.318, 1, '2022-02-19 09:29:59', 131, 4),
(528, 548, NULL, 440, 1, 1275.318, 1, '2022-02-19 09:30:31', 131, 4),
(529, 548, NULL, 441, 1, 1275.318, 1, '2022-02-19 09:31:40', 131, 4),
(530, 538, NULL, 442, 1, 2684.88, 1, '2022-02-19 09:31:55', 131, 4),
(531, 538, NULL, 443, 1, 2684.88, 1, '2022-02-19 09:32:32', 131, 4),
(532, 561, NULL, 444, 1, 9397.08, 1, '2022-02-19 09:33:01', 131, 4),
(533, 538, NULL, 445, 1, 2684.88, 1, '2022-02-19 09:33:35', 131, 4),
(534, 548, NULL, 446, 1, 1275.318, 1, '2022-02-19 09:36:20', 131, 4),
(535, 548, NULL, 447, 1, 1275.318, 1, '2022-02-19 09:37:58', 131, 4),
(536, 554, NULL, 448, 1, 2711.7288, 1, '2022-02-19 09:42:52', 131, 4),
(537, 549, NULL, 449, 1, 5843.2752, 1, '2022-02-19 09:43:33', 131, 4),
(538, 559, NULL, 450, 1, 10739.52, 1, '2022-02-19 09:46:10', 131, 4),
(539, 549, NULL, 451, 1, 5843.2752, 1, '2022-02-19 09:52:22', 131, 4),
(540, 562, NULL, 452, 1, 5369.76, 1, '2022-02-19 09:52:33', 131, 4),
(541, 548, NULL, 453, 1, 1275.318, 1, '2022-02-19 09:54:36', 131, 4),
(542, 561, NULL, 454, 1, 9397.08, 1, '2022-02-19 09:55:40', 131, 4),
(543, 554, NULL, 455, 1, 2711.7288, 1, '2022-02-19 10:00:37', 131, 4),
(544, 554, NULL, 456, 1, 2711.7288, 1, '2022-02-19 10:10:17', 131, 4),
(545, 548, NULL, 457, 1, 1275.318, 1, '2022-02-19 10:17:26', 131, 4),
(546, 562, NULL, 458, 1, 5369.76, 1, '2022-02-19 10:26:22', 131, 4),
(547, 565, NULL, 459, 1, 2819.124, 1, '2022-02-19 10:28:42', 131, 4),
(548, 562, NULL, 460, 1, 5369.76, 1, '2022-02-19 10:30:39', 131, 4),
(549, 561, NULL, 461, 1, 9397.08, 1, '2022-02-19 10:34:16', 131, 4),
(550, 561, NULL, 462, 1, 9397.08, 1, '2022-02-19 10:35:39', 131, 4),
(551, 559, NULL, 463, 1, 10739.52, 1, '2022-02-19 10:38:54', 131, 4),
(552, 549, NULL, 464, 1, 5843.2752, 1, '2022-02-19 10:42:05', 131, 4),
(553, 561, NULL, 465, 1, 9397.08, 1, '2022-02-19 10:42:55', 131, 4),
(554, 548, NULL, 466, 1, 1275.318, 1, '2022-02-19 10:53:05', 131, 4),
(555, 561, NULL, 467, 1, 9397.08, 1, '2022-02-19 10:54:04', 131, 4),
(556, 561, NULL, 468, 1, 9397.08, 1, '2022-02-19 10:55:20', 131, 4),
(557, 549, NULL, 469, 1, 5843.2752, 1, '2022-02-19 10:57:53', 131, 4),
(558, 561, NULL, 470, 1, 9397.08, 1, '2022-02-19 11:08:00', 131, 4),
(559, 554, NULL, 471, 1, 2711.7288, 1, '2022-02-19 11:17:04', 131, 4),
(560, 548, NULL, 472, 1, 1275.318, 1, '2022-02-19 11:22:24', 131, 4),
(561, 538, NULL, 473, 1, 2684.88, 1, '2022-02-19 11:24:19', 131, 4),
(562, 564, NULL, 474, 1, 2684.88, 1, '2022-02-19 11:25:26', 131, 4),
(563, 559, NULL, 475, 1, 10739.52, 1, '2022-02-19 11:27:17', 131, 4),
(564, 554, NULL, 476, 1, 2711.7288, 1, '2022-02-19 11:28:01', 131, 4),
(565, 559, NULL, 477, 1, 10739.52, 1, '2022-02-19 11:40:31', 131, 4),
(566, 554, NULL, 477, 1, 2711.7288, 1, '2022-02-19 11:40:31', 131, 4),
(567, 538, NULL, 478, 1, 2684.88, 1, '2022-02-19 11:42:34', 131, 4),
(568, 554, NULL, 479, 1, 2711.7288, 1, '2022-02-19 11:45:07', 131, 4),
(569, 548, NULL, 480, 1, 1275.318, 1, '2022-02-19 11:46:27', 131, 4),
(570, 554, NULL, 481, 1, 2711.7288, 1, '2022-02-19 11:50:21', 131, 4),
(571, 554, NULL, 482, 1, 2711.7288, 1, '2022-02-19 11:51:43', 131, 4),
(572, 554, NULL, 483, 1, 2711.7288, 1, '2022-02-19 11:58:22', 131, 4),
(573, 548, NULL, 484, 1, 1275.318, 1, '2022-02-19 11:59:41', 131, 4),
(574, 548, NULL, 485, 1, 1275.318, 1, '2022-02-21 08:37:47', 131, 4),
(575, 548, NULL, 486, 1, 1275.318, 1, '2022-02-21 08:38:48', 131, 4),
(576, 554, NULL, 487, 1, 2711.7288, 1, '2022-02-21 08:40:27', 131, 4),
(577, 554, NULL, 488, 1, 2711.7288, 1, '2022-02-21 08:43:20', 131, 4),
(578, 538, NULL, 489, 1, 2684.88, 1, '2022-02-21 08:44:16', 131, 4),
(579, 548, NULL, 490, 1, 1275.318, 1, '2022-02-21 08:54:55', 131, 4),
(580, 538, NULL, 491, 1, 2684.88, 1, '2022-02-21 08:55:52', 131, 4),
(581, 549, NULL, 492, 1, 5843.2752, 1, '2022-02-21 08:56:54', 131, 4),
(582, 548, NULL, 492, 1, 1275.318, 1, '2022-02-21 08:56:54', 131, 4),
(583, 559, NULL, 493, 1, 10739.52, 1, '2022-02-21 08:57:02', 131, 4),
(584, 549, NULL, 494, 1, 5843.2752, 1, '2022-02-21 08:57:07', 131, 4),
(585, 549, NULL, 495, 1, 5843.2752, 1, '2022-02-21 11:07:55', 131, 4),
(586, 548, NULL, 496, 1, 1275.318, 1, '2022-02-21 11:09:06', 131, 4),
(587, 548, NULL, 497, 1, 1275.318, 1, '2022-02-21 17:13:49', 131, 4),
(588, 559, NULL, 498, 1, 10739.52, 1, '2022-02-21 17:15:20', 131, 4),
(589, 548, NULL, 499, 1, 1275.318, 1, '2022-02-22 09:25:33', 131, 4),
(590, 549, NULL, 500, 1, 5843.2752, 1, '2022-02-22 10:43:47', 131, 4),
(591, 548, NULL, 501, 1, 1275.318, 1, '2022-02-23 08:18:48', 131, 4),
(592, 554, NULL, 502, 1, 2711.7288, 1, '2022-02-23 08:21:22', 131, 4),
(593, 549, NULL, 503, 1, 5843.2752, 1, '2022-02-23 08:27:17', 131, 4),
(594, 554, NULL, 504, 1, 2711.7288, 1, '2022-02-23 08:46:46', 131, 4),
(595, 548, NULL, 505, 1, 1275.318, 1, '2022-02-23 08:49:26', 131, 4),
(596, 561, NULL, 506, 1, 9397.08, 1, '2022-02-23 08:50:43', 131, 4),
(597, 549, NULL, 507, 1, 5843.2752, 1, '2022-02-23 11:07:03', 131, 4),
(598, 561, NULL, 508, 1, 9397.08, 1, '2022-02-23 12:57:44', 131, 4),
(599, 562, NULL, 508, 1, 5369.76, 1, '2022-02-23 12:57:44', 131, 4),
(600, 538, NULL, 509, 1, 2684.88, 1, '2022-02-23 13:12:56', 131, 4),
(601, 559, NULL, 510, 1, 10739.52, 1, '2022-02-23 13:23:07', 131, 4),
(602, 538, NULL, 511, 2, 2684.88, 1, '2022-02-23 15:40:54', 131, 4),
(603, 538, NULL, 512, 1, 7.02295, 1, '2022-02-23 16:05:31', 131, 4),
(604, 538, NULL, 513, 1, 7.02295, 1, '2022-02-23 16:18:05', 131, 4),
(605, 538, NULL, 514, 1, 7.02295, 1, '2022-02-23 16:21:49', 131, 4),
(606, 538, NULL, 515, 1, 7.02295, 1, '2022-02-23 16:24:51', 131, 4),
(607, 538, NULL, 516, 1, 7.02295, 1, '2022-02-23 16:26:01', 131, 4),
(608, 538, NULL, 517, 1, 7.02295, 1, '2022-02-24 09:00:06', 131, 4),
(609, 538, NULL, 518, 1, 5.763, 1, '2022-02-24 10:13:40', 131, 4),
(610, 538, NULL, 519, 1, 5.763, 1, '2022-02-24 10:19:13', 131, 4),
(611, 538, NULL, 520, 1, 5.763, 1, '2022-02-24 10:21:51', 131, 4),
(612, 538, NULL, 521, 1, 5.763, 1, '2022-02-24 10:24:41', 131, 4),
(613, 538, NULL, 522, 1, 5.763, 1, '2022-02-24 10:42:55', 131, 4),
(614, 538, NULL, 523, 1, 5.763, 1, '2022-02-24 10:46:11', 131, 4),
(615, 538, NULL, 524, 1, 5.763, 1, '2022-02-24 10:51:55', 131, 4),
(616, 538, NULL, 525, 1, 5.763, 1, '2022-02-24 10:55:19', 131, 4),
(617, 538, NULL, 526, 1, 5.763, 1, '2022-02-24 10:59:20', 131, 4),
(618, 538, NULL, 527, 1, 5.763, 1, '2022-02-24 11:01:25', 131, 4),
(619, 538, NULL, 528, 1, 5.763, 1, '2022-02-24 11:13:39', 131, 4),
(620, 538, NULL, 529, 1, 5.763, 1, '2022-02-24 11:16:32', 131, 4),
(621, 538, NULL, 530, 1, 5.763, 1, '2022-02-24 11:19:20', 131, 4),
(622, 538, NULL, 531, 1, 5.763, 1, '2022-02-24 11:25:06', 131, 4),
(623, 538, NULL, 532, 1, 5.763, 1, '2022-02-24 11:26:11', 131, 4),
(624, 538, NULL, 533, 1, 5.763, 1, '2022-02-24 11:32:08', 131, 4),
(625, 538, NULL, 534, 1, 5.763, 1, '2022-02-24 11:32:45', 131, 4),
(626, 538, NULL, 535, 1, 5.763, 1, '2022-02-24 11:39:08', 131, 4),
(627, 538, NULL, 536, 1, 5.763, 1, '2022-02-24 11:40:23', 131, 4),
(628, 538, NULL, 537, 1, 5.763, 1, '2022-02-24 11:43:21', 131, 4),
(629, 538, NULL, 538, 1, 5.763, 1, '2022-02-24 11:47:33', 131, 4),
(630, 538, NULL, 539, 1, 5.763, 1, '2022-02-24 11:50:02', 131, 4),
(631, 554, NULL, 540, 1, 2711.7288, 1, '2022-02-24 11:52:52', 131, 4),
(632, 554, NULL, 541, 1, 2510.86, 1, '2022-02-24 11:53:55', 131, 4),
(633, 554, NULL, 542, 1, 2510.86, 1, '2022-02-24 11:59:14', 131, 4),
(634, 538, NULL, 543, 1, 5.763, 1, '2022-02-24 12:16:18', 131, 4),
(635, 554, NULL, 544, 1, 2510.86, 1, '2022-02-24 12:17:01', 131, 4),
(636, 549, NULL, 545, 1, 5843.2752, 1, '2022-03-04 09:38:54', 131, 4),
(637, 549, NULL, 546, 1, 5171.04, 1, '2022-03-04 10:36:40', 131, 4),
(638, 549, NULL, 547, 1, 5171.04, 1, '2022-03-04 10:37:25', 131, 4),
(639, 549, NULL, 548, 1, 5171.04, 1, '2022-03-04 10:43:39', 131, 4),
(640, 549, NULL, 549, 1, 5171.04, 1, '2022-03-04 10:45:47', 131, 4),
(641, 549, NULL, 550, 1, 5171.04, 1, '2022-03-04 10:49:00', 131, 4),
(642, 548, NULL, 551, 1, 1045, 1, '2022-03-04 10:52:35', 131, 4),
(643, 549, NULL, 552, 1, 5171.04, 1, '2022-03-04 10:52:58', 131, 4),
(644, 549, NULL, 553, 1, 5171.04, 1, '2022-03-04 10:54:09', 131, 4),
(645, 538, NULL, 554, 1, 2020, 1, '2022-03-04 12:52:15', 131, 4),
(646, 548, NULL, 555, 1, 1045, 1, '2022-03-04 14:09:35', 131, 4),
(647, 548, NULL, 556, 1, 1045, 1, '2022-03-04 14:15:06', 131, 4),
(648, 548, NULL, 557, 1, 1045, 1, '2022-03-04 14:18:54', 131, 4),
(649, 538, NULL, 558, 1, 2282.6, 1, '2022-03-04 17:44:56', 131, 4),
(650, 538, NULL, 559, 2, 2282.6, 1, '2022-03-04 18:13:50', 131, 4),
(651, 538, NULL, 560, 1, 2282.6, 1, '2022-03-04 18:17:11', 131, 4),
(652, 538, NULL, 561, 2, 2282.6, 1, '2022-03-04 18:23:38', 131, 4),
(653, 538, NULL, 562, 2, 2282.6, 1, '2022-03-04 18:29:11', 131, 4),
(654, 538, NULL, 563, 2, 2282.6, 1, '2022-03-04 18:30:28', 131, 4),
(655, 538, NULL, 564, 2, 2282.6, 1, '2022-03-04 18:31:46', 131, 4),
(656, 538, NULL, 565, 2, 2282.6, 1, '2022-03-04 18:34:24', 131, 4),
(657, 538, NULL, 566, 2, 2282.6, 1, '2022-03-07 12:44:35', 131, 4),
(658, 538, NULL, 567, 2, 2282.6, 1, '2022-03-07 13:25:16', 131, 4),
(659, 538, NULL, 568, 2, 2282.6, 1, '2022-03-07 13:27:50', 131, 4),
(660, 538, NULL, 569, 2, 2282.6, 1, '2022-03-07 13:33:01', 131, 4),
(661, 538, NULL, 570, 2, 2282.6, 1, '2022-03-07 13:35:17', 131, 4),
(662, 538, NULL, 571, 4, 2282.6, 1, '2022-03-07 13:36:42', 131, 4),
(663, 538, NULL, 572, 2, 2282.6, 1, '2022-03-07 13:41:01', 131, 4),
(664, 538, NULL, 573, 2, 2282.6, 1, '2022-03-07 13:59:59', 131, 4),
(665, 538, NULL, 574, 2, 2282.6, 1, '2022-03-07 14:02:33', 131, 4),
(666, 538, NULL, 575, 2, 2282.6, 1, '2022-03-07 14:03:27', 131, 4),
(667, 538, NULL, 576, 2, 2282.6, 1, '2022-03-07 14:04:05', 131, 4),
(668, 538, NULL, 577, 2, 2282.6, 1, '2022-03-07 14:08:23', 131, 4),
(669, 538, NULL, 578, 2, 2282.6, 1, '2022-03-07 14:09:25', 131, 4),
(670, 538, NULL, 579, 100, 2, 2, '2022-03-07 14:10:54', 131, 4),
(671, 538, NULL, 580, 3, 2282.6, 1, '2022-03-07 14:11:14', 131, 4),
(672, 538, NULL, 581, 1, 2282.6, 1, '2022-03-07 14:11:59', 131, 4),
(673, 538, NULL, 582, 1, 2282.6, 1, '2022-03-07 14:13:08', 131, 4),
(674, 548, NULL, 583, 1, 1045, 1, '2022-03-07 14:16:40', 131, 4),
(675, 538, NULL, 584, 1, 2282.6, 1, '2022-03-07 14:17:14', 131, 4),
(676, 562, NULL, 585, 1, 4752, 1, '2022-03-07 14:18:31', 131, 4),
(677, 554, NULL, 586, 1, 2222, 1, '2022-03-07 14:18:55', 131, 4),
(678, 538, NULL, 587, 1, 2282.6, 1, '2022-03-07 14:19:47', 131, 4),
(679, 554, NULL, 588, 1, 2222, 1, '2022-03-07 14:23:55', 131, 4),
(680, 562, NULL, 589, 1, 4752, 1, '2022-03-07 14:24:24', 131, 4),
(681, 538, NULL, 590, 1, 2282.6, 1, '2022-03-07 14:25:19', 131, 4),
(682, 538, NULL, 591, 1, 2282.6, 1, '2022-03-07 14:38:39', 131, 4),
(683, 538, NULL, 592, 1, 2282.6, 1, '2022-03-07 14:41:35', 131, 4),
(684, 538, NULL, 593, 1, 2282.6, 1, '2022-03-07 14:46:05', 131, 4),
(685, 538, NULL, 594, 2, 2282.6, 1, '2022-03-07 14:47:03', 131, 4),
(686, 538, NULL, 595, 1, 2282.6, 1, '2022-03-07 14:49:50', 131, 4),
(687, 538, NULL, 596, 1, 2282.6, 1, '2022-03-07 14:50:40', 131, 4),
(688, 538, NULL, 597, 1, 2282.6, 1, '2022-03-07 14:52:00', 131, 4),
(689, 538, NULL, 598, 2, 2282.6, 1, '2022-03-07 14:52:52', 131, 4),
(690, 538, NULL, 599, 1, 2282.6, 1, '2022-03-07 14:53:34', 131, 4),
(691, 538, NULL, 600, 1, 2282.6, 1, '2022-03-07 14:54:13', 131, 4),
(692, 538, NULL, 601, 2, 2282.6, 1, '2022-03-07 14:56:54', 131, 4),
(693, 538, NULL, 602, 1, 2282.6, 1, '2022-03-07 14:58:54', 131, 4),
(694, 538, NULL, 603, 1, 2282.6, 1, '2022-03-07 14:59:37', 131, 4),
(695, 538, NULL, 604, 1, 2282.6, 1, '2022-03-07 15:02:22', 131, 4),
(696, 538, NULL, 605, 1, 2282.6, 1, '2022-03-07 15:05:01', 131, 4),
(697, 538, NULL, 606, 1, 2282.6, 1, '2022-03-07 15:08:32', 131, 4),
(698, 538, NULL, 607, 1, 2282.6, 1, '2022-03-07 15:09:11', 131, 4),
(699, 538, NULL, 608, 1, 2282.6, 1, '2022-03-07 15:11:59', 131, 4),
(700, 538, NULL, 609, 1, 2282.6, 1, '2022-03-07 15:13:20', 131, 4),
(701, 538, NULL, 610, 1, 2282.6, 1, '2022-03-07 15:14:29', 131, 4),
(702, 538, NULL, 611, 1, 2282.6, 1, '2022-03-07 15:15:08', 131, 4),
(703, 538, NULL, 612, 1, 2282.6, 1, '2022-03-07 15:15:21', 131, 4),
(704, 538, NULL, 613, 2, 2282.6, 1, '2022-03-07 15:36:10', 131, 4),
(705, 538, NULL, 614, 1, 2282.6, 1, '2022-03-07 15:38:59', 131, 4),
(706, 538, NULL, 615, 2, 2282.6, 1, '2022-03-07 15:39:22', 131, 4),
(707, 538, NULL, 616, 2, 2282.6, 1, '2022-03-07 15:55:37', 131, 4),
(708, 538, NULL, 617, 2, 2282.6, 1, '2022-03-07 16:00:07', 131, 4),
(709, 538, NULL, 618, 2, 2282.6, 1, '2022-03-07 16:16:23', 131, 4),
(710, 538, NULL, 619, 2, 2282.6, 1, '2022-03-07 16:19:01', 131, 4),
(711, 538, NULL, 620, 2, 2282.6, 1, '2022-03-07 16:40:33', 131, 4),
(712, 538, NULL, 621, 2, 2282.6, 1, '2022-03-07 16:41:34', 131, 4),
(713, 538, NULL, 622, 2, 2282.6, 1, '2022-03-08 08:11:43', 131, 4),
(714, 548, NULL, 623, 1, 1100, 1, '2022-03-08 08:26:29', 131, 4),
(715, 548, NULL, 624, 1, 1100, 1, '2022-03-08 08:27:31', 131, 4),
(716, 548, NULL, 625, 1, 1100, 1, '2022-03-08 08:28:14', 131, 4),
(717, 548, NULL, 626, 1, 1100, 1, '2022-03-08 08:29:57', 131, 4),
(718, 548, NULL, 627, 1, 1100, 1, '2022-03-08 08:31:08', 131, 4),
(719, 548, NULL, 628, 1, 1100, 1, '2022-03-08 08:37:30', 131, 4),
(720, 538, NULL, 629, 2, 2282.6, 1, '2022-03-08 08:37:57', 131, 4),
(721, 548, NULL, 630, 1, 1045, 1, '2022-03-08 08:52:04', 131, 4),
(722, 554, NULL, 630, 1, 2053.33, 1, '2022-03-08 08:52:04', 131, 4),
(723, 548, NULL, 631, 1, 1045, 1, '2022-03-08 09:06:39', 131, 4),
(724, 548, NULL, 632, 2, 1045, 1, '2022-03-08 09:07:11', 131, 4),
(725, 554, NULL, 633, 1, 2053.33, 1, '2022-03-08 09:07:46', 131, 4),
(726, 548, NULL, 634, 1, 1045, 1, '2022-03-08 09:08:12', 131, 4),
(727, 554, NULL, 634, 1, 2053.33, 1, '2022-03-08 09:08:12', 131, 4),
(728, 554, NULL, 635, 1, 2053.33, 1, '2022-03-08 09:25:24', 131, 4),
(729, 554, NULL, 636, 1, 2053.33, 1, '2022-03-08 09:25:56', 131, 4),
(730, 554, NULL, 637, 1, 2053.33, 1, '2022-03-08 09:26:50', 131, 4),
(731, 548, NULL, 638, 1, 1045, 1, '2022-03-08 09:27:14', 131, 4),
(732, 554, NULL, 638, 1, 2053.33, 1, '2022-03-08 09:27:14', 131, 4),
(733, 548, NULL, 639, 1, 1045, 1, '2022-03-08 09:28:04', 131, 4),
(734, 554, NULL, 639, 1, 2053.33, 1, '2022-03-08 09:28:04', 131, 4),
(735, 554, NULL, 640, 1, 2053.33, 1, '2022-03-08 09:29:26', 131, 4),
(736, 548, NULL, 640, 1, 1045, 1, '2022-03-08 09:29:26', 131, 4),
(737, 554, NULL, 641, 1, 2053.33, 1, '2022-03-08 09:35:35', 131, 4),
(738, 538, NULL, 642, 1, 2282.6, 1, '2022-03-08 09:40:03', 131, 4),
(739, 538, NULL, 643, 1, 2282.6, 1, '2022-03-08 09:43:57', 131, 4),
(740, 538, NULL, 644, 1, 2282.6, 1, '2022-03-08 09:44:53', 131, 4),
(741, 538, NULL, 645, 2, 2282.6, 1, '2022-03-08 09:46:55', 131, 4),
(742, 565, NULL, 646, 1, 2310, 1, '2022-03-08 09:51:05', 131, 4),
(743, 565, NULL, 647, 2, 2310, 1, '2022-03-08 09:51:26', 131, 4),
(744, 554, NULL, 648, 1, 2053.33, 1, '2022-03-08 09:52:04', 131, 4),
(745, 554, NULL, 649, 1, 2053.33, 1, '2022-03-08 10:02:31', 131, 4),
(746, 554, NULL, 650, 2, 2053.33, 1, '2022-03-08 10:05:27', 131, 4),
(747, 554, NULL, 651, 1, 2053.33, 1, '2022-03-08 10:07:38', 131, 4),
(748, 548, NULL, 651, 1, 1045, 1, '2022-03-08 10:07:38', 131, 4),
(749, 554, NULL, 652, 1, 2053.33, 1, '2022-03-08 10:16:38', 131, 4),
(750, 554, NULL, 653, 2, 2053.33, 1, '2022-03-08 10:18:44', 131, 4),
(751, 554, NULL, 654, 1, 2053.33, 1, '2022-03-08 10:19:48', 131, 4),
(752, 548, NULL, 654, 1, 1045, 1, '2022-03-08 10:19:48', 131, 4),
(753, 548, NULL, 655, 1, 1045, 1, '2022-03-08 10:23:21', 131, 4),
(754, 554, NULL, 655, 1, 2053.33, 1, '2022-03-08 10:23:21', 131, 4),
(755, 548, NULL, 656, 1, 1045, 1, '2022-03-08 10:27:51', 131, 4),
(756, 554, NULL, 656, 1, 2053.33, 1, '2022-03-08 10:27:51', 131, 4),
(757, 548, NULL, 657, 1, 1045, 1, '2022-03-08 10:31:37', 131, 4),
(758, 554, NULL, 657, 1, 2053.33, 1, '2022-03-08 10:31:37', 131, 4),
(759, 548, NULL, 658, 1, 1045, 1, '2022-03-08 10:32:18', 131, 4),
(760, 554, NULL, 658, 1, 2053.33, 1, '2022-03-08 10:32:18', 131, 4),
(761, 548, NULL, 659, 1, 1045, 1, '2022-03-08 10:34:49', 131, 4),
(762, 554, NULL, 659, 1, 2053.33, 1, '2022-03-08 10:34:49', 131, 4),
(763, 565, NULL, 660, 1, 2310, 1, '2022-03-08 11:01:24', 131, 4),
(764, 565, NULL, 661, 1, 2310, 1, '2022-03-08 11:10:43', 131, 4),
(765, 565, NULL, 662, 1, 2310, 1, '2022-03-08 11:11:31', 131, 4),
(766, 565, NULL, 663, 1, 2310, 1, '2022-03-08 11:12:31', 131, 4),
(767, 565, NULL, 664, 1, 2310, 1, '2022-03-08 11:13:13', 131, 4),
(768, 565, NULL, 665, 1, 2310, 1, '2022-03-08 11:17:07', 131, 4),
(769, 565, NULL, 666, 1, 2310, 1, '2022-03-08 11:18:55', 131, 4),
(770, 565, NULL, 667, 1, 2310, 1, '2022-03-08 11:25:36', 131, 4),
(771, 565, NULL, 668, 1, 2310, 1, '2022-03-08 11:26:40', 131, 4),
(772, 565, NULL, 669, 1, 2310, 1, '2022-03-08 11:27:25', 131, 4),
(773, 565, NULL, 670, 1, 2310, 1, '2022-03-08 11:31:01', 131, 4),
(774, 538, NULL, 671, 1, 2282.6, 1, '2022-03-08 22:09:24', 131, 4),
(775, 538, NULL, 672, 1, 2020, 1, '2022-03-08 22:19:32', 131, 4),
(776, 538, NULL, 673, 1, 2020, 1, '2022-03-08 22:21:56', 131, 4),
(777, 538, NULL, 674, 1, 2020, 1, '2022-03-08 22:22:40', 131, 4),
(778, 548, NULL, 675, 1, 1243, 1, '2022-03-08 22:26:50', 131, 4),
(779, 548, NULL, 676, 1, 1243, 1, '2022-03-08 22:29:11', 131, 4),
(780, 549, NULL, 677, 1, 4788, 1, '2022-03-08 22:42:30', 131, 4),
(781, 549, NULL, 678, 1, 4788, 1, '2022-03-08 23:04:02', 131, 4),
(782, 548, NULL, 679, 1, 1243, 1, '2022-03-08 23:06:47', 131, 4),
(783, 549, NULL, 679, 1, 4788, 1, '2022-03-08 23:06:47', 131, 4),
(784, 538, NULL, 680, 1, 2020, 1, '2022-03-08 23:09:37', 131, 4),
(785, 548, NULL, 681, 1, 1243, 1, '2022-03-08 23:11:42', 131, 4),
(786, 548, NULL, 682, 1, 1243, 1, '2022-03-08 23:12:14', 131, 4),
(787, 549, NULL, 682, 1, 4788, 1, '2022-03-08 23:12:14', 131, 4),
(788, 548, NULL, 683, 1, 1243, 1, '2022-03-08 23:13:48', 131, 4),
(789, 549, NULL, 683, 1, 4788, 1, '2022-03-08 23:13:48', 131, 4),
(790, 548, NULL, 684, 1, 1243, 1, '2022-03-08 23:14:08', 131, 4),
(791, 549, NULL, 684, 1, 4788, 1, '2022-03-08 23:14:08', 131, 4),
(792, 548, NULL, 685, 1, 1243, 1, '2022-03-08 23:52:08', 131, 4),
(793, 549, NULL, 685, 1, 4788, 1, '2022-03-08 23:52:08', 131, 4),
(794, 548, NULL, 686, 1, 1243, 1, '2022-03-08 23:56:58', 131, 4),
(795, 549, NULL, 686, 1, 4788, 1, '2022-03-08 23:56:58', 131, 4),
(796, 548, NULL, 687, 1, 1243, 1, '2022-03-09 00:01:13', 131, 4),
(797, 549, NULL, 687, 1, 4788, 1, '2022-03-09 00:01:13', 131, 4),
(798, 548, NULL, 688, 1, 1243, 1, '2022-03-09 00:01:53', 131, 4),
(799, 549, NULL, 688, 1, 4788, 1, '2022-03-09 00:01:53', 131, 4),
(800, 548, NULL, 689, 1, 1243, 1, '2022-03-09 00:04:21', 131, 4),
(801, 549, NULL, 689, 1, 4788, 1, '2022-03-09 00:04:21', 131, 4),
(802, 548, NULL, 690, 1, 1243, 1, '2022-03-09 00:05:25', 131, 4),
(803, 549, NULL, 690, 1, 4788, 1, '2022-03-09 00:05:25', 131, 4),
(804, 548, NULL, 691, 1, 1243, 1, '2022-03-09 00:06:04', 131, 4),
(805, 549, NULL, 691, 1, 4788, 1, '2022-03-09 00:06:04', 131, 4),
(806, 548, NULL, 692, 1, 1243, 1, '2022-03-09 00:06:46', 131, 4),
(807, 549, NULL, 692, 1, 4788, 1, '2022-03-09 00:06:46', 131, 4),
(808, 548, NULL, 693, 1, 1243, 1, '2022-03-09 00:20:32', 131, 4),
(809, 549, NULL, 693, 1, 4788, 1, '2022-03-09 00:20:32', 131, 4),
(810, 548, NULL, 694, 1, 1243, 1, '2022-03-09 08:06:47', 131, 4),
(811, 549, NULL, 694, 1, 4788, 1, '2022-03-09 08:06:47', 131, 4),
(812, 548, NULL, 695, 1, 1243, 1, '2022-03-09 08:09:52', 131, 4),
(813, 549, NULL, 695, 1, 4788, 1, '2022-03-09 08:09:52', 131, 4),
(814, 548, NULL, 696, 1, 1243, 1, '2022-03-09 08:16:30', 131, 4),
(815, 549, NULL, 696, 1, 4788, 1, '2022-03-09 08:16:30', 131, 4),
(816, 548, NULL, 697, 1, 1243, 1, '2022-03-09 08:18:31', 131, 4),
(817, 549, NULL, 697, 1, 4788, 1, '2022-03-09 08:18:31', 131, 4),
(818, 548, NULL, 698, 1, 1243, 1, '2022-03-09 08:27:05', 131, 4),
(819, 549, NULL, 698, 1, 4788, 1, '2022-03-09 08:27:05', 131, 4),
(820, 548, NULL, 699, 1, 1243, 1, '2022-03-09 08:36:29', 131, 4),
(821, 549, NULL, 699, 1, 4788, 1, '2022-03-09 08:36:29', 131, 4),
(822, 548, NULL, 700, 1, 1243, 1, '2022-03-09 08:38:10', 131, 4),
(823, 549, NULL, 700, 1, 4788, 1, '2022-03-09 08:38:10', 131, 4),
(824, 548, NULL, 701, 1, 1243, 1, '2022-03-09 08:39:59', 131, 4),
(825, 549, NULL, 701, 1, 4788, 1, '2022-03-09 08:39:59', 131, 4),
(826, 548, NULL, 702, 1, 1243, 1, '2022-03-09 08:44:08', 131, 4),
(827, 549, NULL, 702, 1, 4788, 1, '2022-03-09 08:44:08', 131, 4),
(828, 548, NULL, 703, 1, 1243, 1, '2022-03-09 08:47:53', 131, 4),
(829, 549, NULL, 703, 1, 4788, 1, '2022-03-09 08:47:53', 131, 4),
(830, 549, NULL, 704, 1, 4788, 1, '2022-03-09 08:48:20', 131, 4),
(831, 548, NULL, 704, 1, 1243, 1, '2022-03-09 08:48:20', 131, 4),
(832, 548, NULL, 705, 1, 1243, 1, '2022-03-09 08:49:42', 131, 4),
(833, 549, NULL, 705, 1, 4788, 1, '2022-03-09 08:49:42', 131, 4),
(834, 548, NULL, 706, 1, 1243, 1, '2022-03-09 08:50:08', 131, 4),
(835, 549, NULL, 706, 1, 4788, 1, '2022-03-09 08:50:08', 131, 4),
(836, 548, NULL, 707, 1, 1243, 1, '2022-03-09 08:50:45', 131, 4),
(837, 549, NULL, 707, 1, 4788, 1, '2022-03-09 08:50:45', 131, 4),
(838, 548, NULL, 708, 1, 1243, 1, '2022-03-09 08:55:31', 131, 4),
(839, 549, NULL, 708, 1, 4788, 1, '2022-03-09 08:55:31', 131, 4),
(840, 548, NULL, 709, 1, 1243, 1, '2022-03-09 09:02:21', 131, 4),
(841, 549, NULL, 709, 1, 4788, 1, '2022-03-09 09:02:21', 131, 4),
(842, 548, NULL, 710, 1, 1243, 1, '2022-03-09 09:02:37', 131, 4),
(843, 549, NULL, 710, 1, 4788, 1, '2022-03-09 09:02:37', 131, 4),
(844, 548, NULL, 711, 1, 1243, 1, '2022-03-09 09:05:02', 131, 4),
(845, 549, NULL, 712, 1, 4788, 1, '2022-03-09 09:05:26', 131, 4),
(846, 549, NULL, 713, 2, 4788, 1, '2022-03-09 09:07:51', 131, 4),
(847, 548, NULL, 714, 2, 1243, 1, '2022-03-09 09:08:30', 131, 4),
(848, 548, NULL, 715, 1, 1243, 1, '2022-03-09 09:09:25', 131, 4),
(849, 549, NULL, 715, 1, 4788, 1, '2022-03-09 09:09:25', 131, 4),
(850, 548, NULL, 716, 2, 1243, 1, '2022-03-09 09:10:54', 131, 4),
(851, 549, NULL, 716, 1, 4788, 1, '2022-03-09 09:10:54', 131, 4),
(852, 548, NULL, 717, 100, 500, 2, '2022-03-09 09:18:42', 131, 4),
(853, 554, NULL, 718, 1, 2053.33, 1, '2022-03-09 09:19:24', 131, 4),
(854, 554, NULL, 719, 1, 2320.2629, 1, '2022-03-09 09:46:53', 131, 4),
(855, 554, NULL, 720, 1, 2320.2629, 1, '2022-03-09 10:00:30', 131, 4),
(856, 554, NULL, 721, 1, 2320.2629, 1, '2022-03-09 10:48:43', 131, 4),
(857, 554, NULL, 722, 1, 2320.2629, 1, '2022-03-09 10:59:30', 131, 4),
(858, 554, NULL, 723, 1, 2320.2629, 1, '2022-03-09 11:01:33', 131, 4),
(859, 554, NULL, 724, 1, 2320.2629, 1, '2022-03-09 11:03:56', 131, 4),
(860, 554, NULL, 725, 1, 2320.2629, 1, '2022-03-09 11:12:00', 131, 4),
(861, 554, NULL, 726, 1, 2320.2629, 1, '2022-03-09 11:13:07', 131, 4),
(862, 554, NULL, 727, 1, 2320.2629, 1, '2022-03-09 11:14:16', 131, 4),
(863, 554, NULL, 728, 1, 2320.2629, 1, '2022-03-09 11:14:45', 131, 4),
(864, 554, NULL, 729, 1, 2320.2629, 1, '2022-03-09 11:22:29', 131, 4),
(865, 554, NULL, 730, 1, 2320.2629, 1, '2022-03-09 11:26:52', 131, 4),
(866, 548, NULL, 731, 1, 1243, 1, '2022-03-09 11:37:11', 131, 4),
(867, 548, NULL, 732, 1, 1243, 1, '2022-03-09 11:39:46', 131, 4),
(868, 548, NULL, 733, 1, 1243, 1, '2022-03-09 11:40:23', 131, 4),
(869, 548, NULL, 734, 1, 1243, 1, '2022-03-09 11:41:42', 131, 4),
(870, 548, NULL, 735, 1, 1243, 1, '2022-03-09 11:43:01', 131, 4),
(871, 548, NULL, 736, 1, 1243, 1, '2022-03-09 11:48:05', 131, 4),
(872, 548, NULL, 737, 1, 1243, 1, '2022-03-09 11:52:41', 131, 4),
(873, 548, NULL, 738, 1, 1243, 1, '2022-03-09 11:53:24', 131, 4),
(874, 548, NULL, 739, 1, 1243, 1, '2022-03-09 11:54:03', 131, 4),
(875, 548, NULL, 740, 1, 1243, 1, '2022-03-09 11:54:47', 131, 4),
(876, 548, NULL, 741, 1, 1243, 1, '2022-03-09 11:56:18', 131, 4),
(877, 548, NULL, 742, 1, 1243, 1, '2022-03-09 12:00:53', 131, 4),
(878, 548, NULL, 743, 1, 1243, 1, '2022-03-09 12:02:03', 131, 4),
(879, 548, NULL, 744, 1, 1243, 1, '2022-03-09 12:05:40', 131, 4),
(880, 548, NULL, 745, 1, 1243, 1, '2022-03-09 12:06:00', 131, 4),
(881, 548, NULL, 746, 1, 1243, 1, '2022-03-09 12:07:11', 131, 4),
(882, 548, NULL, 747, 1, 1243, 1, '2022-03-09 12:11:50', 131, 4),
(883, 549, NULL, 747, 1, 4788, 1, '2022-03-09 12:11:50', 131, 4),
(884, 548, NULL, 748, 1, 1243, 1, '2022-03-09 12:12:36', 131, 4),
(885, 549, NULL, 748, 1, 4788, 1, '2022-03-09 12:12:36', 131, 4),
(886, 554, NULL, 749, 1, 2320.2629, 1, '2022-03-09 12:13:31', 131, 4),
(887, 554, NULL, 750, 1, 2320.2629, 1, '2022-03-09 12:18:56', 131, 4),
(888, 554, NULL, 751, 1, 2320.2629, 1, '2022-03-09 12:40:45', 131, 4),
(889, 554, NULL, 752, 1, 2320.2629, 1, '2022-03-09 12:41:41', 131, 4),
(890, 554, NULL, 753, 1, 2320.2629, 1, '2022-03-09 12:43:05', 131, 4),
(891, 554, NULL, 754, 1, 2320.2629, 1, '2022-03-09 12:53:59', 131, 4),
(892, 538, NULL, 755, 1, 2020, 1, '2022-03-09 12:55:39', 131, 4),
(893, 548, NULL, 756, 1, 1243, 1, '2022-03-09 12:57:02', 131, 4),
(894, 548, NULL, 757, 2, 1243, 1, '2022-03-09 21:06:00', 131, 4),
(895, 549, NULL, 757, 2, 4788, 1, '2022-03-09 21:06:00', 131, 4),
(896, 548, NULL, 758, 1, 1243, 1, '2022-03-09 21:46:52', 131, 4),
(897, 549, NULL, 758, 1, 4788, 1, '2022-03-09 21:46:52', 131, 4),
(898, 548, NULL, 759, 2, 1243, 1, '2022-03-09 23:09:05', 131, 4),
(899, 549, NULL, 759, 2, 4788, 1, '2022-03-09 23:09:05', 131, 4),
(900, 548, NULL, 760, 2, 1243, 1, '2022-03-09 23:19:09', 131, 4),
(901, 549, NULL, 760, 2, 4788, 1, '2022-03-09 23:19:09', 131, 4),
(902, 548, NULL, 761, 2, 1243, 1, '2022-03-09 23:22:49', 131, 4),
(903, 549, NULL, 761, 2, 4788, 1, '2022-03-09 23:22:49', 131, 4),
(904, 548, NULL, 762, 1, 1243, 1, '2022-03-09 23:23:21', 131, 4),
(905, 549, NULL, 762, 1, 4788, 1, '2022-03-09 23:23:21', 131, 4),
(906, 554, NULL, 763, 1, 2320.2629, 1, '2022-03-09 23:24:15', 131, 4),
(907, 554, NULL, 764, 2, 2320.2629, 1, '2022-03-09 23:25:09', 131, 4),
(908, 538, NULL, 765, 1, 2020, 1, '2022-03-11 12:14:26', 131, 4),
(909, 559, NULL, 765, 1, 9504, 1, '2022-03-11 12:14:26', 131, 4),
(910, 538, NULL, 766, 1, 2020, 1, '2022-03-11 12:19:33', 131, 4),
(911, 559, NULL, 766, 1, 8800, 1, '2022-03-11 12:19:33', 131, 4),
(912, 538, NULL, 767, 2, 2020, 1, '2022-03-11 12:25:43', 131, 4),
(913, 559, NULL, 767, 2, 8800, 1, '2022-03-11 12:25:43', 131, 4),
(914, 538, NULL, 768, 2, 2275.82, 1, '2022-03-11 12:35:50', 131, 4),
(915, 538, NULL, 769, 1, 2014, 1, '2022-03-11 12:39:14', 131, 4),
(916, 548, NULL, 769, 1, 1003.2, 1, '2022-03-11 12:39:14', 131, 4),
(917, 538, NULL, 770, 1, 2014, 1, '2022-03-11 13:01:01', 131, 4),
(918, 548, NULL, 770, 1, 1003.2, 1, '2022-03-11 13:01:01', 131, 4),
(919, 538, NULL, 771, 2, 2014, 1, '2022-03-11 13:11:55', 131, 4),
(920, 548, NULL, 771, 1, 1003.2, 1, '2022-03-11 13:11:55', 131, 4),
(921, 538, NULL, 772, 1, 2014, 1, '2022-03-11 13:20:33', 131, 4),
(922, 538, NULL, 773, 1, 2014, 1, '2022-03-11 13:25:15', 131, 4),
(923, 538, NULL, 774, 1, 2014, 1, '2022-03-11 13:25:49', 131, 4),
(924, 548, NULL, 774, 1, 1003.2, 1, '2022-03-11 13:25:49', 131, 4),
(925, 538, NULL, 775, 2, 2014, 1, '2022-03-11 13:26:16', 131, 4),
(926, 538, NULL, 776, 2, 2014, 1, '2022-03-11 13:26:49', 131, 4),
(927, 548, NULL, 776, 2, 1003.2, 1, '2022-03-11 13:26:49', 131, 4),
(928, 538, NULL, 777, 2, 2014, 1, '2022-03-11 15:17:45', 131, 4),
(929, 548, NULL, 777, 2, 1003.2, 1, '2022-03-11 15:17:45', 131, 4),
(930, 538, NULL, 778, 1, 2395.6, 1, '2022-03-11 15:19:59', 131, 4),
(931, 538, NULL, 779, 2, 2395.6, 1, '2022-03-11 15:22:56', 131, 4),
(932, 538, NULL, 780, 2, 2395.6, 1, '2022-03-11 15:23:35', 131, 4),
(933, 548, NULL, 780, 2, 1288.2, 1, '2022-03-11 15:23:35', 131, 4),
(934, 538, NULL, 781, 1, 2395.6, 1, '2022-03-11 15:28:06', 131, 4),
(935, 548, NULL, 781, 1, 1288.2, 1, '2022-03-11 15:28:06', 131, 4),
(936, 538, NULL, 782, 1, 2395.6, 1, '2022-03-11 15:32:45', 131, 4),
(937, 538, NULL, 783, 2, 2395.6, 1, '2022-03-11 15:33:38', 131, 4),
(938, 538, NULL, 784, 1, 2395.6, 1, '2022-03-11 15:39:25', 131, 4),
(939, 548, NULL, 784, 1, 1288.2, 1, '2022-03-11 15:39:25', 131, 4),
(940, 538, NULL, 785, 1, 2395.6, 1, '2022-03-11 15:42:17', 131, 4),
(941, 548, NULL, 785, 1, 1288.2, 1, '2022-03-11 15:42:17', 131, 4),
(942, 538, NULL, 786, 1, 2395.6, 1, '2022-03-11 15:44:21', 131, 4),
(943, 548, NULL, 786, 1, 1288.2, 1, '2022-03-11 15:44:21', 131, 4),
(944, 538, NULL, 787, 1, 2395.6, 1, '2022-03-11 15:47:04', 131, 4),
(945, 548, NULL, 788, 1, 1288.2, 1, '2022-03-11 15:47:36', 131, 4),
(946, 548, NULL, 789, 1, 1288.2, 1, '2022-03-11 15:49:04', 131, 4),
(947, 548, NULL, 790, 1, 1288.2, 1, '2022-03-11 15:49:49', 131, 4),
(948, 548, NULL, 791, 1, 1288.2, 1, '2022-03-11 15:57:18', 131, 4),
(949, 548, NULL, 792, 1, 1288.2, 1, '2022-03-11 16:10:46', 131, 4),
(950, 548, NULL, 793, 2, 1288.2, 1, '2022-03-11 16:39:15', 131, 4),
(951, 538, NULL, 794, 1, 2395.6, 1, '2022-03-11 16:39:55', 131, 4),
(952, 548, NULL, 794, 1, 1288.2, 1, '2022-03-11 16:39:55', 131, 4),
(953, 538, NULL, 795, 2, 2395.6, 1, '2022-03-11 16:40:29', 131, 4),
(954, 548, NULL, 795, 2, 1288.2, 1, '2022-03-11 16:40:29', 131, 4),
(955, 538, NULL, 796, 101, 2, 2, '2022-03-11 16:44:32', 131, 4),
(956, 538, NULL, 797, 1, 2014, 1, '2022-03-11 16:45:30', 131, 4),
(957, 548, NULL, 797, 1, 1083, 1, '2022-03-11 16:45:30', 131, 4),
(958, 549, NULL, 797, 1, 4788, 1, '2022-03-11 16:45:30', 131, 4),
(959, 538, NULL, 798, 2, 2014, 1, '2022-03-11 16:47:44', 131, 4),
(960, 548, NULL, 798, 1, 1083, 1, '2022-03-11 16:47:44', 131, 4),
(961, 549, NULL, 798, 3, 4788, 1, '2022-03-11 16:47:44', 131, 4),
(962, 538, NULL, 799, 1, 2014, 1, '2022-03-11 16:50:37', 131, 4),
(963, 548, NULL, 799, 1, 1083, 1, '2022-03-11 16:50:37', 131, 4),
(964, 549, NULL, 799, 1, 4788, 1, '2022-03-11 16:50:37', 131, 4),
(965, 538, NULL, 800, 2, 2395.6, 1, '2022-03-11 16:51:16', 131, 4),
(966, 548, NULL, 800, 3, 1288.2, 1, '2022-03-11 16:51:16', 131, 4),
(967, 549, NULL, 800, 5, 5695.2, 1, '2022-03-11 16:51:16', 131, 4),
(968, 538, NULL, 801, 1, 2395.6, 1, '2022-03-11 16:52:34', 131, 4),
(969, 548, NULL, 801, 1, 1288.2, 1, '2022-03-11 16:52:34', 131, 4),
(970, 538, NULL, 802, 2, 2395.6, 1, '2022-03-11 16:53:08', 131, 4),
(971, 548, NULL, 802, 1, 1083, 1, '2022-03-11 16:53:08', 131, 4),
(972, 538, NULL, 803, 5, 2395.6, 1, '2022-03-11 16:55:40', 131, 4),
(973, 548, NULL, 803, 6, 1083, 1, '2022-03-11 16:55:40', 131, 4),
(974, 549, NULL, 803, 2, 4788, 1, '2022-03-11 16:55:40', 131, 4),
(975, 538, NULL, 804, 1, 2275.82, 1, '2022-03-11 16:56:57', 131, 4),
(976, 538, NULL, 805, 2, 2275.82, 1, '2022-03-11 16:57:38', 131, 4),
(977, 548, NULL, 805, 3, 1169.64, 1, '2022-03-11 16:57:38', 131, 4),
(978, 549, NULL, 805, 4, 4883.76, 1, '2022-03-11 16:57:38', 131, 4),
(979, 538, NULL, 806, 1, 2275.82, 1, '2022-03-11 16:58:38', 131, 4),
(980, 548, NULL, 806, 1, 1169.64, 1, '2022-03-11 16:58:38', 131, 4),
(981, 538, NULL, 807, 1, 2275.82, 1, '2022-03-11 16:59:06', 131, 4),
(982, 548, NULL, 807, 1, 1169.64, 1, '2022-03-11 16:59:06', 131, 4),
(983, 538, NULL, 808, 1, 2275.82, 1, '2022-03-11 17:08:13', 131, 4),
(984, 548, NULL, 808, 1, 1169.64, 1, '2022-03-11 17:08:13', 131, 4),
(985, 538, NULL, 809, 1, 2275.82, 1, '2022-03-12 09:26:15', 131, 4),
(986, 548, NULL, 809, 1, 1169.64, 1, '2022-03-12 09:26:15', 131, 4),
(987, 538, NULL, 810, 2, 2275.82, 1, '2022-03-12 09:27:23', 131, 4),
(988, 548, NULL, 810, 2, 1223.79, 1, '2022-03-12 09:27:23', 131, 4),
(989, 538, NULL, 811, 1, 2275.82, 1, '2022-03-12 09:42:51', 131, 4),
(990, 538, NULL, 812, 2, 2275.82, 1, '2022-03-12 09:43:40', 131, 4),
(991, 548, NULL, 812, 2, 1223.79, 1, '2022-03-12 09:43:40', 131, 4),
(992, 538, NULL, 813, 1, 2275.82, 1, '2022-03-12 09:44:34', 131, 4),
(993, 548, NULL, 813, 1, 1169.64, 1, '2022-03-12 09:44:34', 131, 4),
(994, 549, NULL, 814, 1, 4979.52, 1, '2022-03-12 09:49:28', 131, 4),
(995, 538, NULL, 815, 1, 2275.82, 1, '2022-03-14 22:45:29', 131, 4),
(996, 538, NULL, 816, 2, 2275.82, 1, '2022-03-15 09:34:55', 131, 4),
(997, 538, NULL, 817, 1, 2275.82, 1, '2022-03-15 09:35:48', 131, 4),
(998, 538, NULL, 818, 1, 2275.82, 1, '2022-03-15 10:08:25', 131, 4),
(999, 538, NULL, 819, 2, 2275.82, 1, '2022-03-15 11:54:23', 131, 4),
(1000, 538, NULL, 820, 1, 2275.82, 1, '2022-03-15 12:01:00', 131, 4),
(1001, 538, NULL, 821, 1, 2275.82, 1, '2022-03-15 12:02:38', 131, 4),
(1002, 538, NULL, 822, 1, 2275.82, 1, '2022-03-15 12:04:14', 131, 4),
(1003, 538, NULL, 823, 1, 2275.82, 1, '2022-03-15 12:07:05', 131, 4),
(1004, 538, NULL, 824, 1, 2275.82, 1, '2022-03-15 12:13:00', 131, 4),
(1005, 538, NULL, 825, 1, 2275.82, 1, '2022-03-15 12:24:03', 131, 4),
(1006, 538, NULL, 826, 1, 2275.82, 1, '2022-03-15 12:26:38', 131, 4),
(1007, 538, NULL, 827, 1, 2275.82, 1, '2022-03-15 12:28:21', 131, 4),
(1008, 538, NULL, 828, 1, 2275.82, 1, '2022-03-15 12:31:04', 131, 4),
(1009, 538, NULL, 829, 1, 2275.82, 1, '2022-03-15 12:32:36', 131, 4),
(1010, 538, NULL, 830, 2, 2275.82, 1, '2022-03-15 12:35:29', 131, 4),
(1011, 538, NULL, 831, 1, 2275.82, 1, '2022-03-15 12:39:19', 131, 4),
(1012, 538, NULL, 832, 1, 2275.82, 1, '2022-03-15 13:06:03', 131, 4),
(1013, 538, NULL, 833, 1, 2275.82, 1, '2022-03-15 13:07:53', 131, 4),
(1014, 538, NULL, 834, 1, 2275.82, 1, '2022-03-16 17:07:12', 131, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `codigo` longtext DEFAULT NULL,
  `codigo_barra` varchar(50) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `acreditable` tinyint(1) NOT NULL DEFAULT 0,
  `presentacion` varchar(255) DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `stock` double NOT NULL DEFAULT 0,
  `aviso_stock` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `utilidad` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_bodega` int(11) NOT NULL,
  `id_unidad_medida` int(11) NOT NULL,
  `id_descuento` int(11) DEFAULT 0,
  `imagen` varchar(500) NOT NULL,
  `count_mail` int(11) NOT NULL,
  `estado` double NOT NULL,
  `existencia_cabys` tinyint(1) NOT NULL DEFAULT 0,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `codigo`, `codigo_barra`, `nombre`, `marca`, `descripcion`, `acreditable`, `presentacion`, `precio_compra`, `precio_venta`, `stock`, `aviso_stock`, `stock_max`, `utilidad`, `id_proveedor`, `id_bodega`, `id_unidad_medida`, `id_descuento`, `imagen`, `count_mail`, `estado`, `existencia_cabys`, `fecha_creada`) VALUES
(530, '2799805000000', '20213', 'Sandia', 'TEST', '100', 1, 'Sandia', 1000, 2000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(531, '2145', '20213', 'Mamon', 'TEST', '100', 1, 'Mamon', 1500, 2500, 100, 10, 400, 10, 0, 9, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(533, '2799805000000', '20213', 'Palitos de queso', 'TEST', '100', 1, 'Palitos de queso', 1000, 1500, 100, 10, 400, 10, 0, 9, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(534, '21681', '20213', 'Empanadas', 'TEST', '100', 1, 'Empanadas', 1000, 2020, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(535, '87894', '20213', 'Chechos', 'TEST', '100', 1, 'Chechos', 4000, 4500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(536, '189878', '20213', 'Chiriviscos ', 'TEST', '100', 1, 'Chiriviscos ', 1000, 1500, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(537, '654654', '20213', 'Helados de Chocolate', 'TEST', '100', 1, 'Helados de Chocolate', 1000, 3000, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(538, '4141102000000', '20213', 'Helados de Vainilla', 'TEST', 'Servicios informaticos', 1, 'Helados de Vainilla', 2, 2000, 100, 10, 400, 6, 0, 11, 78, 4, 'img/img_productos/5156FefjlqL._SX425_.jpg', 0, 0, 0, '2022-02-16 08:42:01'),
(539, '3213', '20213', 'Helados de Maracuya', 'TEST', '100', 1, 'Helados de Maracuya', 1000, 8000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(540, '313', '20213', 'Helados de Mamon', 'TEST', '100', 1, 'Helados de Mamon', 6000, 8000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(541, '65468', '20213', 'Helados de Fresa', 'TEST', '100', 1, 'Helados de Fresa', 1000, 7000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(542, '3213008', '20213', 'Helados de Menta', 'TEST', '100', 1, 'Helados de Menta', 1000, 4000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(543, '346848', '20213', 'Helados de Te frio', 'TEST', '100', 1, 'Helados de Te frio', 1000, 4500, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(544, '1231233', '20213', 'Helados de Melocoton', 'TEST', '100', 1, 'Helados de Melocoton', 1000, 2000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(545, '1231333', '20213', 'Helados de Ca?a', 'TEST', '100', 1, 'Helados de Ca?a', 1000, 2100, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(546, '221331', '20213', 'Chetos', 'TEST', '100', 1, 'Chetos', 1000, 1500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(548, '2391400009900', '20313', 'Peperoni', 'Peperoni', 'ND', 1, 'Peperoni', 500, 1000, 100, 10, 250, 14, 0, 11, 78, 4, '0', 0, 0, 0, '2022-02-16 08:45:19'),
(549, '2799805000000', '22813', 'Chicharrones', 'Chicharrones', 'ND', 1, 'Chicharrones', 3500, 4500, 100, 10, 1000, 12, 0, 10, 78, 4, '0', 1, 0, 0, '2022-02-16 08:45:19'),
(550, '20214', '20213', 'Sandia', 'TEST', '100', 1, 'Sandia', 1000, 2000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(551, '2145', '20213', 'Mamon', 'TEST', '100', 1, 'Mamon', 1500, 2500, 100, 10, 400, 10, 0, 9, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(553, '977842', '20213', 'Palitos de queso', 'TEST', '100', 1, 'Palitos de queso', 1000, 1500, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(554, '2168100000000', '20213', 'Empanadas', 'TEST', 'Empanadas', 1, 'Empanadas', 1000, 2020, 100, 10, 400, 7, 0, 10, 78, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(555, '87894', '20213', 'Chechos', 'TEST', '100', 1, 'Chechos', 4000, 4500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(556, '189878', '20213', 'Chiriviscos ', 'TEST', '100', 1, 'Chiriviscos ', 1000, 1500, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(557, '654654', '20213', 'Helados de Chocolate', 'TEST', '100', 1, 'Helados de Chocolate', 1000, 3000, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(558, '2323551', '20213', 'Helados de Vainilla', 'TEST', '100', 1, 'Helados de Vainilla', 1000, 2000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(559, '4141102000000', '20213', 'Helados de Maracuya', 'TEST', '100', 1, 'Helados de Maracuya', 1000, 8000, 100, 10, 400, 10, 0, 10, 78, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(560, '313', '20213', 'Helados de Mamon', 'TEST', '100', 1, 'Helados de Mamon', 6000, 8000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(561, '65468', '20213', 'Helados de Fresa', 'TEST', '100', 1, 'Helados de Fresa', 1000, 7000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(562, '3213008', '20213', 'Helados de Menta', 'TEST', '100', 1, 'Helados de Menta', 1000, 4000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(563, '346848', '20213', 'Helados de Te frio', 'TEST', '100', 1, 'Helados de Te frio', 1000, 4500, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(564, '1231233', '20213', 'Helados de Melocoton', 'TEST', '100', 1, 'Helados de Melocoton', 1000, 2000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(565, '4141102000000', '20213', 'Helados de Cageta', 'TEST', '100', 1, 'Helados de Cageta', 1000, 2100, 100, 10, 400, 10, 0, 10, 78, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(566, '221331', '20213', 'Chetos', 'TEST', '100', 1, 'Chetos', 1000, 1500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones`
--

CREATE TABLE `promociones` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `promociones`
--

INSERT INTO `promociones` (`id`, `valor`, `descripcion`, `estado`, `fecha`) VALUES
(48, 100, 'Promocion 1', 1, '2021-12-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_impuestos_productos`
--

CREATE TABLE `r_impuestos_productos` (
  `id` int(11) NOT NULL,
  `id_impuesto` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_impuestos_productos`
--

INSERT INTO `r_impuestos_productos` (`id`, `id_impuesto`, `id_producto`, `fecha_creada`) VALUES
(817, 18, 530, '2022-02-16'),
(818, 18, 531, '2022-02-16'),
(821, 18, 534, '2022-02-16'),
(822, 18, 535, '2022-02-16'),
(823, 18, 536, '2022-02-16'),
(824, 18, 537, '2022-02-16'),
(826, 18, 539, '2022-02-16'),
(827, 18, 540, '2022-02-16'),
(828, 18, 541, '2022-02-16'),
(829, 18, 542, '2022-02-16'),
(830, 18, 543, '2022-02-16'),
(831, 18, 544, '2022-02-16'),
(832, 18, 545, '2022-02-16'),
(833, 18, 546, '2022-02-16'),
(836, 18, 550, '2022-02-16'),
(837, 18, 551, '2022-02-16'),
(839, 18, 553, '2022-02-16'),
(841, 18, 555, '2022-02-16'),
(842, 18, 556, '2022-02-16'),
(843, 18, 557, '2022-02-16'),
(844, 18, 558, '2022-02-16'),
(846, 18, 560, '2022-02-16'),
(847, 18, 561, '2022-02-16'),
(848, 18, 562, '2022-02-16'),
(849, 18, 563, '2022-02-16'),
(850, 18, 564, '2022-02-16'),
(852, 18, 566, '2022-02-16'),
(856, 18, 533, '2022-02-16'),
(875, 17, 554, '2022-03-11'),
(876, 17, 538, '2022-03-11'),
(880, 18, 548, '2022-03-12'),
(881, 19, 549, '2022-03-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_plazo_venta`
--

CREATE TABLE `r_plazo_venta` (
  `id` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_plazo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  `fecha_act` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_plazo_venta`
--

INSERT INTO `r_plazo_venta` (`id`, `id_venta`, `id_plazo`, `estado`, `fecha_creada`, `fecha_act`) VALUES
(8, 381, 2, 0, '2022-02-03', '2022-02-03'),
(9, 382, 2, 0, '2022-02-03', '2022-02-03'),
(10, 383, 2, 0, '2022-02-15', '2022-02-15'),
(11, 384, 2, 0, '2022-02-17', '2022-02-17'),
(12, 385, 2, 0, '2022-02-17', '2022-02-17'),
(13, 386, 2, 0, '2022-02-17', '2022-02-17'),
(14, 387, 2, 0, '2022-02-17', '2022-02-17'),
(15, 388, 2, 0, '2022-02-17', '2022-02-17'),
(16, 389, 2, 0, '2022-02-17', '2022-02-17'),
(17, 390, 2, 0, '2022-02-17', '2022-02-17'),
(18, 391, 2, 0, '2022-02-17', '2022-02-17'),
(19, 392, 2, 0, '2022-02-17', '2022-02-17'),
(20, 393, 2, 0, '2022-02-17', '2022-02-17'),
(21, 394, 2, 0, '2022-02-17', '2022-02-17'),
(22, 395, 2, 0, '2022-02-17', '2022-02-17'),
(23, 396, 2, 0, '2022-02-17', '2022-02-17'),
(24, 397, 2, 0, '2022-02-17', '2022-02-17'),
(25, 398, 2, 0, '2022-02-17', '2022-02-17'),
(26, 399, 2, 0, '2022-02-17', '2022-02-17'),
(27, 400, 2, 0, '2022-02-17', '2022-02-17'),
(28, 401, 2, 0, '2022-02-17', '2022-02-17'),
(29, 402, 2, 0, '2022-02-17', '2022-02-17'),
(30, 403, 2, 0, '2022-02-17', '2022-02-17'),
(31, 404, 2, 0, '2022-02-18', '2022-02-18'),
(32, 405, 2, 0, '2022-02-18', '2022-02-18'),
(33, 406, 2, 0, '2022-02-18', '2022-02-18'),
(34, 407, 2, 0, '2022-02-18', '2022-02-18'),
(35, 408, 2, 0, '2022-02-18', '2022-02-18'),
(36, 409, 2, 0, '2022-02-18', '2022-02-18'),
(37, 410, 2, 0, '2022-02-18', '2022-02-18'),
(38, 411, 2, 0, '2022-02-18', '2022-02-18'),
(39, 412, 2, 0, '2022-02-18', '2022-02-18'),
(40, 413, 2, 0, '2022-02-18', '2022-02-18'),
(41, 414, 2, 0, '2022-02-18', '2022-02-18'),
(42, 415, 2, 0, '2022-02-18', '2022-02-18'),
(43, 416, 2, 0, '2022-02-18', '2022-02-18'),
(44, 417, 2, 0, '2022-02-18', '2022-02-18'),
(45, 418, 2, 0, '2022-02-18', '2022-02-18'),
(46, 419, 2, 0, '2022-02-18', '2022-02-18'),
(47, 420, 2, 0, '2022-02-18', '2022-02-18'),
(48, 421, 2, 0, '2022-02-18', '2022-02-18'),
(49, 422, 2, 0, '2022-02-18', '2022-02-18'),
(50, 423, 2, 0, '2022-02-18', '2022-02-18'),
(51, 424, 2, 0, '2022-02-18', '2022-02-18'),
(52, 425, 2, 0, '2022-02-18', '2022-02-18'),
(53, 426, 2, 0, '2022-02-18', '2022-02-18'),
(54, 427, 2, 0, '2022-02-18', '2022-02-18'),
(55, 428, 2, 0, '2022-02-19', '2022-02-19'),
(56, 429, 2, 0, '2022-02-19', '2022-02-19'),
(57, 430, 2, 0, '2022-02-19', '2022-02-19'),
(58, 431, 2, 0, '2022-02-19', '2022-02-19'),
(59, 432, 2, 0, '2022-02-19', '2022-02-19'),
(60, 433, 2, 0, '2022-02-19', '2022-02-19'),
(61, 434, 2, 0, '2022-02-19', '2022-02-19'),
(62, 435, 2, 0, '2022-02-19', '2022-02-19'),
(63, 436, 2, 0, '2022-02-19', '2022-02-19'),
(64, 437, 2, 0, '2022-02-19', '2022-02-19'),
(65, 438, 2, 0, '2022-02-19', '2022-02-19'),
(66, 439, 2, 0, '2022-02-19', '2022-02-19'),
(67, 440, 2, 0, '2022-02-19', '2022-02-19'),
(68, 441, 2, 0, '2022-02-19', '2022-02-19'),
(69, 442, 2, 0, '2022-02-19', '2022-02-19'),
(70, 443, 2, 0, '2022-02-19', '2022-02-19'),
(71, 444, 2, 0, '2022-02-19', '2022-02-19'),
(72, 445, 2, 0, '2022-02-19', '2022-02-19'),
(73, 446, 2, 0, '2022-02-19', '2022-02-19'),
(74, 447, 2, 0, '2022-02-19', '2022-02-19'),
(75, 448, 2, 0, '2022-02-19', '2022-02-19'),
(76, 449, 2, 0, '2022-02-19', '2022-02-19'),
(77, 450, 2, 0, '2022-02-19', '2022-02-19'),
(78, 451, 2, 0, '2022-02-19', '2022-02-19'),
(79, 452, 2, 0, '2022-02-19', '2022-02-19'),
(80, 453, 2, 0, '2022-02-19', '2022-02-19'),
(81, 454, 2, 0, '2022-02-19', '2022-02-19'),
(82, 455, 2, 0, '2022-02-19', '2022-02-19'),
(83, 456, 2, 0, '2022-02-19', '2022-02-19'),
(84, 457, 2, 0, '2022-02-19', '2022-02-19'),
(85, 458, 2, 0, '2022-02-19', '2022-02-19'),
(86, 459, 2, 0, '2022-02-19', '2022-02-19'),
(87, 460, 2, 0, '2022-02-19', '2022-02-19'),
(88, 461, 2, 0, '2022-02-19', '2022-02-19'),
(89, 462, 2, 0, '2022-02-19', '2022-02-19'),
(90, 463, 2, 0, '2022-02-19', '2022-02-19'),
(91, 464, 2, 0, '2022-02-19', '2022-02-19'),
(92, 465, 2, 0, '2022-02-19', '2022-02-19'),
(93, 466, 2, 0, '2022-02-19', '2022-02-19'),
(94, 467, 2, 0, '2022-02-19', '2022-02-19'),
(95, 468, 2, 0, '2022-02-19', '2022-02-19'),
(96, 469, 2, 0, '2022-02-19', '2022-02-19'),
(97, 470, 2, 0, '2022-02-19', '2022-02-19'),
(98, 471, 2, 0, '2022-02-19', '2022-02-19'),
(99, 472, 2, 0, '2022-02-19', '2022-02-19'),
(100, 473, 2, 0, '2022-02-19', '2022-02-19'),
(101, 474, 2, 0, '2022-02-19', '2022-02-19'),
(102, 475, 2, 0, '2022-02-19', '2022-02-19'),
(103, 476, 2, 0, '2022-02-19', '2022-02-19'),
(104, 477, 2, 0, '2022-02-19', '2022-02-19'),
(105, 478, 2, 0, '2022-02-19', '2022-02-19'),
(106, 479, 2, 0, '2022-02-19', '2022-02-19'),
(107, 480, 2, 0, '2022-02-19', '2022-02-19'),
(108, 481, 2, 0, '2022-02-19', '2022-02-19'),
(109, 482, 2, 0, '2022-02-19', '2022-02-19'),
(110, 483, 2, 0, '2022-02-19', '2022-02-19'),
(111, 484, 2, 0, '2022-02-19', '2022-02-19'),
(112, 485, 2, 0, '2022-02-21', '2022-02-21'),
(113, 486, 2, 0, '2022-02-21', '2022-02-21'),
(114, 487, 2, 0, '2022-02-21', '2022-02-21'),
(115, 488, 2, 0, '2022-02-21', '2022-02-21'),
(116, 489, 2, 0, '2022-02-21', '2022-02-21'),
(117, 490, 2, 0, '2022-02-21', '2022-02-21'),
(118, 491, 2, 0, '2022-02-21', '2022-02-21'),
(119, 492, 2, 0, '2022-02-21', '2022-02-21'),
(120, 493, 2, 0, '2022-02-21', '2022-02-21'),
(121, 494, 2, 0, '2022-02-21', '2022-02-21'),
(122, 495, 2, 0, '2022-02-21', '2022-02-21'),
(123, 496, 2, 0, '2022-02-21', '2022-02-21'),
(124, 497, 2, 0, '2022-02-21', '2022-02-21'),
(125, 498, 2, 0, '2022-02-21', '2022-02-21'),
(126, 499, 2, 0, '2022-02-22', '2022-02-22'),
(127, 500, 2, 0, '2022-02-22', '2022-02-22'),
(128, 501, 2, 0, '2022-02-23', '2022-02-23'),
(129, 502, 2, 0, '2022-02-23', '2022-02-23'),
(130, 503, 2, 0, '2022-02-23', '2022-02-23'),
(131, 504, 2, 0, '2022-02-23', '2022-02-23'),
(132, 505, 2, 0, '2022-02-23', '2022-02-23'),
(133, 506, 2, 0, '2022-02-23', '2022-02-23'),
(134, 507, 3, 0, '2022-02-23', '2022-02-23'),
(135, 508, 2, 0, '2022-02-23', '2022-02-23'),
(136, 509, 2, 0, '2022-02-23', '2022-02-23'),
(137, 510, 2, 0, '2022-02-23', '2022-02-23'),
(138, 511, 2, 0, '2022-02-23', '2022-02-23'),
(139, 512, 2, 0, '2022-02-23', '2022-02-23'),
(140, 513, 2, 0, '2022-02-23', '2022-02-23'),
(141, 514, 2, 0, '2022-02-23', '2022-02-23'),
(142, 515, 2, 0, '2022-02-23', '2022-02-23'),
(143, 516, 2, 0, '2022-02-23', '2022-02-23'),
(144, 517, 2, 0, '2022-02-24', '2022-02-24'),
(145, 518, 2, 0, '2022-02-24', '2022-02-24'),
(146, 519, 2, 0, '2022-02-24', '2022-02-24'),
(147, 520, 2, 0, '2022-02-24', '2022-02-24'),
(148, 521, 2, 0, '2022-02-24', '2022-02-24'),
(149, 522, 2, 0, '2022-02-24', '2022-02-24'),
(150, 523, 2, 0, '2022-02-24', '2022-02-24'),
(151, 524, 2, 0, '2022-02-24', '2022-02-24'),
(152, 525, 2, 0, '2022-02-24', '2022-02-24'),
(153, 526, 2, 0, '2022-02-24', '2022-02-24'),
(154, 527, 2, 0, '2022-02-24', '2022-02-24'),
(155, 528, 2, 0, '2022-02-24', '2022-02-24'),
(156, 529, 2, 0, '2022-02-24', '2022-02-24'),
(157, 530, 2, 0, '2022-02-24', '2022-02-24'),
(158, 531, 2, 0, '2022-02-24', '2022-02-24'),
(159, 532, 2, 0, '2022-02-24', '2022-02-24'),
(160, 533, 2, 0, '2022-02-24', '2022-02-24'),
(161, 534, 2, 0, '2022-02-24', '2022-02-24'),
(162, 535, 2, 0, '2022-02-24', '2022-02-24'),
(163, 536, 2, 0, '2022-02-24', '2022-02-24'),
(164, 537, 2, 0, '2022-02-24', '2022-02-24'),
(165, 538, 2, 0, '2022-02-24', '2022-02-24'),
(166, 539, 2, 0, '2022-02-24', '2022-02-24'),
(167, 540, 2, 0, '2022-02-24', '2022-02-24'),
(168, 541, 2, 0, '2022-02-24', '2022-02-24'),
(169, 542, 2, 0, '2022-02-24', '2022-02-24'),
(170, 543, 2, 0, '2022-02-24', '2022-02-24'),
(171, 544, 2, 0, '2022-02-24', '2022-02-24'),
(172, 545, 2, 0, '2022-03-04', '2022-03-04'),
(173, 546, 2, 0, '2022-03-04', '2022-03-04'),
(174, 547, 2, 0, '2022-03-04', '2022-03-04'),
(175, 548, 2, 0, '2022-03-04', '2022-03-04'),
(176, 549, 2, 0, '2022-03-04', '2022-03-04'),
(177, 550, 2, 0, '2022-03-04', '2022-03-04'),
(178, 551, 2, 0, '2022-03-04', '2022-03-04'),
(179, 552, 2, 0, '2022-03-04', '2022-03-04'),
(180, 553, 2, 0, '2022-03-04', '2022-03-04'),
(181, 554, 2, 0, '2022-03-04', '2022-03-04'),
(182, 555, 2, 0, '2022-03-04', '2022-03-04'),
(183, 556, 2, 0, '2022-03-04', '2022-03-04'),
(184, 557, 2, 0, '2022-03-04', '2022-03-04'),
(185, 558, 2, 0, '2022-03-04', '2022-03-04'),
(186, 559, 2, 0, '2022-03-04', '2022-03-04'),
(187, 560, 2, 0, '2022-03-04', '2022-03-04'),
(188, 561, 2, 0, '2022-03-04', '2022-03-04'),
(189, 562, 2, 0, '2022-03-04', '2022-03-04'),
(190, 563, 2, 0, '2022-03-04', '2022-03-04'),
(191, 564, 2, 0, '2022-03-04', '2022-03-04'),
(192, 565, 2, 0, '2022-03-04', '2022-03-04'),
(193, 566, 2, 0, '2022-03-07', '2022-03-07'),
(194, 567, 2, 0, '2022-03-07', '2022-03-07'),
(195, 568, 2, 0, '2022-03-07', '2022-03-07'),
(196, 569, 2, 0, '2022-03-07', '2022-03-07'),
(197, 570, 2, 0, '2022-03-07', '2022-03-07'),
(198, 571, 2, 0, '2022-03-07', '2022-03-07'),
(199, 572, 2, 0, '2022-03-07', '2022-03-07'),
(200, 573, 2, 0, '2022-03-07', '2022-03-07'),
(201, 574, 2, 0, '2022-03-07', '2022-03-07'),
(202, 575, 2, 0, '2022-03-07', '2022-03-07'),
(203, 576, 2, 0, '2022-03-07', '2022-03-07'),
(204, 577, 2, 0, '2022-03-07', '2022-03-07'),
(205, 578, 2, 0, '2022-03-07', '2022-03-07'),
(206, 580, 2, 0, '2022-03-07', '2022-03-07'),
(207, 581, 2, 0, '2022-03-07', '2022-03-07'),
(208, 582, 2, 0, '2022-03-07', '2022-03-07'),
(209, 583, 2, 0, '2022-03-07', '2022-03-07'),
(210, 584, 2, 0, '2022-03-07', '2022-03-07'),
(211, 585, 2, 0, '2022-03-07', '2022-03-07'),
(212, 586, 2, 0, '2022-03-07', '2022-03-07'),
(213, 587, 2, 0, '2022-03-07', '2022-03-07'),
(214, 588, 2, 0, '2022-03-07', '2022-03-07'),
(215, 589, 2, 0, '2022-03-07', '2022-03-07'),
(216, 590, 2, 0, '2022-03-07', '2022-03-07'),
(217, 591, 2, 0, '2022-03-07', '2022-03-07'),
(218, 592, 2, 0, '2022-03-07', '2022-03-07'),
(219, 593, 2, 0, '2022-03-07', '2022-03-07'),
(220, 594, 2, 0, '2022-03-07', '2022-03-07'),
(221, 595, 2, 0, '2022-03-07', '2022-03-07'),
(222, 596, 2, 0, '2022-03-07', '2022-03-07'),
(223, 597, 2, 0, '2022-03-07', '2022-03-07'),
(224, 598, 2, 0, '2022-03-07', '2022-03-07'),
(225, 599, 2, 0, '2022-03-07', '2022-03-07'),
(226, 600, 2, 0, '2022-03-07', '2022-03-07'),
(227, 601, 2, 0, '2022-03-07', '2022-03-07'),
(228, 602, 2, 0, '2022-03-07', '2022-03-07'),
(229, 603, 2, 0, '2022-03-07', '2022-03-07'),
(230, 604, 2, 0, '2022-03-07', '2022-03-07'),
(231, 605, 2, 0, '2022-03-07', '2022-03-07'),
(232, 606, 2, 0, '2022-03-07', '2022-03-07'),
(233, 607, 2, 0, '2022-03-07', '2022-03-07'),
(234, 608, 2, 0, '2022-03-07', '2022-03-07'),
(235, 609, 2, 0, '2022-03-07', '2022-03-07'),
(236, 610, 2, 0, '2022-03-07', '2022-03-07'),
(237, 611, 2, 0, '2022-03-07', '2022-03-07'),
(238, 612, 2, 0, '2022-03-07', '2022-03-07'),
(239, 613, 2, 0, '2022-03-07', '2022-03-07'),
(240, 614, 2, 0, '2022-03-07', '2022-03-07'),
(241, 615, 2, 0, '2022-03-07', '2022-03-07'),
(242, 616, 2, 0, '2022-03-07', '2022-03-07'),
(243, 617, 2, 0, '2022-03-07', '2022-03-07'),
(244, 618, 2, 0, '2022-03-07', '2022-03-07'),
(245, 619, 2, 0, '2022-03-07', '2022-03-07'),
(246, 620, 2, 0, '2022-03-07', '2022-03-07'),
(247, 621, 2, 0, '2022-03-07', '2022-03-07'),
(248, 622, 2, 0, '2022-03-08', '2022-03-08'),
(249, 623, 2, 0, '2022-03-08', '2022-03-08'),
(250, 624, 2, 0, '2022-03-08', '2022-03-08'),
(251, 625, 2, 0, '2022-03-08', '2022-03-08'),
(252, 626, 2, 0, '2022-03-08', '2022-03-08'),
(253, 627, 2, 0, '2022-03-08', '2022-03-08'),
(254, 628, 2, 0, '2022-03-08', '2022-03-08'),
(255, 629, 2, 0, '2022-03-08', '2022-03-08'),
(256, 630, 2, 0, '2022-03-08', '2022-03-08'),
(257, 631, 2, 0, '2022-03-08', '2022-03-08'),
(258, 632, 2, 0, '2022-03-08', '2022-03-08'),
(259, 633, 2, 0, '2022-03-08', '2022-03-08'),
(260, 634, 2, 0, '2022-03-08', '2022-03-08'),
(261, 635, 2, 0, '2022-03-08', '2022-03-08'),
(262, 636, 2, 0, '2022-03-08', '2022-03-08'),
(263, 637, 2, 0, '2022-03-08', '2022-03-08'),
(264, 638, 2, 0, '2022-03-08', '2022-03-08'),
(265, 639, 2, 0, '2022-03-08', '2022-03-08'),
(266, 640, 2, 0, '2022-03-08', '2022-03-08'),
(267, 641, 2, 0, '2022-03-08', '2022-03-08'),
(268, 642, 2, 0, '2022-03-08', '2022-03-08'),
(269, 643, 2, 0, '2022-03-08', '2022-03-08'),
(270, 644, 2, 0, '2022-03-08', '2022-03-08'),
(271, 645, 2, 0, '2022-03-08', '2022-03-08'),
(272, 646, 2, 0, '2022-03-08', '2022-03-08'),
(273, 647, 2, 0, '2022-03-08', '2022-03-08'),
(274, 648, 2, 0, '2022-03-08', '2022-03-08'),
(275, 649, 2, 0, '2022-03-08', '2022-03-08'),
(276, 650, 2, 0, '2022-03-08', '2022-03-08'),
(277, 651, 2, 0, '2022-03-08', '2022-03-08'),
(278, 652, 2, 0, '2022-03-08', '2022-03-08'),
(279, 653, 2, 0, '2022-03-08', '2022-03-08'),
(280, 654, 2, 0, '2022-03-08', '2022-03-08'),
(281, 655, 2, 0, '2022-03-08', '2022-03-08'),
(282, 656, 2, 0, '2022-03-08', '2022-03-08'),
(283, 657, 2, 0, '2022-03-08', '2022-03-08'),
(284, 658, 2, 0, '2022-03-08', '2022-03-08'),
(285, 659, 2, 0, '2022-03-08', '2022-03-08'),
(286, 660, 2, 0, '2022-03-08', '2022-03-08'),
(287, 661, 2, 0, '2022-03-08', '2022-03-08'),
(288, 662, 2, 0, '2022-03-08', '2022-03-08'),
(289, 663, 2, 0, '2022-03-08', '2022-03-08'),
(290, 664, 2, 0, '2022-03-08', '2022-03-08'),
(291, 665, 2, 0, '2022-03-08', '2022-03-08'),
(292, 666, 2, 0, '2022-03-08', '2022-03-08'),
(293, 667, 2, 0, '2022-03-08', '2022-03-08'),
(294, 668, 2, 0, '2022-03-08', '2022-03-08'),
(295, 669, 2, 0, '2022-03-08', '2022-03-08'),
(296, 670, 2, 0, '2022-03-08', '2022-03-08'),
(297, 671, 2, 0, '2022-03-08', '2022-03-08'),
(298, 672, 2, 0, '2022-03-08', '2022-03-08'),
(299, 673, 2, 0, '2022-03-08', '2022-03-08'),
(300, 674, 2, 0, '2022-03-08', '2022-03-08'),
(301, 675, 2, 0, '2022-03-08', '2022-03-08'),
(302, 676, 2, 0, '2022-03-08', '2022-03-08'),
(303, 677, 2, 0, '2022-03-08', '2022-03-08'),
(304, 678, 2, 0, '2022-03-08', '2022-03-08'),
(305, 679, 2, 0, '2022-03-08', '2022-03-08'),
(306, 680, 2, 0, '2022-03-08', '2022-03-08'),
(307, 681, 2, 0, '2022-03-08', '2022-03-08'),
(308, 682, 2, 0, '2022-03-08', '2022-03-08'),
(309, 683, 2, 0, '2022-03-08', '2022-03-08'),
(310, 684, 2, 0, '2022-03-08', '2022-03-08'),
(311, 685, 2, 0, '2022-03-08', '2022-03-08'),
(312, 686, 2, 0, '2022-03-08', '2022-03-08'),
(313, 687, 2, 0, '2022-03-09', '2022-03-09'),
(314, 688, 2, 0, '2022-03-09', '2022-03-09'),
(315, 689, 2, 0, '2022-03-09', '2022-03-09'),
(316, 690, 2, 0, '2022-03-09', '2022-03-09'),
(317, 691, 2, 0, '2022-03-09', '2022-03-09'),
(318, 692, 2, 0, '2022-03-09', '2022-03-09'),
(319, 693, 2, 0, '2022-03-09', '2022-03-09'),
(320, 694, 2, 0, '2022-03-09', '2022-03-09'),
(321, 695, 2, 0, '2022-03-09', '2022-03-09'),
(322, 696, 2, 0, '2022-03-09', '2022-03-09'),
(323, 697, 2, 0, '2022-03-09', '2022-03-09'),
(324, 698, 2, 0, '2022-03-09', '2022-03-09'),
(325, 699, 2, 0, '2022-03-09', '2022-03-09'),
(326, 700, 2, 0, '2022-03-09', '2022-03-09'),
(327, 701, 2, 0, '2022-03-09', '2022-03-09'),
(328, 702, 2, 0, '2022-03-09', '2022-03-09'),
(329, 703, 2, 0, '2022-03-09', '2022-03-09'),
(330, 704, 2, 0, '2022-03-09', '2022-03-09'),
(331, 705, 2, 0, '2022-03-09', '2022-03-09'),
(332, 706, 2, 0, '2022-03-09', '2022-03-09'),
(333, 707, 2, 0, '2022-03-09', '2022-03-09'),
(334, 708, 2, 0, '2022-03-09', '2022-03-09'),
(335, 709, 2, 0, '2022-03-09', '2022-03-09'),
(336, 710, 2, 0, '2022-03-09', '2022-03-09'),
(337, 711, 2, 0, '2022-03-09', '2022-03-09'),
(338, 712, 2, 0, '2022-03-09', '2022-03-09'),
(339, 713, 2, 0, '2022-03-09', '2022-03-09'),
(340, 714, 2, 0, '2022-03-09', '2022-03-09'),
(341, 715, 2, 0, '2022-03-09', '2022-03-09'),
(342, 716, 2, 0, '2022-03-09', '2022-03-09'),
(343, 718, 2, 0, '2022-03-09', '2022-03-09'),
(344, 719, 2, 0, '2022-03-09', '2022-03-09'),
(345, 720, 2, 0, '2022-03-09', '2022-03-09'),
(346, 721, 2, 0, '2022-03-09', '2022-03-09'),
(347, 722, 2, 0, '2022-03-09', '2022-03-09'),
(348, 723, 2, 0, '2022-03-09', '2022-03-09'),
(349, 724, 2, 0, '2022-03-09', '2022-03-09'),
(350, 725, 2, 0, '2022-03-09', '2022-03-09'),
(351, 726, 2, 0, '2022-03-09', '2022-03-09'),
(352, 727, 2, 0, '2022-03-09', '2022-03-09'),
(353, 728, 2, 0, '2022-03-09', '2022-03-09'),
(354, 729, 2, 0, '2022-03-09', '2022-03-09'),
(355, 730, 2, 0, '2022-03-09', '2022-03-09'),
(356, 731, 2, 0, '2022-03-09', '2022-03-09'),
(357, 732, 2, 0, '2022-03-09', '2022-03-09'),
(358, 733, 2, 0, '2022-03-09', '2022-03-09'),
(359, 734, 2, 0, '2022-03-09', '2022-03-09'),
(360, 735, 2, 0, '2022-03-09', '2022-03-09'),
(361, 736, 2, 0, '2022-03-09', '2022-03-09'),
(362, 737, 2, 0, '2022-03-09', '2022-03-09'),
(363, 738, 2, 0, '2022-03-09', '2022-03-09'),
(364, 739, 2, 0, '2022-03-09', '2022-03-09'),
(365, 740, 2, 0, '2022-03-09', '2022-03-09'),
(366, 741, 2, 0, '2022-03-09', '2022-03-09'),
(367, 742, 2, 0, '2022-03-09', '2022-03-09'),
(368, 743, 2, 0, '2022-03-09', '2022-03-09'),
(369, 744, 2, 0, '2022-03-09', '2022-03-09'),
(370, 745, 2, 0, '2022-03-09', '2022-03-09'),
(371, 746, 2, 0, '2022-03-09', '2022-03-09'),
(372, 747, 2, 0, '2022-03-09', '2022-03-09'),
(373, 748, 2, 0, '2022-03-09', '2022-03-09'),
(374, 749, 2, 0, '2022-03-09', '2022-03-09'),
(375, 750, 2, 0, '2022-03-09', '2022-03-09'),
(376, 751, 2, 0, '2022-03-09', '2022-03-09'),
(377, 752, 2, 0, '2022-03-09', '2022-03-09'),
(378, 753, 2, 0, '2022-03-09', '2022-03-09'),
(379, 754, 2, 0, '2022-03-09', '2022-03-09'),
(380, 755, 2, 0, '2022-03-09', '2022-03-09'),
(381, 756, 2, 0, '2022-03-09', '2022-03-09'),
(382, 757, 2, 0, '2022-03-09', '2022-03-09'),
(383, 758, 2, 0, '2022-03-09', '2022-03-09'),
(384, 759, 2, 0, '2022-03-09', '2022-03-09'),
(385, 760, 2, 0, '2022-03-09', '2022-03-09'),
(386, 761, 2, 0, '2022-03-09', '2022-03-09'),
(387, 762, 2, 0, '2022-03-09', '2022-03-09'),
(388, 763, 2, 0, '2022-03-09', '2022-03-09'),
(389, 764, 2, 0, '2022-03-09', '2022-03-09'),
(390, 765, 2, 0, '2022-03-11', '2022-03-11'),
(391, 766, 2, 0, '2022-03-11', '2022-03-11'),
(392, 767, 2, 0, '2022-03-11', '2022-03-11'),
(393, 768, 2, 0, '2022-03-11', '2022-03-11'),
(394, 769, 2, 0, '2022-03-11', '2022-03-11'),
(395, 770, 2, 0, '2022-03-11', '2022-03-11'),
(396, 771, 2, 0, '2022-03-11', '2022-03-11'),
(397, 772, 2, 0, '2022-03-11', '2022-03-11'),
(398, 773, 2, 0, '2022-03-11', '2022-03-11'),
(399, 774, 2, 0, '2022-03-11', '2022-03-11'),
(400, 775, 2, 0, '2022-03-11', '2022-03-11'),
(401, 776, 2, 0, '2022-03-11', '2022-03-11'),
(402, 777, 2, 0, '2022-03-11', '2022-03-11'),
(403, 778, 2, 0, '2022-03-11', '2022-03-11'),
(404, 779, 2, 0, '2022-03-11', '2022-03-11'),
(405, 780, 2, 0, '2022-03-11', '2022-03-11'),
(406, 781, 2, 0, '2022-03-11', '2022-03-11'),
(407, 782, 2, 0, '2022-03-11', '2022-03-11'),
(408, 783, 2, 0, '2022-03-11', '2022-03-11'),
(409, 784, 2, 0, '2022-03-11', '2022-03-11'),
(410, 785, 2, 0, '2022-03-11', '2022-03-11'),
(411, 786, 2, 0, '2022-03-11', '2022-03-11'),
(412, 787, 2, 0, '2022-03-11', '2022-03-11'),
(413, 788, 2, 0, '2022-03-11', '2022-03-11'),
(414, 789, 2, 0, '2022-03-11', '2022-03-11'),
(415, 790, 2, 0, '2022-03-11', '2022-03-11'),
(416, 791, 2, 0, '2022-03-11', '2022-03-11'),
(417, 792, 2, 0, '2022-03-11', '2022-03-11'),
(418, 793, 2, 0, '2022-03-11', '2022-03-11'),
(419, 794, 2, 0, '2022-03-11', '2022-03-11'),
(420, 795, 2, 0, '2022-03-11', '2022-03-11'),
(421, 797, 2, 0, '2022-03-11', '2022-03-11'),
(422, 798, 2, 0, '2022-03-11', '2022-03-11'),
(423, 799, 2, 0, '2022-03-11', '2022-03-11'),
(424, 800, 2, 0, '2022-03-11', '2022-03-11'),
(425, 801, 2, 0, '2022-03-11', '2022-03-11'),
(426, 802, 2, 0, '2022-03-11', '2022-03-11'),
(427, 803, 2, 0, '2022-03-11', '2022-03-11'),
(428, 804, 2, 0, '2022-03-11', '2022-03-11'),
(429, 805, 2, 0, '2022-03-11', '2022-03-11'),
(430, 806, 2, 0, '2022-03-11', '2022-03-11'),
(431, 807, 2, 0, '2022-03-11', '2022-03-11'),
(432, 808, 2, 0, '2022-03-11', '2022-03-11'),
(433, 809, 2, 0, '2022-03-12', '2022-03-12'),
(434, 810, 2, 0, '2022-03-12', '2022-03-12'),
(435, 811, 2, 0, '2022-03-12', '2022-03-12'),
(436, 812, 2, 0, '2022-03-12', '2022-03-12'),
(437, 813, 2, 0, '2022-03-12', '2022-03-12'),
(438, 814, 2, 0, '2022-03-12', '2022-03-12'),
(439, 815, 2, 0, '2022-03-14', '2022-03-14'),
(440, 816, 2, 0, '2022-03-15', '2022-03-15'),
(441, 817, 2, 0, '2022-03-15', '2022-03-15'),
(442, 818, 2, 0, '2022-03-15', '2022-03-15'),
(443, 819, 2, 0, '2022-03-15', '2022-03-15'),
(444, 820, 2, 0, '2022-03-15', '2022-03-15'),
(445, 821, 2, 0, '2022-03-15', '2022-03-15'),
(446, 822, 2, 0, '2022-03-15', '2022-03-15'),
(447, 823, 2, 0, '2022-03-15', '2022-03-15'),
(448, 824, 2, 0, '2022-03-15', '2022-03-15'),
(449, 825, 2, 0, '2022-03-15', '2022-03-15'),
(450, 826, 2, 0, '2022-03-15', '2022-03-15'),
(451, 827, 2, 0, '2022-03-15', '2022-03-15'),
(452, 828, 2, 0, '2022-03-15', '2022-03-15'),
(453, 829, 2, 0, '2022-03-15', '2022-03-15'),
(454, 830, 2, 0, '2022-03-15', '2022-03-15'),
(455, 831, 2, 0, '2022-03-15', '2022-03-15'),
(456, 832, 2, 0, '2022-03-15', '2022-03-15'),
(457, 833, 2, 0, '2022-03-15', '2022-03-15'),
(458, 834, 2, 0, '2022-03-16', '2022-03-16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_promociones`
--

CREATE TABLE `r_promociones` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT 0,
  `id_categoria` int(11) NOT NULL,
  `id_porcentaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_promociones`
--

INSERT INTO `r_promociones` (`id`, `id_producto`, `id_habitacion`, `id_categoria`, `id_porcentaje`) VALUES
(69, 0, 55, 5, 48),
(70, 0, 57, 5, 48),
(71, 0, 60, 5, 48),
(72, 214, 0, 5, 48);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_utilidades_productos`
--

CREATE TABLE `r_utilidades_productos` (
  `id` int(11) NOT NULL,
  `id_utilidad` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subimpuestos_productos`
--

CREATE TABLE `subimpuestos_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `subimpuestos_productos`
--

INSERT INTO `subimpuestos_productos` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(6, 'Cruz roja', 2, 1, '2021-10-25'),
(7, 'IVA', 13, 1, '2021-10-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldo`
--

CREATE TABLE `sueldo` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `monto` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `dia_pago` int(11) NOT NULL DEFAULT 1,
  `fecha_comienzo` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa`
--

CREATE TABLE `tarifa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa`
--

INSERT INTO `tarifa` (`id`, `nombre`) VALUES
(1, '24 Horas'),
(4, '12 horas'),
(7, 'Doble'),
(8, 'Personal'),
(9, 'Triple'),
(10, 'Cuadruple');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa_habitacion`
--

CREATE TABLE `tarifa_habitacion` (
  `id` int(11) NOT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa_habitacion`
--

INSERT INTO `tarifa_habitacion` (`id`, `id_tarifa`, `id_habitacion`, `precio`) VALUES
(17, 8, 6, 380),
(18, 7, 6, 580),
(19, 9, 6, 700),
(20, 10, 6, 800),
(23, 7, 8, 580),
(24, 7, 7, 580),
(25, 7, 9, 580),
(26, 9, 10, 700),
(27, 7, 11, 580),
(28, 8, 12, 380),
(29, 8, 13, 380),
(30, 8, 14, 500),
(31, 7, 14, 600),
(32, 8, 15, 380),
(33, 7, 16, 580),
(34, 7, 17, 580),
(35, 9, 18, 700),
(36, 7, 19, 580),
(37, 8, 20, 380),
(38, 8, 21, 380),
(39, 9, 22, 700),
(40, 8, 23, 380),
(41, 7, 24, 580),
(42, 7, 25, 580),
(43, 9, 26, 700),
(65, 1, 3, 1000),
(66, 1, 5, 1000),
(67, 1, 4, 1000),
(68, 1, 41, 50000),
(69, 4, 52, 123),
(70, 1, 53, 1000),
(71, 4, 54, 12000),
(72, 8, 55, 5000),
(73, 7, 56, 10000),
(74, 10, 57, 40000),
(75, 1, 58, 40000),
(76, 9, 59, 30000),
(77, 1, 60, 15000),
(78, 9, 61, 15000),
(79, 4, 72, 1000),
(80, 8, 65, 10000),
(81, 9, 65, 1000),
(82, 8, 68, 500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_banco`
--

CREATE TABLE `tipo_banco` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `numero_cuenta` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_banco`
--

INSERT INTO `tipo_banco` (`id`, `nombre`, `numero_cuenta`, `valor`, `fecha_creada`) VALUES
(1, 'Banco Costa Rica', 102, 0, '2021-08-03'),
(2, 'Coocique', 103, 0, '2021-08-03'),
(4, 'Bac San Jose', 104, 0, '2021-08-09'),
(15, 'BP', 0, 0, '2021-12-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_categoria_venta`
--

CREATE TABLE `tipo_categoria_venta` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_categoria_venta`
--

INSERT INTO `tipo_categoria_venta` (`id`, `nombre`, `codigo`, `estado`, `fecha_creada`) VALUES
(8, '123', '123', 1, '2021-11-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `id` int(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor_extra` int(250) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_cliente`
--

INSERT INTO `tipo_cliente` (`id`, `nombre`, `valor_extra`, `estado`, `fecha_creada`) VALUES
(39, 'Huesped de habitación', 1, 0, '2021-06-09'),
(40, 'Cliente Habitual', 0, 0, '2021-06-09'),
(51, 'Cliente Existente', 2, 0, '2021-11-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_comprobante`
--

CREATE TABLE `tipo_comprobante` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_comprobante`
--

INSERT INTO `tipo_comprobante` (`id`, `nombre`, `estado`) VALUES
(1, 'Ticket', 1),
(2, 'Boleta', 2),
(3, 'Factura Fisica', 3),
(7, 'Factura Electrónica ', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descipcion` varchar(600) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id`, `nombre`, `descipcion`, `valor`, `fecha_creada`) VALUES
(2, 'NITE', 'Número de identificación tributario para personas físicas (NITE)', 4, '2018-02-15 09:24:24'),
(71, 'Persona física', 'Persona física costarricense', 1, '2021-08-03 16:44:55'),
(72, 'Persona jurídica', 'Persona jurídica costarricense estatal (pública) o persona jurídica costarricense privada', 2, '2021-08-03 16:45:09'),
(79, 'DIMEX', 'Persona física extranjera (DIMEX)', 3, '2021-11-09 10:23:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `banco` int(11) NOT NULL,
  `numero_aprobacion` int(11) NOT NULL,
  `numero_tarjeta` int(11) NOT NULL,
  `valor_hacienda` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id`, `nombre`, `valor`, `banco`, `numero_aprobacion`, `numero_tarjeta`, `valor_hacienda`, `estado`, `fecha_creada`) VALUES
(1, 'Transferencia Bancaria', 0, 1, 0, 0, '04', 0, '2018-02-15 09:25:24'),
(2, 'Tarjeta de Debito/Credito', 1, 1, 0, 0, '02', 0, '2018-02-15 09:25:24'),
(3, 'Deposito / Recaudado por terceros', 0, 1, 1, 0, '05', 0, '2018-08-22 00:00:00'),
(40, 'Cheque', 1, 0, 0, 0, '03', 0, '2022-02-23 12:43:06'),
(42, 'Efectivo ', 1, 0, 0, 0, '01', 0, '2022-02-23 12:45:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_proceso`
--

CREATE TABLE `tipo_proceso` (
  `id` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_proceso`
--

INSERT INTO `tipo_proceso` (`id`, `id_tipo_cliente`, `nombre`, `valor`, `fecha_creada`) VALUES
(19, 39, 'Cancelado', 1, '2021-06-09'),
(20, 40, 'Cancelado', 1, '2021-06-09'),
(23, 39, 'Cargar a Habitacion', 0, '2021-06-10'),
(27, 51, 'Cancelado', 0, '2021-12-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE `tmp` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad_tmp` int(11) DEFAULT NULL,
  `precio_tmp` double DEFAULT NULL,
  `precio_tmp_exo` double DEFAULT NULL,
  `sessionn_id` varchar(255) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_de_medida`
--

CREATE TABLE `unidad_de_medida` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `unidad_de_medida`
--

INSERT INTO `unidad_de_medida` (`id`, `nombre`, `descripcion`, `valor`, `estado`, `fecha_creada`) VALUES
(78, 'Sp', 'Servicios Profesionales', 1, 0, '2022-10-26'),
(79, 'm', 'Metro', 1, 0, '2022-10-26'),
(80, 'kg', 'Kilogramo', 1, 0, '2022-10-27'),
(81, 's', 'Segundo', 1, 0, '2022-11-03'),
(82, 'A', 'Ampere', 1, 0, '2022-11-03'),
(83, 'K', 'Kelvin', 1, 0, '2022-11-03'),
(84, 'mol', 'Mol', 1, 0, '2022-11-03'),
(85, 'cd', 'Candela', 1, 0, '2022-11-03'),
(86, 'm²', 'metro cuadrado', 1, 0, '2022-11-03'),
(87, 'm³', 'metro cúbico', 1, 0, '2022-11-03'),
(88, 'm/s', 'metro por segundo', 1, 0, '2022-11-03'),
(89, 'm/s²', 'metro por segundo cuadrado', 1, 0, '2022-11-03'),
(90, '1/m', '1 por metro', 1, 0, '2022-11-03'),
(91, 'kg/m³', 'kilogramo por metro cúbico', 1, 0, '2022-11-03'),
(92, 'A/m²', 'ampere por metro cuadrado', 1, 0, '2022-11-03'),
(93, 'A/m', 'ampere por metro', 1, 0, '2022-11-03'),
(94, 'mol/m³', 'mol por metro cúbico', 1, 0, '2022-02-03'),
(95, 'cd/m²', 'candela por metro cuadrado', 1, 0, '2022-02-03'),
(96, '1', 'uno (indice de refracción)', 1, 0, '2022-02-03'),
(97, 'rad', 'radián', 1, 0, '2022-02-03'),
(98, 'sr', 'estereorradián', 1, 0, '2022-02-03'),
(99, 'Hz', 'hertz', 1, 0, '2022-02-03'),
(100, 'N', 'newton', 1, 0, '2022-02-03'),
(101, 'Pa', 'pascal', 1, 0, '2022-02-03'),
(102, 'J', 'Joule', 1, 0, '2022-02-03'),
(103, 'W', 'Watt', 1, 0, '2022-02-03'),
(104, 'C', 'coulomb', 1, 0, '2022-02-03'),
(105, 'V', 'volt', 1, 0, '2022-02-03'),
(106, 'F', 'farad', 1, 0, '2022-02-03'),
(107, 'Ω', 'ohm', 1, 0, '2022-02-03'),
(108, 'S', 'siemens', 1, 0, '2022-02-03'),
(109, 'Wb', 'weber', 1, 0, '2022-02-03'),
(110, 'T', 'tesla', 1, 0, '2022-02-03'),
(111, 'H', 'henry', 1, 0, '2022-02-03'),
(112, '°C', 'grado Celsius', 1, 0, '2022-02-03'),
(113, 'lm', 'lumen', 1, 0, '2022-02-03'),
(114, 'lx', 'lux', 1, 0, '2022-02-03'),
(115, 'Bq', 'Becquerel', 1, 0, '2022-02-03'),
(116, 'Gy', 'gray', 1, 0, '2022-02-03'),
(117, 'Sv', 'sievert', 1, 0, '2022-02-03'),
(118, 'kat', 'katal', 1, 0, '2022-02-03'),
(119, 'Pa·s', 'pascal segundo', 1, 0, '2022-02-03'),
(120, 'N·m', 'newton metro', 1, 0, '2022-02-03'),
(121, 'N/m', 'newton por metro', 1, 0, '2022-02-03'),
(122, 'rad/s', 'radián por segundo', 1, 0, '2022-02-03'),
(123, 'rad/s²', 'radián por segundo cuadrado', 1, 0, '2022-02-03'),
(124, 'W/m²', 'watt por metro cuadrado', 1, 0, '2022-02-03'),
(125, 'J/K', 'joule por kelvin', 1, 0, '2022-02-03'),
(126, 'J/(kg·K)', 'joule por kilogramo kelvin', 1, 0, '2022-02-03'),
(127, 'J/kg', 'joule por kilogramo', 1, 0, '2022-02-03'),
(128, 'W/(m·K)', 'watt por metro kevin', 1, 0, '2022-02-03'),
(129, 'J/m³', 'joule por metro cúbico', 1, 0, '2022-02-03'),
(130, 'V/m', 'volt por metro', 1, 0, '2022-02-03'),
(131, 'C/m³', 'coulomb por metro cúbico', 1, 0, '2022-02-03'),
(132, 'C/m²', 'coulomb por metro cuadrado', 1, 0, '2022-02-03'),
(133, 'F/m', 'farad por metro', 1, 0, '2022-02-03'),
(134, 'H/m', 'henry por metro', 1, 0, '2022-02-03'),
(135, 'J/mol', 'joule por mol', 1, 0, '2022-02-03'),
(136, 'J/(mol·K)', 'joule por mol kelvin', 1, 0, '2022-02-03'),
(137, 'C/kg', 'coulomb por kilogramo', 1, 0, '2022-02-03'),
(138, 'Gy/s', 'gray por segundo', 1, 0, '2022-02-03'),
(139, 'W/sr', 'watt por estereorradián', 1, 0, '2022-02-03'),
(140, 'W/(m²·sr)', 'watt por metro cuadrado estereorradián', 1, 0, '2022-02-03'),
(141, 'kat/m³', 'katal por metro cúbico', 1, 0, '2022-02-03'),
(142, 'min', 'minuto', 1, 0, '2022-02-03'),
(143, 'h', 'hora', 1, 0, '2022-02-03'),
(144, 'd', 'día', 1, 0, '2022-02-03'),
(145, 'º', 'grado', 1, 0, '2022-02-03'),
(146, '´', 'minuto', 1, 0, '2022-02-03'),
(147, '´´', 'segundo', 1, 0, '2022-02-03'),
(148, 'L', 'litro', 1, 0, '2022-02-03'),
(149, 't', 'tonelada', 1, 0, '2022-02-03'),
(150, 'Np', 'neper', 1, 0, '2022-02-03'),
(151, 'B', 'bel', 1, 0, '2022-02-03'),
(152, 'eV', 'electronvolt', 1, 0, '2022-02-03'),
(153, 'u', 'unidad de masa atómica unificada', 1, 0, '2022-02-03'),
(154, 'ua', 'unidad astronómica', 1, 0, '2022-02-03'),
(155, 'Unid', 'Unidad', 1, 0, '2022-02-03'),
(156, 'Gal', 'Galón', 1, 0, '2022-02-03'),
(157, 'g', 'Gramo', 1, 0, '2022-02-03'),
(158, 'Km', 'Kilometro', 1, 0, '2022-02-03'),
(159, 'ln', 'pulgada', 1, 0, '2022-02-03'),
(160, 'cm', 'centimetro', 1, 0, '2022-02-03'),
(161, 'mL', 'mililitro', 1, 0, '2022-02-03'),
(162, 'mm', 'Milimetro', 1, 0, '2022-02-03'),
(163, 'Oz', 'Onzas', 1, 0, '2022-02-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `pago` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `created_at`, `pago`) VALUES
(1, 'Administrador', 'admin', 'admin', 'admin@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2016-12-13 09:08:03', 0),
(4, 'Gabriel', 'Quesada', 'gabrielqa', 'AmbientePruebas', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2021-06-24 14:49:36', 0),
(5, 'Esteban ', 'Alcanzas ', 'Esteban', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:01:26', 0),
(6, 'Priscilla ', 'Cordero', 'Priscilla', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:01:41', 0),
(7, 'Loana ', 'Rodriguez', 'Loana', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:00', 0),
(8, 'Bryan', 'Alpizar', 'Bryan', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:17', 0),
(9, 'Josue', 'Martínez ', 'Josue', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:02:36', 0),
(10, 'Maria', 'Carvajal', 'Maria', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:52', 0),
(11, 'Juan', 'Lopez', 'Juan', 'Gabrielqqquesada@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:39:14', 0),
(12, 'Rodrigo', 'Vega', 'Rodrigo', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:59:50', 0),
(13, 'Valeria', 'Arguellas', 'Valeria', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:08', 0),
(14, 'Valentina', 'Romp', 'Valentina', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:26', 0),
(15, 'Joshua', 'Vega', 'Joshua', 'Gabrielqqquesada@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:46', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utilidad_producto`
--

CREATE TABLE `utilidad_producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `utilidad_producto`
--

INSERT INTO `utilidad_producto` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(1, ' 30%', 30, 1, '2021-10-27'),
(2, '17%', 10, 1, '2021-10-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `id_tipo_comprobante` int(11) DEFAULT NULL,
  `nro_comprobante` varchar(25) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `total` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `id_tipo_comprobante`, `nro_comprobante`, `id_proveedor`, `id_tipo_pago`, `tipo_operacion`, `total`, `id_usuario`, `id_caja`, `fecha_creada`) VALUES
(371, 1, '2312', 753, 1, 1, 150.35328, 4, 121, '2021-12-07 11:20:28'),
(372, 1, '12121', 754, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:20:46'),
(373, 2, '12', 751, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:21:16'),
(374, 1, '13321', 746, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:29:23'),
(375, 1, '1231', 746, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:29:58'),
(376, 1, '12121', 746, 1, 1, 150.35328, 4, 121, '2021-12-09 15:42:56'),
(377, 2, '1212', 751, 1, 1, 150.35328, 4, 121, '2021-12-09 15:43:48'),
(378, 1, '123123', 751, 1, 1, 150.35328, 4, 121, '2021-12-09 15:45:00'),
(379, 2, '2001', NULL, 1, 2, 133, 10, 130, '2022-02-03 16:46:11'),
(380, 1, '1312', NULL, 1, 2, 1000, 10, 130, '2022-02-03 16:54:03'),
(381, 3, '1', 879, 2, 1, 150.35328, 10, 130, '2022-02-03 17:03:46'),
(382, 3, '1', 880, 2, 1, 307006.875, 10, 130, '2022-02-03 17:10:16'),
(383, 3, '21122', 881, 2, 1, 150.35328, 4, 131, '2022-02-15 13:15:23'),
(384, 3, '1232', 0, 2, 1, 2684.88, 4, 131, '2022-02-17 13:31:59'),
(385, 3, '12323', 885, 2, 1, 2684.88, 4, 131, '2022-02-17 13:33:01'),
(386, 3, '2323', 0, 2, 1, 2684.88, 4, 131, '2022-02-17 13:33:40'),
(387, 3, '2331', 0, 2, 1, 0, 4, 131, '2022-02-17 13:35:06'),
(388, 3, '2122', 0, 2, 1, 0, 4, 131, '2022-02-17 13:38:39'),
(389, 3, '121', 889, 2, 1, 2684.88, 4, 131, '2022-02-17 13:40:08'),
(390, 3, '123', 0, 2, 1, 0, 4, 131, '2022-02-17 14:10:37'),
(391, 3, '123', 0, 2, 1, 0, 4, 131, '2022-02-17 14:11:28'),
(392, 3, '231', 0, 2, 1, 0, 4, 131, '2022-02-17 14:24:46'),
(393, 3, '321', 0, 2, 1, 0, 4, 131, '2022-02-17 14:33:15'),
(394, 3, '2312', 0, 2, 1, 0, 4, 131, '2022-02-17 14:33:43'),
(395, 3, '123', 0, 2, 1, 0, 4, 131, '2022-02-17 14:34:19'),
(396, 3, '16654', 0, 2, 1, 5843.2752, 4, 131, '2022-02-17 14:45:47'),
(397, 3, '123', 0, 2, 1, 1275.318, 4, 131, '2022-02-17 14:51:28'),
(398, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-17 14:59:10'),
(399, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-17 14:59:58'),
(400, 3, '1312', 881, 2, 1, 2684.88, 4, 131, '2022-02-17 15:04:22'),
(401, 3, '554', 881, 2, 1, 10739.52, 4, 131, '2022-02-17 16:49:03'),
(402, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-17 16:51:03'),
(403, 3, '123', 904, 2, 1, 5843.2752, 4, 131, '2022-02-17 17:05:40'),
(404, 3, '8874', 881, 2, 1, 10739.52, 4, 131, '2022-02-18 14:56:44'),
(405, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 15:00:47'),
(406, 3, '412', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 15:08:20'),
(407, 3, '4412', 881, 2, 1, 2711.7288, 4, 131, '2022-02-18 15:10:27'),
(408, 3, '331', 881, 2, 1, 10739.52, 4, 131, '2022-02-18 15:12:36'),
(409, 3, '4123', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 15:20:56'),
(410, 3, '858', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 15:23:15'),
(411, 3, '441', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 15:26:03'),
(412, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 15:31:58'),
(413, 3, '123', 881, 2, 1, 5369.76, 4, 131, '2022-02-18 15:37:21'),
(414, 3, '2421', 881, 2, 1, 2711.7288, 4, 131, '2022-02-18 15:39:44'),
(415, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 15:41:40'),
(416, 3, '3312', 881, 2, 1, 10739.52, 4, 131, '2022-02-18 15:45:50'),
(417, 3, '3312', 881, 2, 1, 12108.8088, 4, 131, '2022-02-18 15:48:19'),
(418, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 15:53:14'),
(419, 3, '4123', 881, 2, 1, 9397.08, 4, 131, '2022-02-18 15:54:26'),
(420, 3, '41212', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 16:04:39'),
(421, 3, '3232', 881, 2, 1, 7118.5932, 4, 131, '2022-02-18 16:30:34'),
(422, 3, '12323', 881, 2, 1, 9397.08, 4, 131, '2022-02-18 16:31:53'),
(423, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 16:32:29'),
(424, 3, '232', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 16:33:35'),
(425, 3, '321', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 16:34:10'),
(426, 3, '787', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 20:25:59'),
(427, 3, '123', 930, 2, 1, 10739.52, 4, 131, '2022-02-18 20:32:30'),
(428, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 08:55:21'),
(429, 3, '321', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 08:56:08'),
(430, 3, '221', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 08:58:49'),
(431, 3, '214', 934, 2, 1, 5843.2752, 4, 131, '2022-02-19 08:59:42'),
(432, 3, '20787', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:06:59'),
(433, 3, '241', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:10:35'),
(434, 3, '231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 09:13:57'),
(435, 3, '1412', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 09:16:03'),
(436, 3, '1241', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:19:35'),
(437, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:26:14'),
(438, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:28:33'),
(439, 3, '123', 881, 2, 1, 12014.838, 4, 131, '2022-02-19 09:29:59'),
(440, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:30:31'),
(441, 3, '12', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:31:40'),
(442, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:31:55'),
(443, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:32:32'),
(444, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 09:33:01'),
(445, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:33:35'),
(446, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:36:20'),
(447, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:37:58'),
(448, 3, '123', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 09:42:52'),
(449, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:43:33'),
(450, 3, '321', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 09:46:10'),
(451, 3, '2', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:52:22'),
(452, 3, '21', 881, 2, 1, 5369.76, 4, 131, '2022-02-19 09:52:33'),
(453, 3, '213', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:54:36'),
(454, 3, '1231', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 09:55:40'),
(455, 3, '211', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 10:00:37'),
(456, 3, '231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 10:10:17'),
(457, 3, '12', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 10:17:26'),
(458, 3, '31', 881, 2, 1, 5369.76, 4, 131, '2022-02-19 10:26:22'),
(459, 3, '123', 881, 2, 1, 2819.124, 4, 131, '2022-02-19 10:28:42'),
(460, 3, '88987', 881, 2, 1, 5369.76, 4, 131, '2022-02-19 10:30:39'),
(461, 3, '79887', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:34:16'),
(462, 3, '5456', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:35:39'),
(463, 3, '987', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 10:38:54'),
(464, 3, '65', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 10:42:05'),
(465, 3, '987', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:42:55'),
(466, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 10:53:05'),
(467, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:54:04'),
(468, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:55:20'),
(469, 3, '2011471', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 10:57:53'),
(470, 3, '312', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 11:08:00'),
(471, 3, '987', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:17:04'),
(472, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 11:22:24'),
(473, 3, '1233', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 11:24:19'),
(474, 3, '1231', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 11:25:26'),
(475, 3, '231', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 11:27:17'),
(476, 3, '123', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:28:01'),
(477, 3, '123', 881, 2, 1, 13451.2488, 4, 131, '2022-02-19 11:40:31'),
(478, 3, '31', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 11:42:34'),
(479, 3, '31', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:45:07'),
(480, 3, '3123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 11:46:27'),
(481, 3, '3213', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:50:21'),
(482, 3, '313', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:51:43'),
(483, 3, '33132', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:58:22'),
(484, 3, '312', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 11:59:41'),
(485, 3, '321', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 08:37:47'),
(486, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 08:38:48'),
(487, 3, '321', 881, 2, 1, 2711.7288, 4, 131, '2022-02-21 08:40:27'),
(488, 3, '321', 881, 2, 1, 2711.7288, 4, 131, '2022-02-21 08:43:20'),
(489, 3, '321', 881, 2, 1, 2684.88, 4, 131, '2022-02-21 08:44:16'),
(490, 3, '312', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 08:54:55'),
(491, 3, '312', 881, 2, 1, 2684.88, 4, 131, '2022-02-21 08:55:52'),
(492, 3, '123', 881, 2, 1, 7118.5932, 4, 131, '2022-02-21 08:56:54'),
(493, 3, '331', 881, 2, 1, 10739.52, 4, 131, '2022-02-21 08:57:02'),
(494, 3, '441', 881, 2, 1, 5843.2752, 4, 131, '2022-02-21 08:57:07'),
(495, 3, '321', 881, 2, 1, 5843.2752, 4, 131, '2022-02-21 11:07:55'),
(496, 3, '31', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 11:09:06'),
(497, 3, '3123', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 17:13:49'),
(498, 3, '231', 881, 2, 1, 10739.52, 4, 131, '2022-02-21 17:15:20'),
(499, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-22 09:25:33'),
(500, 3, '231', 881, 2, 1, 5843.2752, 4, 131, '2022-02-22 10:43:47'),
(501, 3, '22', 881, 2, 1, 1275.318, 4, 131, '2022-02-23 08:18:48'),
(502, 3, '1231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-23 08:21:22'),
(503, 3, '2131', 881, 2, 1, 5843.2752, 4, 131, '2022-02-23 08:27:17'),
(504, 3, '1231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-23 08:46:46'),
(505, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-23 08:49:26'),
(506, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-23 08:50:43'),
(507, 3, '8417', 881, 3, 1, 5843.2752, 4, 131, '2022-02-23 11:07:03'),
(508, 3, '323', 881, 2, 1, 14766.84, 4, 131, '2022-02-23 12:57:44'),
(509, 3, '312', 881, 2, 1, 2684.88, 4, 131, '2022-02-23 13:12:56'),
(510, 3, '2321', 881, 2, 1, 10739.52, 4, 131, '2022-02-23 13:23:07'),
(511, 3, '11254', 881, 2, 1, 5369.76, 4, 131, '2022-02-23 15:40:54'),
(512, 3, '123', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:05:31'),
(513, 3, '123', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:18:05'),
(514, 3, '32', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:21:49'),
(515, 3, '123', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:24:51'),
(516, 3, '321', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:26:01'),
(517, 3, '123', 881, 2, 1, 7.02295, 4, 131, '2022-02-24 09:00:06'),
(518, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:13:40'),
(519, 3, '21', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:19:13'),
(520, 3, '1312', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:21:51'),
(521, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:24:41'),
(522, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:42:55'),
(523, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:46:11'),
(524, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:51:55'),
(525, 3, '312', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:55:19'),
(526, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 10:59:20'),
(527, 3, '312', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:01:25'),
(528, 3, '3123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:13:39'),
(529, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:16:32'),
(530, 3, '231', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:19:20'),
(531, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:25:06'),
(532, 3, '3123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:26:11'),
(533, 3, '321', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:32:08'),
(534, 3, '321', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:32:45'),
(535, 3, '321', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:39:08'),
(536, 3, '231', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:40:23'),
(537, 3, '321', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:43:21'),
(538, 3, '321', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:47:33'),
(539, 3, '123', 881, 2, 1, 5.763, 4, 131, '2022-02-24 11:50:02'),
(540, 3, '13', 881, 2, 1, 2711.7288, 4, 131, '2022-02-24 11:52:52'),
(541, 3, '123', 881, 2, 1, 2510.86, 4, 131, '2022-02-24 11:53:55'),
(542, 3, '123', 881, 2, 1, 2510.86, 4, 131, '2022-02-24 11:59:14'),
(543, 3, '213', 881, 2, 1, 5.763, 4, 131, '2022-02-24 12:16:18'),
(544, 3, '123', 881, 2, 1, 2510.86, 4, 131, '2022-02-24 12:17:01'),
(545, 3, '1', 881, 2, 1, 5843.2752, 4, 131, '2022-03-04 09:38:54'),
(546, 3, '123', 881, 2, 1, 5171.04, 4, 131, '2022-03-04 10:36:40'),
(547, 3, '123', 881, 2, 1, 5171.04, 4, 131, '2022-03-04 10:37:25'),
(548, 3, '123', 881, 2, 1, 5171.04, 4, 131, '2022-03-04 10:43:39'),
(549, 3, '123', 881, 2, 1, 5171.04, 4, 131, '2022-03-04 10:45:47'),
(550, 3, '231', 881, 2, 1, 5171.04, 4, 131, '2022-03-04 10:49:00'),
(551, 3, '123', 881, 2, 1, 1045, 4, 131, '2022-03-04 10:52:35'),
(552, 3, '231', 881, 2, 1, 5171.04, 4, 131, '2022-03-04 10:52:58'),
(553, 3, '213', 881, 2, 1, 5171.04, 4, 131, '2022-03-04 10:54:09'),
(554, 3, '123', 881, 2, 1, 2020, 4, 131, '2022-03-04 12:52:15'),
(555, 3, '231', 881, 2, 1, 1045, 4, 131, '2022-03-04 14:09:35'),
(556, 3, '123', 881, 2, 1, 1045, 4, 131, '2022-03-04 14:15:06'),
(557, 3, '312', 881, 2, 1, 1045, 4, 131, '2022-03-04 14:18:54'),
(558, 3, '123', 881, 2, 1, 2282.6, 4, 131, '2022-03-04 17:44:56'),
(559, 3, '123', 881, 2, 1, 4565.2, 4, 131, '2022-03-04 18:13:50'),
(560, 3, '132', 881, 2, 1, 2282.6, 4, 131, '2022-03-04 18:17:11'),
(561, 3, '321', 881, 2, 1, 4565.2, 4, 131, '2022-03-04 18:23:38'),
(562, 3, '312', 881, 2, 1, 4565.2, 4, 131, '2022-03-04 18:29:11'),
(563, 3, '31', 881, 2, 1, 4565.2, 4, 131, '2022-03-04 18:30:28'),
(564, 3, '21', 881, 2, 1, 4565.2, 4, 131, '2022-03-04 18:31:46'),
(565, 3, '321', 881, 2, 1, 4565.2, 4, 131, '2022-03-04 18:34:24'),
(566, 3, '123', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 12:44:35'),
(567, 3, '12', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 13:25:16'),
(568, 3, '321', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 13:27:50'),
(569, 3, '321', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 13:33:01'),
(570, 3, '3213', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 13:35:17'),
(571, 3, '12', 881, 2, 1, 9130.4, 4, 131, '2022-03-07 13:36:42'),
(572, 3, '3213', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 13:41:01'),
(573, 3, '312', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 13:59:59'),
(574, 3, '12', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:02:33'),
(575, 3, '312', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:03:27'),
(576, 3, '123', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:04:05'),
(577, 3, '312', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:08:23'),
(578, 3, '32', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:09:25'),
(579, 1, '2131', NULL, 1, 2, 200, 4, 131, '2022-03-07 14:10:54'),
(580, 3, '21', 881, 2, 1, 6847.8, 4, 131, '2022-03-07 14:11:14'),
(581, 3, '312', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:11:59'),
(582, 3, '312', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:13:08'),
(583, 3, '123', 881, 2, 1, 1045, 4, 131, '2022-03-07 14:16:40'),
(584, 3, '312', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:17:14'),
(585, 3, '213', 881, 2, 1, 4752, 4, 131, '2022-03-07 14:18:31'),
(586, 3, '213', 881, 2, 1, 2222, 4, 131, '2022-03-07 14:18:55'),
(587, 3, '312', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:19:47'),
(588, 3, '312', 881, 2, 1, 2222, 4, 131, '2022-03-07 14:23:55'),
(589, 3, '3213', 881, 2, 1, 4752, 4, 131, '2022-03-07 14:24:24'),
(590, 3, '312', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:25:19'),
(591, 3, '323', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:38:39'),
(592, 3, '312', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:41:35'),
(593, 3, '123', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:46:05'),
(594, 3, '21', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:47:03'),
(595, 3, '323', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:49:50'),
(596, 3, '32', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:50:40'),
(597, 3, '321', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:52:00'),
(598, 3, '323', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:52:52'),
(599, 3, '32', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:53:34'),
(600, 3, '323', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:54:13'),
(601, 3, '323', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 14:56:54'),
(602, 3, '312', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:58:54'),
(603, 3, '12', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 14:59:37'),
(604, 3, '32', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:02:22'),
(605, 3, '12', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:05:01'),
(606, 3, '321', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:08:32'),
(607, 3, '3213', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:09:11'),
(608, 3, '323', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:11:59'),
(609, 3, '323', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:13:20'),
(610, 3, '333', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:14:29'),
(611, 3, '323', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:15:08'),
(612, 3, '212', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:15:21'),
(613, 3, '3213', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 15:36:10'),
(614, 3, '12', 881, 2, 1, 2282.6, 4, 131, '2022-03-07 15:38:59'),
(615, 3, '12', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 15:39:22'),
(616, 3, '312', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 15:55:37'),
(617, 3, '32', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 16:00:07'),
(618, 3, '312', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 16:16:23'),
(619, 3, '323', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 16:19:01'),
(620, 3, '32', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 16:40:33'),
(621, 3, '32', 881, 2, 1, 4565.2, 4, 131, '2022-03-07 16:41:34'),
(622, 3, '323', 881, 2, 1, 4565.2, 4, 131, '2022-03-08 08:11:43'),
(623, 3, '123', 881, 2, 1, 1100, 4, 131, '2022-03-08 08:26:29'),
(624, 3, '132', 881, 2, 1, 1100, 4, 131, '2022-03-08 08:27:31'),
(625, 3, '123', 881, 2, 1, 1100, 4, 131, '2022-03-08 08:28:14'),
(626, 3, '13', 881, 2, 1, 1100, 4, 131, '2022-03-08 08:29:57'),
(627, 3, '321', 881, 2, 1, 1100, 4, 131, '2022-03-08 08:31:08'),
(628, 3, '312', 881, 2, 1, 1100, 4, 131, '2022-03-08 08:37:30'),
(629, 3, '412', 881, 2, 1, 4565.2, 4, 131, '2022-03-08 08:37:57'),
(630, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 08:52:04'),
(631, 3, '123', 881, 2, 1, 1045, 4, 131, '2022-03-08 09:06:39'),
(632, 3, '123', 881, 2, 1, 2090, 4, 131, '2022-03-08 09:07:11'),
(633, 3, '123', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 09:07:46'),
(634, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 09:08:12'),
(635, 3, '123', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 09:25:24'),
(636, 3, '312', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 09:25:56'),
(637, 3, '312', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 09:26:50'),
(638, 3, '3213', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 09:27:14'),
(639, 3, '321', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 09:28:04'),
(640, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 09:29:26'),
(641, 3, '123', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 09:35:35'),
(642, 3, '123', 881, 2, 1, 2282.6, 4, 131, '2022-03-08 09:40:03'),
(643, 3, '123', 881, 2, 1, 2282.6, 4, 131, '2022-03-08 09:43:57'),
(644, 3, '123', 881, 2, 1, 2282.6, 4, 131, '2022-03-08 09:44:53'),
(645, 3, '123', 881, 2, 1, 4565.2, 4, 131, '2022-03-08 09:46:55'),
(646, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 09:51:05'),
(647, 3, '123', 881, 2, 1, 4620, 4, 131, '2022-03-08 09:51:26'),
(648, 3, '123', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 09:52:04'),
(649, 3, '123', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 10:02:31'),
(650, 3, '123', 881, 2, 1, 4106.66, 4, 131, '2022-03-08 10:05:27'),
(651, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 10:07:38'),
(652, 3, '123', 881, 2, 1, 2053.33, 4, 131, '2022-03-08 10:16:38'),
(653, 3, '123', 881, 2, 1, 4106.66, 4, 131, '2022-03-08 10:18:44'),
(654, 3, '23', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 10:19:48'),
(655, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 10:23:21'),
(656, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 10:27:51'),
(657, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 10:31:37'),
(658, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 10:32:18'),
(659, 3, '123', 881, 2, 1, 3098.33, 4, 131, '2022-03-08 10:34:49'),
(660, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:01:24'),
(661, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:10:43'),
(662, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:11:31'),
(663, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:12:31'),
(664, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:13:13'),
(665, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:17:07'),
(666, 3, '12', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:18:55'),
(667, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:25:36'),
(668, 3, '132', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:26:40'),
(669, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:27:25'),
(670, 3, '123', 881, 2, 1, 2310, 4, 131, '2022-03-08 11:31:01'),
(671, 3, '1231', 881, 2, 1, 2282.6, 4, 131, '2022-03-08 22:09:24'),
(672, 3, '123', 881, 2, 1, 2020, 4, 131, '2022-03-08 22:19:32'),
(673, 3, '123', 881, 2, 1, 2020, 4, 131, '2022-03-08 22:21:56'),
(674, 3, '123', 881, 2, 1, 2020, 4, 131, '2022-03-08 22:22:40'),
(675, 3, '123', 881, 2, 1, 1243, 4, 131, '2022-03-08 22:26:50'),
(676, 3, '123', 881, 2, 1, 1243, 4, 131, '2022-03-08 22:29:11'),
(677, 3, '123', 881, 2, 1, 4788, 4, 131, '2022-03-08 22:42:30'),
(678, 3, '32', 881, 2, 1, 4788, 4, 131, '2022-03-08 23:04:01'),
(679, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-08 23:06:47'),
(680, 3, '123', 881, 2, 1, 2020, 4, 131, '2022-03-08 23:09:37'),
(681, 3, '132', 881, 2, 1, 1243, 4, 131, '2022-03-08 23:11:42'),
(682, 3, '65', 881, 2, 1, 6031, 4, 131, '2022-03-08 23:12:14'),
(683, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-08 23:13:48'),
(684, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-08 23:14:08'),
(685, 3, '13', 881, 2, 1, 6031, 4, 131, '2022-03-08 23:52:08'),
(686, 3, '12', 881, 2, 1, 6031, 4, 131, '2022-03-08 23:56:58'),
(687, 3, '32', 881, 2, 1, 6031, 4, 131, '2022-03-09 00:01:13'),
(688, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 00:01:53'),
(689, 3, '321', 881, 2, 1, 6031, 4, 131, '2022-03-09 00:04:21'),
(690, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 00:05:25'),
(691, 3, '321', 881, 2, 1, 6031, 4, 131, '2022-03-09 00:06:04'),
(692, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 00:06:46'),
(693, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 00:20:32'),
(694, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:06:47'),
(695, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:09:52'),
(696, 3, '321', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:16:30'),
(697, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:18:31'),
(698, 3, '3213', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:27:05'),
(699, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:36:29'),
(700, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:38:10'),
(701, 3, '3213', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:39:59'),
(702, 3, '321', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:44:08'),
(703, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:47:53'),
(704, 3, '12', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:48:20'),
(705, 3, '321', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:49:42'),
(706, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:50:08'),
(707, 3, '33', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:50:45'),
(708, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 08:55:31'),
(709, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 09:02:21'),
(710, 3, '321', 881, 2, 1, 6031, 4, 131, '2022-03-09 09:02:37'),
(711, 3, '123', 881, 2, 1, 1243, 4, 131, '2022-03-09 09:05:02'),
(712, 3, '213', 881, 2, 1, 4788, 4, 131, '2022-03-09 09:05:26'),
(713, 3, '123', 881, 2, 1, 9576, 4, 131, '2022-03-09 09:07:51'),
(714, 3, '123', 881, 2, 1, 2486, 4, 131, '2022-03-09 09:08:30'),
(715, 3, '231', 881, 2, 1, 6031, 4, 131, '2022-03-09 09:09:25'),
(716, 3, '231', 881, 2, 1, 7274, 4, 131, '2022-03-09 09:10:54'),
(717, 2, '4123', NULL, 1, 2, 50000, 4, 131, '2022-03-09 09:18:42'),
(718, 3, '12', 881, 2, 1, 2053.33, 4, 131, '2022-03-09 09:19:24'),
(719, 3, '123', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 09:46:53'),
(720, 3, '312', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 10:00:30'),
(721, 3, '12', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 10:48:43'),
(722, 3, '231', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 10:59:30'),
(723, 3, '321', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:01:33'),
(724, 3, '123', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:03:56'),
(725, 3, '321', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:12:00'),
(726, 3, '321', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:13:07'),
(727, 3, '312', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:14:16'),
(728, 3, '31', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:14:45'),
(729, 3, '231', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:22:29'),
(730, 3, '312', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 11:26:52'),
(731, 3, '312', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:37:11'),
(732, 3, '312', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:39:46'),
(733, 3, '312', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:40:23'),
(734, 3, '311', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:41:42'),
(735, 3, '312', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:43:01'),
(736, 3, '312', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:48:05'),
(737, 3, '311', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:52:41'),
(738, 3, '3213', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:53:24'),
(739, 3, '32', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:54:03'),
(740, 3, '32', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:54:47'),
(741, 3, '3112', 881, 2, 1, 1243, 4, 131, '2022-03-09 11:56:18'),
(742, 3, '312', 881, 2, 1, 1243, 4, 131, '2022-03-09 12:00:53'),
(743, 3, '23', 881, 2, 1, 1243, 4, 131, '2022-03-09 12:02:03'),
(744, 3, '123', 881, 2, 1, 1243, 4, 131, '2022-03-09 12:05:40'),
(745, 3, '31', 881, 2, 1, 1243, 4, 131, '2022-03-09 12:06:00'),
(746, 3, '123', 881, 2, 1, 1243, 4, 131, '2022-03-09 12:07:11'),
(747, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 12:11:50'),
(748, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 12:12:36'),
(749, 3, '312', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 12:13:31'),
(750, 3, '12', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 12:18:56'),
(751, 3, '312', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 12:40:45'),
(752, 3, '18', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 12:41:41'),
(753, 3, '23', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 12:43:05'),
(754, 3, '2131', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 12:53:59'),
(755, 3, '123', 881, 2, 1, 2020, 4, 131, '2022-03-09 12:55:39'),
(756, 3, '231', 881, 2, 1, 1243, 4, 131, '2022-03-09 12:57:02'),
(757, 3, '2', 881, 2, 1, 12062, 4, 131, '2022-03-09 21:06:00'),
(758, 3, '123', 881, 2, 1, 6031, 4, 131, '2022-03-09 21:46:52'),
(759, 3, '123', 881, 2, 1, 12062, 4, 131, '2022-03-09 23:09:05'),
(760, 3, '31', 881, 2, 1, 12062, 4, 131, '2022-03-09 23:19:09'),
(761, 3, '3123', 881, 2, 1, 12062, 4, 131, '2022-03-09 23:22:49'),
(762, 3, '312', 881, 2, 1, 6031, 4, 131, '2022-03-09 23:23:21'),
(763, 3, '3213', 881, 2, 1, 2320.2629, 4, 131, '2022-03-09 23:24:15'),
(764, 3, '23', 881, 2, 1, 4640.5258, 4, 131, '2022-03-09 23:25:09'),
(765, 3, '321', 881, 2, 1, 11524, 4, 131, '2022-03-11 12:14:26'),
(766, 3, '31', 881, 2, 1, 10820, 4, 131, '2022-03-11 12:19:33'),
(767, 3, '312', 881, 2, 1, 21640, 4, 131, '2022-03-11 12:25:43'),
(768, 3, '231', 881, 2, 1, 4551.64, 4, 131, '2022-03-11 12:35:50'),
(769, 3, '4123', 881, 2, 1, 3017.2, 4, 131, '2022-03-11 12:39:14'),
(770, 3, '4123', 881, 2, 1, 3017.2, 4, 131, '2022-03-11 13:01:01'),
(771, 3, '14123', 881, 2, 1, 5031.2, 4, 131, '2022-03-11 13:11:55'),
(772, 3, '3123', 881, 2, 1, 2014, 4, 131, '2022-03-11 13:20:33'),
(773, 3, '312', 881, 2, 1, 2014, 4, 131, '2022-03-11 13:25:15'),
(774, 3, '131', 881, 2, 1, 3017.2, 4, 131, '2022-03-11 13:25:49'),
(775, 3, '4123', 881, 2, 1, 4028, 4, 131, '2022-03-11 13:26:16'),
(776, 3, '412', 881, 2, 1, 6034.4, 4, 131, '2022-03-11 13:26:49'),
(777, 3, '123', 881, 2, 1, 6034.4, 4, 131, '2022-03-11 15:17:45'),
(778, 3, '13', 881, 2, 1, 2395.6, 4, 131, '2022-03-11 15:19:59'),
(779, 3, '314', 881, 2, 1, 4791.2, 4, 131, '2022-03-11 15:22:56'),
(780, 3, '31', 881, 2, 1, 7367.6, 4, 131, '2022-03-11 15:23:35'),
(781, 3, '123', 881, 2, 1, 3683.8, 4, 131, '2022-03-11 15:28:06'),
(782, 3, '312', 881, 2, 1, 2395.6, 4, 131, '2022-03-11 15:32:45'),
(783, 3, '321', 881, 2, 1, 4791.2, 4, 131, '2022-03-11 15:33:38'),
(784, 3, '3123', 881, 2, 1, 3683.8, 4, 131, '2022-03-11 15:39:25'),
(785, 3, '312', 881, 2, 1, 3683.8, 4, 131, '2022-03-11 15:42:17'),
(786, 3, '32', 881, 2, 1, 3683.8, 4, 131, '2022-03-11 15:44:21'),
(787, 3, '213', 881, 2, 1, 2395.6, 4, 131, '2022-03-11 15:47:04'),
(788, 3, '23', 881, 2, 1, 1288.2, 4, 131, '2022-03-11 15:47:36'),
(789, 3, '321', 881, 2, 1, 1288.2, 4, 131, '2022-03-11 15:49:04'),
(790, 3, '32', 881, 2, 1, 1288.2, 4, 131, '2022-03-11 15:49:49'),
(791, 3, '231', 881, 2, 1, 1288.2, 4, 131, '2022-03-11 15:57:18'),
(792, 3, '312', 881, 2, 1, 1288.2, 4, 131, '2022-03-11 16:10:46'),
(793, 3, '123', 881, 2, 1, 2576.4, 4, 131, '2022-03-11 16:39:15'),
(794, 3, '123', 881, 2, 1, 3683.8, 4, 131, '2022-03-11 16:39:55'),
(795, 3, '123', 881, 2, 1, 7367.6, 4, 131, '2022-03-11 16:40:29'),
(796, 3, '1241', NULL, 1, 2, 202, 4, 131, '2022-03-11 16:44:32'),
(797, 3, '412', 881, 2, 1, 7885, 4, 131, '2022-03-11 16:45:30'),
(798, 3, '31', 881, 2, 1, 19475, 4, 131, '2022-03-11 16:47:44'),
(799, 3, '123', 881, 2, 1, 7885, 4, 131, '2022-03-11 16:50:37'),
(800, 3, '31', 881, 2, 1, 37131.8, 4, 131, '2022-03-11 16:51:16'),
(801, 3, '1231', 881, 2, 1, 3683.8, 4, 131, '2022-03-11 16:52:34'),
(802, 3, '412', 881, 2, 1, 5874.2, 4, 131, '2022-03-11 16:53:08'),
(803, 3, '123', 881, 2, 1, 28052, 4, 131, '2022-03-11 16:55:40'),
(804, 3, '123', 881, 2, 1, 2275.82, 4, 131, '2022-03-11 16:56:57'),
(805, 3, '123', 881, 2, 1, 27595.6, 4, 131, '2022-03-11 16:57:38'),
(806, 3, '123', 881, 2, 1, 3445.46, 4, 131, '2022-03-11 16:58:38'),
(807, 3, '123', 881, 2, 1, 3445.46, 4, 131, '2022-03-11 16:59:06'),
(808, 3, '123', 881, 2, 1, 3445.46, 4, 131, '2022-03-11 17:08:13'),
(809, 3, '12', 881, 2, 1, 3445.46, 4, 131, '2022-03-12 09:26:15'),
(810, 3, '23', 881, 2, 1, 6999.22, 4, 131, '2022-03-12 09:27:23'),
(811, 3, '12', 881, 2, 1, 2275.82, 4, 131, '2022-03-12 09:42:51'),
(812, 3, '31', 881, 2, 1, 6999.22, 4, 131, '2022-03-12 09:43:40'),
(813, 3, '123', 881, 2, 1, 3445.46, 4, 131, '2022-03-12 09:44:34'),
(814, 3, '23', 881, 2, 1, 4979.52, 4, 131, '2022-03-12 09:49:28'),
(815, 3, '312', 881, 2, 1, 2275.82, 4, 131, '2022-03-14 22:45:29'),
(816, 3, '23', 881, 2, 1, 4551.64, 4, 131, '2022-03-15 09:34:55'),
(817, 3, '321', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 09:35:48'),
(818, 3, '312', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 10:08:25'),
(819, 3, '412', 881, 2, 1, 4551.64, 4, 131, '2022-03-15 11:54:23'),
(820, 3, '4123', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:01:00'),
(821, 3, '4123', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:02:38'),
(822, 3, '4123', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:04:14'),
(823, 3, '4123', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:07:05'),
(824, 3, '4123', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:13:00'),
(825, 3, '4123', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:24:03'),
(826, 3, '32', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:26:38'),
(827, 3, '4213', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:28:21'),
(828, 3, '32', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:31:04'),
(829, 3, '323', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:32:36'),
(830, 3, '23', 881, 2, 1, 4551.64, 4, 131, '2022-03-15 12:35:29'),
(831, 3, '412', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 12:39:19'),
(832, 3, '23', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 13:06:03'),
(833, 3, '32', 881, 2, 1, 2275.82, 4, 131, '2022-03-15 13:07:53'),
(834, 3, '323', 881, 2, 1, 2275.82, 4, 131, '2022-03-16 17:07:12');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_id_caja` (`id_caja`) USING BTREE;

--
-- Indices de la tabla `auto_cabys`
--
ALTER TABLE `auto_cabys`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_clientes_p`
--
ALTER TABLE `categoria_clientes_p`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_categoria` (`id_tipo_categoria`);

--
-- Indices de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `datos_factura`
--
ALTER TABLE `datos_factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descuento_productos`
--
ALTER TABLE `descuento_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalles_producto_factura`
--
ALTER TABLE `detalles_producto_factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `funcionalidades`
--
ALTER TABLE `funcionalidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impuestos_productos`
--
ALTER TABLE `impuestos_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notificaciones_stocks`
--
ALTER TABLE `notificaciones_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `documento` (`documento`(10)),
  ADD KEY `categoria_promo` (`id_categoria_p`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bodega` (`id_bodega`),
  ADD KEY `FK_unidad_de_medida` (`id_unidad_medida`),
  ADD KEY `FK_descuento` (`id_descuento`) USING BTREE,
  ADD KEY `fk_proveedor` (`id_proveedor`);

--
-- Indices de la tabla `promociones`
--
ALTER TABLE `promociones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_impuesto` (`id_impuesto`),
  ADD KEY `FK_producto` (`id_producto`) USING BTREE;

--
-- Indices de la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_venta` (`id_venta`),
  ADD KEY `id_plazo` (`id_plazo`);

--
-- Indices de la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_producto` (`id_producto`) USING BTREE,
  ADD KEY `r_habitacion` (`id_habitacion`),
  ADD KEY `r_categoria` (`id_categoria`),
  ADD KEY `r_porcentaje` (`id_porcentaje`);

--
-- Indices de la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_utilidad` (`id_utilidad`),
  ADD KEY `FK_producto` (`id_producto`);

--
-- Indices de la tabla `subimpuestos_productos`
--
ALTER TABLE `subimpuestos_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_categoria_venta`
--
ALTER TABLE `tipo_categoria_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_cliente` (`id_tipo_cliente`) USING BTREE;

--
-- Indices de la tabla `tmp`
--
ALTER TABLE `tmp`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `unidad_de_medida`
--
ALTER TABLE `unidad_de_medida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `utilidad_producto`
--
ALTER TABLE `utilidad_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT de la tabla `auto_cabys`
--
ALTER TABLE `auto_cabys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categoria_clientes_p`
--
ALTER TABLE `categoria_clientes_p`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `datos_factura`
--
ALTER TABLE `datos_factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `descuento_productos`
--
ALTER TABLE `descuento_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `detalles_producto_factura`
--
ALTER TABLE `detalles_producto_factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `funcionalidades`
--
ALTER TABLE `funcionalidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `gasto`
--
ALTER TABLE `gasto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `impuestos_productos`
--
ALTER TABLE `impuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `notificaciones_stocks`
--
ALTER TABLE `notificaciones_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1437;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433;

--
-- AUTO_INCREMENT de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1015;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=567;

--
-- AUTO_INCREMENT de la tabla `promociones`
--
ALTER TABLE `promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=882;

--
-- AUTO_INCREMENT de la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=459;

--
-- AUTO_INCREMENT de la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `subimpuestos_productos`
--
ALTER TABLE `subimpuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tipo_categoria_venta`
--
ALTER TABLE `tipo_categoria_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `tmp`
--
ALTER TABLE `tmp`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1366;

--
-- AUTO_INCREMENT de la tabla `unidad_de_medida`
--
ALTER TABLE `unidad_de_medida`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `utilidad_producto`
--
ALTER TABLE `utilidad_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=835;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD CONSTRAINT `aperturas_cajas_ibfk_1` FOREIGN KEY (`id_caja`) REFERENCES `mantenimiento_cajas` (`id`);

--
-- Filtros para la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  ADD CONSTRAINT `fk_tipo_categoria` FOREIGN KEY (`id_tipo_categoria`) REFERENCES `tipo_categoria_venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_bodega` FOREIGN KEY (`id_bodega`) REFERENCES `bodegas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_descuentos_as` FOREIGN KEY (`id_descuento`) REFERENCES `descuento_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_unidad_de_medida` FOREIGN KEY (`id_unidad_medida`) REFERENCES `unidad_de_medida` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  ADD CONSTRAINT `fk_impuesto` FOREIGN KEY (`id_impuesto`) REFERENCES `impuestos_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  ADD CONSTRAINT `plazo` FOREIGN KEY (`id_plazo`) REFERENCES `periodos_pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  ADD CONSTRAINT `categoria_promo` FOREIGN KEY (`id_categoria`) REFERENCES `categoria_clientes_p` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `promociones` FOREIGN KEY (`id_porcentaje`) REFERENCES `promociones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  ADD CONSTRAINT `FK_producto_utilidad` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_utilidad_producto` FOREIGN KEY (`id_utilidad`) REFERENCES `utilidad_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD CONSTRAINT `tipo_proceso_ibfk_1` FOREIGN KEY (`id_tipo_cliente`) REFERENCES `tipo_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
