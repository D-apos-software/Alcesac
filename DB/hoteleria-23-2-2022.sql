-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-02-2022 a las 23:35:05
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alcesac_alcesac_pruebas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aperturas_cajas`
--

CREATE TABLE `aperturas_cajas` (
  `id` int(11) NOT NULL,
  `id_caja` int(11) NOT NULL,
  `fecha_apertura` datetime NOT NULL,
  `fecha_cierre` datetime NOT NULL,
  `monto_apertura` int(11) NOT NULL,
  `monto_cierre` int(11) NOT NULL,
  `estado` varchar(500) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `aperturas_cajas`
--

INSERT INTO `aperturas_cajas` (`id`, `id_caja`, `fecha_apertura`, `fecha_cierre`, `monto_apertura`, `monto_cierre`, `estado`, `id_usuario`, `fecha_creada`) VALUES
(123, 24, '2022-01-29 14:45:27', '2022-02-02 15:13:33', 12, 12, '0', 4, '2022-01-29 14:45:31'),
(124, 24, '2022-02-02 15:15:05', '2022-02-03 12:02:16', 100000, 100000, '0', 4, '2022-02-02 15:15:14'),
(125, 26, '2022-02-03 12:02:27', '2022-02-03 12:40:04', 100000, 100123, '0', 1, '2022-02-03 12:02:34'),
(126, 30, '2022-02-03 12:08:59', '2022-02-03 12:42:29', 200, 1200, '0', 4, '2022-02-03 12:09:05'),
(127, 25, '2022-02-03 12:42:35', '2022-02-03 14:00:53', 100, 26100, '0', 1, '2022-02-03 12:42:41'),
(128, 24, '2022-02-03 14:15:19', '0000-00-00 00:00:00', 100, 100, '1', 1, '2022-02-03 14:15:25'),
(129, 28, '2022-02-03 14:20:43', '2022-02-03 15:47:20', 200, 700, '0', 4, '2022-02-03 14:20:48'),
(130, 27, '2022-02-03 15:53:04', '2022-02-03 17:10:51', 200, 306224, '0', 10, '2022-02-03 15:53:07'),
(131, 28, '2022-02-04 13:51:10', '0000-00-00 00:00:00', 200, 607025, '1', 4, '2022-02-04 13:51:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_cabys`
--

CREATE TABLE `auto_cabys` (
  `id` int(11) NOT NULL,
  `descripcion` longtext NOT NULL,
  `cabys` longtext NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `fecha_act` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auto_cabys`
--

INSERT INTO `auto_cabys` (`id`, `descripcion`, `cabys`, `estado`, `fecha`, `fecha_act`) VALUES
(33, 'test2', '2391400009900', 0, '2022-02-11', '2022-02-11 11:45:05'),
(34, 'descripcion', '4141102000000', 0, '2022-02-11', '2022-02-11 09:54:03'),
(35, 'descripcion', '2799805000000', 0, '2022-02-11', '2022-02-11 11:22:20'),
(36, 'descripcion', '4299903010100', 0, '2022-02-11', '2022-02-11 11:49:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bodegas`
--

CREATE TABLE `bodegas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `cantidad_inicial` int(11) NOT NULL,
  `cantidad_maxima` int(11) NOT NULL,
  `cantidad_minima` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bodegas`
--

INSERT INTO `bodegas` (`id`, `codigo`, `nombre`, `cantidad_inicial`, `cantidad_maxima`, `cantidad_minima`, `estado`, `fecha_creada`) VALUES
(6, '001', 'Bodega', 100, 400, 50, 1, '2021-11-03'),
(7, '002', 'Bodega #2', 100, 1000, 50, 1, '2021-11-03'),
(8, '003', 'Bodega #3', 100, 400, 100, 1, '2021-11-03'),
(9, '004', 'Bodega #4', 100, 5000, 100, 1, '2021-11-03'),
(10, '005', 'Bodega #5', 100, 1000, 100, 1, '2021-11-03'),
(11, '006', 'Bodega #6', 100, 900, 20, 1, '2021-11-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(11) NOT NULL,
  `fecha_apertura` datetime DEFAULT NULL,
  `fecha_cierre` datetime DEFAULT NULL,
  `monto_apertura` double DEFAULT NULL,
  `monto_cierre` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `imagen`, `fecha_creada`) VALUES
(1, 'Personal', 'CASA-GOAS_30_INDIVIDUAL_1.jpg', '2018-02-15 09:14:21'),
(2, 'Doble', '1892_rec.jpg', '2019-02-26 11:39:02'),
(3, 'Triple', 'triple.jpg', '2019-02-26 11:39:08'),
(4, 'Cuadruple', 'cuadruple_DSC2747_resize-1024x683.jpg', '2019-02-26 11:40:17'),
(5, 'Especial', 'habitacion-grande-1.jpg', '2019-02-26 11:40:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_clientes_p`
--

CREATE TABLE `categoria_clientes_p` (
  `id` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria_clientes_p`
--

INSERT INTO `categoria_clientes_p` (`id`, `nombre`, `estado`, `fecha`) VALUES
(2, 'Bronce', 1, '2021-12-10'),
(3, 'Plata', 1, '2021-12-10'),
(4, 'Oro', 0, '2021-12-11'),
(5, 'Platino', 1, '2021-12-11'),
(7, 'Diamante', 1, '2021-12-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_venta`
--

CREATE TABLE `categoria_venta` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `id_tipo_categoria` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_proceso`
--

CREATE TABLE `cliente_proceso` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_proceso` int(11) DEFAULT NULL,
  `sesion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente_proceso`
--

INSERT INTO `cliente_proceso` (`id`, `id_cliente`, `id_proceso`, `sesion`) VALUES
(302, 768, 318, '4hvdrr5r5uanipfq6ufppjsm16'),
(303, 769, 319, '4hvdrr5r5uanipfq6ufppjsm16'),
(304, 770, 320, '4hvdrr5r5uanipfq6ufppjsm16'),
(305, 771, 321, '4hvdrr5r5uanipfq6ufppjsm16'),
(306, 772, 322, '4hvdrr5r5uanipfq6ufppjsm16'),
(307, 773, 323, '4hvdrr5r5uanipfq6ufppjsm16'),
(308, 774, 324, '4hvdrr5r5uanipfq6ufppjsm16'),
(309, 775, 325, '4hvdrr5r5uanipfq6ufppjsm16'),
(310, 776, 326, '4hvdrr5r5uanipfq6ufppjsm16'),
(311, 777, 327, '4hvdrr5r5uanipfq6ufppjsm16'),
(312, 778, 328, '4hvdrr5r5uanipfq6ufppjsm16'),
(313, 779, 329, '4hvdrr5r5uanipfq6ufppjsm16'),
(314, 780, 330, '4hvdrr5r5uanipfq6ufppjsm16'),
(315, 781, 331, '4hvdrr5r5uanipfq6ufppjsm16'),
(316, 782, 332, '4hvdrr5r5uanipfq6ufppjsm16'),
(317, 783, 333, '4hvdrr5r5uanipfq6ufppjsm16'),
(318, 784, 334, '4hvdrr5r5uanipfq6ufppjsm16'),
(319, 785, 335, '4hvdrr5r5uanipfq6ufppjsm16'),
(320, 786, 336, '4hvdrr5r5uanipfq6ufppjsm16'),
(321, 787, 337, '4hvdrr5r5uanipfq6ufppjsm16'),
(322, 788, 338, '4hvdrr5r5uanipfq6ufppjsm16'),
(323, 789, 339, '4hvdrr5r5uanipfq6ufppjsm16'),
(324, 790, 340, '4hvdrr5r5uanipfq6ufppjsm16'),
(325, 791, 341, '4dkbmrbnou42i3h2ufillm1srn'),
(326, 792, 342, '4dkbmrbnou42i3h2ufillm1srn'),
(327, 793, 343, '4dkbmrbnou42i3h2ufillm1srn'),
(328, 794, 344, '4dkbmrbnou42i3h2ufillm1srn'),
(329, 795, 345, '4dkbmrbnou42i3h2ufillm1srn'),
(330, 796, 346, '4dkbmrbnou42i3h2ufillm1srn'),
(331, 799, 347, '4dkbmrbnou42i3h2ufillm1srn'),
(332, 800, 348, '4dkbmrbnou42i3h2ufillm1srn'),
(333, 801, 349, '4dkbmrbnou42i3h2ufillm1srn'),
(334, 802, 350, '4dkbmrbnou42i3h2ufillm1srn'),
(335, 803, 351, '4dkbmrbnou42i3h2ufillm1srn'),
(336, 804, 352, '4dkbmrbnou42i3h2ufillm1srn'),
(337, 805, 353, '4dkbmrbnou42i3h2ufillm1srn'),
(338, 806, 354, '4dkbmrbnou42i3h2ufillm1srn'),
(339, 807, 355, '4dkbmrbnou42i3h2ufillm1srn'),
(340, 812, 356, '4dkbmrbnou42i3h2ufillm1srn'),
(341, 814, 357, '4dkbmrbnou42i3h2ufillm1srn'),
(342, 815, 358, '4dkbmrbnou42i3h2ufillm1srn'),
(343, 816, 359, '4dkbmrbnou42i3h2ufillm1srn'),
(344, 818, 360, '4dkbmrbnou42i3h2ufillm1srn'),
(345, 821, 361, '4dkbmrbnou42i3h2ufillm1srn'),
(346, 822, 362, '4dkbmrbnou42i3h2ufillm1srn'),
(347, 828, 363, '4dkbmrbnou42i3h2ufillm1srn'),
(348, 830, 364, '4dkbmrbnou42i3h2ufillm1srn'),
(349, 832, 365, '4dkbmrbnou42i3h2ufillm1srn'),
(350, 833, 366, '4dkbmrbnou42i3h2ufillm1srn'),
(351, 834, 367, '4dkbmrbnou42i3h2ufillm1srn'),
(352, 835, 368, '4dkbmrbnou42i3h2ufillm1srn'),
(353, 836, 369, '4dkbmrbnou42i3h2ufillm1srn'),
(354, 837, 370, '4dkbmrbnou42i3h2ufillm1srn'),
(355, 838, 371, '4dkbmrbnou42i3h2ufillm1srn'),
(356, 841, 372, '4dkbmrbnou42i3h2ufillm1srn'),
(357, 842, 373, '4dkbmrbnou42i3h2ufillm1srn'),
(358, 843, 374, '4dkbmrbnou42i3h2ufillm1srn'),
(359, 844, 375, '4dkbmrbnou42i3h2ufillm1srn'),
(360, 845, 376, '4dkbmrbnou42i3h2ufillm1srn'),
(361, 847, 377, '4dkbmrbnou42i3h2ufillm1srn'),
(362, 848, 378, '4dkbmrbnou42i3h2ufillm1srn'),
(363, 851, 379, '4dkbmrbnou42i3h2ufillm1srn'),
(364, 855, 380, '4dkbmrbnou42i3h2ufillm1srn'),
(365, 857, 381, '4dkbmrbnou42i3h2ufillm1srn'),
(366, 858, 382, '4dkbmrbnou42i3h2ufillm1srn'),
(367, 760, 395, '4dkbmrbnou42i3h2ufillm1srn'),
(368, 760, 396, '4dkbmrbnou42i3h2ufillm1srn'),
(369, 859, 397, '4dkbmrbnou42i3h2ufillm1srn'),
(370, 859, 398, '4dkbmrbnou42i3h2ufillm1srn'),
(371, 760, 399, '4dkbmrbnou42i3h2ufillm1srn'),
(372, 760, 400, '4dkbmrbnou42i3h2ufillm1srn'),
(373, 764, 401, '4dkbmrbnou42i3h2ufillm1srn'),
(374, 764, 402, '4dkbmrbnou42i3h2ufillm1srn'),
(375, 860, 403, '4dkbmrbnou42i3h2ufillm1srn'),
(376, 860, 404, '4dkbmrbnou42i3h2ufillm1srn'),
(377, 760, 405, '4dkbmrbnou42i3h2ufillm1srn'),
(378, 760, 406, '4dkbmrbnou42i3h2ufillm1srn'),
(379, 861, 407, '4dkbmrbnou42i3h2ufillm1srn'),
(380, 862, 408, '4dkbmrbnou42i3h2ufillm1srn'),
(381, 862, 409, '4dkbmrbnou42i3h2ufillm1srn'),
(382, 863, 410, '4dkbmrbnou42i3h2ufillm1srn'),
(383, 863, 411, '4dkbmrbnou42i3h2ufillm1srn'),
(384, 760, 412, 'f3ccs7tr0lrn9p84ct8qv85pe6'),
(385, 864, 413, '7om9i6utbu34l07vpcd9arvnlb'),
(386, 865, 414, 'e9uungkrbhfnmbcimjfi52af2p'),
(387, 866, 415, 'e9uungkrbhfnmbcimjfi52af2p'),
(388, 867, 416, 'e9uungkrbhfnmbcimjfi52af2p'),
(389, 868, 417, 'b0al4v6794bvc4c60rj6kjhr5o'),
(390, 869, 418, 'b0al4v6794bvc4c60rj6kjhr5o'),
(391, 881, 432, 'ojkd79kv0jqtu1v436qub65bma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `rnc` varchar(25) DEFAULT NULL,
  `registro_empresarial` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `usuario_hacienda` varchar(50) NOT NULL,
  `contrasena_hacienda` varchar(50) NOT NULL,
  `llave_criptografica` varchar(50) NOT NULL,
  `token` varchar(600) NOT NULL,
  `pin` int(6) NOT NULL,
  `cod_moneda` varchar(20) NOT NULL,
  `nombre_hac` varchar(100) NOT NULL,
  `numero_id` int(60) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `provincia` int(10) NOT NULL,
  `canton` int(60) NOT NULL,
  `distrito` int(60) NOT NULL,
  `barrio` int(60) NOT NULL,
  `cod_pais_tel_hc` int(60) NOT NULL,
  `tel_hac` int(60) NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `nombre`, `direccion`, `estado`, `telefono`, `fax`, `rnc`, `registro_empresarial`, `ciudad`, `logo`, `usuario_hacienda`, `contrasena_hacienda`, `llave_criptografica`, `token`, `pin`, `cod_moneda`, `nombre_hac`, `numero_id`, `tipo_id`, `provincia`, `canton`, `distrito`, `barrio`, `cod_pais_tel_hc`, `tel_hac`, `fecha`) VALUES
(1, 'Alcesac', 'Costa Rica, Cartago, Rio Cuarto.', 'Cartago', '+50661749563', 'NULL', '0038947384786', 'NULL', 'Tres Rios', 'Alcesac.jpeg', 'cpf-02-0791-0714@stag.comprobanteselectronicos.go.', '@W>oMjx#0K#})l0#xL>!', 'file.p12', 'e8323365a649c9da903a2776a9fb586c', 1234, 'CRC', 'Gabriel Quesada Araya', 207910714, 1, 1, 1, 1, 1, 506, 61749563, '2022-02-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `id_persona` int(11) DEFAULT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento_productos`
--

CREATE TABLE `descuento_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `descuento_productos`
--

INSERT INTO `descuento_productos` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(1, 'Descuento 12%', 12, 1, '2021-10-27'),
(2, 'Descuento 15%', 15, 1, '2021-10-27'),
(3, 'Descuento 20%', 20, 1, '2021-10-27'),
(4, 'Descuento 5%', 5, 1, '2021-10-27'),
(5, 'Sin Descuento', 0, 1, '2021-12-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cajas`
--

CREATE TABLE `estado_cajas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_cajas`
--

INSERT INTO `estado_cajas` (`id`, `nombre`, `valor`, `fecha_creada`) VALUES
(3, 'Ocupada\r\n\n', 0, 2),
(4, 'Disponible', 1, 2),
(5, 'Mantenimiento', 3, 2),
(6, 'Deshabilitada', 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_de_pago`
--

CREATE TABLE `estado_de_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `medio_pago` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_de_pago`
--

INSERT INTO `estado_de_pago` (`id`, `nombre`, `medio_pago`, `fecha_creada`) VALUES
(2, 'Pago realizado', 1, '2021-05-14'),
(8, 'En proceso', 0, '2021-06-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionalidades`
--

CREATE TABLE `funcionalidades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(110) NOT NULL,
  `estado` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `funcionalidades`
--

INSERT INTO `funcionalidades` (`id`, `nombre`, `estado`) VALUES
(1, 'Notificacio Email', 0),
(2, 'Notificaciones SMS', 0),
(3, 'Promociones', 0),
(4, 'Facturación Electrónica', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--

CREATE TABLE `gasto` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `capacidad` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id`, `nombre`, `descripcion`, `precio`, `id_categoria`, `estado`, `capacidad`, `fecha_creada`) VALUES
(52, 'Habitacion 1', '123', 0, 2, 1, 1, '2021-12-04 16:23:49'),
(53, 'Habitacion 2', '123', 0, 1, 1, 1, '2021-12-07 09:09:45'),
(54, 'Habitacion 3', '21', 0, 2, 2, 1, '2021-12-07 09:10:26'),
(55, 'Habitacion 4', 'q', 0, 3, 1, 1, '2021-12-07 09:10:36'),
(56, 'Habitacion 5', '4', 0, 4, 1, 1, '2021-12-07 09:10:47'),
(57, 'Habitacion 6', '5', 0, 5, 1, 1, '2021-12-07 09:10:57'),
(58, 'Habitacion 7', '12', 0, 4, 1, 1, '2021-12-07 09:11:05'),
(59, 'Habitacion 8', '21', 0, 5, 1, 1, '2021-12-07 09:11:14'),
(60, 'Habitacion 9', '2', 0, 2, 1, 1, '2021-12-07 09:11:21'),
(61, 'Habitacion 10', '231', 0, 5, 1, 1, '2021-12-07 09:11:29'),
(62, 'Habitacion 8', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:00'),
(63, 'Habitacion 9', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:10'),
(64, 'Habitacion 10', 'nd', 0, 1, 1, 1, '2022-02-03 12:36:19'),
(65, 'Habitacion 11', 'nd', 0, 1, 1, 1, '2022-02-03 12:38:20'),
(66, 'Habitacion 12', 'nd', 0, 1, 1, 1, '2022-02-03 12:38:27'),
(67, 'Habitacion 13', 'nd', 0, 1, 1, 1, '2022-02-03 12:50:57'),
(68, 'Habitacion 14', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:03'),
(69, 'Habitacion 15', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:12'),
(70, 'Habitacion 16', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:19'),
(71, 'Habitacion 17', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:26'),
(72, 'Habitacion 18', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:33'),
(73, 'Habitacion 19', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:39'),
(74, 'Habitacion 20', 'nd', 0, 1, 1, 1, '2022-02-03 12:51:46'),
(75, 'Habitacion 21', 'nd', 0, 1, 1, 1, '2022-02-03 12:52:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `compras` int(11) NOT NULL,
  `egresos` int(11) NOT NULL,
  `fecha_creada` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`id`, `nombre`, `valor`, `compras`, `egresos`, `fecha_creada`) VALUES
(1, 'IVA', 0, 1, 0, '0000-00-00 00:00:00'),
(3, 'Impuesto sobre la venta 13%', 1, 1, 0, '0000-00-00 00:00:00'),
(4, 'Impuesto sobre la venta 8%', 0, 0, 0, '0000-00-00 00:00:00'),
(5, 'Impuesto sobre la venta 4%', 0, 0, 0, '0000-00-00 00:00:00'),
(6, 'Impuesto sobre la venta 2%', 0, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos_productos`
--

CREATE TABLE `impuestos_productos` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `descripcion` text NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_tipo_impuesto` int(11) NOT NULL,
  `tipo_impuesto` varchar(30) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `impuestos_productos`
--

INSERT INTO `impuestos_productos` (`id`, `valor`, `nombre`, `descripcion`, `estado`, `id_tipo_impuesto`, `tipo_impuesto`, `fecha_creada`) VALUES
(17, 13, '13%', 'TEST', 1, 6, 'asd', '2021-10-25'),
(18, 8, '8%', 'TEST', 1, 6, 'asd', '2021-10-25'),
(19, 4, '4%', 'TEST', 1, 6, 'dsa', '2021-10-26'),
(20, 2, '2%', 'TEST', 1, 7, 'asda', '2021-10-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimiento_cajas`
--

CREATE TABLE `mantenimiento_cajas` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mantenimiento_cajas`
--

INSERT INTO `mantenimiento_cajas` (`id`, `numero`, `estado`, `fecha_creada`) VALUES
(24, 1, 2, '2021-06-29'),
(25, 2, 1, '2021-06-29'),
(26, 3, 1, '2021-06-29'),
(27, 4, 1, '2021-06-29'),
(28, 5, 2, '2021-07-03'),
(30, 6, 1, '2021-07-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones`
--

CREATE TABLE `notificaciones` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notificaciones`
--

INSERT INTO `notificaciones` (`id`, `email`, `estado`, `valor`, `fecha_creada`) VALUES
(8, 'gabrielqqquesada@gmail.com', 1, 0, '2021-09-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones_stocks`
--

CREATE TABLE `notificaciones_stocks` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notificaciones_stocks`
--

INSERT INTO `notificaciones_stocks` (`id`, `valor`, `estado`, `fecha_creada`) VALUES
(1, 10, 1, '2021-09-14'),
(2, 15, 0, '2021-09-14'),
(3, 5, 1, '2021-09-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_pago`
--

CREATE TABLE `periodos_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `dias` int(250) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `periodos_pago`
--

INSERT INTO `periodos_pago` (`id`, `nombre`, `dias`, `fecha_creada`) VALUES
(2, 'Contado', 0, '2021-06-05'),
(3, 'Limite de fecha', 0, '2021-06-09'),
(4, 'Por pagos', 0, '2021-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `tipo_documento` int(11) DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `razon_social` varchar(150) DEFAULT NULL,
  `provincia` int(11) NOT NULL,
  `canton` int(11) NOT NULL,
  `distrito` int(11) NOT NULL,
  `barrio` int(11) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL,
  `tipo` int(11) DEFAULT 1,
  `vip` int(11) NOT NULL DEFAULT 0,
  `contador` int(11) NOT NULL DEFAULT 0,
  `limite` int(11) NOT NULL DEFAULT 7,
  `nacionalidad` varchar(25) DEFAULT NULL,
  `estado_civil` varchar(12) DEFAULT NULL,
  `ocupacion` varchar(255) DEFAULT NULL,
  `medio_transporte` varchar(65) DEFAULT NULL,
  `destino` varchar(55) DEFAULT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `telefono_sec` int(11) NOT NULL,
  `celular` varchar(25) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `exonerado` int(11) DEFAULT NULL,
  `id_categoria_p` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `tipo_documento`, `documento`, `giro`, `nombre`, `fecha_nac`, `razon_social`, `provincia`, `canton`, `distrito`, `barrio`, `direccion`, `fecha_creada`, `tipo`, `vip`, `contador`, `limite`, `nacionalidad`, `estado_civil`, `ocupacion`, `medio_transporte`, `destino`, `motivo`, `telefono`, `telefono_sec`, `celular`, `email`, `exonerado`, `id_categoria_p`) VALUES
(881, 71, '207910714', 'NULL', 'JOSE GABRIEL QUESADA ARAYA', '0000-00-00', 'NULL', 4, 5, 1, 122, 'NULL', '2022-02-15 13:15:20', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, '24510254', 24510254, NULL, 'gabrielqqquesada@gmail.com', 0, 0),
(882, 2, '123', '1', '1', '0000-00-00', 'NITE', 2, 2, 7, 232, '123', '2022-02-17 09:53:52', 1, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, '12332312', 12332312, NULL, 'gquesada@d-apos.com', 0, 0),
(885, 71, '207910716', NULL, 'MARIA JOSE CAMPOS BARRANTES', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-17 13:33:01', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'gabrielqqquesada@gmail.com', 0, 0),
(889, 71, '207910715', NULL, 'MARIA CELESTE CAPARROSO ZUÑIGA', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-17 13:40:07', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 'gabrielqqquesada@gmail.com', 0, 0),
(904, 71, '207910719', NULL, 'MARCO ANTONIO ALFARO ALVARADO', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-17 17:05:40', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 0),
(930, 71, '208300168', NULL, 'VALERIA BARRANTES HERNANDEZ', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-18 20:32:30', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 0),
(934, 71, '207910415', NULL, 'ANGIE DANIELA FERNANDEZ CHACON', NULL, NULL, 0, 0, 0, 0, NULL, '2022-02-19 08:59:42', 3, 0, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL,
  `tipo_proceso` int(11) NOT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_apertura` int(11) NOT NULL,
  `precio` double NOT NULL DEFAULT 0,
  `cobro_ext` int(11) NOT NULL,
  `cant_noche` float NOT NULL DEFAULT 1,
  `dinero_dejado` double NOT NULL DEFAULT 0,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `fecha_entrada` datetime DEFAULT NULL,
  `fecha_salida` datetime DEFAULT NULL,
  `total` double NOT NULL DEFAULT 0,
  `id_usuario` int(11) DEFAULT NULL,
  `cant_personas` double DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `fecha_creada` datetime DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT 1,
  `observacion` varchar(255) DEFAULT NULL,
  `pagado` int(11) DEFAULT NULL,
  `nro_operacion` int(25) DEFAULT NULL,
  `num_tarjeta` int(11) DEFAULT NULL,
  `banco` int(11) DEFAULT NULL,
  `num_aprobacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id`, `tipo_proceso`, `id_habitacion`, `id_tarifa`, `id_cliente`, `id_apertura`, `precio`, `cobro_ext`, `cant_noche`, `dinero_dejado`, `id_tipo_pago`, `fecha_entrada`, `fecha_salida`, `total`, `id_usuario`, `cant_personas`, `id_caja`, `estado`, `fecha_creada`, `cantidad`, `observacion`, `pagado`, `nro_operacion`, `num_tarjeta`, `banco`, `num_aprobacion`) VALUES
(432, 1, 54, 71, 881, 131, 12000, 0, 1, 0, 1, '2022-02-17 15:03:09', '2022-02-18 12:00:00', 0, 4, 1, 131, 0, '2022-02-17 15:03:09', 1, NULL, 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_sueldo`
--

CREATE TABLE `proceso_sueldo` (
  `id` int(11) NOT NULL,
  `id_sueldo` int(11) DEFAULT NULL,
  `monto` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `tipo` int(11) NOT NULL DEFAULT 1,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso_venta`
--

CREATE TABLE `proceso_venta` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_operacion` int(11) DEFAULT NULL,
  `id_venta` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `fecha_creada` datetime DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso_venta`
--

INSERT INTO `proceso_venta` (`id`, `id_producto`, `id_operacion`, `id_venta`, `cantidad`, `precio`, `tipo_operacion`, `fecha_creada`, `id_caja`, `id_usuario`) VALUES
(463, 212, NULL, 379, 2, 5, 2, '2022-02-03 16:46:11', 130, 10),
(464, 214, NULL, 379, 1, 123, 2, '2022-02-03 16:46:11', 130, 10),
(465, 493, NULL, 380, 1, 1000, 2, '2022-02-03 16:54:03', 130, 10),
(466, 212, NULL, 381, 1, 150.35328, 1, '2022-02-03 17:03:46', 130, 10),
(467, 504, NULL, 382, 50, 6140.1375, 1, '2022-02-03 17:10:16', 130, 10),
(468, 212, NULL, 383, 1, 150.35328, 1, '2022-02-15 13:15:23', 131, 4),
(469, 538, NULL, 384, 1, 2684.88, 1, '2022-02-17 13:31:59', 131, 4),
(470, 538, NULL, 385, 1, 2684.88, 1, '2022-02-17 13:33:01', 131, 4),
(471, 538, NULL, 386, 1, 2684.88, 1, '2022-02-17 13:33:40', 131, 4),
(472, 548, NULL, 387, 1, 1275.318, 1, '2022-02-17 13:35:06', 131, 4),
(473, 538, NULL, 388, 1, 2684.88, 1, '2022-02-17 13:38:39', 131, 4),
(474, 538, NULL, 389, 1, 2684.88, 1, '2022-02-17 13:40:08', 131, 4),
(475, 548, NULL, 390, 1, 1275.318, 1, '2022-02-17 14:10:37', 131, 4),
(476, 538, NULL, 391, 1, 2684.88, 1, '2022-02-17 14:11:28', 131, 4),
(477, 538, NULL, 392, 1, 2684.88, 1, '2022-02-17 14:24:46', 131, 4),
(478, 538, NULL, 393, 1, 2684.88, 1, '2022-02-17 14:33:15', 131, 4),
(479, 538, NULL, 394, 1, 2684.88, 1, '2022-02-17 14:33:43', 131, 4),
(480, 554, NULL, 395, 1, 2711.7288, 1, '2022-02-17 14:34:19', 131, 4),
(481, 549, NULL, 396, 1, 5843.2752, 1, '2022-02-17 14:45:47', 131, 4),
(482, 548, NULL, 397, 1, 1275.318, 1, '2022-02-17 14:51:28', 131, 4),
(483, 538, NULL, 398, 1, 2684.88, 1, '2022-02-17 14:59:10', 131, 4),
(484, 538, NULL, 399, 1, 2684.88, 1, '2022-02-17 14:59:58', 131, 4),
(485, 538, NULL, 400, 1, 2684.88, 1, '2022-02-17 15:04:22', 131, 4),
(486, 559, NULL, 401, 1, 10739.52, 1, '2022-02-17 16:49:03', 131, 4),
(487, 548, NULL, 402, 1, 1275.318, 1, '2022-02-17 16:51:03', 131, 4),
(488, 549, NULL, 403, 1, 5843.2752, 1, '2022-02-17 17:05:40', 131, 4),
(489, 559, NULL, 404, 1, 10739.52, 1, '2022-02-18 14:56:44', 131, 4),
(490, 549, NULL, 405, 1, 5843.2752, 1, '2022-02-18 15:00:47', 131, 4),
(491, 548, NULL, 406, 1, 1275.318, 1, '2022-02-18 15:08:20', 131, 4),
(492, 554, NULL, 407, 1, 2711.7288, 1, '2022-02-18 15:10:27', 131, 4),
(493, 559, NULL, 408, 1, 10739.52, 1, '2022-02-18 15:12:36', 131, 4),
(494, 538, NULL, 409, 1, 2684.88, 1, '2022-02-18 15:20:56', 131, 4),
(495, 538, NULL, 410, 1, 2684.88, 1, '2022-02-18 15:23:15', 131, 4),
(496, 549, NULL, 411, 1, 5843.2752, 1, '2022-02-18 15:26:03', 131, 4),
(497, 549, NULL, 412, 1, 5843.2752, 1, '2022-02-18 15:31:58', 131, 4),
(498, 562, NULL, 413, 1, 5369.76, 1, '2022-02-18 15:37:21', 131, 4),
(499, 554, NULL, 414, 1, 2711.7288, 1, '2022-02-18 15:39:44', 131, 4),
(500, 548, NULL, 415, 1, 1275.318, 1, '2022-02-18 15:41:40', 131, 4),
(501, 559, NULL, 416, 1, 10739.52, 1, '2022-02-18 15:45:50', 131, 4),
(502, 554, NULL, 417, 1, 2711.7288, 1, '2022-02-18 15:48:19', 131, 4),
(503, 561, NULL, 417, 1, 9397.08, 1, '2022-02-18 15:48:19', 131, 4),
(504, 548, NULL, 418, 1, 1275.318, 1, '2022-02-18 15:53:14', 131, 4),
(505, 561, NULL, 419, 1, 9397.08, 1, '2022-02-18 15:54:26', 131, 4),
(506, 549, NULL, 420, 1, 5843.2752, 1, '2022-02-18 16:04:39', 131, 4),
(507, 549, NULL, 421, 1, 5843.2752, 1, '2022-02-18 16:30:34', 131, 4),
(508, 548, NULL, 421, 1, 1275.318, 1, '2022-02-18 16:30:34', 131, 4),
(509, 561, NULL, 422, 1, 9397.08, 1, '2022-02-18 16:31:53', 131, 4),
(510, 538, NULL, 423, 1, 2684.88, 1, '2022-02-18 16:32:29', 131, 4),
(511, 538, NULL, 424, 1, 2684.88, 1, '2022-02-18 16:33:35', 131, 4),
(512, 548, NULL, 425, 1, 1275.318, 1, '2022-02-18 16:34:10', 131, 4),
(513, 564, NULL, 426, 1, 2684.88, 1, '2022-02-18 20:25:59', 131, 4),
(514, 559, NULL, 427, 1, 10739.52, 1, '2022-02-18 20:32:30', 131, 4),
(515, 548, NULL, 428, 1, 1275.318, 1, '2022-02-19 08:55:21', 131, 4),
(516, 564, NULL, 429, 1, 2684.88, 1, '2022-02-19 08:56:08', 131, 4),
(517, 559, NULL, 430, 1, 10739.52, 1, '2022-02-19 08:58:49', 131, 4),
(518, 549, NULL, 431, 1, 5843.2752, 1, '2022-02-19 08:59:42', 131, 4),
(519, 538, NULL, 432, 1, 2684.88, 1, '2022-02-19 09:06:59', 131, 4),
(520, 549, NULL, 433, 1, 5843.2752, 1, '2022-02-19 09:10:35', 131, 4),
(521, 554, NULL, 434, 1, 2711.7288, 1, '2022-02-19 09:13:57', 131, 4),
(522, 554, NULL, 435, 1, 2711.7288, 1, '2022-02-19 09:16:03', 131, 4),
(523, 548, NULL, 436, 1, 1275.318, 1, '2022-02-19 09:19:35', 131, 4),
(524, 548, NULL, 437, 1, 1275.318, 1, '2022-02-19 09:26:14', 131, 4),
(525, 549, NULL, 438, 1, 5843.2752, 1, '2022-02-19 09:28:33', 131, 4),
(526, 559, NULL, 439, 1, 10739.52, 1, '2022-02-19 09:29:59', 131, 4),
(527, 548, NULL, 439, 1, 1275.318, 1, '2022-02-19 09:29:59', 131, 4),
(528, 548, NULL, 440, 1, 1275.318, 1, '2022-02-19 09:30:31', 131, 4),
(529, 548, NULL, 441, 1, 1275.318, 1, '2022-02-19 09:31:40', 131, 4),
(530, 538, NULL, 442, 1, 2684.88, 1, '2022-02-19 09:31:55', 131, 4),
(531, 538, NULL, 443, 1, 2684.88, 1, '2022-02-19 09:32:32', 131, 4),
(532, 561, NULL, 444, 1, 9397.08, 1, '2022-02-19 09:33:01', 131, 4),
(533, 538, NULL, 445, 1, 2684.88, 1, '2022-02-19 09:33:35', 131, 4),
(534, 548, NULL, 446, 1, 1275.318, 1, '2022-02-19 09:36:20', 131, 4),
(535, 548, NULL, 447, 1, 1275.318, 1, '2022-02-19 09:37:58', 131, 4),
(536, 554, NULL, 448, 1, 2711.7288, 1, '2022-02-19 09:42:52', 131, 4),
(537, 549, NULL, 449, 1, 5843.2752, 1, '2022-02-19 09:43:33', 131, 4),
(538, 559, NULL, 450, 1, 10739.52, 1, '2022-02-19 09:46:10', 131, 4),
(539, 549, NULL, 451, 1, 5843.2752, 1, '2022-02-19 09:52:22', 131, 4),
(540, 562, NULL, 452, 1, 5369.76, 1, '2022-02-19 09:52:33', 131, 4),
(541, 548, NULL, 453, 1, 1275.318, 1, '2022-02-19 09:54:36', 131, 4),
(542, 561, NULL, 454, 1, 9397.08, 1, '2022-02-19 09:55:40', 131, 4),
(543, 554, NULL, 455, 1, 2711.7288, 1, '2022-02-19 10:00:37', 131, 4),
(544, 554, NULL, 456, 1, 2711.7288, 1, '2022-02-19 10:10:17', 131, 4),
(545, 548, NULL, 457, 1, 1275.318, 1, '2022-02-19 10:17:26', 131, 4),
(546, 562, NULL, 458, 1, 5369.76, 1, '2022-02-19 10:26:22', 131, 4),
(547, 565, NULL, 459, 1, 2819.124, 1, '2022-02-19 10:28:42', 131, 4),
(548, 562, NULL, 460, 1, 5369.76, 1, '2022-02-19 10:30:39', 131, 4),
(549, 561, NULL, 461, 1, 9397.08, 1, '2022-02-19 10:34:16', 131, 4),
(550, 561, NULL, 462, 1, 9397.08, 1, '2022-02-19 10:35:39', 131, 4),
(551, 559, NULL, 463, 1, 10739.52, 1, '2022-02-19 10:38:54', 131, 4),
(552, 549, NULL, 464, 1, 5843.2752, 1, '2022-02-19 10:42:05', 131, 4),
(553, 561, NULL, 465, 1, 9397.08, 1, '2022-02-19 10:42:55', 131, 4),
(554, 548, NULL, 466, 1, 1275.318, 1, '2022-02-19 10:53:05', 131, 4),
(555, 561, NULL, 467, 1, 9397.08, 1, '2022-02-19 10:54:04', 131, 4),
(556, 561, NULL, 468, 1, 9397.08, 1, '2022-02-19 10:55:20', 131, 4),
(557, 549, NULL, 469, 1, 5843.2752, 1, '2022-02-19 10:57:53', 131, 4),
(558, 561, NULL, 470, 1, 9397.08, 1, '2022-02-19 11:08:00', 131, 4),
(559, 554, NULL, 471, 1, 2711.7288, 1, '2022-02-19 11:17:04', 131, 4),
(560, 548, NULL, 472, 1, 1275.318, 1, '2022-02-19 11:22:24', 131, 4),
(561, 538, NULL, 473, 1, 2684.88, 1, '2022-02-19 11:24:19', 131, 4),
(562, 564, NULL, 474, 1, 2684.88, 1, '2022-02-19 11:25:26', 131, 4),
(563, 559, NULL, 475, 1, 10739.52, 1, '2022-02-19 11:27:17', 131, 4),
(564, 554, NULL, 476, 1, 2711.7288, 1, '2022-02-19 11:28:01', 131, 4),
(565, 559, NULL, 477, 1, 10739.52, 1, '2022-02-19 11:40:31', 131, 4),
(566, 554, NULL, 477, 1, 2711.7288, 1, '2022-02-19 11:40:31', 131, 4),
(567, 538, NULL, 478, 1, 2684.88, 1, '2022-02-19 11:42:34', 131, 4),
(568, 554, NULL, 479, 1, 2711.7288, 1, '2022-02-19 11:45:07', 131, 4),
(569, 548, NULL, 480, 1, 1275.318, 1, '2022-02-19 11:46:27', 131, 4),
(570, 554, NULL, 481, 1, 2711.7288, 1, '2022-02-19 11:50:21', 131, 4),
(571, 554, NULL, 482, 1, 2711.7288, 1, '2022-02-19 11:51:43', 131, 4),
(572, 554, NULL, 483, 1, 2711.7288, 1, '2022-02-19 11:58:22', 131, 4),
(573, 548, NULL, 484, 1, 1275.318, 1, '2022-02-19 11:59:41', 131, 4),
(574, 548, NULL, 485, 1, 1275.318, 1, '2022-02-21 08:37:47', 131, 4),
(575, 548, NULL, 486, 1, 1275.318, 1, '2022-02-21 08:38:48', 131, 4),
(576, 554, NULL, 487, 1, 2711.7288, 1, '2022-02-21 08:40:27', 131, 4),
(577, 554, NULL, 488, 1, 2711.7288, 1, '2022-02-21 08:43:20', 131, 4),
(578, 538, NULL, 489, 1, 2684.88, 1, '2022-02-21 08:44:16', 131, 4),
(579, 548, NULL, 490, 1, 1275.318, 1, '2022-02-21 08:54:55', 131, 4),
(580, 538, NULL, 491, 1, 2684.88, 1, '2022-02-21 08:55:52', 131, 4),
(581, 549, NULL, 492, 1, 5843.2752, 1, '2022-02-21 08:56:54', 131, 4),
(582, 548, NULL, 492, 1, 1275.318, 1, '2022-02-21 08:56:54', 131, 4),
(583, 559, NULL, 493, 1, 10739.52, 1, '2022-02-21 08:57:02', 131, 4),
(584, 549, NULL, 494, 1, 5843.2752, 1, '2022-02-21 08:57:07', 131, 4),
(585, 549, NULL, 495, 1, 5843.2752, 1, '2022-02-21 11:07:55', 131, 4),
(586, 548, NULL, 496, 1, 1275.318, 1, '2022-02-21 11:09:06', 131, 4),
(587, 548, NULL, 497, 1, 1275.318, 1, '2022-02-21 17:13:49', 131, 4),
(588, 559, NULL, 498, 1, 10739.52, 1, '2022-02-21 17:15:20', 131, 4),
(589, 548, NULL, 499, 1, 1275.318, 1, '2022-02-22 09:25:33', 131, 4),
(590, 549, NULL, 500, 1, 5843.2752, 1, '2022-02-22 10:43:47', 131, 4),
(591, 548, NULL, 501, 1, 1275.318, 1, '2022-02-23 08:18:48', 131, 4),
(592, 554, NULL, 502, 1, 2711.7288, 1, '2022-02-23 08:21:22', 131, 4),
(593, 549, NULL, 503, 1, 5843.2752, 1, '2022-02-23 08:27:17', 131, 4),
(594, 554, NULL, 504, 1, 2711.7288, 1, '2022-02-23 08:46:46', 131, 4),
(595, 548, NULL, 505, 1, 1275.318, 1, '2022-02-23 08:49:26', 131, 4),
(596, 561, NULL, 506, 1, 9397.08, 1, '2022-02-23 08:50:43', 131, 4),
(597, 549, NULL, 507, 1, 5843.2752, 1, '2022-02-23 11:07:03', 131, 4),
(598, 561, NULL, 508, 1, 9397.08, 1, '2022-02-23 12:57:44', 131, 4),
(599, 562, NULL, 508, 1, 5369.76, 1, '2022-02-23 12:57:44', 131, 4),
(600, 538, NULL, 509, 1, 2684.88, 1, '2022-02-23 13:12:56', 131, 4),
(601, 559, NULL, 510, 1, 10739.52, 1, '2022-02-23 13:23:07', 131, 4),
(602, 538, NULL, 511, 2, 2684.88, 1, '2022-02-23 15:40:54', 131, 4),
(603, 538, NULL, 512, 1, 7.02295, 1, '2022-02-23 16:05:31', 131, 4),
(604, 538, NULL, 513, 1, 7.02295, 1, '2022-02-23 16:18:05', 131, 4),
(605, 538, NULL, 514, 1, 7.02295, 1, '2022-02-23 16:21:49', 131, 4),
(606, 538, NULL, 515, 1, 7.02295, 1, '2022-02-23 16:24:51', 131, 4),
(607, 538, NULL, 516, 1, 7.02295, 1, '2022-02-23 16:26:01', 131, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `codigo` longtext DEFAULT NULL,
  `codigo_barra` varchar(50) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `acreditable` tinyint(1) NOT NULL DEFAULT 0,
  `presentacion` varchar(255) DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `stock` double NOT NULL DEFAULT 0,
  `aviso_stock` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `utilidad` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_bodega` int(11) NOT NULL,
  `id_unidad_medida` int(11) NOT NULL,
  `id_descuento` int(11) DEFAULT 0,
  `imagen` varchar(500) NOT NULL,
  `count_mail` int(11) NOT NULL,
  `estado` double NOT NULL,
  `existencia_cabys` tinyint(1) NOT NULL DEFAULT 0,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `codigo`, `codigo_barra`, `nombre`, `marca`, `descripcion`, `acreditable`, `presentacion`, `precio_compra`, `precio_venta`, `stock`, `aviso_stock`, `stock_max`, `utilidad`, `id_proveedor`, `id_bodega`, `id_unidad_medida`, `id_descuento`, `imagen`, `count_mail`, `estado`, `existencia_cabys`, `fecha_creada`) VALUES
(530, '2799805000000', '20213', 'Sandia', 'TEST', '100', 1, 'Sandia', 1000, 2000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(531, '2145', '20213', 'Mamon', 'TEST', '100', 1, 'Mamon', 1500, 2500, 100, 10, 400, 10, 0, 9, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(533, '2799805000000', '20213', 'Palitos de queso', 'TEST', '100', 1, 'Palitos de queso', 1000, 1500, 100, 10, 400, 10, 0, 9, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(534, '21681', '20213', 'Empanadas', 'TEST', '100', 1, 'Empanadas', 1000, 2020, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(535, '87894', '20213', 'Chechos', 'TEST', '100', 1, 'Chechos', 4000, 4500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(536, '189878', '20213', 'Chiriviscos ', 'TEST', '100', 1, 'Chiriviscos ', 1000, 1500, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(537, '654654', '20213', 'Helados de Chocolate', 'TEST', '100', 1, 'Helados de Chocolate', 1000, 3000, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(538, '4141102000000', '20213', 'Helados de Vainilla', 'TEST', '100', 1, 'Helados de Vainilla', 2, 5, 100, 10, 400, 10, 0, 11, 157, 5, '0', 0, 0, 0, '2022-02-16 08:42:01'),
(539, '3213', '20213', 'Helados de Maracuya', 'TEST', '100', 1, 'Helados de Maracuya', 1000, 8000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(540, '313', '20213', 'Helados de Mamon', 'TEST', '100', 1, 'Helados de Mamon', 6000, 8000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(541, '65468', '20213', 'Helados de Fresa', 'TEST', '100', 1, 'Helados de Fresa', 1000, 7000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(542, '3213008', '20213', 'Helados de Menta', 'TEST', '100', 1, 'Helados de Menta', 1000, 4000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(543, '346848', '20213', 'Helados de Te frio', 'TEST', '100', 1, 'Helados de Te frio', 1000, 4500, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(544, '1231233', '20213', 'Helados de Melocoton', 'TEST', '100', 1, 'Helados de Melocoton', 1000, 2000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(545, '1231333', '20213', 'Helados de Ca?a', 'TEST', '100', 1, 'Helados de Ca?a', 1000, 2100, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(546, '221331', '20213', 'Chetos', 'TEST', '100', 1, 'Chetos', 1000, 1500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:42:01'),
(548, '2391400009900', '20313', 'Peperoni', 'Peperoni', 'ND', 1, 'Peperoni', 500, 1000, 100, 10, 250, 10, 0, 11, 157, 4, '0', 0, 0, 0, '2022-02-16 08:45:19'),
(549, '22813', '22813', 'Chicharrones', 'Chicharrones', 'ND', 1, 'Chicharrones', 3500, 4500, 100, 10, 1000, 12, 0, 7, 157, 4, '0', 0, 0, 0, '2022-02-16 08:45:19'),
(550, '20214', '20213', 'Sandia', 'TEST', '100', 1, 'Sandia', 1000, 2000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(551, '2145', '20213', 'Mamon', 'TEST', '100', 1, 'Mamon', 1500, 2500, 100, 10, 400, 10, 0, 9, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(553, '977842', '20213', 'Palitos de queso', 'TEST', '100', 1, 'Palitos de queso', 1000, 1500, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(554, '21681', '20213', 'Empanadas', 'TEST', '100', 1, 'Empanadas', 1000, 2020, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(555, '87894', '20213', 'Chechos', 'TEST', '100', 1, 'Chechos', 4000, 4500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(556, '189878', '20213', 'Chiriviscos ', 'TEST', '100', 1, 'Chiriviscos ', 1000, 1500, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(557, '654654', '20213', 'Helados de Chocolate', 'TEST', '100', 1, 'Helados de Chocolate', 1000, 3000, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(558, '2323551', '20213', 'Helados de Vainilla', 'TEST', '100', 1, 'Helados de Vainilla', 1000, 2000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(559, '3213', '20213', 'Helados de Maracuya', 'TEST', '100', 1, 'Helados de Maracuya', 1000, 8000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(560, '313', '20213', 'Helados de Mamon', 'TEST', '100', 1, 'Helados de Mamon', 6000, 8000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(561, '65468', '20213', 'Helados de Fresa', 'TEST', '100', 1, 'Helados de Fresa', 1000, 7000, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(562, '3213008', '20213', 'Helados de Menta', 'TEST', '100', 1, 'Helados de Menta', 1000, 4000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(563, '346848', '20213', 'Helados de Te frio', 'TEST', '100', 1, 'Helados de Te frio', 1000, 4500, 100, 10, 400, 10, 0, 8, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26'),
(564, '1231233', '20213', 'Helados de Melocoton', 'TEST', '100', 1, 'Helados de Melocoton', 1000, 2000, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(565, '1231333', '20213', 'Helados de Ca?a', 'TEST', '100', 1, 'Helados de Ca?a', 1000, 2100, 100, 10, 400, 10, 0, 10, 157, 5, '0', 0, 0, 0, '2022-02-16 08:48:26'),
(566, '221331', '20213', 'Chetos', 'TEST', '100', 1, 'Chetos', 1000, 1500, 100, 10, 400, 10, 0, 7, 157, 5, '0', 0, 1, 0, '2022-02-16 08:48:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones`
--

CREATE TABLE `promociones` (
  `id` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `promociones`
--

INSERT INTO `promociones` (`id`, `valor`, `descripcion`, `estado`, `fecha`) VALUES
(48, 100, 'Promocion 1', 1, '2021-12-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `documento` varchar(12) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_impuestos_productos`
--

CREATE TABLE `r_impuestos_productos` (
  `id` int(11) NOT NULL,
  `id_impuesto` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_impuestos_productos`
--

INSERT INTO `r_impuestos_productos` (`id`, `id_impuesto`, `id_producto`, `fecha_creada`) VALUES
(817, 18, 530, '2022-02-16'),
(818, 18, 531, '2022-02-16'),
(821, 18, 534, '2022-02-16'),
(822, 18, 535, '2022-02-16'),
(823, 18, 536, '2022-02-16'),
(824, 18, 537, '2022-02-16'),
(826, 18, 539, '2022-02-16'),
(827, 18, 540, '2022-02-16'),
(828, 18, 541, '2022-02-16'),
(829, 18, 542, '2022-02-16'),
(830, 18, 543, '2022-02-16'),
(831, 18, 544, '2022-02-16'),
(832, 18, 545, '2022-02-16'),
(833, 18, 546, '2022-02-16'),
(835, 18, 549, '2022-02-16'),
(836, 18, 550, '2022-02-16'),
(837, 18, 551, '2022-02-16'),
(839, 18, 553, '2022-02-16'),
(840, 18, 554, '2022-02-16'),
(841, 18, 555, '2022-02-16'),
(842, 18, 556, '2022-02-16'),
(843, 18, 557, '2022-02-16'),
(844, 18, 558, '2022-02-16'),
(845, 18, 559, '2022-02-16'),
(846, 18, 560, '2022-02-16'),
(847, 18, 561, '2022-02-16'),
(848, 18, 562, '2022-02-16'),
(849, 18, 563, '2022-02-16'),
(850, 18, 564, '2022-02-16'),
(851, 18, 565, '2022-02-16'),
(852, 18, 566, '2022-02-16'),
(853, 18, 548, '2022-02-16'),
(856, 18, 533, '2022-02-16'),
(857, 17, 538, '2022-02-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_plazo_venta`
--

CREATE TABLE `r_plazo_venta` (
  `id` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_plazo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha_creada` date NOT NULL,
  `fecha_act` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_plazo_venta`
--

INSERT INTO `r_plazo_venta` (`id`, `id_venta`, `id_plazo`, `estado`, `fecha_creada`, `fecha_act`) VALUES
(8, 381, 2, 0, '2022-02-03', '2022-02-03'),
(9, 382, 2, 0, '2022-02-03', '2022-02-03'),
(10, 383, 2, 0, '2022-02-15', '2022-02-15'),
(11, 384, 2, 0, '2022-02-17', '2022-02-17'),
(12, 385, 2, 0, '2022-02-17', '2022-02-17'),
(13, 386, 2, 0, '2022-02-17', '2022-02-17'),
(14, 387, 2, 0, '2022-02-17', '2022-02-17'),
(15, 388, 2, 0, '2022-02-17', '2022-02-17'),
(16, 389, 2, 0, '2022-02-17', '2022-02-17'),
(17, 390, 2, 0, '2022-02-17', '2022-02-17'),
(18, 391, 2, 0, '2022-02-17', '2022-02-17'),
(19, 392, 2, 0, '2022-02-17', '2022-02-17'),
(20, 393, 2, 0, '2022-02-17', '2022-02-17'),
(21, 394, 2, 0, '2022-02-17', '2022-02-17'),
(22, 395, 2, 0, '2022-02-17', '2022-02-17'),
(23, 396, 2, 0, '2022-02-17', '2022-02-17'),
(24, 397, 2, 0, '2022-02-17', '2022-02-17'),
(25, 398, 2, 0, '2022-02-17', '2022-02-17'),
(26, 399, 2, 0, '2022-02-17', '2022-02-17'),
(27, 400, 2, 0, '2022-02-17', '2022-02-17'),
(28, 401, 2, 0, '2022-02-17', '2022-02-17'),
(29, 402, 2, 0, '2022-02-17', '2022-02-17'),
(30, 403, 2, 0, '2022-02-17', '2022-02-17'),
(31, 404, 2, 0, '2022-02-18', '2022-02-18'),
(32, 405, 2, 0, '2022-02-18', '2022-02-18'),
(33, 406, 2, 0, '2022-02-18', '2022-02-18'),
(34, 407, 2, 0, '2022-02-18', '2022-02-18'),
(35, 408, 2, 0, '2022-02-18', '2022-02-18'),
(36, 409, 2, 0, '2022-02-18', '2022-02-18'),
(37, 410, 2, 0, '2022-02-18', '2022-02-18'),
(38, 411, 2, 0, '2022-02-18', '2022-02-18'),
(39, 412, 2, 0, '2022-02-18', '2022-02-18'),
(40, 413, 2, 0, '2022-02-18', '2022-02-18'),
(41, 414, 2, 0, '2022-02-18', '2022-02-18'),
(42, 415, 2, 0, '2022-02-18', '2022-02-18'),
(43, 416, 2, 0, '2022-02-18', '2022-02-18'),
(44, 417, 2, 0, '2022-02-18', '2022-02-18'),
(45, 418, 2, 0, '2022-02-18', '2022-02-18'),
(46, 419, 2, 0, '2022-02-18', '2022-02-18'),
(47, 420, 2, 0, '2022-02-18', '2022-02-18'),
(48, 421, 2, 0, '2022-02-18', '2022-02-18'),
(49, 422, 2, 0, '2022-02-18', '2022-02-18'),
(50, 423, 2, 0, '2022-02-18', '2022-02-18'),
(51, 424, 2, 0, '2022-02-18', '2022-02-18'),
(52, 425, 2, 0, '2022-02-18', '2022-02-18'),
(53, 426, 2, 0, '2022-02-18', '2022-02-18'),
(54, 427, 2, 0, '2022-02-18', '2022-02-18'),
(55, 428, 2, 0, '2022-02-19', '2022-02-19'),
(56, 429, 2, 0, '2022-02-19', '2022-02-19'),
(57, 430, 2, 0, '2022-02-19', '2022-02-19'),
(58, 431, 2, 0, '2022-02-19', '2022-02-19'),
(59, 432, 2, 0, '2022-02-19', '2022-02-19'),
(60, 433, 2, 0, '2022-02-19', '2022-02-19'),
(61, 434, 2, 0, '2022-02-19', '2022-02-19'),
(62, 435, 2, 0, '2022-02-19', '2022-02-19'),
(63, 436, 2, 0, '2022-02-19', '2022-02-19'),
(64, 437, 2, 0, '2022-02-19', '2022-02-19'),
(65, 438, 2, 0, '2022-02-19', '2022-02-19'),
(66, 439, 2, 0, '2022-02-19', '2022-02-19'),
(67, 440, 2, 0, '2022-02-19', '2022-02-19'),
(68, 441, 2, 0, '2022-02-19', '2022-02-19'),
(69, 442, 2, 0, '2022-02-19', '2022-02-19'),
(70, 443, 2, 0, '2022-02-19', '2022-02-19'),
(71, 444, 2, 0, '2022-02-19', '2022-02-19'),
(72, 445, 2, 0, '2022-02-19', '2022-02-19'),
(73, 446, 2, 0, '2022-02-19', '2022-02-19'),
(74, 447, 2, 0, '2022-02-19', '2022-02-19'),
(75, 448, 2, 0, '2022-02-19', '2022-02-19'),
(76, 449, 2, 0, '2022-02-19', '2022-02-19'),
(77, 450, 2, 0, '2022-02-19', '2022-02-19'),
(78, 451, 2, 0, '2022-02-19', '2022-02-19'),
(79, 452, 2, 0, '2022-02-19', '2022-02-19'),
(80, 453, 2, 0, '2022-02-19', '2022-02-19'),
(81, 454, 2, 0, '2022-02-19', '2022-02-19'),
(82, 455, 2, 0, '2022-02-19', '2022-02-19'),
(83, 456, 2, 0, '2022-02-19', '2022-02-19'),
(84, 457, 2, 0, '2022-02-19', '2022-02-19'),
(85, 458, 2, 0, '2022-02-19', '2022-02-19'),
(86, 459, 2, 0, '2022-02-19', '2022-02-19'),
(87, 460, 2, 0, '2022-02-19', '2022-02-19'),
(88, 461, 2, 0, '2022-02-19', '2022-02-19'),
(89, 462, 2, 0, '2022-02-19', '2022-02-19'),
(90, 463, 2, 0, '2022-02-19', '2022-02-19'),
(91, 464, 2, 0, '2022-02-19', '2022-02-19'),
(92, 465, 2, 0, '2022-02-19', '2022-02-19'),
(93, 466, 2, 0, '2022-02-19', '2022-02-19'),
(94, 467, 2, 0, '2022-02-19', '2022-02-19'),
(95, 468, 2, 0, '2022-02-19', '2022-02-19'),
(96, 469, 2, 0, '2022-02-19', '2022-02-19'),
(97, 470, 2, 0, '2022-02-19', '2022-02-19'),
(98, 471, 2, 0, '2022-02-19', '2022-02-19'),
(99, 472, 2, 0, '2022-02-19', '2022-02-19'),
(100, 473, 2, 0, '2022-02-19', '2022-02-19'),
(101, 474, 2, 0, '2022-02-19', '2022-02-19'),
(102, 475, 2, 0, '2022-02-19', '2022-02-19'),
(103, 476, 2, 0, '2022-02-19', '2022-02-19'),
(104, 477, 2, 0, '2022-02-19', '2022-02-19'),
(105, 478, 2, 0, '2022-02-19', '2022-02-19'),
(106, 479, 2, 0, '2022-02-19', '2022-02-19'),
(107, 480, 2, 0, '2022-02-19', '2022-02-19'),
(108, 481, 2, 0, '2022-02-19', '2022-02-19'),
(109, 482, 2, 0, '2022-02-19', '2022-02-19'),
(110, 483, 2, 0, '2022-02-19', '2022-02-19'),
(111, 484, 2, 0, '2022-02-19', '2022-02-19'),
(112, 485, 2, 0, '2022-02-21', '2022-02-21'),
(113, 486, 2, 0, '2022-02-21', '2022-02-21'),
(114, 487, 2, 0, '2022-02-21', '2022-02-21'),
(115, 488, 2, 0, '2022-02-21', '2022-02-21'),
(116, 489, 2, 0, '2022-02-21', '2022-02-21'),
(117, 490, 2, 0, '2022-02-21', '2022-02-21'),
(118, 491, 2, 0, '2022-02-21', '2022-02-21'),
(119, 492, 2, 0, '2022-02-21', '2022-02-21'),
(120, 493, 2, 0, '2022-02-21', '2022-02-21'),
(121, 494, 2, 0, '2022-02-21', '2022-02-21'),
(122, 495, 2, 0, '2022-02-21', '2022-02-21'),
(123, 496, 2, 0, '2022-02-21', '2022-02-21'),
(124, 497, 2, 0, '2022-02-21', '2022-02-21'),
(125, 498, 2, 0, '2022-02-21', '2022-02-21'),
(126, 499, 2, 0, '2022-02-22', '2022-02-22'),
(127, 500, 2, 0, '2022-02-22', '2022-02-22'),
(128, 501, 2, 0, '2022-02-23', '2022-02-23'),
(129, 502, 2, 0, '2022-02-23', '2022-02-23'),
(130, 503, 2, 0, '2022-02-23', '2022-02-23'),
(131, 504, 2, 0, '2022-02-23', '2022-02-23'),
(132, 505, 2, 0, '2022-02-23', '2022-02-23'),
(133, 506, 2, 0, '2022-02-23', '2022-02-23'),
(134, 507, 3, 0, '2022-02-23', '2022-02-23'),
(135, 508, 2, 0, '2022-02-23', '2022-02-23'),
(136, 509, 2, 0, '2022-02-23', '2022-02-23'),
(137, 510, 2, 0, '2022-02-23', '2022-02-23'),
(138, 511, 2, 0, '2022-02-23', '2022-02-23'),
(139, 512, 2, 0, '2022-02-23', '2022-02-23'),
(140, 513, 2, 0, '2022-02-23', '2022-02-23'),
(141, 514, 2, 0, '2022-02-23', '2022-02-23'),
(142, 515, 2, 0, '2022-02-23', '2022-02-23'),
(143, 516, 2, 0, '2022-02-23', '2022-02-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_promociones`
--

CREATE TABLE `r_promociones` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT 0,
  `id_categoria` int(11) NOT NULL,
  `id_porcentaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_promociones`
--

INSERT INTO `r_promociones` (`id`, `id_producto`, `id_habitacion`, `id_categoria`, `id_porcentaje`) VALUES
(69, 0, 55, 5, 48),
(70, 0, 57, 5, 48),
(71, 0, 60, 5, 48),
(72, 214, 0, 5, 48);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_utilidades_productos`
--

CREATE TABLE `r_utilidades_productos` (
  `id` int(11) NOT NULL,
  `id_utilidad` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subimpuestos_productos`
--

CREATE TABLE `subimpuestos_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `subimpuestos_productos`
--

INSERT INTO `subimpuestos_productos` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(6, 'Cruz roja', 2, 1, '2021-10-25'),
(7, 'IVA', 13, 1, '2021-10-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldo`
--

CREATE TABLE `sueldo` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `monto` double DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `dia_pago` int(11) NOT NULL DEFAULT 1,
  `fecha_comienzo` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa`
--

CREATE TABLE `tarifa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa`
--

INSERT INTO `tarifa` (`id`, `nombre`) VALUES
(1, '24 Horas'),
(4, '12 horas'),
(7, 'Doble'),
(8, 'Personal'),
(9, 'Triple'),
(10, 'Cuadruple');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifa_habitacion`
--

CREATE TABLE `tarifa_habitacion` (
  `id` int(11) NOT NULL,
  `id_tarifa` int(11) DEFAULT NULL,
  `id_habitacion` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarifa_habitacion`
--

INSERT INTO `tarifa_habitacion` (`id`, `id_tarifa`, `id_habitacion`, `precio`) VALUES
(17, 8, 6, 380),
(18, 7, 6, 580),
(19, 9, 6, 700),
(20, 10, 6, 800),
(23, 7, 8, 580),
(24, 7, 7, 580),
(25, 7, 9, 580),
(26, 9, 10, 700),
(27, 7, 11, 580),
(28, 8, 12, 380),
(29, 8, 13, 380),
(30, 8, 14, 500),
(31, 7, 14, 600),
(32, 8, 15, 380),
(33, 7, 16, 580),
(34, 7, 17, 580),
(35, 9, 18, 700),
(36, 7, 19, 580),
(37, 8, 20, 380),
(38, 8, 21, 380),
(39, 9, 22, 700),
(40, 8, 23, 380),
(41, 7, 24, 580),
(42, 7, 25, 580),
(43, 9, 26, 700),
(65, 1, 3, 1000),
(66, 1, 5, 1000),
(67, 1, 4, 1000),
(68, 1, 41, 50000),
(69, 4, 52, 123),
(70, 1, 53, 1000),
(71, 4, 54, 12000),
(72, 8, 55, 5000),
(73, 7, 56, 10000),
(74, 10, 57, 40000),
(75, 1, 58, 40000),
(76, 9, 59, 30000),
(77, 1, 60, 15000),
(78, 9, 61, 15000),
(79, 4, 72, 1000),
(80, 8, 65, 10000),
(81, 9, 65, 1000),
(82, 8, 68, 500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_banco`
--

CREATE TABLE `tipo_banco` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `numero_cuenta` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_banco`
--

INSERT INTO `tipo_banco` (`id`, `nombre`, `numero_cuenta`, `valor`, `fecha_creada`) VALUES
(1, 'Banco Costa Rica', 102, 0, '2021-08-03'),
(2, 'Coocique', 103, 0, '2021-08-03'),
(4, 'Bac San Jose', 104, 0, '2021-08-09'),
(15, 'BP', 0, 0, '2021-12-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_categoria_venta`
--

CREATE TABLE `tipo_categoria_venta` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_categoria_venta`
--

INSERT INTO `tipo_categoria_venta` (`id`, `nombre`, `codigo`, `estado`, `fecha_creada`) VALUES
(8, '123', '123', 1, '2021-11-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `id` int(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor_extra` int(250) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_cliente`
--

INSERT INTO `tipo_cliente` (`id`, `nombre`, `valor_extra`, `estado`, `fecha_creada`) VALUES
(39, 'Huesped de habitación', 1, 0, '2021-06-09'),
(40, 'Cliente Habitual', 0, 0, '2021-06-09'),
(51, 'Cliente Existente', 2, 0, '2021-11-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_comprobante`
--

CREATE TABLE `tipo_comprobante` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_comprobante`
--

INSERT INTO `tipo_comprobante` (`id`, `nombre`, `estado`) VALUES
(1, 'Ticket', 1),
(2, 'Boleta', 2),
(3, 'Factura Fisica', 3),
(7, 'Factura Electrónica ', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descipcion` varchar(600) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id`, `nombre`, `descipcion`, `valor`, `fecha_creada`) VALUES
(2, 'NITE', 'Número de identificación tributario para personas físicas (NITE)', 4, '2018-02-15 09:24:24'),
(71, 'Persona física', 'Persona física costarricense', 1, '2021-08-03 16:44:55'),
(72, 'Persona jurídica', 'Persona jurídica costarricense estatal (pública) o persona jurídica costarricense privada', 2, '2021-08-03 16:45:09'),
(79, 'DIMEX', 'Persona física extranjera (DIMEX)', 3, '2021-11-09 10:23:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `banco` int(11) NOT NULL,
  `numero_aprobacion` int(11) NOT NULL,
  `numero_tarjeta` int(11) NOT NULL,
  `valor_hacienda` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id`, `nombre`, `valor`, `banco`, `numero_aprobacion`, `numero_tarjeta`, `valor_hacienda`, `estado`, `fecha_creada`) VALUES
(1, 'Transferencia Bancaria', 0, 1, 0, 0, '04', 0, '2018-02-15 09:25:24'),
(2, 'Tarjeta de Debito/Credito', 1, 1, 0, 0, '02', 0, '2018-02-15 09:25:24'),
(3, 'Deposito / Recaudado por terceros', 0, 1, 1, 0, '05', 0, '2018-08-22 00:00:00'),
(40, 'Cheque', 1, 0, 0, 0, '03', 0, '2022-02-23 12:43:06'),
(42, 'Efectivo ', 1, 0, 0, 0, '01', 0, '2022-02-23 12:45:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_proceso`
--

CREATE TABLE `tipo_proceso` (
  `id` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_proceso`
--

INSERT INTO `tipo_proceso` (`id`, `id_tipo_cliente`, `nombre`, `valor`, `fecha_creada`) VALUES
(19, 39, 'Cancelado', 1, '2021-06-09'),
(20, 40, 'Cancelado', 1, '2021-06-09'),
(23, 39, 'Cargar a Habitacion', 0, '2021-06-10'),
(27, 51, 'Cancelado', 0, '2021-12-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tmp`
--

CREATE TABLE `tmp` (
  `id_tmp` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `cantidad_tmp` int(11) DEFAULT NULL,
  `precio_tmp` double DEFAULT NULL,
  `precio_tmp_exo` double DEFAULT NULL,
  `sessionn_id` varchar(255) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_de_medida`
--

CREATE TABLE `unidad_de_medida` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `unidad_de_medida`
--

INSERT INTO `unidad_de_medida` (`id`, `nombre`, `descripcion`, `valor`, `estado`, `fecha_creada`) VALUES
(78, 'Sp', 'Servicios Profesionales', 1, 0, '2022-10-26'),
(79, 'm', 'Metro', 1, 0, '2022-10-26'),
(80, 'kg', 'Kilogramo', 1, 0, '2022-10-27'),
(81, 's', 'Segundo', 1, 0, '2022-11-03'),
(82, 'A', 'Ampere', 1, 0, '2022-11-03'),
(83, 'K', 'Kelvin', 1, 0, '2022-11-03'),
(84, 'mol', 'Mol', 1, 0, '2022-11-03'),
(85, 'cd', 'Candela', 1, 0, '2022-11-03'),
(86, 'm²', 'metro cuadrado', 1, 0, '2022-11-03'),
(87, 'm³', 'metro cúbico', 1, 0, '2022-11-03'),
(88, 'm/s', 'metro por segundo', 1, 0, '2022-11-03'),
(89, 'm/s²', 'metro por segundo cuadrado', 1, 0, '2022-11-03'),
(90, '1/m', '1 por metro', 1, 0, '2022-11-03'),
(91, 'kg/m³', 'kilogramo por metro cúbico', 1, 0, '2022-11-03'),
(92, 'A/m²', 'ampere por metro cuadrado', 1, 0, '2022-11-03'),
(93, 'A/m', 'ampere por metro', 1, 0, '2022-11-03'),
(94, 'mol/m³', 'mol por metro cúbico', 1, 0, '2022-02-03'),
(95, 'cd/m²', 'candela por metro cuadrado', 1, 0, '2022-02-03'),
(96, '1', 'uno (indice de refracción)', 1, 0, '2022-02-03'),
(97, 'rad', 'radián', 1, 0, '2022-02-03'),
(98, 'sr', 'estereorradián', 1, 0, '2022-02-03'),
(99, 'Hz', 'hertz', 1, 0, '2022-02-03'),
(100, 'N', 'newton', 1, 0, '2022-02-03'),
(101, 'Pa', 'pascal', 1, 0, '2022-02-03'),
(102, 'J', 'Joule', 1, 0, '2022-02-03'),
(103, 'W', 'Watt', 1, 0, '2022-02-03'),
(104, 'C', 'coulomb', 1, 0, '2022-02-03'),
(105, 'V', 'volt', 1, 0, '2022-02-03'),
(106, 'F', 'farad', 1, 0, '2022-02-03'),
(107, 'Ω', 'ohm', 1, 0, '2022-02-03'),
(108, 'S', 'siemens', 1, 0, '2022-02-03'),
(109, 'Wb', 'weber', 1, 0, '2022-02-03'),
(110, 'T', 'tesla', 1, 0, '2022-02-03'),
(111, 'H', 'henry', 1, 0, '2022-02-03'),
(112, '°C', 'grado Celsius', 1, 0, '2022-02-03'),
(113, 'lm', 'lumen', 1, 0, '2022-02-03'),
(114, 'lx', 'lux', 1, 0, '2022-02-03'),
(115, 'Bq', 'Becquerel', 1, 0, '2022-02-03'),
(116, 'Gy', 'gray', 1, 0, '2022-02-03'),
(117, 'Sv', 'sievert', 1, 0, '2022-02-03'),
(118, 'kat', 'katal', 1, 0, '2022-02-03'),
(119, 'Pa·s', 'pascal segundo', 1, 0, '2022-02-03'),
(120, 'N·m', 'newton metro', 1, 0, '2022-02-03'),
(121, 'N/m', 'newton por metro', 1, 0, '2022-02-03'),
(122, 'rad/s', 'radián por segundo', 1, 0, '2022-02-03'),
(123, 'rad/s²', 'radián por segundo cuadrado', 1, 0, '2022-02-03'),
(124, 'W/m²', 'watt por metro cuadrado', 1, 0, '2022-02-03'),
(125, 'J/K', 'joule por kelvin', 1, 0, '2022-02-03'),
(126, 'J/(kg·K)', 'joule por kilogramo kelvin', 1, 0, '2022-02-03'),
(127, 'J/kg', 'joule por kilogramo', 1, 0, '2022-02-03'),
(128, 'W/(m·K)', 'watt por metro kevin', 1, 0, '2022-02-03'),
(129, 'J/m³', 'joule por metro cúbico', 1, 0, '2022-02-03'),
(130, 'V/m', 'volt por metro', 1, 0, '2022-02-03'),
(131, 'C/m³', 'coulomb por metro cúbico', 1, 0, '2022-02-03'),
(132, 'C/m²', 'coulomb por metro cuadrado', 1, 0, '2022-02-03'),
(133, 'F/m', 'farad por metro', 1, 0, '2022-02-03'),
(134, 'H/m', 'henry por metro', 1, 0, '2022-02-03'),
(135, 'J/mol', 'joule por mol', 1, 0, '2022-02-03'),
(136, 'J/(mol·K)', 'joule por mol kelvin', 1, 0, '2022-02-03'),
(137, 'C/kg', 'coulomb por kilogramo', 1, 0, '2022-02-03'),
(138, 'Gy/s', 'gray por segundo', 1, 0, '2022-02-03'),
(139, 'W/sr', 'watt por estereorradián', 1, 0, '2022-02-03'),
(140, 'W/(m²·sr)', 'watt por metro cuadrado estereorradián', 1, 0, '2022-02-03'),
(141, 'kat/m³', 'katal por metro cúbico', 1, 0, '2022-02-03'),
(142, 'min', 'minuto', 1, 0, '2022-02-03'),
(143, 'h', 'hora', 1, 0, '2022-02-03'),
(144, 'd', 'día', 1, 0, '2022-02-03'),
(145, 'º', 'grado', 1, 0, '2022-02-03'),
(146, '´', 'minuto', 1, 0, '2022-02-03'),
(147, '´´', 'segundo', 1, 0, '2022-02-03'),
(148, 'L', 'litro', 1, 0, '2022-02-03'),
(149, 't', 'tonelada', 1, 0, '2022-02-03'),
(150, 'Np', 'neper', 1, 0, '2022-02-03'),
(151, 'B', 'bel', 1, 0, '2022-02-03'),
(152, 'eV', 'electronvolt', 1, 0, '2022-02-03'),
(153, 'u', 'unidad de masa atómica unificada', 1, 0, '2022-02-03'),
(154, 'ua', 'unidad astronómica', 1, 0, '2022-02-03'),
(155, 'Unid', 'Unidad', 1, 0, '2022-02-03'),
(156, 'Gal', 'Galón', 1, 0, '2022-02-03'),
(157, 'g', 'Gramo', 1, 0, '2022-02-03'),
(158, 'Km', 'Kilometro', 1, 0, '2022-02-03'),
(159, 'ln', 'pulgada', 1, 0, '2022-02-03'),
(160, 'cm', 'centimetro', 1, 0, '2022-02-03'),
(161, 'mL', 'mililitro', 1, 0, '2022-02-03'),
(162, 'mm', 'Milimetro', 1, 0, '2022-02-03'),
(163, 'Oz', 'Onzas', 1, 0, '2022-02-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `pago` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `lastname`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `created_at`, `pago`) VALUES
(1, 'Administrador', 'admin', 'admin', 'admin@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2016-12-13 09:08:03', 0),
(4, 'Gabriel', 'Quesada', 'gabrielqa', 'AmbientePruebas', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2021-06-24 14:49:36', 0),
(5, 'Esteban ', 'Alcanzas ', 'Esteban', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:01:26', 0),
(6, 'Priscilla ', 'Cordero', 'Priscilla', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:01:41', 0),
(7, 'Loana ', 'Rodriguez', 'Loana', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:00', 0),
(8, 'Bryan', 'Alpizar', 'Bryan', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:17', 0),
(9, 'Josue', 'Martínez ', 'Josue', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:02:36', 0),
(10, 'Maria', 'Carvajal', 'Maria', 'gabrielquesadajgqa@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 1, '2022-02-03 14:02:52', 0),
(11, 'Juan', 'Lopez', 'Juan', 'Gabrielqqquesada@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:39:14', 0),
(12, 'Rodrigo', 'Vega', 'Rodrigo', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 14:59:50', 0),
(13, 'Valeria', 'Arguellas', 'Valeria', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:08', 0),
(14, 'Valentina', 'Romp', 'Valentina', 'usuariodapos@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:26', 0),
(15, 'Joshua', 'Vega', 'Joshua', 'Gabrielqqquesada@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', NULL, 1, 0, '2022-02-03 15:00:46', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `utilidad_producto`
--

CREATE TABLE `utilidad_producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `valor` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha_creada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `utilidad_producto`
--

INSERT INTO `utilidad_producto` (`id`, `nombre`, `valor`, `estado`, `fecha_creada`) VALUES
(1, ' 30%', 30, 1, '2021-10-27'),
(2, '17%', 10, 1, '2021-10-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `id_tipo_comprobante` int(11) DEFAULT NULL,
  `nro_comprobante` varchar(25) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `tipo_operacion` int(11) NOT NULL DEFAULT 1,
  `total` double DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL,
  `fecha_creada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `id_tipo_comprobante`, `nro_comprobante`, `id_proveedor`, `id_tipo_pago`, `tipo_operacion`, `total`, `id_usuario`, `id_caja`, `fecha_creada`) VALUES
(371, 1, '2312', 753, 1, 1, 150.35328, 4, 121, '2021-12-07 11:20:28'),
(372, 1, '12121', 754, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:20:46'),
(373, 2, '12', 751, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:21:16'),
(374, 1, '13321', 746, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:29:23'),
(375, 1, '1231', 746, 1, 1, 1509.4992, 4, 121, '2021-12-07 11:29:58'),
(376, 1, '12121', 746, 1, 1, 150.35328, 4, 121, '2021-12-09 15:42:56'),
(377, 2, '1212', 751, 1, 1, 150.35328, 4, 121, '2021-12-09 15:43:48'),
(378, 1, '123123', 751, 1, 1, 150.35328, 4, 121, '2021-12-09 15:45:00'),
(379, 2, '2001', NULL, 1, 2, 133, 10, 130, '2022-02-03 16:46:11'),
(380, 1, '1312', NULL, 1, 2, 1000, 10, 130, '2022-02-03 16:54:03'),
(381, 3, '1', 879, 2, 1, 150.35328, 10, 130, '2022-02-03 17:03:46'),
(382, 3, '1', 880, 2, 1, 307006.875, 10, 130, '2022-02-03 17:10:16'),
(383, 3, '21122', 881, 2, 1, 150.35328, 4, 131, '2022-02-15 13:15:23'),
(384, 3, '1232', 0, 2, 1, 2684.88, 4, 131, '2022-02-17 13:31:59'),
(385, 3, '12323', 885, 2, 1, 2684.88, 4, 131, '2022-02-17 13:33:01'),
(386, 3, '2323', 0, 2, 1, 2684.88, 4, 131, '2022-02-17 13:33:40'),
(387, 3, '2331', 0, 2, 1, 0, 4, 131, '2022-02-17 13:35:06'),
(388, 3, '2122', 0, 2, 1, 0, 4, 131, '2022-02-17 13:38:39'),
(389, 3, '121', 889, 2, 1, 2684.88, 4, 131, '2022-02-17 13:40:08'),
(390, 3, '123', 0, 2, 1, 0, 4, 131, '2022-02-17 14:10:37'),
(391, 3, '123', 0, 2, 1, 0, 4, 131, '2022-02-17 14:11:28'),
(392, 3, '231', 0, 2, 1, 0, 4, 131, '2022-02-17 14:24:46'),
(393, 3, '321', 0, 2, 1, 0, 4, 131, '2022-02-17 14:33:15'),
(394, 3, '2312', 0, 2, 1, 0, 4, 131, '2022-02-17 14:33:43'),
(395, 3, '123', 0, 2, 1, 0, 4, 131, '2022-02-17 14:34:19'),
(396, 3, '16654', 0, 2, 1, 5843.2752, 4, 131, '2022-02-17 14:45:47'),
(397, 3, '123', 0, 2, 1, 1275.318, 4, 131, '2022-02-17 14:51:28'),
(398, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-17 14:59:10'),
(399, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-17 14:59:58'),
(400, 3, '1312', 881, 2, 1, 2684.88, 4, 131, '2022-02-17 15:04:22'),
(401, 3, '554', 881, 2, 1, 10739.52, 4, 131, '2022-02-17 16:49:03'),
(402, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-17 16:51:03'),
(403, 3, '123', 904, 2, 1, 5843.2752, 4, 131, '2022-02-17 17:05:40'),
(404, 3, '8874', 881, 2, 1, 10739.52, 4, 131, '2022-02-18 14:56:44'),
(405, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 15:00:47'),
(406, 3, '412', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 15:08:20'),
(407, 3, '4412', 881, 2, 1, 2711.7288, 4, 131, '2022-02-18 15:10:27'),
(408, 3, '331', 881, 2, 1, 10739.52, 4, 131, '2022-02-18 15:12:36'),
(409, 3, '4123', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 15:20:56'),
(410, 3, '858', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 15:23:15'),
(411, 3, '441', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 15:26:03'),
(412, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 15:31:58'),
(413, 3, '123', 881, 2, 1, 5369.76, 4, 131, '2022-02-18 15:37:21'),
(414, 3, '2421', 881, 2, 1, 2711.7288, 4, 131, '2022-02-18 15:39:44'),
(415, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 15:41:40'),
(416, 3, '3312', 881, 2, 1, 10739.52, 4, 131, '2022-02-18 15:45:50'),
(417, 3, '3312', 881, 2, 1, 12108.8088, 4, 131, '2022-02-18 15:48:19'),
(418, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 15:53:14'),
(419, 3, '4123', 881, 2, 1, 9397.08, 4, 131, '2022-02-18 15:54:26'),
(420, 3, '41212', 881, 2, 1, 5843.2752, 4, 131, '2022-02-18 16:04:39'),
(421, 3, '3232', 881, 2, 1, 7118.5932, 4, 131, '2022-02-18 16:30:34'),
(422, 3, '12323', 881, 2, 1, 9397.08, 4, 131, '2022-02-18 16:31:53'),
(423, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 16:32:29'),
(424, 3, '232', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 16:33:35'),
(425, 3, '321', 881, 2, 1, 1275.318, 4, 131, '2022-02-18 16:34:10'),
(426, 3, '787', 881, 2, 1, 2684.88, 4, 131, '2022-02-18 20:25:59'),
(427, 3, '123', 930, 2, 1, 10739.52, 4, 131, '2022-02-18 20:32:30'),
(428, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 08:55:21'),
(429, 3, '321', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 08:56:08'),
(430, 3, '221', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 08:58:49'),
(431, 3, '214', 934, 2, 1, 5843.2752, 4, 131, '2022-02-19 08:59:42'),
(432, 3, '20787', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:06:59'),
(433, 3, '241', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:10:35'),
(434, 3, '231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 09:13:57'),
(435, 3, '1412', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 09:16:03'),
(436, 3, '1241', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:19:35'),
(437, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:26:14'),
(438, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:28:33'),
(439, 3, '123', 881, 2, 1, 12014.838, 4, 131, '2022-02-19 09:29:59'),
(440, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:30:31'),
(441, 3, '12', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:31:40'),
(442, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:31:55'),
(443, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:32:32'),
(444, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 09:33:01'),
(445, 3, '123', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 09:33:35'),
(446, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:36:20'),
(447, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:37:58'),
(448, 3, '123', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 09:42:52'),
(449, 3, '123', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:43:33'),
(450, 3, '321', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 09:46:10'),
(451, 3, '2', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 09:52:22'),
(452, 3, '21', 881, 2, 1, 5369.76, 4, 131, '2022-02-19 09:52:33'),
(453, 3, '213', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 09:54:36'),
(454, 3, '1231', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 09:55:40'),
(455, 3, '211', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 10:00:37'),
(456, 3, '231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 10:10:17'),
(457, 3, '12', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 10:17:26'),
(458, 3, '31', 881, 2, 1, 5369.76, 4, 131, '2022-02-19 10:26:22'),
(459, 3, '123', 881, 2, 1, 2819.124, 4, 131, '2022-02-19 10:28:42'),
(460, 3, '88987', 881, 2, 1, 5369.76, 4, 131, '2022-02-19 10:30:39'),
(461, 3, '79887', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:34:16'),
(462, 3, '5456', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:35:39'),
(463, 3, '987', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 10:38:54'),
(464, 3, '65', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 10:42:05'),
(465, 3, '987', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:42:55'),
(466, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 10:53:05'),
(467, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:54:04'),
(468, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 10:55:20'),
(469, 3, '2011471', 881, 2, 1, 5843.2752, 4, 131, '2022-02-19 10:57:53'),
(470, 3, '312', 881, 2, 1, 9397.08, 4, 131, '2022-02-19 11:08:00'),
(471, 3, '987', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:17:04'),
(472, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 11:22:24'),
(473, 3, '1233', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 11:24:19'),
(474, 3, '1231', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 11:25:26'),
(475, 3, '231', 881, 2, 1, 10739.52, 4, 131, '2022-02-19 11:27:17'),
(476, 3, '123', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:28:01'),
(477, 3, '123', 881, 2, 1, 13451.2488, 4, 131, '2022-02-19 11:40:31'),
(478, 3, '31', 881, 2, 1, 2684.88, 4, 131, '2022-02-19 11:42:34'),
(479, 3, '31', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:45:07'),
(480, 3, '3123', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 11:46:27'),
(481, 3, '3213', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:50:21'),
(482, 3, '313', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:51:43'),
(483, 3, '33132', 881, 2, 1, 2711.7288, 4, 131, '2022-02-19 11:58:22'),
(484, 3, '312', 881, 2, 1, 1275.318, 4, 131, '2022-02-19 11:59:41'),
(485, 3, '321', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 08:37:47'),
(486, 3, '231', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 08:38:48'),
(487, 3, '321', 881, 2, 1, 2711.7288, 4, 131, '2022-02-21 08:40:27'),
(488, 3, '321', 881, 2, 1, 2711.7288, 4, 131, '2022-02-21 08:43:20'),
(489, 3, '321', 881, 2, 1, 2684.88, 4, 131, '2022-02-21 08:44:16'),
(490, 3, '312', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 08:54:55'),
(491, 3, '312', 881, 2, 1, 2684.88, 4, 131, '2022-02-21 08:55:52'),
(492, 3, '123', 881, 2, 1, 7118.5932, 4, 131, '2022-02-21 08:56:54'),
(493, 3, '331', 881, 2, 1, 10739.52, 4, 131, '2022-02-21 08:57:02'),
(494, 3, '441', 881, 2, 1, 5843.2752, 4, 131, '2022-02-21 08:57:07'),
(495, 3, '321', 881, 2, 1, 5843.2752, 4, 131, '2022-02-21 11:07:55'),
(496, 3, '31', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 11:09:06'),
(497, 3, '3123', 881, 2, 1, 1275.318, 4, 131, '2022-02-21 17:13:49'),
(498, 3, '231', 881, 2, 1, 10739.52, 4, 131, '2022-02-21 17:15:20'),
(499, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-22 09:25:33'),
(500, 3, '231', 881, 2, 1, 5843.2752, 4, 131, '2022-02-22 10:43:47'),
(501, 3, '22', 881, 2, 1, 1275.318, 4, 131, '2022-02-23 08:18:48'),
(502, 3, '1231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-23 08:21:22'),
(503, 3, '2131', 881, 2, 1, 5843.2752, 4, 131, '2022-02-23 08:27:17'),
(504, 3, '1231', 881, 2, 1, 2711.7288, 4, 131, '2022-02-23 08:46:46'),
(505, 3, '123', 881, 2, 1, 1275.318, 4, 131, '2022-02-23 08:49:26'),
(506, 3, '123', 881, 2, 1, 9397.08, 4, 131, '2022-02-23 08:50:43'),
(507, 3, '8417', 881, 3, 1, 5843.2752, 4, 131, '2022-02-23 11:07:03'),
(508, 3, '323', 881, 2, 1, 14766.84, 4, 131, '2022-02-23 12:57:44'),
(509, 3, '312', 881, 2, 1, 2684.88, 4, 131, '2022-02-23 13:12:56'),
(510, 3, '2321', 881, 2, 1, 10739.52, 4, 131, '2022-02-23 13:23:07'),
(511, 3, '11254', 881, 2, 1, 5369.76, 4, 131, '2022-02-23 15:40:54'),
(512, 3, '123', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:05:31'),
(513, 3, '123', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:18:05'),
(514, 3, '32', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:21:49'),
(515, 3, '123', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:24:51'),
(516, 3, '321', 881, 2, 1, 7.02295, 4, 131, '2022-02-23 16:26:01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_id_caja` (`id_caja`) USING BTREE;

--
-- Indices de la tabla `auto_cabys`
--
ALTER TABLE `auto_cabys`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_clientes_p`
--
ALTER TABLE `categoria_clientes_p`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_categoria` (`id_tipo_categoria`);

--
-- Indices de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descuento_productos`
--
ALTER TABLE `descuento_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `funcionalidades`
--
ALTER TABLE `funcionalidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `impuestos_productos`
--
ALTER TABLE `impuestos_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notificaciones_stocks`
--
ALTER TABLE `notificaciones_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `documento` (`documento`(10)),
  ADD KEY `categoria_promo` (`id_categoria_p`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bodega` (`id_bodega`),
  ADD KEY `FK_unidad_de_medida` (`id_unidad_medida`),
  ADD KEY `FK_descuento` (`id_descuento`) USING BTREE,
  ADD KEY `fk_proveedor` (`id_proveedor`);

--
-- Indices de la tabla `promociones`
--
ALTER TABLE `promociones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_impuesto` (`id_impuesto`),
  ADD KEY `FK_producto` (`id_producto`) USING BTREE;

--
-- Indices de la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_venta` (`id_venta`),
  ADD KEY `id_plazo` (`id_plazo`);

--
-- Indices de la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_producto` (`id_producto`) USING BTREE,
  ADD KEY `r_habitacion` (`id_habitacion`),
  ADD KEY `r_categoria` (`id_categoria`),
  ADD KEY `r_porcentaje` (`id_porcentaje`);

--
-- Indices de la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_utilidad` (`id_utilidad`),
  ADD KEY `FK_producto` (`id_producto`);

--
-- Indices de la tabla `subimpuestos_productos`
--
ALTER TABLE `subimpuestos_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_categoria_venta`
--
ALTER TABLE `tipo_categoria_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_cliente` (`id_tipo_cliente`) USING BTREE;

--
-- Indices de la tabla `tmp`
--
ALTER TABLE `tmp`
  ADD PRIMARY KEY (`id_tmp`);

--
-- Indices de la tabla `unidad_de_medida`
--
ALTER TABLE `unidad_de_medida`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `utilidad_producto`
--
ALTER TABLE `utilidad_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT de la tabla `auto_cabys`
--
ALTER TABLE `auto_cabys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `bodegas`
--
ALTER TABLE `bodegas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categoria_clientes_p`
--
ALTER TABLE `categoria_clientes_p`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cliente_proceso`
--
ALTER TABLE `cliente_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `descuento_productos`
--
ALTER TABLE `descuento_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estado_cajas`
--
ALTER TABLE `estado_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estado_de_pago`
--
ALTER TABLE `estado_de_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `funcionalidades`
--
ALTER TABLE `funcionalidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `gasto`
--
ALTER TABLE `gasto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `impuestos_productos`
--
ALTER TABLE `impuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mantenimiento_cajas`
--
ALTER TABLE `mantenimiento_cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `notificaciones_stocks`
--
ALTER TABLE `notificaciones_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `periodos_pago`
--
ALTER TABLE `periodos_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1033;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433;

--
-- AUTO_INCREMENT de la tabla `proceso_sueldo`
--
ALTER TABLE `proceso_sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proceso_venta`
--
ALTER TABLE `proceso_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=608;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=567;

--
-- AUTO_INCREMENT de la tabla `promociones`
--
ALTER TABLE `promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=858;

--
-- AUTO_INCREMENT de la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT de la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `subimpuestos_productos`
--
ALTER TABLE `subimpuestos_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tarifa`
--
ALTER TABLE `tarifa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tarifa_habitacion`
--
ALTER TABLE `tarifa_habitacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT de la tabla `tipo_banco`
--
ALTER TABLE `tipo_banco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tipo_categoria_venta`
--
ALTER TABLE `tipo_categoria_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `tmp`
--
ALTER TABLE `tmp`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=934;

--
-- AUTO_INCREMENT de la tabla `unidad_de_medida`
--
ALTER TABLE `unidad_de_medida`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `utilidad_producto`
--
ALTER TABLE `utilidad_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=517;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aperturas_cajas`
--
ALTER TABLE `aperturas_cajas`
  ADD CONSTRAINT `aperturas_cajas_ibfk_1` FOREIGN KEY (`id_caja`) REFERENCES `mantenimiento_cajas` (`id`);

--
-- Filtros para la tabla `categoria_venta`
--
ALTER TABLE `categoria_venta`
  ADD CONSTRAINT `fk_tipo_categoria` FOREIGN KEY (`id_tipo_categoria`) REFERENCES `tipo_categoria_venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_bodega` FOREIGN KEY (`id_bodega`) REFERENCES `bodegas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_descuentos_as` FOREIGN KEY (`id_descuento`) REFERENCES `descuento_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_unidad_de_medida` FOREIGN KEY (`id_unidad_medida`) REFERENCES `unidad_de_medida` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_impuestos_productos`
--
ALTER TABLE `r_impuestos_productos`
  ADD CONSTRAINT `fk_impuesto` FOREIGN KEY (`id_impuesto`) REFERENCES `impuestos_productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_plazo_venta`
--
ALTER TABLE `r_plazo_venta`
  ADD CONSTRAINT `plazo` FOREIGN KEY (`id_plazo`) REFERENCES `periodos_pago` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venta` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_promociones`
--
ALTER TABLE `r_promociones`
  ADD CONSTRAINT `categoria_promo` FOREIGN KEY (`id_categoria`) REFERENCES `categoria_clientes_p` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `promociones` FOREIGN KEY (`id_porcentaje`) REFERENCES `promociones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `r_utilidades_productos`
--
ALTER TABLE `r_utilidades_productos`
  ADD CONSTRAINT `FK_producto_utilidad` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_utilidad_producto` FOREIGN KEY (`id_utilidad`) REFERENCES `utilidad_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tipo_proceso`
--
ALTER TABLE `tipo_proceso`
  ADD CONSTRAINT `tipo_proceso_ibfk_1` FOREIGN KEY (`id_tipo_cliente`) REFERENCES `tipo_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
