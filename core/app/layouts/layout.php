<!-- VISTA PRINCIPAL DE LA APLICACION -->
<?php require __DIR__ . '/../../../core/app/action/actions-functions-promos.php'; ?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Alcesac Pruebas </title>
    <link rel="icon" type="image/ico" href="assets/images/favicon.ico" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ============================================
        ================= Stylesheets ===================
        ============================================= -->
    <!-- vendor css files -->
    <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/vendor/animate.css">
    <link rel="stylesheet" href="assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="assets/js/vendor/animsition/css/animsition.min.css">
    <link rel="stylesheet" href="assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="assets/js/vendor/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="assets/js/vendor/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="assets/js/vendor/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="assets/js/vendor/chosen/chosen.css">
    <link rel="stylesheet" href="assets/js/vendor/summernote/summernote.css">
    <link rel="stylesheet" href="assets/css/vendor/weather-icons.min.css">
    <link rel="stylesheet" href="css/login/sty.css" type="text/css" media="all" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
    <!--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
    <!--<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <!-- project main css files -->
    <!-- ALERTS  -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="assets/css/main.css">
    <!--/ stylesheets -->



    <!-- ==========================================
        ================= Modernizr ===================
        =========================================== -->
    <script src="assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <!--/ modernizr -->





</head>





<body id="minovate" class="<?php if (isset($_SESSION["user_id"]) || isset($_SESSION["client_id"])) : ?> appWrapper sidebar-sm-forced <?php else : ?>appWrapper<?php endif; ?>">






    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->












    <!-- ====================================================
        ================= Application Content ===================
        ===================================================== -->
    <div id="wrap" class="animsition">






        <!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->

        <?php if (isset($_SESSION["user_id"]) || isset($_SESSION["client_id"])) : ?>
            <section id="header">
                <header class="clearfix">

                    <!-- Branding -->
                    <div class="branding">
                        <a class="brand" href="./">
                            <span><strong style="margin-left:-20px;">Administración Hotel</strong></span>
                        </a>
                        <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
                    </div>
                    <!-- Branding end -->



                    <!-- Left-side navigation -->
                    <ul class="nav-left pull-left list-unstyled list-inline">
                        <li class="sidebar-collapse divided-right">
                            <a href="#" class="collapse-sidebar">
                                <i class="fa fa-outdent"></i>
                            </a>
                        </li>

                    </ul>
                    <!-- Left-side navigation end -->


                    <!-- Search -->
                    <div class="search" id="main-search">
                        <form action="index.php?view=cliente" method="get">
                            <input type="hidden" name="view" value="recepcion">
                            <input type="text" class="form-control underline-input" name="buscar" placeholder="Buscar Cliente...">
                        </form>
                    </div>
                    <!-- Search end -->




                    <!-- Right-side navigation -->
                    <ul class="nav-right pull-right list-inline">
                        <?php
                        $u = null;
                        $u = UserData::getById(Session::getUID());
                        ?>
                        <?php if ($u->is_admin) : ?>
                            <!-- Opcion Administracion -->
                            <li class="<?php if ($_GET['view'] == 'users') {
                                            echo 'active';
                                        } ?>">
                            <li class="dropdown nav-profile">

                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="https://img.icons8.com/color/50/000000/user-shield.png" class="img-circle size-30x30" /><span>Administración<i class="fa fa-angle-down"></i></span>
                                </a>

                                <ul class="dropdown-menu animated littleFadeInRight" role="menu">
                                    <a href class="dropdown-toggle" data-toggle="dropdown">
                                        <li><a href="./?view=users"><i class="fa fa-sign-out"></i> Usuarios</a></li>
                                        <li><a href="./?view=configuracion"><i class="fa fa-sign-out"></i> Configuración</a></li>
                                        <li><a href="./?view=base_de_datos"><i class="fa fa-sign-out"></i> Base de datos</a></li>
                                        <li></li>
                                </ul>
                            </li>
                            <!-- Opcion Mantenimientos -->

                            <li style="font-size: 13px;" class="<?php if ($_GET['view'] == 'mantenimientos') {
                                                                    echo 'active';
                                                                } ?>"><a href="./?view=mantenimientos"><img style="margin-right:12px;" class="img-circle size-30x30" src="https://img.icons8.com/fluent/48/000000/toolbox.png" /><span>Mantenimientos</span></a></li>
                            </li>
                            <!-- Opcion Configuracion -->
                            <li class="dropdown nav-profile">

                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="https://img.icons8.com/dusk/64/000000/services--v2.png" class="img-circle size-30x30" /><span> Configuración<i class="fa fa-angle-down"></i></span>
                                </a>

                                <?php

                                $datos_Facturacion = get_estado_function(4);
                                ?>



                                <ul class="dropdown-menu animated littleFadeInRight" role="menu">
                                    <a href class="dropdown-toggle" data-toggle="dropdown">
                                        <li><a href="./?view=funcionalidades"><i class="fa fa-caret-right"></i> Funcionalidades</a></li>
                                        <li><a href="./?view=habitacion"> <i class="fa fa-caret-right"></i> Habitaciones</a></li>
                                        <li><a href="./?view=categoria"> <i class="fa fa-caret-right"></i> Categorías</a></li>
                                        <li><a href="./?view=tarifa"> <i class="fa fa-caret-right"></i> Tarifas</a></li>

                                        <?php if ($datos_Facturacion->estado != 0) {
                                        ?>

                                            <li><a href="./?view=sala_facturacion"> <i class="fa fa-caret-right"></i> Facturación Electrónica</a></li>

                                        <?php } ?>

                                        <li></li>
                                </ul>
                            </li>
                        <?php endif; ?>


                        <li class="dropdown nav-profile">

                            <a href class="dropdown-toggle" data-toggle="dropdown">
                                <img src="https://img.icons8.com/nolan/64/nui2.png" class="img-circle size-30x30" />
                                <span><?php if (isset($_SESSION["user_id"])) {
                                            echo UserData::getById($_SESSION["user_id"])->name;
                                        } ?><i class="fa fa-angle-down"></i></span>
                            </a>

                            <ul class="dropdown-menu animated littleFadeInRight" role="menu">
                                <!--
                                <li>
                                    <a href="#">
                                        <span class="badge bg-greensea pull-right">86%</span>
                                        <i class="fa fa-user"></i>Perfil
                                    </a>
                                </li>
                            -->
                                <li>
                                    <a href="./logout.php">
                                        <i class="fa fa-sign-out"></i>Salir
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-comments"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- Right-side navigation end -->



                </header>

            </section>
            <!--/ HEADER Content  -->





            <!-- =================================================
            ================= CONTROLS Content ===================
            ================================================== -->
            <div id="controls">





                <!-- ================================================
                ================= SIDEBAR Content ===================
                ================================================= -->
                <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navegación <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <?php
                                        $u = null;
                                        $u = UserData::getById(Session::getUID());
                                        ?>

                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                        <ul id="navigation">
                                            <li class="<?php if ($_GET['view'] == 'reserva') {
                                                            echo 'active';
                                                        } ?>"><a href="./?view=reserva">
                                                    <img src="assets/images/d.png" alt="" class="img-circle size-30x30"><span></span>
                                                    </i> <span>Reserva</span></a></li>
                                            <li class="<?php if ($_GET['view'] == 'recepcion') {
                                                            echo 'active';
                                                        } ?>">
                                                <a href="index.php?view=recepcion"><img class="img-circle size-30x30" src="https://img.icons8.com/dusk/64/000000/front-desk.png" /></i> <span>Recepción</span>
                                                    <!--<span class="badge bg-lightred">6</span>-->
                                                </a>
                                                <!-- <ul>
                                                    <li><a href="./?view=venta"><i class="fa fa-caret-right"></i> Registrar Habitación</a></li>
                                                </ul>  -->
                                            </li>
                                            <li class="<?php if ($_GET['view'] == 'productos' or $_GET['view'] == 'venta') {
                                                            echo 'active';
                                                        } ?>">
                                                <a role="button" tabindex="0"><img class="img-circle size-30x30" src="https://img.icons8.com/emoji/48/000000/department-store.png" /></i> <span>Punto de venta</span></a>
                                                <ul>
                                                    <li><a href="./?view=venta"><i class="fa fa-caret-right"></i> Vender</a></li>
                                                    <li><a href="./?view=productos"><i class="fa fa-caret-right"></i> Productos</a></li>
                                                    <li><a href="./?view=pre_reporte_rango_producto"><i class="fa fa-caret-right"></i> Reporte por rango productos</a></li>
                                                    <!--
                                                    <li><a href="./?view=proveedores"><i class="fa fa-caret-right"></i> Proveedores</a></li>
                                                -->
                                                </ul>
                                            </li>

                                            <li class="<?php if ($_GET['view'] == 'kardex' or $_GET['view'] == 'compra' or $_GET['view'] == 'lista_compra') {
                                                            echo 'active';
                                                        } ?> ">
                                                <a role="button" tabindex="0"><img class="img-circle size-30x30" src="https://img.icons8.com/plasticine/100/000000/bookmark--v2.png" /> <span>Inventario</span></a>
                                                <ul>
                                                    <li><a href="./?view=kardex"><i class="fa fa-caret-right"></i> Kardex</a></li>
                                                    <li><a href="./?view=compra"><i class="fa fa-caret-right"></i> Reabastecer</a></li>
                                                    <li><a href="./?view=lista_compra"><i class="fa fa-caret-right"></i> Consulta compra por fecha</a></li>
                                                </ul>
                                            </li>
                                            <!-- Habilitar solo cuando el servicio requiera una caja -->
                                            <!--<li class="<?php if ($_GET['view'] == 'apertura_caja') {
                                                                echo 'active';
                                                            } ?>">
                                                <a role="button" tabindex="0"><img class="img-circle size-30x30" src="https://img.icons8.com/officel/50/000000/cash-register.png"/> <span>Módulo caja</span></a>
                                                <ul>
                                                    <li><a href="./?view=apertura_caja"><i class="fa fa-caret-right"></i> Apertura caja</a></li>
                                                    <li><a href="./?view=cierre_caja"><i class="fa fa-caret-right"></i> Cierre caja</a></li>
                                                    <li><a href="./?view=reporte_caja"><i class="fa fa-caret-right"></i> Resumen liquidación</a></li>
                                                </ul>
                                            </li>-->
                                            <!-- Habilitar solo cuando el servicio requiera mas de una caja -->
                                            <li class="<?php if ($_GET['view'] == 'sala_de_cajas') {
                                                            echo 'active';
                                                        } ?>">
                                                <a href="./?view=sala_de_cajas" role="button" tabindex="0"><img class="img-circle size-30x30" src="https://img.icons8.com/officel/50/000000/cash-register.png" /> <span>Módulo caja</span></a>
                                            </li>

                                            <li class="<?php if ($_GET['view'] == 'egreso' or $_GET['view'] == 'egresos' or $_GET['view'] == 'reporte_gasto') {
                                                            echo 'active';
                                                        } ?>">
                                                <a role="button" tabindex="0"><img class="img-circle size-30x30" src="https://img.icons8.com/color/50/000000/bearish.png" /> <span>Egresos</span> </a>
                                                <ul>
                                                    <li><a href="index.php?view=egreso"><i class="fa fa-table"></i> <span>Nuevo egreso</span></a></li>
                                                    <li><a href="./?view=egresos"> <i class="fa fa-caret-right"></i> Lista egresos</a></li>
                                                    <li><a href="./?view=reporte_gasto"> <i class="fa fa-caret-right"></i> Reportes por fechas</a></li>
                                                </ul>
                                            </li>
                                            <?php
                                            $datos_promocion = get_estado_function(3);
                                            if ($datos_promocion->estado != 0) {
                                            ?>

                                                <li>
                                                    <a href="./?view=sala_promociones" role="button" tabindex="0"><img class="img-circle size-30x30" src="https://cdn-icons.flaticon.com/png/512/791/premium/791979.png?token=exp=1645136424~hmac=8d85a7127d4ddf6d65326b1a21beebe8" /> <span>Promos</span></a>
                                                </li>
                                            <?php } ?>
                                            <!--
                                          
                                            <li class="<?php if ($_GET['view'] == 'sueldo') {
                                                            echo 'active';
                                                        } ?>">
                                                <a role="button" tabindex="0"><i class="fa fa-money"></i> <span>Pagos</span></a>
                                                <ul>
                                                    <li><a href="./?view=sueldo"><i class="fa fa-caret-right"></i> Realizar pago</a></li>
                                                    
                                                </ul>
                                            </li> 
                                        -->



                                            <?php if ($u->is_admin) : ?>

                                                <li class="<?php if ($_GET['view'] == 'cliente') {
                                                                echo 'active';
                                                            } ?>">
                                                    <a href="index.php?view=cliente"><img class="img-circle size-30x30" src="https://img.icons8.com/fluent/48/000000/check-in-desk.png" /></i> <span>Contactos</span></a>
                                                    <ul>
                                                        <li><a href="index.php?view=cliente"><i class="fa fa-caret-right"></i> <span>Clientes</span></a></li>
                                                        <li><a href="index.php?view=proveedores"> <i class="fa fa-caret-right"></i> Proveedores</a></li>
                                                    </ul>
                                                </li>
                                            <?php endif; ?>

                                            <?php

                                            if ($datos_Facturacion->estado != 0) {
                                            ?>

                                                <li>
                                                    <a href="./?view=sala_facturacion" role="button" tabindex="0"><img class="img-circle size-30x30" src="https://cdn-icons-png.flaticon.com/512/4313/4313458.png" /> <span>Facturas</span></a>
                                                </li>
                                            <?php } ?>
                                            <li>
                                                <a href="./?view=sala_reportes" role="button" tabindex="0"><img class="img-circle size-30x30" src="https://img.icons8.com/color/50/000000/bar-chart--v1.png" /> <span>Reportes</span></a>
                                            </li>
                                            <!--<li>
                                              <a  role="button" tabindex="0"><img class="img-circle size-30x30" src="https://img.icons8.com/fluent/48/000000/toolbox.png"/> <span>Mantenimientos</span></a>
                                              <ul >
                                                <li class="dropdown-submenu">
                                                    <a href="#">Recepción</a>
                                                    <ul class="dropdown-menu">
                                                    <li><a href="./?view=tipo_documento"><i class="fa fa-caret-right"></i>Documento</a></li>                                              
                                                <li><a href="./?view=estado_de_pago"><i class="fa fa-caret-right"></i>Estado de pago</a></li>     
                                                <li><a href="./?view=tipo_de_pago"><i class="fa fa-caret-right"></i>Medio de pago</a></li>
                                                    </ul>
                                                </li>  
                                                <li class="dropdown-submenu">
                                                    <a href="#">Punto de venta</a>
                                                    <ul style="margin-top:-80px;" class="dropdown-menu">
                                                    <li><a href="./?view=tipo_cliente"><i class="fa fa-caret-right"></i>Tipo Cliente</a></li>                                              
                                                    <li><a href="./?view=tipo_comprobante"><i class="fa fa-caret-right"></i>Tipo Comprobante</a></li>     
                                                    <li><a href="./?view=periodos_pago"><i class="fa fa-caret-right"></i>Periodos de pago</a></li>
                                                    <li><a href="./?view=tipo_proceso"><i class="fa fa-caret-right"></i>Proceso de pago</a></li>
                                                    </ul>
                                                </li>    
                                                <li class="dropdown-submenu">
                                                    <a href="#">Modulo de caja</a>
                                                    <ul class="dropdown-menu">
                                                    <li><a href="./?view=cajas"><i class="fa fa-caret-right"></i>Cantidad de cajas</a></li>
                                                    <li><a href="./?view=sala_de_cajas"><i class="fa fa-caret-right"></i>Sala de cajas</a></li>                                                                                            
                                                    </ul>
                                                </li>                                                  </ul> 
                                            </li>-->


                                        </ul>
                                        <!--/ NAVIGATION Content -->


                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>


                </aside>
                <!--/ SIDEBAR Content -->


            </div>
            <!--/ CONTROLS Content -->

        <?php endif; ?>


        <!-- ====================================================
            ================= CONTENT ===============================
            ===================================================== -->



        <?php if (isset($_SESSION["user_id"]) || isset($_SESSION["client_id"])) : ?>
            <section id="content">
                <div class="page page-sidebar-xs-layout">

                    <?php View::load("principal"); ?>

                </div>
            </section>
        <?php else : ?>


            <style type="text/css">
                html,
                body {
                    height: 100%;
                    background: #fff;
                    overflow: hidden;
                }
            </style>


            <script type="text/javascript" src="css/wow/wow.js"></script>
            <script type="text/javascript">
                /* WOW animations */

                wow = new WOW({
                    animateClass: 'animated',
                    offset: 100
                });
                wow.init();
            </script>

            <!--
<img src="assets/images/bg//blurred-bg-3.jpg" class="login-img wow fadeIn" alt="">

  col -->
            <div class="">

                <ul class="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <div class="area">
                    <section class="login">
                        <?php
                        $configuracion = ConfiguracionData::getAllConfiguracion();
                        //Validamos la cantidad de datos encontrados dentro del objeto
                        $object = (array) $configuracion;
                        $object = count($object);
                        if ($object > 0) {
                            $nombre = $configuracion->nombre;
                            $logo = $configuracion->logo;
                        ?>



                        <?php } else {
                            $nombre = '';
                            $logo = '';
                        ?>
                        <?php }
                        ?>
                        <div class="login_box">
                            <div class="left">
                                <div class="top_link"><a href="#"><img src="https://drive.google.com/u/0/uc?id=16U__U5dJdaTfNGobB_OpwAJ73vM50rPV&export=download" alt="">Volver</a></div>
                                <div class="contact">
                                    <form role="form" action="./?action=processlogin" method="post">
                                        <h3>Iniciar sesión</h3>
                                        <input class="form-control" required="" id="exampleInputEmail1" placeholder="Ingrese su usuario" name="username" type="text" placeholder="Usuario">
                                        <input type="password" name="password" required="" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                                        <button class="submit">Ingresar</button>
                                    </form>
                                </div>
                            </div>
                            <div class="right">
                                <div class="right-text">
                                    <h2>Alcesac</h2>
                                    <h5>Sistema de Hoteleria</h5>
                                </div>
                                <div class="right-inductor"><img src="https://lh3.googleusercontent.com/fife/ABSRlIoGiXn2r0SBm7bjFHea6iCUOyY0N2SrvhNUT-orJfyGNRSMO2vfqar3R-xs5Z4xbeqYwrEMq2FXKGXm-l_H6QAlwCBk9uceKBfG-FjacfftM0WM_aoUC_oxRSXXYspQE3tCMHGvMBlb2K1NAdU6qWv3VAQAPdCo8VwTgdnyWv08CmeZ8hX_6Ty8FzetXYKnfXb0CTEFQOVF4p3R58LksVUd73FU6564OsrJt918LPEwqIPAPQ4dMgiH73sgLXnDndUDCdLSDHMSirr4uUaqbiWQq-X1SNdkh-3jzjhW4keeNt1TgQHSrzW3maYO3ryueQzYoMEhts8MP8HH5gs2NkCar9cr_guunglU7Zqaede4cLFhsCZWBLVHY4cKHgk8SzfH_0Rn3St2AQen9MaiT38L5QXsaq6zFMuGiT8M2Md50eS0JdRTdlWLJApbgAUqI3zltUXce-MaCrDtp_UiI6x3IR4fEZiCo0XDyoAesFjXZg9cIuSsLTiKkSAGzzledJU3crgSHjAIycQN2PH2_dBIa3ibAJLphqq6zLh0qiQn_dHh83ru2y7MgxRU85ithgjdIk3PgplREbW9_PLv5j9juYc1WXFNW9ML80UlTaC9D2rP3i80zESJJY56faKsA5GVCIFiUtc3EewSM_C0bkJSMiobIWiXFz7pMcadgZlweUdjBcjvaepHBe8wou0ZtDM9TKom0hs_nx_AKy0dnXGNWI1qftTjAg=w1920-h979-ft" alt=""></div>
                            </div>
                        </div>
                    </section>

                </div>

            <?php endif; ?>


            <!--/ CONTENT -->






            </div>
            <!--/ Application Content -->



            <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
            <?php if (isset($_GET['view'])) { ?>
                <?php if ($_GET['view'] != 'reserva' and $_GET['view'] != 'proceso' and $_GET['view'] != 'proceso_venta' and $_GET['view'] != 'reporte_fecha_circular') { ?>
                    <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
                <?php }; ?>
            <?php } else if (!isset($_SESSION["user_id"])) { ?>
                <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>

            <?php } ?>

            <script src="assets/js/vendor/bootstrap/bootstrap.min.js"></script>

            <script src="assets/js/vendor/jRespond/jRespond.min.js"></script>

            <script src="assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

            <script src="assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

            <script src="assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>

            <script src="assets/js/vendor/screenfull/screenfull.min.js"></script>

            <script src="assets/js/vendor/owl-carousel/owl.carousel.min.js"></script>

            <script src="assets/js/vendor/daterangepicker/moment.min.js"></script>

            <script src="assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


            <script src="assets/js/vendor/chosen/chosen.jquery.min.js"></script>


            <script src="assets/js/vendor/summernote/summernote.min.js"></script>

            <script src="assets/js/vendor/coolclock/coolclock.js"></script>
            <script src="assets/js/vendor/coolclock/excanvas.js"></script>
            <script src="assets/js/vendor/footable/footable.all.min.js"></script>

            <!--/ vendor javascripts -->




            <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
            <script src="assets/js/main.js"></script>
            <!--/ custom javascripts -->








</body>


</html>