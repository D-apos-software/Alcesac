<?php
// El siguiente codigo se encarga de registrar todos los datos de una factura al momento de procesarla, se registran en bd
// Relacion: VISTA VENTAS

$dt = new Proceso_hacienda();

$dt->id_cliente = $_POST['id_cliente'];
$dt->tipo_documento = $_POST['tipo_documento'];
$dt->tipo_cedula = $_POST['tipo_cedula'];
$dt->fecha_emision = $_POST['fecha_emision'];
$dt->emisor_nombre = $_POST['emisor_nombre'];
$dt->emisor_tipo_identif = $_POST['emisor_tipo_identif'];
$dt->emisor_num_identif = $_POST['emisor_num_identif'];
$dt->nombre_comercial = $_POST['nombre_comercial'];
$dt->emisor_provincia = $_POST['emisor_provincia'];
$dt->emisor_canton = $_POST['emisor_canton'];
$dt->emisor_distrito = $_POST['emisor_distrito'];
$dt->emisor_barrio = $_POST['emisor_barrio'];
$dt->emisor_cod_pais_tel = $_POST['emisor_cod_pais_tel'];
$dt->emisor_email = $_POST['emisor_email'];

$dt->receptor_nombre = $_POST['receptor_nombre'];
$dt->receptor_tipo_identif = $_POST['receptor_tipo_identif'];
$dt->receptor_num_identif = $_POST['receptor_num_identif'];
$dt->receptor_provincia = $_POST['receptor_provincia'];
$dt->receptor_canton = $_POST['receptor_canton'];
$dt->receptor_distrito = $_POST['receptor_distrito'];
$dt->receptor_barrio = $_POST['receptor_barrio'];
$dt->receptor_cod_pais_tel = $_POST['receptor_cod_pais_tel'];
$dt->receptor_tel = $_POST['receptor_tel'];
$dt->receptor_email = $_POST['receptor_email'];

$dt->condicion_venta = $_POST['condicion_venta'];
$dt->plazo_credito = $_POST['plazo_credito'];
$dt->medio_pago = $_POST['medio_pago'];
$dt->cod_moneda = $_POST['cod_moneda'];
$dt->tipo_cambio = $_POST['tipo_cambio'];

$dt->total_serv_gravados = $_POST['total_serv_gravados'];
$dt->total_serv_exentos = $_POST['total_serv_exentos'];
$dt->total_merc_gravada = $_POST['total_merc_gravada'];
$dt->total_merc_exenta = $_POST['total_merc_exenta'];
$dt->total_gravados = $_POST['total_gravados'];
$dt->total_exentos = $_POST['total_exentos'];
$dt->total_ventas = $_POST['total_ventas'];
$dt->total_descuentos = $_POST['total_descuentos'];
$dt->total_ventas_neta = $_POST['total_ventas_neta'];
$dt->total_impuesto = $_POST['total_impuesto'];
$dt->total_comprobante = $_POST['total_comprobante'];
$dt->token_llave = $_POST['token_llave'];
$dt->id_caja = $_POST['id_caja'];
$dt->id_usuario = $_POST['id_usuario'];
$dt->omitir_receptor = $_POST['omitir_receptor'];
$dt->estado = $_POST['estado'];
$sas = $dt->add();
echo ($sas[1]);
