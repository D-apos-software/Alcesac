<?php
  // El siguiente codigo se encarga de mostrar el primer grafico visualizado en la vista de reportes.
  // Relacion: VISTA GRAFICOS/REPORTES


$base = new Database();
$con = $base->connect();
	
$return_arr = array();

/* Si la conexión a la base de datos , ejecuta instrucción SQL. */
if ($con) 
{
    
	
        // Iniciamos la condiciones si es dia mes o año
		if($_POST['dato'] == 'dia'){
			//Ventas de productos por dias del mes (x)
            $fetch = mysqli_query($con,"SELECT sum(precio) as monto, Day(fecha_creada) as fecha FROM `proceso_venta`
            WHERE tipo_operacion = 1 and MONTH(proceso_venta.fecha_creada) = MONTH(CURRENT_DATE())
            and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY day(fecha_creada)"); 

		}else if($_POST['dato'] == 'mes'){

			//Ventas de productos por meses del año 
            $fetch = mysqli_query($con,"SELECT sum(precio) as monto, month(fecha_creada) as fecha FROM `proceso_venta` WHERE tipo_operacion = 1
			and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY MONTH(fecha_creada)"); 

		
		}else if($_POST['dato'] == 'año'){
			//Ventas de productos por años 
            $fetch = mysqli_query($con,"SELECT sum(precio) as monto, year(fecha_creada) as fecha FROM `proceso_venta` WHERE tipo_operacion = 1 GROUP BY year(fecha_creada)"); 

		}

		else {
			//Ventas de productos por dias del mes
            $fetch = mysqli_query($con,"SELECT sum(precio) as monto, Day(fecha_creada) as fecha FROM `proceso_venta`
            WHERE tipo_operacion = 1 and MONTH(proceso_venta.fecha_creada) = MONTH(CURRENT_DATE())
            and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY day(fecha_creada)"); 
		}
    
    
    /* Recuperar y almacenar en conjunto los resultados de la consulta.*/
	while ($row = mysqli_fetch_array($fetch)) {
		$row_array['monto'] = $row['monto'];
		$row_array['fecha']=$row['fecha'];
		array_push($return_arr,$row_array);
    }  
}

/* Cierra la conexión. */
mysqli_close($con);

/* Codifica el resultado del array en JSON. */

echo json_encode($return_arr);

?>