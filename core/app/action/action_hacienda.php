<?php
// El siguiente codigo se encarga de realizar calculos y validar datos especificos del ministerio de hacienda.
// Relacion: Ventas y Alquiler


set_include_path('/../../../core/app/action/action-functions-products.php');

function productos_list($id_producto, $cantidad, $precioTmp)
{
    $codigo_tar = 0;
    $imp = 0;
    $montotal = 0;
    $desc = 0;
    $tarifa = 0;

    $Total_sr_gravados = 0;
    $Total_sr_exentos = 0;
    $Total_mr_gravados = 0;
    $Total_mr_exentos = 0;
    $Total_gravados = 0;
    $Total_exento = 0;
    $Total_ventas = 0;
    $Total_Descuento = 0;
    $Total_ventas_neta = 0;
    $Total_impuestos = 0;
    $Total_comprobante = 0;

    $resDescuento = 0;

    $producto = ProductoData::getById($id_producto);
    // Codigo Cabys 
    //print($producto->codigo."<br>");
    $codigo = $producto->codigo;
    // Cantidad
    //print($cantidad."<br>");
    // Unidad De Medida
    $unidadMd = Unidad_de_medida::getById($producto->id_unidad_medida);
    //print($unidadMd->nombre."<br>");
    $Unidad_De_Medida = $unidadMd->nombre;
    // Descripcion
    //print($producto->descripcion."<br>");
    $descripcion = $producto->descripcion;

    //----------------- Montos -------------------//

    $precio = total_con_utilidad($producto->precio_venta, $producto->utilidad);
    //---------- TOTAL DE VENTAS ---------//
    $Total_ventas += $precio * $cantidad;


    // Precio Unitario
    //print($producto->precio_venta."<br>");
    $precioU = $precio; //$precioTmp;
    // Monto Total
    //print(montoTotal($producto->precio_venta, $cantidad)."<br>");



    $mTotal = montoTotal($precio, $cantidad);

    $montotal += $mTotal;

    // Subtotal
    // print(montoTotal($producto->precio_venta, $cantidad)."<br>");

    //------------------ Descuento ------------//

    $resDescuento = montoDescuento($producto->id_descuento, $mTotal);
    $subtotal = $mTotal - $resDescuento;
    $desc += montoDescuento($producto->id_descuento, $mTotal);

    //---------- TOTAL DE DESCUENTO ----------//
    $Total_Descuento += montoDescuento($producto->id_descuento, $mTotal);
    //---------- FIN TOTAL DE DESCUENTO ------//

    // ------------- VENTAS NETA -------------//
    $Total_ventas_neta =  $mTotal - $Total_Descuento;
    // ----------- FIN VENTAS NETA -------------//



    //------------------ fin Descuento ------------//



    //------------------ Impuesto ------------//

    $get_impuesto_valor = Tipo_r_impuestos_productos::get_impuesto_valor($producto->id);
    $suma_impuesto = 0;

    if (isset($get_impuesto_valor[0])) {
        foreach ($get_impuesto_valor as $impuesto) {
            $tarifa = $impuesto->valor;
            $codigo_tar = $impuesto->tipo_impuesto;
            $impuesto = $impuesto->valor / 100;
            $impuesto = $Total_ventas_neta * $impuesto;
            $imp += $impuesto;
            $Total_impuestos += $impuesto;
            $suma_impuesto = $suma_impuesto + $impuesto;
        }
    }

    //------------------ fin Impuesto ------------//

    // ------------ TOTAL COMPROBANTE -------------//
    $Total_comprobante +=  $Total_ventas_neta + $Total_impuestos;
    // ----------- FIN TOTAL COMPROBANTE ------------//



    if ($Unidad_De_Medida != 'Sp') {

        //Productos Grabados Impuestos
        if ($imp != 0) {
            $Total_mr_gravados += $montotal;
            $Total_gravados += $montotal;
        } else {
            $Total_mr_exentos += $montotal;
            $Total_exento += $montotal;
        }
    } else {
        //Servicios Grabados Impuestos
        if ($imp != 0) {
            $Total_sr_gravados += $montotal;
            $Total_gravados += $montotal;
        } else {
            $Total_sr_exentos += $montotal;
            $Total_exento += $montotal;
        }
    }






    // Monto Total Lineal
    //print(montoTotal($producto->precio_venta, $cantidad)."<br>");
    $mtLinea = MontoLineal($subtotal, $imp, $cantidad);

    $DTImpuestos = ['01', $codigo_tar, $tarifa, $Total_impuestos];

    $listaP = [$codigo, $cantidad, $Unidad_De_Medida, $descripcion, $precioU, $mTotal, $subtotal, $mtLinea, $DTImpuestos];

    $valoresGlobales =
        [
            $Total_sr_gravados,
            $Total_sr_exentos,
            $Total_gravados,
            $Total_exento,
            $Total_ventas,
            $Total_Descuento,
            $Total_ventas_neta,
            $Total_impuestos,
            $Total_comprobante,
            $Total_mr_gravados,
            $Total_mr_exentos
        ];
    $listGeneral = [$valoresGlobales, $listaP];

    return $listGeneral;
}


function montoTotal($mt, $cantidad)
{
    return $mt * $cantidad;
}


function montoDescuento($id, $valor)
{
    $Descuento = Descuento_Producto::getById($id);
    $resp = ($Descuento->valor / 100) * $valor;
    return $resp;
}


function MontoLineal($valor, $impV, $cant)
{
    if ($impV != 0) {
        return $impV + $valor;
        //return ($impV * $cant) + $valor;
    } else {
        return $valor;
    }
}

function FechaAct()
{
    $ret = "2022-01-17T00:46:00-06:00";
    $ret = "2022-02-22T00:22:22-51:52";
    $data = getdate();
    if (intval($data['mon']) > 9) {
        $fecha = '' . $data['year'] . '-' . $data['mon'] . '-' . $data['mday'] . 'T00:' . $data['wday'] . ':' . $data['hours'] . '-' . $data['minutes'] . ':' . $data['seconds'];
    } else {
        $fecha = '' . $data['year'] . '-0' . $data['mon'] . '-' . $data['mday'] . 'T00:' . $data['mday'] . ':' . $data['hours'] . '-' . $data['minutes'] . ':' . $data['seconds'];
    }
    return $fecha;
}


function TipoCedula($id)
{
    $id_r = strval($id);
    $id_r = '0' . $id_r;
    return $id_r;
}

function TipoCedulaText($id)
{
    /* fisico, juridico, dimex o nite */
    if ($id == 1) {
        return 'fisica';
    } else if ($id == 2) {
        return 'juridico';
    } else if ($id == 3) {
        return 'dimex';
    } else if ($id == 4) {
        return 'nite';
    }
}


function TipoProd()
{
}
