<?php

// El siguiente codigo se encarga del mantenimiento de categoria de clientes
// Relacion: VISTA CATEGORIA CLIENTE


$action = $_POST["id_estado"];

//Add
if ($action == 'add') {

    $habitacion = new Categoria_Clientes_P();
    $habitacion->nombre = $_POST["nombre"];
    $habitacion->add();
    print "<script>window.location='index.php?view=categoria_clientes_p';</script>";
}

//Update

else if ($action == 'update') {

    $documento = Categoria_Clientes_P::getById($_POST["id"]);
    $documento->nombre = $_POST["nombre"];
    $documento->update();
    print "<script>window.location='index.php?view=categoria_clientes_p';</script>";
} else if ($action == 'actualizar') {

    $documento = Categoria_Clientes_P::getById($_POST["id"]);

    if ($documento->estado == 0) {
        $documento->estado = 1;
    } else if ($documento->estado == 1) {
        $documento->estado = 0;
    }
    $documento->update_e();
    print "<script>window.location='index.php?view=categoria_clientes_p';</script>";
}

//Delete

if ($action == 'delete') {
    $habi = Categoria_Clientes_P::getById($_POST['id']);
    $habi->del();
    //Core::redir("./index.php?view=m_documento");
}
