<?php
// El siguiente codigo se encarga de eliminar documentos almacenados en la carpetas respaldos pruebas (estos son respaldos de base de datos)
// Relacion: VISTA BASE DE DATOS


$files = glob('DB/Respaldos_prueba/*'); // obtenemos los nombres
foreach ($files as $file) { // itramos los datos
  if (is_file($file))
    unlink($file); // eliminamos archivos
}
print "<script>window.location='index.php?view=base_de_datos';</script>";
