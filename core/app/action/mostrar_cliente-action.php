<script src="js/vista_venta/busqueda_cliente.js"></script>

<?php
/* En este action se actualiza la informacion al seleccionar el tipo de cliente, enviamos los parametros desde la vista venta-view
y los obtenemos para poder saber cual es el id del tipo de cliente que se selecciono.
*/
$ID = $_POST['id_mostrar_cliente'];
$ID_valor_extra = null;
if ($ID != "0" and $ID != 0) {

  $Info = TipoCliente::getById($ID);
  $valor_extra = $Info->valor_extra;
  $ID_valor_extra = strval($valor_extra);
}
if (isset($_POST['id_mostrar_cliente']) and $ID_valor_extra == '1') { ?>



  <link rel="stylesheet" href="css/vista_cliente/style.css">
  <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-12">
        <label>Cargar a una Habitación</label>
        <?php $habitaciones = ProcesoData::getProceso(); ?>
        <select class="form-control select2" required name="id_operacion">
          <option value="">---- Selecciona Habitación ----</option>
          <?php foreach ($habitaciones as $habitacion) : ?>
            <option value="<?php echo $habitacion->id; ?>"><?php echo 'Habitacion::  ' . $habitacion->getHabitacion()->nombre . ' &nbsp;&nbsp;&nbsp; Huesped:: ' . $habitacion->getCliente()->nombre; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
  </div>
  <div><?php //print_r($Info);
        //echo $ID
        ?></div>

  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>Proceso</label>
        <!-- <select class="form-control select2" required  name="pagado" >
                        <option value="1">Cancelado</option>  
         </select> -->
        <?php $tipo_clientes = TipoProceso::getTipoCliente($ID); ?>
        <select class="form-control select2" name="pagado" id="pagado" required>
          <option value="">--- Selecciona ---</option>
          <?php foreach ($tipo_clientes as $tipo_cliente) : ?>
            <option value="<?php echo $tipo_cliente->valor; ?>"><?php echo $tipo_cliente->nombre; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="col-sm-6">
        <label>Exonerado</label>
        <input type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1" style="height: 26px; margin:-2px;">
      </div>
    </div>
  <?php } else if (isset($ID_valor_extra) and $ID_valor_extra == '0') { ?>


    <div class="form-group">
      <div class="row">
        <div class="col-sm-6">
          <label>Tipo de Identificación</label>
          <?php $tipos_identificacion = TipoDocumentoData::getAll(); ?>
          <select class="form-control" required name="tipo_documento" id="tipo_documento">
            <?php foreach ($tipos_identificacion as $id_t) : ?>
              <option value="<?php echo $id_t->valor ?>"><?php echo $id_t->nombre ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-sm-6">
          <label>Identificación</label>
          <input type="number" name="documento" id="documento" class="form-control" required placeholder="Identificación">
        </div>
        <input type="hidden" name="validacion_email" class="form-control" value="0">
        <!--<div class="col-sm-12"> 
                    <label>Teléfono</label>
                    <input type="text" name="telefono" class="form-control"  placeholder="Teléfono Celular">
                  </div>-->
      </div>
    </div>
  </div>
  </div>

  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>Nombres del cliente</label>
        <input type="text" required name="nombre_cliente" id="nombre_cliente" class="form-control" placeholder="Ingrese nombre (Opcional)">
      </div>
      <div class="col-sm-6">
        <label>Correo Electrónico</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email (Opcional)">
      </div>
      <input type="hidden" name="validacion_email" class="form-control" value="0">
      <!--<div class="col-sm-12"> 
                    <label>Teléfono</label>
                    <input type="text" name="telefono" class="form-control"  placeholder="Teléfono Celular">
                  </div>-->
    </div>
  </div>


  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>Proceso</label>
        <!-- <select class="form-control select2" required  name="pagado" >
                        <option value="1">Cancelado</option>  
         </select> -->
        <?php $tipo_clientes = TipoProceso::getTipoCliente($ID); ?>
        <select class="form-control select2" required name="pagado" id="pagado">
          <option value="">--- Selecciona ---</option>
          <?php foreach ($tipo_clientes as $tipo_cliente) : ?>
            <option value="<?php echo $tipo_cliente->id_tipo_cliente; ?>"><?php echo $tipo_cliente->nombre; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="col-sm-6">
        <label>Exonerado</label>
        <input type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1" style="height: 26px; margin:-2px;">
      </div>
    </div>

    <?php
    $datos_Facturacion = Funcionalidad::getById(4);
    ?>
    <?php if($datos_Facturacion->estado != 0){ ?>
    <div class="form-group" style="margin-top:10px!important;">
      <div class="row">
        <div class="col-sm-12">
          <label>C - Promocion</label>
          <?php $categoria =  Categoria_Clientes_P::getActivos(); ?>
          <select class="form-control select2" name="categoria" id="categoria" style=" width: 100%!important;" required>
            <option value="0">Seleccionar (Opcional)</option>
            <?php foreach ($categoria as $datos_c) : ?>
              <option value="<?php echo $datos_c->id ?>"><?php echo $datos_c->nombre ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
    <?php } ?>

  <?php } else if (isset($ID_valor_extra) and $ID_valor_extra == '2') {  ?>

    <button href="" data-toggle="modal" data-target="#myModalC" class="form-control select2">Buscar Cliente</button>

    <!--<div class="form-group">
      <div class="row">
        <div class="col-sm-12">
          <label>Buscar Cliente</label>
          <input class="form-control select2" id="marcas" type="text" name="Marcas" placeholder="ID de Cliente">
          <?php
          $personasD = PersonaData::get_documento();
          $vD = array();
          $vN = array();
          foreach ($personasD as $persona) {
            array_push($vD, $persona->documento);
          }
          ?>
        </div>
      </div>
    </div>-->

    <br>
    <div id="cliente_doc">
    </div>
  </div>


<?php } ?>

<script>
  var marcas = <?= json_encode($vD)  ?>;

  function autocomplete(inp, arr) {
    var currentFocus;
    inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      closeAllLists();
      if (!val) {
        return false;
      }
      currentFocus = -1;
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      this.parentNode.appendChild(a);
      for (i = 0; i < arr.length; i++) {
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          b = document.createElement("DIV");
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          b.addEventListener("click", function(e) {
            inp.value = this.getElementsByTagName("input")[0].value;
            closeAllLists();
            var doc_client = inp.value;
            var parametros = {
              "documento": doc_client,
              "id": 0,
              "id_mostrar_cliente": "show_data",
              "tipo": <?php echo $ID  ?>
            }
            $.ajax({
              type: "POST",
              url: 'index.php?action=actions_datos_cliente',
              data: parametros,
              success: function(resp) {
                $('#cliente_doc').html(resp);
              }
            });
          });
          a.appendChild(b);
        }
      }
    });

    inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {

        currentFocus++;

        addActive(x);
      } else if (e.keyCode == 38) {
        currentFocus--;

        addActive(x);
      } else if (e.keyCode == 13) {

        e.preventDefault();
        if (currentFocus > -1) {

          if (x) x[currentFocus].click();
        }
      }
    });

    function addActive(x) {
      if (!x) return false;
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);

      x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }

    function closeAllLists(elmnt) {

      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }

    document.addEventListener("click", function(e) {
      closeAllLists(e.target);
    });
  }

  autocomplete(document.getElementById("marcas"), marcas);
</script>