<?php

//Añadimos las relaciones de las promociones 
//Habitacion y Producto

function insert_relacion_p_y_h($habitacion, $producto, $porcentaje, $categoria)
{
	$proceso = new Tipo_r_promociones();
	$proceso->id_producto = $producto;
	$proceso->id_habitacion = $habitacion;
	$proceso->id_categoria = $categoria;
	$proceso->id_porcentaje = $porcentaje;
	$proceso->add();
}

function update_del($id_promocion)
{
	$proceso = Tipo_r_promociones::getById($id_promocion);
	$proceso->del();
}
function update_del_f_promo($id_promocion)
{
	$proceso_p = Tipo_r_promociones::getByID_Promo($id_promocion);
	$proceso_p->del_promo();
}

function get_estado_function($id)
{
	$proceso_p = Funcionalidad::getById($id);
	return $proceso_p;
}


//Creamos validacion de productos en promocion 
function val_productos_promocion($id_producto, $precio_producto, $id_cliente)
{

	//print($id_producto.'-'.$precio_producto.'-'.$id_cliente);



	$data_clt = PersonaData::getById($id_cliente);
	//Sacamos la categoria del cliente
	$id_categoria = $data_clt->id_categoria_p;
	//print($id_categoria);
	$data_vp = Tipo_r_promociones::getby_idP($id_producto, $id_categoria);

	//print_r($data_vp);
	if (isset($data_vp)) {
		$data_vp = $data_vp->valor / 100;
		$precio_pr = $data_vp * $precio_producto;
		$precio_pr = $precio_producto - $precio_pr;
		return $precio_pr;
	} else {
		return $precio_producto;
	}
}


//Creamos validacion de productos en habitacion 
function val_habitacion_promocion($id_habit, $precio_habitacion, $id_cliente)
{


	$data_clt = PersonaData::getById($id_cliente);
	//Sacamos la categoria del cliente
	$id_categoria = $data_clt->id_categoria_p;
	//print($id_categoria);
	if ($id_categoria > 0) {
		$data_vp = Tipo_r_promociones::getby_idH($id_habit, $id_categoria);
		//print_r($data_vp);
		if (isset($data_vp)) {
			$data_vp = $data_vp->valor / 100;
			$precio_pr = $data_vp * $precio_habitacion;
			$precio_pr = $precio_habitacion - $precio_pr;
			return $precio_pr;
		} else {
			return $precio_habitacion;
		}
	} else {
		return $precio_habitacion;
	}
}
