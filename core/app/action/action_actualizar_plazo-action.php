<?php
// El siguiente codigo se encarga de actualizar los periodos de pago 
// Relacion: VISTA VENTA


$estado = $_POST['estado'];
if ($estado == 0) {
    try {
        $habitacion = new PeriodosData();
        $habitacion->nombre = $_POST["nombre"];
        $habitacion->dias = $_POST["dias"];
        $habitacion->add();
        echo 0;
        unset($estado);
    } catch (Exception $e) {
        echo 1;
        unset($estado);
    }
} else if ($estado == 1) {
?>
    <?php $tipo_comprobantes = PeriodosData::getAll(); ?>
    <select data-placeholder="..." id="periodo" name="periodo" class="form-control" style="text-transform:uppercase;" onchange="CargarNV(this.value);">
        <option value="">--- Selecciona ---</option>
        <?php foreach ($tipo_comprobantes as $tipo_comprobante) : ?>
            <option value="1"><?php echo $tipo_comprobante->nombre; ?></option>
        <?php endforeach; ?>
        <option value="0">Nuevo plazo +</option>
    </select>
<?php
    unset($estado);
}
