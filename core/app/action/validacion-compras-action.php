<?php
require __DIR__ . '/../../../core/app/action/action-functions-products.php';

if ($_POST['estado'] == "bodega") {


    $id_producto = $_POST["id_producto"];
    $cantidad = $_POST["cantidad_aum"];
    $Data_bodega = ProductoData::getById($id_producto);
    $bodega_stock = ProcesoVentaData::get_stock_bodega($Data_bodega->id_bodega);
    $total_stock = $bodega_stock->stock + $bodega_stock->compras;
    $total_stock = $total_stock - $bodega_stock->ventas;
    $total_stock = $total_stock + $cantidad;

    if ($bodega_stock->cantidad_maxima > $total_stock) {
        echo 1;
    } else {
        echo 2;
    }
} else if ($_POST['estado'] == "cant_maxima") {


    $id_producto = $_POST["id_producto"];
    $cantidad = $_POST["cantidad_aum"];
    $stock = $_POST["stock"];

    $validacion_stock_maximo = stock_maximo_producto($id_producto, $cantidad, $stock);
   
    echo $validacion_stock_maximo;
   
}
