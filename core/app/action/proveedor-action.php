<head>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
</head>
<?php
// El siguiente codigo se encarga de agregar un nuevo proveedor
// Relacion: VISTA PROVEEDORES

$estado = $_POST["id_estado"];
if ($estado == "agregar") {
} else if ($estado == "editar") {
} else if ($estado == "eliminar") {
    if (count($_POST) > 0) {

        $cliente = new PersonaData();
        $cliente->tipo_documento = $_POST["tipo_documento"];
        $cliente->documento = $_POST["documento"];
        $cliente->nombre = $_POST["nombre"];
        $razon_social = "NULL";
        if ($_POST["razon_social"] != "") {
            $razon_social = $_POST["razon_social"];
        }
        $direccion = "NULL";
        if ($_POST["direccion"] != "") {
            $direccion = $_POST["direccion"];
        }

        $cliente->razon_social = $razon_social;
        $cliente->direccion = $direccion;

        $cliente->addProveedor();
?>
        <script>
            swal({
                title: "¡Se eliminó correctamente!",
                icon: 'success',
                text: "Se eliminó correctamente.<br>",
                type: "success",
            }).then(function() {
                window.location = "index.php?view=proveedor";
            });
        </script>

<?php

    }
}
