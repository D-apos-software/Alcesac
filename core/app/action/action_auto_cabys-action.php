<?php
// El siguiente codigo se encarga de validar si existe el codigo cabys mostrado en la lista de productos 
// Relacion: VISTA PRODUCTOS


$estado = $_POST['et'];

//Agregamos lista cabys
if ($estado == 0) {

    $existente =  existe($_POST["cabys"]);

    if ($existente == true) {
        echo 2;
    } else {
        try {
            $data = new Auto_Cabys();
            $data->descripcion = $_POST["descripcion"];
            $data->cabys = $_POST["cabys"];
            $data->add();
            echo 0;
        } catch (Exception $e) {
            echo 1;
        }
    }
}
//Modificamos lista cabys
else if ($estado == 1) {

    try {
        $data = Auto_Cabys::getById($_POST["id"]);
        $data->descripcion = $_POST["descripcion"];
        $data->update();
        echo 1;
    } catch (Exception $e) {
        echo 2;
    }
}
//Actualizams estado lista cabys
else if ($estado == 2) {

    $func = Auto_Cabys::getById($_POST["id"]);
    if ($func->estado == 0) {
        $func->estado = 1;
    } else if ($func->estado == 1) {
        $func->estado = 0;
    }
    $func->update_estado();
}
//Eliminamos lista cabys
else {

    $func = Auto_Cabys::getById($_POST['id']);
    $func->del();
}

function existe($val)
{
    $function = Auto_Cabys::getByCodigo($val);

    //return true;
    if (isset($function)) {
        return true;
    } else {
        return false;
    }
}
