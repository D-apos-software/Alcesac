<?php
// El siguiente codigo se encarga del mantenimiento de PROMOCIONES
// Relacion: VISTA PROMOCIONES

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
</head>

<body>
    <?php
    require __DIR__ . '/../../../core/app/action/actions-functions-promos.php';

    $estado = $_POST['estado'];
    //AGREGAR
    if ($estado == 'agregar') {
        $wrong = 0;
        $categoria = $_POST['categoria'];
        $promocion = new Promociones();
        $promocion->valor = $_POST['valor'];
        $promocion->descripcion = $_POST['descripcion'];
        $data_promo = $promocion->add();
        // ---------------- Luego ----------------------------
        // Agregar relaciones entre tablas en r_promociones
        if (isset($_POST["dropdown-group"]) || isset($_POST["dropdown2-group"])) {

            // Habitaciones 
            if (isset($_POST["dropdown-group"])) {
                $data = $_POST["dropdown-group"];
                foreach ($data as $habitacion) {
                    $value_hab = Tipo_r_promociones::get_Hb_Cat($habitacion, $categoria);
                    //print_r($value_hab);
                    if (empty($value_hab) == 1) {
                        insert_relacion_p_y_h($habitacion, 0, $data_promo[1], $categoria);
                    } else {
                        $wrong += 1;
    ?>
                        <script type="text/javascript">
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Habitaciones con promociones iguales',
                            }).then(function() {
                                window.location = "index.php?view=sala_promociones";
                            });
                        </script>
                    <?php
                    }
                }
            }
            // Productos 
            if (isset($_POST["dropdown2-group"])) {
                $data_p = $_POST["dropdown2-group"];
                foreach ($data_p as $producto) {
                    $value_pr = Tipo_r_promociones::get_Pr_Cat($producto, $categoria);
                    if (empty($value_pr) == 1) {
                        insert_relacion_p_y_h(0, $producto, $data_promo[1], $categoria);
                    } else {
                        $wrong += 1;
                    ?>
                        <script type="text/javascript">
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Productos con promociones iguales',
                            }).then(function() {
                                window.location = "index.php?view=sala_promociones";
                            });
                        </script>
            <?php
                    }
                }
            }
        }
        if ($wrong == 0) {
            ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'success',
                    title: 'Satisfactorio',
                    showConfirmButton: false,
                    timer: 1700
                }).then(function() {
                    window.location = "index.php?view=sala_promociones";
                })
            </script>
    <?php
        }
    }
    //ACTUALIZAR
    else if ($estado == 'editar') {
        $categoria = $_POST['categoria'];
        $promocion = Promociones::getById($_POST['id']);
        $promocion->valor = $_POST['valor'];
        $promocion->descripcion = $_POST['descripcion'];
        $promocion->update();
        // ---------------- Luego Eliminamos los antiguos----------------------------

        update_del_f_promo($_POST['id']);
        // Agregar las nuevas relaciones entre tablas en r_promociones
        // Habitaciones 
        if (isset($_POST["dropdown-group"])) {
            $data = $_POST["dropdown-group"];
            //print_r($data);
            foreach ($data as $habitacion) {
                insert_relacion_p_y_h($habitacion, 0, $_POST['id'], $categoria);
            }
        }
        // Productos 
        if (isset($_POST["dropdown2-group"])) {
            $data_p = $_POST["dropdown2-group"];
            //print_r($data_p);
            foreach ($data_p as $producto) {
                insert_relacion_p_y_h(0, $producto, $_POST['id'], $categoria);
            }
        }
        Core::redir("./index.php?view=sala_promociones");
    }
    //CAMBIAR ESTADO
    else if ($estado == 'estado') {
        $promocion = Promociones::getById($_POST['id']);
        if ($promocion->estado == 0) {
            $promocion->estado = 1;
        } else {
            $promocion->estado = 0;
        }
        $promocion->update_estado();
        Core::redir("./index.php?view=sala_promociones");
    }
    //ELIMINAR
    else if ($estado == 'eliminar') {
        $proceso = Promociones::getById($_POST['id']);
        $proceso->del();
    }

    ?>
</body>

</html>