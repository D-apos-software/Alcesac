<?php
// El siguiente codigo se encarga del mantenimiento de categoria de ventas
// Relacion: VISTA CATEGORIA VENTAS

$action = $_POST["id_estado"];

//Add
if ($action == 'add') {

    $bodega = new Categoria_venta();
    $bodega->nombre = $_POST["nombre"];
    $bodega->codigo = $_POST["codigo"];
    $bodega->id_tipo_categoria = $_POST["id_tipo_categoria"];
    $bodega->estado = 1;
    $bodega->add();
    print "<script>window.location='index.php?view=sala_categoria_ventas';</script>";
}

//Update
else if ($action == 'update') {
    $bodega = Categoria_venta::getById($_POST["id_documento"]);
    $bodega->codigo = $_POST["codigo"];
    $bodega->nombre = $_POST["nombre"];
    $bodega->id_tipo_categoria = $_POST["id_tipo_categoria"];
    $bodega->estado = $_POST["estado"];
    $bodega->update();
    //print('<br>'.$_POST["codigo"].'<br>'.$_POST["nombre"].'<br>'.$_POST["id_tipo_categoria"].'<br>'.$_POST["estado"]);
    print "<script>window.location='index.php?view=sala_categoria_ventas';</script>";
}


//Desactivar
else if ($action == 'desactivar') {
    print('<br><br><br><br>');
    $bodega = Categoria_venta::getById($_POST["id"]);
    $bodega->estado = 0;
    $bodega->update_estado();
    print "<script>window.location='index.php?view=sala_categoria_ventas';</script>";
}
//Delete
if ($action == 'delete') {

    $bodega = Categoria_venta::getById($_POST['id']);
    $bodega->del();

    //Core::redir("./index.php?view=m_documento");
}
