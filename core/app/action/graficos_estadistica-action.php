<?php
// El siguiente codigo se encarga de mostrar los datos estadisticos de la vista reportes
// Relacion: VISTA ESTADISTICAS/REPORTES


$base = new Database();
$con = $base->connect();

$return_arr = array();

/* Si la conexión a la base de datos , ejecuta instrucción SQL. */
if ($con) {
	if ($_POST['paso'] == '1') {


		$fecha_ini = $_POST['fecha_init'];
		$fecha_fin = $_POST['fecha_fin'];

		//Dia 
		if ($_POST['fecha_gc'] == '1') {
			// Iniciamos la condiciones si es dia mes o año
			if ($_POST['dato'] == '1') {
				//Ganancia
				$fetch = mysqli_query($con, "SELECT sum(precio) as total, Day(fecha_salida) as fecha, Month(fecha_salida) as mes, year(fecha_salida) as ano FROM `proceso` WHERE  date(fecha_salida) >= \"$fecha_ini\" AND date(fecha_salida) <= \"$fecha_fin\" AND pagado = 1 GROUP by Day(fecha_salida),Month(fecha_salida),Year(fecha_salida) ORDER BY YEAR(fecha_salida),month(fecha_salida),day(fecha_salida)");
			} else {
				//Cantidad
				$fetch = mysqli_query($con, "SELECT COUNT(fecha_salida) as total, Day(fecha_salida) as fecha, Month(fecha_salida) as mes, year(fecha_salida) as ano FROM `proceso` WHERE date(fecha_salida) >= \"$fecha_ini\" AND date(fecha_salida) <= \"$fecha_fin\" AND pagado = 1 GROUP by Day(fecha_salida),Month(fecha_salida),Year(fecha_salida) ORDER BY YEAR(fecha_salida),month(fecha_salida),day(fecha_salida)");
			}
		}
		//Mes 
		if ($_POST['fecha_gc'] == '2') {
			// Iniciamos la condiciones si es dia mes o año
			if ($_POST['dato'] == '1') {
				//Ganancia
				$fetch = mysqli_query($con, "SELECT sum(total) as total, MONTH(fecha_salida) as fecha, year(fecha_salida) as ano FROM `proceso` WHERE date(fecha_salida) >= \"$fecha_ini\" AND date(fecha_salida) <= \"$fecha_fin\" AND pagado = 1 GROUP by Month(fecha_salida),Year(fecha_salida) ORDER BY YEAR(fecha_salida),month(fecha_salida)");
			} else {
				//Crecimiento
				$fetch = mysqli_query($con, "SELECT MONTH(fecha_salida) as fecha, COUNT(fecha_salida) as total, year(fecha_salida) as ano FROM `proceso` WHERE  date(fecha_salida) >= \"$fecha_ini\" AND date(fecha_salida) <= \"$fecha_fin\"  AND pagado = 1 GROUP by Month(fecha_salida),Year(fecha_salida) ORDER BY YEAR(fecha_salida),month(fecha_salida)");
			}
		}
		//Año
		if ($_POST['fecha_gc'] == '3') {
			// Iniciamos la condiciones si es dia mes o año
			if ($_POST['dato'] == '1') {
				//Ganancia
				$fetch = mysqli_query($con, "SELECT sum(total) as total, YEAR(fecha_salida) as fecha FROM `proceso` WHERE date(fecha_salida) >= \"$fecha_ini\" AND date(fecha_salida) <= \"$fecha_fin\" AND pagado = 1 GROUP BY(YEAR(fecha_salida))");
			} else {
				//Crecimiento
				$fetch = mysqli_query($con, "SELECT YEAR(fecha_salida) as fecha, COUNT(fecha_salida) as total FROM `proceso` WHERE  date(fecha_salida) >= \"$fecha_ini\" AND date(fecha_salida) <= \"$fecha_fin\" AND pagado = 1 GROUP BY(YEAR(fecha_salida))");
			}
		}
		/* Recuperar y almacenar en conjunto los resultados de la consulta.*/
		while ($row = mysqli_fetch_array($fetch)) {
			$row_array['total'] = $row['total'];
			$row_array['fecha'] = $row['fecha'];
			array_push($return_arr, $row_array);
		}
	} else if ($_POST['paso'] == '2') {


		$fecha_ini = $_POST['fecha_init'];
		$fecha_fin = $_POST['fecha_fin'];

		// Iniciamos la condiciones si es dia mes o año
		if ($_POST['dato'] == '1') {
			//Medio de pago
			$fetch = mysqli_query($con, "SELECT COUNT(proceso.id_tipo_pago) as cantidad,
				tipo_pago.nombre as nombre
				FROM `proceso`
				INNER JOIN tipo_pago ON tipo_pago.id = proceso.id_tipo_pago
				WHERE date(proceso.fecha_salida) >= \"$fecha_ini\" AND date(proceso.fecha_salida) <= \"$fecha_fin\" GROUP BY proceso.id_tipo_pago");
		} else if ($_POST['dato'] == '2') {
			//Estado de pago
			$fetch = mysqli_query($con, "SELECT COUNT(proceso.estado) as cantidad,
				estado_de_pago.nombre as nombre
				FROM `proceso`
				INNER JOIN estado_de_pago ON estado_de_pago.medio_pago = proceso.estado 
				WHERE date(proceso.fecha_salida) >= \"$fecha_ini\" AND date(proceso.fecha_salida) <= \"$fecha_fin\"
				GROUP BY proceso.estado");
		} else if ($_POST['dato'] == '3') {
			//Tipo de Documento
			$fetch = mysqli_query($con, "SELECT COUNT(proceso.estado) as cantidad,
				estado_de_pago.nombre as nombre
				FROM `proceso`
				INNER JOIN estado_de_pago ON estado_de_pago.medio_pago = proceso.estado 
				WHERE date(proceso.fecha_salida) >= \"$fecha_ini\" AND date(proceso.fecha_salida) <= \"$fecha_fin\"
				GROUP BY proceso.estado");
		} else if ($_POST['dato'] == '4') {
			//Tipo de Comprobante
			$fetch = mysqli_query($con, "SELECT COUNT(proceso.estado) as cantidad,
				estado_de_pago.nombre as nombre
				FROM `proceso`
				INNER JOIN estado_de_pago ON estado_de_pago.medio_pago = proceso.estado 
				WHERE date(proceso.fecha_salida) >= \"$fecha_ini\" AND date(proceso.fecha_salida) <= \"$fecha_fin\"
				GROUP BY proceso.estado");
		}

		/* Recuperar y almacenar en conjunto los resultados de la consulta.*/
		while ($row = mysqli_fetch_array($fetch)) {
			$row_array['nombre'] = $row['nombre'];
			$row_array['cantidad'] = $row['cantidad'];
			array_push($return_arr, $row_array);
		}
	} else if ($_POST['paso'] == '3') {
		$fecha_ini = $_POST['fecha_init'];
		$fecha_fin = $_POST['fecha_fin'];
		$id_producto = $_POST['dato'];
		// Consultamos acerca de las cantidades y total de ganancias por fechas de los productos en general.
		if ($_POST['dato'] == '1') {
			$fetch = mysqli_query($con, "SELECT sum(nro_comprobante) as total,DAY(fecha_creada) as fecha, COUNT(nro_comprobante) as cantidad 
				FROM `venta` 
				WHERE date(fecha_creada) >= \"$fecha_ini\" and date(fecha_creada) <= \"$fecha_fin\" and tipo_operacion = 1 
				group by day(fecha_creada), month(fecha_creada), year(fecha_creada) 
				ORDER BY YEAR(fecha_creada),month(fecha_creada),day(fecha_creada)");
		}
		// Consultamos acerca de las cantidades y total de ganancias por fechas por producto en especifico.
		else if ($_POST['dato'] != '1') {
			$fetch = mysqli_query($con, "SELECT sum(venta.nro_comprobante) as total,DAY(venta.fecha_creada) as fecha, COUNT(venta.nro_comprobante) as cantidad, producto.nombre
			FROM `venta` 
			INNER JOIN proceso_venta ON proceso_venta.id_venta = venta.id
            INNER JOIN producto ON proceso_venta.id_producto = producto.id
			WHERE date(venta.fecha_creada) >= \"$fecha_ini\" and date(venta.fecha_creada) <= \"$fecha_fin\" and venta.tipo_operacion = 1
            AND producto.id = \"$id_producto\"
			group by day(venta.fecha_creada), month(venta.fecha_creada), year(venta.fecha_creada) 
			ORDER BY YEAR(venta.fecha_creada),month(venta.fecha_creada),day(venta.fecha_creada)");
		}
		while ($row = mysqli_fetch_array($fetch)) {
			$row_array['total'] = $row['total'];
			$row_array['fecha'] = $row['fecha'];
			$row_array['cantidad'] = $row['cantidad'];
			array_push($return_arr, $row_array);
		}
	} else if ($_POST['paso'] == '4') {

		$fecha_ini = $_POST['fecha_init'];
		$fecha_fin = $_POST['fecha_fin'];
		$producto1 = $_POST['producto1'];
		$producto2 = $_POST['producto2'];
		// Consultamos acerca de las cantidades y total de ganancias por fechas de los productos en general.
		$fetch = mysqli_query($con, "SELECT producto.id, producto.nombre as nombre, sum(venta.nro_comprobante) as ganancia,DAY(venta.fecha_creada) as fecha, sum(proceso_venta.cantidad) as 
			cantidad FROM `venta` INNER JOIN proceso_venta ON proceso_venta.id_venta = venta.id INNER JOIN producto ON producto.id = proceso_venta.id_producto 
			WHERE date(venta.fecha_creada) >= \"$fecha_ini\" and date(venta.fecha_creada) <= \"$fecha_fin\" and 
			producto.id = $producto1 OR producto.id = $producto2 GROUP BY producto.id,day(date(venta.fecha_creada)) ORDER BY fecha");

		while ($row = mysqli_fetch_array($fetch)) {
			$row_array['nombre'] = $row['nombre'];
			$row_array['ganancia'] = $row['ganancia'];
			$row_array['cantidad'] = $row['cantidad'];
			$row_array['id'] = $row['id'];
			$row_array['fecha'] = $row['fecha'];
			array_push($return_arr, $row_array);
		}
	} else if ($_POST['paso'] == '5') {

		$fecha_ini = $_POST['fecha_init'];
		$fecha_fin = $_POST['fecha_fin'];
		// Consultamos acerca de las cantidades y total de ganancias por fechas de los productos en general.
		$fetch = mysqli_query($con, "SELECT (SELECT sum(v.nro_comprobante)as monto_total_ventas FROM venta as v WHERE v.tipo_operacion = 1 and date(v.fecha_creada) >= \"$fecha_ini\"  and date(v.fecha_creada) <= \"$fecha_fin\") as ventas,
			(SELECT sum(p.precio)as recepcion FROM proceso as p WHERE p.estado = 1 and date(p.fecha_creada) >= \"$fecha_ini\"  and date(p.fecha_creada) <= \"$fecha_fin\") as recepcion,
			(SELECT sum(e.precio)as egreso FROM gasto as e Where date(e.fecha_creacion) >= \"$fecha_ini\" and date(e.fecha_creacion) <= \"$fecha_fin\") as egreso,
			(SELECT sum(c.nro_comprobante)as monto_total_ventas FROM venta as c WHERE c.tipo_operacion = 0 and date(c.fecha_creada) >= \"$fecha_ini\"  and date(c.fecha_creada) <= \"$fecha_fin\") as compra,
			sum(proceso.dinero_dejado) as dinero_dejado 
			FROM proceso WHERE date(proceso.fecha_creada) >= \"$fecha_ini\" and date(proceso.fecha_creada) <= \"$fecha_fin\"");

		while ($row = mysqli_fetch_array($fetch)) {
			$row_array['ventas'] = $row['ventas'];
			$row_array['recepcion'] = $row['recepcion'];
			$row_array['dinero_dejado'] = $row['dinero_dejado'];
			$row_array['egreso'] = $row['egreso'];
			$row_array['compra'] = $row['compra'];
			array_push($return_arr, $row_array);
		}
	}
}

/* Cierra la conexión. */
mysqli_close($con);

/* Codifica el resultado del array en JSON. */

echo json_encode($return_arr);
