<?php
// El siguiente codigo se encarga del mantenimiento de procesos de pago
// Relacion: VISTA PROCESOS DE PAGO

$action = $_POST["id_estado"];

//Agregar

if ($action == 'add') {

    if (count($_POST) > 0) {
        if (isset($_POST['tipo_id_cliente'])) {
            $habitacion = new TipoProceso();
            $habitacion->id_tipo_cliente = $_POST['tipo_id_cliente'];
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->valor = 0;
            $habitacion->add();
            print "<script>window.location='index.php?view=tipo_proceso';</script>";
        } else {
            $habitacion = new TipoProceso();
            $habitacion->id_tipo_cliente = $_POST['tipo_id_cliente'];
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->valor = 0;
            $habitacion->add();
            print "<script>window.location='index.php?view=tipo_proceso';</script>";
        }
    }
}

//Actualizar

else if ($action == 'update') {

    if (count($_POST) > 0) {
        if (isset($_POST['tipo_id_cliente'])) {
            $habitacion = TipoProceso::getById($_POST["id_documento"]);
            $habitacion->id_tipo_cliente = $_POST['tipo_id_cliente'];
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->valor = 0;
            $habitacion->update();
            print "<script>window.location='index.php?view=tipo_proceso';</script>";
        } else {
            $habitacion = TipoProceso::getById($_POST["id_documento"]);
            $habitacion->id_tipo_cliente = $_POST['tipo_id_cliente'];
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->valor = 0;
            $habitacion->update();
            print "<script>window.location='index.php?view=tipo_proceso';</script>";
        }
    }
}

//Eliminar

if ($action == 'delete') {

    $habi = TipoProceso::getById($_POST["id"]);
    $habi->del();


    Core::redir("./index.php?view=tipo_proceso");
}
