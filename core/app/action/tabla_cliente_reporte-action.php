<?php

$condicion = $_POST['dato'];

if ($condicion == 1) {


?>

    <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">
        <thead class="thead-light">
            <tr>
                <th style="font-size: 12px;" scope="col">Cliente</th>
                <th style="font-size: 12px;" scope="col">Tipo de Identificación</th>
                <th style="font-size: 12px;" scope="col">ID</th>
                <th style="font-size: 12px;" scope="col"># Hospedajes</th>
                <th style="font-size: 12px;" scope="col">Ganancia por Hospedaje</th>
                <th style="font-size: 12px;" scope="col">Ganancia por Productos</th>
                <th style="font-size: 12px;" scope="col">Email</th>
                <th style="font-size: 12px;" scope="col">Teléfono</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $Datos_usuario = PersonaData::get_datos_generales();
            foreach ($Datos_usuario as $Datos_usuarios) :
            ?>
                <tr>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->cliente ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->documento ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->id ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->catidad_hospedajes ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->ganancia_recepcion . " ₡"; ?></td>
                    <td style="font-size: 12px;">
                        <?php if ($Datos_usuarios->ganancia_ventas != null) {
                            echo $Datos_usuarios->ganancia_ventas . " ₡";
                        } else {
                            echo "0 ₡";
                        }
                        ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->email ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->telefono ?></td>


                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot class="hide-if-no-paging">
            <tr>
                <td colspan="9" class="text-center">
                    <ul class="pagination"></ul>
                </td>
            </tr>
        </tfoot>
    </table>

<?php



} else if ($condicion == 2) {

?>

    <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">
        <thead class="thead-light">
            <tr>
                <th style="font-size: 12px;" scope="col">Cliente</th>
                <th style="font-size: 12px;" scope="col">Tipo de Identificación</th>
                <th style="font-size: 12px;" scope="col">ID</th>
                <th style="font-size: 12px;" scope="col"># Hospedajes</th>
                <th style="font-size: 12px;" scope="col">Ganancia por Hospedaje</th>
                <th style="font-size: 12px;" scope="col">Ganancia por Productos</th>
                <th style="font-size: 12px;" scope="col">Email</th>
                <th style="font-size: 12px;" scope="col">Teléfono</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $Datos_usuario = PersonaData::get_datos_generales_externos();
            foreach ($Datos_usuario as $Datos_usuarios) :
            ?>
                <tr>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->cliente ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->documento ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->id ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->catidad_hospedajes ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->ganancia_recepcion . " ₡"; ?></td>
                    <td style="font-size: 12px;">
                        <?php if ($Datos_usuarios->ganancia_ventas != null) {
                            echo $Datos_usuarios->ganancia_ventas . " ₡";
                        } else {
                            echo "0 ₡";
                        }
                        ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->email ?></td>
                    <td style="font-size: 12px;"><?php echo $Datos_usuarios->telefono ?></td>


                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot class="hide-if-no-paging">
            <tr>
                <td colspan="9" class="text-center">
                    <ul class="pagination"></ul>
                </td>
            </tr>
        </tfoot>
    </table>

<?php






}
