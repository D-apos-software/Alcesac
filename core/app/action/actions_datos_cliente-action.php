<?php
// El siguiente codigo se encarga de mostrar el listado de los clientes en una lista al realizar la busqueda de los clientes existentes
// Relacion: VISTA VENTA


require __DIR__ . '/../../../core/app/action/action-functions-products.php'; ?>

<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<?php
$ID = $_POST['id_mostrar_cliente'];
$documento = $_POST['documento'];
$ID_TIPO = $_POST['tipo'];
$id_cliente = $_POST['id'];
if ($ID == "show_data") {
?>
    <?php $info = PersonaData::get_documento_data($documento); ?>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                <label>Nombres del cliente</label>
                <input type="text" class="form-control" value="<?php echo $info->nombre; ?>" required name="nombre_cliente">
            </div>
            <div class="col-sm-6">
                <label>Correo Electrónico</label>
                <input type="email" name="email" class="form-control" value="<?php echo $info->email ?>">
            </div>
            <input type="hidden" name="validacion_email" class="form-control" value="0">
            <!--<div class="col-sm-12"> 
                    <label>Teléfono</label>
                    <input type="text" name="telefono" class="form-control"  placeholder="Teléfono Celular">
                  </div>-->
        </div>
    </div>
    </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                <label>Tipo de Identificación</label>
                <?php $tipos_identificacion = TipoDocumentoData::getAll(); ?>
                <select class="form-control" required name="tipo_documento" id="tipo_documento">
                    <?php foreach ($tipos_identificacion as $id_t) : ?>
                        <option value="<?php echo $id_t->id ?>" <?php if ($info->tipo_documento != null & $info->tipo_documento == $id_t->id) {
                                                                    echo "selected";
                                                                } ?>><?php echo $id_t->nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-6">
                <label>Identificación</label>
                <input type="number" name="documento" class="form-control" required value="<?php echo $info->documento; ?>">
            </div>
            <input type="hidden" name="validacion_email" class="form-control" value="0">
            <!--<div class="col-sm-12"> 
                    <label>Teléfono</label>
                    <input type="text" name="telefono" class="form-control"  placeholder="Teléfono Celular">
                  </div>-->
        </div>
    </div>
    </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                <label>Proceso</label>
                <?php $tipo_clientes = TipoProceso::getTipoCliente(51); ?>
                <select class="form-control select2" required name="pagado">
                    <option value="">--- Selecciona ---</option>
                    <?php foreach ($tipo_clientes as $tipo_cliente) : ?>
                        <option><?php echo $tipo_cliente->nombre; ?></option>
                    <?php endforeach; ?>
                </select>
                <h1><?php echo $ID_TIPO; ?></h1>
            </div>
            <div class="col-sm-6">
                <label>Exonerado</label>
                <input type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="<?php echo $info->exonerado; ?>" style="height: 26px; margin:-2px;">
            </div>
        </div>




    <?php
} else if ($ID == "show_cliente") {
    $info = PersonaData::getById($id_cliente); ?>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label>Nombres del cliente</label>
                    <input type="text" class="form-control" value="<?php echo $info->nombre; ?>" required name="nombre_cliente">
                </div>
                <div class="col-sm-6">
                    <label>Correo Electrónico</label>
                    <input type="email" name="email" class="form-control" value="<?php echo $info->email ?>">
                </div>
                <input type="hidden" name="validacion_email" class="form-control" value="0">
                <!--<div class="col-sm-12"> 
                    <label>Teléfono</label>
                    <input type="text" name="telefono" class="form-control"  placeholder="Teléfono Celular">
                  </div>-->
            </div>
        </div>
    </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                <label>Tipo de Identificación</label>
                <?php $tipos_identificacion = TipoDocumentoData::getAll(); ?>
                <select class="form-control" required name="tipo_documento" id="tipo_documento">
                    <?php foreach ($tipos_identificacion as $id_t) : ?>
                        <option value="<?php echo $id_t->id ?>" <?php if ($info->tipo_documento != null & $info->tipo_documento == $id_t->id) {
                                                                    echo "selected";
                                                                } ?>><?php echo $id_t->nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-6">
                <label>Identificación</label>
                <input type="number" name="documento" class="form-control" required value="<?php echo $info->documento; ?>">
            </div>
            <input type="hidden" name="validacion_email" class="form-control" value="0">
            <input type="hidden" name="validacion_id" class="form-control" value="<?php echo $info->id; ?>">

            <!--<div class="col-sm-12"> 
                    <label>Teléfono</label>
                    <input type="text" name="telefono" class="form-control"  placeholder="Teléfono Celular">
                  </div>-->
        </div>
    </div>
    </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                <label>Proceso</label>
                <?php $tipo_clientes = TipoProceso::getTipoCliente($ID_TIPO); ?>
                <select class="form-control select2" required name="pagado">
                    <option value="">--- Selecciona ---</option>
                    <?php foreach ($tipo_clientes as $tipo_cliente) : ?>
                        <option value="<?php echo $tipo_cliente->id_tipo_cliente; ?>"><?php echo $tipo_cliente->nombre; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-6">
                <label>Exonerado</label>
                <input type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1" style="height: 26px; margin:-2px;">
            </div>
        </div>

    <?php
}
    ?>