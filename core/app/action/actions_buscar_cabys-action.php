<?php

// El siguiente codigo se encarga de actualizar listado de productos buscados 
// Relacion: VISTA PRODUCTOS / VISTA LISTA DE CODIGOS CABYS

if (isset($_POST['Lista']) and $_POST['Lista'] != "") {
    $clientes =  $_POST['Lista'];
} else {
    $clientes =  [];
} ?>
<?php


if ($_POST['estado'] == 1 or $_POST['estado'] == 3) {
    if (count($clientes) > 0) {  ?>

        <thead style="color: white; background-color: #827e7e;">
            <tr>
                <th>Código</th>
                <th>Descripcion</th>
                <th data-hide="phone">IMP</th>
                <th data-hide='phone, tablet'>Seleccionar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($clientes['cabys'] as $cliente) : ?>
                <tr>
                    <td><?php echo $cliente['codigo']; ?></td>
                    <td><?php echo $cliente['descripcion']; ?></td>
                    <td><?php echo $cliente['impuesto']; ?></td>
                    <td><?php if ($_POST['estado'] == 1) {
                        ?><a onclick="select(<?php echo $cliente['codigo']; ?>)" class="btn btn-success btn-xs">Añadir</a>
                            <?php
                        } else if ($_POST['estado'] == 3) {
                            if ($_POST['idModal'] == 0) {
                            ?><a onclick="select_vista_producto(<?php echo $cliente['codigo']; ?>)" class="btn btn-success btn-xs">Seleccionar</a>
                            <?php
                            } else if ($_POST['idModal'] > 0) {
                            ?><a onclick="select_vista_productoM(<?php echo $cliente['codigo']; ?>,<?php echo $_POST['idModal']; ?>)" class="btn btn-success btn-xs">Seleccionar</a>
                        <?php
                            }
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot class="hide-if-no-paging" style="left: -20px;">
            <tr>
                <td colspan="10" class="text-center">
                    <ul class="pagination"></ul>
                </td>
            </tr>
        </tfoot>


    <?php } else {
        echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
    };
} else if ($_POST['estado'] == 0 or $_POST['estado'] == 2) {

    if (count($clientes) > 0) {  ?>

        <thead style="color: white; background-color: #827e7e;">
            <tr>
                <th>Código</th>
                <th>Descripcion</th>
                <th data-hide="phone">IMP</th>
                <th data-hide='phone, tablet'>Seleccionar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($clientes as $cliente) : ?>
                <tr>
                    <td><?php echo $cliente['codigo']; ?></td>
                    <td><?php echo $cliente['descripcion']; ?></td>
                    <td><?php echo $cliente['impuesto']; ?></td>
                    <td><?php if ($_POST['estado'] == 0) {
                        ?><a onclick="select(<?php echo $cliente['codigo']; ?>)" class="btn btn-success btn-xs">Añadir</a>
                            <?php
                        } else if ($_POST['estado'] == 2) {
                            if ($_POST['idModal'] == 0) {
                            ?><a onclick="select_vista_producto(<?php echo $cliente['codigo']; ?>)" class="btn btn-success btn-xs">Seleccionar</a>
                            <?php
                            } else if ($_POST['idModal'] > 0){
                            ?><a onclick="select_vista_productoM(<?php echo $cliente['codigo']; ?>,<?php echo $_POST['idModal']; ?>)" class="btn btn-success btn-xs">Seleccionar</a>
                        <?php
                            }
                        }
                        ?>

                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot class="hide-if-no-paging" style="left: -20px;">
            <tr>
                <td colspan="7" class="text-center">
                    <ul class="pagination"></ul>
                </td>
            </tr>
        </tfoot>


<?php } else {
        echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
    };
}
?>



<script>
    function select(cabys) {
        console.log(cabys);
        var parametros = {
            "cabys": cabys,
            "descripcion": 'descripcion',
            "et": 0
        }
        $.ajax({
            type: "POST",
            url: 'index.php?action=action_auto_cabys',
            data: parametros,
            success: function(resp) {
                console.log(resp);
                if (resp == 0) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Exitosa',
                        showConfirmButton: false,
                        timer: 1700
                    }).then(function() {
                        window.location.reload(); // Recargar página
                    })

                } else if (resp == 2) {
                    Swal.fire({
                        icon: 'info',
                        title: 'El Código ya existe en el listado.',
                        showConfirmButton: false,
                        timer: 1700
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'No se añadio al listado',
                        showConfirmButton: false,
                        timer: 1700
                    })
                }

            }
        });
    }

    function select_vista_producto(cabys) {
        //alert(cabys)
        
        $('#myModalBusquedaCabys').modal('hide'); //end of line
        $("#myModal_1").modal(); //end of line
        document.getElementById("codigo").style.display = "none";
        document.getElementById("codigo_s").style.display = "flex";
        document.getElementById("codigo_s").value = cabys;
        document.getElementById("myModal_1").style.overflow = "auto";
    }


    function select_vista_productoM(cabys, IDModal) {
        $('#myModalBusquedaCabys').modal('hide'); //end of line
        $("#myModal"+IDModal).modal(); //end of line
        document.getElementById("codigoM"+IDModal).style.display = "none";
        //alert(cabys+"<br>"+IDModal);
        document.getElementById("myModal"+IDModal).style.overflow = "auto";
        document.getElementById("codigo_sM"+IDModal).style.display = "flex";
        document.getElementById("codigo_sM"+IDModal).value = cabys;
    }
</script>