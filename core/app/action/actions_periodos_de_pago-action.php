<head>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
</head>

<body>

    <?php
    // El siguiente codigo se encarga del mantenimiento de periodos de  pago
    // Relacion: VISTA PERIODO DE PAGO
    $action = $_POST["id_estado"];

    //Agregar

    if ($action == 'add') {
        try {
            if (count($_POST) > 0) {
                $habitacion = new PeriodosData();
                $habitacion->nombre = $_POST["nombre"];
                $habitacion->dias = $_POST["dias"];
                $habitacion->add();
            }
    ?>
            <meta http-equiv="refresh" content="2; url=index.php?view=periodos_pago">
            <script type="text/javascript">
                Swal.fire({
                    icon: 'success',
                    title: 'Satisfactorio',
                    showConfirmButton: false,
                    timer: 1700
                }).then(function() {
                    window.location = "index.php?view=periodos_pago";
                })
            </script>
        <?php
        } catch (Exception $e) {
        ?>
            <script>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error al añadir un nuevo periodo',
                    footer: '<a href="index.php">Volver al inicio?</a>'
                }).then(function() {
                    window.location = "index.php?view=periodos_pago";
                });
            </script>
            <meta http-equiv="refresh" content="3; url=index.php?view=periodos_pago">
        <?php
        }
    }

    //Actualizar

    else if ($action == 'update') {
        try {
            if (count($_POST) > 0) {

                $documento = PeriodosData::getById($_POST["id_documento"]);
                $documento->nombre = $_POST["nombre"];
                $documento->dias = $_POST["dias"];
                $documento->update();
            }
        ?>
            <meta http-equiv="refresh" content="2; url=index.php?view=periodos_pago">
            <script type="text/javascript">
                Swal.fire({
                    icon: 'success',
                    title: 'Satisfactorio',
                    showConfirmButton: false,
                    timer: 1700
                }).then(function() {
                    window.location = "index.php?view=periodos_pago";
                })
            </script>
        <?php
        } catch (Exception $e) {
        ?>
            <script>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error al añadir un nuevo periodo',
                    footer: '<a href="index.php">Volver al inicio?</a>'
                }).then(function() {
                    window.location = "index.php?view=periodos_pago";
                });
            </script>
            <meta http-equiv="refresh" content="3; url=index.php?view=periodos_pago">
    <?php
        }
    }

    //Eliminar

    else if ($action == 'delete') {

        $habi = PeriodosData::getById($_POST["id"]);
        $habi->del();

        Core::redir("./index.php?view=periodos_pago");
    }

    ?>
</body>