<?php
// El siguiente codigo se encarga del mantenimiento de medio de pago
// Relacion: VISTA MEDIOS DE PAGO
$action = $_POST["id_estado"];

//Agregar

if ($action == 'add') {
    $habitacion = new TipoPago();
    $habitacion->nombre = $_POST["nombre"];
    $habitacion->valor_hacienda = $_POST["valor_hacienda"];
    if (isset($_POST['valor'])) {
        //echo "valor";
        $habitacion->valor = 1;
    } else {
        $habitacion->valor = 0;
    }
    if (isset($_POST['banco'])) {
        //echo "banco";
        $habitacion->banco = 1;
    } else {
        $habitacion->banco = 0;
    }
    if (isset($_POST['numero_ap'])) {
        //echo "numero_ap";
        $habitacion->numero_aprobacion = 1;
    } else {
        $habitacion->numero_aprobacion = 0;
    }
    if (isset($_POST['numero_tar'])) {
        //echo "numerotar";
        $habitacion->numero_tarjeta = 1;
    } else {
        $habitacion->numero_tarjeta = 0;
    }

    $habitacion->add();
    print "<script>window.location='index.php?view=tipo_de_pago';</script>";
}


//Actualizar

else if ($action == 'update') {

    /*if (isset($_POST['valor'])){
            $documento = TipoPago::getById($_POST["id_documento"]);
            $documento->nombre = $_POST["nombre"];
            $documento->valor = $_POST["campo"];
            $documento->update();
            print "<script>window.location='index.php?view=tipo_de_pago';</script>";

        }*/
    $documento = TipoPago::getById($_POST["id_documento"]);
    $documento->nombre = $_POST["nombre"];
    $documento->valor_hacienda = $_POST["valor_hacienda"];

    if (isset($_POST['valor'])) {
        $documento->valor = $_POST["valor"];
    } else {
        $documento->valor = 0;
    }
    if (isset($_POST['banco'])) {
        $documento->banco = $_POST["banco"];
    } else {
        $documento->banco = 0;
    }
    if (isset($_POST['numero_ap'])) {
        $documento->numero_aprobacion = $_POST["numero_ap"];
    } else {
        $documento->numero_aprobacion = 0;
    }
    if (isset($_POST['numero_tar'])) {
        $documento->numero_tarjeta = $_POST["numero_tar"];
    } else {
        $documento->numero_tarjeta = 0;
    }

    $documento->update();
    print "<script>window.location='index.php?view=tipo_de_pago';</script>";
}


//Eliminar

else if ($action == 'delete') {
    $habi = TipoPago::getById($_POST["id"]);
    $habi->del();

    Core::redir("./index.php?view=tipo_de_pago");
}


//Estado

else if ($action == 'actualizar') {

    $habi = TipoPago::getById($_POST["id"]);
    if ($habi->estado == 1) {
        echo $habi->estado;
        $habi->estado = 0;
    } else {
        echo $habi->estado;
        $habi->estado = 1;
    }
    $habi->update_estado();
}
