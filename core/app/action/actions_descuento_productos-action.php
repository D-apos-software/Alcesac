<?php 
// MANTENIMIENTO DE sala_descuentos
// Segun el tipo de descuentos, realizamos la siguiente accion
$action = $_POST["id_estado"];
if ($action == 'add'){
// AGREGAR
if( count($_POST) > 0 ){

        $habitacion = new Descuento_Producto();
        $habitacion->nombre = $_POST["nombre"];
        $habitacion->valor = $_POST["valor"];
        $as=$habitacion->add();
        print "<script>window.location='index.php?view=sala_descuento_productos';</script>";
}
}
//ACTUALIZAR
else if ($action == 'update'){
if(count($_POST)>0){
        $documento = Descuento_Producto::getById($_POST["id_documento"]);
        $documento->nombre = $_POST["nombre"];
        $documento->valor = $_POST["valor"];
        $documento->estado = $_POST["estado"];
        $documento->update();
        print "<script>window.location='index.php?view=sala_descuento_productos';</script>";
}
}
//ELIMINAR
if ($action == 'delete'){
        $habi = Descuento_Producto::getById($_POST["id"]);
        $habi->del();
        Core::redir("./index.php?view=sala_descuento_productos");
}
//Desactivar
if ($action == 'desactivar'){
        $documento = Descuento_Producto::getById($_POST["id"]);
        $documento->estado = 0; 
        $documento->update_inactive();
        print "<script>window.location='index.php?view=sala_descuento_productos';</script>";
}
?>