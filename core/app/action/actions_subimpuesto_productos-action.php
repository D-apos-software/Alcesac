<?php
// El siguiente codigo se encarga del mantenimiento de subimpuesto de productos
// Relacion: VISTA PRODUCTOS

$action = $_POST["impuesto"];
if ($action == 'add') {
        // AGREGAR
        if (count($_POST) > 0) {
                $habitacion = new Tipo_subimpuesto_producto();
                $habitacion->nombre = $_POST["nombre"];
                $habitacion->valor = $_POST["valor"];
                $as = $habitacion->add();
                print "<script>window.location='index.php?view=sala_subimpuestos_productos';</script>";
        }
}
//ACTUALIZAR
else if ($action == 'update') {
        if (count($_POST) > 0) {
                $documento = Tipo_subimpuesto_producto::getById($_POST["id_documento"]);
                $documento->nombre = $_POST["nombre"];
                $documento->valor = $_POST["valor"];
                $documento->estado = $_POST["estado"];
                $documento->update();
                print "<script>window.location='index.php?view=sala_subimpuestos_productos';</script>";
        }
}
//ELIMINAR  
if ($action == 'delete') {
        $habi = Tipo_subimpuesto_producto::getById($_POST["id"]);
        $habi->del();
        Core::redir("./index.php?view=sala_subimpuestos_productos");
}
//Desactivar
if ($action == 'desactivar') {
        $documento = Tipo_subimpuesto_producto::getById($_POST["id"]);
        $documento->estado = 0;
        $documento->update_inactive();
        print "<script>window.location='index.php?view=sala_subimpuestos_productos';</script>";
}
