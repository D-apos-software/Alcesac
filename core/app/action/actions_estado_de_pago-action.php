<?php 
// El siguiente codigo se encarga del mantenimiento de estado de pago
// Relacion: VISTA ESTADO DE PAGO

    $action = $_POST["id_estado"];
    
    
    if ($action == 'add'){
    //AGREGAR
    if( count($_POST) > 0 ){
        if (isset($_POST['campo'])){
            $habitacion = new Estado_de_pago();
            $habitacion->medio_pago = $_POST['campo']; 
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->add();
            print "<script>window.location='index.php?view=estado_de_pago';</script>";

        }else{
            $habitacion = new Estado_de_pago();
            $habitacion->medio_pago = 0; 
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->add();
            print "<script>window.location='index.php?view=estado_de_pago';</script>";
        }

    }
    }


    //ACTUALIZAR
    else if ($action == 'update'){
    if(count($_POST)>0){
        if (isset($_POST['campo'])){
            $documento = Estado_de_pago::getById($_POST["id_documento"]);
            $documento->nombre = $_POST["nombre"];
            $documento->medio_pago = $_POST["campo"];
            $documento->update();
            print "<script>window.location='index.php?view=estado_de_pago';</script>";

        }else{
            $documento = Estado_de_pago::getById($_POST["id_documento"]);
            $documento->nombre = $_POST["nombre"];
            $documento->medio_pago = 0;
            $documento->update();
            print "<script>window.location='index.php?view=estado_de_pago';</script>";
        }



    }
    }

    //ELIMINAR

    if ($action == 'delete'){
            $habi = Estado_de_pago::getById($_POST["id"]);
            $habi->del();
            Core::redir("./index.php?view=estado_de_pago");
    }
?>