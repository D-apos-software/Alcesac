<?php
// El siguiente codigo se encarga del mantenimiento de categoria de ventas
// Relacion: VISTA VENTAS

$action = $_POST["id_estado"];

//Add
if ($action == 'add') {

    $bodega = new Tipo_categoria_venta();
    $bodega->codigo = $_POST["codigo"];
    $bodega->nombre = $_POST["nombre"];
    $bodega->estado = 1;
    $bodega->add();
    print "<script>window.location='index.php?view=sala_tipo_categoria_ventas';</script>";
}

//Update
else if ($action == 'update') {
    print('<br><br><br><br>');
    $bodega = Tipo_categoria_venta::getById($_POST["id_documento"]);
    $bodega->codigo = $_POST["codigo"];
    $bodega->nombre = $_POST["nombre"];
    $bodega->estado = 1;
    $bodega->update();
    print "<script>window.location='index.php?view=sala_tipo_categoria_ventas';</script>";
}


//Desactivar
else if ($action == 'desactivar') {
    print('<br><br><br><br>');
    $bodega = Tipo_categoria_venta::getById($_POST["id"]);
    $bodega->estado = 0;
    $bodega->update_estado();
    print "<script>window.location='index.php?view=sala_tipo_categoria_ventas';</script>";
}
//Delete
if ($action == 'delete') {

    $bodega = Tipo_categoria_venta::getById($_POST['id']);
    $bodega->del();

    //Core::redir("./index.php?view=m_documento");
}
