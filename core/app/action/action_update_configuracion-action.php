<?php
// El siguiente codigo se encarga de actualizar y registrar los datos de la vista configuracion
// Relacion: VISTA CONFIGURACIONES / API HACIENDA

// Todos los datos 
if ($_POST['estado'] == 0) {
	$empresa = ConfiguracionData::getById($_POST["id_config"]);
	$empresa->token = $_POST["token"];
	$empresa->llave_criptografica = 'file.p12';
	$empresa->usuario_hacienda = $_POST["usuario_hacienda"];
	$empresa->contrasena_hacienda = $_POST["contrasena_hacienda"];
	$empresa->pin = $_POST["pin"];
	$empresa->cod_moneda = $_POST["cod_moneda"];
	$empresa->nombre_hac = $_POST["nombre_hac"];
	$empresa->numero_id = $_POST["numero_id"];
	$empresa->provincia = $_POST["provincia"];
	$empresa->canton = $_POST["canton"];
	$empresa->distrito = $_POST["distrito"];
	$empresa->barrio = $_POST["barrio"];
	$empresa->cod_pais_tel_hc = $_POST["cod_pais_tel_hc"];
	$empresa->tel_hac = $_POST["tel_hac"];
	$empresa->tipo_id = $_POST["tipoCed"];
	$empresa->update_token();
	echo 'exitoso';

} else if ($_POST['estado'] == 1) {
	$empresa = ConfiguracionData::getById($_POST["id_config"]);
	$empresa->llave_criptografica = 'file.p12';
	$empresa->usuario_hacienda = $_POST["usuario_hacienda"];
	$empresa->contrasena_hacienda = $_POST["contrasena_hacienda"];
	$empresa->pin = $_POST["pin"];
	$empresa->cod_moneda = $_POST["cod_moneda"];
	$empresa->nombre_hac = $_POST["nombre_hac"];
	$empresa->numero_id = $_POST["numero_id"];
	$empresa->provincia = $_POST["provincia"];
	$empresa->canton = $_POST["canton"];
	$empresa->distrito = $_POST["distrito"];
	$empresa->barrio = $_POST["barrio"];
	$empresa->cod_pais_tel_hc = $_POST["cod_pais_tel_hc"];
	$empresa->tel_hac = $_POST["tel_hac"];
	$empresa->tipo_id = $_POST["tipoCed"];
	$empresa->update_data_hacienda();
	echo 'exitoso';
}


function upload_file($empresa)
{
	// Elimianr el archivo anterior
	if (file_exists('hacienda/llave_criptografica/' . $empresa->llave_criptografica)) {
		unlink('hacienda/llave_criptografica/' . $empresa->llave_criptografica);
	}
	// Subir 
	$file = new Upload($_FILES["llave_criptografica"]);
	if ($file->uploaded) {
		$file->Process("hacienda/llave_criptografica/");
		// Renombrar
		rename("hacienda/llave_criptografica/" . $file->file_dst_name, "hacienda/llave_criptografica/file.p12");
	}
}
