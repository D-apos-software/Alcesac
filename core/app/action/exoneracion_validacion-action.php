<?php
// El siguiente codigo se encarga de mostrar opcion de impuestos en la vista de mantenimiento de productos
// Relacion: VISTA PRODUCTOS
if ($_POST['id'] == '1') {

?>

    <head>
        <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
        <link rel="stylesheet" href="css/vista_productos/style.css">
    </head>
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"> Impuesto &nbsp;</span>
            <div class="dropdown" data-control="checkbox-dropdown">
                <label class="form-control dropdown-label">Select</label>
                <div class="dropdown-list">
                    <?php $lista = array(); ?>
                    <?php $proveedores = Tipo_impuesto_productos::get_active(); ?>
                    <?php foreach ($proveedores as $proveedor) : ?>
                        <label class="dropdown-option">
                            <input type="checkbox" name="dropdown-group[]" value="<?php echo $proveedor->id; ?>" />
                            <?php
                            echo $proveedor->nombre;
                            array_push($lista, $proveedor->id);
                            ?>
                        </label>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
        <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/footable/footable.all.min.js"></script>
        <script src="js/vista_productos/function.js"></script>
    <?php

}


    ?>