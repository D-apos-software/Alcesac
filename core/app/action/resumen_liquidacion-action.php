<?php 
$ID = $_POST['id'];

if($ID == 1){
?>
<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/tablas/tablas.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>       
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
 <?php 
    date_default_timezone_set('America/Costa_Rica');
    $hoy = date("Y-m-d");
    $hora = date("H:i:s"); 
    $anio= date("Y");   
    $inicio=date("Y-01-01"); 
    $fin=date("Y-12-31") ;    
    $primer_dia = date("01-m-d");
    $fecha_inicio = $_POST['fecha_init'];
    $fecha_final = $_POST['fecha_fin'];
$output = '';  
$output .= '  
<div id="table_padre">
<table>
<div class="row">
            <div class="col-md-12">
              <div class="card card-plain">
                <div style="background-color:#0a90cd;" class="card-header card-header-primary">
                  <p class="card-category">Historial de Aperturas</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table  id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom"  class="table table-hover">
                      <thead class="">
                        <tr>
                          <th style="font-size: 12px;" scope="col">Usuario</th>
                          <th style="font-size: 12px;" scope="col">Caja</th>                    
                          <th style="font-size: 12px;" scope="col">Monto aperturado</th>
                          <th style="font-size: 12px;" scope="col">Monto de Cierre</th>
                          <th style="font-size: 12px;" scope="col">Fecha Apertura</th>
                          <th style="font-size: 12px;" scope="col">Fecha Cierre</th>
                          <th style="font-size: 12px;" scope="col">Historial</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>';
$datos = CajasAperturas::resumen_liquidaciones_fechas($fecha_inicio,$fecha_final);
foreach($datos as $Datos_usuarios):
$output .= '
<td>'.$Datos_usuarios->usuario .'</td>
<td>'.$Datos_usuarios->numero_caja.' </td>
<td>'. $Datos_usuarios->apertura.'</td>
<td>'. $Datos_usuarios->cierre.'</td>
<td>'. $Datos_usuarios->hora_inicio.'</td>
<td>'.$Datos_usuarios->hora_cierre.'</td>
<td>
      <a href=""  data-toggle="modal" data-target="#myModal_edit'.$Datos_usuarios->id.'"  class="btn btn-primary btn-block2"><i class="glyphicon glyphicon-edit"></i> Mostrar Historial</a>
</td>
</tr>









<div class="modal fade bs-example-modal-xm" id="myModal_edit'.$Datos_usuarios->id.'" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-warning">
          <div class="modal-dialog">
            <div class="modal-content">
            <form class="form-horizontal" method="post" id="addproduct" action="index.php?action=actions_documento" role="form">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span></span>Resumen de liquidación</h4>
              </div>
               '; $datos_historial=CajasAperturas::resumen_liquidaciones_historial_5($Datos_usuarios->id);  
               $output .= ' <div class="tile-body">
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Fecha:</label> <label style="font-size: 18px;margin-left:240px; color:blue;" for="">'.$datos_historial->fecha .'</label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Hora de apertura:</label><label style="font-size: 18px;margin-left:157px; color:blue;" for="">
                 
                                  '. $datos_historial->hora_inicial.'

                  </label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Apertura caja:</label><label style="font-size: 18px;margin-left:185px; color:blue;" for="">
                 
                                  '.'₡ '.$datos_historial->monto_apertura.'

                 </label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Alquiler habitación: </label><label style="font-size: 18px;margin-left:145px; color:blue;" for="">
                ';
                  $historial = intval($datos_historial->habitacion_total); 
                  $cobro_otro = intval($datos_historial->cobro_ext_diferente_proceso); 
                  $total  = $historial - $cobro_otro; 
                  '₡ '. $total;  if($datos_historial->cobro_ext > 0){ "<br>Cobro extra: ".$datos_historial->cobro_ext; }
                  $output .= '</label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Cobro extra de habitación: </label><label style="font-size: 18px;margin-left:83px; color:blue;" for="">
                 '.'₡ '.$datos_historial->cobro_ext_diferente_proceso2.'
                </label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Ventas: </label><label style="font-size: 18px;margin-left:237px; color:blue;" for="">
               
                '. '₡ '.$datos_historial->venta.'

               </label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Compras: </label><label style="font-size: 18px;margin-left:218px; color:blue;" for="">
                
                '.'₡ '.$datos_historial->compra.'
             
                </label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Egresos: </label><label style="font-size: 18px;margin-left:225px; color:blue;" for="">
                
                '.'₡ '.$datos_historial->gasto.'
                </label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Hora de cierre:</label> <label style="font-size: 18px;margin-left:172px; color:blue;" for=""> 
                 
                  '. $datos_historial->hora_cierre.'
                </label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Monto de Cierre:</label> <label style="font-size: 18px;margin-left:160px; color:green;" for="">
                  
                  '.'₡ '.$datos_historial->monto_cierre.'

                </label>
              </div>

              <div> 
              <div class="modal-footer">
                <button style="font-size: 18px; margin-left:px; margin-bottom:-20px;" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
              </div>
            </form>
           
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
';

endforeach;
$output .= '
<tr>
<td colspan="7" class="text-center">
<ul class="pagination"></ul>
</td>
</tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
</table>
</div>';  
           
 
      echo $output;  
    
?>



<?php
}else if($ID == 2){
?>
  <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">

  <select name="caja_abierta" id="caja_abierta" style="font-size: 14px; width: 20%; margin-top: -65.5px;" class="form-control input-sm w-sm mb-12 inline-block" onchange="Cargar_datos_en_proceso(this.value);">
  <?php 
  $cajas_abiertas = CajasAperturas::getAllAbierto_aperturas();
  foreach($cajas_abiertas as $caja):?>
  <option value="<?php echo $caja->id?>"><?php echo "Caja #".$caja->numero." en proceso" ?></option>
  <?php endforeach;?>
  </select>

<?php 
     date_default_timezone_set('America/Costa_Rica');
     $hoy = date("Y-m-d");
     $hora = date("H:i:s");
                    
?>

<style type="text/css">
  table.dataTable thead .sorting:after {
    opacity: 0.0;
    content: "\e150";
}

table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after {
    opacity: 0.0;
}
</style>


<body id="minovate" class="appWrapper sidebar-sm-forced">
<div name="contenedor_padre" id="contenedor_padre" style="margin-top:-125px;margin-left:-80px;">
<div class="row" style="margin-top:-50px; margin-left:359px">
<div  name="contenedor_hijo" id="contenedor_hijo">

<style type="text/css">
  
  .hh:hover{
    background-color: white;
  }
  .small-box-footer {
    position: relative;
    text-align: center;
    padding: 0px 0;
    color: #fff;
    color: rgba(255,255,255,0.8);
    display: block;
    z-index: 10;
    background: rgba(0,0,0,0.1);
    text-decoration: none;
}
.nav-tabs-custom>.nav-tabs>li>a {
    color: #3c8dbc;
    font-weight: bold;
    border-radius: 0 !important;
}
.nav-tabs-custom>.nav-tabs>li.active {
    border-top-color: #00a65a;
}
.h5, h5 {
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>

 

<br>

<?php $cajas = CajasAperturas::getAllAbierto(); 
      $object = (array) $cajas;
      $object = count($object); 
      if( $object > 0 ){
           $id_caja=$cajas->id;
      }else{
          $id_caja = 0;
      } 


if($id_caja!=0){
?>

<?php $caja_abierta=CajasAperturas::getById($id_caja); ?>

<section style="margin-left:70px; margin-top:20px;" class="tile tile-simple col-md-4">
        <div class="tile-widget dvd dvd-btm" style="text-align: center;">
            <h5 class="box-title"><b>REPORTE DE CAJA</b></h5>
             <!-- /.box-tools -->
        </div>
      <!-- /.box-header -->


            <!-- /.box-header -->
            <form method="post"  action="index.php?view=agregar_caja" id="addcaja">
              <div class="tile-body p-0" style="text-align: left;">

                

   
                <!-- INGRESOS -->
                                              <?php $montos_sin_cerrar = ProcesoData::getIngresoCaja($id_caja);
                                                    $total_sin_cerrar=0;
                                                    if(count($montos_sin_cerrar)>0){

                                                      foreach($montos_sin_cerrar as $monto_sin_cerrar):
                                                        $total_sin_cerrar=(($monto_sin_cerrar->precio*$monto_sin_cerrar->cant_noche)+$monto_sin_cerrar->total)+$total_sin_cerrar;
                                                      endforeach;

                                                    }
                                              ?> 


                                              <?php  
                                              if($id_caja!=0){ 
                                              $reporproducts = ProcesoVentaData::getIngresoCaja($id_caja);
                                              $subtotal3=0;
                                              if(count($reporproducts)>0){ ?>
                                                 <?php foreach($reporproducts as $reporproduct):?>
                                                      <?php $subtotal1=$reporproduct->cantidad*$reporproduct->precio; ?>
                                                  <?php $subtotal3=$subtotal1+$subtotal3; ?>
                                                  <?php endforeach; ?>
                                              <?php }else{$subtotal3=0;} ?>
                                              <?php }else{$subtotal3=0;} ?> 

                                              <!-- FIN INGRESOS -->




                                              <!-- EGRESOS -->

                                              <?php $montos_sin_cerrar_egresos = GastoData::getEgresoCaja($id_caja);
                                                    $total_sin_cerrar_egreso=0;
                                                    if(count($montos_sin_cerrar_egresos)>0){

                                                      foreach($montos_sin_cerrar_egresos as $montos_sin_cerrar_egreso):
                                                        $total_sin_cerrar_egreso=$montos_sin_cerrar_egreso->precio+$total_sin_cerrar_egreso;
                                                      endforeach;

                                                    } 
                                              ?>
 
                                              <?php $montos_sin_cerrar_sueldos = ProcesoSueldoData::getSueldoCaja($id_caja);
                                                    $total_sin_cerrar_sueldos=0;
                                                    if(count($montos_sin_cerrar_sueldos)>0){ 

                                                      foreach($montos_sin_cerrar_sueldos as $montos_sin_cerrar_sueldo):
                                                        $total_sin_cerrar_sueldos=$montos_sin_cerrar_sueldo->monto+$total_sin_cerrar_sueldos;
                                                      endforeach;

                                                    } 
                                              ?>


                                              <?php  
                                              if($id_caja!=0){ 
                                              $reporproducts_es = ProcesoVentaData::getEgresoCaja($id_caja);
                                              $subtotal4=0;
                                              if(count($reporproducts_es)>0){ ?>
                                                 <?php foreach($reporproducts_es as $reporproduct_e):?>
                                                      <?php $subtotal1=$reporproduct_e->cantidad*$reporproduct_e->precio; ?>
                                                  <?php $subtotal4=$subtotal1+$subtotal4; ?>
                                                  <?php endforeach; ?>
                                              <?php }else{$subtotal4=0;} ?>
                                              <?php }else{$subtotal4=0;} ?>


                                              
                                            <!-- Total egreso -->
                                            <?php $total_egreso=$total_sin_cerrar_egreso+$total_sin_cerrar_sueldos+$subtotal4; ?>
                                            <!-- Fin Total egreso -->

                                            <!-- Total ingreso -->
                                            <?php $total_ingreso=$total_sin_cerrar+$subtotal3; ?>
                                            <!-- Fin Total ingreso -->
                                            <?php ?>
                <table class="table mb-0">
                 
                  <tr>
                      <td><h5>FECHA:</h5></td>
                      <td><h5 class="control-label text-red"><?php echo $hoy; ?></h5></td>
                  </tr>
                   <tr>
                      <td><h5><br>Apertura caja:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$   <?php echo number_format($caja_abierta->monto_apertura,2,'.',','); ?></h5></td>
                  </tr>
                  <tr>
                      <td><h5><br>Alquiler habitación:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$   <?php echo number_format($total_sin_cerrar,2,'.',','); ?></h5></td>
                  </tr>
                  <tr>
                      <td><h5><br>Servicio habitación:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$   <?php echo number_format($subtotal3,2,'.',','); ?></h5></td>
                  </tr>
                  <tr>
                      <td><h5><br>Egresos:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$  <?php echo number_format($total_egreso,2,'.',','); ?></h5></td>
                  </tr>
                  <tr style="border-top: 2px solid #00a65a">
                      <td><h5><br>TOTAL:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$   <?php echo number_format($caja_abierta->monto_apertura+$total_ingreso-$total_egreso,2,'.',','); ?></h5></td>
                  </tr>
    
                </table>
 
              </div>

             

             

          </form>


</section>
<section>
<div class="row">

  <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->

          <div style="margin-left:80px" class="nav-tabs-custom">
            <ul class="nav nav-tabs" style="background-color: #d2d6de;">
              <li class="active"><a href="#tab_1" data-toggle="tab">Tabla alquiler</a></li>
              <li><a href="#tab_2" data-toggle="tab">Tabla servicio a la habitación</a></li>
              <li class="pull-right text-red"><a href="reporte/pdf/documentos/reporte_diario_caja.php" target="_blank" class="text-muted"><i class="fa fa-print"></i> IMPRIMIR</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <?php $reportediarios = ProcesoData::getIngresoCaja($id_caja);
                if(count($reportediarios)>0){
                  // si hay usuarios
                  ?>
                  <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">

                  <thead style="color: black; background-color: #d2d6de;">
                        <th>Nº</th> 
                        <th>Habitación</th>
                        <th>Precio</th>
                        <th>Cant</th>
                        <th>Total</th>
                        <th>Hora ingreso</th>
                        <th>Hora salida</th> 
                  </thead> 
                   <?php $numero=0;?>
                   <?php $total=0;?>
                   <?php foreach($reportediarios as $reportediario):?>
                   <?php $numero=$numero+1;?>
                      <tr>
                        <td><?php echo $numero; ?></td>
                        <td><?php echo $reportediario->getHabitacion()->nombre; ?></td>
                        <td><b>$    <?php echo number_format($reportediario->precio,2,'.',','); ?></b></td>
                        <td><b>$    <?php echo number_format($reportediario->cant_noche,2,'.',','); ?></b></td>
                        <?php $subtotal= ($reportediario->precio*$reportediario->cant_noche)+$reportediario->total ?>
                        <td>$    <?php echo number_format($subtotal,2,'.',','); ?></td>
                        <td><?php echo date($reportediario->fecha_entrada); ?></td>
                        <td><?php echo date($reportediario->fecha_salida); ?></td>
                      </tr> 
                            <?php $total=$subtotal+$total; ?>
                    <?php endforeach; ?>

                     <tfoot style="color: black; background-color: #e3e4e6;">
                        <th colspan="4"><p class="pull-right">Total</p></th>
                        <th><b>$    <?php echo number_format($total,2,'.',','); ?> </b></th> 
                        <th></th>
                        <th></th>
                    </tfoot>

                  </table>

               <?php }else{ 
            echo"<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";

                };
                ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <?php $reporproducts = ProcesoVentaData::getIngresoCaja($id_caja);
                if(count($reporproducts)>0){
                  // si hay usuarios 
                  ?>
                  <table  id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">

                  <thead style="color: black; background-color: #d2d6de;">
                        <th>Nº</th> 
                        <th>Habitación</th>
                        <th>Precio tarifa</th>
                        <th>Cantidad</th> 
                        <th>Precio unitario</th>
                        <th>Total</th>
                        <th>Hora </th> 
                  </thead>
                   <?php $numero=0;?>
                   <?php $subtotal2=0;?>
                   <?php foreach($reporproducts as $reporproduct):?>
                   <?php $numero=$numero+1;?>
                   <?php if($reporproduct->fecha_creada!=NULL){ ?>
                      <tr>
                        <td><?php echo $numero; ?></td>
                        <td><?php echo $reporproduct->getProceso()->getHabitacion()->nombre; ?></td>
                        <td><?php echo $reporproduct->getProducto()->nombre; ?></td>
                        <td><b><?php echo $reporproduct->cantidad; ?></b></td>
                        <td><b>$   <?php echo number_format($reporproduct->precio,2,'.',','); ?></b></td>
                        <?php $subtotal1=$reporproduct->cantidad*$reporproduct->precio; ?>
                        <td><b>$   <?php echo number_format($subtotal1,2,'.',','); ?></b></td>
                        <td><?php echo date($reporproduct->fecha_creada); ?></td>
                      </tr> 
                    <?php $subtotal2=$subtotal1+$subtotal2; ?>
                    <?php }; ?>
                    <?php endforeach; ?>

                    <tfoot style="color: black; background-color: #e3e4e6;">
                        <th colspan="5"><p class="pull-right">Total</p></th>
                        <th><b>$    <?php echo number_format($subtotal2,2,'.',','); ?></b> </th> 
                        <th></th>
                    </tfoot>

                  </table>

               <?php }else{ 
            echo"<h4 class='alert alert-success'>NO HAY NINGUNA VENTA HOY </h4>";

                };
                ?>
              </div>
              <!-- /.tab-pane -->
              
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>

    </div>
</div>

</section>


<?php }else{
  echo "<p class='danger' style='margin-top:100px; margin-left:200px;'>No tiene ninguna caja abierta</p>";
} ?>
       <script src="assets/js/vendor/bootstrap/bootstrap.min.js"></script>
        <script src="assets/js/vendor/jRespond/jRespond.min.js"></script>
        <script src="assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>
        <script src="assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
        <script src="assets/js/vendor/screenfull/screenfull.min.js"></script>
        <script src="assets/js/vendor/footable/footable.all.min.js"></script>
        <!--<script src="assets/js/main.js"></script>-->
        <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
        <script>
            $(window).load(function(){

                $('.footable').footable();

            });
</script>  
</div> 
</div> 
</div>
<?php
}

// ----------------------------------------------------------------- Finaliza caso #2 -------------------------------------------------------------------------------------

?>
<?php 
if($_POST['id'] == 3){
 $id_caja = $_POST['id_caja'];
 $output = '';                 
 $output .= '  
 <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">';

     date_default_timezone_set('America/Costa_Rica');
     $hoy = date("Y-m-d");
     $hora = date("H:i:s");
                    

$output .= '
<style type="text/css">
  table.dataTable thead .sorting:after {
    opacity: 0.0;
    content: "\e150";
}

table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after {
    opacity: 0.0;
}
</style>


<body id="minovate" class="appWrapper sidebar-sm-forced">
<div name="contenedor_padre" id="contenedor_padre" style="margin-top:-125px;margin-left:-80px;" class="row">
<div style="margin-left:450px;" name="contenedor_hijo" id="contenedor_hijo">

<style type="text/css">
  
  .hh:hover{
    background-color: white;
  }
  .small-box-footer {
    position: relative;
    text-align: center;
    padding: 0px 0;
    color: #fff;
    color: rgba(255,255,255,0.8);
    display: block;
    z-index: 10;
    background: rgba(0,0,0,0.1);
    text-decoration: none;
}
.nav-tabs-custom>.nav-tabs>li>a {
    color: #3c8dbc;
    font-weight: bold;
    border-radius: 0 !important;
}
.nav-tabs-custom>.nav-tabs>li.active {
    border-top-color: #00a65a;
}
.h5, h5 {
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>

 

<br>
';


if($id_caja!=0){


 $caja_abierta=CajasAperturas::getById($id_caja);

$output .= '<section style="margin-left:70px; margin-top:20px;" class="tile tile-simple col-md-4">
        <div class="tile-widget dvd dvd-btm" style="text-align: center;">
            <h5 class="box-title"><b>REPORTE DE CAJA</b></h5>
             <!-- /.box-tools -->
        </div>
      <!-- /.box-header -->


            <!-- /.box-header -->
            <form method="post"  action="index.php?view=agregar_caja" id="addcaja">
              <div class="tile-body p-0" style="text-align: left;">

                

   
                <!-- INGRESOS -->';
                                              $montos_sin_cerrar = ProcesoData::getIngresoCaja($id_caja);
                                                    $total_sin_cerrar=0;
                                                    if(count($montos_sin_cerrar)>0){

                                                      foreach($montos_sin_cerrar as $monto_sin_cerrar):
                                                        $total_sin_cerrar=(($monto_sin_cerrar->precio*$monto_sin_cerrar->cant_noche)+$monto_sin_cerrar->total)+$total_sin_cerrar;
                                                      endforeach;

                                                    }
                                              if($id_caja!=0){ 
                                              $reporproducts = ProcesoVentaData::getIngresoCaja($id_caja);
                                              $subtotal3=0;
                                              if(count($reporproducts)>0){ 
                                                  foreach($reporproducts as $reporproduct):
                                                      $subtotal1=$reporproduct->cantidad*$reporproduct->precio; 
                                                   $subtotal3=$subtotal1+$subtotal3; 
                                                   endforeach; 
                                               }else{$subtotal3=0;} 
                                               }else{$subtotal3=0;}  
                                               $montos_sin_cerrar_egresos = GastoData::getEgresoCaja($id_caja);
                                                    $total_sin_cerrar_egreso=0;
                                                    if(count($montos_sin_cerrar_egresos)>0){

                                                      foreach($montos_sin_cerrar_egresos as $montos_sin_cerrar_egreso):
                                                        $total_sin_cerrar_egreso=$montos_sin_cerrar_egreso->precio+$total_sin_cerrar_egreso;
                                                      endforeach;

                                                    } 
                                              
 
                                              $montos_sin_cerrar_sueldos = ProcesoSueldoData::getSueldoCaja($id_caja);
                                                    $total_sin_cerrar_sueldos=0;
                                                    if(count($montos_sin_cerrar_sueldos)>0){ 

                                                      foreach($montos_sin_cerrar_sueldos as $montos_sin_cerrar_sueldo):
                                                        $total_sin_cerrar_sueldos=$montos_sin_cerrar_sueldo->monto+$total_sin_cerrar_sueldos;
                                                      endforeach;

                                                    } 
                                              


                                               
                                              if($id_caja!=0){ 
                                              $reporproducts_es = ProcesoVentaData::getEgresoCaja($id_caja);
                                              $subtotal4=0;
                                              if(count($reporproducts_es)>0){ 
                                                  foreach($reporproducts_es as $reporproduct_e):
                                                       $subtotal1=$reporproduct_e->cantidad*$reporproduct_e->precio;
                                                   $subtotal4=$subtotal1+$subtotal4;
                                                  endforeach; 
                                              }else{$subtotal4=0;} 
                                              }else{$subtotal4=0;} 
                                             $total_egreso=$total_sin_cerrar_egreso+$total_sin_cerrar_sueldos+$subtotal4; 
                                             $total_ingreso=$total_sin_cerrar+$subtotal3; 
                                             $output .= '<!-- Fin Total ingreso -->
                                            
                <table class="table mb-0">
                 
                  <tr>
                      <td><h5>FECHA:</h5></td>
                      <td><h5 class="control-label text-red">' . $hoy.'</h5></td>
                  </tr>
                   <tr>
                      <td><h5><br>Apertura caja:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$'. $caja_abierta->monto_apertura.'</h5></td>
                  </tr>
                  <tr>
                      <td><h5><br>Alquiler habitación:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$'.$total_sin_cerrar.'</h5></td>
                  </tr>
                  <tr>
                      <td><h5><br>Servicio habitación:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$'.$subtotal3.'</h5></td>
                  </tr>
                  <tr>
                      <td><h5><br>Egresos:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$'.$total_egreso.'</h5></td>
                  </tr>
                  <tr style="border-top: 2px solid #00a65a">
                      <td><h5><br>TOTAL:</h5></td>
                      <td>
                        <h5 class="control-label text-red"><br>$'. number_format($caja_abierta->monto_apertura+$total_ingreso-$total_egreso,2,'.',','); 
                        $output .= '</h5></td>
                  </tr>
    
                </table>
 
              </div>

             

             

          </form>


</section>
<section>
<div class="row">

  <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->

          <div style="margin-left:80px" class="nav-tabs-custom">
            <ul class="nav nav-tabs" style="background-color: #d2d6de;">
              <li class="active"><a href="#tab_1" data-toggle="tab">Tabla alquiler</a></li>
              <li><a href="#tab_2" data-toggle="tab">Tabla servicio a la habitación</a></li>
              <li class="pull-right text-red"><a href="reporte/pdf/documentos/reporte_diario_caja.php" target="_blank" class="text-muted"><i class="fa fa-print"></i> IMPRIMIR</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">';
                 $reportediarios = ProcesoData::getIngresoCaja($id_caja);
                if(count($reportediarios)>0){
                  // si hay usuarios
                  $output .= '
                  <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">

                  <thead style="color: black; background-color: #d2d6de;">
                        <th>Nº</th> 
                        <th>Habitación</th>
                        <th>Precio</th>
                        <th>Cant</th>
                        <th>Total</th>
                        <th>Hora ingreso</th>
                        <th>Hora salida</th> 
                  </thead>'; 
                    $numero=0; 
                    $total=0;
                    foreach($reportediarios as $reportediario):
                    $numero=$numero+1;
                    $output .= '<tr>
                        <td>'. $numero; $output .= '</td>
                        <td>'. $reportediario->getHabitacion()->nombre; $output .= '</td>
                        <td><b>$'.number_format($reportediario->precio,2,'.',','); $output .= '</b></td>
                        <td><b>$'.number_format($reportediario->cant_noche,2,'.',','); $output .= '</b></td>
                        '; $subtotal= ($reportediario->precio*$reportediario->cant_noche)+$reportediario->total;
                        $output .= '<td>$'. number_format($subtotal,2,'.',','); $output .= '</td>
                        <td>'. date($reportediario->fecha_entrada); $output .= '</td>
                        <td>'. date($reportediario->fecha_salida); $output .= '</td>
                      </tr> 
                            '; $total=$subtotal+$total;
                       endforeach; 
                       $output .= '
                     <tfoot style="color: black; background-color: #e3e4e6;">
                        <th colspan="4"><p class="pull-right">Total</p></th>
                        <th><b>$ '. number_format($total,2,'.',','); $output .= '</b></th> 
                        <th></th>
                        <th></th>
                    </tfoot>

                  </table>

               '; }
                $output .= '
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">';
               $reporproducts = ProcesoVentaData::getIngresoCaja($id_caja);
                if(count($reporproducts)>0){
                  $output .= '
                  <table  id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">

                  <thead style="color: black; background-color: #d2d6de;">
                        <th>Nº</th> 
                        <th>Habitación</th>
                        <th>Precio tarifa</th>
                        <th>Cantidad</th> 
                        <th>Precio unitario</th>
                        <th>Total</th>
                        <th>Hora </th> 
                  </thead>
                   ';$numero=0;
                     $subtotal2=0;
                     foreach($reporproducts as $reporproduct):
                     $numero=$numero+1;
                     if($reporproduct->fecha_creada!=NULL){
                      $output .= '
                      <tr>
                        <td>'.$numero .'</td>
                        <td>'.$reporproduct->getProceso()->getHabitacion()->nombre .'</td>
                        <td>'.$reporproduct->getProducto()->nombre .'</td>
                        <td><b><'.$reporproduct->cantidad.'</b></td>
                        <td><b>$   '. number_format($reporproduct->precio,2,'.',',').'</b></td>
                        '. $subtotal1=$reporproduct->cantidad*$reporproduct->precio.'
                        <td><b>$   '. number_format($subtotal1,2,'.',',').'</b></td>
                        <td>'. date($reporproduct->fecha_creada).'</td>
                      </tr> 
                    '.$subtotal2=$subtotal1+$subtotal2.'
                     '; }; 
                     endforeach; 
                     $output .= '
                    <tfoot style="color: black; background-color: #e3e4e6;">
                        <th colspan="5"><p class="pull-right">Total</p></th>
                        <th><b>$ '. number_format($subtotal2,2,'.',',').'</b> </th> 
                        <th></th>
                    </tfoot>

                  </table>

               '; }
                $output .= '
              </div>
              <!-- /.tab-pane -->
              
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>

    </div>
</div>

</section>
';  
            
  
       echo $output; 

              }
      }
?>