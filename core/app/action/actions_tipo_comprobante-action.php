<?php 
  // El siguiente codigo se encarga del mantenimiento de tipo comprobante
    // Relacion: VISTA TIPO COMPROBANTE

$action = $_POST["id_estado"];
    
//Agregar
    if ($action == 'add'){

        if( count($_POST) > 0 ){
            $habitacion = new TipoComprobanteData();
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->add();
            print "<script>window.location='index.php?view=tipo_comprobante';</script>";
        }
    }
//Actualizar

    else if ($action == 'update'){
        if(count($_POST)>0){

            $documento = TipoComprobanteData::getById($_POST["id_documento"]);
            $documento->nombre = $_POST["nombre"];
            $documento->update();
            print "<script>window.location='index.php?view=tipo_comprobante';</script>";
        
        
        }
    }
    //Eliminar
    if ($action == 'delete'){
       
        $habi = TipoComprobanteData::getById($_POST["id"]);
        $habi->del();


        Core::redir("./index.php?view=tipo_comprobante");
    }
