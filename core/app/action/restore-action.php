<?php
	session_start();
 
	//include our function
	include 'core/app/action/Restaurar_DataBase.php';
 
	if(isset($_POST['restore'])){
		
		//get post data
		$server = 'localhost';
		$username = 'root';
		$password = '';
		$dbname = 'test';
 
		//moving the uploaded sql file
		$filename = $_FILES['sql']['name'];
		move_uploaded_file($_FILES['sql']['tmp_name'],'DB/Archivos_subidos/' . $filename);
		$file_location = 'DB/Archivos_subidos/' . $filename;
 
		//restore database using our function
		$restore = restore($server, $username, $password, $dbname, $file_location);
 
		if($restore['error']){
			$_SESSION['error'] = $restore['message'];
		}
		else{
			$_SESSION['success'] = $restore['message'];
		}
 
	}
	else{
		$_SESSION['error'] = 'Completa las credenciales de la base de datos primero';
	}
 
	print "<script>window.location='index.php?view=base_de_datos';</script>";
 
?>