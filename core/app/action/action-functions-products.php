<?php

// El siguiente codigo se encarga del mantenimiento de los productos
// Relacion: VISTA PRODUCTOS

//Añadimos la lista de los nuevos impuestos 
function insert($impuesto, $producto)
{
	$proceso_impuesto = new Tipo_r_impuestos_productos();
	$proceso_impuesto->id_impuesto = $impuesto;
	$proceso_impuesto->id_producto = $producto;
	$proceso_impuesto->add();
}

//Eliminamos la lista que estaba creada.
function delete($id_producto)
{
	$process_delete = Tipo_r_impuestos_productos::getimpuestos2($id_producto);
	if (isset($process_delete)) {
		$process_delete->del_by_id_producto();
	}
}

//Añadimos la lista de los nuevos utilidad 
function insert_utilidad($utilidad, $producto)
{
	print("<br>" . $utilidad . "<br>" . $producto);
	$proceso_utilidad = new Tipo_r_utilidades_productos();
	$proceso_utilidad->id_utilidad = $utilidad;
	$proceso_utilidad->id_producto = $producto;
	$proceso_utilidad->add();
}
//Eliminamos la lista que estaba creada.
function delete_utilidad($id_producto)
{
	$process_delete = Tipo_r_utilidades_productos::getutilidad2($id_producto);
	$process_delete->del_by_id_producto();
}

// Formulas del valor total, añadiendo impuestos y descuentos
function total_de_venta($Valor_producto, $utilidad, $descuento, $id_producto)
{

	// Valor del producto
	//$Valor_producto += $Valor_producto * 0.13;

	//Utilidad 
	$utilidad = $utilidad / 100;
	$Valor_producto += $Valor_producto * $utilidad;
	//Descuento
	$descuento = $descuento / 100;
	$Valor_producto -= $descuento * $Valor_producto;
	//Variable donde sumamos todos los impuestos asociados al producto
	$suma_impuesto = 0;
	//Realizamos consulta por medio del id del producto $impuesto
	$get_impuesto_valor = Tipo_r_impuestos_productos::get_impuesto_valor($id_producto);
	//Recorremos lista de impuestos a convertir y sumar
	foreach ($get_impuesto_valor as $impuesto) {
		$impuesto = $impuesto->valor / 100;
		$impuesto = $Valor_producto * $impuesto;
		//print($impuesto."<br>");
		$suma_impuesto = $suma_impuesto + $impuesto;
		//$Valor_producto += $impuesto;
	}


	$Valor_producto = $suma_impuesto + $Valor_producto;
	echo $Valor_producto;
	//print(number_format($Valor_producto, 2, ',', ' '));
}

// Formulas del valor total, añadiendo impuestos y descuentos retornando
function total_de_venta_excel($Valor_producto, $utilidad, $descuento, $id_producto)
{

	// Valor del producto
	//$Valor_producto += $Valor_producto * 0.13;

	//Utilidad 
	$utilidad = $utilidad / 100;
	$Valor_producto += $Valor_producto * $utilidad;
	//Descuento
	$descuento = $descuento / 100;
	$Valor_producto -= $descuento * $Valor_producto;
	//Variable donde sumamos todos los impuestos asociados al producto
	$suma_impuesto = 0;
	//Realizamos consulta por medio del id del producto $impuesto
	$get_impuesto_valor = Tipo_r_impuestos_productos::get_impuesto_valor($id_producto);
	//Recorremos lista de impuestos a convertir y sumar
	foreach ($get_impuesto_valor as $impuesto) {
		$impuesto = $impuesto->valor / 100;
		$impuesto = $Valor_producto * $impuesto;
		//print($impuesto."<br>");
		$suma_impuesto = $suma_impuesto + $impuesto;
		//$Valor_producto += $impuesto;
	}


	$Valor_producto = $suma_impuesto + $Valor_producto;
	return $Valor_producto;
	//print(number_format($Valor_producto, 2, ',', ' '));
}
function impuesto_total($Valor_producto, $utilidad, $descuento, $id_producto)
{

	// Valor del producto
	//$Valor_producto += $Valor_producto * 0.13;
	//Utilidad 
	$utilidad = $utilidad / 100;
	$Valor_producto += $Valor_producto * $utilidad;
	//Descuento
	$descuento = $descuento / 100;
	$Valor_producto -= $descuento * $Valor_producto;
	//Variable donde sumamos todos los impuestos asociados al producto
	$suma_impuesto = 0;
	//Realizamos consulta por medio del id del producto $impuesto
	$get_impuesto_valor = Tipo_r_impuestos_productos::get_impuesto_valor($id_producto);
	//Recorremos lista de impuestos a convertir y sumar
	foreach ($get_impuesto_valor as $impuesto) {
		$impuesto = $impuesto->valor / 100;
		$impuesto = $Valor_producto * $impuesto;
		//print($impuesto."<br>");
		$suma_impuesto = $suma_impuesto + $impuesto;
		//$Valor_producto += $impuesto;
	}
	print(number_format($suma_impuesto, 2, ',', ' '));
}

//Obtener Stock de productos version realizada
function stock_productos($id_producto, $stock_producto)
{
	$entrada_producto = 0;
	$entradas = ProcesoVentaData::getAllEntradas($id_producto);
	if (count($entradas) > 0) {
		foreach ($entradas as $entrada) : $entrada_producto = $entrada->cantidad + $entrada_producto;
		endforeach;
	} else {
		$entrada_producto = 0;
	};
	$salida_producto = 0;
	$salidas = ProcesoVentaData::getAllSalidas($id_producto);
	if (count($salidas) > 0) {
		foreach ($salidas as $salida) : $salida_producto = $salida->cantidad + $salida_producto;
		endforeach;
	} else {
		$salida_producto = 0;
	};
	$stock = ($stock_producto + $entrada_producto) - $salida_producto;
?>
	<?php
	return $stock;
	?></td> <?php
		}

		//Obtener Stock de productos version realizada
		function stock_producto_egreso($id_producto)
		{
			foreach ($id_producto as $pro) {
				$producto = ProductoData::getById($pro);
				$stock_total = stock_productos($producto->id, $producto->stock);
				if ($stock_total > $producto->aviso_stock) {
					$producto->count_mail = 0;
					$producto->update_aviso();
				}
			}
		}

		// stock maximo por producto (en compra)

		function stock_maximo_producto($id_producto, $cantidad, $stock)
		{
			$stock_maximo = ProductoData::getById($id_producto);
			$stock = $stock + $cantidad;

			if ($stock_maximo->stock_max < $stock) {
				return 1;
			} else {
				return 2;
			}
		}
		function total_sin_impuestos($Valor_producto, $id_producto)
		{


			$producto_v = ProductoData::getById($id_producto);
			//Utilidad 
			$utilidad = $producto_v->utilidad / 100;
			$Valor_producto += $Valor_producto * $utilidad;
			//Descuento
			$descuento = Descuento_Producto::getById($producto_v->id_descuento);
			$descuento = $descuento->valor / 100;
			$Valor_producto -= $descuento * $Valor_producto;

			return $Valor_producto;
			//print(number_format($Valor_producto, 2, ',', ' '));
		}
		function total_sin_impuestos_decimales($Valor_producto, $id_producto)
		{


			$producto_v = ProductoData::getById($id_producto);
			//Utilidad 
			$utilidad = $producto_v->utilidad / 100;
			$Valor_producto += $Valor_producto * $utilidad;
			//Descuento
			$descuento = Descuento_Producto::getById($producto_v->id_descuento);
			$descuento = $descuento->valor / 100;
			$Valor_producto -= $descuento * $Valor_producto;

			echo $Valor_producto;
			//print(number_format($Valor_producto, 2, ',', ' '));
		}
		function impuesto_total_venta($Valor_producto, $utilidad, $descuento, $id_producto)
		{

			// Valor del producto
			//$Valor_producto += $Valor_producto * 0.13;
			//Utilidad 
			$utilidad = $utilidad / 100;
			$Valor_producto += $Valor_producto * $utilidad;
			//Descuento
			$descuento = $descuento / 100;
			$Valor_producto -= $descuento * $Valor_producto;
			//Variable donde sumamos todos los impuestos asociados al producto
			$suma_impuesto = 0;
			//Realizamos consulta por medio del id del producto $impuesto
			$get_impuesto_valor = Tipo_r_impuestos_productos::get_impuesto_valor($id_producto);
			//Recorremos lista de impuestos a convertir y sumar
			foreach ($get_impuesto_valor as $impuesto) {
				$impuesto = $impuesto->valor / 100;
				$impuesto = $Valor_producto * $impuesto;
				//print($impuesto."<br>");
				$suma_impuesto = $suma_impuesto + $impuesto;
				//$Valor_producto += $impuesto;
			}
			return $suma_impuesto;
		}


		// Formulas del valor total, añadiendo impuestos y descuentos
		function total_con_utilidad($Valor_producto, $utilidad)
		{

			// Valor del producto

			//Utilidad 
			$utilidad = $utilidad / 100;
			$Valor_producto += $Valor_producto * $utilidad;
			
			return $Valor_producto;
			//print(number_format($Valor_producto, 2, ',', ' '));
		}
