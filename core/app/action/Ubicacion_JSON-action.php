<?php
// El siguiente codigo se encarga de mostrar todas las provincias,cantones,distritos de COSTA RICA
// Relacion: VISTA CLIENTE/VISTA CONFIRGURACION


$action = $_POST['id'];


if ($action == 1) {
    //Canton
    $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $_POST['idProvincia'] . "/cantones.json");
    $Cantones = get_object_vars(json_decode($jsonc));
?>
    <select name="canton" id="canton" required class="form-control" onchange="CargarDistrito(this.value,<?php echo $_POST['idProvincia'] ?>);">
        <?php foreach ($Cantones as $key => $tipo_documento) : ?>
            <option value="<?php echo ($key); ?>"><?php echo ($tipo_documento); ?></option>
        <?php endforeach; ?>
    </select>
<?php
} else if ($action == 2) {

    //Distrito
    $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $_POST['idProvincia'] . "/canton/" . $_POST['idCanton'] . "/distritos.json");
    $Distritos = get_object_vars(json_decode($jsonc));
?>
    <select name="distrito" id="distrito" required class="form-control" onchange="CargarBarrio();">
        <?php foreach ($Distritos as $key => $tipo_documento) : ?>
            <option value="<?php echo $key; ?>"><?php echo ($tipo_documento); ?></option>
        <?php endforeach; ?>
    </select>
<?php
} else if ($action == 3) {
    //Canton Actualizar
    $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $_POST['idProvincia'] . "/cantones.json");
    $Cantones = get_object_vars(json_decode($jsonc));
?>
    <select name="canton" id="canton" required class="form-control" onchange="CargarDistritoM(this.value,<?php echo $_POST['idProvincia'] ?>, <?php echo $_POST['cl_id'] ?>);">
        <?php foreach ($Cantones as $key => $tipo_documento) : ?>
            <option value="<?php echo ($key); ?>"><?php echo ($tipo_documento); ?></option>
        <?php endforeach; ?>
    </select>
<?php
} else if ($action == 4) {

    //Distrito Actualizar
    $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $_POST['idProvincia'] . "/canton/" . $_POST['idCanton'] . "/distritos.json");
    $Distritos = get_object_vars(json_decode($jsonc));
?>
    <select name="distrito" id="distrito" required class="form-control" onchange="CargarBarrio(value);">
        <?php foreach ($Distritos as $key => $tipo_documento) : ?>
            <option value="<?php echo $key; ?>"><?php echo ($tipo_documento); ?></option>
        <?php endforeach; ?>
    </select>
<?php
}









?>