<?php
// El siguiente codigo se encarga del mantenimiento de utilidad de productos
// Relacion: VISTA UTILIDADES DE PRODUCTOS

$action = $_POST["id_estado"];

//Add
if ($action == 'add') {

    $bodega = new Utilidad_Productos();
    $bodega->nombre = $_POST["nombre"];
    $bodega->valor = $_POST["valor"];
    $bodega->estado = 1;
    $bodega->add();
    print "<script>window.location='index.php?view=sala_utilidad_productos';</script>";
}

//Update
else if ($action == 'update') {
    print('<br><br><br><br>');
    $bodega = Utilidad_Productos::getById($_POST["id_documento"]);
    $bodega->nombre = $_POST["nombre"];
    $bodega->valor = $_POST["valor"];
    $bodega->estado = $_POST["estado"];
    $bodega->update();
    print "<script>window.location='index.php?view=sala_utilidad_productos';</script>";
}


//Desactivar
else if ($action == 'desactivar') {
    print('<br><br><br><br>');
    $bodega = Utilidad_Productos::getById($_POST["id"]);
    $bodega->estado = 0;
    $bodega->update_estado();
    print "<script>window.location='index.php?view=sala_utilidad_productos';</script>";
}
//Delete
if ($action == 'delete') {

    $bodega = Utilidad_Productos::getById($_POST['id']);
    $bodega->del();

    //Core::redir("./index.php?view=m_documento");
}
