<?php
// El siguiente codigo se encarga del mantenimiento de notificaciones de stock en productos (MAIL)
// Relacion: VISTA NOTIFICACION DE STOCKS DE PRODUCTOS /DT = ESTA SE ENCUENTRA EN LA VISTA DE SALA DE NOTIFICACIONES/STOCKS

		$action = $_POST["id_estado"];
		//Add
		if ($action == 'add'){
            $habitacion = new StockNotificacionesProductos();
            $habitacion->valor = $_POST["valor"];
            $habitacion->add();
            print "<script>window.location='index.php?view=sala_notificaciones_stocks_productos';</script>";
		}
		//Update
		else if ($action == 'update'){
			$documento = StockNotificacionesProductos::getById($_POST["id_documento"]);
			$documento->valor = $_POST["valor"];
			$documento->update();
			print "<script>window.location='index.php?view=sala_notificaciones_stocks_productos';</script>";
		}
		//Delete
		if ($action == 'delete'){
			$habi = StockNotificacionesProductos::getById($_POST['id']);
			$habi->del();
		}

		 //Desactivar
		 if ($action == 'desactivar'){
			$documento = StockNotificacionesProductos::getById($_POST["id"]);
			$documento->estado = 1;
			$documento->id = $_POST["id"];
			$documento->update();
        }

		//Activar
		if ($action == 'activar'){
			$documento = StockNotificacionesProductos::getById($_POST["id"]);
			$documento->estado = 0;
			$documento->id = $_POST["id"];
			$documento->update();
        }
