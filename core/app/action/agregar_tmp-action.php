<?php
require __DIR__ . '/../../../core/app/action/action-functions-products.php';
// Este documento es el encargado de agregar, modificar y eliminar las (sesiones) encontradas en la tabla Tmp
$session_id = session_id();

if (isset($_POST['id'])) {
    $id = $_POST['id'];
}
if (isset($_POST['cantidad'])) {
    $cantidad = $_POST['cantidad'];
}
if (isset($_POST['precio_venta'])) {
    $precio_venta = $_POST['precio_venta'];
}
if (isset($_POST['precio_venta_exo'])) {
    $precio_venta_exo = $_POST['precio_venta_exo'];
}
if (isset($_POST['cantidad']) and isset($_POST['precio_venta'])) {
    $producto = TmpData::getByIdProductoVenta($_POST['id']);
    $object = (array) $producto;
    $object = count($object);
    //Creamos condiciones en donde verificamos si el tipo de insercion es de compra = 2 o venta = 1
    if ($object  > 0) {
        if ($producto->tipo_operacion == 1) {
            $temporal = TmpData::getById($producto->id_tmp);
            $temporal->cantidad_tmp = $producto->cantidad_tmp + $_POST['cantidad'];
            $temporal->updateCantidad();
        } else {
            $temporal = new TmpData();
            $temporal->id_producto = $_POST['id'];
            $temporal->cantidad_tmp = $_POST['cantidad'];
            $temporal->precio_tmp = $_POST["precio_venta"];
            $temporal->precio_tmp_exo = 0;
            $temporal->sessionn_id = $session_id;
            $temporal->tipo_operacion = 2;
            $temporal->addTmp();
        }
    } else {
        $temporal = new TmpData();
        $temporal->id_producto = $_POST['id'];
        $temporal->cantidad_tmp = $_POST['cantidad'];
        $temporal->precio_tmp = $_POST["precio_venta"];
        $temporal->precio_tmp_exo = $_POST["precio_venta_exo"];
        $temporal->sessionn_id = $session_id;
        $temporal->tipo_operacion = 1;
        $temporal->addTmp();
    }
}



if (isset($_GET['id'])) //codigo elimina un elemento del array
{
    $del = TmpData::getById($_GET["id"]);
    $del->del();
}

$tmps = TmpData::getAllTemporal($session_id);
// Si hay usuarios
?>
<?php
$sumador_total = 0;
foreach ($tmps as $tmp) : ?>
    <tr>
        <td></td>
        <td><?php if ($tmp->id_producto != null) {
                echo $tmp->getProduct()->nombre;
            } else {
                echo "<center>----</center>";
            }  ?></td>
        <td><?php echo $tmp->cantidad_tmp; ?></td>
        <td>$ <?php

                //echo number_format($tmp->precio_tmp, 2, '.', ',');
                $venta_corriente = ProductoData::getById($tmp->id_producto);
                echo number_format($venta_corriente->precio_venta, 2, '.', ',');
                ?>
        </td>
        <td>$ <?php

                $venta_corriente = ProductoData::getById($tmp->id_producto);
                $id = $tmp->id_producto;
                $valor_SI = total_sin_impuestos($venta_corriente->precio_venta, $id);
                echo $valor_SI * $tmp->cantidad_tmp;
                ?>
        </td>
        <?php $sumar_t = $tmp->cantidad_tmp * $tmp->precio_tmp; ?>
        <td>$ <?php echo number_format($sumar_t, 2, '.', ','); ?></td>
        <td><span class="pull-right"><a href="#" onclick="eliminar('<?php echo $tmp->id_tmp ?>')"><i class="glyphicon glyphicon-trash"></i> </a></span></td>
    </tr>
<?php
    $sumador_total += $sumar_t;
endforeach ?>
<tr></tr>
<!--<tr style="background-color: #f3f3f3;">
    <td colspan=5><span class="pull-right">SinImp</span></td>
    <td><span class="pull-left">
            <?php
            $val = array();
            $total_sin_impuesto = 0;
            foreach ($tmps as $tmp) :
                $precio = $tmp->precio_tmp;
                /*$rest = $precio * 0.13;
                $precio = $precio - $rest;*/
                $precio = $precio * $tmp->cantidad_tmp;
                array_push($val, $precio);
            endforeach;

            foreach ($val as $suma_total) {
                $total_sin_impuesto += $suma_total;
            }
            echo '$  ' . number_format($total_sin_impuesto, 2, '.', ',');
            ?>
        </span></td>
    <td></td>
</tr>-->
<tr style="background-color: #f3f3f3;">
    <td colspan=5><span class="pull-right">Impuesto</span></td>
    <td><span class="pull-left">
            <?php
            $val = array();
            $total_impuesto = 0;
            foreach ($tmps as $tmp):
                $producto = ProductoData::getById($tmp->id_producto);
                $descuento = Descuento_Producto::getById($producto->id_descuento);
                $valor_imp = impuesto_total_venta($producto->precio_venta, $producto->utilidad, $descuento->valor, $tmp->id_producto);
                $valor_imp = $tmp->cantidad_tmp * $valor_imp;
                //echo $valor_imp;
                array_push($val, $valor_imp);
            endforeach;
            foreach ($val as $suma_total) {
                $total_impuesto += $suma_total;
            }
            echo '$  ' . number_format($total_impuesto, 2, '.', ',');
            ?>
            <?php
            /*$val = array();
            $total_impuesto = 0;
            foreach ($tmps as $tmp) :
                $precio = $tmp->precio_tmp;
                $rest = $precio * 0.13;
                $rest = $rest * $tmp->cantidad_tmp;
                array_push($val, $rest);
            endforeach;
            foreach ($val as $suma_total) {
                $total_impuesto += $suma_total;
            }
            echo '$  ' . number_format($total_impuesto, 2, '.', ',');*/
            ?>
        </span></td>
    <td></td>
</tr>

<tr style="background-color: #e4e4e4;">
    <td colspan=5><span class="pull-right">TOTAL </span></td>
    <td><span class="pull-left"><?php echo '$  ' . number_format($sumador_total, 2, '.', ','); ?></span></td>
    <td></td>
</tr>