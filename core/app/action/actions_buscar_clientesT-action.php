        <?php
        // El siguiente codigo se encarga de buscar clientes existente en base de datos
        // Relacion: VISTA VENTAS / VISTA PROCESO RECEPCION


        $tipo = 51;
        ?>

        <!-- tile body -->
        <div class="tile-body">
            <div class="form-group">
                <center>
                    <h5 for="filter_1" style="padding-top: 5px; font-size: 25px;">Buscar:</h5>
                    <input id="filter_1" style="width: 550px;" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
                </center>
            </div>
            <?php if (isset($_GET['buscar']) and $_GET['buscar'] != "") {
                $clientes = PersonaData::getLike($_GET['buscar']);
            } else {
                $clientes = PersonaData::getAll_ventas();
            } ?>
            <?php
            if (count($clientes) > 0) {  ?>
                <table id="searchTextResults" data-filter="#filter_1" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">
                    <thead style="color: white; background-color: #827e7e;">
                        <tr>
                            <th>Nº</th>
                            <th>Tipo documento</th>
                            <th data-hide="phone">ID</th>
                            <th data-hide='phone, tablet'>Nombre completo</th>
                            <th data-hide='phone, tablet'>Email</th>
                            <th data-hide='phone, tablet'>Teléfono</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($clientes as $cliente) : ?>
                            <tr>
                                <td><?php echo $cliente->id; ?></td>
                                <td><b><?php echo $cliente->getTipoDocumento()->nombre; ?></b></td>
                                <td><?php echo $cliente->documento; ?></td>
                                <td><?php echo $cliente->nombre; ?></td>
                                <td><?php echo $cliente->email; ?></td>
                                <td><?php echo $cliente->telefono; ?></td>
                                <td>
                                    <a onclick="select(<?php echo $cliente->id; ?>)" class="btn btn-success btn-xs">Seleccinar</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot class="hide-if-no-paging" style="left: -20px;">
                        <tr>
                            <td colspan="9" class="text-center">
                                <ul class="pagination"></ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>

            <?php } else {
                echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
            };
            ?>

        </div>
        <!-- /tile body -->

        <script>
            function select(id_cliente, documento) {
                var parametros = {
                    "documento": null,
                    "id": id_cliente,
                    "id_mostrar_cliente": "show_cliente",
                    "tipo": <?php echo $tipo; ?>
                }
                $.ajax({
                    type: "POST",
                    url: 'index.php?action=actions_datos_cliente',
                    data: parametros,
                    success: function(resp) {
                        $('#cliente_doc').html(resp).then($('#myModalC').modal('hide'));
                    }
                });
            }
        </script>