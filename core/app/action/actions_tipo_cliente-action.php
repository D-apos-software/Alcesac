<?php

// El siguiente codigo se encarga del mantenimiento tipo cliente
// Relacion: VISTA CLIENTES

$action = $_POST["id_estado"];

//Agregar
if ($action == 'add') {

    if (count($_POST) > 0) {
        if (isset($_POST['campo'])) {
            $habitacion = new TipoCliente();
            $habitacion->valor_extra = $_POST['campo'];
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->add();
            print "<script>window.location='index.php?view=tipo_cliente';</script>";
        } else {
            $habitacion = new TipoCliente();
            $habitacion->valor_extra = $_POST['campo'];
            $habitacion->nombre = $_POST["nombre"];
            $habitacion->add();
            print "<script>window.location='index.php?view=tipo_cliente';</script>";
        }
    }
}

//Actualizar
if ($action == 'update') {
    if (count($_POST) > 0) {
        if (isset($_POST['campo'])) {
            $documento = TipoCliente::getById($_POST["id_documento"]);
            $documento->nombre = $_POST["nombre"];
            $documento->valor_extra = $_POST["campo"];
            $documento->update();
            print "<script>window.location='index.php?view=tipo_cliente';</script>";
        } else {
            $documento = TipoCliente::getById($_POST["id_documento"]);
            $documento->nombre = $_POST["nombre"];
            $documento->valor_extra = $_POST["campo"];
            $documento->update();
            print "<script>window.location='index.php?view=tipo_cliente';</script>";
        }
    }
}

//Eliminar
if ($action == 'eliminar') {

    $habi = TipoCliente::getById($_POST["id"]);
    $habi->del();

    Core::redir("./index.php?view=tipo_cliente");
}

//Activar
if ($action == 'activar') {

    $documento = TipoCliente::getById($_POST["id"]);
    $documento->estado = 0;
    $documento->cambiar_estado();

    Core::redir("./index.php?view=tipo_cliente_estado");
}

//Desactivar
if ($action == 'desactivar') {

    $documento = TipoCliente::getById($_POST["id"]);
    $documento->estado = 1;
    $documento->cambiar_estado();

    Core::redir("./index.php?view=tipo_cliente_estado");
}
