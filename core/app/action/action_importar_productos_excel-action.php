<?php
// En este archivo nos llegan los parametros del excel enviado desde la vista productos_excel, para importar los productos (IMPORTACION MASIVA)
require __DIR__ . '/../../../core/app/action/action-functions-products.php';
require 'lib/PHPExcel/Classes/PHPExcel.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
</head>

<body>

    <?php
    $path = $_FILES['dataCliente']['name'];
    $array = pathinfo($path);
    $extension_doc = $array["extension"];

    $nombres = array();
    $alert = 0;


    if ($extension_doc == 'csv') {



        $tipo       = $_FILES['dataCliente']['type'];
        $tamanio    = $_FILES['dataCliente']['size'];
        $archivotmp = $_FILES['dataCliente']['tmp_name'];
        $lineas     = file($archivotmp);

        $i = 0;

        foreach ($lineas as $linea) {
            $cantidad_registros = count($lineas);
            $cantidad_regist_agregados =  ($cantidad_registros - 1);

            if ($i != 0) {

                $datos = explode(";", $linea);

                $codigo                   = !empty($datos[0])  ? ($datos[0]) : '';
                $codigo_barra             = !empty($datos[1])  ? ($datos[1]) : '';
                $nombre                   = !empty($datos[2])  ? ($datos[2]) : '';
                $marca                    = !empty($datos[3])  ? ($datos[3]) : '';
                $descripcion              = !empty($datos[4])  ? ($datos[4]) : '';
                $acreditable              = !empty($datos[5])  ? ($datos[5]) : '';
                $presentacion             = !empty($datos[6])  ? ($datos[6]) : '';
                $presio_compra            = !empty($datos[7])  ? ($datos[7]) : '';
                $presio_venta             = !empty($datos[8])  ? ($datos[8]) : '';
                $stock_inicial            = !empty($datos[9])  ? ($datos[9]) : '';
                $aviso_stock_min          = !empty($datos[10])  ? ($datos[10]) : '';
                $aviso_stock_max          = !empty($datos[11])  ? ($datos[11]) : '';
                $utilidad                 = !empty($datos[12])  ? ($datos[12]) : '';
                $bodega                   = !empty($datos[13])  ? ($datos[13]) : '';
                $unidad_de_medida         = !empty($datos[14])  ? ($datos[14]) : '';
                $descuento                = !empty($datos[15])  ? ($datos[15]) : '';
                $impuestos                = !empty($datos[16])  ? ($datos[16]) : '';

                //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------- Agregamos datos a la tabla de productos --------------------------------------------------------------------------------------------------
                //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

                $producto = new ProductoData();

                if ($codigo != Null && $codigo != "") {
                    $producto->codigo = $codigo;
                } else {
                    $producto->codigo = 'Nulo';
                }

                if ($codigo_barra != Null && $codigo_barra != "") {
                    $producto->codigo_barra = $codigo_barra;
                } else {
                    $producto->codigo_barra = 'Nulo';
                }

                if ($nombre != Null && $nombre != "") {
                    $producto->nombre = $nombre;
                } else {
                    $producto->nombre = 'Nulo';
                }

                if ($presentacion != Null && $presentacion != "") {
                    $producto->presentacion = $presentacion;
                } else {
                    $producto->presentacion = 'Nulo';
                }

                if ($marca != Null && $marca != "") {
                    $producto->marca = $marca;
                } else {
                    $producto->marca = 'Nulo';
                }

                if ($descripcion != Null && $descripcion != "") {
                    $producto->descripcion = $descripcion;
                } else {
                    $producto->descripcion = 'Nulo';
                }

                if ($presio_compra != Null && $presio_compra != "") {
                    $producto->precio_compra = $presio_compra;
                } else {
                    $producto->precio_compra = 0;
                }

                if ($presio_venta != Null && $presio_venta != "") {
                    $producto->precio_venta = $presio_venta;
                } else {
                    $producto->precio_venta = 0;
                }

                if ($stock_inicial != Null && $stock_inicial != "") {
                    $producto->stock = $stock_inicial;
                } else {
                    $producto->stock = 0;
                }

                $producto->id_proveedor = 0;

                if ($aviso_stock_max != Null && $aviso_stock_max != "") {
                    $producto->stock_max = $aviso_stock_max;
                } else {
                    $producto->stock_max = 0;
                }

                if ($aviso_stock_min != Null && $aviso_stock_min != "") {
                    $producto->aviso_stock = $aviso_stock_min;
                } else {
                    $producto->aviso_stock = 0;
                }

                $producto->utilidad =  $utilidad;
                $producto->imagen = 0;

                if ($acreditable == 'Si' || $acreditable == 'si' || $acreditable == 'sI') {
                    $producto->acreditable = 1;
                } else {
                    $producto->acreditable = 0;
                }

                $dt_bodega = Bodegas::getByname($bodega);
                if (!empty($dt_bodega)) {
                    $producto->id_bodega = $dt_bodega->id;
                } else {
                    $producto->id_bodega = null;
                }
                $dt_un_medida = Unidad_de_medida::getByname($unidad_de_medida);
                if (!empty($dt_un_medida)) {
                    $producto->id_unidad_medida = $dt_un_medida->id;
                } else {
                    $producto->id_unidad_medida = null;
                }


                $dt_descuento = Descuento_Producto::getByValor($descuento);
                if (!empty($dt_descuento)) {
                    if ($descuento < $utilidad) {
                        $producto->id_descuento = $dt_descuento->id;
                    } else {
                        $producto->id_descuento = 5;
                        $alert = 1;
                        array_push($nombres, $nombre);
                    }
                } else {
                    $producto->id_descuento = 5;
                }


                $data_product = $producto->add();


                //print($impuestos);
                //echo ('<br><br><br>');

                $lista_impuestos = array();

                $separador = ",";
                $li_impuestos = explode($separador, $impuestos);


                //Insertamos informacion relacionada entre producto y los impuestos seleccionados
                foreach ($li_impuestos as $datas) {
                    $valor = Tipo_impuesto_productos::getbyvalor($datas);
                    //array_push($lista_impuestos, $datas);
                    insert($valor->id, $data_product[1]);
                }
            }
            $i++;
        }

    ?>

        <script>
            const cars = [<?php echo json_encode($nombres); ?>];

            let text = "";
            for (let i = 0; i < cars.length; i++) {
                text += cars[i] + "<br>";
            }

            var alerta = "<?php echo $alert; ?>";
            if (alerta == 1) {
                Swal.fire({
                    title: '<strong>Aviso <u>Importante</u></strong>',
                    icon: 'info',
                    html: 'Los siguientes productos contenian el valor de <b>DESCUENTO</b> mayor a la <b>UTILIDAD:</b><br>' + '- ' + text,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Entendido!',
                    confirmButtonAriaLabel: 'Thumbs up, great!'
                }).then(function() {
                    window.location = "index.php?view=productos_excel";
                })
            } else {
                Swal.fire({
                    icon: 'success',
                    title: 'Importación satisfactoria',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    window.location = "index.php?view=productos_excel";
                })
            }
        </script>



    <?php

    } else if ($extension_doc == 'xlsx') {


		//moving the uploaded sql file
		move_uploaded_file($_FILES['dataCliente']['tmp_name'],'DB/Excel_tmp/' . $path);
        $archivo = 'DB/Excel_tmp/'.$path;


        $excel = PHPExcel_IOFactory::load($archivo);
        
        
        $numerofila = $excel -> setActiveSheetIndex(0)->getHighestRow();
        
        for($i=2; $i <= $numerofila; $i++){
            
            $Codigo_xlsx = $excel -> getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
            $Codigo_barra_xlsx = $excel -> getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
            $nombre_xlsx = $excel -> getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
            $marca_xlsx = $excel -> getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
            $descripcion_xlsx = $excel -> getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
            $acreditable_xlsx = $excel -> getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
            $presentacion_xlsx = $excel -> getActiveSheet()->getCell('G'.$i)->getCalculatedValue();
            $presio_compra_xlsx = $excel -> getActiveSheet()->getCell('H'.$i)->getCalculatedValue();
            $presio_venta_xlsx = $excel -> getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
            $stock_xlsx = $excel -> getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
            $aviso_stock_min_xlsx = $excel -> getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
            $aviso_stock_max_xlsx = $excel -> getActiveSheet()->getCell('L'.$i)->getCalculatedValue();
            $utilidad_xlsx = $excel -> getActiveSheet()->getCell('M'.$i)->getCalculatedValue();
            $bodega_xlsx = $excel -> getActiveSheet()->getCell('N'.$i)->getCalculatedValue();
            $unidad_de_medida_xlsx = $excel -> getActiveSheet()->getCell('O'.$i)->getCalculatedValue();
            $descuento_xlsx = $excel -> getActiveSheet()->getCell('P'.$i)->getCalculatedValue();
            //Impuestos
            $impuesto_xlsx = $excel -> getActiveSheet()->getCell('Q'.$i)->getCalculatedValue();

            
            //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            //--------------------------------------------------------- Agregamos datos a la tabla de productos --------------------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 


                $producto = new ProductoData();

                if ($Codigo_xlsx != Null && $Codigo_xlsx != "") {
                    $producto->codigo = $Codigo_xlsx;
                } else {
                    $producto->codigo = 'Nulo';
                }

                if ($Codigo_barra_xlsx != Null && $Codigo_barra_xlsx != "") {
                    $producto->codigo_barra = $Codigo_barra_xlsx;
                } else {
                    $producto->codigo_barra = 'Nulo';
                }

                if ($nombre_xlsx != Null && $nombre_xlsx != "") {
                    $producto->nombre = $nombre_xlsx;
                } else {
                    $producto->nombre = 'Nulo';
                }

                if ($presentacion_xlsx != Null && $presentacion_xlsx != "") {
                    $producto->presentacion = $presentacion_xlsx;
                } else {
                    $producto->presentacion = 'Nulo';
                }

                if ($marca_xlsx != Null && $marca_xlsx != "") {
                    $producto->marca = $marca_xlsx;
                } else {
                    $producto->marca = 'Nulo';
                }

                if ($descripcion_xlsx != Null && $descripcion_xlsx != "") {
                    $producto->descripcion = $descripcion_xlsx;
                } else {
                    $producto->descripcion = 'Nulo';
                }

                if ($presio_compra_xlsx != Null && $presio_compra_xlsx != "") {
                    $producto->precio_compra = $presio_compra_xlsx;
                } else {
                    $producto->precio_compra = 0;
                }

                if ($presio_venta_xlsx != Null && $presio_venta_xlsx != "") {
                    $producto->precio_venta = $presio_venta_xlsx;
                } else {
                    $producto->precio_venta = 0;
                }

                if ($stock_xlsx != Null && $stock_xlsx != "") {
                    $producto->stock = $stock_xlsx;
                } else {
                    $producto->stock = 0;
                }

                $producto->id_proveedor = 0;

                if ($aviso_stock_max_xlsx != Null && $aviso_stock_max_xlsx != "") {
                    $producto->stock_max = $aviso_stock_max_xlsx;
                } else {
                    $producto->stock_max = 0;
                }

                if ($aviso_stock_min_xlsx != Null && $aviso_stock_min_xlsx != "") {
                    $producto->aviso_stock = $aviso_stock_min_xlsx;
                } else {
                    $producto->aviso_stock = 0;
                }

                $producto->utilidad =  $utilidad_xlsx;
                $producto->imagen = 0;

                if ($acreditable_xlsx == 'Si' || $acreditable_xlsx == 'si' || $acreditable_xlsx == 'sI') {
                    $producto->acreditable = 1;
                } else {
                    $producto->acreditable = 0;
                }

                $dt_bodega = Bodegas::getByname($bodega_xlsx);
                if (!empty($dt_bodega)) {
                    $producto->id_bodega = $dt_bodega->id;
                } else {
                    $producto->id_bodega = null;
                }
                $dt_un_medida = Unidad_de_medida::getByname($unidad_de_medida_xlsx);
                if (!empty($dt_un_medida)) {
                    $producto->id_unidad_medida = $dt_un_medida->id;
                } else {
                    $producto->id_unidad_medida = null;
                }


                $dt_descuento = Descuento_Producto::getByValor($descuento_xlsx);
                if (!empty($dt_descuento)) {
                    if ($descuento_xlsx < $utilidad_xlsx) {
                        $producto->id_descuento = $dt_descuento->id;
                    } else {
                        $producto->id_descuento = 5;
                        $alert = 1;
                        array_push($nombres, $nombre_xlsx);
                    }
                } else {
                    $producto->id_descuento = 5;
                }


                $data_product = $producto->add();

                $separador = ",";
                $li_impuestos = explode($separador, $impuesto_xlsx);


                //Insertamos informacion relacionada entre producto y los impuestos seleccionados
                foreach ($li_impuestos as $datas) {
                    $valor = Tipo_impuesto_productos::getbyvalor($datas);
                    //array_push($lista_impuestos, $datas);
                    insert($valor->id, $data_product[1]);
                }

        }

        // Una ves realizamos la insercion de los productos encontrados en el excel xlsx, eliminamos el archivo ya que es inecesario
        $files = glob('DB/Excel_tmp/*'); // obtenemos los nombres
        foreach($files as $file){ // itramos los datos
          if(is_file($file))
            unlink($file); // eliminamos archivos
        }
    ?>
          <script>
            const cars = [<?php echo json_encode($nombres); ?>];

            let text = "";
            for (let i = 0; i < cars.length; i++) {
                text += cars[i] + "<br>";
            }

            var alerta = "<?php echo $alert; ?>";
            if (alerta == 1) {
                Swal.fire({
                    title: '<strong>Aviso <u>Importante</u></strong>',
                    icon: 'info',
                    html: 'Los siguientes productos contenian el valor de <b>DESCUENTO</b> mayor a la <b>UTILIDAD:</b><br>' + '- ' + text,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Entendido!',
                    confirmButtonAriaLabel: 'Thumbs up, great!'
                }).then(function() {
                    window.location = "index.php?view=productos_excel";
                })
            } else {
                Swal.fire({
                    icon: 'success',
                    title: 'Importación satisfactoria',
                    showConfirmButton: false,
                    timer: 1500
                }).then(function() {
                    window.location = "index.php?view=productos_excel";
                })
            }
        </script>


    <?php
    }


    ?>
</body>

</html>