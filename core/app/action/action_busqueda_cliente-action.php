<?php
// El siguiente codigo se encarga de validar si el cliente existe dentro de la base de datos y sino existe realizar la busqueda el tribunal 
// Relacion: VISTA VENTA

if ($_POST['fs'] == '0') {
    $Datos_cliente = PersonaData::getByDocumento($_POST['id']);
    echo json_encode($Datos_cliente);
} else if ($_POST['fs'] == '1') {
    $Datos_cliente = ProcesoData::getProcesoIDCliente($_POST['id']);
    echo json_encode($Datos_cliente);
} else if ($_POST['fs'] == '3') {
    $tipo_cliente = TipoDocumentoData::getById($_POST['TipoDocumentoData']);
    echo $tipo_cliente->valor;
}
