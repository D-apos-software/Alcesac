<?php
// El siguiente codigo se encarga del mantenimiento de unidades de medida
// Relacion: VISTA UNIDAD DE MEDIDA

$action = $_POST["id_estado"];

//Add
if ($action == 'add') {

    $bodega = new Unidad_de_medida();
    $bodega->nombre = $_POST["nombre"];
    $bodega->descripcion = $_POST["descripcion"];
    $bodega->estado = 0;
    $bodega->add();
    print "<script>window.location='index.php?view=sala_unidad_de_medida';</script>";
}

//Update
else if ($action == 'update') {
    print('<br><br><br><br>');
    $bodega = Unidad_de_medida::getById($_POST["id_documento"]);
    $bodega->nombre = $_POST["nombre"];
    $bodega->descripcion = $_POST["descripcion"];
    $bodega->update();
    print "<script>window.location='index.php?view=sala_unidad_de_medida';</script>";
}


//Desactivar
else if ($action == 'actualizar') {
    $bodega = Unidad_de_medida::getById($_POST["id"]);
    if ($bodega->estado == 1) {
        $bodega->estado = 0;
    } else {
        $bodega->estado = 1;
    }
    $bodega->update_estado();
    print "<script>window.location='index.php?view=sala_unidad_de_medida';</script>";
}
//Delete
if ($action == 'delete') {

    $bodega = Unidad_de_medida::getById($_POST['id']);
    $bodega->del();

    //Core::redir("./index.php?view=m_documento");
}
