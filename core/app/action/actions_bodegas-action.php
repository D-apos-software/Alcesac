<?php

// El siguiente codigo se encarga del mantenimiento de bodegas
// Relacion: VISTA BODEGAS

$action = $_POST["id_estado"];

//Add
if ($action == 'add') {

    $bodega = new Bodegas();
    $bodega->codigo = $_POST["codigo"];
    $bodega->nombre = $_POST["nombre"];
    $bodega->cantidad_inicial = $_POST["cantidad_inicial"];
    $bodega->cantidad_minima = $_POST["cantidad_minima"];
    $bodega->cantidad_maxima = $_POST["cantidad_maxima"];
    $bodega->estado = 1;
    $bodega->add();
    print "<script>window.location='index.php?view=sala_bodegas';</script>";
}

//Update
else if ($action == 'update') {
    print('<br><br><br><br>');
    $bodega = Bodegas::getById($_POST["id_documento"]);
    $bodega->codigo = $_POST["codigo"];
    $bodega->nombre = $_POST["nombre"];
    $bodega->cantidad_inicial = $_POST["cantidad_inicial"];
    $bodega->cantidad_maxima = $_POST["cantidad_maxima"];
    $bodega->cantidad_minima = $_POST["cantidad_minima"];
    $bodega->estado = $_POST["estado"];
    $bodega->update();
    print "<script>window.location='index.php?view=sala_bodegas';</script>";
}


//Desactivar
else if ($action == 'desactivar') {
    print('<br><br><br><br>');
    $bodega = Bodegas::getById($_POST["id"]);
    $bodega->estado = 0;
    $bodega->update_estado();
    print "<script>window.location='index.php?view=sala_bodegas';</script>";
}
//Delete
if ($action == 'delete') {

    $bodega = Bodegas::getById($_POST['id']);
    $bodega->del();

    //Core::redir("./index.php?view=m_documento");
}
