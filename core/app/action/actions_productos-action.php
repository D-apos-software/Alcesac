<?php 
    // El siguiente codigo se encarga del mantenimiento de PRODUCTOS
    // Relacion: VISTA PRODUCTOS
    
    $action = $_POST["estado"];


    //ELIMINAR

    if ($action == 'eliminar'){
            $habi = ProductoData::getById($_POST["id"]);
            $habi->del();
            Core::redir("./index.php?view=sala_impuestos");
    }

    
    //Desactivar

    if ($action == 'desactivar'){
            $producto = ProductoData::getById($_POST["id"]);
            $producto->estado = 1; 
            $producto->update_estado();
            print "<script>window.location='index.php?view=sala_impuestos';</script>";
        }

    //Activar

        if ($action == 'activar'){
            $producto = ProductoData::getById($_POST["id"]);
            $producto->estado = 0; 
            $producto->update_estado();
            print "<script>window.location='index.php?view=sala_impuestos';</script>";
        }
