<?php 
// El siguiente codigo se encarga de cambiar el estado de las funcionalidades generales de alcesac (activar o desactivar funcionalidades)
// Relacion: VISTA FUNCIONALIDADES //http://localhost/functionalities_for_d_apos/?view=funcionalidades

$action = $_POST['id_estado'];

if ($action == 'actualizar') {
    $func = Funcionalidad::getById($_POST["id"]);
    if($func->estado == 0){
        $func->estado = 1;
    }else if($func->estado == 1){
        $func->estado = 0;
    }
    $func->update_estado();
    print "<script>window.location='index.php?view=sala_bodegas';</script>";
}

?>