<?php

// El siguiente codigo se encarga de exportar el listado mostrado en un documento excel
// Relacion: VISTA PRODUCTOS EXCEL //http://localhost/functionalities_for_d_apos/index.php?view=productos_excel

include '../model/ProductoData.php';
include '../model/Unidad_de_medida.php';
include '../model/PersonaData.php';
include '../model/Descuento_Producto.php';
include '../model/Bodegas.php';
include '../model/Tipo_r_impuestos_productos.php';
include '../action/action-functions-products.php';
include '../../../core/autoload.php';

?>
<?php
//$sql = mysqli_query($con, "SELECT * FROM producto");
$sql = ProductoData::getAll();

$docu = "Estado_de_Productos.xls";
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename=' . $docu);
header('Pragma: no-cache');
header('Expires: 0');
echo '<table border=1>';
echo '<tr>';
echo '<th colspan=4>Estado de los productos Actuales</th>';
echo '<tr>';
echo '<tr><th>Codigo</th><TH>Codigo de Barra</TH><TH>Nombre</TH><TH>Marca</TH><TH>Descripcion</TH><TH>Acreditable</TH><TH>Presentacion</TH>
<TH>Precio de Compra</TH><TH>Precio de Venta Antes de Impuestos y Utilidad</TH><TH>Stock</TH><TH>Aviso Stock Minimo</TH><TH>Aviso Stock Maximo</TH><TH>Utilidad</TH><TH>Proveedor</TH><TH>Bodega</TH>
<TH>Unidad de Medida</TH><TH>Descuento</TH><TH>Imagen</TH><TH>Emails</TH><TH>Estado</TH><TH>Impuestos</TH><TH>Valor de Venta</TH></tr>';
foreach ($sql as $producto) {
	echo '<tr>';
	echo '<td>' . $producto->codigo . '</td>';
	echo '<td>' . $producto->codigo_barra . '</td>';
	echo '<td>' . $producto->nombre . '</td>';
	echo '<td>' . $producto->marca . '</td>';
	echo '<td>' . $producto->descripcion . '</td>';
	if ($producto->acreditable == 0) {
		echo '<td>No</td>';
	} else {
		echo '<td>Si</td>';
	}
	echo '<td>' . $producto->presentacion . '</td>';
	echo '<td>' . $producto->precio_compra . '</td>';
	echo '<td>' . $producto->precio_venta . '</td>';

	//$Datos_stock = ProductoData::get_stock_totales($row['id']);
	$id = $producto->id;
	$Datos_stock = ProductoData::get_stock_totales($id);
	$stock = ($Datos_stock->stock + $Datos_stock->compra) - $Datos_stock->venta;
	echo '<td>' . $stock . '</td>';

	echo '<td>' . $producto->aviso_stock . '</td>';
	echo '<td>' . $producto->stock_max . '</td>';
	echo '<td>' . $producto->utilidad . '% </td>';

	if ($producto->id_proveedor != null && $producto->id_proveedor != "" && $producto->id_proveedor != 0) {
		$id_proveedor =	$producto->id_proveedor;
		$sql_persona = mysqli_query($con, "select * from persona where id=$id_proveedor");
		$Proveedor = mysqli_fetch_array($sql_persona);
		echo '<td>' . $Proveedor . '</td>';
	} else {
		echo '<td>No hay proveedor</td>';
	}


	if ($producto->id_bodega != null) {
		$id_bodega = $producto->id_bodega;
		$Bodega = Bodegas::getById($id_bodega);
		echo '<td>' . $Bodega->nombre . '</td>';
	} else {
		echo '<td>No hay bodega asignada</td>';
	}

	if ($producto->id_unidad_medida != null) {
		$id_unidad_medida =	$producto->id_unidad_medida;
		$Unidad_Medida = Unidad_de_medida::getById($id_unidad_medida);
		echo '<td>' . $Unidad_Medida->nombre . '</td>';
	} else {
		echo '<td>No hay una unidad de medida asignada</td>';
	}

	if ($producto->id_descuento != null) {
		$id_descuento =	$producto->id_descuento;
		$Descuento_P = Descuento_Producto::getById($id_descuento);
		echo '<td>' . $Descuento_P->nombre . '</td>';
	} else {
		echo '<td>No hay una descuento asignado</td>';
	}

	echo '<td>' . $producto->imagen . '</td>';
	echo '<td>' . $producto->count_mail . '</td>';
	if ($producto->estado == 0) {
		echo '<td>Activo</td>';
	} else {
		echo '<td>Inactivo</td>';
	}

	$Nombre_imp = Tipo_r_impuestos_productos::get_nombre_impuesto($producto->id);
	$impuestos = "Impuestos Aplicados: ";
	$count = 0;
	foreach ($Nombre_imp as $nm) {
		//$lista_impuestos += $nm->nombre;
		if ($count == 0) {
			$impuestos = $impuestos . " " . $nm->nombre;
		} else {
			$impuestos = $impuestos . "," . $nm->nombre;
		}
		$count += 1;
	}
	echo '<td>' . $impuestos . '</td>';

	$id_descuento =	$producto->id_descuento;
	$Ds_pro = Descuento_Producto::getById($id_descuento);
	$total_venta =  total_de_venta_excel($producto->precio_venta, $producto->utilidad, $Ds_pro->valor,  $producto->id);
	echo '<td>' . $total_venta . '</td>';
	echo '</tr>';
}
echo '</table>';

die();


?>