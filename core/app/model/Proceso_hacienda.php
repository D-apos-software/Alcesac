<?php
class Proceso_hacienda {

	public static $tablename = "datos_factura";


	public function Proceso_hacienda(){

        $this->id_cliente = ""; 
		$this->tipo_documento = "";
		$this->tipo_cedula = "";
		$this->fecha_emision = "NOW()";
        $this->emisor_nombre = "";
		$this->emisor_tipo_identif = "";
		$this->emisor_num_identif = ""; 
		$this->nombre_comercial = "";
		$this->emisor_provincia = "";
		$this->emisor_canton = "";
		$this->emisor_distrito = "";
		$this->emisor_barrio = "";
		$this->emisor_cod_pais_tel = "";
		$this->emisor_email = "";

		$this->receptor_nombre = "";
        $this->receptor_tipo_identif = "";
		$this->receptor_num_identif = "";
		$this->receptor_provincia = "";
		$this->receptor_canton = "";
		$this->receptor_distrito = "";
		$this->receptor_barrio = "";
		$this->receptor_cod_pais_tel = "";
		$this->receptor_tel = "";
        $this->receptor_email = "";

		$this->condicion_venta = "";
		$this->plazo_credito = "";
		$this->medio_pago = "";
		$this->cod_moneda = "";
		$this->tipo_cambio = "";
		$this->total_serv_gravados = "";
        $this->total_serv_exentos = "";
        $this->total_merc_gravada = "";
		$this->total_merc_exenta = "";
		$this->total_gravados = "";
		$this->total_exentos = "";

		$this->total_ventas ="";
		$this->total_descuentos = "";
		$this->total_ventas_neta = "";
		$this->total_impuesto = "";
		$this->total_comprobante = "";
		
		$this->token_llave = "";
		$this->id_caja = "";
		$this->id_usuario = "";
		$this->omitir_receptor = "";
		$this->estado = "0";

        
	} 


	public function add(){
		$sql = "insert into datos_factura (id_cliente,tipo_documento,tipo_cedula,fecha_emision,emisor_nombre,emisor_tipo_identif,emisor_num_identif,nombre_comercial,emisor_provincia,emisor_canton,emisor_distrito,emisor_barrio,emisor_cod_pais_tel,emisor_email,receptor_nombre,receptor_tipo_identif,receptor_num_identif,receptor_provincia,receptor_canton,receptor_distrito,receptor_barrio,receptor_cod_pais_tel,receptor_tel,receptor_email,condicion_venta,plazo_credito,medio_pago,cod_moneda,tipo_cambio,total_serv_gravados,total_serv_exentos,total_merc_gravada,total_merc_exenta,total_gravados,total_exentos,total_ventas,total_descuentos,total_ventas_neta,total_impuesto,total_comprobante,token_llave,id_caja,id_usuario,omitir_receptor,estado) ";
		$sql .= "value (
			$this->id_cliente,
			\"$this->tipo_documento\",
			\"$this->tipo_cedula\",
			NOW(),
			\"$this->emisor_nombre\",
			\"$this->emisor_tipo_identif\",
			\"$this->emisor_num_identif\",
			\"$this->nombre_comercial\",
			\"$this->emisor_provincia\",
			\"$this->emisor_canton\"
			,\"$this->emisor_distrito\"
			,\"$this->emisor_barrio\"
			,\"$this->emisor_cod_pais_tel\"
			,\"$this->emisor_email\"
			,\"$this->receptor_nombre\"
			,\"$this->receptor_tipo_identif\"
			,\"$this->receptor_num_identif\"
			,\"$this->receptor_provincia\"
			,\"$this->receptor_canton\"
			,\"$this->receptor_distrito\"
			,\"$this->receptor_barrio\"
			,\"$this->receptor_cod_pais_tel\"
			,\"$this->receptor_tel\"
			,\"$this->receptor_email\"
			,\"$this->condicion_venta\"
			,\"$this->plazo_credito\"
			,\"$this->medio_pago\"
			,\"$this->cod_moneda\"
			,\"$this->tipo_cambio\"
			,\"$this->total_serv_gravados\"
			,\"$this->total_serv_exentos\"
			,\"$this->total_merc_gravada\"
			,\"$this->total_merc_exenta\"
			,\"$this->total_gravados\"
			,\"$this->total_exentos\"
			,\"$this->total_ventas\"
			,\"$this->total_descuentos\"
			,\"$this->total_ventas_neta\"
			,\"$this->total_impuesto\"
			,\"$this->total_comprobante\"
			,\"$this->token_llave\"
			,\"$this->id_caja\"
			,\"$this->id_usuario\"
			,\"$this->omitir_receptor\"
			,\"$this->estado\")";
		return Executor::doit($sql);
	}


	public static function getAll(){
		$sql = "select * from ".self::$tablename." ORDER BY(fecha_emision) DESC";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Proceso_hacienda());
	}
}
