<?php
class PersonaData {
	public static $tablename = "persona";


	public function PersonaData(){
		$this->documento = ""; 
		$this->nombre = "";
		$this->email = "";
		$this->telefono = "";
		$this->telefono_sec = "";
		$this->fecha_nac = "";
		$this->razon_social = ""; 
		$this->direccion = "";
		$this->provincia = "";
		$this->canton = "";
		$this->barrio = "";
		$this->distrito = "";
		$this->exonerado = "0";
		$this->id_categoria_p = "0";
		$this->fecha_creada = "NOW()";
	} 

	public function getTipoDocumento(){ return TipoDocumentoData::getById($this->tipo_documento);}

	public function addCliente(){
		$sql = "insert into persona (tipo_documento,documento,giro,nombre,razon_social,fecha_nac,direccion,provincia,canton,barrio,distrito,tipo,fecha_creada,email,telefono,telefono_sec,exonerado,id_categoria_p) ";
		$sql .= "value ($this->tipo_documento,\"$this->documento\",\"$this->giro\",\"$this->razon_social\",\"$this->nombre\",\"$this->fecha_nac\",\"$this->direccion\",\"$this->provincia\",\"$this->canton\",\"$this->barrio\",\"$this->distrito\",1,$this->fecha_creada,\"$this->email\",\"$this->telefono\",\"$this->telefono_sec\",\"$this->exonerado\",\"$this->id_categoria_p\")";
		return Executor::doit($sql);
	}

       

	public function add(){
		$sql = "insert into persona (tipo_documento,documento,nombre,giro,razon_social,direccion,tipo,fecha_creada,ocupacion,medio_transporte,destino,motivo,telefono,email,exonerado,id_categoria_p) ";
		$sql .= "value ($this->tipo_documento,\"$this->documento\",\"$this->nombre\",\"$this->giro\",\"$this->razon_social\",\"$this->direccion\",1,$this->fecha_creada,\"$this->ocupacion\",\"$this->medio_transporte\",\"$this->destino\",\"$this->motivo\",\"$this->telefono\",\"$this->email\",\"$this->exonerado\",\"$this->id_categoria_p\")";
		return Executor::doit($sql);
	}

	public function add001(){
		$sql = "insert into persona (tipo_documento,documento,nombre,direccion,tipo,fecha_creada) ";
		$sql .= "value ($this->tipo_documento,\"$this->documento\",\"$this->nombre\",\"$this->direccion\",1,$this->fecha_creada)";
		return Executor::doit($sql);
	}

	public function addClienteVenta(){
		$sql = "insert into persona (tipo_documento,documento,nombre,tipo,fecha_creada,email,exonerado,id_categoria_p) ";
		$sql .= "value (\"$this->tipo_documento\",\"$this->documento\",\"$this->nombre\",3,NOW(),\"$this->email\",\"$this->exonerado\",\"$this->id_categoria_p\")";
		return Executor::doit($sql);
	}

	public function addProveedor(){
		$sql = "insert into persona (tipo_documento,documento,nombre,razon_social,direccion,tipo,fecha_creada) ";
		$sql .= "value ($this->tipo_documento,\"$this->documento\",\"$this->nombre\",\"$this->razon_social\",\"$this->direccion\",2,$this->fecha_creada)";
		return Executor::doit($sql); 
	}

	public static function delById($id){ 
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

// partiendo de que ya tenemos creado un objecto PersonaData previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",descripcion=\"$this->descripcion\",id_categoria=$this->id_categoria where id=$this->id";
		Executor::doit($sql); 
	}

	public function updateVip(){
		$sql = "update ".self::$tablename." set vip=$this->vip,limite=$this->limite where id=$this->id";
		Executor::doit($sql); 
	}

	public function updateContador(){
		$sql = "update ".self::$tablename." set contador=$this->contador where id=$this->id";
		Executor::doit($sql); 
	}

	public function updatecliente(){
		$sql = "update ".self::$tablename." set tipo_documento=$this->tipo_documento,documento=\"$this->documento\",giro=\"$this->giro\",telefono=\"$this->telefono\",telefono_sec=\"$this->telefono_sec\",email=\"$this->email\",nombre=\"$this->nombre\",razon_social=\"$this->razon_social\",fecha_nac=\"$this->fecha_nac\",direccion=\"$this->direccion\",provincia=\"$this->provincia\",canton=\"$this->canton\",barrio=\"$this->barrio\",distrito=\"$this->distrito\",exonerado=\"$this->exonerado\",id_categoria_p=\"$this->id_categoria_p\"  where id=$this->id";
		Executor::doit($sql);
	}

	public function updateProveedor(){
		$sql = "update ".self::$tablename." set tipo_documento=$this->tipo_documento,documento=\"$this->documento\",nombre=\"$this->nombre\",razon_social=\"$this->razon_social\",fecha_nac=\"$this->fecha_nac\",direccion=\"$this->direccion\" where id=$this->id";
		Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto PersonaData previamente utilizamos el contexto
	public function update_existente(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",documento=\"$this->documento\",tipo_documento=\"$this->tipo_documento\",exonerado=\"$this->exonerado\",email=\"$this->email\" where id=$this->id";
		Executor::doit($sql); 
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new PersonaData());

	}

	

	public static function getByDocumento($id){
		$sql = "select * from ".self::$tablename." where documento=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new PersonaData());
	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename." where tipo=1 ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PersonaData());
	}

	public static function getAll_ventas(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new PersonaData());
	}

	public static function getAll_clientes(){
		$sql = "SELECT id,tipo_documento,documento,nombre,razon_social,email,telefono,giro,direccion,count(documento) as total FROM `persona` WHERE tipo = 1 GROUP BY(documento)";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PersonaData());
	}
	public static function getAll_clientes_tipo_documento(){
		$sql = "SELECT id,tipo_documento,documento,nombre,razon_social,email,telefono,telefono_sec,giro,direccion,count(documento) as total, exonerado, id_categoria_p,provincia,canton,distrito,barrio FROM `persona` WHERE tipo = 1 or tipo = 3 GROUP BY tipo_documento,documento";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PersonaData());
	}

	public static function get_cantidad_reservas($id_cliente){
		$sql = "SELECT count(proceso.id_cliente) as total FROM `proceso` WHERE proceso.id_cliente = $id_cliente;";
		$query = Executor::doit($sql);
		return Model::one($query[0],new PersonaData());
	}

	public static function getProveedor(){
		$sql = "select * from ".self::$tablename." where tipo=2 ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PersonaData());
	}
 

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%' or documento like '%$q%'";
		$query = Executor::doit($sql);
		return Model::one($query[0],new PersonaData());

	}

	public static function getLikeDni($documento){
		$sql = "select * from ".self::$tablename." where documento=\"$documento\" ";
		$query = Executor::doit($sql);
		return Model::one($query[0],new PersonaData());

	}

	//Graficos estadisticos 

	public static function get_datos_generales(){
		$sql = "SELECT tipo_documento.nombre as documento,persona.documento as id,persona.nombre as cliente,persona.email,
		persona.telefono,
        if(persona.id = proceso.id_cliente, count(documento) , '0') as catidad_hospedajes,
        count(documento) as total,
		sum(proceso.precio + proceso.dinero_dejado) as ganancia_recepcion,
		if(proceso_venta.precio = 0, '0', sum(proceso_venta.precio)) as ganancia_ventas
		FROM `persona` 
		LEFT JOIN proceso ON proceso.id_cliente = persona.id
		INNER JOIN tipo_documento ON tipo_documento.id = persona.tipo_documento
		LEFT OUTER JOIN proceso_venta ON proceso_venta.id_operacion = proceso.id
		WHERE tipo = 1  GROUP BY  persona.tipo_documento,persona.documento";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PersonaData());
	}
	
	public static function get_datos_generales_externos(){
		$sql = "SELECT tipo_documento.nombre as documento,persona.documento as id,persona.nombre as cliente,persona.email, persona.telefono,
		 if(persona.id = proceso.id_cliente, count(documento) , '0') as catidad_hospedajes, count(documento) as total, sum(proceso.precio + proceso.dinero_dejado) as ganancia_recepcion,
		 if(venta.total = 0, '0', sum(venta.total)) as ganancia_ventas 
		 FROM `persona` 
		 LEFT JOIN proceso ON proceso.id_cliente = persona.id 
		 INNER JOIN tipo_documento ON tipo_documento.id = persona.tipo_documento 
		 LEFT OUTER JOIN proceso_venta ON proceso_venta.id_operacion = proceso.id 
		 INNER JOIN venta ON venta.id_proveedor = persona.id WHERE tipo = 3 GROUP BY persona.documento;";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PersonaData());
	}

	public static function get_search($seudonimo){
		$sql = "SELECT * FROM persona WHERE nombre LIKE '$seudonimo' LIMIT 1";
		$query = Executor::doit($sql);
		//echo json_encode($equipo);
		return Model::many($query[0],new PersonaData());
	}
	public static function get_search_id($seudonimo){
		$sql = "SELECT * FROM persona WHERE nombre LIKE '$seudonimo' LIMIT 1";
		$query = Executor::doit($sql);
		//echo json_encode($equipo);
		return Model::many($query[0],new PersonaData());
	}
	public static function get_name(){
		$sql = "SELECT nombre,id FROM persona";
		$query = Executor::doit($sql);
		//echo json_encode($equipo);
		return Model::many($query[0],new PersonaData());
	}
	public static function get_documento(){
		$sql = "SELECT documento,id FROM persona";
		$query = Executor::doit($sql);
		//echo json_encode($equipo);
		return Model::many($query[0],new PersonaData());
	}

	public static function get_documento_data($documento){
		$sql = "SELECT * FROM persona WHERE documento LIKE '$documento'";
		$query = Executor::doit($sql);
		//echo json_encode($equipo);
		return Model::one($query[0],new PersonaData());
	}

}

?>