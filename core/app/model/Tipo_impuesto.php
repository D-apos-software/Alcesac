<?php
class Tipo_impuesto {
	public static $tablename = "impuestos";

	public function Tipo_impuesto(){
		$this->nombre = "";
		$this->valor = "0";
		$this->compras = "0";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into impuestos (nombre,valor,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",1,\"$this->fecha_creada\")";
		Executor::doit($sql);
	}
	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set compras=$this->compras, valor=$this->valor where id=$this->id";
		Executor::doit($sql);
	}
	public function update_inactive(){
		$sql = "update ".self::$tablename." set valor=$this->valor where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_impuesto());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_impuesto());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_impuesto());

	}


}

?>