<?php
class Tipo_r_promociones {
	public static $tablename = "r_promociones";

	public function Tipo_r_promociones(){
		$this->id_producto = "0";
		$this->id_habitacion = "0";
		$this->id_categoria = "0";
		$this->id_porcentaje = "0";
	}

	public function add(){
		$sql = "insert into r_promociones (id_producto,id_habitacion,id_categoria,id_porcentaje) ";
		$sql .= "value ($this->id_producto,$this->id_habitacion,$this->id_categoria,$this->id_porcentaje)";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}
	public function del_promo(){
		$sql = "delete from ".self::$tablename." where id_porcentaje = $this->id_porcentaje";
		Executor::doit($sql);
	}
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_promociones());

	}
	public static function getByID_Promo($id_porcentaje){
		$sql = "select * from ".self::$tablename." where id_porcentaje=$id_porcentaje";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_promociones());

	}

	public static function getByID_h($id_porcentaje){
		$sql = "select * from ".self::$tablename." where id_porcentaje=$id_porcentaje";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_promociones());

	}
	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_promociones());
	}

	public static function getby_idP($id_producto,$id_categoria_p){
		$sql = "SELECT promociones.valor FROM promociones 
		INNER JOIN r_promociones as rela on rela.id_porcentaje = promociones.id
		WHERE rela.id_categoria = $id_categoria_p AND rela.id_producto = $id_producto";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_promociones());
	}
	public static function getby_idH($id_habitacion,$id_categoria_p){
		$sql = "SELECT promociones.valor FROM promociones 
		INNER JOIN r_promociones as rela on rela.id_porcentaje = promociones.id
		WHERE rela.id_categoria = $id_categoria_p AND rela.id_habitacion = $id_habitacion";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_promociones());
	}
	public static function get_Pr_Cat($id_producto, $cat){
		$sql = "SELECT id FROM r_promociones WHERE r_promociones.id_producto = $id_producto AND r_promociones.id_categoria = $cat;";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_promociones());
	}
	public static function get_Hb_Cat($id_habitacion, $cat){
		$sql = "SELECT id FROM r_promociones WHERE r_promociones.id_habitacion = $id_habitacion AND r_promociones.id_categoria = $cat;";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_promociones());
	}
}

?>