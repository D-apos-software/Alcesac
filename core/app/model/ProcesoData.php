<?php
class ProcesoData {
	public static $tablename = "proceso";


	public function ProcesoData(){
		$this->dinero_dejado = ""; 
		$this->fecha_entrada = "";
		$this->fecha_salida = "";
		$this->cant_personas = "";
		$this->fecha_creada = "NOW()";
		$this->nro_operacion = "";
		$this->num_tarjeta = "";
		$this->banco = "";
		$this->id_cliente = "0";
		$this->num_aprobacion = "";
	} 

	public function getHabitacion(){ return HabitacionData::getById($this->id_habitacion);}
	public function getTarifaHabitacion(){ return TarifaHabitacionData::getById($this->id_tarifa);}
	public function getCliente(){ return PersonaData::getById($this->id_cliente);}
	public function getUsuario(){ return UserData::getById($this->id_usuario);}

	public function addIngreso(){
		$sql = "insert into proceso (tipo_proceso,id_habitacion,id_tarifa,id_cliente,id_apertura,precio,cant_noche,dinero_dejado,id_tipo_pago,fecha_entrada,fecha_salida,id_usuario,cant_personas,id_caja,estado,fecha_creada,cantidad,pagado,nro_operacion,num_tarjeta,banco,num_aprobacion) ";
		$sql .= "value (\"$this->tipo_proceso\",\"$this->id_habitacion\",\"$this->id_tarifa\",\"$this->id_cliente\",\"$this->id_apertura\",\"$this->precio\",\"$this->cant_noche\",\"$this->dinero_dejado\",\"$this->id_tipo_pago\",\"$this->fecha_entrada\",\"$this->fecha_salida\",$this->id_usuario,\"$this->cant_personas\",\"$this->id_caja\",0,$this->fecha_creada,$this->cantidad,\"$this->pagado\",$this->nro_operacion,$this->num_tarjeta,$this->banco,$this->num_aprobacion)";
		return Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto PersonaData previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",descripcion=\"$this->descripcion\",id_categoria=$this->id_categoria where id=$this->id";
		Executor::doit($sql);
	}

	public function updateHabitacionTarifa(){
		$sql = "update ".self::$tablename." set id_habitacion=$this->id_habitacion,id_tarifa=$this->id_tarifa,precio=\"$this->precio\"  where id=$this->id";
		Executor::doit($sql);
	} 

	public function updateSalida(){
		$sql = "update ".self::$tablename." set total=\"$this->total\", id_tipo_pago=$this->id_tipo_pago,estado=1 where id=$this->id";
		Executor::doit($sql);
	}


	public function update_DatosSalida_pago(){
		//$sql = "update ".self::$tablename." set total=\"$this->total\", id_tipo_pago=$this->id_tipo_pago, id_apertura=$this->id_apertura,pagado=1 where id=$this->id";
		$sql = "update ".self::$tablename." set precio=$this->precio, estado=$this->estado, id_tipo_pago=$this->id_tipo_pago,id_apertura=$this->id_apertura,cobro_ext=$this->cobro_ext,pagado=1 where id=$this->id";
		Executor::doit($sql);
	}
	public function update_DatosSalida_pendiente(){
		//$sql = "update ".self::$tablename." set total=\"$this->total\", id_tipo_pago=$this->id_tipo_pago, id_apertura=$this->id_apertura,pagado=1 where id=$this->id";
		$sql = "update ".self::$tablename." set num_tarjeta=$this->num_tarjeta,banco=$this->banco,num_aprobacion=$this->num_aprobacion,nro_operacion=$this->nro_operacion,precio=$this->precio, estado=$this->estado, id_tipo_pago=$this->id_tipo_pago,id_apertura=$this->id_apertura, id_caja=$this->id_caja,cobro_ext=$this->cobro_ext,pagado=1 where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ProcesoData());

	}

	public static function getByRecepcion($id){
		$sql = "select * from ".self::$tablename." where id_habitacion=$id and estado=0";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ProcesoData());

	}


	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where id_habitacion like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	}
	public static function getMontoExtra($id_apertura){
		$sql = "select * from ".self::$tablename." where tipo_proceso = 1 and id_caja !=$id_apertura and id_apertura=$id_apertura";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	} 
	public static function getProceso(){
		$sql = "select * from ".self::$tablename." where estado=0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	} 

	public static function getProcesoHabitacion($id_habitacion){
		$sql = "select * from ".self::$tablename." where id_habitacion=$id_habitacion";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	} 

	public static function getProcesoCliente($q){
		$sql = "select * from ".self::$tablename." where estado=0 and id_cliente=$q ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData()); 

	}

	public static function getProcesoIDCliente($q){
		$sql = "SELECT * FROM `proceso` Inner JOIN persona ON persona.id = proceso.id_cliente WHERE persona.documento = $q ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData()); 

	}

	public static function getReporteDiario($hoy){
		$sql = "select * from ".self::$tablename." where (date(fecha_entrada) = \"$hoy\" or date(fecha_salida) = \"$hoy\") and pagado=1 ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	}

	public static function getReporteDiarioUser($hoy,$user){
		$sql = "select * from ".self::$tablename." where (date(fecha_entrada) = \"$hoy\" or date(fecha_salida) = \"$hoy\") and id_usuario=$user and pagado=1 ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	}

	public static function getIngresoCaja($id_caja){
		$sql = "select * from ".self::$tablename." where id_caja=$id_caja and pagado=1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());
	}

	public static function getIngresoRangoFechas($start,$end){
		$sql = "select * from ".self::$tablename." where  date(fecha_entrada) >= \"$start\" and date(fecha_entrada) <= \"$end\" and pagado=1 ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());
	}


	

	public static function getReporteMensualUser($anio,$mes,$user){
		$sql = "select * from ".self::$tablename." where ( year(fecha_entrada) = $anio or year(fecha_salida) = $anio) and (month(fecha_entrada) = $mes or month(fecha_salida) = $mes) and id_usuario=$user ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	}

	public static function getReporteMensual($anio,$mes){
		$sql = "select * from ".self::$tablename." where ( year(fecha_entrada) = $anio or year(fecha_salida) = $anio) and (month(fecha_entrada) = $mes or month(fecha_salida) = $mes)  ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	}

	public static function getReporteMensual11($dia,$mes){
		$sql = "select * from ".self::$tablename." where day(fecha_entrada) = \"$dia\"  and month(fecha_entrada) = \"$mes\" ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	}

	
	public static function getFechaAños(){
		$sql = "SELECT DISTINCTROW YEAR(NOW()) FROM `gasto`";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());

	}
	
	public static function getFechaDia_mesActual($opcion){
		if($opcion == 1){
			$sql = "SELECT sum(total) as ganancia, Day(fecha_salida) as fecha, COUNT(fecha_salida) as cantidad FROM `proceso` WHERE year(fecha_salida) = year(CURRENT_DATE()) and MONTH(fecha_salida) = MONTH(CURRENT_DATE()) GROUP BY(Day(fecha_salida))";
			$query = Executor::doit($sql);
			return Model::many($query[0],new ProcesoData());
			// $sql = "select * from ".self::$tablename." where date(fecha_creada) >= \"$start\" and date(fecha_creada) <= \"$end\" and date(fecha_creada) != 'NULL' and tipo_operacion=2 ";
		}

	}
	public static function get_medio_pago(){
		
		$sql = "SELECT COUNT(proceso.id_tipo_pago) as cantidad,
		tipo_pago.nombre as nombre
		FROM `proceso`
		INNER JOIN tipo_pago ON tipo_pago.id = proceso.id_tipo_pago
		WHERE proceso.pagado = 1
		GROUP BY proceso.id_tipo_pago";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());
	
	} 

	public static function get_ganancia_productos($fecha_ini,$fecha_fin,$producto1){
		$sql = "SELECT producto.id, producto.nombre as nombre,
		 sum(venta.nro_comprobante) as ganancia,
		 DAY(venta.fecha_creada) as fecha, sum(proceso_venta.cantidad) as cantidad
		 FROM `venta` INNER JOIN proceso_venta ON proceso_venta.id_venta = venta.id INNER JOIN producto ON producto.id = proceso_venta.id_producto 
		 WHERE date(venta.fecha_creada) >= \"$fecha_ini\" and date(venta.fecha_creada) <= \"$fecha_fin\" and 
		producto.id = $producto1 GROUP BY producto.id,day(date(venta.fecha_creada)) ORDER BY fecha";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());
	} 
	
	public static function get_generales($fecha_ini,$fecha_fin){
		$sql = "SELECT (SELECT sum(v.nro_comprobante)as monto_total_ventas FROM venta as v WHERE v.tipo_operacion = 1 and date(v.fecha_creada) >= \"$fecha_ini\"  and date(v.fecha_creada) <= \"$fecha_fin\") as ventas,
		(SELECT sum(p.precio)as recepcion FROM proceso as p WHERE p.estado = 1 and date(p.fecha_creada) >= \"$fecha_ini\"  and date(p.fecha_creada) <= \"$fecha_fin\") as recepcion,
		(SELECT sum(e.precio)as egreso FROM gasto as e Where date(e.fecha_creacion) >= \"$fecha_ini\" and date(e.fecha_creacion) <= \"$fecha_fin\") as egreso,
		(SELECT sum(c.nro_comprobante)as monto_total_ventas FROM venta as c WHERE c.tipo_operacion = 0 and date(c.fecha_creada) >= \"$fecha_ini\"  and date(c.fecha_creada) <= \"$fecha_fin\") as compra,
		sum(proceso.dinero_dejado) as dinero_dejado 
		FROM proceso WHERE date(proceso.fecha_creada) >= \"$fecha_ini\" and date(proceso.fecha_creada) <= \"$fecha_fin\"";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProcesoData());
	
	} 
	
}
?>