<?php
class Tipo_r_utilidades_productos {
	public static $tablename = "r_utilidades_productos";

	public function Tipo_r_utilidades_productos(){
		$this->id_utilidad = "";
		$this->id_producto = "";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into r_utilidades_productos (id_utilidad,id_producto,fecha_creada) ";
		$sql .= "value (\"$this->id_utilidad\",\"$this->id_producto\",NOW())";
		Executor::doit($sql);
	}
	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}
	public function del_by_id_producto(){
		$sql = "delete from ".self::$tablename." where id_producto=$this->id_producto";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set id_utilidad=\"$this->id_utilidad\",id_producto=\"$this->id_producto\" where id=$this->id";
		Executor::doit($sql);
	}
	public function update_inactive(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_utilidades_productos());
	}
	public static function getutilidad($id){
		$sql = "select id_utilidad from ".self::$tablename." where id_producto=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_utilidades_productos());

	}
	public static function getutilidad2($id){
		$sql = "select * from ".self::$tablename." where id_producto=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_utilidades_productos());

	}

	public static function delete($id){
		$sql = "delete from ".self::$tablename." where id_producto = $id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_utilidades_productos());
	}
	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_utilidades_productos());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_utilidades_productos());

	}


}

?>