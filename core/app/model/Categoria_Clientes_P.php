<?php
class Categoria_Clientes_P {
	public static $tablename = "categoria_clientes_p";


	public function Categoria_Clientes_P(){
		$this->nombre = ""; 
		$this->fecha = "NOW()"; 
	} 

	public function add(){
		$sql = "insert into categoria_clientes_p (nombre,fecha) ";
		$sql .= "value (\"$this->nombre\",NOW())";
		return Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}


	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\" where id=$this->id";
		Executor::doit($sql);
	}
	public function update_e(){
		$sql = "update ".self::$tablename." set estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Categoria_Clientes_P());

	}


	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Categoria_Clientes_P());
	}

	public static function getActivos(){
		$sql = "select * from ".self::$tablename.' where estado = 1';
		$query = Executor::doit($sql);
		return Model::many($query[0],new Categoria_Clientes_P());
	}
	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombres like '%$q%' or apellidos like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Categoria_Clientes_P());

	}


}

?>