<?php
class Estado_de_pago {
	public static $tablename = "estado_de_pago";



	public function Estado_de_pago(){
		$this->nombre = "";
		$this->medio_pago = "";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into estado_de_pago (nombre,medio_pago,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",$this->medio_pago,NOW())";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

// partiendo de que ya tenemos creado un objecto Estado_de_pago previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",medio_pago=\"$this->medio_pago\" where id=$this->id";
		Executor::doit($sql);
	}

	

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Estado_de_pago());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Estado_de_pago());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Estado_de_pago());

	}


}

?>