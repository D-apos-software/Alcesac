<?php
class Utilidad_Productos {
	public static $tablename = "utilidad_producto";

	public function Utilidad_Productos(){
		$this->nombre = "";
		$this->valor = "";
		$this->estado="0";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into utilidad_producto (nombre,valor,estado,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",\"$this->valor\",$this->estado,NOW())";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}

	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

    // partiendo de que ya tenemos creado un objecto Tipo categoria venta previamente utilizamos el contexto
	public function update(){
		$sql = "update utilidad_producto set nombre=\"$this->nombre\",valor=\"$this->valor\",estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}
	public function update_estado(){
		$sql = "update utilidad_producto set estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Utilidad_Productos());
	}

	public static function getByValor($valor)
	{
		$sql = "select * from " . self::$tablename . " where valor='$valor'";
		$query = Executor::doit($sql);
		return Model::one($query[0], new Utilidad_Productos());
	}
	
	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Utilidad_Productos());
	}

	public static function get_active(){
		$sql = "select * from ".self::$tablename ." WHERE estado=1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Utilidad_Productos());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Utilidad_Productos());

	}

}
