<?php
class Detalle_producto_hac {

	public static $tablename = "detalles_producto_factura";


	public function Detalle_producto_hac(){

        $this->codigo = 0; 
		$this->cantidad = 0;
		$this->unidad_medida = "";
		$this->detalle = "";
        $this->precio_unitario = 0;
		$this->monto_total = 0;
		$this->subtotal = 0; 
		$this->monto_total_lineal = 0;
		$this->d_monto_descuento = "";
		$this->d_naturaleza_descuento = "";
		$this->imp_codigo = "";
		$this->imp_codigo_Tarifa = "";
		$this->imp_tarifa = "";
		$this->mp_monto = "";
		$this->id_fact_hacienda = "";

	} 


	public function add(){
		$sql = "insert into detalles_producto_factura (codigo,cantidad,unidad_medida,detalle,precio_unitario,monto_total,subtotal,monto_total_lineal,d_monto_descuento,d_naturaleza_descuento,imp_codigo,imp_codigo_Tarifa,imp_tarifa,mp_monto,id_fact_hacienda)";
		$sql .= "value ($this->codigo,\"$this->cantidad\",\"$this->unidad_medida\",\"$this->detalle\",\"$this->precio_unitario\",\"$this->monto_total\",\"$this->subtotal\",\"$this->monto_total_lineal\",\"$this->d_monto_descuento\",\"$this->d_naturaleza_descuento\",\"$this->imp_codigo\",\"$this->imp_codigo_Tarifa\",\"$this->imp_tarifa\",\"$this->mp_monto\",\"$this->id_fact_hacienda\")";
		return Executor::doit($sql);
	}


	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id_fact_hacienda=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Detalle_producto_hac());

	}

}
