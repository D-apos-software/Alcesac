<?php
class TipoCliente {
	public static $tablename = "tipo_cliente";

	public function TipoCliente(){
		$this->nombre = "";
		$this->valor_extra="";
		$this->estado="0";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into tipo_cliente (nombre,valor_extra,estado,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",$this->valor_extra,0,$this->fecha_creada)";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

// partiendo de que ya tenemos creado un objecto TipoCliente previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",valor_extra=\"$this->valor_extra\" where id=$this->id";
		Executor::doit($sql);
	}

	public function cambiar_estado(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new TipoCliente());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new TipoCliente());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new TipoCliente());

	}


}

?>