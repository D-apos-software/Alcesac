<?php
class TipoPago {
	public static $tablename = "tipo_pago";

	public function TipoPago(){
		$this->nombre = "";
		$this->valor = "";
		$this->valor_hacienda = "";
		$this->banco = "";
		$this->numero_aprobacion = "";
		$this->numero_tarjeta = "";
		$this->estado = "";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into tipo_pago (nombre,valor,banco,numero_aprobacion,numero_tarjeta,valor_hacienda,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",$this->valor,$this->banco,$this->numero_aprobacion,$this->numero_tarjeta,$this->valor_hacienda,$this->fecha_creada)";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",valor=\"$this->valor\",banco=\"$this->banco\",numero_aprobacion=\"$this->numero_aprobacion\",numero_tarjeta=\"$this->numero_tarjeta\",valor_hacienda=\"$this->valor_hacienda\" where id=$this->id";
		Executor::doit($sql);
	}

	
	public function update_estado(){
		$sql = "update ".self::$tablename." set estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new TipoPago());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new TipoPago());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new TipoPago());

	}


}

?>