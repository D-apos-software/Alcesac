<?php
class Descuento_Producto {
	public static $tablename = "descuento_productos";

	public function Descuento_Producto(){
		$this->nombre = "";
		$this->valor = "0";
		$this->estado = "0";
		$this->fecha_creada = "NOW()";
	}
	//public function getname_tipo_impuesto(){ return Tipo_subimpuesto_producto::getById($this->id_tipo_impuesto);}

	public function add(){
		$sql = "insert into descuento_productos (nombre,valor,estado,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",$this->valor,1,NOW())";
		Executor::doit($sql);
	}
	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\", valor=$this->valor, estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	public function update_inactive(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Descuento_Producto());

	}

	public static function getByValor($valor)
	{
		$sql = "select * from " . self::$tablename . " where valor='$valor'";
		$query = Executor::doit($sql);
		return Model::one($query[0], new Descuento_Producto());
	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Descuento_Producto());
	}
	public static function get_active(){
		$sql = "select * from ".self::$tablename." WHERE estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Descuento_Producto());
	}
	public static function getAll_active(){
		$sql = "select * from ".self::$tablename." where estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Descuento_Producto());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Descuento_Producto());

	}


}
