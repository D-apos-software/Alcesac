<?php
class ProductoData
{
	public static $tablename = "producto";


	public function ProductoData()
	{
		$this->codigo = "";
		$this->codigo_barra = "";
		$this->nombre = "";
		$this->marca = "";
		$this->descripcion = "";
		$this->acreditable = "";
		$this->precio_compra = "";
		$this->precio_venta = "";
		$this->aviso_stock = "";
		$this->stock_max = "";
		$this->utilidad = "0";
		$this->id_proveedor = "0";
		$this->id_bodega = "";
		$this->id_unidad_medida = "";
		$this->id_descuento = "";
		$this->imagen = "";
		$this->count_mail = "0";
		$this->estado = "0";
		$this->fecha_creada = "NOW()";
	}

	public function getProveedor()
	{
		return PersonaData::getById($this->id_proveedor);
	}
	public function getDescuento()
	{
		return Descuento_Producto::getById($this->id_descuento);
	}
	

	public function add()
	{
		$sql = "insert into producto (codigo,codigo_barra,nombre,marca,presentacion,descripcion,acreditable,precio_compra,precio_venta,stock,aviso_stock,stock_max,utilidad,id_proveedor,id_bodega,id_unidad_medida,id_descuento,imagen,fecha_creada)";
		$sql .= "VALUES (\"$this->codigo\",\"$this->codigo_barra\",\"$this->nombre\",\"$this->marca\",\"$this->presentacion\",\"$this->descripcion\",\"$this->acreditable\",
		\"$this->precio_compra\",\"$this->precio_venta\",\"$this->stock\",\"$this->aviso_stock\",\"$this->stock_max\",\"$this->utilidad\",\"$this->id_proveedor\",
		\"$this->id_bodega\",\"$this->id_unidad_medida\",\"$this->id_descuento\",\"$this->imagen\",NOW())";
		return Executor::doit($sql);
	}

	public static function delById($id)
	{
		$sql = "delete from " . self::$tablename . " where id=$id";
		Executor::doit($sql);
	}
	public function del()
	{
		$sql = "delete from " . self::$tablename . " where id=$this->id";
		Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto ProductoData previamente utilizamos el contexto
	public function update()
	{
		$sql = "update " . self::$tablename . " set codigo=\"$this->codigo\",codigo_barra=\"$this->codigo_barra\",
		nombre=\"$this->nombre\",marca=\"$this->marca\",presentacion=\"$this->presentacion\",
		descripcion=\"$this->descripcion\",acreditable=\"$this->acreditable\",precio_compra=\"$this->precio_compra\",
		precio_venta=\"$this->precio_venta\",aviso_stock=\"$this->aviso_stock\",stock_max=\"$this->stock_max\",utilidad=\"$this->utilidad\",
		id_proveedor=\"$this->id_proveedor\",id_bodega=$this->id_bodega,
		id_unidad_medida=$this->id_unidad_medida, id_descuento=$this->id_descuento,imagen=\"$this->imagen\" where id=$this->id";
		Executor::doit($sql);
	}

	public function update_aviso()
	{
		$sql = "update " . self::$tablename . " set count_mail=\"$this->count_mail\" where id=$this->id";
		Executor::doit($sql);
	}

	public function update_estado()
	{
		$sql = "update " . self::$tablename . " set estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new ProductoData());
	}

	public static function getActivos(){ 
		$sql = "select * from ".self::$tablename." WHERE estado = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new ProductoData());
	}
	public static function getAll()
	{
		$sql = "select * from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0], new ProductoData());
	}
	public static function get_ganancias_N_B()
	{
		$sql = "select id,nombre,codigo,sum(precio_venta) as neta,count(precio_compra) as bruta from " . self::$tablename." GROUP BY(producto.id)";
		$query = Executor::doit($sql);
		return Model::many($query[0], new ProductoData());
	}
	public static function getAll_stocks_bajos()
	{
		$sql = "SELECT * FROM `producto` WHERE stock < aviso_stock";
		$query = Executor::doit($sql);
		return Model::many($query[0], new ProductoData());
	}

	public static function getLike($q)
	{
		$sql = "select * from " . self::$tablename . " where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0], new ProductoData());
	}

	public function get_stock_bodega($id)
	{
		$sql = "SELECT SUM(producto.stock) as cantidad_stock, bodegas.cantidad_maxima FROM `producto` INNER JOIN bodegas ON bodegas.id = producto.id_bodega WHERE bodegas.cantidad_maxima > producto.stock AND producto.id_bodega = $id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new ProductoData());
	}
	public function get_stock_producto_venta($id){
		$sql = "SELECT aviso_stock,producto.nombre,
		(SELECT stock - sum(proceso_venta.cantidad) as total FROM proceso_venta INNER JOIN producto p1 on p1.id = proceso_venta.id_producto WHERE proceso_venta.tipo_operacion = 1  AND p1.id = $id GROUP BY(proceso_venta.id_producto)) as venta,
		(SELECT sum(proceso_venta.cantidad) as total FROM proceso_venta INNER JOIN producto p1 on p1.id = proceso_venta.id_producto WHERE proceso_venta.tipo_operacion = 2  AND p1.id = $id GROUP BY(proceso_venta.id_producto)) as compra 
		FROM producto WHERE producto.id = $id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new ProductoData());
	}
	public function get_stock_totales($id){
		$sql = "SELECT producto.nombre,producto.stock as stock,
		(SELECT sum(proceso_venta.cantidad) as total FROM proceso_venta INNER JOIN producto p1 on p1.id = proceso_venta.id_producto WHERE proceso_venta.tipo_operacion = 1 AND p1.id = $id GROUP BY(proceso_venta.id_producto)) as venta,
		(SELECT sum(proceso_venta.cantidad) as total FROM proceso_venta INNER JOIN producto p1 on p1.id = proceso_venta.id_producto WHERE proceso_venta.tipo_operacion = 2 AND p1.id = $id GROUP BY(proceso_venta.id_producto)) as compra
		 FROM producto WHERE producto.id = $id;";
		$query = Executor::doit($sql);
		return Model::one($query[0], new ProductoData());
	}

	
}
