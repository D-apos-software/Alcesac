<?php
class Tipo_subimpuesto_producto
{

	public static $tablename = "subimpuestos_productos";

	public function Tipo_subimpuesto_producto()
	{
		$this->nombre = "";
		$this->valor = "0";
		$this->estado = "0";
		$this->fecha_creada = "NOW()";
	}
	public function add()
	{
		$sql = "insert into subimpuestos_productos (nombre,valor,estado,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",\"$this->valor\",1,NOW())";
		Executor::doit($sql);
	}
	public static function delById($id)
	{
		$sql = "delete from " . self::$tablename . " where id=$id";
		Executor::doit($sql);
	}
	public function del()
	{
		$sql = "delete from " . self::$tablename . " where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\", valor=$this->valor, estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	public function update_inactive()
	{
		$sql = "update " . self::$tablename . " set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new Tipo_subimpuesto_producto());
	}
	public static function getAll()
	{
		$sql = "select * from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0], new Tipo_subimpuesto_producto());
	}
	public static function getAll_active()
	{
		$sql = "SELECT * FROM `subimpuestos_productos` WHERE subimpuestos_productos.estado = 1;";
		$query = Executor::doit($sql);
		return Model::many($query[0], new Tipo_subimpuesto_producto());
	}
	public static function getLike($q)
	{
		$sql = "select * from " . self::$tablename . " where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0], new Tipo_subimpuesto_producto());
	}
}
