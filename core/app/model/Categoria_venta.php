<?php
class Categoria_venta {

	public static $tablename = "categoria_venta";

	public function Categoria_venta(){
		$this->nombre = "";
		$this->codigo = "";
		$this->id_tipo_categoria = "0";
		$this->estado="0";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into categoria_venta (nombre,codigo,id_tipo_categoria,estado,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",\"$this->codigo\",$this->id_tipo_categoria,$this->estado,NOW())";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

    // partiendo de que ya tenemos creado un objecto categoria venta previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",codigo=\"$this->codigo\",id_tipo_categoria=\"$this->id_tipo_categoria\",estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Categoria_venta());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Categoria_venta());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Categoria_venta());

	}


}

?>