<?php
class Tipo_r_plazo_venta {
	public static $tablename = "r_plazo_venta";

	public function Tipo_r_plazo_venta(){
		$this->id_venta = "";
		$this->id_plazo = "";
		$this->estado = "0";
		$this->fecha_creada = "NOW()";
        $this->fecha_act = "NOW()";
	}
	//public function getname_tipo_impuesto(){ return Tipo_subimpuesto_producto::getById($this->id_tipo_impuesto);}

	public function add(){
		$sql = "insert into r_plazo_venta (id_venta,id_plazo,estado,fecha_creada,fecha_act) ";
		$sql .= "value (\"$this->id_venta\",\"$this->id_plazo\",$this->estado,NOW(),NOW())";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}

	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set id_venta=\"$this->venta\", id_plazo=\"$this->id_plazo\" where id=$this->id";
		Executor::doit($sql);
	}

	public function update_incomplete(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_plazo_venta());

	}

	public static function getByEstado($estado){
		$sql = "select * from ".self::$tablename." where estado=$estado";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_plazo_venta());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_plazo_venta());
	}

	public static function get_active(){
		$sql = "select * from ".self::$tablename." WHERE estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_plazo_venta());
	}

	public static function getAll_active(){
		$sql = "select * from ".self::$tablename." where estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_plazo_venta());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_plazo_venta());

	}

}
