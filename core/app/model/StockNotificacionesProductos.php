<?php
class StockNotificacionesProductos {
	public static $tablename = "notificaciones_stocks";

	public function StockNotificacionesProductos(){
		$this->valor = "";
		$this->estado = 0;
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into notificaciones_stocks (valor,fecha_creada) ";
		$sql .= "value (\"$this->valor\",$this->fecha_creada)";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

// partiendo de que ya tenemos creado un objecto StockNotificacionesProductos previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set valor=\"$this->valor\", estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}

	

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new StockNotificacionesProductos());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new StockNotificacionesProductos());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where valor like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new StockNotificacionesProductos());

	}

	public static function estado_inactivos(){
		$sql = "SELECT * FROM `notificaciones_stocks`  WHERE estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new StockNotificacionesProductos());

	}
	public static function estado_activos(){
		$sql = "SELECT * FROM `notificaciones_stocks`  WHERE estado = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new StockNotificacionesProductos());

	}

}

?>