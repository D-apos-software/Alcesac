<?php
class CajasAperturas {
	public static $tablename = "aperturas_cajas";

	public function CajasAperturas(){

		$this->id_caja = "";
        $this->fecha_apertura = "";
        $this->fecha_cierre = "";
        $this->monto_apertura = "";
        $this->monto_cierre = "";
		$this->fecha_creada = "NOW()"; 
	} 

	public function getUsuario(){ return UserData::getById($this->id_usuario);}
	public function getNombre_Caja(){ return CajasMData::getById($this->id_caja);}

	public function add(){
		$sql = "insert into ".self::$tablename." (id_caja,fecha_apertura,monto_apertura,estado,id_usuario,fecha_creada) ";
		$sql .= "value (\"$this->id_caja\",\"$this->fecha_apertura\",\"$this->monto_apertura\",1,$this->id_usuario,NOW())";
		return Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

// partiendo de que ya tenemos creado un objecto ProductData previamente utilizamos el contexto
	public function cerrarcaja(){
		$sql = "update ".self::$tablename." set fecha_cierre=\"$this->fecha_cierre\",monto_cierre=\"$this->monto_cierre\",estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set tipo=\"$this->tipo\",id_vehiculo=$this->id_vehiculo,fecha=\"$this->fecha\",hora=\"$this->hora\" where id=$this->id";
		Executor::doit($sql);
	}
	public function update_Montoactual(){
		$sql = "update ".self::$tablename." set monto_cierre=\"$this->monto_cierre\" where id_usuario=$this->id_usuario and estado=1";
		Executor::doit($sql);
	}

	public function update_Monto_Espera(){
		$sql = "update ".self::$tablename." set monto_cierre=\"$this->monto_cierre\" where id_usuario=$this->id_usuario and estado=1";
		Executor::doit($sql);
	}

	
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());

	}
	
	public static function getByIdcaja($id_caja){
		$sql = "select * from ".self::$tablename." where id_caja=$id_caja";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}



	public static function getAll(){
		$sql = "select * from ".self::$tablename." order by fecha_apertura desc ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}

	public static function getCierreCaja_id_user(){
		$sql = "select * from ".self::$tablename." where estado=1";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	public static function getCierreCaja($id_user){
		$sql = "select * from ".self::$tablename." where estado=1 and id_usuario=$id_user";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}

	public static function getMontoActual($id_user){
		$sql = "select * from ".self::$tablename." where estado=1 and id_usuario=$id_user";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}

	public static function getAllAbierto(){
		$sql = "select * from ".self::$tablename." where estado=1";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	public static function getAllAbierto_aperturas(){
		$sql = "SELECT mantenimiento_cajas.numero, aperturas_cajas.id FROM `aperturas_cajas` INNER JOIN mantenimiento_cajas ON mantenimiento_cajas.id = aperturas_cajas.id_caja WHERE aperturas_cajas.estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	public static function getAllCajasEstadoAbierto(){
		$sql = "select * from ".self::$tablename." where estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}

	public static function getLike($p){
		$sql = "select * from ".self::$tablename." where id like '%$p%'  ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}

	public static function getCajaOcupada($id_caja){
		$sql = "select * from ".self::$tablename." where estado=1 and id_caja=$id_caja";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}

	
	public static function getApertura_caja_monto_espera($id_apertura){
		$sql = "SELECT proceso.total as total FROM `aperturas_cajas` INNER JOIN proceso ON proceso.id_apertura = aperturas_cajas.id WHERE aperturas_cajas.id = $id_apertura and proceso.estado=1 and proceso.pagado = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());

	}


	//-----------------------------------------------------------------------------------------------------------------------------------------------------------
	//Monto aperturado (X)
	public static function getMontoAperturado(){
		$sql = "SELECT SUM(monto_apertura) as monto_Aperturado FROM `aperturas_cajas` where DATE(fecha_apertura) = DATE(NOW())";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	//Usuario activos, relacionados con el ingreso y egreso que han realizado -- many
	public static function getUsuarioActivo(){
		$sql = "SELECT user.name, aperturas_cajas.monto_apertura, gasto.precio FROM `aperturas_cajas` INNER JOIN user ON aperturas_cajas.id_usuario = user.id INNER JOIN gasto ON gasto.id_caja = aperturas_cajas.id WHERE fecha_apertura = DATE(NOW())";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	//-------------------------------------------------------------- EGRESOS ---------------------------------------------------------------------------------------------
	//Monto total de Egreso durante el dia (X)
	public static function getTotalEgreso(){
		$sql = "SELECT SUM(precio) AS egreso FROM `gasto` WHERE DATE(fecha) = DATE(NOW())";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	//Monto total de Egreso en relacion con las compras _ durante el dia (X)
	public static function getTotalEgreso_compras(){
		$sql = "SELECT sum(precio) as total FROM `proceso_venta` WHERE DATE(fecha_creada) = DATE(NOW()) AND proceso_venta.tipo_operacion = 2";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	//Monto total de Egreso durante el Mes 
	public static function getTotalEgreso_mes(){
		$sql = "SELECT SUM(precio) AS egreso FROM `gasto` WHERE MONTH(fecha) = MONTH(Now()) AND YEAR(fecha) = YEAR(Now())";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	//Monto total de Egreso en relacion con las compras del mes (X)
	public static function getTotalEgreso_compras_mes(){
		$sql = "SELECT sum(precio) as total FROM `proceso_venta` WHERE MONTH(fecha_creada) = MONTH(Now()) AND YEAR(fecha_creada) = YEAR(Now()) AND tipo_operacion = 2";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	//------------------------------------------------------------- INGRESO ----------------------------------------------------------------------------------------------
	//Monto total de Ingreso durante el dia en ventas (X)
	public static function getTotalIngreso(){
		$sql = "select sum(proceso_venta.precio) as total from proceso_venta WHERE proceso_venta.tipo_operacion = 1 and date(fecha_creada) = date(now())";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	//Monto total de Ingreso durante el dia en recepcion (X)
	public static function getTotalIngreso_recepcion(){
		$sql = "SELECT sum(precio) as total FROM `proceso` WHERE pagado = 1 AND date(fecha_creada) = date(now())";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}

	//Crecimiento en ventas
	public static function getCrecimiento_de_ventas(){
		$sql = "SELECT MONTH(proceso_venta.fecha_creada) AS MES, SUM(proceso.precio) as ganancia_en_habitaciones, SUM(proceso_venta.precio) AS ganancia_de_productos, SUM(proceso.precio) + SUM(proceso_venta.precio) AS total FROM `proceso_venta` INNER JOIN proceso ON MONTH(proceso.fecha_entrada) = MONTH(proceso_venta.fecha_creada) GROUP BY MES";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}


	//--------------------------------------------------------------- HABITACIONES --------------------------------------------------------------------------------------------
	//Habitaciones relacionada a sus ingresos durante el mes (X)
	public static function get_habitaciones_ingresos(){
		$sql = "SELECT habitacion.nombre as nombre, SUM(proceso.precio) as ganancia, COUNT(proceso.precio) as cantidad FROM `habitacion` INNER JOIN proceso ON proceso.id_habitacion = habitacion.id WHERE MONTH(proceso.fecha_entrada) = MONTH(CURRENT_DATE()) GROUP BY habitacion.nombre";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	//Habitaciones relacionada a sus ingresos durante el dia (X)
	public static function get_habitaciones_ingresos_dia(){
		$sql = "SELECT sum(precio) as monto FROM `proceso` WHERE MONTH(fecha_creada) = MONTH(now()) AND YEAR(fecha_creada) = YEAR(now())";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	//--------------------------------------------------------------- PRODUCTOS --------------------------------------------------------------------------------------------
	//Productos mas vendidos en etapas de fechas mes actual (X)
	public static function get_GananciasProductos(){
		$sql = "SELECT producto.nombre, SUM(proceso_venta.precio) as ganancias FROM `proceso_venta` INNER JOIN producto ON proceso_venta.id_producto = producto.id WHERE proceso_venta.tipo_operacion = 1 and MONTH(proceso_venta.fecha_creada) = MONTH(CURRENT_DATE()) and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY producto.id ORDER BY producto.nombre";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	//Productos mas comprados(egreso) en etapas de fechas mes actual
	public static function get_PerdidaProductos(){
		$sql = "SELECT producto.nombre, SUM(proceso_venta.precio) as ganancias FROM `proceso_venta` INNER JOIN producto ON proceso_venta.id_producto = producto.id WHERE proceso_venta.tipo_operacion = 2 and MONTH(proceso_venta.fecha_creada) = MONTH(CURRENT_DATE()) and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY producto.id ORDER BY producto.nombre";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}

	//Fecha ventas crecimiento
	public static function get_grafico_ventas_crecimiento($dato){
		//print($dato);
		if($dato == 'dia'){
			//Ventas de productos por dias del mes (x)
			$sql = "SELECT sum(precio) as monto, Day(fecha_creada) as fecha FROM `proceso_venta`
			WHERE tipo_operacion = 1 and MONTH(proceso_venta.fecha_creada) = MONTH(CURRENT_DATE())
			and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY day(fecha_creada)";
			$query = Executor::doit($sql);
			return Model::many($query[0],new CajasAperturas());
		}else if($dato == 'mes'){
			//Ventas de productos por meses del año 
			$sql = "SELECT sum(precio) as monto, month(fecha_creada) as fecha FROM `proceso_venta` WHERE tipo_operacion = 1
			and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY MONTH(fecha_creada)";
		   $query = Executor::doit($sql);
		   return Model::many($query[0],new CajasAperturas());
		}else if($dato == 'año'){
			//Ventas de productos por años 
			$sql = "SELECT sum(precio) as monto, year(fecha_creada) as fecha FROM `proceso_venta` WHERE tipo_operacion = 1 GROUP BY year(fecha_creada)";
			$query = Executor::doit($sql);
			return Model::many($query[0],new CajasAperturas());
		}
		else {
			//Ventas de productos por dias del mes (x)
			$sql = "SELECT sum(precio) as monto, Day(fecha_creada) as fecha FROM `proceso_venta`
			WHERE tipo_operacion = 1 and MONTH(proceso_venta.fecha_creada) = MONTH(CURRENT_DATE())
			and YEAR(proceso_venta.fecha_creada) = YEAR(CURRENT_DATE()) GROUP BY day(fecha_creada)";
			$query = Executor::doit($sql);
			return Model::many($query[0],new CajasAperturas());
		}

		
	}

	
	//----------------------------------------------------------------- USUARIOS ------------------------------------------------------------------------------------------
	//Usuario con mas ventas
	public static function get_usuarios_con_mayores_ventas(){
		$sql = "SELECT MONTH(proceso_venta.fecha_creada) AS MES, user.username AS Usuario, SUM(proceso.precio) as ganancia_en_habitaciones, SUM(proceso_venta.precio) AS ganancia_de_productos, SUM(proceso.precio) + SUM(proceso_venta.precio) AS sum FROM `proceso_venta` INNER JOIN proceso ON MONTH(proceso.fecha_entrada) = MONTH(proceso_venta.fecha_creada) INNER JOIN user ON user.id = proceso.id_usuario GROUP BY Usuario";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	//Usuarios datos durante el dia (X)
	public static function get_usuarios_datos_dia(){
		$sql = "SELECT user.username AS Usuario, mantenimiento_cajas.numero, time(aperturas_cajas.fecha_creada) AS hora_inicial,aperturas_cajas.monto_apertura as monto_inicial, time(aperturas_cajas.fecha_cierre) as hora_cierre,aperturas_cajas.monto_cierre as monto_final FROM `aperturas_cajas` INNER JOIN user ON user.id = aperturas_cajas.id_usuario INNER JOIN mantenimiento_cajas ON mantenimiento_cajas.id = aperturas_cajas.id_caja WHERE date(aperturas_cajas.fecha_creada) = date(NOW())";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}

	//--------------------------------------------------------------- Resumen de liquidacion -----------------------------------------------------------------------------------------
	//Modos de pago mas utilizados
	public static function resumen_liquidaciones(){
		$sql = "SELECT aperturas_cajas.id as id, user.name as usuario, mantenimiento_cajas.numero as numero_caja, aperturas_cajas.monto_apertura apertura, aperturas_cajas.monto_cierre cierre, aperturas_cajas.fecha_apertura hora_inicio, aperturas_cajas.fecha_cierre hora_cierre
		FROM `aperturas_cajas`
		INNER JOIN user ON user.id = aperturas_cajas.id_usuario
		INNER JOIN mantenimiento_cajas ON mantenimiento_cajas.id = aperturas_cajas.id_caja
		ORDER BY aperturas_cajas.fecha_cierre";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}

	public static function resumen_liquidaciones_fechas($fecha_ini,$fecha_fin){
		$sql = "SELECT aperturas_cajas.id as id, user.name as usuario, mantenimiento_cajas.numero 
		as numero_caja, aperturas_cajas.monto_apertura apertura, aperturas_cajas.monto_cierre cierre,
		aperturas_cajas.fecha_apertura hora_inicio, aperturas_cajas.fecha_cierre hora_cierre
		FROM `aperturas_cajas`
		INNER JOIN user ON user.id = aperturas_cajas.id_usuario
		INNER JOIN mantenimiento_cajas ON mantenimiento_cajas.id = aperturas_cajas.id_caja
		WHERE date(aperturas_cajas.fecha_cierre) >= \"$fecha_ini\"
		AND date(aperturas_cajas.fecha_cierre) <= \"$fecha_fin\"
		ORDER BY Day(aperturas_cajas.fecha_cierre),MONTH(aperturas_cajas.fecha_cierre),YEAR(aperturas_cajas.fecha_cierre)";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	public static function resumen_liquidaciones_historial_1($id){
		$sql = "SELECT date(aperturas_cajas.fecha_creada) as fecha,
		 time(aperturas_cajas.fecha_creada) as hora,
		aperturas_cajas.monto_apertura as Monto_inicial,
		 sum(proceso.precio) as habitacion,
		time(aperturas_cajas.fecha_cierre) as hora_cierre,
		 aperturas_cajas.monto_cierre as monto_final
		FROM `aperturas_cajas` 
		LEFT JOIN proceso ON proceso.id_apertura = aperturas_cajas.id 
		WHERE aperturas_cajas.id = $id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	public static function resumen_liquidaciones_historial_2($id){
		$sql = "SELECT proceso_venta.tipo_operacion,
		sum(proceso_venta.precio) as venta
		FROM `proceso_venta` WHERE proceso_venta.id_caja = $id
		and proceso_venta.tipo_operacion = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	public static function resumen_liquidaciones_historial_3($id){
		$sql = "SELECT sum(gasto.precio) as egreso
		FROM `aperturas_cajas` 
		LEFT JOIN gasto ON gasto.id_caja = aperturas_cajas.id
		WHERE aperturas_cajas.id = $id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	public static function resumen_liquidaciones_historial_4($id){
		$sql = "SELECT proceso_venta.tipo_operacion,
		sum(proceso_venta.precio) as compra
		FROM `proceso_venta` WHERE proceso_venta.id_caja = $id
		and proceso_venta.tipo_operacion = 2";
		$query = Executor::doit($sql);
		return Model::many($query[0],new CajasAperturas());
	}
	
	public static function resumen_liquidaciones_historial_5($id){
		$sql = "SELECT 
		(SELECT date(c1.fecha_cierre) FROM `aperturas_cajas` c1 WHERE c1.id = $id) as fecha_cierre,
		(SELECT date(c1.fecha_apertura) FROM `aperturas_cajas` c1 WHERE c1.id =$id ) as fecha,
		(SELECT time(c1.fecha_apertura) FROM `aperturas_cajas` c1 WHERE c1.id = $id) as hora_inicial,
		(SELECT c1.monto_apertura FROM `aperturas_cajas` c1 WHERE c1.id = $id) as monto_apertura,
		(SELECT  time(c1.fecha_cierre) FROM `aperturas_cajas` c1 WHERE c1.id = $id) as hora_cierre,
		(SELECT c1.monto_cierre FROM `aperturas_cajas` c1 WHERE c1.id =$id ) as monto_cierre,
		(SELECT sum(pro1.cobro_ext) as cobro_extra FROM `proceso` pro1, aperturas_cajas ac WHERE pro1.id_apertura = $id and pro1.estado = 1 and pro1.id_apertura = ac.id AND ac.id = $id AND pro1.id_caja = ac.id) as cobro_ext,
		(SELECT sum(pro1.cobro_ext) as cobro_extra FROM `proceso` pro1, aperturas_cajas ac WHERE pro1.id_apertura = $id and pro1.estado = 1 and pro1.id_apertura = ac.id AND ac.id = $id AND pro1.id_caja != ac.id) as cobro_ext_externo,
		(SELECT sum(p1.nro_comprobante) as total FROM `venta` p1, aperturas_cajas ac WHERE p1.tipo_operacion = 2 AND p1.id_caja = ac.id AND ac.id = $id) as compra,
		(SELECT sum(p1.nro_comprobante) as total FROM `venta` p1, aperturas_cajas ac WHERE p1.tipo_operacion = 1 AND p1.id_caja = ac.id AND ac.id = $id) as venta,
		(SELECT sum(g1.precio) as total FROM `gasto` g1, aperturas_cajas ac WHERE g1.id_caja = ac.id AND ac.id = $id)as gasto,
		(SELECT sum(pro1.precio) as habitacion FROM `proceso` pro1, aperturas_cajas ac WHERE pro1.id_caja = ac.id and ac.id = $id) as habitacion_total,
        (SELECT sum(pro1.cobro_ext) as cobro_otro FROM `proceso` pro1 WHERE pro1.id_caja = $id AND pro1.id_apertura != $id) as cobro_ext_diferente_proceso,
		(SELECT sum(pro1.cobro_ext) as cobro_otro FROM `proceso` pro1 WHERE pro1.id_caja != $id AND pro1.id_apertura = $id) as cobro_ext_diferente_proceso2, 
        (SELECT sum(pro1.cobro_ext) as cobro_otro FROM `proceso` pro1 WHERE pro1.id_caja = $id AND pro1.id_apertura = $id) as cobro_ext_mismo_proceso
		FROM aperturas_cajas ac 
		LEFT JOIN venta p ON ac.id = p.id_caja 
		LEFT JOIN gasto g ON ac.id = g.id_caja 
		INNER JOIN proceso pro ON ac.id = pro.id_apertura
        GROUP BY ac.id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new CajasAperturas());
	}
	

/* SELECT (SELECT sum(p1.precio) as total FROM `proceso_venta` p1, aperturas_cajas ac WHERE p1.tipo_operacion = 2 AND p1.id_caja = ac.id AND ac.id = 109) as venta, (SELECT sum(p1.precio) as total FROM `proceso_venta` p1, aperturas_cajas ac WHERE p1.tipo_operacion = 1 AND p1.id_caja = ac.id AND ac.id = 109) as compra FROM aperturas_cajas ac INNER JOIN proceso_venta p ON ac.id = p.id_caja GROUP BY ac.id*/
/* SELECT 
date(ac.fecha_apertura) as fecha, 
time(ac.fecha_apertura) as hora_inicial,
time(ac.monto_cierre) as hora_cierre,
ac.monto_apertura as monto_apertura,
ac.fecha_apertura as fecha,
(SELECT sum(p1.precio) as total FROM `proceso_venta` p1, aperturas_cajas ac WHERE p1.tipo_operacion = 2 AND p1.id_caja = ac.id AND ac.id = 109) as venta,
(SELECT sum(p1.precio) as total FROM `proceso_venta` p1, aperturas_cajas ac WHERE p1.tipo_operacion = 1 AND p1.id_caja = ac.id AND ac.id = 109) as compra,
(SELECT sum(g1.precio) as total FROM `gasto` g1, aperturas_cajas ac WHERE g1.id_caja = ac.id AND ac.id = 109) as gasto,
(SELECT sum(pro1.precio) as habitacion FROM `proceso` pro1, aperturas_cajas ac WHERE pro1.id_apertura = ac.id AND ac.id = 109) as alquiler
FROM aperturas_cajas ac 
INNER JOIN proceso_venta p ON ac.id = p.id_caja 
INNER JOIN gasto g ON ac.id = g.id_caja 
INNER JOIN proceso pro ON ac.id = pro.id_apertura
GROUP BY ac.id*/
}

?>