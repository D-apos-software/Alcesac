
<?php 

class Actualizar_Apertura {
	public function Actualizar_Apertura(){
	} 
    	public function actualizar(){
            
            $total_ingreso = 0;
            $total_egreso = 0;
               date_default_timezone_set('America/Costa_Rica');
               $hoy = date("Y-m-d");
          
                $u=null;
                $u = UserData::getById(Session::getUID());
                $usuario = $u->is_admin;
                $id_usuario = $u->id;
          
                $hora = date("H:i:s");
                $fecha_completo = date("Y-m-d H:i:s");   
                       
                $monto_cierre = 0;
                $caja_abierta = CajasAperturas::getCierreCaja($id_usuario); 
                    
                        $object = (array) $caja_abierta;
                        $object = count($object); 
                        
                            if( $object > 0){
                        
                                    $montos_sin_cerrar = ProcesoData::getIngresoCaja($caja_abierta->id); 
                                    $total_sin_cerrar=0;
                                    if(count($montos_sin_cerrar)>0){

                                        foreach($montos_sin_cerrar as $monto_sin_cerrar):
                                            $total_sin_cerrar=(($monto_sin_cerrar->precio*$monto_sin_cerrar->cant_noche)+$monto_sin_cerrar->total)+$total_sin_cerrar;
                                        endforeach;

                                    }

                                    if($caja_abierta->id!=0){ 
                                        $reporproducts = ProcesoVentaData::getIngresoCaja($caja_abierta->id);
                                        $subtotal3=0;
                                        if(count($reporproducts)>0){ 
                                            foreach($reporproducts as $reporproduct):
                                                $subtotal1=$reporproduct->cantidad*$reporproduct->precio; 
                                            $subtotal3=$subtotal1+$subtotal3; 
                                            endforeach; 
                                        }else{$subtotal3=0;} 
                                    }else{$subtotal3=0;}  

                                    // FIN INGRESOS 

                                    // EGRESOS 

                                    $montos_sin_cerrar_egresos = GastoData::getEgresoCaja($caja_abierta->id);
                                            $total_sin_cerrar_egreso=0;
                                            if(count($montos_sin_cerrar_egresos)>0){

                                            foreach($montos_sin_cerrar_egresos as $montos_sin_cerrar_egreso):
                                                $total_sin_cerrar_egreso=$montos_sin_cerrar_egreso->precio+$total_sin_cerrar_egreso;
                                            endforeach;
                                    } 

                                    $montos_sin_cerrar_sueldos = ProcesoSueldoData::getSueldoCaja($caja_abierta->id);
                                            $total_sin_cerrar_sueldos=0;
                                            if(count($montos_sin_cerrar_sueldos)>0){ 

                                            foreach($montos_sin_cerrar_sueldos as $montos_sin_cerrar_sueldo):
                                                $total_sin_cerrar_sueldos=$montos_sin_cerrar_sueldo->monto+$total_sin_cerrar_sueldos;
                                            endforeach;
                                    } 
   
                                    if($caja_abierta->id!=0){ 
                                    $reporproducts_es = ProcesoVentaData::getEgresoCaja($caja_abierta->id);
                                    $subtotal4=0;
                                    if(count($reporproducts_es)>0){ 
                                        foreach($reporproducts_es as $reporproduct_e):
                                            $subtotal1=$reporproduct_e->cantidad*$reporproduct_e->precio; 
                                        $subtotal4=$subtotal1+$subtotal4; 
                                        endforeach; 
                                    }else{$subtotal4=0;} 
                                    }else{$subtotal4=0;} 
      
                                    // Total egreso 
                                    $total_egreso=$total_sin_cerrar_egreso+$total_sin_cerrar_sueldos+$subtotal4; 
                                    // Fin Total egreso 
                                    // Total ingreso 
                                    $total_debido=0;
                                    $id_apertura = $caja_abierta->id;
                                    $lista_debido=[];
                                    $saldo_salida = CajasAperturas::getApertura_caja_monto_espera($id_apertura);
                                    foreach($saldo_salida as $id_aumentar):
                                        array_push($lista_debido,$id_aumentar->total);
                                    endforeach;
                                    foreach($lista_debido as $monto):
                                    $total_debido += $monto;
                                    endforeach;
                                    $monto_extra = ProcesoData::getMontoExtra($id_apertura);
                                    $cobro_ext = 0;
                                    foreach($monto_extra as $valor):
                                        $cobro_ext += $valor->cobro_ext;
                                    endforeach;
                                    $total_ingreso=$total_sin_cerrar+$subtotal3+$total_debido+$cobro_ext; 
                                    // Fin Total ingreso 
                                //echo number_format($caja_abierta->monto_apertura+$total_ingreso-$total_egreso,2,'.',',');
                                $monto_cierre = number_format($caja_abierta->monto_apertura+$total_ingreso-$total_egreso,2,'.',',');
                            //Actualizamos el monto de cierre por el monto que se lleva actualmente, para que el admin este al tanto de los valores en cada caja
                            $actual_estado = CajasAperturas::getMontoActual($id_usuario); 
                            //Convertirmos el valor que ingresaremos como monto de la actualizacion 
                            $monto_cierre_convertido = str_replace (',', '', $monto_cierre);
                            $actual_estado->monto_cierre = $monto_cierre_convertido;
                            $actual_estado->id_usuario = $id_usuario;
                            $actual_estado->update_Montoactual();

                  }
          
	}
}


  
             
?>
