<?php
class Tipo_impuesto_productos {
	public static $tablename = "impuestos_productos";

	public function Tipo_impuesto_productos(){
		$this->nombre = "";
		$this->descripcion = "";
		$this->valor = "0";
		$this->estado = "0";
		$this->tipo_impuesto  = "";
		$this->id_tipo_impuesto  = "0";
		$this->fecha_creada = "NOW()";
	}
	//public function getname_tipo_impuesto(){ return Tipo_subimpuesto_producto::getById($this->id_tipo_impuesto);}

	public function add(){
		$sql = "insert into impuestos_productos (nombre,descripcion,valor,estado,tipo_impuesto,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",\"$this->descripcion\",$this->valor,1,\"$this->tipo_impuesto\",NOW())";
		Executor::doit($sql);
	}
	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\", descripcion=\"$this->descripcion\", valor=$this->valor, estado=$this->estado,tipo_impuesto=\"$this->tipo_impuesto\" where id=$this->id";
		Executor::doit($sql);
	}
	public function update_inactive(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_impuesto_productos());

	}

	public static function getByValor($valor){
		$sql = "select * from ".self::$tablename." where valor=$valor";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_impuesto_productos());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_impuesto_productos());
	}
	public static function get_active(){
		$sql = "select * from ".self::$tablename." WHERE estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_impuesto_productos());
	}
	public static function getAll_active(){
		$sql = "select * from ".self::$tablename." where estado = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_impuesto_productos());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_impuesto_productos());

	}

}
