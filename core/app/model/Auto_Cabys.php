<?php
class Auto_Cabys {
	public static $tablename = "auto_cabys";

	public function Auto_Cabys(){

		$this->descripcion = "";
		$this->cabys = 0; 
		$this->fecha = "";
		$this->estado = true;

	} 

	public function add(){
		$sql = "insert into auto_cabys (descripcion,cabys,estado,fecha,fecha_act) ";
		$sql .= "value (\"$this->descripcion\",\"$this->cabys\",0,NOW(),NOW())";
		return Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set descripcion=\"$this->descripcion\",fecha_act=NOW() where id=$this->id";
		Executor::doit($sql);
	}


	public function update_estado(){
		$sql = "update ".self::$tablename." set estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}
	
	
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Auto_Cabys());

	}
	public static function getByCodigo($id){
		$sql = "select * from ".self::$tablename." where cabys=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Auto_Cabys());

	}
	

	public static function getAll(){
		$sql = "select * from ".self::$tablename." order by fecha desc ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Auto_Cabys());
	}

	public static function getActivos(){
		$sql = "select * from ".self::$tablename." WHERE estado = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Auto_Cabys());
	}

	public static function getLike($p){
		$sql = "select * from ".self::$tablename." where id like '%$p%'  ";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Auto_Cabys());
	}


}

?>