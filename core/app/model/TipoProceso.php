<?php
class TipoProceso
{
	public static $tablename = "tipo_proceso";



	public function TipoProceso()
	{
		$this->id_tipo_cliente = "";
		$this->nombre = "";
		$this->valor = "";
		$this->fecha_creada = "NOW()";
	}

	public function getname_tipo_cliente()
	{
		return TipoCliente::getById($this->id_tipo_cliente);
	}

	public function add()
	{
		$sql = "insert into tipo_proceso (id_tipo_cliente,nombre,valor,fecha_creada) ";
		$sql .= "value ($this->id_tipo_cliente,\"$this->nombre\",$this->valor,$this->fecha_creada)";
		Executor::doit($sql);
	}

	public static function delById($id)
	{
		$sql = "delete from " . self::$tablename . " where id=$id";
		Executor::doit($sql);
	}
	public function del()
	{
		$sql = "delete from " . self::$tablename . " where id=$this->id";
		Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto TipoProceso previamente utilizamos el contexto
	public function update()
	{
		$sql = "update " . self::$tablename . " set id_tipo_cliente=\"$this->id_tipo_cliente\",nombre=\"$this->nombre\",valor=\"$this->valor\" where id=$this->id";
		Executor::doit($sql);
	}



	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new TipoProceso());
	}

	public static function getAll()
	{
		$sql = "select * from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0], new TipoProceso());
	}


	public static function getLike($q)
	{
		$sql = "select * from " . self::$tablename . " where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0], new TipoProceso());
	}
	//Query select tipo de cliente con valor 1 o 0 o 2
	public static function getTipoCliente($id_cliente)
	{
		$sql = "select tipo_proceso.nombre, tipo_proceso.id_tipo_cliente, tipo_proceso.valor from tipo_proceso where tipo_proceso.id_tipo_cliente = " . $id_cliente . "";
		$query = Executor::doit($sql);
		return Model::many($query[0], new TipoProceso());
	}
	
}
