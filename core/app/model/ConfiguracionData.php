<?php
class ConfiguracionData {
	public static $tablename = "configuracion";

	public function ConfiguracionData(){
		$this->nombre = "";
		$this->direccion = "";
		$this->estado = "";
		$this->telefono = "";
		$this->fax = "";
		$this->rnc = "";
		$this->llave_criptografica = "";		
		$this->usuario_hacienda = "";		
		$this->contrasena_hacienda = "";
		$this->token = "";
		
		$this->pin = "";
		$this->cod_moneda = "";
		$this->nombre_hac = "";
		$this->numero_id = "";
		$this->provincia = "";
		$this->canton = "";
		$this->distrito = "";
		$this->barrio = "";
		$this->cod_pais_tel_hc = "";
		$this->tel_hac = "";
		$this->tipo_id = "";

		
		
		$this->fecha = "NOW()";


	}

	
	public function add(){
		$sql = "insert into ".self::$tablename." (nombre,direccion,estado,telefono,fax,rnc,registro_empresarial,ciudad,fecha) ";
		$sql .= "value (\"$this->nombre\",\"$this->direccion\",\"$this->estado\",\"$this->telefono\",\"$this->fax\",\"$this->rnc\",\"$this->registro_empresarial\",\"$this->ciudad\",NOW())";
		return Executor::doit($sql);
	}
	


	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

// partiendo de que ya tenemos creado un objecto ProductData previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",direccion=\"$this->direccion\",estado=\"$this->estado\",telefono=\"$this->telefono\",fax=\"$this->fax\",rnc=\"$this->rnc\",registro_empresarial=\"$this->registro_empresarial\",ciudad=\"$this->ciudad\",fecha= NOW() where id=$this->id";
		Executor::doit($sql);
	}


	public function update_logo(){
		$sql = "update ".self::$tablename." set logo=\"$this->logo\" where id=$this->id";
		Executor::doit($sql);
	}


	public function update_data_hacienda(){
		$sql = "update ".self::$tablename." set usuario_hacienda=\"$this->usuario_hacienda\", contrasena_hacienda=\"$this->contrasena_hacienda\", llave_criptografica=\"$this->llave_criptografica\", pin=\"$this->pin\",cod_moneda=\"$this->cod_moneda\",nombre_hac=\"$this->nombre_hac\",numero_id=\"$this->numero_id\",provincia=\"$this->provincia\",canton=\"$this->canton\",distrito=\"$this->distrito\",barrio=\"$this->barrio\",cod_pais_tel_hc=\"$this->cod_pais_tel_hc\",tel_hac=\"$this->tel_hac\",tipo_id=\"$this->tipo_id\" where id=$this->id";
		Executor::doit($sql);
	}

	public function update_token(){
		$sql = "update ".self::$tablename." set usuario_hacienda=\"$this->usuario_hacienda\", contrasena_hacienda=\"$this->contrasena_hacienda\", llave_criptografica=\"$this->llave_criptografica\", token=\"$this->token\", pin=\"$this->pin\",cod_moneda=\"$this->cod_moneda\",nombre_hac=\"$this->nombre_hac\",numero_id=\"$this->numero_id\",provincia=\"$this->provincia\",canton=\"$this->canton\",distrito=\"$this->distrito\",barrio=\"$this->barrio\",cod_pais_tel_hc=\"$this->cod_pais_tel_hc\",tel_hac=\"$this->tel_hac\",tipo_id=\"$this->tipo_id\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ConfiguracionData());

	}

	public static function getAllConfiguracion(){
		$sql = "select * from ".self::$tablename." limit 1";
		$query = Executor::doit($sql);
		return Model::one($query[0],new ConfiguracionData());
	}



	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new ConfiguracionData());
	}

	public static function getAllCONF(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::one($query[0],new ConfiguracionData());
	}





}

?>