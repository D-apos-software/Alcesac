<?php
class Unidad_de_medida
{
	public static $tablename = "unidad_de_medida";

	public function Unidad_de_medida()
	{
		$this->nombre = "";
		$this->descripcion = "";
		$this->valor = "";
		$this->estado = "0";
		$this->fecha_creada = "NOW()";
	}

	public function add()
	{
		$sql = "insert into unidad_de_medida (nombre,descripcion,valor,estado,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",\"$this->descripcion\",\"$this->valor\",$this->estado,NOW())";
		Executor::doit($sql);
	}

	public static function delById($id)
	{
		$sql = "delete from " . self::$tablename . " where id=$id";
		Executor::doit($sql);
	}

	public function del()
	{
		$sql = "delete from " . self::$tablename . " where id=$this->id";
		Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto Tipo categoria venta previamente utilizamos el contexto
	public function update()
	{
		$sql = "update unidad_de_medida set nombre=\"$this->nombre\",descripcion=\"$this->descripcion\" where id=$this->id";
		Executor::doit($sql);
	}
	public function update_estado()
	{
		$sql = "update unidad_de_medida set estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new Unidad_de_medida());
	}

	public static function getByName($nombre)
	{
		$sql = "select * from " . self::$tablename . " where nombre='$nombre'";
		$query = Executor::doit($sql);
		return Model::one($query[0], new Unidad_de_medida());
	}


	public static function getAll()
	{
		$sql = "select * from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0], new Unidad_de_medida());
	}

	public static function getactive()
	{
		$sql = "select * from " . self::$tablename." where estado = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0], new Unidad_de_medida());
	}

	public static function getLike($q)
	{
		$sql = "select * from " . self::$tablename . " where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0], new Unidad_de_medida());
	}
}
