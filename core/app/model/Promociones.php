<?php
class Promociones {
	public static $tablename = "promociones";


	public function Promociones(){
		$this->valor = "";
        $this->descripcion = "";
        $this->estado="0";
		$this->fecha = "NOW()"; 
	} 

	public function add(){
		$sql = "insert into promociones (valor,descripcion,estado,fecha) ";
		$sql .= "value (\"$this->valor\",\"$this->descripcion\",\"$this->estado\",NOW())";
		return Executor::doit($sql);
	}


	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}


	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set valor=\"$this->valor\",descripcion=\"$this->descripcion\" where id=$this->id";
		Executor::doit($sql);
	}
	
	public function update_estado(){
		$sql = "update ".self::$tablename." set estado=\"$this->estado\" where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Promociones());

	}


	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Promociones());
	}

	public static function show_data(){
		$sql = 
        "SELECT categoria_clientes_p.nombre as nombre,promociones.valor as valor,promociones.descripcion as descripcion, promociones.estado as estado, promociones.id as id from promociones 
        INNER JOIN r_promociones on r_promociones.id_porcentaje = promociones.id
        INNER JOIN categoria_clientes_p on categoria_clientes_p.id = r_promociones.id_categoria GROUP BY promociones.id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Promociones());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombres like '%$q%' or apellidos like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Promociones());

	}


}

?>