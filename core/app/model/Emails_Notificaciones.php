<?php
class Emails_Notificaciones {
	public static $tablename = "notificaciones";

	public function Emails_Notificaciones(){
		$this->email = "";
		$this->estado = "0";
		$this->valor = "0";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into notificaciones (email,estado,valor,fecha_creada) ";
		$sql .= "value (\"$this->email\",0,0,NOW())";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}

	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set email=\"$this->email\", estado=$this->estado, valor=$this->valor where id=$this->id";
		Executor::doit($sql);
	}

	public function updateEstado(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Emails_Notificaciones());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Emails_Notificaciones());
	}

	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where email like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Emails_Notificaciones());

	}
	public static function emails_notificacion(){
		$sql = "SELECT email FROM `notificaciones` WHERE estado = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Emails_Notificaciones());

	}

}

?>