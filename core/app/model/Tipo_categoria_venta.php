<?php
class Tipo_categoria_venta {
	public static $tablename = "tipo_categoria_venta";



	public function Tipo_categoria_venta(){
		$this->nombre = "";
		$this->codigo = "";
		$this->estado="0";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into tipo_categoria_venta (nombre,codigo,estado,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",\"$this->codigo\",$this->estado,$this->fecha_creada)";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

    // partiendo de que ya tenemos creado un objecto Tipo categoria venta previamente utilizamos el contexto
	public function update(){
		$sql = "update tipo_categoria_venta set nombre=\"$this->nombre\",codigo=\"$this->codigo\",estado=1 where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_categoria_venta());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_categoria_venta());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_categoria_venta());

	}


}

?>