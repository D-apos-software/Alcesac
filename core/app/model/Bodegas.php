<?php
class Bodegas {
	public static $tablename = "bodegas";


	public function Bodegas(){
		$this->codigo = "";
		$this->nombre = ""; 
		$this->cantidad_inicial = "0";
		$this->cantidad_maxima = "0";
		$this->cantidad_minima = "0";
		$this->estado = "0"; 
		$this->fecha_creada = "NOW()"; 
	} 

	public function add(){
		$sql = "insert into bodegas (codigo,nombre,cantidad_inicial,cantidad_maxima,cantidad_minima,estado,fecha_creada) ";
		$sql .= "value (\"$this->codigo\",\"$this->nombre\",\"$this->cantidad_inicial\",\"$this->cantidad_maxima\",\"$this->cantidad_minima\",\"$this->estado\",NOW())";
		return Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}


	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

    // Partiendo de que ya tenemos creado un objecto ClienteData previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set codigo=\"$this->codigo\",nombre=\"$this->nombre\",cantidad_inicial=$this->cantidad_inicial,cantidad_maxima=$this->cantidad_maxima,cantidad_minima=$this->cantidad_minima,estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}

	public function update_estado(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Bodegas());

	}

	public static function getByName($nombre){
		$sql = "select * from ".self::$tablename." where nombre='$nombre'";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Bodegas());
	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Bodegas());
	}

	
	public static function get_activas(){
		$sql = "select * from ".self::$tablename." where estado =1";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Bodegas());
	}
	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombres like '%$q%' or apellidos like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Bodegas());

	}


}

?>