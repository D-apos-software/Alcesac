<?php
class PeriodosData {

	public static $tablename = "periodos_pago";

	public function PeriodosData(){
		$this->nombre = "";
		$this->dias = "";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into periodos_pago (nombre,dias,fecha_creada) ";
		$sql .= "value (\"$this->nombre\",$this->dias,$this->fecha_creada)";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}

// partiendo de que ya tenemos creado un objecto periodos_pago previamente utilizamos el contexto
	public function update(){
		$sql = "update ".self::$tablename." set nombre=\"$this->nombre\",dias=\"$this->dias\" where id=$this->id";
		Executor::doit($sql);
	}

	

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new PeriodosData());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new PeriodosData());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PeriodosData());

	}


}

?>