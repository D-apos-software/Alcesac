<?php
class Tipo_r_impuestos_productos {
	public static $tablename = "r_impuestos_productos";

	public function Tipo_r_impuestos_productos(){
		$this->id_impuesto = "";
		$this->id_producto = "";
		$this->fecha_creada = "NOW()";
	}

	public function add(){
		$sql = "insert into r_impuestos_productos (id_impuesto,id_producto,fecha_creada) ";
		$sql .= "value (\"$this->id_impuesto\",\"$this->id_producto\",NOW())";
		Executor::doit($sql);
	}
	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}
	public function del(){
		$sql = "delete from ".self::$tablename." where id=$this->id";
		Executor::doit($sql);
	}
	public function del_by_id_producto(){
		$sql = "delete from ".self::$tablename." where id_producto=$this->id_producto";
		Executor::doit($sql);
	}

	public function update(){
		$sql = "update ".self::$tablename." set id_impuesto=\"$this->id_impuesto\",id_producto=\"$this->id_producto\" where id=$this->id";
		Executor::doit($sql);
	}
	public function update_inactive(){
		$sql = "update ".self::$tablename." set estado=$this->estado where id=$this->id";
		Executor::doit($sql);
	}
	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_impuestos_productos());
	}
	public static function getimpuestos($id){
		$sql = "select id_impuesto from ".self::$tablename." where id_producto=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_impuestos_productos());

	}
	public static function getimpuestos2($id){
		$sql = "select * from ".self::$tablename." where id_producto=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new Tipo_r_impuestos_productos());

	}

	public static function delete($id){
		$sql = "delete from ".self::$tablename." where id_producto = $id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_impuestos_productos());
	}
	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_impuestos_productos());
	}


	public static function getLike($q){
		$sql = "select * from ".self::$tablename." where nombre like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0],new Tipo_r_impuestos_productos());

	}

	public function get_impuesto_valor($id)
	{
		$sql = "SELECT impuestos_productos.valor,impuestos_productos.tipo_impuesto from r_impuestos_productos INNER JOIN impuestos_productos ON impuestos_productos.id = r_impuestos_productos.id_impuesto WHERE r_impuestos_productos.id_producto =".$id;
		$query = Executor::doit($sql);
		return Model::many($query[0], new ProductoData());
	}


	public function get_nombre_impuesto($id){
		$sql = "SELECT impuestos_productos.nombre FROM `r_impuestos_productos` INNER JOIN impuestos_productos on impuestos_productos.id = r_impuestos_productos.id_impuesto INNER JOIN producto on producto.id = r_impuestos_productos.id_producto WHERE producto.id =".$id;
		$query = Executor::doit($sql);
		return Model::many($query[0], new Tipo_r_impuestos_productos());
	}

}

?>