<head>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
  <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
</head>
<?php
date_default_timezone_set('America/Costa_Rica');
$hoy = date("Y-m-d");

$u = null;
$u = UserData::getById(Session::getUID());
$usuario = $u->is_admin;
$id_usuario = $u->id;

$hora = date("H:i:s");
$fecha_completo = date("Y-m-d H:i:s");

?>
<link rel="stylesheet" href="css/vista_cliente/check_btn.css">
<script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
<div class="row" style="margin-top:60px;">
  <div class="col-sm-6 col-md-8">
    <!-- Detalle de Compra -->
    <div class="panel panel-default" id="panel-detalle">
      <div class="panel-heading">
        <h6 class="panel-title">Detalle de Venta</h6>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <div class="row">
            <div class="col-sm-12">
              <div class="input-group">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">
                  <span class="glyphicon glyphicon-plus"></span> Agregar productos
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table id="tbldetalle" class="table table-xxs">
            <thead>
              <tr class="bg-blue">
                <th></th>
                <th>Producto</th>
                <th>Cant.</th>
                <th class="text-initial">Precio</th>
                <th class="text-initial">Utilidad</th>
                <th class="text-initial">Importe</th>
                <!--<th class="text-center">Impuesto</th>-->
                <th class="text-center">Quitar</th>
              </tr>
            </thead>
            <tbody id="resultados">
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- /Detalle de Compra -->
    <div class="form-group">
      <div class="row">
        <div class="col-sm-3">
          <a href="index.php?view=cancelar_venta" class="btn btn-danger btn-labeled btn-block"><b>
              <i class="icon-cancel-circle2"></i>
            </b> Cancelar Venta</a>
        </div>
      </div>
    </div>
  </div>



  <!-- tile -->





  <!-- Informacion Proveedor -->
  <div class="col-sm-6 col-md-4">

    <!-- tile -->
    <section class="tile">

      <!-- tile header -->
      <div class="tile-header bg-slategray text-center" style="background-color: #4caf50 !important;
    padding: 20px 15px !important;">

        <h1 class="custom-font text-uppercase" id="total_compra"><strong>IMPORTANTE </strong></h1>

      </div>
      <!-- /tile header -->



      <!-- tile body -->
      <div class="tile-body">
        <div class="form-group">
          <div class="row">
            <div class="col-sm-12" style="margin-bottom:20px;">
              <input class="form-control" id="cd1" type="text" placeholder="ID Cliente">
              <button type="submit" class="btn btn-success
                    btn-labeled btn-block btn-ladda btn-ladda-spinner" id="cd"><b>
              </b>Buscar</button>
            </div>
          </div>
        </div>

        <form autocomplete="off" method="post" action="index.php?view=addventa">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-12">
                <label>Tipo de cliente</label>

                <?php $tipo_clientes = TipoCliente::getAll(); ?>
                <select class="form-control select2" required name="tipo_id_cliente" id="tipo_id_cliente" onchange="CargarTipoCliente(this.value);">
                  <option value="0">--- Selecciona ---</option>
                  <?php foreach ($tipo_clientes as $tipo_cliente) : ?>
                    <?php if ($tipo_cliente->estado == 0) { ?>
                      <option value="<?php echo $tipo_cliente->id; ?>"><?php echo $tipo_cliente->nombre; ?></option>
                  <?php }
                  endforeach; ?>
                </select>
              </div>
            </div>
          </div>

          <div id="tipo_cliente">

          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label>Comprobante</label>
                <?php $tipo_comprobantes = TipoComprobanteData::getAll(); ?>
                <select data-placeholder="..." name="id_tipo_comprobante" id="id_tipo_comprobante" class="form-control" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
                  <option value="">--- Selecciona ---</option>
                  <?php foreach ($tipo_comprobantes as $tipo_comprobante) : ?>
                    <option value="<?php echo $tipo_comprobante->estado; ?>"><?php echo $tipo_comprobante->nombre; ?></option>
                  <?php endforeach; ?>
                </select>
                <!--<select  data-placeholder="..."  name="id_tipo_comprobante"
                      class="form-control" style="text-transform:uppercase;"
                                    onkeyup="javascript:this.value=this.value.toUpperCase();">
                                      <option value="1">TICKET</option>
                                      <option value="2">BOLETA</option>
                                      <option value="3">FACTURA</option>
                                      
                    </select>-->

              </div>

              <div class="col-sm-6" hidden>
                <label>Fecha Comprobante</label>
                <input type="date" id="txtFechaC" name="fecha_comprobante" placeholder="" class="form-control" value="<?php echo $hoy; ?>" disabled>
              </div>

              <div class="col-sm-6">
                <label>Medio de Pago</label>
                <div id="select_plazo">
                  <?php $TPagos = TipoPago::getAll(); ?>
                  <select data-placeholder="..." id="m_pago" name="m_pago" class="form-control" style="text-transform:uppercase;">
                    <option value="">--- Selecciona ---</option>
                    <?php foreach ($TPagos as $md) : ?>
                      <option value="<?php echo $md->valor_hacienda; ?>"><?php echo $md->nombre; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

            </div>
          </div>

          <div class="form-group">
            <div class="row">

              <div class="col-sm-6">
                <label>Periodo de Pago</label>
                <div id="select_plazo">
                  <?php $plazos = PeriodosData::getAll(); ?>
                  <select data-placeholder="..." id="periodo" name="periodo" class="form-control" style="text-transform:uppercase;" onchange="CargarNV(this.value);">
                    <option value="">--- Selecciona ---</option>
                    <?php foreach ($plazos as $plazo) : ?>
                      <option value="<?php echo $plazo->id; ?>"><?php echo $plazo->nombre; ?></option>
                    <?php endforeach; ?>
                    <option value="0">Nuevo plazo +</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="col-sm">
                  <label>Num Comprobante</label>

                  <input type="number" id="num_comprobante" name="num_comprobante" placeholder="Número de comprobante" class="form-control" value="" required>

                </div>
              </div>
            </div>
          </div>
      </div>
      <input type="hidden" id="cambio_mon" name="cambio_mon" value="" >               
      <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-success
                    btn-labeled btn-block btn-ladda btn-ladda-spinner"><b><i class="fa fa-money"></i>
              </b> Guardar Venta</button>
          </div>
        </div>
      </div>
      <tbody id="resultados"></tbody>

      </form>

  </div>
  <!-- /tile body -->
  </section>
  <!-- /tile -->
</div>
<!-- Informacion Proveedor -->
</div>



</div>
<!-- /content area -->
</div>
<!-- /main content -->
</div>
<!-- /page content -->
</div>

<!-- The Modal -->
<div class="modal" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Productos con Bajo Stock</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Aviso</th>
              <th style="color:#d43f3a;">Stock</th>
            </tr>
          </thead>
          <tbody>
            <?php $productos = ProductoData::getAll();
            if (count($productos) > 0) {
            ?>
              <?php foreach ($productos as $producto) : ?>
                <?php $entrada_producto = 0; ?>
                <?php $entradas = ProcesoVentaData::getAllEntradas($producto->id);
                if (count($entradas) > 0) { ?>
                  <?php foreach ($entradas as $entrada) : $entrada_producto = $entrada->cantidad + $entrada_producto;
                  endforeach; ?>
                <?php } else {
                  $entrada_producto = 0;
                }; ?>
                <?php $salida_producto = 0; ?>
                <?php $salidas = ProcesoVentaData::getAllSalidas($producto->id);
                if (count($salidas) > 0) { ?>
                  <?php foreach ($salidas as $salida) : $salida_producto = $salida->cantidad + $salida_producto;
                  endforeach; ?>
                <?php } else {
                  $salida_producto = 0;
                }; ?>
                <?php $stock = ($producto->stock + $entrada_producto) - $salida_producto; ?>
                <?php

                if ($stock < $producto->aviso_stock) {
                  $alert_stock = true;
                ?>
                  <tr>
                    <td><?php echo $producto->nombre; ?></td>
                    <td><?php echo $producto->aviso_stock; ?></td>
                    <td><?php echo $stock; ?></td>
                  <?php } ?>
                <?php endforeach; ?>
          <tfoot class="hide-if-no-paging">
            <tr>
              <td colspan="6" class="text-center">
                <ul class="pagination"></ul>
              </td>
            </tr>
          </tfoot>

        <?php }
        ?>



        <?php
        ?>
        </tbody>
        </table>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
</div>
<!-- /page container -->
</body>

</html>



<!-- Carga los datos ajax -->

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">


        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">ACEPTAR</span></button>
        <h4 class="modal-title" id="myModalLabel">Buscar productos</h4>
      </div>
      <div class="modal-body">


        <div id="loader" style="position: absolute; text-align: center; top: 55px;  width: 100%;display:none;"></div><!-- Carga gif animado -->
        <div class="outer_div"></div><!-- Datos ajax Final -->

      </div>


    </div>
  </div>
</div>

<!-- Modal Cliente-->
<div class="modal fade bs-example-modal-lg" id="myModalC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">


        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">ACEPTAR</span></button>
        <h4 class="modal-title" id="myModalLabel">Buscar Cliente</h4>
      </div>
      <div class="modal-body">


        <div id="loader_c" style="position: absolute; text-align: center; top: 55px;  width: 100%;display:none;"></div><!-- Carga gif animado -->
        <div class="outer_div_c"></div><!-- Datos ajax Final -->

      </div>


    </div>
  </div>
</div>
<!-- The Modal -->
<div class="modal fade bs-example-modal-xm" id="myModal_NV" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-success">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><span class="fa fa-file-text">&nbsp;&nbsp;</span> Nuevo plazo</h4>
        </div>
        <div class="modal-body" style="background-color:#fff !important;">
          <div class="row">
            <div class="col-md-offset-1 col-md-10">
              <div class="form-group" style="padding-top:20px">
                <div class="input-group">
                  <span class="input-group-addon"> Nombre &nbsp;&nbsp;</span>
                  <input type="text" class="form-control col-md-8" id="nombre" name="nombre" placeholder="Nombre" required>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"> Días &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                  <input type="text" class="form-control col-md-8" id="dias" name="dias" placeholder="Días" required>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
              <input type="hidden" class="form-control" name="id_estado" value="add">
              <button type="submit" class="btn btn-outline" onclick="actualizar_plazos()">Añadir</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </div>
    <!--Final del modal  -->

    <script>
      var estado == <?php echo $alert_stock; ?>;

      if (estado == true) {
        $(function() {
          $("#myModal2").modal();
        });
      }
    </script>



    <script type="text/javascript">
      function actualizar_plazos() {
        var nombre = $('#nombre').val();
        var dias = $('#dias').val();

        parametros = {
          estado: 0,
          nombre: nombre,
          dias: dias
        };

        $.ajax({
          type: "POST",
          url: 'index.php?action=action_actualizar_plazo',
          data: parametros,
          success: function(data) {
            if (data == 0) {
              Swal.fire({
                icon: 'success',
                title: 'Registro exitoso',
                showConfirmButton: false,
                timer: 1700
              }).then(function() {
                $('#dias').val('');
                $('#nombre').val('');
                //Refrescar select periodo
                $.ajax({
                  type: "POST",
                  url: 'index.php?action=action_actualizar_plazo',
                  data: "estado=" + 1,
                  success: function(data) {
                    console.log(data);
                    $("#select_plazo").html(data);
                  }
                });
                //-----  fin del refrescar select periodo------
              })
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error al crear un nuevo plazo',
                footer: '<a href="index.php">Volver al inicio?</a>'
              }).then(function() {
                $('#dias').val('');
                $('#nombre').val('');
              })
            }
          }
        });
      }

      //Modal productos
      $(document).ready(function() {
        load(1);
        load_c(1);
        valor_moneda();
      });
    </script>

    <script src="js/vista_venta/script.js"></script>
    <script src="js/vista_venta/busqueda_cliente.js"></script>
 