<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/tablas/tablas.css" type="text/css">
<link rel="stylesheet" href="css/vista_cliente/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<body id="minovate" class="appWrapper sidebar-sm-forced">
  <div class="row">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="index.php?view=reserva"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#">Configuración</a></li>
        <li class="active">Documento de identificación</li>
      </ol>
    </section>
  </div>



  <!-- row -->
  <div class="row">
    <!-- col -->
    <div class="col-md-12">
      <section class="tile">
        <div class="tile-header dvd dvd-btm">
          <h1 class="custom-font"><strong>Funcionalidades</strong></h1>
          <ul class="controls">
            <li class="dropdown">
              <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
              </a>
              <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                <li>
                  <a role="button" tabindex="0" class="tile-toggle">
                    <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                    <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-refresh">
                    <i class="fa fa-refresh"></i> Refresh
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-fullscreen">
                    <i class="fa fa-expand"></i> Fullscreen
                  </a>
                </li>
              </ul>
            </li>
            <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <!-- tile body -->
        <div class="tile-body">
          <div class="form-group">
            <label for="filter" style="padding-top: 5px">Buscar:</label>
            <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
          </div>



          <?php $Documentos = Funcionalidad::getAll();
          if ($Documentos != null) {
            // si hay usuarios
          ?>
            <!-- Muestra de la tabla -->
            <table>
              <div class="row">
                <div class="col-md-12">
                  <div class="card card-plain">
                    <div class="card-header card-header-primary">
                      <center>
                        <h4 class="card-title mt-0">Funcionalidades</h4>
                      </center>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" class="table table-hover">
                          <thead class="">
                            <tr>
                              <th>
                                Funcionalidad
                              </th>
                              <th>
                                Estado
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($Documentos as $Documento) : ?>
                              <tr>
                                <td><?php echo $Documento->nombre; ?></td>
                                <td>
                                  <input onclick="actualizar(<?php echo $Documento->id; ?>)" type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1" style="height: 26px; margin:-2px;" <?php if ($Documento->estado == 1) echo "checked"  ?>>
                                </td>
                              </tr>

                            <?php endforeach; ?>
                          <tfoot class="hide-if-no-paging" style="left: -20px;">
                            <tr>
                              <td colspan="5" class="text-center">
                                <ul class="pagination"></ul>
                              </td>
                            </tr>
                          </tfoot>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </table>

          <?php } else {
            echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
          };
          ?>
        </div>
      </section>
    </div>
  </div>


  <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>

  <script src="assets/js/vendor/footable/footable.all.min.js"></script>


  <script>
    $(window).load(function() {

      $('.footable').footable();

    });

    function actualizar(id) {
      var parametros = {
        "id": id,
        "id_estado": "actualizar"
      };
      $.ajax({
        type: "POST",
        url: 'index.php?action=action_funcionalidades',
        data: parametros,
        success: function(data) {
          Swal.fire({
            icon: 'success',
            title: 'Actulizacion exitosa',
            showConfirmButton: false,
            timer: 1700
          })
        }
      });
    }
  </script>

  <style>
    .modal-footer {
      background-color: #ffffff !important;
    }

    .input-group .form-control {
      position: relative;
      z-index: 2;
      float: left;
      width: 70% !important;
      margin-bottom: 0;
    }

    .modal-footer {
      padding: 15px;
      text-align: right;
      border-top: 1px solid #ffffff;
    }

    .table {

      box-shadow: none;
    }
  </style>