<?php
date_default_timezone_set('America/Costa_Rica');
$hoy = date("Y-m-d");
$hora = date("H:i:s");
$anio = date("Y");
$inicio = date("Y-01-01");
$fin = date("Y-12-31");

$primer_dia = date("01-m-d");

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/vista_reportes/reportes_graficos_chartjs.scss">
  <!-- Favicon -->
  <link rel="stylesheet" href="css/vista_principal/vendor/nucleo/css/nucleo.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="css/vista_principal/css/argon.css?v=1.2.0" type="text/css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>
  <script src="js/jquery-2.2.3.min.js"></script>
  <!--<script src="js/vistas_reportes/reportes_clientes_generales.js"></script> -->

</head>

<body>
  <br>
  <br>
  <br>

  <div id="padre">
    <div id="graficos_resultado">
      <!-- Inicio de los graficos -->

      <div class="col-xl-12">
        <div class="card">
          <div class="card-header border-0">
            <div class="row align-items-center">
              <div class="col">
                <h3 style="font-size: 14px;" class="mb-0">Datos generales de Clientes</h3>
              </div>
              <div class="col text-right">
                <select name="select_tp_cl" id="select_tp_cl" style="font-size: 14px; margin-left:430px; width: 34%;" class="form-control select2">
                  <option value="1">Hospedados</option>
                  <option value="2">Externos</option>
                </select>
              </div>
            </div>
          </div>
          <div class="table-responsive" id="table-responsive">
            <!-- Projects table -->
            <!-- INICIA FOREACH -->
            <div id="tbl_cli" class="tbl_cli">
              <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">
                <thead class="thead-light">
                  <tr>
                    <th style="font-size: 12px;" scope="col">Cliente</th>
                    <th style="font-size: 12px;" scope="col">Tipo de Identificación</th>
                    <th style="font-size: 12px;" scope="col">ID</th>
                    <th style="font-size: 12px;" scope="col"># Hospedajes</th>
                    <th style="font-size: 12px;" scope="col">Ganancia por Hospedaje</th>
                    <th style="font-size: 12px;" scope="col">Ganancia por Productos</th>
                    <th style="font-size: 12px;" scope="col">Email</th>
                    <th style="font-size: 12px;" scope="col">Teléfono</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $Datos_usuario = PersonaData::get_datos_generales();
                  foreach ($Datos_usuario as $Datos_usuarios) :
                  ?>
                    <tr>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->cliente ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->documento ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->id ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->catidad_hospedajes ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->ganancia_recepcion . " ₡"; ?></td>
                      <td style="font-size: 12px;">
                        <?php if ($Datos_usuarios->ganancia_ventas != null) {
                          echo $Datos_usuarios->ganancia_ventas . " ₡";
                        } else {
                          echo "0 ₡";
                        }
                        ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->email ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->telefono ?></td>


                    </tr>
                  <?php endforeach; ?>
                </tbody>
                <tfoot class="hide-if-no-paging">
                  <tr>
                    <td colspan="9" class="text-center">
                      <ul class="pagination"></ul>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>



      <div class="row">
        <div class="col-xl-7.2">
          <div style="margin-left:25px; box-shadow: 0 1px 10px rgb(80 93 80); font-size: 14px;" class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Crecimiento de clientes en Hospedaje</h3>
                  <div name="container">
                    <select name="select_gc" id="select_gc" style="font-size: 14px; width: 24%;" class="form-control select2">
                      <option value="2">Cantidad</option>
                      <option value="1">Ganancia</option>
                    </select>
                    <select name="fecha_gc" id="fecha_gc" style="font-size: 14px; width: 24%;  margin-left: 227px; margin-top: -38px;" class="form-control select2">
                      <option value="1">Día</option>
                      <option value="2">Mes</option>
                      <option value="3">Año</option>
                    </select>
                  </div>

                  <div class="tile-body  col-xl-4" style="position:relative; left: 550px;  margin-top:-42px;">

                    <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                    <input style="font-size: 14px;" type="date" class="typeahead form-control" id="start_gc" name="start_gc" value="<?php echo date("Y-m-01");    ?>">

                    <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                    <input style="font-size: 14px;" type="date" class="typeahead form-control" id="end_gc" name="end_gc" value="<?php echo $hoy; ?>">
                  </div>
                  <!-- /tile body -->
                  <div class="tile-footer">
                    <div class="form-group text-center">
                      <button style="margin-top:15px; font-size: 14px; visibility:hidden; position:relative; left: 340px;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                    </div>
                  </div>
                  <div style="margin-bottom:-20px;" id="buyersPadre" class="chart">
                    <canvas id="buyers" width="800" height="330"></canvas>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div style="margin-right:-75px;
          box-shadow: 0 1px 10px rgb(80 93 80);" class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Variantes</h3>
                  <br>
                  <select name="select_variantes" id="select_variantes" style="font-size: 14px; width: 34%;" class="form-control select2" required>
                    <option value="1">Medio de pago</option>
                    <option value="2">Estado de pago</option>
                    <option value="3">Tipo de Documento</option>
                    <option value="4">Tipo de Comprobante</option>
                  </select>
                  <div class="tile-body col-xl-5" style="position:relative; left: 280px; margin-top:-55px;">

                    <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                    <input style="font-size: 14px;" type="date" class="typeahead form-control" id="start_var" name="start_var" value="<?php echo date("Y-m-01");  ?>">

                    <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                    <input style="font-size: 14px;" type="date" class="typeahead form-control" id="end_var" name="end_var" value="<?php echo $hoy; ?>">
                  </div>
                  <!-- /tile body -->
                  <div class="tile-footer">
                    <div class="form-group text-center">
                      <button style="margin-top:15px; font-size: 14px;  position:relative; left: 170px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                    </div>
                  </div>
                  <div style="margin-top:-40px;" id="oilChartPadre" name="oilChartPadre">
                    <canvas id="oilChart" name="oilChart" width="530" height="400"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- valores estaticos -->
      <?php

      $Lista_fecha = [];
      $Lista_cantidad = [];
      $Lista_ganancia = [];


      $opcion = 1;
      $grafico = ProcesoData::getFechaDia_mesActual($opcion);
      foreach ($grafico as $graficos) :
        array_push($Lista_fecha, $graficos->fecha);
        array_push($Lista_cantidad, $graficos->cantidad);
        array_push($Lista_ganancia, $graficos->ganancia);
      endforeach;
      ?>


      <script>
        var lista = <?php echo json_encode($Lista_fecha); ?>;
        var lista2 = <?php echo json_encode($Lista_cantidad); ?>;
        var lista3 = <?php echo json_encode($Lista_ganancia); ?>;

        // line chart data
        var ctx = document.getElementById('buyers').getContext('2d');

        var chart = new Chart(ctx, {
          // The type of chart we want to create
          type: 'line', // also try bar or other graph types

          // The data for our dataset
          data: {
            labels: lista,
            // Information about the dataset
            datasets: [{
              label: ["Cantidad"],
              backgroundColor: 'lightblue',
              borderColor: 'royalblue',
              data: lista2,
            }]

          }

        });
      </script>


      <?php


      $Lista_fecha_tipo = [];
      $Lista_cantidad_tipo = [];

      $grafico = ProcesoData::get_medio_pago();
      foreach ($grafico as $graficos) :
        array_push($Lista_fecha_tipo, $graficos->nombre);
        array_push($Lista_cantidad_tipo, $graficos->cantidad);
      endforeach;
      ?>


      <script>
        var Lista_fecha_tipo = <?php echo json_encode($Lista_fecha_tipo); ?>;
        var Lista_cantidad_tipo = <?php echo json_encode($Lista_cantidad_tipo); ?>;

        // Iniciamos con el grafico pie 
        new Chart(document.getElementById("oilChart"), {
          type: 'pie',
          data: {
            labels: Lista_fecha_tipo,
            datasets: [{
              label: "Population (millions)",
              backgroundColor: ["#122f50", "#20b9d8", "#20dab3", "#2a5d9d", "#f5eb8f"],

              data: Lista_cantidad_tipo
            }]
          },
          options: {
            title: {
              display: true,
            }
          }
        });
      </script>


      <!-- valores estaticos -->
      <?php


      $ganancia_ventas = [];
      $cantidad_ventas = [];
      $fecha = [];



      $grafico_venta = VentaData::get_grafica_ventas();
      foreach ($grafico_venta as $graficos) :
        array_push($ganancia_ventas, $graficos->ganancia_total);
        array_push($cantidad_ventas, $graficos->cantidad);
        array_push($fecha, $graficos->dia);

      endforeach;
      ?>


      <script>
        var cantidad_ventas = <?php echo json_encode($cantidad_ventas); ?>;
        var ganancia_ventas = <?php echo json_encode($ganancia_ventas); ?>;
        var fecha = <?php echo json_encode($fecha); ?>;


        // bar chart data
        var ctx = document.getElementById("income").getContext('2d');
        var barChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: fecha,
            datasets: [{
              label: 'Ganancias',
              data: ganancia_ventas,
              backgroundColor: "#829de5"
            }, {
              label: 'Cantidad',
              data: cantidad_ventas,
              backgroundColor: "#bedaed"
            }]
          }
        });
      </script>


      <!-------------------------------------------------------------- Creamos la extraccion y envio de datos en el segundo grafico  ----------------------------------------------------------------->

      <script type="text/javascript">
        //Instanciamos y limpiamos los arrays a utilizar

        // Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion

        $('#end_var').change(function() {
          var value = $(this).val();
          tiempoReal_variantes(value);
        });
        $('#select_variantes').change(function() {
          var value = $(this).val();
          tiempoReal_variantes(value);
        });
        $('#start_var').change(function() {
          var value = $(this).val();
          tiempoReal_variantes(value);
        });

        let fecha_variante = [];
        var total_variante = [];

        function tiempoReal_variantes(value) {

          // Extraemos valores para el cambio de datos en el grafico

          var dato = document.getElementById("select_variantes").value;
          var fecha_init = document.getElementById('start_var').value;
          var fecha_fin = document.getElementById('end_var').value;

          document.getElementById('oilChart').remove();
          document.getElementById('oilChartPadre').innerHTML = '<canvas id="oilChart" name="oilChart" width="530" height="400"></canvas>';

          var paso = "2";

          $.ajax({
              url: 'index.php?action=graficos_estadistica',
              type: 'POST',
              dataType: 'html',
              data: {
                fecha_fin: fecha_fin,
                dato: dato,
                fecha_init: fecha_init,
                paso: paso
              },
            })
            .done(function(resultado) {
              console.log(resultado);
              fecha_variante = [];
              total_variante = [];
              resultado1 = JSON.parse(resultado);
              resultado1.forEach(element => {
                total_variante.push(element.cantidad);
                fecha_variante.push(element.nombre);
              });

              // Iniciamos con el grafico pie 
              pai_variante(fecha_variante, total_variante); // iniciamos chart nuevamente

              function pai_variante(fecha_variante, total_variante) {
                new Chart(document.getElementById("oilChart"), {
                  type: 'pie',
                  data: {
                    labels: fecha_variante,
                    datasets: [{
                      label: "Population (millions)",
                      backgroundColor: ["#122f50", "#20b9d8", "#20dab3", "#2a5d9d", "#f5eb8f"],

                      data: total_variante
                    }]
                  },
                  options: {
                    title: {
                      display: true,
                    }
                  }
                });
              }



            })


        }
      </script>




      <!-------------------------------------------------------------- Creamos la extraccion y envio de datos en el primer grafico  ----------------------------------------------------------------->
      <script type="text/javascript">
        //Instanciamos y limpiamos los arrays a utilizar

        $('#select_tp_cl').change(function() {
          var value = $(this).val();
          TR_tabla(value);
        });

        // Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion
        $('#select_gc').change(function() {
          var value = $(this).val();
          tiempoReal(value);
        });

        $('#start_gc').change(function() {
          var value = $(this).val();
          tiempoReal(value);
        });

        $('#end_gc').change(function() {
          var value = $(this).val();
          tiempoReal(value);
        });

        $('#fecha_gc').change(function() {
          var value = $(this).val();
          tiempoReal(value);
        });

        let fecha_dato = [];
        var total_dato = [];


        function TR_tabla(value) {


          //Iniciamos con la sustitucion de informacion 
          $.ajax({
            type: "POST",
            url: 'index.php?action=tabla_cliente_reporte',
            data: 'dato=' + value,
            success: function(resp) {
              console.log(resp);
              $('#table-responsive').html(resp);
            }
          });
        }

        function tiempoReal(value) {

          document.getElementById('buyers').remove();
          document.getElementById('buyersPadre').innerHTML = '<canvas id="buyers" width="800" height="330"></canvas>';

          // Extraemos valores para el cambio de datos en el grafico
          var dato = document.getElementById("select_gc").value;
          var fecha_init = document.getElementById('start_gc').value;
          var fecha_fin = document.getElementById('end_gc').value;
          var fecha_gc = document.getElementById('fecha_gc').value;
          var paso = "1";

          $.ajax({
              url: 'index.php?action=graficos_estadistica',
              type: 'POST',
              dataType: 'html',
              data: {
                dato: dato,
                fecha_init: fecha_init,
                fecha_fin: fecha_fin,
                fecha_gc: fecha_gc,
                paso: paso
              },
            })
            .done(function(resultado) {
              fecha_dato = [];
              total_dato = [];
              resultado1 = JSON.parse(resultado);
              resultado1.forEach(element => {
                total_dato.push(element.total);
                fecha_dato.push(element.fecha);
              });
              //alert(resultado);
              //alert(fecha_dato);
              //alert(total_dato);
              var valor;
              var dato = document.getElementById("select_gc").value;
              //alert(dato);
              if (dato == '1') {
                valor = 'Ganancia';
              } else if (dato == '2') {
                valor = 'Cantidad';
              }

              function createMainChart(total_dato, fecha_dato) {
                // line chart data
                var ctx = document.getElementById('buyers').getContext('2d');
                var chart = new Chart(ctx, {
                  // The type of chart we want to create
                  type: 'line', // also try bar or other graph types

                  // The data for our dataset
                  data: {
                    labels: fecha_dato,
                    // Information about the dataset
                    datasets: [{
                      label: valor,
                      backgroundColor: 'lightblue',
                      borderColor: 'royalblue',
                      data: total_dato,
                    }]

                  }

                });
              }

              createMainChart(total_dato, fecha_dato); // iniciamos chart nuevamente

            })


        }
      </script>
      </script>


      <!---------------------------------------------------------------------------- final ----------------------------------------------------------------------------->
    </div>
  </div>


</html>




<script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>

<script src="assets/js/vendor/footable/footable.all.min.js"></script>

<script>
  $(window).load(function() {

    $('.footable').footable();

  });

  function Cargar(val) {

    $('#graficos_resultado').html("Por favor espera un momento");
    $.ajax({
      type: "POST",
      url: './?action=graficos_reportes',
      data: 'id_mostrar_cliente=' + val,
      success: function(resp) {
        document.getElementById('graficos_resultado').remove();
        document.getElementById('padre').innerHTML = '<div id="graficos_resultado"></div>';
        $('#graficos_resultado').html(resp);
      }
    });
  };
</script>