<head>
    <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/vista_cabys/style.css" type="text/css">
    <link rel="stylesheet" href="css/tablas/tablas.css" type="text/css">
    <link rel="stylesheet" href="css/vista_cliente/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>

<body id="minovate" class="appWrapper sidebar-sm-forced">
    <div class="row">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="index.php?view=reserva"><i class="fa fa-home"></i> Inicio</a></li>
                <li><a href="#">Configuración</a></li>
                <li class="active">Documento de identificación</li>
            </ol>
        </section>
    </div>

    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <section class="tile">
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font"><strong>MANTENIMIENTO DE</strong> LISTADO CABYS</h1>
                    <ul class="controls">
                        <li class="remove">
                            <div style="paddin: 8px" class="btn-1">
                                <a href="#" style="paddin: 8px" data-toggle="modal" data-target="#myModal"> <span class="fa fa-user">&nbsp;<span>Registrar</span></a>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
                            </a>
                            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                <li>
                                    <a role="button" tabindex="0" class="tile-toggle">
                                        <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                        <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-refresh">
                                        <i class="fa fa-refresh"></i> Refresh
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-fullscreen">
                                        <i class="fa fa-expand"></i> Fullscreen
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
                    </ul>
                </div>
                <!-- tile body -->
                <div class="tile-body"></div>
                <div class="form-group">
                    <label for="filter" id="bs1" name="bs1">Buscar:</label>
                    <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
                </div>



                <?php $Documentos = Auto_Cabys::getAll();
                if ($Documentos != null) {
                    // si hay usuarios
                ?>
                    <!-- Muestra de la tabla -->
                    <table>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-plain">
                                    <div class="card-header card-header-primary">
                                        <center>
                                            <h4 class="card-title mt-0"> Listado de Códigos Cabys</h4>
                                        </center>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" class="table table-hover">
                                                <thead class="">
                                                    <tr>
                                                        <th>
                                                            Descripcion
                                                        </th>
                                                        <th>
                                                            Estado
                                                        </th>
                                                        <th>
                                                            Actualizar
                                                        </th>
                                                        <th>
                                                            Eliminar
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($Documentos as $Documento) : ?>
                                                        <tr>
                                                            <td><?php echo $Documento->descripcion; ?></td>
                                                            <td>
                                                                <input onclick="actualizar_estado(<?php echo $Documento->id; ?>)" type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1" style="height: 26px; margin:-2px;" <?php if ($Documento->estado == 0) echo "checked"  ?>>
                                                            </td>
                                                            <td>
                                                                <a style="margin-top:5px; margin-bottom:-2px;" href="" data-toggle="modal" data-target="#myModal_edit<?php echo $Documento->id; ?>" class="btn btn-primary btn-block2"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                                                            </td>
                                                            <td>
                                                                <a style="margin-top:5px; margin-bottom:-2px;" onclick="del(<?php echo $Documento->id; ?>)" class="btn btn-danger btn-block2"><i class="glyphicon glyphicon-edit"></i> Eliminar</a>
                                                            </td>
                                                        </tr>
                                                        <div class="modal fade bs-example-modal-xm" id="myModal_edit<?php echo $Documento->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog modal-success">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">

                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title"><span class="fa fa-file-text">&nbsp;&nbsp;</span>ELEMENTO</h4>
                                                                        </div>
                                                                        <div class="modal-body" style="background-color:#fff !important;">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-1 col-md-10">

                                                                                    <div class="form-group" style="padding-top:20px">
                                                                                        <div class="input-group">
                                                                                            <span class="input-group-addon"> Descripción &nbsp;&nbsp;</span>
                                                                                            <input type="text" class="form-control col-md-8" id="descripcion" name="descripcion" value="<?php echo $Documento->descripcion; ?>" required placeholder="Ingresar descripción">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button style="margin-left: 35px;" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                                                                                    <button onclick="actualizar(<?php echo $Documento->id; ?>)" style="margin-left: 350px;" class="btn btn-outline">Actualizar</button>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                        </div>
                                                                        <!-- /.modal-dialog -->
                                                                    </div>
                                                                    <!-- /.modal -->
                                                                </div>

                                                            <?php endforeach; ?>
                                                <tfoot class="hide-if-no-paging" style="left: -20px;">
                                                    <tr>
                                                        <td colspan="5" class="text-center">
                                                            <ul class="pagination"></ul>
                                                        </td>
                                                    </tr>
                                                </tfoot>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </table>







                <?php } else {
                    echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
                };
                ?>

        </div>


        </section>

    </div>
    </div>

    <!-- MODAL DE BUSQUEDA  -->
    <div class="modal fade bs-example-modal-xm" id="myModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-success">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <center>
                                <h4 class="modal-title"> INGRESAR ELEMENTO</h4>
                            </center>
                        </div>
                        <div class="modal-body" style="background-color:#fff !important;">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">

                                    <div id="camp1" name="camp1" class="form-group">
                                        <center>
                                            <h5 for="filter_1" style="padding-top: 5px; font-size: 25px;">Buscar:</h5>
                                        </center>
                                        <input id="filter_1" name="filter_1" type="text" id="txtbusca" name="txtbusca" class="form-control input-sm w-sm mb-12 inline-block" />
                                    </div>
                                    <!-- <div id="camp1" name="camp1" class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"> Cabys &nbsp;&nbsp;</span>
                                            <input type="number" class="form-control col-md-8" name="nombre" required placeholder="Cabys" disabled="disabled">
                                        </div>
                                    </div>
 -->
                                </div>
                    </form>
                    <table id="loader_c" name="loader_c" data-filter="#filter_1" data-page-size="7" class="footable table table-custom2" style="font-size: 11px;"></table>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- FINAL DE MODAL DE BUSQUEDA -->
    <!-- Modal Cliente-->
    <div class="modal fade bs-example-modal-lg" id="myModalC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <!--                 <div id="loader_c" style="position: absolute; text-align: center; top: 55px;  width: 100%;display:none;"></div><!-- Carga gif animado -->
                    <div class="outer_div_c"></div><!-- Datos ajax Final -->
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //Modal productos
        $(document).ready(function() {
            load_c();
        });
    </script>

    <script type="text/javascript">
        function isNum(val) {
            return !isNaN(val)
        }

        $(document).ready(function() {
            $("#filter_1").keyup(function() {

                d = document.getElementById("filter_1").value;
                var formato = isNum(d);
                //console.log(formato); 



                // En caso de ser numerico 
                if (formato == true) {

                    var codigo = document.getElementById("filter_1").value;
                    $.ajax({
                        "url": "https://api.hacienda.go.cr/fe/cabys?codigo=" + codigo,
                        "method": "GET"
                    }).done(function(response) {
                        const lista = response;
                        parametros = {
                            Lista: lista,
                            estado: 0
                        }
                        $.ajax({
                            type: "POST",
                            url: 'index.php?action=actions_buscar_cabys',
                            data: parametros,
                            success: function(data) {
                                $("#loader_c").html(data);
                            }
                        });

                    });
                }

                // Si es string
                else {

                    var codigo = document.getElementById("filter_1").value;
                    $.ajax({
                        "url": "https://api.hacienda.go.cr/fe/cabys?q=" + codigo + '&top=10',
                        "method": "GET"
                    }).done(function(response) {
                        const lista = response;
                        parametros = {
                            Lista: lista,
                            estado: 1
                        }
                        $.ajax({
                            type: "POST",
                            url: 'index.php?action=actions_buscar_cabys',
                            data: parametros,
                            success: function(data) {
                                $("#loader_c").html(data);
                            }
                        });

                    });
                }

            })
        })
    </script>
    <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
    <script src="assets/js/vendor/footable/footable.all.min.js"></script>
    <script src="js/vista_venta/script.js"></script>
    <script src="js/vista_auto_cabys/script.js"></script>

    <style type="text/css">
        .modal-footer {
            background-color: #ffffff !important;
        }

        .input-group .form-control {
            position: relative;
            z-index: 2;
            float: left;
            width: 77% !important;
            margin-bottom: 0;
        }

        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 1px solid #ffffff;
        }

        .table {

            box-shadow: none;
        }

        #bs1 {
            padding-left: 43px !important;
        }

        .card {
            width: 95%;
            margin-left: 25px;
        }

        .table.table-custom>thead>tr td,
        .table.table-custom>thead>tr th,
        .table.table-custom>tbody>tr td,
        .table.table-custom>tbody>tr th,
        .table.table-custom>tfoot>tr td,
        .table.table-custom>tfoot>tr th {
            padding: 8px;
            padding-left: 60px !important;
        }

        .table.table-custom2>thead>tr td,
        .table.table-custom2>thead>tr th,
        .table.table-custom2>tbody>tr td,
        .table.table-custom2>tbody>tr th,
        .table.table-custom2>tfoot>tr td,
        .table.table-custom2>tfoot>tr th {
            padding-left: 23px !important;
        }

        @media (min-width: 992px) {
            .col-md-offset-1 {
                margin-left: 6.033333%;
            }
        }

        .btn {
            margin-bottom: -2px;
        }

        .input-group {
            width: 126%;
        }


        input#filter_1 {
            width: 535px;
        }


        .bs1 {
            padding-left: 43px !important;
        }


        .input-group {
            margin-left: 0px;
        }
    </style>