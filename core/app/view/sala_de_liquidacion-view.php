
<?php 
    date_default_timezone_set('America/Costa_Rica');
    $hoy = date("Y-m-d");
    $hora = date("H:i:s"); 
    $anio= date("Y");   
    $inicio=date("Y-01-01"); 
    $fin=date("Y-12-31") ;    
    $primer_dia = date("01-m-d");
?>
<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/tablas/tablas.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>       
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<body id="minovate" class="appWrapper sidebar-sm-forced">
<br><br><br>
 <!-- row --> 
<div class="row">
  <!-- col -->
  <div class="col-md-12">
    <section class="tile">
      <div class="tile-header dvd dvd-btm">  
        <h1 class="custom-font"><strong>Resumen de Liquidación</strong></h1>
        <ul class="controls">
          <li class="dropdown">
            <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
            <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
            </a>
            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                <li>
                  <a role="button" tabindex="0" class="tile-toggle">
                  <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                  <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-refresh">
                    <i class="fa fa-refresh"></i> Refresh
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-fullscreen">
                  <i class="fa fa-expand"></i> Fullscreen
                  </a>
                </li>
            </ul>
          </li>
          <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
        </ul>
      </div>
      <!-- tile body -->
      
      <div class="tile-body">
        <div class="form-group"  id="fechas" name="fechas">
          <label for="filter" style="padding-top: 5px">Buscar:</label>
          <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block"/>
          <label style="margin-left:20px;"><strong>Fecha</strong> Inicio</label>
          <input style="font-size: 14px;" type="date" class="form-control input-sm w-sm mb-12 inline-block" id="start_var" name="start_var" value="<?php echo date("Y-m-01");  ?>">
          <label style="margin-left:20px;"><strong>Fecha</strong> Fin</label>
          <input style="font-size: 14px;" type="date" class="form-control input-sm w-sm mb-12 inline-block" id="end_var"name="end_var" value="<?php echo $hoy; ?>">
        </div>
      <div>   

       <select name="fecha_gc" id="fecha_gc" style="font-size: 14px; width: 20%;  margin-left: 1100px; margin-top: -44.5px;" class="form-control input-sm w-sm mb-12 inline-block" onchange="Cargar(this.value);">  
          <option value="1" >Historial de liquidación</option>
          <option value="2" >Hoy</option>
        </select>
        <div id="padre">
        <div id="resultado">
              <?php $Documentos = CajasAperturas::resumen_liquidaciones();
                if($Documentos != null){
                  // si hay usuarios
                  ?>
                <!-- Muestra de la tabla -->
                <div id="table_padre">
                <table>
                    <div class="row">
                                <div class="col-md-12">
                                  <div class="card card-plain">
                                    <div style="background-color:#0a90cd;" class="card-header card-header-primary">
                                      <p class="card-category">Historial de Aperturas</p>
                                    </div>
                                    <div class="card-body">
                                      <div class="table-responsive">
                                        <table  id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom"  class="table table-hover">
                                          <thead class="">
                                            <tr>
                                              <th style="font-size: 12px;" scope="col">Usuario</th>
                                              <th style="font-size: 12px;" scope="col">Caja</th>                    
                                              <th style="font-size: 12px;" scope="col">Monto aperturado</th>
                                              <th style="font-size: 12px;" scope="col">Monto de Cierre</th>
                                              <th style="font-size: 12px;" scope="col">Fecha Apertura</th>
                                              <th style="font-size: 12px;" scope="col">Fecha Cierre</th>
                                              <th style="font-size: 12px;" scope="col">Historial</th>
                                          </tr>
                                          </thead>
                                          <tbody>

                                            <?php                  
                                            $init_fecha = "<script>document.writeln(document.getElementById('start_var').value);</script>";
                                            $fin_fecha = "<script>document.writeln(document.getElementById('end_var').value);</script>";
                                            $Datos_usuario = CajasAperturas::resumen_liquidaciones_fechas(date("Y-m-01"),$hoy);
                                            foreach($Datos_usuario as $Datos_usuarios):
                                            ?>
                                            <tr>
                                            <td><?php echo $Datos_usuarios->usuario ?></td>
                                            <td><?php echo $Datos_usuarios->numero_caja ?></td>
                                            <td><?php echo $Datos_usuarios->apertura ?></td>
                                            <td><?php echo $Datos_usuarios->cierre ?></td>
                                            <td><?php echo $Datos_usuarios->hora_inicio?></td>
                                            <td><?php echo $Datos_usuarios->hora_cierre?></td>
                                            <td>
                                                    <a href=""  data-toggle="modal" data-target="#myModal_edit<?php echo $Datos_usuarios->id ?>"  class="btn btn-primary btn-block2"><i class="glyphicon glyphicon-edit"></i> Mostrar Historial</a>
                                            </td>
                                          </tr>

      <div class="modal fade bs-example-modal-xm" id="myModal_edit<?php echo $Datos_usuarios->id; ?>" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-warning">
          <div class="modal-dialog">
            <div class="modal-content">
            <form class="form-horizontal" method="post" id="addproduct" action="index.php?action=actions_documento" role="form">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span></span>Resumen de liquidación</h4>
              </div>
               <?php $datos_historial=CajasAperturas::resumen_liquidaciones_historial_5($Datos_usuarios->id);  ?>  

              <div class="tile-body">
               <?php 'si sumando todo no me da el resultado esperado coloco una condicion'?>     
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Fecha:</label> <label style="font-size: 18px;margin-left:240px; color:blue;" for=""><?php echo $datos_historial->fecha ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Hora de apertura:</label><label style="font-size: 18px;margin-left:157px; color:blue;" for="">
                <?php 
                                  echo $datos_historial->hora_inicial;

                  ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Apertura caja:</label><label style="font-size: 18px;margin-left:185px; color:blue;" for="">
                <?php 
                                  echo '₡ '.$datos_historial->monto_apertura;

                  ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Alquiler habitación: </label><label style="font-size: 18px;margin-left:145px; color:blue;" for="">
                <?php 
                  $historial = intval($datos_historial->habitacion_total); 
                  $cobro_otro = intval($datos_historial->cobro_ext_diferente_proceso); 
                  $total  = $historial - $cobro_otro; 
                 echo '₡ '. $total; if($datos_historial->cobro_ext > 0){ echo "<br>Cobro extra: ".$datos_historial->cobro_ext; }
                  ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Cobro extra de habitación: </label><label style="font-size: 18px;margin-left:83px; color:blue;" for="">
                <?php 
                echo '₡ '.$datos_historial->cobro_ext_diferente_proceso2;
                ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Ventas: </label><label style="font-size: 18px;margin-left:237px; color:blue;" for="">
                <?php 
                                 echo '₡ '.$datos_historial->venta;

                ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Compras: </label><label style="font-size: 18px;margin-left:218px; color:blue;" for="">
                 <?php 
                  echo '₡ '.$datos_historial->compra;
             
                ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Egresos: </label><label style="font-size: 18px;margin-left:225px; color:blue;" for="">
                <?php 
                  echo '₡ '.$datos_historial->gasto;
                ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Hora de cierre:</label> <label style="font-size: 18px;margin-left:172px; color:blue;" for=""> 
                <?php 
                  echo $datos_historial->hora_cierre;
                ?></label>
              </div>
              <div class="form-group">
                <label style="font-size: 18px; margin-left:50px;" for="filter" style="padding-top: 5px">Monto de Cierre:</label> <label style="font-size: 18px;margin-left:160px; color:green;" for="">
                <?php 
                  echo '₡ '.$datos_historial->monto_cierre;

                ?></label>
              </div>

              <div> 
              <div class="modal-footer">
                <button style="font-size: 18px; margin-left:px; margin-bottom:-20px;" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
              </div>
            </form>
           
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>

      <?php endforeach; ?>
      
        <tfoot class="hide-if-no-paging">
        <tr>
            <td colspan="7" class="text-center">
            <ul class="pagination"></ul>
            </td>
        </tr>
        </tfoot>
                                          
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                    </table>
                    </div>






               <?php }else{ 
            echo"<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";

                };
                ?>

           </div>    


</section>

</div>
</div>
     
    </div>
    </div></div>
       <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
        
        <script src="assets/js/vendor/footable/footable.all.min.js"></script>
      
        
 <script>
 
$(window).load(function(){

   $('.footable').footable();
   
 });

 function del($id){
  Swal.fire({
  title: 'Estas seguro?',
  text: "",
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar'
}).then((result) => {
  if (result.isConfirmed) {
    var parametros={"id":$id,"id_estado":"delete"}; 
    $('#tipo_cliente').html("Por favor espera un momento");    
        $.ajax({
            type: "POST",
            url: 'index.php?action=actions_banco',
            data: parametros,
            success:function(data){
              window.location.reload(); // Recargar página
              }
        });
    //window.location="index.php?view=delete_documento&id="+$id
  }
 
})
}

</script>

<style>
.modal-footer {
    background-color: #ffffff!important;
}
.input-group .form-control {
    position: relative;
    z-index: 2;
    float: left;
    width: 70%!important;
    margin-bottom: 0;
}
.modal-footer {
    padding: 15px;
    text-align: right;
    border-top: 1px solid #ffffff;
}
.table {

    box-shadow:none;
}
</style>

<script>
  $('#fecha_gc').change(function(){ 
    var id_select = document.getElementById("fecha_gc").value;
    if(id_select == 2){
      document.getElementById('fechas').style.visibility='hidden';
    }else{
      document.getElementById('fechas').style.visibility='visible';
    }
  });
  $('#start_var').change(function(){ 
    var value = $(this).val();
    cargarTable(value);
  });
  $('#end_var').change(function(){ 
      var value = $(this).val();
      cargarTable(value);
  });
  function cargarTable(value){
    var fecha_init = document.getElementById("start_var").value;
    var fecha_fin = document.getElementById("end_var").value;
    var id = document.getElementById("fecha_gc").value;
    $.ajax({  
        url:'index.php?action=resumen_liquidacion',  
        method:"POST",  
        data:{fecha_init:fecha_init, fecha_fin:fecha_fin, id:id},  
        success:function(data)  
        {  

             $('#table_padre').html(data);  
             
        }  
    });  
  }
  //Realizamos el cambio de datos especificos por caja en proceso.
  function Cargar_datos_en_proceso()
    {   
        var id_caja = document.getElementById("caja_abierta").value;
        var id = 3;
        $.ajax({
            type: "POST",
            url: './?action=resumen_liquidacion',
            data:{id:id, id_caja:id_caja},  
            success: function(resp){
              document.getElementById('contenedor_hijo').remove();
                document.getElementById('contenedor_padre').innerHTML = '<div style="margin-top:-50px; margin-left:70px" name="contenedor_hijo" id="contenedor_hijo"></div>';
                $('#contenedor_hijo').html(resp);
                //alert(id_caja);
            }
        });
    };

    //Cargamos las secciones resumen de liquidacion y historial del dia
    function Cargar(val)
    {   
        var fecha_init = document.getElementById("start_var").value;
        var fecha_fin = document.getElementById("end_var").value;
        $('#graficos_resultado').html("Por favor espera un momento");    
        $.ajax({
            type: "POST",
            url: './?action=resumen_liquidacion',
            data: {id:val, fecha_init:fecha_init, fecha_fin:fecha_fin},   
            success: function(resp){
              document.getElementById('resultado').remove();
                document.getElementById('padre').innerHTML = '<div id="resultado"></div>';
                $('#resultado').html(resp);
            }
        });
    };

</script>







