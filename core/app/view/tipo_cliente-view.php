<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<body id="minovate" class="appWrapper sidebar-sm-forced">
  <div class="row">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="index.php?view=reserva"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#">Configuración</a></li>
        <li class="active">Tipo Cliente</li>
      </ol>
    </section>
  </div>


  <!-- row -->
  <div class="row">
    <!-- col -->
    <div class="col-md-12">
      <section class="tile">
        <div class="tile-header dvd dvd-btm">
          <h1 class="custom-font"><strong>MANTENIMIENTO DE</strong> TIPO DE CLIENTE</h1>
          <ul class="controls">
            <li class="remove">
              <div style="paddin:8px" class="btn-1">
                <!--<a href="#" style="paddin: 8px" data-toggle="modal" data-target="#myModal"> <span class="fa fa-user">&nbsp;<span>Registrar</span></a>-->
                <a href="index.php?view=tipo_cliente_estado" style="paddin:8px" data-toggle="modal"> <span class="fa fa-user">&nbsp;<span>Inactivos</span></a>
              </div>
            </li>
            <li class="dropdown">
              <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
              </a>
              <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                <li>
                  <a role="button" tabindex="0" class="tile-toggle">
                    <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                    <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-refresh">
                    <i class="fa fa-refresh"></i> Refresh
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-fullscreen">
                    <i class="fa fa-expand"></i> Fullscreen
                  </a>
                </li>
              </ul>
            </li>
            <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <!-- tile body -->
        <div class="tile-body">
          <div class="form-group">
            <label for="filter" style="padding-top: 5px">Buscar:</label>
            <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
          </div>



          <?php $Documentos = TipoCliente::getAll();
          if ($Documentos != null) {
            // si hay usuarios
          ?>
            <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">

              <thead style="color: white; background-color: #827e7e;">
                <th>NOMBRE</th>
                <th>Actualizar</th>
                <th>Desactivar</th>

                <!--
                        <th></th>
                      -->
              </thead>
              <?php foreach ($Documentos as $Documento) :
                if($Documento->estado == 0){
                  
                ?>
                <tr>
                  <td><?php echo $Documento->nombre; ?></td>
                  <td>
                    <a href="" data-toggle="modal" data-target="#myModal_edit<?php echo $Documento->id; ?>" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                  </td>
                  <td>
                    <a onclick="desac(<?php echo $Documento->id; ?>)" class="btn btn-blue btn-xs"><i class="glyphicon glyphicon-edit"></i> Desactivar</a>
                  </td>
                  <!--<td> <a onclick="del(<?php echo $Documento->id; ?>)" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></td>-->
                </tr>





                <div class="modal fade bs-example-modal-xm" id="myModal_edit<?php echo $Documento->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-warning">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <form class="form-horizontal" method="post" id="addproduct" action="index.php?action=actions_tipo_cliente" role="form">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><span class="fa fa-sitemap">&nbsp;&nbsp;</span>Actualizar medio</h4>
                          </div>
                          <div class="modal-body" style="background-color:#fff !important;">

                            <div class="row">
                              <div class="col-md-offset-1 col-md-10">

                                <div class="form-group">
                                  <div class="input-group">
                                    <span class="input-group-addon"> Estado &nbsp;&nbsp;</span>
                                    <input type="text" class="form-control col-md-8" name="nombre" value="<?php echo $Documento->nombre; ?>" required placeholder="Ingrese nombre">
                                  </div>
                                  <br>
                                  <div class="input-group">
                                    <span class="input-group-addon"> Categoria &nbsp;&nbsp;</span>
                                    <select style="font-size: 14px; width: 51%;" class="form-control col-md-8" name="campo" id="campo" required>
                                      <option value="0" <?php if ($Documento->valor_extra) echo "selected"; ?>>Cliente Externo</option>
                                      <option value="1" <?php if ($Documento->valor_extra) echo "selected"; ?>>Cargar a una Habitación</option>
                                      <option value="2" <?php if ($Documento->valor_extra == 2) echo "selected"; ?>>Cliente Existente</option>
                                    </select>
                                  </div>

                                  <!--<div class="checkbox" >
                        <label><input type="checkbox" id="value1" name="campo" value="1">Nombre del cliente&nbsp;&nbsp;</label>
                        <label><input type="checkbox" id="value2" name="campo" value="0">Cargar a una Habitación</label>
                      </div> -->
                                  <br>
                                </div>
                                <input type="hidden" class="form-control" name="precio" value="0" required placeholder="Ingresar Medio">
                              </div>
                            </div>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                            <input type="hidden" class="form-control" name="id_documento" value="<?php echo $Documento->id; ?>">
                            <input type="hidden" class="form-control" name="id_estado" value="update">
                            <button type="submit" class="btn btn-outline">Actualizar Datos</button>
                          </div>
                        </form>

                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->
                </div>





              <?php
                } endforeach; ?>
              <tfoot class="hide-if-no-paging" style="left: -20px;">
                <tr>
                  <td colspan="5" class="text-center">
                    <ul class="pagination"></ul>
                  </td>
                </tr>
              </tfoot>
            </table>

          <?php } else {
            echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
          };
          ?>

        </div>


      </section>

    </div>
  </div>

  <div class="modal fade bs-example-modal-xm" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-success">
      <div class="modal-dialog">
        <div class="modal-content">
          <form class="form-horizontal" method="post" id="addproduct" action="index.php?action=actions_tipo_cliente" role="form">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><span class="fa fa-file-text">&nbsp;&nbsp;</span> INGRESA UN NUEVO TIPO DE CLIENTE</h4>
            </div>
            <div class="modal-body" style="background-color:#fff !important;">
              <div class="row">
                <div class="col-md-offset-1 col-md-10">
                  <div class="form-group" style="padding-top:20px">
                    <div class="input-group">
                      <span class="input-group-addon"> Estado &nbsp;&nbsp;</span>
                      <input type="text" class="form-control col-md-8" name="nombre" required placeholder="Nombre">
                    </div>
                    <br>
                    <div class="input-group">
                      <span class="input-group-addon"> Estado &nbsp;&nbsp;</span>
                      <select style="font-size: 14px; width: 51%;" class="form-control col-md-8" name="campo" id="campo" required>
                        <option value="0">Cliente Externo</option>
                        <option value="1">Cargar a una Habitación</option>
                        <option value="2">Cliente Existente</option>
                      </select>
                    </div>
                    <!--<div class="checkbox" >
                        <label><input type="checkbox" id="value1" name="campo" value="1">Nombre del cliente&nbsp;&nbsp;</label>
                        <label><input type="checkbox" id="value2" name="campo" value="0">Cargar a una Habitación</label>
                      </div> -->
                    <br>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                  <input type="hidden" class="form-control" name="id_estado" value="add">
                  <button type="submit" class="btn btn-outline">Añadir</button>
                </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>

  <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>

  <script src="assets/js/vendor/footable/footable.all.min.js"></script>


  <script>
    $(window).load(function() {

      $('.footable').footable();

    });

    function desac(id) {
      Swal.fire({
        title: 'Estas seguro?',
        text: "",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
      }).then((result) => {
        if (result.isConfirmed) {
          var parametros = {
            "id": id,
            "id_estado": "desactivar"
          };
          $('#tipo_cliente').html("Por favor espera un momento");
          $.ajax({
            type: "POST",
            url: 'index.php?action=actions_tipo_cliente',
            data: parametros,
            success: function(data) {
              window.location.reload(); // Recargar página
            }
          });
        }

      })
    }
  </script>

  <style>
  </style>