<?php
date_default_timezone_set('America/Costa_Rica');
$hoy = date("Y-m-d");
$hora = date("H:i:s");
$anio = date("Y");
$inicio = date("Y-01-01");
$fin = date("Y-12-31");

$primer_dia = date("01-m-d");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vista_reportes/reportes_graficos_chartjs.scss">
    <!-- Favicon -->
    <link rel="stylesheet" href="css/vista_principal/vendor/nucleo/css/nucleo.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="css/vista_principal/css/argon.css?v=1.2.0" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>
    <script src="js/jquery-2.2.3.min.js"></script>

</head>

<body>
    <br>
    <br>
    <br>
    <div id="padre">
        <div id="graficos_resultado">
            <!-- Inicio de los graficos -->
                <div class="col-xl-12">
                    <div style="margin-left:5px; margin-right:5px; box-shadow: 0 1px 10px rgb(80 93 80); font-size: 14px;" class="card">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Ganancias por productos</h3>
                                    <br>
                                    <?php
                                    $productos = ProductoData::getAll();
                                    ?>
                                    <select id="select_producto" name="select_producto" style="font-size: 14px; width: 24%;" class="form-control select2" required>
                                        <option value="1">Todos</option>
                                        <?php foreach ($productos as $producto) : ?>
                                            <option value="<?php echo $producto->id ?>"><?php echo $producto->nombre ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                    <div class="tile-body  col-xl-3" style="position:relative; left:970px; margin-top:-50px;">

                                        <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                                        <input style="font-size: 14px;" type="date" class="typeahead form-control" name="start_prod" id="start_prod" value="<?php echo date("Y-m-01"); ?>">

                                        <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                                        <input style="font-size: 14px;" type="date" class="typeahead form-control" name="end_prod" id="end_prod" value="<?php echo $hoy; ?>">
                                    </div>
                                    <!-- /tile body -->
                                    <div class="tile-footer">
                                        <div class="form-group text-center">
                                            <button style="margin-top:15px; font-size: 14px;  position:relative; left: 567px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                        </div>
                                    </div>
                                    <div id="incomePadre">
                                        <canvas id="income" width="1200" height="300"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-xl-12">
            <div style="margin-left:5px; margin-right:5px; box-shadow: 0 1px 10px rgb(80 93 80); font-size: 14px;" class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Ganancias por productos</h3>
                            <br>
                            <select id="select_tipo" name="select_tipo" style="font-size: 14px; width: 24%;" class="form-control select2" required>
                                <option value="1">Todos</option>
                                <option value="1">Ganancias</option>
                                <option value="2">Cantidad</option>
                            </select>
                            <select id="producto1" name="producto1" style="font-size: 14px; width: 15%; margin-left: 367px; margin-top: -38px;" class="form-control select2" required>
                                <?php foreach ($productos as $producto) : ?>
                                    <option value="<?php echo $producto->id ?>"><?php echo $producto->nombre ?></option>
                                <?php endforeach; ?>
                            </select>
                            <select id="producto2" name="producto2" style="font-size: 14px; width: 15%; margin-left: 597px; margin-top: -38px;" class="form-control select2" required>
                                <?php foreach ($productos as $producto) : ?>
                                    <option value="<?php echo $producto->id ?>"><?php echo $producto->nombre ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="tile-body  col-xl-3" style="position:relative; left:970px; margin-top:-50px;">

                                <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" name="start_prod2" id="start_prod2" value="<?php echo date("Y-m-01"); ?>">

                                <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" name="end_prod2" id="end_prod2" value="<?php echo $hoy; ?>">
                            </div>
                            <!-- /tile body -->
                            <div class="tile-footer">
                                <div class="form-group text-center">
                                    <button style="margin-top:15px; font-size: 14px;  position:relative; left: 567px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                </div>
                            </div>
                            <div id="mylineGraphPadre">
                                <canvas id="mylineGraph" style="margin-top:-65px;" width="80" height="29px"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!-- valores estaticos -->
    <?php

    $Lista_fecha = [];
    $Lista_cantidad = [];
    $Lista_ganancia = [];


    $opcion = 1;
    $grafico = ProcesoData::getFechaDia_mesActual($opcion);
    foreach ($grafico as $graficos) :
        array_push($Lista_fecha, $graficos->fecha);
        array_push($Lista_cantidad, $graficos->cantidad);
        array_push($Lista_ganancia, $graficos->ganancia);
    endforeach;
    ?>


    <script>
        var lista = <?php echo json_encode($Lista_fecha); ?>;
        var lista2 = <?php echo json_encode($Lista_cantidad); ?>;
        var lista3 = <?php echo json_encode($Lista_ganancia); ?>;

        // line chart data
        var ctx = document.getElementById('buyers').getContext('2d');

        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line', // also try bar or other graph types

            // The data for our dataset
            data: {
                labels: lista,
                // Information about the dataset
                datasets: [{
                    label: ["Cantidad"],
                    backgroundColor: 'lightblue',
                    borderColor: 'royalblue',
                    data: lista2,
                }]

            }

        });
    </script>
    <?php
    //-----------------------------Grafico 4 estatico ---------------------

    $Lista_fecha_estatic = [];
    $Lista_cantidad_estatic = [];
    $Lista_ganancia1_estatic = [];
    $Lista_ganancia2_estatic = [];
    $nombres_estatic = [];
    $productos_lista_estatic = [];
    foreach ($productos as $producto_estatic) :
        array_push($productos_lista_estatic, $producto_estatic->id);
    endforeach;

    $fecha_ini_estatic = date("Y-m-01");
    $fecha_fin_estatic = $hoy;
    $grafico4_estatic = ProcesoData::get_ganancia_productos($fecha_ini_estatic, $fecha_fin_estatic, $productos_lista_estatic[0]);
    foreach ($grafico4_estatic as $graficos) :
        array_push($Lista_fecha_estatic, $graficos->fecha);
        array_push($Lista_ganancia1_estatic, $graficos->ganancia);
        array_push($nombres_estatic, $graficos->nombre);
    endforeach;
    ?>
    <script>
        let nombre_producto1_estatic = <?php echo json_encode($nombres_estatic); ?>;
        let total_producto1_estatic = <?php echo json_encode($Lista_ganancia1_estatic); ?>;
        let fecha_rango_estatic = <?php echo json_encode($Lista_fecha_estatic); ?>;

        var line = document.getElementById("mylineGraph").getContext("2d");

        var mylineGraph = new Chart(line, {
            type: "line",
            data: {
                labels: fecha_rango_estatic,
                datasets: [{
                        label: nombre_producto1_estatic[0],
                        data: total_producto1_estatic,
                        fill: true,
                        borderColor: "rgba(55, 130, 220, .65)",
                        backgroundColor: "rgba(55, 130, 220, 0.1)",
                        lineTension: 0,
                        pointBorderWidth: 2,
                        borderDash: [5, 5],
                        pointStyle: "rectRounded"
                    }
                    /*
                          {
                            label: nombre_producto2,
                            data: total_producto2,
                            fill: false,
                            borderColor: "rgba(245, 35, 56, .65)",
                            lineTension: 0,
                            pointBorderWidth: 2,
                            pointStyle: "rectRounded"
                          }*/
                ]
            },
            options: {
                title: {
                    display: true,
                    text: "Gráfico de ventas"
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                }
            }
        });




        // -----------------------------------------Fin -------------------------
    </script>

    <?php


    $Lista_fecha_tipo = [];
    $Lista_cantidad_tipo = [];

    $grafico = ProcesoData::get_medio_pago();
    foreach ($grafico as $graficos) :
        array_push($Lista_fecha_tipo, $graficos->nombre);
        array_push($Lista_cantidad_tipo, $graficos->cantidad);
    endforeach;
    ?>


    <script>
        var Lista_fecha_tipo = <?php echo json_encode($Lista_fecha_tipo); ?>;
        var Lista_cantidad_tipo = <?php echo json_encode($Lista_cantidad_tipo); ?>;

        // Iniciamos con el grafico pie 
        new Chart(document.getElementById("oilChart"), {
            type: 'pie',
            data: {
                labels: Lista_fecha_tipo,
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#122f50", "#20b9d8", "#20dab3", "#2a5d9d", "#f5eb8f"],

                    data: Lista_cantidad_tipo
                }]
            },
            options: {
                title: {
                    display: true,
                }
            }
        });
    </script>


    <!-- valores estaticos -->
    <?php


    $ganancia_ventas = [];
    $cantidad_ventas = [];
    $fecha = [];



    $grafico_venta = VentaData::get_grafica_ventas();
    foreach ($grafico_venta as $graficos) :
        array_push($ganancia_ventas, $graficos->ganancia_total);
        array_push($cantidad_ventas, $graficos->cantidad);
        array_push($fecha, $graficos->dia);

    endforeach;
    ?>


    <script>
        var cantidad_ventas = <?php echo json_encode($cantidad_ventas); ?>;
        var ganancia_ventas = <?php echo json_encode($ganancia_ventas); ?>;
        var fecha = <?php echo json_encode($fecha); ?>;


        // bar chart data
        var ctx = document.getElementById("income").getContext('2d');
        var barChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: fecha,
                datasets: [{
                    label: 'Ganancias',
                    data: ganancia_ventas,
                    backgroundColor: "#829de5"
                }, {
                    label: 'Cantidad',
                    data: cantidad_ventas,
                    backgroundColor: "#bedaed"
                }]
            }
        });
    </script>




    <!-------------------------------------------------------------- Creamos la extraccion y envio de datos en el Tercer grafico  ----------------------------------------------------------------->

    <script type="text/javascript">
        //Instanciamos y limpiamos los arrays a utilizar

        // Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion
        $('#select_producto').change(function() {
            var value = $(this).val();
            tiempoReal_productos(value);
        });
        $('#start_prod').change(function() {
            var value = $(this).val();
            tiempoReal_productos(value);
        });
        $('#end_prod').change(function() {
            var value = $(this).val();
            tiempoReal_productos(value);
        });



        let fecha_producto = [];
        var total_producto = [];
        var cantidad_producto = [];

        function tiempoReal_productos(value) {

            // Extraemos valores para el cambio de datos en el grafico

            var dato = document.getElementById("select_producto").value;
            var fecha_init = document.getElementById('start_prod').value;
            var fecha_fin = document.getElementById('end_prod').value;

            document.getElementById('income').remove();
            document.getElementById('incomePadre').innerHTML = '<canvas id="income" name="income" width="1200" height="300"></canvas>';


            var paso = "3";

            $.ajax({
                    url: 'index.php?action=graficos_estadistica',
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        dato: dato,
                        fecha_init: fecha_init,
                        fecha_fin: fecha_fin,
                        paso: paso
                    },
                })
                .done(function(resultado) {
                    fecha_producto = [];
                    total_producto = [];
                    cantidad_producto = [];
                    resultado1 = JSON.parse(resultado);
                    resultado1.forEach(element => {
                        total_producto.push(element.total);
                        fecha_producto.push(element.fecha);
                        cantidad_producto.push(element.cantidad);
                    });


                    // Iniciamos con el grafico nuevamente

                    function lista_Produtos(fecha_producto, total_producto, cantidad_producto) {
                        var ctx = document.getElementById("income").getContext('2d');
                        var barChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: fecha_producto,
                                datasets: [{
                                    label: 'Ganancias',
                                    data: total_producto,
                                    backgroundColor: "#829de5"
                                }, {
                                    label: 'Cantidad',
                                    data: cantidad_producto,
                                    backgroundColor: "#bedaed",
                                }]
                            }
                        });
                    }

                    lista_Produtos(fecha_producto, total_producto, cantidad_producto); // iniciamos chart nuevamente


                })


        }
    </script>
    <script type="text/javascript">
        //---------------------------------------------------------------------------- Grafico #4 ------------------------------------------------------------------------------------------------
        // Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion
        $('#select_tipo').change(function() {
            var value = $(this).val();
            tiempoReal_gc(value);
        });
        $('#start_prod2').change(function() {
            var value = $(this).val();
            tiempoReal_gc(value);
        });
        $('#end_prod2').change(function() {
            var value = $(this).val();
            tiempoReal_gc(value);
        });
        $('#producto1').change(function() {
            var value = $(this).val();
            tiempoReal_gc(value);
        });
        $('#producto2').change(function() {
            var value = $(this).val();
            tiempoReal_gc(value);
        });

        var nombre_producto1;
        var nombre_producto2;
        var total_producto1 = [];
        var total_producto2 = [];
        let fecha_rango = [];

        function tiempoReal_gc(value) {
            // Extraemos valores para el cambio de datos en el grafico
            var dato = document.getElementById("select_tipo").value;
            var fecha_init = document.getElementById('start_prod2').value;
            var fecha_fin = document.getElementById('end_prod2').value;
            var producto1 = document.getElementById('producto1').value;
            var producto2 = document.getElementById('producto2').value;
            document.getElementById('mylineGraph').remove();
            document.getElementById('mylineGraphPadre').innerHTML = '<canvas id="mylineGraph" name="mylineGraph" style="margin-top:-65px;" width="80" height="29px"></canvas>';


            var paso = "4";

            $.ajax({
                    url: 'index.php?action=graficos_estadistica',
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        producto1: producto1,
                        producto2: producto2,
                        fecha_init: fecha_init,
                        fecha_fin: fecha_fin,
                        paso: paso
                    },
                })
                .done(function(resultado) {
                    total_producto1 = [];
                    total_producto2 = [];
                    fecha_rango = [];
                    resultado1 = JSON.parse(resultado);

                    resultado1.forEach(element => {
                        if (element.id == producto1) {

                            nombre_producto1 = element.nombre;
                            if (dato == 1) {
                                total_producto1.push(element.ganancia);
                            } else if (dato == 2) {
                                total_producto1.push(element.cantidad);
                            }
                        } else if (element.id == producto2) {
                            nombre_producto2 = element.nombre;
                            if (dato == 1) {
                                total_producto2.push(element.ganancia);
                            } else if (dato == 2) {
                                total_producto2.push(element.cantidad);
                            }
                        }
                        fecha_rango.push(element.fecha);
                    });


                    // Iniciamos con el grafico nuevamente

                    function lista_Productos(nombre_producto1, nombre_producto2, total_producto1, total_producto2, fecha_rango) {

                        var line = document.getElementById("mylineGraph").getContext("2d");
                        var mylineGraph = new Chart(line, {
                            type: "line",
                            data: {
                                labels: fecha_rango,
                                datasets: [{
                                        label: nombre_producto1,
                                        data: total_producto1,
                                        fill: true,
                                        borderColor: "rgba(55, 130, 220, .65)",
                                        backgroundColor: "rgba(55, 130, 220, 0.1)",
                                        lineTension: 0,
                                        pointBorderWidth: 2,
                                        borderDash: [5, 5],
                                        pointStyle: "rectRounded"
                                    },
                                    {
                                        label: nombre_producto2,
                                        data: total_producto2,
                                        fill: true,
                                        borderColor: "rgba(55, 130, 220, .65)",
                                        backgroundColor: "rgba(55, 130, 220, 0.1)",
                                        lineTension: 0,
                                        pointBorderWidth: 2,
                                        borderDash: [5, 5],
                                        pointStyle: "rectRounded"
                                    }

                                ]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: "Gráfico de ventas"
                                },
                                plugins: {
                                    datalabels: {
                                        display: false
                                    }
                                }
                            }
                        });

                    }

                    lista_Productos(nombre_producto1, nombre_producto2, total_producto1, total_producto2, fecha_rango); // iniciamos chart nuevamente

                })


        }
    </script>

    <!---------------------------------------------------------------------------- final ----------------------------------------------------------------------------->

    </div>
    </div>


</html>




<script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>

<script src="assets/js/vendor/footable/footable.all.min.js"></script>

<script>
    $(window).load(function() {

        $('.footable').footable();

    });

    function Cargar(val) {

        $('#graficos_resultado').html("Por favor espera un momento");
        $.ajax({
            type: "POST",
            url: './?action=graficos_reportes',
            data: 'id_mostrar_cliente=' + val,
            success: function(resp) {
                document.getElementById('graficos_resultado').remove();
                document.getElementById('padre').innerHTML = '<div id="graficos_resultado"></div>';
                $('#graficos_resultado').html(resp);
            }
        });
    };
</script>