<!-- Espaciamos -->
<br><br><br>
<!-- - -->
<div class="container my-5">
  <link rel="stylesheet" href="css/vista_Backup/style.css">
  <link rel="stylesheet" href="css/vista_productos_excel/style.css">
  <link rel="stylesheet" href="css/vista_Backup/botones.css">
  <link rel="stylesheet" href="css/vista_Backup/seccion_eliminar.css">

  <section>
    <div class="text-center mb-5">
      <h2 class="font-weight-bold display-4 ">Formato <span style="color: #48a2ef;">Excel</span></h2>
      <div class="archivo" name="archivo" id="archivo"> </div>
    </div>
  </section>
</div>
<!-- Espaciamos -->
<section>
  <div class="text-center">
    <h2 class="font-weight-bold">Ejemplo del Orden y <span style="color: #48a2ef;">Formato</span></h2>

    <section style=" margin-top:-70px;">
      <div class="blog-card python">
        <div class="content-mask">
          <h2>Indicaciónes a <spam style="color: #48a2ef;">tomar en cuenta</spam></h2>
          <div class="content-list">
              <li> Se pueden dejar en blanco los espacios que no poseen.</li>
              <li> Se debe de ingresar el nombre de la bodega creada dentro de la columna Bodega.</li>
              <li> Se debe de ingresar el nombre de la unidad de medida creada dentro de la columna<br> Unidad de Medida.</li>
              <li> Se debe de ingresar el valor del descuento creado dentro de la columna Descuento.</li>
              <li> Se debe de ingresar el valor del impuesto creado dentro de la columna Impuestos,<br> si se desea agregar mas de un impuesto este debe ir separado por comas.</li>
          </div>
          <button type="button" class="python category" data-toggle="modal" data-target="#myModale">
            <span class="glyphicon"></span>Ver Ejemplo
          </button>
        </div>
        <div class="horizontal"></div>
      </div>
    </section>
  </div>
</section>
<!-- Modal Cliente-->
<div class="modal fade bs-example-modal-lg" id="myModale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="container_table">
    <table>
      <thead>
        <tr>
          <th>Codigo</th>
          <th>Codigo barra</th>
          <th>Nombre</th>
          <th>Marca</th>
          <th>Descripcion</th>
          <th>Acreditable</th>
          <th>Presentacion</th>
          <th>Presio Compra</th>
          <th>Presio Venta</th>
          <th>Stock Inicial</th>
          <th>Aviso Stock Min</th>
          <th>Aviso Stock Max</th>
          <th>Utilidad</th>
          <th>Bodega</th>
          <th>Unidad de Medida</th>
          <th>Descuento</th>
          <th>Impuestos</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>8399000000000</td>
          <td>F32PB001</td>
          <td>Papiolas</td>
          <td>Struck</td>
          <td>Papas</td>
          <td>Si</td>
          <td>Papiolas</td>
          <td>1000</td>
          <td>2000</td>
          <td>100</td>
          <td>10</td>
          <td>500</td>
          <td>10</td>
          <td>Bodega 1</td>
          <td>g</td>
          <td>10</td>
          <td>5,12</td>
        </tr>
        <tr>
          <td>8399000000000</td>
          <td>F32PB001</td>
          <td>Ranchitas</td>
          <td>Struck</td>
          <td>Papas</td>
          <td>Si</td>
          <td>Papiolas</td>
          <td>1000</td>
          <td>2000</td>
          <td>100</td>
          <td>10</td>
          <td>500</td>
          <td>10</td>
          <td>Bodega 1</td>
          <td>g</td>
          <td>10</td>
          <td>5,12</td>
        </tr>
        <tr>
          <td>8399000000000</td>
          <td>F32PB001</td>
          <td>Quesitos</td>
          <td>Struck</td>
          <td>Papas</td>
          <td>Si</td>
          <td>Papiolas</td>
          <td>1000</td>
          <td>2000</td>
          <td>100</td>
          <td>10</td>
          <td>500</td>
          <td>10</td>
          <td>Bodega 1</td>
          <td>g</td>
          <td>10</td>
          <td>10</td>
        </tr>
        <tr>
          <td>8399000000000</td>
          <td>F32PB001</td>
          <td>Salsa de Tomate</td>
          <td>Struck</td>
          <td>Papas</td>
          <td>Si</td>
          <td>Papiolas</td>
          <td>1000</td>
          <td>2000</td>
          <td>100</td>
          <td>10</td>
          <td>500</td>
          <td>10</td>
          <td>Bodega 1</td>
          <td>g</td>
          <td>10</td>
          <td>12</td>
        </tr>
        <tr>
          <td>8399000000000</td>
          <td>F32PB001</td>
          <td>Helado de fresa</td>
          <td>Struck</td>
          <td>Papas</td>
          <td>Si</td>
          <td>Papiolas</td>
          <td>1000</td>
          <td>2000</td>
          <td>100</td>
          <td>10</td>
          <td>500</td>
          <td>10</td>
          <td>Bodega 1</td>
          <td>g</td>
          <td>10</td>
          <td>5</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<br>
<!-- - -->
<div class="container_boton">
  <div class="row">
    <!-- Primera opcion -->
    <div class="col-md-4" style="margin-left:240px">
      <div class="main">
        <div class="service">
          <div class="service-logo">
            <img src="https://www.incubaweb.com/wp-content/uploads/2016/09/upload.png">
          </div>
          <h4>Ingresar Productos</h4>
          <p>Añadirá productos por medio de un documento Xlsx o Csv</p><br>
          <p></p>


          <form method="POST" action="index.php?action=action_importar_productos_excel" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="sql" class="col-sm-3 col-form-label">Instrucciones</label>
              <div class="col-sm-9">
                <input type="file" accept=".xlsx, .xls, .csv" name="dataCliente" id="file-input" class="file-input__input" required>
              </div>
            </div>
            <button type="submit" style="margin-top: -34px;color: #fff; background-color: #337ab700;border-color: #2e6da400;" name="restore">
              <div type="submit" name="restore" style="margin-bottom:-20px;" class="contenedor-redes-sociales">
                <a style="margin-bottom:1px;" class="facebook" target="_blank">
                  <span class="circulo"><i class="fa fa-upload" aria-hidden="true"></i></span>
                  <span class="titulo">Subir</span>
                  <span class="titulo-hover">Ahora</span>
                </a>
              </div>
            </button>
          </form>
        </div>
      </div>
    </div>

    <!-- Segunda opcion -->
    <div class="col-md-4">
      <div class="main">
        <div class="service">
          <div class="service-logo">
            <img src="http://colecr.com/wp-content/uploads/2017/12/downloads-icon-15.png" height="103px" style="width: 115%;">
          </div>
          <h4>Descargar Productos</h4>
          <p style="margin-bottom: 50px;">Descargar el contenido de los productos que se encuentran actualmente en la base de datos. </p>
          <div style="margin-bottom:-20px;" class="contenedor-redes-sociales">
            <a style="margin-bottom:1px;" class="facebook" href="core/app/action/action-exportar_excel.php" target="_blank" download>
              <span class="circulo"><i class="fa fa-download" aria-hidden="true"></i></span>
              <span class="titulo">Descargar</span>
              <span class="titulo-hover">Ahora</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>

<br><br>


<style>
  * {
    margin: 0px;
    padding: 0px;
    font-family: helvetica;
  }

  p#texto {
    text-align: center;
    color: white;
  }

  div#div_file {}

  input#btn_enviar {
    position: absolute;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    width: 100%;
    height: 100%;
    opacity: 0;
  }
</style>