<?php

use Twilio\Rest\Client;
use Sendinblue\Mailin;
use Twilio\Rest\Serverless\V1\Service\FunctionList;

require __DIR__ . '/../../../vendor/Mailin.php';
require __DIR__ . '/../../../core/app/action/notificacion_mail_stock-action.php';
require __DIR__ . '/../../../core/app/action/notificaciones_ventas-action.php';
require __DIR__ . '/../../../core/app/action/action-functions-personas.php';
require __DIR__ . '/../../../core/app/action/action_hacienda.php';
?>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="js/hacienda/script_venta.js"></script>
<?php


//Importamos los archivos o extensiones para el uso de los servicios de terceros Twillio/Sendinblue
$Lista_productos = [];
$exonerado = 0;
$session_id = session_id();
$tmps = TmpData::getAllTemporal($session_id);
$u = null;
$u = UserData::getById(Session::getUID());
$usuario = $u->is_admin;
$id_usuario = $u->id;
if (count($tmps) > 0) {




	$contador = 0;
	foreach ($tmps as $te) :

		$entrada_producto = 0;
		$entradas = ProcesoVentaData::getAllEntradas($te->id_producto);
		if (count($entradas) > 0) {
			foreach ($entradas as $entrada) : $entrada_producto = $entrada->cantidad + $entrada_producto;
			endforeach;
		} else {
			$entrada_producto = 0;
		};


		$salida_producto = 0;
		$salidas = ProcesoVentaData::getAllSalidas($te->id_producto);
		if (count($salidas) > 0) {
			foreach ($salidas as $salida) : $salida_producto = $salida->cantidad + $salida_producto;
			endforeach;
		} else {
			$salida_producto = 0;
		};


		$producto = ProductoData::getById($te->id_producto);

		$saldo = ($producto->stock + $entrada_producto) - $salida_producto;

		if ($te->cantidad_tmp > $saldo) :
			$contador = $contador + 1;

			//print "<script>alert('Stock agotado para algunos productos.... ALERTA!!! $contador No se pudo realizar la venta');</script>";
?>
			<script>
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'Stock agotado para algunos productos.... <?php echo $producto->nombre ?>',
					footer: '<a href="index.php">Volver al inicio?</a>'
				}).then(function() {
					window.location = "index.php?view=venta";
				});
			</script>
			<meta http-equiv="refresh" content="3; url=index.php?view=venta">
		<?php
		//print "<script>window.location='index.php?view=cancelar_venta';</script>";
		endif;


	endforeach;


	if (count($_POST) > 0 and $contador == '0') {

		$cajas = CajasAperturas::getCierreCaja($id_usuario);
		$object = (array) $cajas;
		$object = count($object);
		if ($object > 0) {
			$id_caja = $cajas->id;
		} else {
			$id_caja = 'NULL';
		}


		//Tipo de cliente (Existente = 2, Cliente habitacion = 1, Nuevo cliente (Habitual) = 0)
		$ID_UNICO = $_POST['tipo_id_cliente'];
		$Info = TipoCliente::getById($ID_UNICO);
		$valor = $Info->valor_extra;

		//$id_caja='NULL';
		$total = 0;
		$total_ex = 0;
		$tmpes = TmpData::getAllTemporal($session_id);
		foreach ($tmpes as $p) :

			if ($valor == 1 || $valor == 2) {

				if ($valor == 1) {
					//echo 'valor: ' . $valor;
					$proceso = ProcesoData::getById($_POST['id_operacion']);
					$id_cliente = $proceso->id_cliente;
				} else {
					$id_cliente = $_POST['validacion_id'];
				}
				//Validar que tenga categoria
				$categoria_p = PersonaData::getById($id_cliente);

				if ($categoria_p->id_categoria_p != 0) {
					//echo '<br> Tiene categoria';
					$precio_p = val_productos_promocion($p->id_producto, $p->precio_tmp, $id_cliente);
					$precio_p_exo = val_productos_promocion($p->id_producto, $p->precio_tmp_exo, $id_cliente);
					$total = ($precio_p * $p->cantidad_tmp) + $total;
					$total_ex = ($precio_p_exo * $p->cantidad_tmp) + $total_ex;
				} else {
					//echo '<br> No tiene categoria';
					$total = ($p->precio_tmp * $p->cantidad_tmp) + $total;
					$total_ex = ($p->precio_tmp_exo * $p->cantidad_tmp) + $total_ex;
				}
			} else {

				$cliente = new PersonaData();

				$tip_doc = TipoDocumentoData::getByValor($_POST["tipo_documento"]);
				$cliente->tipo_documento = $tip_doc->id;
				$cliente->documento = $_POST["documento"];
				$cliente->nombre = $_POST["nombre_cliente"];
				$cliente->email = $_POST['email'];
				if (isset($_POST["checkbox"]) and $_POST['checkbox'] != "") {
					$cliente->exonerado = $_POST["checkbox"];
				}
				if (isset($_POST["categoria"])) {
					$cliente->id_categoria_p = $_POST["categoria"];
				} else {
					$cliente->id_categoria_p = 0;
				}

				$new_cli = $cliente->addClienteVenta();
				if ($new_cli[1] != 0) {
					$id_cliente = $new_cli[1];
				} else {
					$dataCliente_d = PersonaData::getByDocumento($_POST["documento"]);
					$id_cliente = $dataCliente_d->id;
				}

				//Validar que tenga categoria
				$categoria_p = PersonaData::getById($id_cliente);
				//print_r($categoria_p);
				if ($categoria_p != null and $categoria_p != "") {
					if ($categoria_p->id_categoria_p != 0) {
						$precio_p = val_productos_promocion($p->id_producto, $p->precio_tmp, $id_cliente);
						$precio_p_exo = val_productos_promocion($p->id_producto, $p->precio_tmp_exo, $id_cliente);
						$total = ($precio_p * $p->cantidad_tmp) + $total;
						$total_ex = ($precio_p_exo * $p->cantidad_tmp) + $total_ex;
					} else {
						$total = ($p->precio_tmp * $p->cantidad_tmp) + $total;
						$total_ex = ($p->precio_tmp_exo * $p->cantidad_tmp) + $total_ex;
					}
				}
			}


		endforeach;



		//Cliente de habitacion
		if (isset($valor) and $valor == '1') {
			//echo '<br> Entra a la seccion habitacion<br>';

			if (isset($_POST["checkbox"]) and $_POST['checkbox'] != "") {
				$exonerado = $_POST["checkbox"];
			}

			$venta = new VentaData();
			$venta->id_tipo_comprobante = $_POST['id_tipo_comprobante'];
			$venta->id_tipo_pago = 1;
			if ($exonerado == 0) {
				//echo 'No éxonerado';
				$venta->total = $total;
			} else {
				//echo 'éxonerado';
				$venta->total = $total_ex;
			}
			$venta->nro_comprobante = $_POST['num_comprobante'];
			$venta->id_usuario = $_SESSION["user_id"];
			$venta->id_caja = $id_caja;
			$v = $venta->addVentaHuesped();

			$plazo_venta = new Tipo_r_plazo_venta();
			$plazo_venta->id_venta = $v[1];
			$plazo_venta->id_plazo = $_POST['periodo'];
			$dt_pr = PeriodosData::getById($_POST['periodo']);
			//echo '2 id de periodo: '.$dt_pr->dias;
			if ($dt_pr->dias == 0) {
				$plazo_venta->estado = 0;
			} else {
				$plazo_venta->estado = 1;
			}
			$plazo_venta->add();
			//-------------------------------------------------------- Division Envio de Email ---------------------------------------------

			$proceso = ProcesoData::getById($_POST['id_operacion']);
			//$cliente = PersonaData::getById(527);
			//envio_email($cliente->email,$cliente->nombre,$session_id);
			//global_Notificacion();
			$tmps12 = TmpData::getAllTemporal($session_id);
			$id_proceso_venta = '';

			if (isset($_POST["checkbox"]) and $_POST['checkbox'] != "") {
				$exonerado = $_POST["checkbox"];
			}

			// Condicionamos, si el valor de exoneracion es 1 (activo) entonces agregamos los productos sin impuesto sino se realiza de forma corriente.
			if ($exonerado == 0) {
				foreach ($tmps12 as $p) :
					//echo '<br>No exonerado f2<br><br>';
					//Validacion de categoria 
					$categoria_p = PersonaData::getById($id_cliente);
					if ($categoria_p->id_categoria_p != 0) {
						//print('<br> id producto'.$id_producto.'<br>precio producto sin exoneracion'.$precio_producto.'<br> id cliente'.$id_cliente);
						$precio = val_productos_promocion($p->id_producto, $p->precio_tmp, $id_cliente);
					} else {
						$precio = $p->precio_tmp;
					}
					$procesoventa = new ProcesoVentaData();
					$procesoventa->id_producto = $p->id_producto;
					$procesoventa->id_caja = $id_caja;
					$procesoventa->id_operacion = $_POST['id_operacion'];
					$procesoventa->id_venta = $v[1];
					$procesoventa->cantidad = $p->cantidad_tmp;
					$procesoventa->precio = $precio;
					$procesoventa->id_usuario = $_SESSION["user_id"];
					if ($_POST['pagado'] == 0) {
						$procesoventa->fecha_creada = "NULL";
					}
					$id_proceso_venta = $procesoventa->add();
				endforeach;
			} else {
				//echo 'Exonerado f2';
				//Validacion de categoria 
				$categoria_p = PersonaData::getById($id_cliente);
				if ($categoria_p->id_categoria_p != 0) {
					//print('<br> id producto'.$id_producto.'<br>precio producto sin exoneracion'.$precio_producto.'<br> id cliente'.$id_cliente);
					$precio = val_productos_promocion($p->id_producto, $p->precio_tmp_exo, $id_cliente);
				} else {
					$precio = $p->precio_tmp;
				}
				$procesoventa = new ProcesoVentaData();
				$procesoventa->id_producto = $p->id_producto;
				$procesoventa->id_caja = $id_caja;
				$procesoventa->id_operacion = $_POST['id_operacion'];
				$procesoventa->id_venta = $v[1];
				$procesoventa->cantidad = $p->cantidad_tmp;
				$procesoventa->precio = $precio;
				$procesoventa->id_usuario = $_SESSION["user_id"];
				if ($_POST['pagado'] == 0) {
					$procesoventa->fecha_creada = "NULL";
				}
				$procesoventa->add();
			}
		}
		//Cliente existente o cliente habitual
		else if (isset($valor) && $valor == '0' || $valor == '2') {

			//print($_POST['email']);
			$email = "sin email";
			if (isset($_POST["email"]) and $_POST['email'] != "") {
				$email = $_POST["email"];
			}

			$exonerado = 0;
			if (isset($_POST["checkbox"]) and $_POST['checkbox'] != "") {
				$exonerado = $_POST["checkbox"];
			}

			if (isset($_POST['nombre_cliente']) and $_POST['nombre_cliente'] != '') {

				if ($valor == "2") {
					actualizar_cliente($_POST['validacion_id'], $_POST["nombre_cliente"], $_POST["tipo_documento"], $_POST["documento"], $email, $exonerado);
					$id_cliente = $_POST['validacion_id'];
				} else if ($valor == "0") {
					$id_cliente;
				}
			} else {
				$id_cliente = 'NULL';
			}


			//$Telefono_cliente = $_POST['telefono'];
			$Nombre_cliente = $_POST["nombre_cliente"];

		?>
			<!-- <script>
				const {
					value: formValues
				} = await Swal.fire({
					title: 'Multiple inputs',
					html: '<input id="swal-input1" class="swal2-input">' +
						'<input id="swal-input2" class="swal2-input">',
					focusConfirm: false,
					preConfirm: () => {
						return [
							document.getElementById('swal-input1').value,
							document.getElementById('swal-input2').value
						]
					}
				})

				if (formValues) {
					Swal.fire(JSON.stringify(formValues))
				}
			</script> -->
			<br><?php

				//-------------------------------------------------------- Division Envio de Email

				if ($_POST['email'] != null) {
					if (isset($_POST["checkbox"]) and $_POST['checkbox'] != "") {
						$exonerado = $_POST["checkbox"];
					} else {
						$exonerado = 0;
					}
					envio_email($id_cliente, $session_id, $exonerado);
					Envio_stock_mail();
				}
				//global_Notificacion();
				?>
		<?php

			//-------------------------------------------------------- Division Envio de SMS

			//-------------------------------------------------------- Se continua con el proceso

			//----------------------------------- CONDICION DE EXONERACION DEL PRODUCTO Y VENTA --------------------------------------------//

			if (isset($_POST["checkbox"]) and $_POST['checkbox'] != "") {
				$exonerado = $_POST["checkbox"];
			} else {
				$exonerado = 0;
			}

			$venta = new VentaData();
			$venta->id_tipo_comprobante = $_POST['id_tipo_comprobante'];
			$venta->id_tipo_pago = $_POST['periodo'];
			if ($exonerado == 0) {
				$venta->total = $total;
			} else {
				$venta->total = $total_ex;
			}
			$venta->nro_comprobante = $_POST['num_comprobante'];
			$venta->id_proveedor = $id_cliente;
			$venta->id_usuario = $_SESSION["user_id"];
			$venta->id_caja = $id_caja;
			$v = $venta->addVenta();

			$plazo_venta = new Tipo_r_plazo_venta();
			$plazo_venta->id_venta = $v[1];
			$plazo_venta->id_plazo = $_POST['periodo'];
			$dt_pr = PeriodosData::getById($_POST['periodo']);

			//echo '<br><br><br><br><br>'.'periodo: '.$dt_pr->dias;
			//echo '<br>'.'id_venta: '.$v[1];

			if ($dt_pr->dias == 0) {
				$plazo_venta->estado = 0;
			} else {
				$plazo_venta->estado = 1;
			}
			$plazo_venta->add();


			$tmps = TmpData::getAllTemporal($session_id);



			// Condicionamos, si el valor de exoneracion es 1 (activo) entonces agregamos los productos sin impuesto sino a se realiza de forma corriente.
			if ($exonerado == 0) {
				//echo 'A';
				foreach ($tmps as $p) :



					//Validacion de categoria 
					$categoria_p = PersonaData::getById($id_cliente);
					if ($categoria_p->id_categoria_p != 0) {
						//print('<br> id producto'.$id_producto.'<br>precio producto sin exoneracion'.$precio_producto.'<br> id cliente'.$id_cliente);
						$precio = val_productos_promocion($p->id_producto, $p->precio_tmp, $id_cliente);
					} else {
						$precio = $p->precio_tmp;
					}

					$procesoventa = new ProcesoVentaData();
					$procesoventa->id_producto = $p->id_producto;
					$procesoventa->id_operacion = 'NULL';
					$procesoventa->id_venta = $v[1];
					$procesoventa->cantidad = $p->cantidad_tmp;
					$procesoventa->precio = $precio;
					$procesoventa->id_usuario = $_SESSION["user_id"];
					$procesoventa->id_caja = $id_caja;
					if ($_POST['pagado'] == 0) {
						$procesoventa->fecha_creada = "NULL";
					}

					//echo '<script>alert('.$p->precio_tmp.');</script>';

					$procesoventa->add();
				endforeach;
			} else {

				//echo 'B<br>';
				foreach ($tmps as $p) :
					// Validacion de categoria 
					$categoria_p = PersonaData::getById($id_cliente);
					if ($categoria_p->id_categoria_p != 0) {
						//echo 'A';
						$precio = val_productos_promocion($p->id_producto, $p->precio_tmp_exo, $id_cliente);
					} else {
						//echo 'B';
						$precio = $p->precio_tmp_exo;
					}
					$procesoventa = new ProcesoVentaData();
					$procesoventa->id_producto = $p->id_producto;
					$procesoventa->id_operacion = 'NULL';
					$procesoventa->id_venta = $v[1];
					$procesoventa->cantidad = $p->cantidad_tmp;
					$procesoventa->precio = $precio;
					$procesoventa->id_usuario = $_SESSION["user_id"];
					$procesoventa->id_caja = $id_caja;
					if ($_POST['pagado'] == 0) {
						$procesoventa->fecha_creada = "NULL";
					}
					//echo '<script>alert('.$p->precio_tmp.');</script>';

					$procesoventa->add();
				endforeach;
			}
		}

		$Lista = [];
		$ResLista = [];
		$ResLista2 = [];

		// ----------------  Totales  ----------------//
		/* $Total_Precios = 0;
		$Total_sr_gravados = 0;
		$Total_sr_exentos = 0;
		$Total_gravados = 0;
		$Total_exento = 0;
		$Total_ventas = 0;
		$Total_Descuento = 0;
		$Total_ventas_neta = 0;
		$Total_impuestos = 0;
		$Total_comprobante = 0; */
		// ---------------- Fin Totales  ----------------//





		$dels = TmpData::getAllTemporal($session_id);
		foreach ($dels as $del) :
			// Añadimos los productos a la lista hacienda.
			$listPro = productos_list($del->id_producto, $del->cantidad_tmp, $del->precio_tmp);
			array_push($ResLista, $listPro[1]);
			array_push($ResLista2, $listPro[0]);




			//--------------------------------------------------------------------------------
			$eliminar = TmpData::getById($del->id_tmp);
			$eliminar->del();
		endforeach;
		print("<br><br><br><br><br><br><br>");
		//------------- Lista de Productos  ---------//
		//print_r($ResLista);
		print("<br><br><br><br><br><br><br>");
		//print_r($ResLista2);
		$RsProGlobal = [$ResLista, $ResLista2];
		//------------- Fin Lista de Productos  ---------//
		?>
		<?php
		//--------------------------// ENVIAMOS DOCUMENTOS HACIENDA //----------------------------------//


		//--------------------------// PRODUCTOS //----------------------------------//

		$producto1 = ['8399000000000', '1', 'Sp', 'Servicios informaticos', '5.763', '5.763', '5.763', '5.763'];
		$producto2 = ['8399000000000', '1', 'Sp', 'Servicios informaticos', '5', '5', '5', '5'];
		$producto3 = ['8399000000000', '1', 'Sp', 'Servicios informaticos', '5', '5', '5', '5'];
		$producto4 = ['8399000000000', '1', 'Sp', 'Servicios informaticos', '5', '5', '5', '5'];
		$producto5 = ['8399000000000', '1', 'Sp', 'Servicios informaticos', '5', '5', '5', '5'];
		$Lista = [$producto1];
		print("<br><br><br><br><br><br><br>");
		//print_r($Lista);
		//--------------------------// DATOS RECEPTOR //----------------------------------//
		$id_recep = strval($_POST['tipo_documento']);
		$nombre_receptor = $_POST['nombre_cliente'];
		$receptor_tipo_identif = '0' . $id_recep;
		$receptor_num_identif = $_POST['documento'];
		$cambio_m = $_POST['cambio_mon'];
		$mail_receptor = $_POST['email'];


		/* $total_serv_gravados = $Total_sr_gravados;
		$total_serv_exentos = $Total_sr_exentos;
		$total_gravados = $Total_gravados;
		$total_exento = $Total_exento;
		$total_ventas = $Total_ventas;
		$total_descuento = $Total_Descuento;
		$total_ventas_neta = $Total_ventas_neta;
		$total_impuestos = $Total_impuestos;
		$total_comprobante = $Total_comprobante; */

		$caja_fact = CajasAperturas::getById($id_caja);
		$m_pago = TipoPago::getById($_POST['m_pago']);
		$ListaVenta = [
			$nombre_receptor,
			$receptor_tipo_identif,
			$receptor_num_identif,
			$cambio_m,
			$mail_receptor,
			$m_pago->valor_hacienda,
			$id_usuario,
			$caja_fact->id_caja,
			/* $total_serv_exentos,
			$total_exento,
			$total_ventas,
			$total_ventas_neta,
			$total_comprobante */
		];
		print("<br><br>");
		//print_r($ListaVenta);
		print("<br><br>");
		//--------------------------// DATOS EMISOR //----------------------------------//

		$info_admin = ConfiguracionData::getAllCONF();

		$usuario_hacienda = $info_admin->usuario_hacienda;
		$contrasena = $info_admin->contrasena_hacienda;
		$pinP12 = $info_admin->pin;
		$token_llave = $info_admin->token;
		$emisor_nombre = $info_admin->nombre_hac;
		$cod_moneda = $info_admin->cod_moneda;
		$fecha_emision = FechaAct();
		$emisor_num_identif = $info_admin->numero_id;
		$emisor_tipo_identif = TipoCedula($info_admin->tipo_id);
		$nombre_comercial = $info_admin->nombre_hac;
		$emisor_provincia = $info_admin->provincia;
		$emisor_canton = $info_admin->canton;
		$emisor_distrito = $info_admin->distrito;
		$emisor_barrio = $info_admin->barrio;
		$emisor_otras_senas = $info_admin->direccion;
		$emisor_cod_pais_tel = $info_admin->cod_pais_tel_hc;
		$emisor_tel = $info_admin->tel_hac;
		$cedula = $info_admin->numero_id;
		$tipoCedula = TipoCedulaText($info_admin->tipo_id);

		$ListaAdmin = [
			$usuario_hacienda,
			$contrasena,
			$pinP12,
			$token_llave,
			$emisor_nombre,
			$cod_moneda,
			$fecha_emision,
			$emisor_num_identif,
			$emisor_tipo_identif,
			$nombre_comercial,
			$emisor_provincia,
			$emisor_canton,
			$emisor_distrito,
			$emisor_barrio,
			$emisor_otras_senas,
			$emisor_cod_pais_tel,
			$emisor_tel,
			$cedula,
			$tipoCedula
		];

		//print_r($ListaAdmin);

		$estado_funct_hacienda = Funcionalidad::getById(4);
		if ($estado_funct_hacienda->estado == 1) {
		?>
			<script type="text/javascript">
				Espera();
				cls(<?php echo json_encode($RsProGlobal) ?>, <?php echo json_encode($ListaVenta) ?>, <?php echo json_encode($ListaAdmin) ?>)
			</script>
		<?php
		} else {
		?>
			<script type="text/javascript">
				Espera();
			</script>

		<?php
			echo '<meta http-equiv="refresh" content="2; url=index.php?view=venta">';
		}

		Actualizar_Apertura::actualizar();
	}
	Actualizar_Apertura::actualizar();
} else {
	Actualizar_Apertura::actualizar(); ?>
	<script type="text/javascript">
		Swal.fire('No se agrego ningun producto!')
	</script>
	<meta http-equiv="refresh" content="1; url=index.php?view=venta">
<?php
	//print "<script>window.location='index.php?view=venta';</script>";
}

function envio_email_finalizado($api_key, $to_email, $from_email, $from_name, $subject, $message, $to_name)
{
	$validacion_email = Funcionalidad::getById(1);
	if ($validacion_email->estado == 1) {
		$mailin = new Mailin('https://api.sendinblue.com/v2.0', $api_key);
		$data = array(
			"to" => array($to_email => $to_name),
			"from" => array($from_email, $from_name),
			"subject" => $subject,
			"html" => $message,
			"attachment" => array()
		);
		$response = $mailin->send_email($data);
		// Codigo para el envio del correo
	}
}


?>

<style>
	.swal2-icon.swal2-info {
		font-size: 19px;
		line-height: 29px;
	}

	.swal2-popup {
		width: 48em;
	}
</style>