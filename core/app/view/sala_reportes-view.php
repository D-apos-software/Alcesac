<!DOCTYPE html>
<html lang="en">

<head>
  <title>CSS Cards</title>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i&display=swap" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="css/vista_reportes/vista_inicial.css" type="text/css">

  <link rel="stylesheet" href="StyleCards.css" />
</head>

<body>
  <br><br><br><br>

  <div class="card-category-1">

    <div class="basic-card basic-card-aqua">
      <div class="card-content">
        <span class="card-title">Ventas y Compras</span>
        <p class="card-text">
        <div class="card-link">
        <div style="margin-bottom:5px;"><a href="?view=lista_compra" title="Read Full">Reporte compras por Fechas.</a></div>
        <div style="margin-bottom:5px;"><a href="?view=pre_reporte_fecha_circular" title="Read Full">Diagrama general por Fecha.</a></div>
        <div style="margin-bottom:5px;"><a href="?view=pre_reporte_anio_barra" title="Read Full">Diagrama Anual.</a></div>
        <div style="margin-bottom:5px;"><a href="?view=pre_reporte_rango_producto" title="Read Full">Reporte de ventas por producto</a></div>
        </div>
        </p>
      </div>
    </div>

    <div class="basic-card basic-card-lips">
      <div class="card-content">
        <span class="card-title">Egresos e Ingresos</span>
        <p class="card-text">
        <div class="card-link">
        <div style="margin-bottom:5px;"><a href="?view=reporte_gasto" title="Read Full">Reporte de Egresos por Fechas</a></div>
        <div style="margin-bottom:5px;"><a href="?view=reporte_mensual" title="Read Full">Reporte mensual</a></div>
        <br>
        <div style="margin-bottom:5px;"><a href="?view=reporte_cliente_generales" title="Read Full">&nbsp;</a></div><br>
        <div style="margin-bottom:5px;"><a href="?view=reporte_producto_generales" title="Read Full">&nbsp;</a></div>
        </div>
        </p>
      </div>
    </div>

    <div class="basic-card basic-card-light">
      <div class="card-content">
        <span class="card-title">Productos y Clientes</span>
        <p class="card-text">
        <div class="card-link">
          <div style="margin-bottom:5px;"><a href="?view=kardex" title="Read Full">Reporte de saldos y movimientos</a></div>
          <div style="margin-bottom:5px;"><a href="?view=reporte_cliente_generales" title="Read Full">Reportes de clientes</a></div>
          <div style="margin-bottom:5px;"><a href="?view=reporte_producto_generales" title="Read Full">Ganancias por producto</a></div>
          <div style="margin-bottom:5px;"><a href="?view=rentabilidad_por_producto" title="Read Full">Rentabilidad por producto</a></div>
        </div>
        </p>
      </div>
    </div>

    <div class="basic-card basic-card-dark">
      <div class="card-content">
        <span class="card-title">Recepcion y Cajas</span>
        <p class="card-text">
        <div class="card-link">
          <div style="margin-bottom:5px;"><a href="?view=reporte_user" title="Read Full">Reporte diario por usuario</a></div>
          <div style="margin-bottom:5px;"><a href="?view=pre_reporte_rango" title="Read Full">Reporte de recepcion por fecha</a></div>
          <div style="margin-bottom:5px;"><a href="?view=reporte_estado" title="Read Full">Reporte de estado habitación</a></div>
          <div style="margin-bottom:5px;"><a href="?view=pre_reporte_fecha_barra" title="Read Full">Diagrama de reservaciones</a></div>
        </div>
        </p>
      </div>
    </div>


  </div>

  <br />
  <!--     Reportes Cantidades        -->
  <div class="container py-4">
    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-3">
      <div class="col">
        <div class="card">
          <div class="list-group list-group-flush">

            <div class="list-group-item d-flex py-3 align-items-center">
              <a class="position-relative" href="#1">
                <img class="profile-img-sm rounded" src="https://via.placeholder.com/256/fe669e/fff.png?text=25%" />
              </a>
              <div class="flex-fill ps-4 overflow-hidden">
                <div class="fw-bold mb-0 text-truncate">Ventas y Compras</div>
                <div class="progress progress-sm">
                  <div style="width: 74%;" class="progress-bar bg-konkon"></div>
                </div>
              </div>
            </div>

            <div class="list-group-item d-flex py-3 align-items-center">
              <a class="position-relative" href="#2">
                <img class="profile-img-sm rounded" src="https://via.placeholder.com/256/fd7e14/fff.png?text=20%" />
              </a>
              <div class="flex-fill ps-4 overflow-hidden">
                <div class="fw-bold mb-0 text-truncate">Egresos e Ingresos</div>
                <div class="progress progress-sm">
                  <div style="width: 40%;" class="progress-bar bg-warning"></div>
                </div>
              </div>
            </div>

            <div class="list-group-item d-flex py-3 align-items-center">
              <a class="position-relative" href="#3">
                <img class="profile-img-sm rounded" src="https://via.placeholder.com/256/0d6efd/fff.png?text=5%" />
              </a>
              <div class="flex-fill ps-4 overflow-hidden">
                <div class="fw-bold mb-0 text-truncate">Productos y Clientes</div>
                <div class="progress progress-sm">
                  <div style="width: 26%;" class="progress-bar bg-primary"></div>
                </div>
              </div>
            </div>

            <div class="list-group-item d-flex py-3 align-items-center">
              <a class="position-relative" href="#4">
                <img class="profile-img-sm rounded" src="https://via.placeholder.com/256/7952b3/fff.png?text=50%" />
              </a>
              <div class="flex-fill ps-4 overflow-hidden">
                <div class="fw-bold mb-0 text-truncate">Recepcion y Cajas</div>
                <div class="progress progress-sm">
                  <div style="width:100%;background-color:#7952b3;" class="progress-bar"></div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>