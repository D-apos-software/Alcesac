
<?php 
     date_default_timezone_set('America/Costa_Rica');
     $hoy = date("Y-m-d");
     $hora = date("H:i:s");
                    
?>

<style type="text/css">
  table.dataTable thead .sorting:after {
    opacity: 0.0;
    content: "\e150";
}

table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after {
    opacity: 0.0;
}
</style>
<div style="margin-top:25px;" class="row">

 <section  style="margin-left:500px;" class="content-header">
      <h3>
        <span></span> REPORTE POR RANGO DE FECHA 
      </h3> 
</section>
</div>

<style type="text/css">
  
  .hh:hover{
    background-color: white;
  }
  .small-box-footer {
    position: relative;
    text-align: center;
    padding: 0px 0;
    color: #fff;
    color: rgba(255,255,255,0.8);
    display: block;
    z-index: 10;
    background: rgba(0,0,0,0.1);
    text-decoration: none;
}
.nav-tabs-custom>.nav-tabs>li>a {
    color: #3c8dbc;
    font-weight: bold;
    border-radius: 0 !important;
}
.nav-tabs-custom>.nav-tabs>li.active {
    border-top-color: #00a65a;
}
.h5, h5 {
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>

<br>

<section style="border-radius: 20px; margin-left:300px; box-shadow: 0 4px 20px 0px rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(0 0 0 / 53%);" class="tile col-md-6 ">
    <form method="post" action="index.php?view=reporte_rango_producto">
        <div class="tile-body">

            <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
            <input type="date" class="typeahead form-control" name="start" value="NOW()">

            <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
            <input type="date" class="typeahead form-control" name="end" value="NOW()">

        </div>
        <!-- /tile body -->
        <div class="tile-footer">
            <div class="form-group text-center">
                
                <button class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver reporte</button>
            </div>
        </div>
    </form>

  </section>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
   
  });
</script>

<script>
  $(function () {
    $("#example2").DataTable();
   
  });
</script>
