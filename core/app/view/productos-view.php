<?php
require __DIR__ . '/../../../core/app/action/action-functions-products.php'; ?>

<head>
  <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
  <link rel="stylesheet" href="css/vista_productos/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript"></script>
  <!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
  <script src="script.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>

<body id="minovate" class="appWrapper sidebar-sm-forced">
  <div class="row">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="index.php?view=cliebte"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#">Punto de venta</a></li>
        <li class="active">Productos</li>
      </ol>
    </section>
  </div>


  <!-- row -->
  <div class="row">
    <!-- col -->
    <div class="col-md-12">
      <button type="button" id="btn_estado" class="btn btn-success" data-toggle="modal" data-target="#myModal">
        <span class="glyphicon glyphicon-indent-left"></span> Estado de productos
      </button>
      <section class="tile">
        <div class="tile-header dvd dvd-btm">
          <h1 class="custom-font"><strong>MANTENIMIENTO DE PRODUCTOS </strong> DE VENTA</h1>
          <ul class="controls">
            <li class="remove">
              <a href="index.php?view=productos_excel"> <span class="fa fa-eye"></span>&nbsp; Uso de Excel</a>
            </li>
            <li class="remove">
              <a href="#" data-toggle="modal" data-target="#myModal_1"> <span class="fa fa-cutlery"></span>&nbsp; Registrar un producto</a>
            </li>
            <li class="remove">
              <a href="index.php?view=producto_estado" data-toggle="modal"> <span class="fa fa-user"></span>&nbsp; Inactivos</a>
            </li>
            <li class="dropdown">
              <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
              </a>
              <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                <li>
                  <a role="button" tabindex="0" class="tile-toggle">
                    <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                    <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-refresh">
                    <i class="fa fa-refresh"></i> Refresh
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-fullscreen">
                    <i class="fa fa-expand"></i> Fullscreen
                  </a>
                </li>
              </ul>
            </li>
            <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>


        <div class="tile-body">
          <div class="form-group">
            <label for="filter" style="padding-top: 5px">Buscar:</label>
            <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
          </div>


          <?php $productos = ProductoData::getAll();
          if (count($productos) > 0) {
            // si hay usuarios
          ?>
            <table id="searchTextResults" data-filter="#filter" data-page-size="10" class="footable table table-custom" style="font-size: 11px;">

              <thead style="color: white; background-color: #827e7e;">
                <th>CÓDIGO</th>
                <th>NOMBRE</th>
                <th>MARCA</th>
                <th>Cod Cabys</th>
                <th>PRECIO COMPRA</th>
                <th>PRECIO VENTA</th>
                <th>IMPUESTO</th>
                <th>TOTAL</th>
                <th></th>
                <th></th>
              </thead>
              <?php foreach ($productos as $producto) :
                if ($producto->estado == 0) { ?>
                  <tr>
                    <td><?php echo $producto->codigo; ?></td>
                    <td><?php echo $producto->nombre; ?></td>
                    <td><?php if ($producto->marca != "NULL") {
                          echo $producto->marca;
                        } else {
                          echo "------";
                        } ?></td>
                    <td id='foo<?php echo $producto->id ?>'>
                      <script>
                        var cabs = <?php echo $producto->codigo ?>;
                        $.ajax({
                          "url": "https://api.hacienda.go.cr/fe/cabys?codigo=" + cabs,
                          "method": "GET"
                        }).done(function(response) {
                          var cont=0;
                          response.forEach(element => cont +=1);
                          if (response == null || response == "" || cont > 1) {
                            document.getElementById("foo<?php echo $producto->id ?>").innerHTML = "<div style='color:#d43f3a'>Invalido</div>";
                          } else {
                            document.getElementById("foo<?php echo $producto->id ?>").innerHTML = "<div style='color:green'>Valido</div>";
                          }
                        });
                      </script>
                    </td>
                    <td><b><?php if ($producto->precio_compra != "NULL") {
                              echo '$   ' . number_format($producto->precio_compra, 2, '.', ',');
                            } else {
                              echo "------";
                            } ?></b></td>
                    <td><b>$ <?php echo number_format($producto->precio_venta, 2, '.', ','); ?></b></td>
                    <td><?php
                        $descuento_t = $producto->getDescuento()->valor;

                        echo impuesto_total($producto->precio_venta, $producto->utilidad, $descuento_t, $producto->id);


                        ?></td>
                    <td><?php
                        $descuento_t = $producto->getDescuento()->valor;

                        echo total_de_venta($producto->precio_venta, $producto->utilidad, $descuento_t, $producto->id);
                        ?></td>
                    <td>
                      <a href="#" data-toggle="modal" onclick="setTipoCabys(<?php echo $producto->existencia_cabys ?>)" data-target="#myModal<?php echo $producto->id; ?>" id="edit<?php echo $producto->id; ?>" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                    </td>
                    <td> <a onclick="del(<?php echo $producto->id ?>)" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Desactivar</a></td>
                  </tr>
                  <div class="modal fade bs-example-modal-xm" name="myModal<?php echo $producto->id; ?>" id="myModal<?php echo $producto->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-warning">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=updateproduct" role="form" enctype="multipart/form-data">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <button type="button" class="btn btn-success" data-toggle="modal" id="vlestadoM<?php echo $producto->id ?>" name="vlestadoM<?php echo $producto->id ?>" onclick="activar_busquedaM(value,<?php echo $producto->id ?>)" value="3">
                                <span class="glyphicon glyphicon-search"></span> Busqueda directa
                              </button>
                              <h4 class="modal-title"><span class="fa fa-cutlery"></span>&nbsp;&nbsp;&nbsp; EDITAR PRODUCTO</h4>
                            </div>
                            <div class="modal-body" style="background-color:#fff !important;">

                              <div class="row">
                                <div class="col-md-offset-1 col-md-11" style="margin-left:23.1px">

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Código&nbsp;</span>
                                      <!--<input type="text" class="form-control col-md-8" name="codigo" value="<?php echo $producto->codigo; ?>" required placeholder="Ingrese Código"> -->
                                      <?php $cabys_list = Auto_Cabys::getActivos(); ?>
                                      <!-- OPCION 1 SELECT -->
                                      <select class="form-control select" name="codigoM<?php echo $producto->id ?>" id="codigoM<?php echo $producto->id ?>" style="display:none">
                                        <option value="">--- Selecciona ---</option>
                                        <?php foreach ($cabys_list as $list) : ?>
                                          <option value="<?php echo $list->cabys; ?>" <?php if ($producto->codigo == $list->cabys) echo "selected"; ?>><?php echo $list->descripcion; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                      <!-- OPCION 2 BUSCADOR -->
                                      <input type="text" class="form-control" id="codigo_sM<?php echo $producto->id ?>" name="codigo_sM<?php echo $producto->id ?>" required placeholder="Código" value="<?php echo $producto->codigo; ?>" >
                                      <input type="text" id="cd2<?php echo $producto->id ?>" name="cd2<?php echo $producto->id ?>" value="3" hidden />


                                      <span class="input-group-addon"> Código de barra</span>
                                      <input type="text" class="form-control" name="codigo_barra" value="<?php echo $producto->codigo_barra; ?>" required placeholder="Ingresar CD barra">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Nombre</span>
                                      <input type="text" class="form-control" name="nombre" id="nombre_m" value="<?php echo $producto->nombre; ?>" required placeholder="Ingrese Nombre">
                                      <span class="input-group-addon"> Marca &nbsp;&nbsp;</span>
                                      <input type="text" class="form-control" name="marca" id="marca_m" value="<?php if ($producto->marca != 'NULL') {
                                                                                                                  echo $producto->marca;
                                                                                                                } else {
                                                                                                                  echo "";
                                                                                                                } ?>" placeholder="Ingrese Marca (OPCIONAL)">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Presentación&nbsp;&nbsp;&nbsp;</span>
                                      <input type="text" class="form-control" name="presentacion" id="presentacion_m" value="<?php if ($producto->presentacion != 'NULL') {
                                                                                                                                echo $producto->presentacion;
                                                                                                                              } else {
                                                                                                                                echo "";
                                                                                                                              } ?>" placeholder="Ingrese presentacion (OPCIONAL)">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Precio Compra</span>
                                      <input type="number" class="form-control" name="precio_compra" id="precio_compra_m" step="any" value="<?php if ($producto->precio_compra != 'NULL') {
                                                                                                                                              echo $producto->precio_compra;
                                                                                                                                            } else {
                                                                                                                                              echo "";
                                                                                                                                            } ?>" placeholder="Ingresar">
                                      <span class="input-group-addon"> Precio Venta&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                      <input type="number" style="border-color: red;" class="form-control" step="any" value="<?php echo $producto->precio_venta; ?>" id="precio_venta_m" name="precio_venta" required placeholder="Ingresar">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Stock inicial</span>
                                      <input type="number" class="form-control" id="stock_m" name="stock" disabled required value="<?php if ($producto->stock != 'NULL') {
                                                                                                                                      echo $producto->stock;
                                                                                                                                    } else {
                                                                                                                                      echo "";
                                                                                                                                    } ?>" placeholder="Ingrese Stock">
                                      <input type="text" name="stock_m" value="<?php echo $producto->stock; ?>" hidden>
                                      <input type="text" name="id_pro" value="<?php echo $producto->id; ?>" hidden>
                                      <span class="input-group-addon"> Proveedor&nbsp;&nbsp;</span>
                                      <select class="form-control select2" required name="id_proveedor_m" id="id_proveedor_m">
                                        <option value="0">(OPCIONAL)</option>
                                        <?php $proveedores = PersonaData::getProveedor(); ?>
                                        <?php foreach ($proveedores as $proveedor) : ?>
                                          <option value="<?php echo $proveedor->id; ?>" <?php if ($proveedor->id == $producto->id_proveedor) {
                                                                                          echo "selected";
                                                                                        } ?>><?php echo $proveedor->nombre; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Aviso Stock </span>
                                      <input type="text" class="form-control" name="aviso_stock" id="aviso_stock" value="<?php echo $producto->aviso_stock ?>">
                                      <span class="input-group-addon"> Descripcion </span>
                                      <input type="text" class="form-control" name="descripcion" id="descripcion_m" value="<?php echo $producto->descripcion ?>">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Unidad de Medida&nbsp;&nbsp;&nbsp; </span>
                                      <select class="form-control select2" required name="id_unidad_medida" id="id_unidad_medida_m">
                                        <option value="NULL">---- (OPCIONAL) ----</option>
                                        <?php $unidad_medidas = Unidad_de_medida::getactive(); ?>
                                        <?php foreach ($unidad_medidas as $unidad_medida) : ?>
                                          <option value="<?php echo $unidad_medida->id; ?>" <?php if ($unidad_medida->id == $producto->id_unidad_medida) {
                                                                                              echo "selected";
                                                                                            } ?>><?php echo $unidad_medida->nombre; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                      <span class="input-group-addon"> Stock Máximo</span>
                                      <input type="text" class="form-control" id="cantidad_maxima" name="cantidad_maxima" value="<?php echo $producto->stock_max; ?>">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Descuento&nbsp; </span>
                                      <select class="form-control select2" required name="id_descuento" id="id_descuento">
                                        <option value="0">Selecciona</option>
                                        <?php $utilidads = Descuento_Producto::getAll(); ?>
                                        <?php foreach ($utilidads as $utilidad) : ?>
                                          <option value="<?php echo $utilidad->id; ?>" <?php if ($utilidad->id == $producto->id_descuento) {
                                                                                          echo "selected";
                                                                                        } ?>><?php echo $utilidad->nombre; ?></option>
                                        <?php endforeach; ?>
                                      </select>


                                      <span class="input-group-addon"> Utilidad</span>
                                      <input type="text" class="form-control" name="utilidad" id="utilidad_m" value="<?php echo $producto->utilidad; ?>" required placeholder="Ingrese Nombre">

                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Estado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                      <select id="id_acreditable_m" class="form-control select2" required name="id_acreditable">
                                        <option value="0" <?php if ($producto->acreditable == 0) {
                                                            echo 'Selected';
                                                          } ?>>No acreditable</option>
                                        <option value="1" <?php if ($producto->acreditable == 1) {
                                                            echo 'Selected';
                                                          } ?>>Acreditable</option>
                                      </select>
                                      <?php $bodegas = Bodegas::get_activas(); ?>
                                      <span class="input-group-addon"> Bodega </span>
                                      <select class="form-control select2" required name="id_bodega" id="id_bodega_m">
                                        <?php foreach ($bodegas as $bodega) : ?>
                                          <option value="<?php echo $bodega->id; ?>" <?php
                                                                                      if ($bodega->id == $producto->id_bodega) {
                                                                                        echo 'selected';
                                                                                      } ?>><?php echo $bodega->nombre; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                  <!-- ----------------- IMG ---------------->

                                  <!--<div class="avatar-upload_m">
                                  <div class="avatar-edit_m">
                                    <input type='file' name="imagen_m" id="imagen_m" accept=".png, .jpg, .jpeg" />
                                    <label for="imagen_m"></label>
                                  </div>
                                  <div class="avatar-preview_m">
                                    <div id="imagePreview_m" style="background-image: url(<?php echo $producto->imagen ?>);">
                                    </div>
                                  </div>
                                </div>-->
                                  <div class="avatar-upload_m">
                                    <div class="avatar-preview_m">
                                      <div id="imagePreview_m" style="background-image: url(<?php echo $producto->imagen ?>);">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="input-group">
                                      <input type='file' name="imagen_m" id="imagen_m" accept=".png, .jpg, .jpeg" />
                                    </div>
                                  </div>

                                  <!-- ----------------------------------------------------------------------- PRUEBAS -------------------------------------------------------------------------------------------------------------------------->
                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Impuesto&nbsp;&nbsp;&nbsp;</span>
                                      <div class="dropdown" data-control="checkbox-dropdown">
                                        <label class="form-control dropdown-label">Select</label>
                                        <div class="dropdown-list">
                                          <?php $lista = array(); ?>
                                          <?php $proveedores = Tipo_impuesto_productos::get_active(); ?>
                                          <?php
                                          $proveedores2 = Tipo_r_impuestos_productos::getimpuestos($producto->id);
                                          $datos_impuesto = array();
                                          $datos_impuesto2 = array();

                                          foreach ($proveedores2 as $value) {
                                            array_push($datos_impuesto, $value);
                                          }
                                          foreach ($proveedores2 as $value) {
                                            array_push($datos_impuesto2, $value->id_impuesto);
                                          }

                                          ?>
                                          <?php
                                          foreach ($proveedores as $proveedor) :
                                          ?>
                                            <label class="dropdown-option">
                                              <input type="checkbox" name="dropdown-group[]" value="<?php echo $proveedor->id; ?>" <?php
                                                                                                                                    foreach ($datos_impuesto2 as $impuesto_update) :
                                                                                                                                      if ($proveedor->id == $impuesto_update) {
                                                                                                                                        echo "checked";
                                                                                                                                      }
                                                                                                                                    endforeach;
                                                                                                                                    ?> />
                                              <?php
                                              echo $proveedor->nombre;
                                              array_push($lista, $proveedor->id);
                                              ?>
                                            </label>
                                          <?php

                                          endforeach;
                                          ?>
                                          <!--<input class="form-control" name="lista_impuestos[]" value="<?php print_r($lista); ?>">
                                         <input class="form-control" name="lista_impuestos2[]" value="<?php print_r($datos_impuesto2); ?>">-->
                                        </div>
                                      </div>

                                      <!--<span class="input-group-addon"> Utilidad</span>
                                    <div class="dropdown" data-control="checkbox-dropdown">
                                      <label class="form-control dropdown-label">Select</label>
                                      <div class="dropdown-list">
                                        <?php $lista = array(); ?>
                                        <?php $proveedores = Utilidad_Productos::get_active(); ?>
                                        <?php
                                        $descuentos = Tipo_r_utilidades_productos::getutilidad($producto->id);
                                        $datos_descuento = array();
                                        $datos_descuento2 = array();

                                        foreach ($descuentos as $value) {
                                          array_push($datos_descuento, $value);
                                        }
                                        foreach ($descuentos as $value) {
                                          array_push($datos_descuento2, $value->id_utilidad);
                                        }

                                        ?>
                                        <?php
                                        foreach ($proveedores as $proveedor) :


                                        ?>
                                          <label class="dropdown-option">
                                            <input type="checkbox" name="utilidad[]" value="<?php echo $proveedor->id; ?>" <?php
                                                                                                                            foreach ($datos_descuento2 as $impuesto_update) :
                                                                                                                              if ($proveedor->id == $impuesto_update) {
                                                                                                                                echo "checked";
                                                                                                                              }
                                                                                                                            endforeach;
                                                                                                                            ?> />
                                            <?php
                                            echo $proveedor->nombre;
                                            array_push($lista, $proveedor->id);
                                            ?>
                                          </label>
                                        <?php

                                        endforeach;
                                        ?>
                                      </div>
                                    </div>-->
                                    </div>
                                  </div>

                                  <!-- ----------------------------------------------------------------------- PRUEBAS FINAL  -------------------------------------------------------------------------------------------------------------------------->


                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                              <input type="hidden" class="form-control" value="<?php echo $producto->id; ?>" name="id_producto">
                              <input type="hidden" class="form-control" value="<?php echo $producto->imagen; ?>" name="imagen_delete">
                              <button type="submit" class="btn btn-outline">Actualizar producto</button>
                            </div>
                          </form>


                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </div>



              <?php }
              endforeach; ?>
              <tfoot class="hide-if-no-paging">
                <tr>
                  <td colspan="8" class="text-center">
                    <ul class="pagination"></ul>
                  </td>
                </tr>
              </tfoot>
            </table>

          <?php } else {
            echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
          };
          ?>

        </div>
      </section>

    </div>
  </div>





  <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Productos con Bajo Stock</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Aviso</th>
                <th style="color:#d43f3a;">Stock</th>
              </tr>
            </thead>
            <tbody>
              <?php $productos = ProductoData::getAll();
              if (count($productos) > 0) {
              ?>
                <?php foreach ($productos as $producto) : ?>
                  <?php $entrada_producto = 0; ?>
                  <?php $entradas = ProcesoVentaData::getAllEntradas($producto->id);
                  if (count($entradas) > 0) { ?>
                    <?php foreach ($entradas as $entrada) : $entrada_producto = $entrada->cantidad + $entrada_producto;
                    endforeach; ?>
                  <?php } else {
                    $entrada_producto = 0;
                  }; ?>
                  <?php $salida_producto = 0; ?>
                  <?php $salidas = ProcesoVentaData::getAllSalidas($producto->id);
                  if (count($salidas) > 0) { ?>
                    <?php foreach ($salidas as $salida) : $salida_producto = $salida->cantidad + $salida_producto;
                    endforeach; ?>
                  <?php } else {
                    $salida_producto = 0;
                  }; ?>
                  <?php $stock = ($producto->stock + $entrada_producto) - $salida_producto; ?>
                  <?php

                  if ($stock < $producto->aviso_stock) {
                    $alert_stock = true;
                  ?>
                    <tr>
                      <td><?php echo $producto->nombre; ?></td>
                      <td><?php echo $producto->aviso_stock; ?></td>
                      <td><?php echo $stock; ?></td>
                    <?php } ?>
                  <?php endforeach; ?>
            <tfoot class="hide-if-no-paging">
              <tr>
                <td colspan="6" class="text-center">
                  <ul class="pagination"></ul>
                </td>
              </tr>
            </tfoot>

          <?php }
          ?>



          <?php
          ?>
          </tbody>
          </table>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  </div>





  <div class="modal fade bs-example-modal-xm" id="myModal_1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-success">
      <div class="modal-dialog">
        <div class="modal-content">
          <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=addproduct" role="form" enctype="multipart/form-data">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <button type="button" class="btn btn-success" data-toggle="modal" id="vlestado" name="vlestado" onclick="activar_busqueda(value)" value="0">
                <span class="glyphicon glyphicon-search"></span> Busqueda directa
              </button>
              <h4 class="modal-title"><span class="fa fa-cutlery"></span> INGRESAR NUEVO PRODUCTO</h4>
            </div>

            <div class="modal-body" style="background-color:#fff !important;">

              <div class="row">
                <div class="col-md-offset-1 col-md-10">

                  <div class="form-group">
                    <div class="input-group">
                      <!-- <span class="input-group-addon" style="margin-left:20px;"> Código&nbsp;&nbsp; </span>
                      <input type="text" class="form-control col-md-8" name="codigo" required placeholder="Ingrese Código"> -->
                      <span class="input-group-addon"> Código&nbsp;</span>
                      <!--<input type="text" class="form-control col-md-8" name="codigo" value="<?php echo $producto->codigo; ?>" required placeholder="Ingrese Código"> -->
                      <?php $cabys_list = Auto_Cabys::getActivos(); ?>

                      <!-- OPCION 1 SELECT -->
                      <select class="form-control select" required name="codigo" id="codigo">
                        <option value="">--- Selecciona ---</option>
                        <?php foreach ($cabys_list as $list) : ?>
                          <option value="<?php echo $list->cabys; ?>" ><?php echo $list->descripcion; ?></option>
                        <?php endforeach; ?>
                      </select>

                      <!-- OPCION 2 BUSCADOR -->
                      <input type="text" class="form-control" id="codigo_s" name="codigo_s" required placeholder="Código" >
                      <input type="text" id="cd1" name="cd1" value="0" hidden />


                      <span class="input-group-addon"> Codigo de barra</span>
                      <input type="text" class="form-control" id="codigo_barra" name="codigo_barra" required placeholder="Ingresar CD barra">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Nombre&nbsp;</span>
                      <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="Ingrese Nombre">
                      <span class="input-group-addon"> Marca&nbsp;&nbsp;&nbsp;&nbsp;</span>
                      <input type="text" class="form-control" name="marca" placeholder="Ingrese Marca ">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Presentación&nbsp;&nbsp;&nbsp; </span>
                      <input type="text" class="form-control" name="presentacion" placeholder="Ingrese presentacion ">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Precio Compra</span>
                      <input type="number" class="form-control" step="any" id="precio_compra" name="precio_compra" placeholder="Ingresar">
                      <span class="input-group-addon"> Precio Venta</span>
                      <input type="number" style="border-color: red;" class="form-control" step="any" name="precio_venta" required placeholder="Ingresar">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Stock inicial</span>
                      <input type="number" class="form-control" id="stock" name="stock" required placeholder="Ingrese Stock">
                      <span class="input-group-addon"> Proveedor&nbsp;&nbsp;</span>
                      <select class="form-control select2" required name="id_proveedor">
                        <option value="NULL">(OPCIONAL)</option>
                        <?php $proveedores = PersonaData::getProveedor(); ?>
                        <?php foreach ($proveedores as $proveedor) : ?>
                          <option value="<?php echo $proveedor->id; ?>"><?php echo $proveedor->nombre; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Aviso Stock</span>
                      <input type="text" class="form-control" name="aviso_stock" placeholder="(OPCIONAL)">
                      <span class="input-group-addon"> Descripcion </span>
                      <input type="text" class="form-control" name="descripcion" placeholder="(OPCIONAL)">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Unidades de Medida</span>
                      <select class="form-control select2" required name="id_unidad_medida">
                        <option value="NULL">Selecciona</option>
                        <?php $unidad_medidas = Unidad_de_medida::getactive(); ?>
                        <?php foreach ($unidad_medidas as $unidad_medida) : ?>
                          <option value="<?php echo $unidad_medida->id; ?>"><?php echo $unidad_medida->nombre; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <span class="input-group-addon"> Stock Máximo</span>
                      <input type="text" class="form-control" id="cantidad_maxima" name="cantidad_maxima" placeholder="(OPCIONAL)">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Descuento </span>
                      <select class="form-control select2" required id="id_descuento" name="id_descuento">
                        <option value="0">Selecciona</option>
                        <?php $descuentos = Descuento_Producto::getAll(); ?>

                        <?php foreach ($descuentos as $descuento) : ?>
                          <option value="<?php echo $descuento->id; ?>"><?php echo $descuento->nombre; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <span class="input-group-addon"> Utilidad&nbsp;&nbsp;&nbsp;</span>
                      <input type="number" class="form-control" id="utilidad" name="utilidad" required placeholder="Ingrese la utilidad">
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Estado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                      <!--<div style="margin-left:20px;"class="checkbox">
                        <label><input type="checkbox" name="campo" value="1"  <?php //if ($Documento->medio_pago) echo "checked"; 
                                                                              ?> ></label>
                      </div>-->

                      <select id="id_acreditable" class="form-control select2" required name="id_acreditable">
                        <option value="0">No acreditable</option>
                        <option value="1">Acreditable</option>
                      </select>
                      <span class="input-group-addon"> Bodega &nbsp;&nbsp;</span>
                      <select class="form-control select2" required name="id_bodega">
                        <?php $bodegas = Bodegas::get_activas(); ?>
                        <?php foreach ($bodegas as $bodega) : ?>
                          <option value="<?php echo $bodega->id; ?>"><?php echo $bodega->nombre; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' name="imagen" id="imagen" accept=".png, .jpg, .jpeg" />
                      <label for="imagen"></label>
                    </div>
                    <div class="avatar-preview">
                      <div id="imagePreview" style="background-image: url(https://arquilab.com.mx/wp-content/uploads/2021/05/1-14.jpg);">
                      </div>
                    </div>
                  </div>


                  <!-- 
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Estado Exogeranción:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                      <select class="form-control" name="exoneracion" onchange="CargarImpuestos(this.value);" required>
                    <option value="0" >Selecciona</option>
                    <option value="1" >Activo</option>
                    <option value="0" >Inactivo</option>
                  </select> 
                    </div>
                  </div>
                  
                   <div id="mostrar_impuestos">-->


                  <!-- Si hay productos exonerados -->


                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Impuesto &nbsp;</span>
                      <div class="dropdown" data-control="checkbox-dropdown">
                        <label class="form-control dropdown-label">Select</label>
                        <div class="dropdown-list">
                          <?php $lista = array(); ?>
                          <?php $proveedores = Tipo_impuesto_productos::get_active(); ?>
                          <?php foreach ($proveedores as $proveedor) : ?>
                            <label class="dropdown-option">
                              <input type="checkbox" name="dropdown-group[]" value="<?php echo $proveedor->id; ?>" />
                              <?php
                              echo $proveedor->nombre;
                              array_push($lista, $proveedor->id);
                              ?>
                            </label>
                          <?php endforeach; ?>
                          <!--<input class="form-control" name="lista_impuestos[]" value="<?php print_r($lista); ?>">-->

                        </div>
                      </div>
                      <!-- <span class="input-group-addon"> Utilidad&nbsp;&nbsp;&nbsp; </span>
                      <div class="dropdown" data-control="checkbox-dropdown">
                        <label class="form-control dropdown-label">Select</label>
                        <div class="dropdown-list">
                          <?php $lista = array(); ?>
                          <?php $utilidades = Utilidad_Productos::get_active(); ?>
                          <?php foreach ($utilidades as $utilidad) : ?>
                            <label class="dropdown-option">
                              <input type="checkbox" name="utilidad[]" value="<?php echo $utilidad->id; ?>" />
                              <?php
                              echo $utilidad->nombre;
                              array_push($lista, $utilidad->id);
                              ?>
                            </label>
                          <?php endforeach; ?>
                        </div>
                      </div>-->
                    </div>
                  </div>

                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-outline">Registrar producto</button>
            </div>
          </form>

        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>
  <!-- MODAL DE BUSQUEDA  -->
  <div class="modal fade bs-example-modal-xm" id="myModalBusquedaCabys" role="dialog" aria-labelledby="myModalBusquedaCabys">
    <div class="modal-dialog modal-success">
      <div class="modal-dialog">
        <div class="modal-content">
          <form class="form-horizontal">
            <div class="modal-header">
              <button type="button" id="buscador" name="buscador" class="close" onclick="activar_modal_registro(value)" value="0">
                <span aria-hidden="true">&times;</span></button>
              <center>
                <h4 class="modal-title"> INGRESAR ELEMENTO</h4>
              </center>
            </div>
            <div class="modal-body" style="background-color:#fff !important;">
              <div class="row">
                <div class="col-md-offset-1 col-md-10">

                  <div id="camp1" name="camp1" class="form-group">
                    <center>
                      <h5 for="filter_1" style="padding-top: 5px; font-size: 25px;">Buscar:</h5>
                    </center>
                    <input id="filter_1" name="filter_1" type="text" id="txtbusca" name="txtbusca" class="form-control input-sm w-sm mb-12 inline-block" />
                  </div>
                  <!-- <div id="camp1" name="camp1" class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"> Cabys &nbsp;&nbsp;</span>
                                            <input type="number" class="form-control col-md-8" name="nombre" required placeholder="Cabys" disabled="disabled">
                                        </div>
                                    </div>
 -->
                </div>
          </form>
          <table id="loader_c" name="loader_c" data-filter="#filter_1" data-page-size="7" class="footable table table-custom2" style="font-size: 11px;"></table>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>
  <!-- FINAL DE MODAL DE BUSQUEDA -->

  <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
  <script src="assets/js/vendor/footable/footable.all.min.js"></script>
  <script src="js/vista_productos/function.js"></script>
  <script src="js/vista_auto_cabys/script.js"></script>



  <script type="text/javascript">
    //Modal productos
    $(document).ready(function() {
      load_c();
    });
  </script>

  <script type="text/javascript">
    function isNum(val) {
      return !isNaN(val)
    }

    $(document).ready(function() {
      $("#filter_1").keyup(function() {

        d = document.getElementById("filter_1").value;
        var formato = isNum(d);
        //console.log(formato); 



        // En caso de ser numerico 
        if (formato == true) {
          var idModal = localStorage.getItem('idModal');

          var codigo = document.getElementById("filter_1").value;
          $.ajax({
            "url": "https://api.hacienda.go.cr/fe/cabys?codigo=" + codigo,
            "method": "GET"
          }).done(function(response) {
            const lista = response;
            parametros = {
              Lista: lista,
              estado: 2,
              idModal: idModal
            }
            $.ajax({
              type: "POST",
              url: 'index.php?action=actions_buscar_cabys',
              data: parametros,
              success: function(data) {
                $("#loader_c").html(data);
              }
            });

          });
        }

        // Si es string
        else {
          var idModal = localStorage.getItem('idModal');
          var codigo = document.getElementById("filter_1").value;
          $.ajax({
            "url": "https://api.hacienda.go.cr/fe/cabys?q=" + codigo + '&top=10',
            "method": "GET"
          }).done(function(response) {
            const lista = response;
            parametros = {
              Lista: lista,
              estado: 3,
              idModal: idModal
            }
            $.ajax({
              type: "POST",
              url: 'index.php?action=actions_buscar_cabys',
              data: parametros,
              success: function(data) {
                $("#loader_c").html(data);
              }
            });

          });
        }

      })
    })
  </script>


  <script>
    $(window).load(function() {

      $('.footable').footable();

    });
  </script>


  <script>
    var estado = <?php echo $alert_stock; ?>;

    if (estado == true) {
      $(function() {
        $("#myModal").modal();
      });
    }
  </script>


  <script>
    function del(id) {
      Swal.fire({
        title: 'Estas seguro?',
        text: "",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
      }).then((result) => {
        var parametros = {
          "id": id,
          "estado": "desactivar"
        };
        $('#tipo_cliente').html("Por favor espera un momento");
        $.ajax({
          type: "POST",
          url: 'index.php?action=actions_productos',
          data: parametros,
          success: function(data) {
            window.location.reload(); // Recargar página
          }
        });
      })
    }

    function valida(e) {
      tecla = (document.all) ? e.keyCode : e.which;

      //Tecla de retroceso para borrar, siempre la permite
      if (tecla == 8) {
        return true;
      }

      // Patron de entrada, en este caso solo acepta numeros
      patron = /[0-9]/;
      tecla_final = String.fromCharCode(tecla);
      return patron.test(tecla_final);
    }



    /*function CargarImpuestos(val) {
      $('#mostrar_impuestos').html("Por favor espera un momento");
      $.ajax({
        type: "POST",
        url: 'index.php?action=exoneracion_validacion',
        data: 'id=' + val,
        success: function(resp) {
          $('#mostrar_impuestos').html(resp);
        }
      });
    };*/

    function activar_busqueda(val) {
      if (val == 0) {
        $('#myModal_1').modal('hide'); //end of line
        $("#myModalBusquedaCabys").modal(); //end of line
        document.getElementById("vlestado").value = 1;
        document.getElementById("cd1").value = 1;
        document.getElementById("buscador").value = 0;
        localStorage.setItem('idModal', '0');
      } else if (val == 1) {
        document.getElementById("codigo_s").style.display = "none";
        document.getElementById("codigo").style.display = "flex";
        document.getElementById("vlestado").value = 0;
        document.getElementById("cd1").value = 0;
      }
    }


    function activar_busquedaM(val, mdID) {
      
       if (val == 2) {
        $('#myModal' + mdID).modal('hide'); //end of line
        $("#myModalBusquedaCabys").modal(); //end of line
        document.getElementById("buscador").value = mdID;
        localStorage.setItem('idModal', mdID);
        document.getElementById("vlestadoM"+mdID).value = 3;
        document.getElementById("cd2"+mdID).value = 3;
      }
      if (val == 3) {
        document.getElementById("codigo_sM"+mdID).style.display = "none";
        document.getElementById("codigoM"+mdID).style.display = "flex";
        document.getElementById("vlestadoM"+mdID).value = 2;
        document.getElementById("cd2"+mdID).value = 2;

      }
 
    }

    function activar_modal_registro(val) {
      if (val == 0) {
        $('#myModalBusquedaCabys').modal('hide'); //end of line
        $("#myModal_1").modal(); //end of line
      } else {
        $('#myModalBusquedaCabys').modal('hide'); //end of line
        $("#myModal" + val).modal(); //end of line
      }


    }
  </script>