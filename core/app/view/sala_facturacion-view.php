<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modulo Facturación</title>
</head>

<body>
    <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/tablas/tablas.css" type="text/css">
    <link rel="stylesheet" href="css/hacienda/style.css">

    <link rel="stylesheet" href="css/vista_Backup/style.css">
    <link rel="stylesheet" href="css/vista_Backup/botones.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <body id="minovate" class="appWrapper sidebar-sm-forced">
        <div class="row">
            <section class="content-header">
                <ol class="breadcrumb">
                    <li><a href="index.php?view=reserva"><i class="fa fa-home"></i> Inicio</a></li>
                    <li><a href="#">Configuración</a></li>
                    <li class="active">Documento de identificación</li>
                </ol>
            </section>
        </div>



        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-md-12">
                <section class="tile">
                    <div class="tile-header dvd dvd-btm">
                        <h1 class="custom-font"><strong>Facturación Electronica</strong></h1>
                        <ul class="controls">
                            <li class="dropdown">
                                <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
                                </a>
                                <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                    <li>
                                        <a role="button" tabindex="0" class="tile-toggle">
                                            <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                            <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a role="button" tabindex="0" class="tile-refresh">
                                            <i class="fa fa-refresh"></i> Refresh
                                        </a>
                                    </li>
                                    <li>
                                        <a role="button" tabindex="0" class="tile-fullscreen">
                                            <i class="fa fa-expand"></i> Fullscreen
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
                        </ul>
                    </div>
                    <!-- tile body -->
                    <div class="tile-body">
                        <div class="form-group">
                            <label for="filter" style="padding-top: 5px">Buscar:</label>
                            <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
                        </div>



                        <?php $Documentos = Proceso_hacienda::getAll();
                        if ($Documentos != null) {
                            // si hay usuarios
                        ?>
                            <!-- Muestra de la tabla -->
                            <table>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-plain">
                                            <div class="card-header card-header-primary">
                                                <center>
                                                    <h4 class="card-title mt-0">Facturas Realizadas</h4>
                                                </center>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" class="table table-hover">
                                                        <thead class="">
                                                            <tr>
                                                                <th>
                                                                    # Factura
                                                                </th>
                                                                <th>
                                                                    Caja
                                                                </th>
                                                                <th>
                                                                    Encargado
                                                                </th>
                                                                <th>
                                                                    Tipo Documento
                                                                </th>
                                                                <th>
                                                                    Datos Generales
                                                                </th>
                                                                <th>
                                                                    Datos Emisor
                                                                </th>
                                                                <th>
                                                                    Datos Receptor
                                                                </th>
                                                                <th>
                                                                    Detalles de Productos
                                                                </th>
                                                                <th>
                                                                    Estado
                                                                </th>
                                                                <th>
                                                                    Descargar
                                                                </th>
                                                                <th>
                                                                    Fecha
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($Documentos as $Documento) : ?>
                                                                <tr>
                                                                    <td><?php echo $Documento->id; ?></td>
                                                                    <?php $caja = CajasMData::getById($Documento->id_caja); ?>
                                                                    <td><?php print($caja->numero); ?></td>
                                                                    <?php $user = UserData::getById($Documento->id_usuario); ?>
                                                                    <td><?php print($user->name); ?></td>
                                                                    <?php if ($Documento->tipo_documento == "FE") {
                                                                        $Documento->tipo_documento = "Factura Electrónica";
                                                                    } else if ($Documento->tipo_documento == "TE") {
                                                                        $Documento->tipo_documento = "Tiquete Electrónico";
                                                                    } else if ($Documento->tipo_documento == "NC") {
                                                                        $Documento->tipo_documento = "Nota de Crédito";
                                                                    } else if ($Documento->tipo_documento == "ND") {
                                                                        $Documento->tipo_documento = "Nota de Débito";
                                                                    }   ?>
                                                                    <td><?php print($Documento->tipo_documento); ?></td>
                                                                    <td>
                                                                        <center><a data-toggle="modal" data-target="#myModal_edit<?php echo $Documento->id; ?>" style="background-color:#418bca; margin-bottom: -4px; border-color: #ffffff;" class="btn btn-primary btn-block2"><i class="glyphicon glyphicon glyphicon-signal"></i> Mostrar</a></center>
                                                                    </td>
                                                                    <td>
                                                                        <center><a data-toggle="modal" data-target="#myModal_emisor<?php echo $Documento->id; ?>" style="background-color:#418bca; margin-bottom: -4px; border-color: #ffffff;" class="btn btn-primary btn-block2"><i class="glyphicon glyphicon glyphicon-signal"></i> Mostrar</a></center>
                                                                    </td>
                                                                    <td>
                                                                        <center><a data-toggle="modal" data-target="#myModal_receptor<?php echo $Documento->id; ?>" style="background-color:#418bca; margin-bottom: -4px; border-color: #ffffff;" class="btn btn-primary btn-block2"><i class="glyphicon glyphicon glyphicon-signal"></i> Mostrar</a></center>
                                                                    </td>
                                                                    <td>
                                                                        <center><a data-toggle="modal" data-target="#myModal_prod<?php echo $Documento->id; ?>" style="background-color:#418bca; margin-bottom: -4px; border-color: #ffffff;" class="btn btn-primary btn-block2"><i class="glyphicon glyphicon glyphicon-signal"></i> Mostrar</a></center>
                                                                    </td>

                                                                    <td>
                                                                        <?php if ($Documento->estado == 1) {
                                                                            echo "<div style='color:#d9534f'>Rechazada</div>";
                                                                        } else {
                                                                            echo "<div style='color:#51c751'>Aceptada</div>";
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    <td>
                                                                        <center><a data-toggle="modal" data-target="#myModal_descarga<?php echo $Documento->id; ?>" style="background-color:#ffa726; margin-bottom: -4px; border-color: #ffffff;" class="btn btn-primary btn-block2"><i class="glyphicon glyphicon glyphicon-signal"></i> Documento</a></center>
                                                                    </td>
                                                                    <td>
                                                                        <div><?php echo $Documento->fecha_emision ?></div>
                                                                    </td>
                                                                </tr>

                                                                <!-- -----------------------------------------------------------------DATOS GENERALES-------------------------------------------------------------- -->


                                                                <div class="modal fade bs-example-modal-lg" id="myModal_edit<?php echo $Documento->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                    <div class="modal-dialog modal-success">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span></button>
                                                                                        <button type="button" class="btn btn-success" data-toggle="modal" id="vlestado" name="vlestado" onclick="" value="0">
                                                                                            <span class="glyphicon glyphicon-search"></span> Busqueda directa
                                                                                        </button>
                                                                                        <center>
                                                                                            <h4 class="modal-title"><span class="fa fa-folder-open"></span> Datos Generales</h4>
                                                                                        </center>
                                                                                    </div>



                                                                                    <main class="tab">


                                                                                        <ul class="tab__list" role="tablist">
                                                                                            <li class="tab__title">
                                                                                                <a class="tab__link is-active" href="#tab-<?php echo $Documento->id; ?>-1" role="tab" aria-selected="false" aria-controls="tab-<?php echo $Documento->id; ?>-1" tabindex="0">Sección 1</a>
                                                                                            </li>
                                                                                            <li class="tab__title">
                                                                                                <a class="tab__link" href="#tab-<?php echo $Documento->id; ?>-2" role="tab" aria-selected="false" aria-controls="tab-<?php echo $Documento->id; ?>-2" tabindex="0">Subtotales</a>
                                                                                            </li>
                                                                                            <li class="tab__title">
                                                                                                <a class="tab__link" href="#tab-<?php echo $Documento->id; ?>-3" role="tab" aria-selected="false" aria-controls="tab-<?php echo $Documento->id; ?>-3" tabindex="0">Totales</a>
                                                                                            </li>
                                                                                        </ul>



                                                                                        <div class="tab__main">
                                                                                        <section id="tab-<?php echo $Documento->id; ?>-1" class="tab__content" role="tabpanel" aria-expanded="true" >
                                                                                                <div class="row">
                                                                                                    <div class="col-md-offset-1 col-md-10">
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">

                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Plazo Credito&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->plazo_credito ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Medio Pago&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->medio_pago ?>" disabled>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Código Moneda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->cod_moneda ?>" disabled>
                                                                                                                        <span class="input-group-addon ab1"> Condición de Venta</span>
                                                                                                                        <?php
                                                                                                                        $cd_venta;
                                                                                                                        if ($Documento->condicion_venta == 1) {
                                                                                                                            $cd_venta = "Contado";
                                                                                                                        } else if ($Documento->condicion_venta == 2) {
                                                                                                                            $cd_venta = "Crédito";
                                                                                                                        } else if ($Documento->condicion_venta == 3) {
                                                                                                                            $cd_venta = "Consignación";
                                                                                                                        } else if ($Documento->condicion_venta == 4) {
                                                                                                                            $cd_venta = "Apartado";
                                                                                                                        } else if ($Documento->condicion_venta == 5) {
                                                                                                                            $cd_venta = "Arrendamiento con opción de compra";
                                                                                                                        } else if ($Documento->condicion_venta == 6) {
                                                                                                                            $cd_venta = "Arrendamiento en función financiera";
                                                                                                                        } else if ($Documento->condicion_venta == 99) {
                                                                                                                            $cd_venta = "Otro";
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $cd_venta ?>" disabled>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Tipo de Cambio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->tipo_cambio ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Dato General&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="#### " disabled>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                            </section>
                                                                                            <section id="tab-<?php echo $Documento->id; ?>-2" class="tab__content" role="tabpanel" aria-expanded="false" hidden>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-offset-1 col-md-10">
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">

                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Total Servicio Gravado&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->total_serv_gravados ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Total Servicio Exento&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->total_serv_exentos ?>" disabled>

                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Total Mercaderia Gravada &nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->total_merc_gravada ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Total Mercaderia Exenta&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->total_merc_exenta ?>" disabled>

                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Total Gravados&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->total_gravados ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Total Exento&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->total_exentos ?>" disabled>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                            </section>
                                                                                            <section id="tab-<?php echo $Documento->id; ?>-3" class="tab__content" role="tabpanel" aria-expanded="false" hidden>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-offset-1 col-md-10">
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Total Ventas&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->total_ventas ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Total Descuento&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->total_descuentos ?>" disabled>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Total Venta Neta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->total_ventas_neta ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Total Impuesto&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->total_impuesto ?>" disabled>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <div class="input-group">
                                                                                                                        <span class="input-group-addon"> Total Comprobante&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->total_comprobante ?>" disabled>
                                                                                                                        <span class="input-group-addon"> Token &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                        <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->token_llave ?>" disabled>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                            </section>
                                                                                        </div>
                                                                                    </main>

                                                                                    <div class="modal-body" style="background-color:#fff !important;">

                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                                                                                            <button type="button" class="btn btn-outline" data-dismiss="modal">Aceptar</button>
                                                                                        </div>
                                                                                </form>

                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                        </div>
                                                                        <!-- /.modal-dialog -->
                                                                    </div>
                                                                    <!-- /.modal -->
                                                                </div>

                                                                <!-- -----------------------------------------------------------------FIN DE DATOS GENERALES-------------------------------------------------------------- -->
                                                        <tfoot class="hide-if-no-paging"></tfoot>
                                                        <!-- -----------------------------------------------------------------DATOS EMISOR-------------------------------------------------------------- -->

                                                        <div class="modal fade bs-example-modal-lg" id="myModal_emisor<?php echo $Documento->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog modal-success">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span></button>
                                                                                <center>
                                                                                    <h4 class="modal-title"><span class="fa fa-folder-open"></span> Datos Emisor</h4>
                                                                                </center>
                                                                            </div>
                                                                            <main class="tab">
                                                                                <div class="tab__main">
                                                                                    <section id="tab-3" class="tab__content is-active" role="tabpanel" aria-expanded="true">
                                                                                        <div class="row">
                                                                                            <div class="col-md-offset-1 col-md-10">
                                                                                                <div class="form-group">
                                                                                                    <div class="input-group">
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Tipo Documento&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <?php
                                                                                                                $tipo_identif;
                                                                                                                if ($Documento->emisor_tipo_identif == 1) {
                                                                                                                    $tipo_identif = "Cédula Física";
                                                                                                                } else if ($Documento->emisor_tipo_identif == 2) {
                                                                                                                    $tipo_identif = "Cédula Jurídica";
                                                                                                                } else if ($Documento->emisor_tipo_identif == 3) {
                                                                                                                    $tipo_identif = "DIMEX";
                                                                                                                } else if ($Documento->emisor_tipo_identif == 4) {
                                                                                                                    $tipo_identif = "NITE";
                                                                                                                }
                                                                                                                ?>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $tipo_identif ?>" disabled>
                                                                                                                <span class="input-group-addon"> Cédula&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" style="width:100% !important;" name="marca" placeholder="<?php echo $Documento->emisor_num_identif ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Nombre Completo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" style="width:100% !important;" name="marca" placeholder="<?php echo $Documento->emisor_nombre ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Provincia &nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->emisor_provincia ?>" disabled>
                                                                                                                <span class="input-group-addon"> Canton &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" style="width:100% !important;" class="form-control" name="marca" placeholder="<?php echo $Documento->emisor_canton ?>" disabled>

                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Distrito &nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->emisor_distrito ?>" disabled>
                                                                                                                <span class="input-group-addon"> Código País&nbsp;</span>
                                                                                                                <input type="text" style="width:100% !important;" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->emisor_cod_pais_tel ?>" disabled>

                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Mail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" style="width:100% !important;" class="form-control" name="marca" placeholder="<?php echo $Documento->emisor_email ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                    </section>
                                                                                </div>
                                                                            </main>

                                                                            <div class="modal-body" style="background-color:#fff !important;">

                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                                                                                    <button type="button" class="btn btn-outline" data-dismiss="modal">Aceptar</button>
                                                                                </div>
                                                                        </form>

                                                                    </div>
                                                                    <!-- /.modal-content -->
                                                                </div>
                                                                <!-- /.modal-dialog -->
                                                            </div>
                                                            <!-- /.modal -->
                                                        </div>

                                                        <!-- -----------------------------------------------------------------FIN DE DATOS EMISOR-------------------------------------------------------------- -->
                                                        <tfoot class="hide-if-no-paging">
                                                        </tfoot>
                                                        <!-- -----------------------------------------------------------------DATOS RECEPTOR-------------------------------------------------------------- -->
                                                        <div class="modal fade bs-example-modal-lg" id="myModal_receptor<?php echo $Documento->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog modal-success">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span></button>
                                                                                <center>
                                                                                    <h4 class="modal-title"><span class="fa fa-folder-open"></span> Datos Emisor</h4>
                                                                                </center>
                                                                            </div>
                                                                            <main class="tab">
                                                                                <div class="tab__main">
                                                                                    <section id="tab-6" class="tab__content is-active" role="tabpanel" aria-expanded="true">
                                                                                        <div class="row">
                                                                                            <div class="col-md-offset-1 col-md-10">
                                                                                                <div class="form-group">
                                                                                                    <div class="input-group-receptor">
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Tipo Documento&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <?php
                                                                                                                $tipo_identif;
                                                                                                                if ($Documento->receptor_tipo_identif == 1) {
                                                                                                                    $tipo_identif = "Cédula Física";
                                                                                                                } else if ($Documento->receptor_tipo_identif == 2) {
                                                                                                                    $tipo_identif = "Cédula Jurídica";
                                                                                                                } else if ($Documento->receptor_tipo_identif == 3) {
                                                                                                                    $tipo_identif = "DIMEX";
                                                                                                                } else if ($Documento->receptor_tipo_identif == 4) {
                                                                                                                    $tipo_identif = "NITE";
                                                                                                                }
                                                                                                                ?>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $tipo_identif ?>" disabled>
                                                                                                                <span class="input-group-addon"> Cédula&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input style="width:70% !important;" type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->receptor_num_identif ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Nombre &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input style="width:88.5% !important;" type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->receptor_nombre ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>





                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Provincia &nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->emisor_provincia ?>" disabled>
                                                                                                                <span class="input-group-addon"> Canton &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" style="width:74% !important;" class="form-control" name="marca" placeholder="<?php echo $Documento->emisor_canton ?>" disabled>

                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Distrito &nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $Documento->emisor_distrito ?>" disabled>
                                                                                                                <span class="input-group-addon"> Código País&nbsp;</span>
                                                                                                                <input type="text" style="width:74% !important;" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $Documento->emisor_cod_pais_tel ?>" disabled>

                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Mail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" style="width:89% !important;" class="form-control" name="marca" placeholder="<?php echo $Documento->emisor_email ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                    </section>
                                                                                </div>
                                                                            </main>

                                                                            <div class="modal-body" style="background-color:#fff !important;">

                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                                                                                    <button type="button" class="btn btn-outline" data-dismiss="modal">Aceptar</button>
                                                                                </div>
                                                                        </form>

                                                                    </div>
                                                                    <!-- /.modal-content -->
                                                                </div>
                                                                <!-- /.modal-dialog -->
                                                            </div>
                                                            <!-- /.modal -->
                                                        </div>

                                                        <!-- -----------------------------------------------------------------FIN DE DATOS RECEPTOR-------------------------------------------------------------- -->
                                                        <tfoot class="hide-if-no-paging"></tfoot>
                                                        <!-- -----------------------------------------------------------------DATOS PRODUCTOS-------------------------------------------------------------- -->
                                                        <div class="modal fade bs-example-modal-lg" id="myModal_prod<?php echo $Documento->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog modal-success">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span></button>
                                                                                <center>
                                                                                    <h4 class="modal-title"><span class="fa fa-folder-open"></span> Datos Emisor</h4>
                                                                                </center>
                                                                            </div>
                                                                            <main class="tab">
                                                                                <?php $dt_producto = Detalle_producto_hac::getById($Documento->id) ?>
                                                                                <div class="tab__main">
                                                                                    <section id="tab-12" class="tab__content is-active" role="tabpanel" aria-expanded="true">
                                                                                        <div class="row">
                                                                                            <div class="col-md-offset-1 col-md-10">
                                                                                                <div class="form-group">
                                                                                                    <div class="input-group">
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Código &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $dt_producto->codigo ?>" disabled>
                                                                                                                <span class="input-group-addon"> Cantidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $dt_producto->cantidad ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Unidad de medidad &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $dt_producto->unidad_medida ?>" disabled>
                                                                                                                <span class="input-group-addon"> Detalle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $dt_producto->detalle ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Precio Unitario &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $dt_producto->precio_unitario ?>" disabled>
                                                                                                                <span class="input-group-addon"> Monto total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $dt_producto->monto_total ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Subtotal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $dt_producto->subtotal ?>" disabled>
                                                                                                                <span class="input-group-addon"> Monto total lineal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $dt_producto->monto_total_lineal ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                    </section>
                                                                                    <section id="tab-11" class="tab__content" role="tabpanel" aria-expanded="false" hidden>
                                                                                        <div class="row">
                                                                                            <div class="col-md-offset-1 col-md-10">
                                                                                                <div class="form-group">
                                                                                                    <div class="input-group">
                                                                                                        <div class="form-group">
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Monto Descuento &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $dt_producto->d_monto_descuento ?>" disabled>
                                                                                                                <span class="input-group-addon"> Naturaleza de descuento&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $dt_producto->d_naturaleza_descuento ?>" disabled>
                                                                                                            </div>
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Código Impuesto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $dt_producto->imp_codigo ?>" disabled>
                                                                                                                <span class="input-group-addon"> Codigo Impuesto Tarifa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $dt_producto->imp_codigo_Tarifa ?>" disabled>
                                                                                                            </div>
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"> Impuesto Tarifa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" id="nombre" name="nombre" required placeholder="<?php echo $dt_producto->imp_tarifa ?>" disabled>
                                                                                                                <span class="input-group-addon"> Monto Impuesto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                <input type="text" class="form-control" name="marca" placeholder="<?php echo $dt_producto->mp_monto ?>" disabled>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                    </section>
                                                                                </div>
                                                                            </main>

                                                                            <div class="modal-body" style="background-color:#fff !important;">

                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                                                                                    <button type="button" class="btn btn-outline" data-dismiss="modal">Aceptar</button>
                                                                                </div>
                                                                        </form>

                                                                    </div>
                                                                    <!-- /.modal-content -->
                                                                </div>
                                                                <!-- /.modal-dialog -->
                                                            </div>
                                                            <!-- /.modal -->
                                                        </div>

                                                        <!-- -----------------------------------------------------------------FIN DE DATOS PRODUCTOS-------------------------------------------------------------- -->
                                                        <tfoot class="hide-if-no-paging"></tfoot>
                                                        <!-- ----------------------------------------------------------------- DOCUMENTOS -------------------------------------------------------------- -->
                                                        <div class="modal fade bs-example-modal-lg" id="myModal_descarga<?php echo $Documento->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog modal-success">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span></button>
                                                                                <center>
                                                                                    <h4 class="modal-title"><span class="fa fa-folder-open"></span> Documentos</h4>
                                                                                </center>
                                                                            </div>
                                                                            <div class="flexbox-container">
                                                                                <div class="main">
                                                                                    <div class="service">
                                                                                        <h4>Descargar XML</h4>
                                                                                        <p style="margin-bottom: 50px;">Descarga de la factura creada por medio de un documento xml</p>
                                                                                        <div style="margin-bottom:-20px;" class="contenedor-redes-sociales">
                                                                                            <a style="margin-bottom:1px;" class="facebook" href="core/app/action/" target="_blank" download>
                                                                                                <span class="circulo"><i class="fa fa-download" aria-hidden="true"></i></span>
                                                                                                <span class="titulo">Descargar</span>
                                                                                                <span class="titulo-hover">Ahora</span>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="main">
                                                                                    <div class="service">
                                                                                        <h4>Descargar PDF</h4>
                                                                                        <p style="margin-bottom: 50px;">Descarga de la factura creada por medio de un documento pdf</p>
                                                                                        <div style="margin-bottom:-20px;" class="contenedor-redes-sociales">
                                                                                            <a style="margin-bottom:1px;" class="facebook" href="core/app/action/" target="_blank" download>
                                                                                                <span class="circulo"><i class="fa fa-download" aria-hidden="true"></i></span>
                                                                                                <span class="titulo">Descargar</span>
                                                                                                <span class="titulo-hover">Ahora</span>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="modal-body" style="background-color:#fff !important;">
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                                                                                    <button type="button" class="btn btn-outline" data-dismiss="modal">Aceptar</button>
                                                                                </div>
                                                                        </form>

                                                                    </div>
                                                                    <!-- /.modal-content -->
                                                                </div>
                                                                <!-- /.modal-dialog -->
                                                            </div>
                                                            <!-- /.modal -->
                                                        </div>


                                                        <!-- ----------------------------------------------------------------- FIN DOCUMENTOS -------------------------------------------------------------- -->

                                                    <?php endforeach; ?>
                                                    <tfoot class="hide-if-no-paging">
                                                        <tr>
                                                            <td colspan="5" class="text-center">
                                                                <ul class="pagination"></ul>
                                                            </td>
                                                        </tr>
                                                    </tfoot>

                                                    </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </table>

                        <?php } else {
                            echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
                        };
                        ?>
                    </div>
                </section>
            </div>
        </div>

        <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>

        <script src="assets/js/vendor/footable/footable.all.min.js"></script>

        <script src="js/hacienda/script.js"></script>



        <script>
            $(window).load(function() {

                $('.footable').footable();

            });
        </script>