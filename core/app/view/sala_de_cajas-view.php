<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/carts/carts.css">
<link rel="stylesheet" href="css/buttons/buttons.css">

<body id="minovate" class="appWrapper sidebar-sm-forced">
  <div class="row">

    <?php


    date_default_timezone_set('America/Costa_Rica');
    $hoy = date("Y-m-d");

    $u = null;
    $u = UserData::getById(Session::getUID());
    $usuario = $u->is_admin;
    $id_usuario = $u->id;

    $hora = date("H:i:s");
    $fecha_completo = date("Y-m-d H:i:s");

    ?>
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="index.php?view=reserva"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><a href="#">recepción</a></li>
      </ol>
    </section>
  </div>
  <!-- row -->
  <div class="row">
    <!-- col -->
    <div class="col-md-12">
      <section class="tile" id="padre">
        <div class="tile-header dvd dvd-btm">
          <h1 class="custom-font"><strong> VISTA GENERAL</strong> RECEPCIÓN</h1>
          <ul class="controls">

            <li class="remove">
              <a style="color: #616f77;"><i class="fa fa-arrow-circle-left" style="color: #616f77;"></i> Disponible</a>
            </li>
            <li class="remove">
              <a style="color: #d9534f;"><i class="fa fa-arrow-circle-right" style="color: #d9534f;"></i> Ocupado</a>
            </li>
            <li class="divisor"></li>
            <div class="container">
              <li><a href="./?view=cajas" class="button type1">
                  Mantenimiento
                </a></li>

              <li><a href="./?view=sala_de_cierre" class="button type2">
                  Cierre de caja
                </a></li>

              <li><a href="./?view=sala_de_liquidacion" class="button type3">
                  Resumen liquidación
                </a></li>
            </div>

            <li class="dropdown">
              <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
              </a>
              <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                <li>
                  <a role="button" tabindex="0" class="tile-toggle">
                    <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                    <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                  </a>
                </li>
            </li>
          </ul>
          </ul>
        </div>
        <div class="tile-body">
          <div class="row">
            <?php $habitaciones = CajasMData::getAll();
            // si hay usuarios 
            ?>
            <?php foreach ($habitaciones as $habitacion) : ?>
              <?php if ($habitacion->estado != 3) { ?>
                <div class="col-lg-2 col-xs-6">

                <?php } else if ($habitacion->estado == 3) { ?>
                  <div class="col-lg-2 col-xs-6" style="display:none;">

                  <?php }; ?>


                  <!-- small box -->
                  <?php if ($habitacion->estado == 1) { ?>
                    <section class="tile bg-greensea widget-appointments">

                    <?php } else if ($habitacion->estado == 2) { ?>
                      <section class="tile bg-danger widget-appointments">
                      <?php } else if ($habitacion->estado == 3) { ?>
                        <section class="tile bg-danger widget-appointments">

                        <?php  }; ?>


                        <?php $CajasAperturas = CajasAperturas::getAll();
                        if ($habitacion->estado == 1) { ?>
                          <?php
                          foreach($CajasAperturas as $cajaDisponible):
                          endforeach;
                          $CajasAperturas2 = CajasAperturas::getMontoActual($id_usuario);
                          //print_r($CajasAperturas2);
                          if ($CajasAperturas2 != null) { ?>
                            <!-- En esta seccion mostramos cada uno de las cajas disponibles-->
                            <div class="tile-body" style="padding: 1px;">
                              <div class="shop-card">
                                <div class="title">
                                  Caja : #<?php echo $habitacion->numero; ?> Disponible
                                </div>
                                <div class="cta">
                                  <div class="price">Apertura</div>
                                  <!--<button class="btn">Seleccionar<span class="bg" data-toggle="modal" data-target="#aperturaUsuario<?php echo $habitacion->id; ?>"></span></button>-->
                                  <button onclick="denegarPermiso()" class="btn">Seleccionar<span class="bg"></span></button>
                                </div>
                              </div>
                            </div>


                          <?php } else { ?>

                            <!-- En esta seccion mostramos cada uno de las cajas disponibles-->
                            <div class="tile-body" style="padding: 1px;">
                              <div class="shop-card">
                                <div class="title">
                                  Caja : #<?php echo $habitacion->numero; ?> Disponible
                                </div>
                                <div class="cta">
                                  <div class="price">Apertura</div>
                                  <!--<button class="btn">Seleccionar<span class="bg" data-toggle="modal" data-target="#aperturaUsuario<?php echo $habitacion->id; ?>"></span></button>-->
                                  <button class="btn" data-toggle="modal" data-target="#aperturaUsuario<?php echo $habitacion->id; ?>">Seleccionar<span class="bg"></span></button>
                                </div>
                              </div>
                            </div>
                            <!-- /tile body -->
                          <?php
                          }



                          ?>
                        <?php
                        } else if ($habitacion->estado == 3) { ?>
                          <div class="tile-body" style="padding: 50px;"></div>
                        <?php } else if ($habitacion->estado == 2) { ?>
                          <!-- En esta seccion mostramos cada uno de las cajas Ocupadas-->
                          <div class="tile-body" style="padding: 1px;">
                            <div class="shop-card2">
                              <div class="title">
                                Caja : #<?php echo $habitacion->numero; ?> Ocupada
                              </div>
                              <div style="font-size: 18px; margin-bottom:-8px;" class="title">
                                <?php
                                $info_usuario = CajasAperturas::getCajaOcupada($habitacion->id);
                                $nombre_usuario = UserData::getById($info_usuario->id_usuario);

                                ?>
                                <div style="margin-bottom:-18px; margin-top:-18px; font-size:14px; color:#c61a1a;" class="title"><?php print_r($nombre_usuario->username); ?></div>

                              </div>
                              <div class="cta">
                                <div class="price">Apertura</div>
                                <button onclick="ocupado()" class="btn">Seleccionar<span class="bg"></span></button>
                              </div>
                            </div>
                          </div>


                        <?php }; ?>

                        </section>

                  </div>


                  <div class="modal fade bs-example-modal-xm" id="aperturaUsuario<?php echo $habitacion->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-info">
                      <div class="modal-dialog">
                        <div class="modal-content">

                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><span class="fa fa-spinner"></span> Abrir Apertura</h4>
                            <!-- /.box-header -->
                            <form method="post" action="index.php?action=actions_cajas" id="addcaja">
                              <div class="tile-body p-0" style="text-align: left;">

                                <table style="margin-top:20px">
                                  <th style="width: 50%;"></th>
                                  <th style="width: 45%;"></th>
                                  <tr>
                                    <td>
                                      <h5>FECHA APERTURA:</h5>
                                    </td>
                                    <td>
                                      <h5 class="control-label text-red"><?php echo $hoy . ' ' . $hora; ?></h5>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td>
                                      <h5>MONTO APERTURA:</h5>
                                    </td>
                                    <td><input type="text" name="monto_apertura" onchange="this.value=this.value.replace(/\.$/, '')" onKeyUp="if (isNaN(this.value)) this.value=this.value.replace(/[^0-9.]/g,'')" required class="form-control text-red" placeholder="Ingrese monto" style="border-color: #dd4b47;"></td>

                                  </tr>
                                </table>

                              </div>


                              <!-- tile footer -->
                              <div class="tile-footer dvd dvd-top">

                                <div class="input-group">
                                  <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-left">Refrescar</a>
                                  <input type="hidden" name="fecha_apertura" value="<?php echo $fecha_completo; ?>">
                                  <input type="hidden" name="hora" value="<?php echo $hora; ?>">
                                  <input type="hidden" name="id_usuario" value="<?php echo $id_usuario; ?>">
                                  <?php $cajas_abiertas = CajasAperturas::getAllAbierto();
                                  $object = (array) $cajas_abiertas;
                                  $object = count($object);
                                  /*if( $object > 0)
                       
                       {
                         $caja_abierta='1';
                        } else {
                          $caja_abierta='0';
                        }*/
                                  ?>
                                  <input type="hidden" name="" value="<?php echo $caja_abierta; ?>" id="caja_abierta">
                                  <input type="hidden" class="form-control" name="id_estado" value="agregar_apertura">
                                  <input type="hidden" class="form-control" name="id_caja" value=<?php echo $habitacion->id ?>>
                                  <?php //echo $habitacion->id;
                                  $Info_caja = CajasAperturas::getAll();
                                  // print_r($Info_caja);
                                  $resultado = 0;
                                  //echo $id_usuario; 
                                  foreach ($Info_caja as $Info_caja2) :
                                    /*echo $Info_caja2->id_usuario;
                            echo $Info_caja2->estado;*/
                                    if ($Info_caja2->id_usuario == $id_usuario and $Info_caja2->estado == 1) {
                                      $resultado = 1;
                                    }
                                  endforeach;
                                  //print($resultado);
                                  if ($resultado != 1) {
                                  ?>
                                    <input type="submit" class="btn btn-sm btn-success btn-flat pull-right" value="Dar apertura">
                                  <?php }
                                  ?>

                                </div>

                              </div>
                              <!-- /tile footer -->

                            </form>
                          </div>


                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </div>
                <?php endforeach; ?>

      </section>
    </div>
  </div>

  <script>
    function ocupado() {
      Swal.fire({
        title: '',
        text: 'Hay alguien mas ocupando esta caja!',
        icon: 'error',
        confirmButtonColor: '#b91a1a',
        confirmButtonText: 'Volver luego'
      })
    }

    function ocultarPadre() {

    }

    function denegarPermiso() {

      Swal.fire({
        title: '',
        text: 'Ya estas utilizando una caja, vuelve cuando la cierres!',
        icon: 'error',
        confirmButtonColor: '#b91a1a',
        confirmButtonText: 'Volver luego'
      })
    }

    /* ----------------------------------------------------------- DIVISION DE SECCIONES------------------------------------------------------------------------------------------- */
  </script>
  <script>
    $("#addcaja").submit(function(e) {
      caja = $("#caja_abierta").val();

      if (caja == "1") {
        Swal.fire({
          title: '',
          text: 'Ya estas utilizando una caja, vuelve cuando la cierres!',
          icon: 'error',
          confirmButtonColor: '#b91a1a',
          confirmButtonText: 'Volver luego'
        })
        e.preventDefault();
      }
    });
  </script>




  <?php $cajas_estado_abierto = CajasAperturas::getAllCajasEstadoAbierto();
  $object = (array) $cajas_estado_abierto;
  $object = count($object);

  ?>

  <!-- tile -->
  <section class="tile  col-md-12">

    <!-- tile header -->
    <div class="tile-header dvd dvd-btm">
      <h1 class="custom-font"><strong>CAJAS ABIERTAS EN FECHAS DE </strong>HOY</h1>
    </div>
    <!-- /tile header -->

    <!-- tile body -->
    <div class="tile-body p-0">
      <?php if ($object > 0) { ?>
        <table class="table table-condensed" style="font-size: 12px;">
          <thead style="background-color: #16a085; color: white;">
            <tr>
              <th>FECHA DE APERTURA</th>
              <th>MONTO APERTURA</th>
              <th>EGRESOS</th>
              <th>VENTA TOTAL</th>

              <th>VENTA TOTAL + APERTURA</th>
              <th>USUARIO RESPONSABLE</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($cajas_estado_abierto as $cajas) : ?>
              <tr>
                <td><?php echo $cajas->fecha_apertura; ?></td>
                <td>$ <?php echo number_format($cajas->monto_apertura, 2, '.', ','); ?></td>

                <!-- INGRESOS -->
                <?php $montos_sin_cerrar = ProcesoData::getIngresoCaja($cajas->id);
                $total_sin_cerrar = 0;
                if (count($montos_sin_cerrar) > 0) {

                  foreach ($montos_sin_cerrar as $monto_sin_cerrar) :
                    $total_sin_cerrar = (($monto_sin_cerrar->precio * $monto_sin_cerrar->cant_noche) + $monto_sin_cerrar->total) + $total_sin_cerrar;
                  endforeach;
                }
                ?>


                <?php
                if ($cajas->id != 0) {
                  $reporproducts = ProcesoVentaData::getIngresoCaja($cajas->id);
                  $subtotal3 = 0;
                  if (count($reporproducts) > 0) { ?>
                    <?php foreach ($reporproducts as $reporproduct) : ?>
                      <?php $subtotal1 = $reporproduct->cantidad * $reporproduct->precio; ?>
                      <?php $subtotal3 = $subtotal1 + $subtotal3; ?>
                    <?php endforeach; ?>
                  <?php } else {
                    $subtotal3 = 0;
                  } ?>
                <?php } else {
                  $subtotal3 = 0;
                } ?>

                <!-- FIN INGRESOS -->




                <!-- EGRESOS -->

                <?php $montos_sin_cerrar_egresos = GastoData::getEgresoCaja($cajas->id);
                $total_sin_cerrar_egreso = 0;
                if (count($montos_sin_cerrar_egresos) > 0) {

                  foreach ($montos_sin_cerrar_egresos as $montos_sin_cerrar_egreso) :
                    $total_sin_cerrar_egreso = $montos_sin_cerrar_egreso->precio + $total_sin_cerrar_egreso;
                  endforeach;
                }
                ?>

                <?php $montos_sin_cerrar_sueldos = ProcesoSueldoData::getSueldoCaja($cajas->id);
                $total_sin_cerrar_sueldos = 0;
                if (count($montos_sin_cerrar_sueldos) > 0) {

                  foreach ($montos_sin_cerrar_sueldos as $montos_sin_cerrar_sueldo) :
                    $total_sin_cerrar_sueldos = $montos_sin_cerrar_sueldo->monto + $total_sin_cerrar_sueldos;
                  endforeach;
                }
                ?>


                <?php
                if ($cajas->id != 0) {
                  $reporproducts_es = ProcesoVentaData::getEgresoCaja($cajas->id);
                  $subtotal4 = 0;
                  if (count($reporproducts_es) > 0) { ?>
                    <?php foreach ($reporproducts_es as $reporproduct_e) : ?>
                      <?php $subtotal1 = $reporproduct_e->cantidad * $reporproduct_e->precio; ?>
                      <?php $subtotal4 = $subtotal1 + $subtotal4; ?>
                    <?php endforeach; ?>
                  <?php } else {
                    $subtotal4 = 0;
                  } ?>
                <?php } else {
                  $subtotal4 = 0;
                } ?>



                <!-- Total egreso -->
                <?php $total_egreso = $total_sin_cerrar_egreso + $total_sin_cerrar_sueldos + $subtotal4; ?>
                <!-- Fin Total egreso -->

                <!-- Total ingreso -->
                <?php $total_ingreso = $total_sin_cerrar + $subtotal3; ?>
                <!-- Fin Total ingreso -->
                <?php ?>

                <td>$ <?php echo number_format($total_egreso, 2, '.', ','); ?></td>
                <td>$ <?php echo number_format($total_ingreso, 2, '.', ','); ?></td>
                <?php $total = ($cajas->monto_apertura + $total_ingreso) - $total_egreso; ?>
                <td>$ <?php echo number_format($total, 2, '.', ','); ?></td>
                <td><?php if ($cajas->id_usuario != null) {
                      echo $cajas->getUsuario()->name;
                    } else {
                      echo "<center>----</center>";
                    }  ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      <?php } else { ?>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> No hay ningún apertura de caja!</h4>

        </div>
      <?php }; ?>

    </div>
    <!-- /tile body -->

  </section>
  <!-- /tile -->


  <script src='lib/locale/es.js'></script>

  <!--bug de carga-->
  <!--<script src="js/reception.js">-->