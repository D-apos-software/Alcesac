<?php


if (count($_POST) > 0) {

  $cliente = PersonaData::getById($_POST["id_cliente"]);
  $cliente->tipo_documento = $_POST["tipo_documento"];
  $cliente->documento = $_POST["documento"];
  $cliente->nombre = $_POST["nombre"];

  $razon_social = "NULL";
  if ($_POST["razon_social"] != "") {
    $razon_social = $_POST["razon_social"];
  }

  $giro = "NULL";
  if ($_POST["giro"] != "") {
    $giro = $_POST["giro"];
  }

  $direccion = "NULL";
  if ($_POST["direccion"] != "") {
    $direccion = $_POST["direccion"];
  }



  $provincia = "NULL";
  if ($_POST["provincia"] != "") {
    $provincia = $_POST["provincia"];
  }

  $canton = "NULL";
  if ($_POST["canton"] != "") {
    $canton = $_POST["canton"];
  }else{
    $canton = $_POST["cantonMD"];
  }

  $distrito = "NULL";
  if ($_POST["distrito"] != "") {
    $distrito = $_POST["distrito"];
  }else{
    $distrito = $_POST["distritoMD"];
  }

  $barrio = "NULL";
  if ($_POST["barrio"] != "") {
    $barrio = $_POST["barrio"];
  }





  $fecha_nac = "";
  if ($_POST["fecha_nac"] != "") {
    $fecha_nac = $_POST["fecha_nac"];
  }

  $telefono = "NULL";
  if ($_POST["telefono"] != "") {
    $telefono = $_POST["telefono"];
  }

  $telefono_sec = "NULL";
  if ($_POST["telefono_sec"] != "") {
    $telefono_sec = $_POST["telefono_sec"];
  }

  $email = "";
  if ($_POST["email"] != "") {
    $email = $_POST["email"];
  }

  $exonerado = "0";
  if ($_POST["checkbox"] != "") {
    $exonerado = $_POST["checkbox"];
  }

  $id_categoria_p = 0;
  if ($_POST["id_categoria_p"] != "") {
    $id_categoria_p = $_POST["id_categoria_p"];
  }



  $cliente->razon_social = $razon_social;
  $cliente->giro = $giro;
  $cliente->direccion = $direccion;
  $cliente->provincia = $provincia;
  $cliente->canton = $canton;
  $cliente->distrito = $distrito;
  $cliente->barrio = $barrio;
  $cliente->fecha_nac = $fecha_nac;
  $cliente->email = $email;
  $cliente->telefono = $telefono;
  $cliente->telefono_sec = $telefono_sec;
  $cliente->exonerado = $exonerado;
  $cliente->id_categoria_p = $id_categoria_p;
  $cliente->updatecliente();

  print "<script>window.location='index.php?view=cliente';</script>";
}
