<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<?php
require __DIR__ . '/../../../core/app/action/action-functions-products.php';

if (count($_POST) > 0) {
	$utilidad = $_POST["utilidad"];

	$marca = "NULL";
	if ($_POST["marca"] != "") {
		$marca = $_POST["marca"];
	}

	$presentacion = "NULL";
	if ($_POST["presentacion"] != "") {
		$presentacion = $_POST["presentacion"];
	}

	$descripcion = "NULL";
	if ($_POST["descripcion"] != "") {
		$descripcion = $_POST["descripcion"];
	}

	$precio_compra = "0";
	if ($_POST["precio_compra"] != "") {
		$precio_compra = $_POST["precio_compra"];
	}

	$id_proveedor = "0";
	if ($_POST["id_proveedor"] != "") {
		$id_proveedor = $_POST["id_proveedor"];
	}
	$id_acreditable = "0";
	if ($_POST["id_acreditable"] != "") {
		$id_acreditable = $_POST["id_acreditable"];
	}

	if (empty($_POST["id_bodega"]) == null) {
		$id_bodega = $_POST["id_bodega"];
	}


	
	
	if( $_POST['id_descuento'] != 0 && $_POST['id_descuento'] != "0"){
		$descuento = Descuento_Producto::getById($_POST["id_descuento"]);
		$valor_D = $descuento->valor;
	}else{
		$valor_D = 0;
	}
	$utilidad = $_POST["utilidad"];
	
	//print($_POST['id_descuento']);

	if ($valor_D > $utilidad) {
?>
		<script>
			swal({
				title: "¡ERROR!",
				text: "La Utilidad no puede ser menor al Descuento",
				type: "error",
			}).then(function() {
				window.location = "index.php?view=productos";
			});
		</script>
	<?php
	} else if ($utilidad == 0 || $utilidad < 0) {

	?>
		<script>
			swal({
				title: "¡ERROR!",
				text: "Verificar el valor de utilidad ingresado",
				type: "error",
			}).then(function() {
				window.location = "index.php?view=productos";
			});
		</script>


		<?php
	} else {
		//Validamos que la cantidad del producto a registrar no sobrepase la cantidad maxima del almacenamiento de la bodega
		$bodega_stock = ProcesoVentaData::get_stock_bodega($_POST['id_bodega']);
		if (isset($bodega_stock)) {
			$total_stock = $bodega_stock->stock + $bodega_stock->compras;
			$total_stock = $total_stock - $bodega_stock->ventas;
			//print("<br><br><br>" . $total_stock);
			$total_stock = $total_stock + $_POST['stock'];
			//print("<br><br><br>" . $total_stock);
			//print("<br><br><br>" . $bodega_stock->cantidad_maxima);
			$stock_val = $bodega_stock->cantidad_maxima;
		} else {

			$total_stock = 0;
			$stock_val = 1;
		}
		if ($stock_val < $total_stock) {
		?>

			<script>
				swal({
					title: "¡ERROR!",
					text: "La cantidad de stock inicial del producto añadir, supera la cantidad máxima del almacenamiento de la bodega seleccionada.",
					type: "error",
				}).then(function() {
					window.location = "index.php?view=productos";
				});
			</script>

			<?php
		} else {
			try {


				$producto = new ProductoData();
				
				 if($_POST["cd1"] == 0){
					$producto->codigo = $_POST["codigo"];
				}else if($_POST["cd1"] == 1){
					$producto->codigo = $_POST["codigo_s"];
				} 

				$producto->codigo_barra = $_POST["codigo_barra"];
				$producto->nombre = $_POST["nombre"];

				//Validando y Moviendo imagen 
				if ($_FILES['imagen'] != Null) {
					$archivo = $_FILES['imagen']['tmp_name'];
					$destino = 'img/img_productos/' . $_FILES['imagen']['name'];
					move_uploaded_file($archivo, $destino);
				}
				/*print("<br><br><br><br>".$_POST["nombre"]."<br>".$_POST["codigo_barra"]."<br>".$_POST["codigo"]."<br>"
				.$presentacion."<br>".$marca."<br>".$descripcion."<br>".$id_acreditable."<br>"
				.$precio_compra."<br>".$_POST["precio_venta"]."<br>".$_POST["stock"]."<br>".$id_proveedor.
				"<br>". $_POST["cantidad_maxima"]."<br>".$_POST["id_bodega"]."<br>".$_POST["aviso_stock"].
				"<br>".$_POST["id_unidad_medida"]."<br>".$_POST["id_descuento"]."<br>".$_POST["utilidad"]."<br>".$destino);*/
				$producto->presentacion = $presentacion;
				$producto->marca = $marca;
				$producto->descripcion = $descripcion;
				$producto->acreditable = $id_acreditable;
				$producto->precio_compra = $precio_compra;
				$producto->precio_venta = $_POST["precio_venta"];
				$producto->stock = $_POST["stock"];
				$producto->id_proveedor = $id_proveedor;
				$producto->stock_max = $_POST["cantidad_maxima"];
				$producto->id_bodega = $_POST["id_bodega"];
				$producto->aviso_stock = $_POST["aviso_stock"];
				$producto->id_unidad_medida = $_POST["id_unidad_medida"];
				$producto->id_descuento = $_POST["id_descuento"];
				$producto->utilidad = $_POST["utilidad"];
				$producto->imagen = $destino;
				$data_product = $producto->add();

				
					//Insertamos informacion relacionada entre producto y los impuestos seleccionados
					$data = $_POST["dropdown-group"];
					foreach ($data as $datas) {
						//print($datas . 'insert <br>');
						insert($datas, $data_product[1]);
					}


				//Insertamos informacion relacionada entre producto y los descuentos seleccionados
				/*$datas_utilidad = $_POST["utilidad"];
			foreach ($datas_utilidad as $id_utilidad) {
				//print($id_descuento . 'insert <br>');
				insert_utilidad($id_utilidad, $data_product[1]);
			}*/

			?>
				<script>
					swal({
						title: "¡Se Registro correctamente!",
						icon: 'success',
						text: "Se registro correctamente.<br> Causa : ".$th,
						type: "success",
					}).then(function() {
						window.location = "index.php?view=productos";
					});
				</script>

			<?php

			} catch (\Throwable $th) {
			?>
				<script>
					swal({
						title: "¡ERROR!",
						text: "No se ha podido registrar el producto.<br> Causa : ".$th,
						type: "error",
					}).then(function() {
						window.location = "index.php?view=productos";
					});
				</script>
<?php
			}
		}
	}
}
