<?php
require __DIR__ . '/../../../core/app/action/action-functions-personas.php';
?>

<head>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<br><br><br><?php
            $session_id = session_id();
            ini_set('date.timezone', 'America/Costa_Rica');
            
            if (count($_POST) > 0) {

              try {

                $u = null;
                $u = UserData::getById(Session::getUID());
                $usuario = $u->is_admin;
                $id_usuario = $u->id;


                $cajas = CajasAperturas::getMontoActual($id_usuario);
                $id_caja = $cajas->id;

                $cliente = new PersonaData();
                $tip_doc = TipoDocumentoData::getByValor($_POST["tipo_documento"]);
                $cliente->tipo_documento = $tip_doc->id;
                $cliente->documento = $_POST["documento"];
                $cliente->nombre = $_POST["nombre"];
                $cliente->giro = $_POST["giro"];

                $direccion = "NULL";
                if ($_POST["direccion"] != "") {
                  $direccion = $_POST["direccion"];
                }
                $cliente->direccion = $direccion;

                $razon_social = "NULL";
                if ($_POST["razon_social"] != "") {
                  $razon_social = $_POST["razon_social"];
                }
                $cliente->razon_social = $razon_social;

                $ocupacion = "NULL";
                if ($_POST["ocupacion"] != "") {
                  $ocupacion = $_POST["ocupacion"];
                }
                $cliente->ocupacion = $ocupacion;

                $medio_transporte = "NULL";
                if ($_POST["medio_transporte"] != "") {
                  $medio_transporte = $_POST["medio_transporte"];
                }
                $cliente->medio_transporte = $medio_transporte;

                $destino = "NULL";
                if ($_POST["destino"] != "") {
                  $destino = $_POST["destino"];
                }
                $cliente->destino = $destino;

                $motivo = "NULL";
                if ($_POST["motivo"] != "") {
                  $motivo = $_POST["motivo"];
                }
                $cliente->motivo = $motivo;

                //Añadimos telefono y email
                $email = "NULL";
                if ($_POST["email"] != "") {
                  $email = $_POST["email"];
                }
                $cliente->email = $email;

                $telefono = "NULL";
                if ($_POST["telefono"] != "") {
                  $telefono = $_POST["telefono"];
                }
                $cliente->telefono = $telefono;


                //Validacion exoneracion 
                if (isset($_POST["checkbox"]) and $_POST['checkbox'] != "") {
                  $cliente->exonerado = $_POST["checkbox"];
                }

                // Categoria promocion 
                $id_categoria_p = 0;
                if ($_POST["categoria"] != "") {
                  $id_categoria_p = $_POST["categoria"];
                }
                $cliente->id_categoria_p = $id_categoria_p;

                $clientes_bd = PersonaData::getLikeDni($_POST["documento"]);
                $a = '';
                if($clientes_bd != null && $clientes_bd != ""){
                  $a = (int)$clientes_bd->documento;
                }
                
                $b =(int)$_POST["documento"];
                $persona_exist = 0;
                if ($a == $b ) {
                  $persona_exist = 1;
                  $s_2 = $clientes_bd->id;
                } else {
                  $s = $cliente->add();
                  $persona_exist = 0;
                }

                //$s = $cliente->add();
                $habitacion = HabitacionData::getById($_POST["id_habitacion"]);
                $habitacion->estado = 2;
                $habitacion->updateEstado();
                 if ($persona_exist == 0) {
                  $precio = val_habitacion_promocion($_POST["id_habitacion"], $_POST["precio"], $s[1]);
                } else {
                  $precio = val_habitacion_promocion($_POST["id_habitacion"], $_POST["precio"], $s_2);
                } 
                $proceso = new ProcesoData();
                $proceso->id_habitacion = $_POST["id_habitacion"];
                $proceso->id_tarifa = $_POST["id_tarifa"];


                 if ($persona_exist == 0) {
                  $proceso->id_cliente = $s[1];
                } else {
                  $proceso->id_cliente = $s_2;
                } 

                $proceso->precio = $precio;
                $proceso->cant_noche = $_POST["cant_noche"];
                $proceso->dinero_dejado = 0;
                $proceso->fecha_entrada = date('Y-m-j H:i:s');
                $proceso->fecha_salida = $_POST["fecha_salida"] . ' ' . $_POST['hora_salida'];
                $proceso->id_usuario = $_SESSION["user_id"];
                $proceso->cant_personas = 1;
                $proceso->id_caja = $id_caja;
                $proceso->cantidad = $_POST["cantidad"];
                $proceso->pagado = $_POST["pagado"];


                // Validamos el id de la apertura actual
                if ($_POST["pagado"] == 1) {
                  $proceso->estado = 1;
                  $proceso->tipo_proceso = 1;
                } else {
                  $proceso->estado = 0;
                  $proceso->tipo_proceso = 0;
                }

                $informacion = CajasAperturas::getCierreCaja($id_usuario);
                //if($_POST["pagado"] == 1){
                $proceso->id_apertura = $informacion->id;
                //}else{
                // $proceso->id_apertura = 0;
                //}

                if ($_POST["numero_aprobacion"] != null) {
                  $proceso->num_aprobacion = $_POST["numero_aprobacion"];
                } else {
                  $proceso->num_aprobacion = 0;
                }
                if ($_POST["nro_operacion"] != null) {
                  $proceso->nro_operacion = $_POST["nro_operacion"];
                } else {
                  $proceso->nro_operacion = 0;
                }
                if ($_POST["banco"] != null) {
                  $proceso->banco = $_POST["banco"];
                } else {
                  $proceso->banco = 0;
                }
                if ($_POST["num_tarjeta"] != null) {
                  $proceso->num_tarjeta = $_POST["num_tarjeta"];
                } else {
                  $proceso->num_tarjeta = 0;
                }
                if ($_POST["id_tipo_pago"] != null && $_POST["id_tipo_pago"] != 0) {
                  $proceso->id_tipo_pago = $_POST["id_tipo_pago"];
                } else {
                  $proceso->id_tipo_pago = 0;
                }



                $f = $proceso->addIngreso();


                $cliente_proceso = new ClienteProcesoData();
                if($persona_exist == 0){
                  $cliente_proceso->id_cliente = $s[1];
                }else{
                  $cliente_proceso->id_cliente = $s_2;
                }
                
                $cliente_proceso->sesion = $session_id;
                $cliente_proceso->id_proceso = $f[1];
                $cliente_proceso->add();




                Actualizar_Apertura::actualizar();

            ?>

      <meta http-equiv="refresh" content="2; url=index.php?view=venta"> 
    <script type="text/javascript">
      Swal.fire({
        icon: 'success',
        title: 'Reserva exitosa',
        showConfirmButton: false,
        timer: 1700
      }).then(function() {
        window.location = "index.php?view=recepcion";
      })
    </script>
  <?php
              } catch (Exception $e) {
  ?>
    <script>
      Swal.fire({
        icon: 'error',
        title: 'Aviso...',
        text: 'No se pudo realizar la reserva ....',
        footer: '<a href="index.php">Volver al inicio?</a>'
      }).then(function() {
        window.location = "index.php?view=recepcion";
      });
    </script>
    <meta http-equiv="refresh" content="3; url=index.php?view=recepcion">
  <?php

              }
            } else {
  ?>
  <script>
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'No se agregó ningun cliente ....',
      footer: '<a href="index.php">Volver al inicio?</a>'
    }).then(function() {
      window.location = "index.php?view=recepcion";
    });
  </script>
  <meta http-equiv="refresh" content="3; url=index.php?view=recepcion">
<?php
            }

?>