<head>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
</head>

<?php

$dataCliente = PersonaData::getByDocumento($_POST["documento"]);

if($dataCliente->id){
?>
 <script>
				Swal.fire({
					icon: 'error',
					title: 'Error',
					text: 'El cliente ingresado ya existe',
					footer: '<a href="index.php">Volver al inicio?</a>'
				}).then(function() {
					window.location = "index.php?view=cliente";
				});
			</script>
			<meta http-equiv="refresh" content="3; url=index.php?view=cliente">

<?php
}else{
if(count($_POST)>0){
try{
	$cliente = new PersonaData();
	$cliente->tipo_documento = $_POST["tipo_documento"];
	$cliente->documento = $_POST["documento"];
	$cliente->nombre = $_POST["nombre"];

	$razon_social="NULL";
  if($_POST["razon_social"]!=""){ $razon_social=$_POST["razon_social"];}

  $giro="NULL";
  if($_POST["giro"]!=""){ $giro=$_POST["giro"];}

  $direccion="NULL";
  if($_POST["direccion"]!=""){ $direccion=$_POST["direccion"];}
  
  $provincia="NULL";
  if($_POST["provincia"]!=""){ $provincia=$_POST["provincia"];}

  $canton="NULL";
  if($_POST["canton"]!=""){ $canton=$_POST["canton"];}

  $barrio="NULL";
  if($_POST["barrio"]!=""){ $barrio=$_POST["barrio"];}

  $distrito="NULL";
  if($_POST["distrito"]!=""){ $distrito=$_POST["distrito"];}


  $fecha_nac="";
  if($_POST["fecha_nac"]!=""){ $fecha_nac=$_POST["fecha_nac"];}
  
  $telefono="NULL";
  if($_POST["telefono"]!=""){ $telefono=$_POST["telefono"];}

  $telefono2="NULL";
  if($_POST["telefono_sec"]!=""){ $telefono2=$_POST["telefono_sec"];}

  $email="";
  if($_POST["email"]!=""){ $email=$_POST["email"];}
  
  $exonerado="0";
  if (isset($_POST['checkbox'])){
  if($_POST["checkbox"]!=""){ $exonerado=$_POST["checkbox"];}
  }
  $id_categoria_p = 0;
  if (isset($_POST['id_categoria_p'])){
    if($_POST["id_categoria_p"]!=""){ $id_categoria_p=$_POST["id_categoria_p"];}
    }
	$cliente->razon_social = $razon_social;
	$cliente->giro = $giro;
	$cliente->direccion = $direccion;
  $cliente->provincia = $provincia;
  $cliente->canton = $canton;
  $cliente->distrito = $distrito;
  $cliente->barrio = $barrio;
	$cliente->fecha_nac = $fecha_nac;
	$cliente->email = $email;
	$cliente->telefono = $telefono;
  $cliente->telefono2 = $telefono2;
	$cliente->exonerado = $exonerado;
  $cliente->id_categoria_p = $id_categoria_p;
	$cliente->addCliente();
?>
<meta http-equiv="refresh" content="2; url=index.php?view=cliente">
		<script type="text/javascript">
			Swal.fire({
				icon: 'success',
				title: 'Satisfactoria',
				showConfirmButton: false,
				timer: 1700
			}).then(function() {
				window.location = "index.php?view=cliente";
			})
		</script>

<?php 
  }catch(Exception $e){
       
   ?>
   
   <script>
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'No se añadio el usuario!',
					footer: '<a href="index.php">Volver al inicio?</a>'
				}).then(function() {
					window.location = "index.php?view=cliente";
				});
			</script>
			<meta http-equiv="refresh" content="3; url=index.php?view=cliente">
   
   
   
    <?php
  }

}
}

?>