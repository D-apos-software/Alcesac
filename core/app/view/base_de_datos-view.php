<!-- Espaciamos -->
<br><br><br>
<!-- - -->
<div class="container my-5">
<link rel="stylesheet" href="css/vista_Backup/style.css">
<link rel="stylesheet" href="css/vista_Backup/botones.css">
<link rel="stylesheet" href="css/vista_Backup/seccion_eliminar.css">

    <section>
        <div class="text-center mb-5">
            <span>Relaciones de base de datos</span>
            <h2 class="font-weight-bold display-4 ">¿Que deseas <span style="color: #2196f3;">realizar</span> ?</h2>
            <div class="archivo" name="archivo" id="archivo" > </div>
        </div>
    </section>
</div>
<!-- Espaciamos -->
<br><br><br><br><br>
<!-- - -->
<div class="container_boton">
  <div class="row">
        <!-- Primera opcion -->
        <div class="col-md-4" style="margin-left:240px">
          <div class="main">
            <div class="service">
              <div class="service-logo">
                <img src="https://www.incubaweb.com/wp-content/uploads/2016/09/upload.png">
              </div>
              <h4>Actualizar Base de Datos</h4>
              <p>Sustituiras el contenido que se encuentran en la base de datos de tu Software.</p>
              

              <form method="POST" action="index.php?action=restore" enctype="multipart/form-data">
					    <div class="form-group row">
					      	<label for="sql" class="col-sm-3 col-form-label">Archivo</label>
					      	<div class="col-sm-9">
					        	<input type="file" class="form-control-file" id="sql" name="sql" placeholder="base de datos que deseas restaurar" required>
					      	</div>
					    </div>
					    <button type="submit" style="margin-top: -24px;color: #fff; background-color: #337ab700;border-color: #2e6da400;  " name="restore">
              <div type="submit" name="restore" style="margin-bottom:-20px;" class="contenedor-redes-sociales">
                    <a style="margin-bottom:1px;" class="facebook" target="_blank" >
                        <span class="circulo"><i class="fa fa-upload" aria-hidden="true"></i></span>
                        <span class="titulo">Subir</span>
                        <span class="titulo-hover">Restaurar</span>
                    </a>
                </div></button>
					</form>
            </div>
          </div>
        </div>
        
        <!-- Segunda opcion -->
        <div class="col-md-4">
          <div   class="main">
            <div class="service">
              <div class="service-logo">
                <img src="http://colecr.com/wp-content/uploads/2017/12/downloads-icon-15.png" height="103px"  style="width: 115%;">
              </div>
              <h4>Descargar Base de Datos</h4>
              <p style="margin-bottom: 50px;">Descargar el contenido que se encuentran en la base de datos de tu Software.</p>
              <div style="margin-bottom:-20px;" class="contenedor-redes-sociales">
              <a style="margin-bottom:1px;" class="facebook" href="<?php include('core/app/action/Backup_DataBase.php'); ?>" target="_blank" download>
                <span class="circulo"><i class="fa fa-download" aria-hidden="true"></i></span>
                <span class="titulo">Descargar</span>
                <span class="titulo-hover">Ahora</span>
              </a>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>
<br><br><br>
<div class="mazi-container">
        <div class="mazi-wrapper">
          <div class="sec-md-35">
            <div class="cards">
              <div class="card left-t">
                <h2>Eliminar Respaldos</h2>
                <p>Cada respaldo se almacena dentro de tu almacenamiento, si el un dia quieres eliminar ese almacenamiento y ves que no es necesario tener tantos respaldos alojados en tu almacenamiento, da click en el siguiente boton para eliminarlo y liberar espacio que quizas necesites.</p>
                <div style="margin-bottom:-20px;" class="contenedor-redes-sociales">
                <a style="margin-bottom:1px; margin-left: 114px; margin-top: 13px; color:rgb(159, 55, 228); border-color: rgb(159, 55, 228)" class="facebook2" href="index.php?action=eliminar_ficheros" >
                    <span  class="circulo"><i class="fa fa-hand-o-up" aria-hidden="true"></i></i></span>
                    <span style=""class="titulo">Eliminar</span>
                    <span class="titulo-hover">Archivos</span>
                </a>
                </div>
              </div>    
              <!--<div class="card left-b">
                <h2>Earn</h2>
                <p>The yield earned from the prize pool is distribured to lottery winners. Lottery winners are picked using chainlink VRF to ensure fair lottery and randomness.</p>
              </div>-->
            </div>
          </div>
          <div class="sec-md-30">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="408.598" height="513.23" viewBox="0 0 408.598 513.23">
                <defs>
                  <clipPath id="clip-path">
                    <rect id="Rectangle_30" data-name="Rectangle 30" width="108" height="108" transform="translate(73 523)" fill="#8247e5"/>
                  </clipPath>
                </defs>
                <g id="how-it-works-bg" transform="translate(-750 -2540)">
                  <g id="logo-icon" transform="translate(833 2017)" clip-path="url(#clip-path)">
                    <g id="Group_36" data-name="Group 36" transform="translate(83.336 523)">
                      <path id="Path_106" data-name="Path 106" d="M1079.107,592.009V649.6l20.242,20.242V611.585Z" transform="translate(-1079.107 -592.009)" fill="#8247e5"/>
                      <path id="Path_107" data-name="Path 107" d="M1099.349,592.009V649.6l-20.242,20.242V611.585Z" transform="translate(-1012.072 -592.009)" fill="#8247e5"/>
                      <path id="Path_108" data-name="Path 108" d="M1079.107,592.009v87.758l20.242,20.242V611.585Z" transform="translate(-1045.59 -592.009)" fill="#8247e5"/>
                    </g>
                  </g>
                  <g class="all-path">
                    <g class="no1">
                      <path id="Path_109" data-name="Path 109" d="M925.135,2566.549v209.384H754.115" transform="translate(0 35)" fill="none" stroke="#8247e5" stroke-width="2"/>
                      <circle id="Ellipse_12" data-name="Ellipse 12" cx="4.5" cy="4.5" r="4.5" transform="translate(750 2806)" fill="#8247e5"/>
                    </g>
                    <g class="no2">
                      <path id="Path_309" data-name="Path 309" d="M766.98,2566.549v209.384H926.6" transform="translate(227.885 32.966)" fill="none" stroke="#8247e5" stroke-width="2"/>
                      <circle id="Ellipse_18" data-name="Ellipse 18" cx="4.5" cy="4.5" r="4.5" transform="translate(1149.598 2803.966)" fill="#8247e5"/>
                    </g>
                    <!--<g class="no3">
                      <path id="Path_110" data-name="Path 110" d="M963.781,2600.978v394.874H924.015" transform="translate(-14 52.878)" fill="none" stroke="#8247e5" stroke-width="2"/>
                      <circle id="Ellipse_13" data-name="Ellipse 13" cx="4.5" cy="4.5" r="4.5" transform="translate(905.515 3044.23)" fill="#8247e5"/>
                    </g>
                    <g class="no4">
                      <path id="Path_308" data-name="Path 308" d="M924.015,2600.978v394.874h38.771" transform="translate(48.61 52.878)" fill="none" stroke="#8247e5" stroke-width="2"/>
                      <circle id="Ellipse_17" data-name="Ellipse 17" cx="4.5" cy="4.5" r="4.5" transform="translate(1006.895 3044.23)" fill="#8247e5"/>
                    </g>-->
                  </g>
                </g>
              </svg>      
          </div>
          <div class="sec-md-35">
            <div class="cards">
              <div class="card right-t">
                <h2>Limpiar Almacenamiento</h2>
                <p>Creamos una seccion la cual se basa en eliminar el contenido que se encuentra dentro de cada tabla dentro de la base datos.</p>
                <div style="margin-bottom:-20px;" class="contenedor-redes-sociales">
                <a  style="margin-bottom:1px; margin-left: 114px; margin-top: 13px;" class="facebook2" href="https://www.facebook.com/FormandoLaWeb" target="_blank">
                    <span class="circulo"><i class="fa fa-hand-o-up" aria-hidden="true"></i></span>
                    <span class="titulo">Limpiar</span>
                    <span class="titulo-hover">Almacenamiento</span>
                </a>
                </div>
              </div>
              <!--<div class="card right-b">
                <h2>Deposit</h2>
                <p>Remove your deposit at anytime. As long as you stay in the pools you continue to be eligible to win.</p>
              </div>-->
            </div>
          </div>
        </div>
      </div>

<br><br>

<style>
* {
    margin:0px;
    padding:0px;
    font-family: helvetica;
  }

p#texto{
	text-align: center;
	color:white;
}

div#div_file{

}

input#btn_enviar{
	position:absolute;
	top:0px;
	left:0px;
	right:0px;
	bottom:0px;
	width:100%;
	height:100%;
	opacity: 0;
}
</style>