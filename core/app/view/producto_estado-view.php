<?php require __DIR__ . '/../../../core/app/action/action-functions-products.php'; ?>

<head>
    <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
    <link rel="stylesheet" href="css/vista_productos/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>

<body id="minovate" class="appWrapper sidebar-sm-forced">
    <div class="row">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="index.php?view=cliebte"><i class="fa fa-home"></i> Inicio</a></li>
                <li><a href="#">Punto de venta</a></li>
                <li class="active">Productos</li>
            </ol>
        </section>
    </div>
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <section class="tile">

                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font"><strong>Productos Inactivos</strong></h1>
                    <ul class="controls">
                        <li class="remove">
                            <a href="index.php?view=productos" data-toggle="modal"> <span class="fa fa-user"></span> Activos</a>
                        </li>
                        <li class="dropdown">
                            <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
                            </a>
                            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                <li>
                                    <a role="button" tabindex="0" class="tile-toggle">
                                        <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                        <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-refresh">
                                        <i class="fa fa-refresh"></i> Refresh
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-fullscreen">
                                        <i class="fa fa-expand"></i> Fullscreen
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
                    </ul>
                </div>


                <div class="tile-body">
                    <div class="form-group">
                        <label for="filter" style="padding-top: 5px">Buscar:</label>
                        <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
                    </div>


                    <?php $productos = ProductoData::getAll();
                    if (count($productos) > 0) {
                        // si hay usuarios
                    ?>
                        <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">

                            <thead style="color: white; background-color: #827e7e;">
                                <th>CÓDIGO</th>
                                <th>NOMBRE</th>
                                <th>Activar</th>
                                <th>Eliminar Permanente</th>
                            </thead>
                            <?php foreach ($productos as $producto) :

                                if ($producto->estado == 1) {
                            ?>
                                    <tr>
                                        <td><?php echo $producto->codigo; ?></td>
                                        <td><?php echo $producto->nombre; ?></td>
                                        <td>
                                            <a onclick="activ(<?php echo $producto->id; ?>)" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-repeat"></i> Activar</a>
                                        </td>
                                        <td> <a onclick="del(<?php echo $producto->id; ?>)" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></td>
                                    </tr>
                            <?php }
                            endforeach; ?>
                            <tfoot class="hide-if-no-paging">
                                <tr>
                                    <td colspan="8" class="text-center">
                                        <ul class="pagination"></ul>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    <?php } else {
                        echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
                    };
                    ?>

                </div>
            </section>

        </div>
    </div>




    <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
    <script src="assets/js/vendor/footable/footable.all.min.js"></script>
    <script src="js/vista_productos/function.js"></script>


    <script>
        $(window).load(function() {

            $('.footable').footable();

        });
    </script>



    <script>
        function del($id) {
            Swal.fire({
                title: 'Estas seguro?',
                text: "",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar'
            }).then((result) => {
                if (result.isConfirmed) {
                    var parametros = {
                        "id": $id,
                        "id_estado": "desactivar"
                    };
                    $('#tipo_cliente').html("Por favor espera un momento");
                    $.ajax({
                        type: "POST",
                        url: 'index.php?action=actions_tipo_categoria_ventas',
                        data: parametros,
                        success: function(data) {
                            window.location.reload(); // Recargar página
                        }
                    });
                    //window.location="index.php?view=delete_documento&id="+$id
                }
            })
        }

        function del(id) {
            Swal.fire({
                title: 'Estas seguro?',
                text: "",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar'
            }).then((result) => {
                if (result.isConfirmed) {
                    var parametros = {
                        "id": id,
                        "estado": "eliminar"
                    };
                    $('#tipo_cliente').html("Por favor espera un momento");
                    $.ajax({
                        type: "POST",
                        url: 'index.php?action=actions_productos',
                        data: parametros,
                        success: function(data) {
                            window.location.reload(); // Recargar página
                        }
                    });
                }
            })
        }

        function activ(id) {
            Swal.fire({
                title: 'Estas seguro?',
                text: "",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                var parametros = {
                    "id": id,
                    "estado": "activar"
                };
                $('#tipo_cliente').html("Por favor espera un momento");
                $.ajax({
                    type: "POST",
                    url: 'index.php?action=actions_productos',
                    data: parametros,
                    success: function(data) {
                        window.location.reload(); // Recargar página
                    }
                });
            })
        }


        function valida(e) {
            tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            // Patron de entrada, en este caso solo acepta numeros
            patron = /[0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }
    </script>
    <style>

    </style>