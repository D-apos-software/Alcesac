<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<link rel="stylesheet" href="css/vista_cliente/style.css">
<?php
//Provincias
$json = file_get_contents("https://ubicaciones.paginasweb.cr/provincias.json");
$Provincias = get_object_vars(json_decode($json));

?>

<body id="minovate" class="appWrapper sidebar-sm-forced">
  <div class="row">
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="index.php?view=reserva"><i class="fa fa-home"></i> Inicio</a></li>
        <li class="active"><a href="#">Clientes</a></li>
      </ol>
    </section>
  </div>

  <!-- row -->
  <div class="row">
    <!-- col -->
    <div class="col-md-12">
      <section class="tile">
        <div class="tile-header dvd dvd-btm">
          <h1 class="custom-font"><strong>DIRECTORIO DEL</strong> CLIENTE</h1>
          <ul class="controls">
            <li class="remove">
              <a data-toggle="modal" data-target="#myModal"><i class="fa fa-user-plus"></i> NUEVO CLIENTE</a>
            </li>
            <li class="dropdown">
              <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
              </a>
              <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                <li>
                  <a role="button" tabindex="0" class="tile-toggle">
                    <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                    <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-refresh">
                    <i class="fa fa-refresh"></i> Refresh
                  </a>
                </li>
                <li>
                  <a role="button" tabindex="0" class="tile-fullscreen">
                    <i class="fa fa-expand"></i> Fullscreen
                  </a>
                </li>
              </ul>
            </li>
            <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <!-- tile body -->
        <div class="tile-body">
          <div class="form-group">
            <h5 for="filter" style="padding-top: 5px">Buscar:</h5>
            <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
          </div>
          <?php if (isset($_GET['buscar']) and $_GET['buscar'] != "") {
            $clientes = PersonaData::getLike($_GET['buscar']);
          } else {
            $clientes = PersonaData::getAll_clientes_tipo_documento();
          } ?>
          <?php
          if (count($clientes) > 0) {  ?>
            <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">
              <thead style="color: white; background-color: #827e7e;">
                <tr>
                  <th>Nº</th>
                  <th>Tipo documento</th>
                  <th data-hide="phone">ID</th>
                  <th data-hide='phone, tablet'>Nombre completo</th>
                  <th data-hide='phone, tablet'>Email</th>
                  <th data-hide='phone, tablet'>Teléfono</th>
                  <th data-hide='phone, tablet'># Reservaciones</th>
                  <th data-hide='phone, tablet'>Razón social</th>
                  <th data-hide='phone, tablet'>Dirección</th>
                  <th data-hide='phone, tablet'>Fecha nac.</th>
                  <!--
                  <th></th>
                -->
                  <th></th>

                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($clientes as $cliente) : ?>
                  <tr>
                    <td><?php echo $cliente->id; ?></td>
                    <td><b><?php echo $cliente->getTipoDocumento()->nombre; ?></b></td>
                    <td><?php echo $cliente->documento; ?></td>
                    <td><?php echo $cliente->nombre; ?></td>
                    <td><?php echo $cliente->email; ?></td>
                    <td><?php echo $cliente->telefono; ?></td>

                    <?php $reservas = PersonaData::get_cantidad_reservas($cliente->id); ?>
                    <?php if (isset($reservas)) {                        ?>
                      <td><?php echo $reservas->total; ?></td>
                    <?php } else { ?>
                      <td><?php echo 0; ?></td>
                    <?php } ?>
                    <td><?php if ($cliente->razon_social != NULL) {
                          echo $cliente->razon_social;
                        } else {
                          echo "--------";
                        } ?></td>
                    <td><?php if ($cliente->direccion != "NULL") {
                          echo $cliente->direccion;
                        } else {
                          echo "--------";
                        } ?></td>
                    <td><?php if ($cliente->fecha_nac != NULL and $cliente->fecha_nac != '0000-00-00') {
                          echo $cliente->fecha_nac;
                        } else {
                          echo "--------";
                        } ?></td>

                    <!--
                        <td>
                        <?php if ($cliente->vip == '0') { ?>
                        
                        <a href=""  data-toggle="modal" data-target="#myModalVip<?php echo $cliente->id; ?>"  class="btn btn-success btn-xs" style="background-color: #a928ea; border-color: #b80eca;"><i class="fa fa-card-o"></i> C. VIP</a>
                        <?php } else { ?>
                        <a href=""  data-toggle="modal" data-target="#myModalVip1<?php echo $cliente->id; ?>"  class="btn btn-success btn-xs"><i class="fa fa-card-o"></i> C. VIP</a>
                        <?php }; ?>
                        
                        </td>
                      -->
                    <td>
                      <a href="index.php?view=contacto&id=<?php echo $cliente->id; ?>" class="btn btn-info btn-xs"><i class="fa fa-users"></i> Contactos</a>
                    </td>
                    <td>
                      <a href="" data-toggle="modal" data-target="#myModal<?php echo $cliente->id; ?>" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                    </td>
                    <td>
                      <a onclick="del(<?php echo $cliente->id; ?>)" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                    </td>
                  </tr>

                  <div class="modal fade bs-example-modal-xm" id="myModalVip1<?php echo $cliente->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-warning">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <form class="form-horizontal" method="post" action="index.php?view=updatevip" role="form">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title"><span class="fa fa-users"></span> CLIENTE VIP</h4>
                            </div>
                            <div class="modal-body" style="background-color:#fff !important;">

                              <div class="row">
                                <div class="col-md-offset-1 col-md-10">



                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Puntos acumulados &nbsp;</span>
                                      <input type="text" class="form-control" disabled="" name="" value="<?php echo $cliente->contador; ?>">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Puntos alcanzar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                                      <input type="text" class="form-control" disabled="" name="" value="<?php echo $cliente->limite; ?>">
                                    </div>
                                  </div>



                                </div>
                              </div>

                            </div>
                            <div class="modal-footer">
                              <input type="hidden" name="id_cliente" value="<?php echo $cliente->id; ?>">
                              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>

                            </div>
                          </form>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </div>



                  <div class="modal fade bs-example-modal-xm" id="myModalVip<?php echo $cliente->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-warning">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <form class="form-horizontal" method="post" action="index.php?view=updatevip" role="form">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title"><span class="fa fa-users"></span> EDITAR CLIENTE</h4>
                            </div>
                            <div class="modal-body" style="background-color:#fff !important;">

                              <div class="row">
                                <div class="col-md-offset-1 col-md-10">



                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Cliente vip &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                                      <select class="form-control" name="vip">
                                        <option value="0" <?php if ($cliente->vip == '0') {
                                                            echo "selected";
                                                          }; ?>>Inactivo</option>
                                        <option value="1" <?php if ($cliente->vip == '1') {
                                                            echo "selected";
                                                          }; ?>>Activo</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Ptos. acumular </span>
                                      <input type="number" name="limite" class="form-control" placeholder="Ejem. 6">
                                    </div>
                                  </div>

                                </div>
                              </div>

                            </div>
                            <div class="modal-footer">
                              <input type="hidden" name="id_cliente" value="<?php echo $cliente->id; ?>">
                              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                              <button type="submit" class="btn btn-outline">Actualizar Datos</button>
                            </div>
                          </form>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </div>



                  <div class="modal fade bs-example-modal-xm" id="myModal<?php echo $cliente->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-warning">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=updatecliente" role="form">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title"><span class="fa fa-users"></span> EDITAR CLIENTE</h4>
                            </div>
                            <div class="modal-body" style="background-color:#fff !important;">

                              <div class="row">
                                <div class="col-md-offset-1 col-md-10">


                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Tipo de documento </span>
                                      <?php $tipo_documentos = TipoDocumentoData::getAll(); ?>
                                      <select name="tipo_documento" required class="form-control">
                                        <?php foreach ($tipo_documentos as $tipo_documento) : ?>
                                          <option value="<?php echo $tipo_documento->id; ?>" <?php if ($cliente->tipo_documento != null && $cliente->tipo_documento == $tipo_documento->id) {
                                                                                                echo "selected";
                                                                                              } ?>><?php echo $tipo_documento->nombre; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Nombre &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                      <input type="text" class="form-control col-md-8" value="<?php echo $cliente->nombre; ?>" name="nombre" required placeholder="Ingrese el nombre">

                                      <span class="input-group-addon"> ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                                      <input type="number" class="form-control col-md-8" value="<?php echo $cliente->documento; ?>" name="documento" min="0" max="9999999999" required placeholder="Ingrese el ID del documento">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Email &nbsp;&nbsp;&nbsp;</span>
                                      <input style="margin-right:140px!important;" type="email" class="form-control col-md-8" value="<?php echo $cliente->email; ?>" name="email" required placeholder="Ingrese el email">
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Teléfono &nbsp;&nbsp;</span>
                                      <input type="tel" class="form-control col-md-8" value="<?php echo $cliente->telefono; ?>" name="telefono" pattern="[0-9]{8}" required placeholder="Ingrese el teléfono">

                                      <span class="input-group-addon"> Teléfono Sec &nbsp;&nbsp;</span>
                                      <input type="tel" class="form-control col-md-8" value="<?php echo $cliente->telefono_sec; ?>" name="telefono_sec" pattern="[0-9]{8}" required placeholder="Secundario">
                                    </div>
                                  </div>



                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Razón social &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                      <input type="text" class="form-control col-md-8" value="<?php echo $cliente->razon_social; ?>" name="razon_social" placeholder="Ingrese Razón social (OPCIONAL)">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Giro &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                      <input type="text" class="form-control col-md-8" value="<?php echo $cliente->giro; ?>" name="giro" placeholder="Ingrese Giro (OPCIONAL)">
                                    </div>
                                  </div>
                                  <?php if ($cliente->provincia != 0 and $cliente->canton != 0 and $cliente->distrito != 0) { ?>
                                    <div class="form-group">
                                      <div class="input-group">
                                        <span class="input-group-addon"> Provincia </span>
                                        <select name="provincia" id="provincia" required class="form-control" onchange="CargarCantonM(this.value,<?php echo $cliente->id ?>,<?php echo $cliente->id ?>);" onclick="ActualizarData(<?php echo $cliente->id ?>,1)">
                                          <?php foreach ($Provincias as  $key => $value) : ?>
                                            <option value="<?php echo ($key); ?>" <?php if ($cliente->provincia == $key) {
                                                                                    echo "selected";
                                                                                  } ?>><?php echo $value; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                        <span class="input-group-addon" style="display:none;" id="canton_vm<?php echo $cliente->id ?>"> Canton </span>
                                        <div id="id_cantonm<?php echo $cliente->id ?>"></div>
                                        <?php

                                        //Canton
                                        $jsonc = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $cliente->provincia . "/cantones.json");
                                        $Cantones = get_object_vars(json_decode($jsonc));
                                        ?>
                                        <span class="input-group-addon" id="title_c<?php echo $cliente->id ?>"> Canton </span>
                                        <select name="canton" id="canton<?php echo $cliente->id ?>" required class="form-control" onclick="ActualizarData(<?php echo $cliente->id ?>,2)">
                                          <?php foreach ($Cantones as $key => $tipo_documento) : ?>
                                            <option value="<?php echo ($key); ?>" <?php if ($cliente->canton == $key) {
                                                                                    echo "selected";
                                                                                  } ?>><?php echo ($tipo_documento); ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                      </div>
                                      <!-- /.input group -->
                                    </div>
                                    <?php
                                    //Distrito
                                    $jsond = file_get_contents("https://ubicaciones.paginasweb.cr/provincia/" . $cliente->provincia . "/canton/" . $cliente->canton . "/distritos.json");
                                    $Distritos = get_object_vars(json_decode($jsond));
                                    ?>
                                    <div class="form-group">
                                      <div class="input-group">
                                        <span class="input-group-addon" id="title_d<?php echo $cliente->id ?>"> Distrito </span>
                                        <select name="distrito" id="distrito<?php echo $cliente->id ?>" required class="form-control">
                                          <?php foreach ($Distritos as $key => $tipo_documento) : ?>
                                            <option value="<?php echo $key; ?>" <?php if ($cliente->distrito == $key) {
                                                                                  echo "selected";
                                                                                } ?>><?php echo ($tipo_documento); ?></option>
                                          <?php endforeach; ?>
                                        </select>

                                        
                                        <span class="input-group-addon" style="display:none;" id="distrito_vm<?php echo $cliente->id ?>"> Distrito </span>
                                        <div id="id_distritom<?php echo $cliente->id ?>"></div>
                                        <div class="input-group-addon">
                                          Barrio
                                        </div>
                                        <input type="text" class="form-control" placeholder="Barrio (OPCIONAL)" name="barrio" id="barrio" value="<?php echo $cliente->barrio; ?>">
                                      </div>
                                      <!-- /.input group -->
                                    </div>
                                  <?php } else { ?>
                                    <div class="form-group">
                                      <div class="input-group">
                                        <span class="input-group-addon"> Provincia </span>
                                        <select name="provincia" id="provincia" required class="form-control" onchange="CargarCantonM(this.value,<?php echo $cliente->id ?>,<?php echo $cliente->id ?>);" onclick="ActualizarData(<?php echo $cliente->id ?>,1)">
                                          <?php foreach ($Provincias as  $key => $value) : ?>
                                            <option value="<?php echo ($key); ?>"><?php echo $value; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                        <span class="input-group-addon" style="display:none;" id="canton_vm<?php echo $cliente->id ?>"> Canton </span>
                                        <div id="id_cantonm<?php echo $cliente->id ?>"></div>
                                      

                                        <input type="hidden" name="cantonMD" id="cantonMD" value="1" />
                                        <input type="hidden" name="distritoMD" id="distritoMD" value="1" />


                                      </div>
                                      <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                      <div class="input-group">
                                        <span class="input-group-addon" style="display:none;" id="distrito_vm<?php echo $cliente->id ?>"> Distrito </span>
                                        <div id="id_distritom<?php echo $cliente->id ?>"></div>
                                        <div class="input-group-addon">
                                          Barrio
                                        </div>
                                        <input type="text" class="form-control" placeholder="Barrio (OPCIONAL)" name="barrio" id="barrio" value="<?php echo $cliente->barrio; ?>">
                                      </div>
                                      <!-- /.input group -->
                                    </div>



                                  <?php } ?>


                                  <div class="form-group">
                                    <div class="input-group">
                                      <textarea style="width:553px; height:45px;" cols="30" rows="10" class="form-control col-md-8" name="direccion" placeholder="Detalles de la dirección"><?php echo $cliente->direccion; ?></textarea>
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"> Fecha nac&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                                      <input type="date" name="fecha_nac" class="form-control" value="<?php echo $cliente->fecha_nac; ?>">

                                      <span class="input-group-addon"> Exonerado </span>
                                      <div class="form-control">
                                        <input type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1" style="height: 26px; margin:-2px;" <?php if ($cliente->exonerado) echo "checked"  ?>>
                                      </div>
                                    </div>
                                  </div>




                                  <?php $estado_f = get_estado_function(3);
                                  if ($estado_f->estado != 0) {
                                  ?>
                                    <div class="form-group">
                                      <div class="input-group">
                                        <span class="input-group-addon"> Categoria </span>
                                        <?php $datos_cat = Categoria_Clientes_P::getActivos(); ?>
                                        <select class="form-control" name="id_categoria_p" id="id_categoria_p">
                                          <option value="0" <?php if ($cliente->id_categoria_p == 0) {
                                                              echo "selected";
                                                            } ?>>Seleccionar (Opcional)</option>
                                          <?php foreach ($datos_cat as $dt) : ?>
                                            <option value="<?php echo $dt->id; ?>" <?php if ($cliente->id_categoria_p == $dt->id) {
                                                                                      echo "selected";
                                                                                    } ?>><?php echo $dt->nombre; ?></option>
                                          <?php endforeach; ?>
                                        </select>
                                      </div>
                                    </div>
                                  <?php } ?>
                                </div>
                              </div>

                            </div>
                            <div class="modal-footer">
                              <input type="hidden" name="id_cliente" value="<?php echo $cliente->id; ?>">
                              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                              <button type="submit" class="btn btn-outline">Actualizar Datos</button>
                            </div>
                          </form>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  </div>

                <?php endforeach; ?>
              </tbody>
              <tfoot class="hide-if-no-paging" style="left: -20px;">
                <tr>
                  <td colspan="9" class="text-center">
                    <ul class="pagination"></ul>
                  </td>
                </tr>
              </tfoot>
            </table>

          <?php } else {
            echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
          };
          ?>



        </div>
        <!-- /tile body -->

      </section>
      <!-- /tile -->

    </div>
    <!-- /col -->
  </div>
  <!-- /row -->






  <div class="modal fade bs-example-modal-xm" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-success">
      <div class="modal-dialog">
        <div class="modal-content">
          <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=addclient" role="form">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><span class="fa fa-users">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> INGRESAR NUEVO CLIENTE</h4>
            </div>
            <div class="modal-body" style="background-color:#fff !important;">

              <div class="row">
                <div class="col-md-offset-1 col-md-10">


                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Tipo de documento </span>
                      <?php $tipo_documentos = TipoDocumentoData::getAll(); ?>
                      <select name="tipo_documento" required class="form-control">
                        <?php foreach ($tipo_documentos as $tipo_documento) : ?>
                          <option value="<?php echo $tipo_documento->id; ?>"><?php echo $tipo_documento->nombre; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Nombres </span>
                      <input type="text" class="form-control col-md-8" name="nombre" id="nombre" required placeholder="Ingrese nombre">

                      <span class="input-group-addon"> ID </span>
                      <input type="number" class="form-control col-md-8" name="documento" id="documento" required placeholder="Ingrese el ID del documento">

                    </div>
                  </div>



                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        E-mail
                      </div>
                      <input type="email" class="form-control" name="email" id="email" placeholder="Ingrese E-mail (OPCIONAL)">
                    </div>
                    <!-- /.input group -->
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        Teléfono
                      </div>
                      <input type="tel" class="form-control" placeholder="Teléfono (OPCIONAL)" name="telefono" id="telefono" pattern="[0-9]{8}" value="">

                      <div class="input-group-addon">
                        Teléfono Sec
                      </div>
                      <input type="tel" class="form-control" placeholder="Secundario (OPCIONAL)" name="telefono_sec" id="telefono_sec" pattern="[0-9]{8}" value="">
                    </div>
                  </div>



                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Razón social </span>
                      <input type="text" class="form-control col-md-8" name="razon_social" placeholder="Ingrese Razón social (OPCIONAL)">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Giro </span>
                      <input type="text" class="form-control col-md-8" name="giro" placeholder="Ingrese Giro (OPCIONAL)">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Provincia </span>
                      <select name="provincia" id="provincia" required class="form-control" onchange="CargarCanton(this.value);">
                        <?php foreach ($Provincias as  $key => $value) : ?>
                          <option id="st" value="<?php echo ($key); ?>"><?php echo $value; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <span class="input-group-addon" id="canton_v"> Canton </span>
                      <div id="id_canton"></div>

                    </div>
                    <!-- /.input group -->
                  </div>

                  <div class="form-group" id="form_ub2">
                    <div class="input-group">
                      <span class="input-group-addon" id="distrito_v"> Distrito </span>
                      <div id="id_distrito"></div>
                      <div class="input-group-addon">
                        Barrio
                      </div>
                      <input type="text" class="form-control" placeholder="Barrio (OPCIONAL)" name="barrio" id="barrio" value="">
                    </div>
                    <!-- /.input group -->
                  </div>


                  <div class="form-group">
                    <div class="input-group">
                      <textarea style="width:585px; height:49px;" cols="30" rows="10" class="form-control col-md-8" name="direccion" placeholder="Dirección Exacta (OPCIONAL)"></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"> Fecha nac </span>
                      <input type="date" name="fecha_nac" class="form-control">
                      <span class="input-group-addon"> Exonerado </span>
                      <div class="form-control">
                        <input type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1">
                      </div>
                    </div>
                  </div>
                  <?php
                  $estado_f = get_estado_function(3);
                  if ($estado_f->estado != 0) {
                  ?>
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon"> Categoria </span>
                        <?php $datos_cat = Categoria_Clientes_P::getActivos(); ?>
                        <select class="form-control" name="id_categoria_p" id="id_categoria_p">
                          <option value="0">Seleccionar (Opcional)</option>
                          <?php foreach ($datos_cat as $dt) : ?>
                            <option value="<?php echo $dt->id; ?>"><?php echo $dt->nombre; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  <?php
                  }
                  ?>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
              <button type="submit" name="agregar" class="btn btn-outline">Agregar Datos</button>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>
  <style>

  </style>
  <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
  <script src="assets/js/vendor/footable/footable.all.min.js"></script>
  <script>
    $(window).load(function() {

      $('.footable').footable();

    });
  </script>
  <script src="assets/js/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="assets/js/vendor/jRespond/jRespond.min.js"></script>
  <script src="assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>
  <script src="assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
  <script src="assets/js/vendor/screenfull/screenfull.min.js"></script>
  <script src="assets/js/vendor/footable/footable.all.min.js"></script>
  <!--<script src="assets/js/main.js"></script>-->
  <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
  <script src="js/vista_clientes/script.js"></script>