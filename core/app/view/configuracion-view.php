  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vista_configuracion/style.css">
    <link rel="stylesheet" type="text/css" href="css/vista_configuracion/util.css">
    <link rel="stylesheet" type="text/css" href="css/vista_configuracion/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="script.js"></script>
  </head>

  <body>




    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section style="margin-top:50px;margin-bottom:-20px; margin-left:20px;" class="content-header">
        <h1>
          <small>Configuraciónes Importante</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
          <li class="active">Configuración</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-3">
            <div class="box box-solid">
              <div class="box-body no-padding">
                <!-- <ul class="nav nav-tabs nav-stacked">
                  <li class="active"><a href="#tab_1" data-toggle="tab" class="btn-primary">PRINCIPAL</a></li>
                  <li><a href="#tab_3" data-toggle="tab" class="btn-primary"> <i class="fa fa-filter"></i>OTROS</a></li>
                </ul>  -->
                <ul class="form_submenuV">
                  <li><a class="profile" href="#tab_1" data-toggle="tab"><i class="icon-user"></i>Empresa</a></li>
                  <?php
                  $Fact_estado = Funcionalidad::getById(4);
                  if ($Fact_estado->estado == 1) { ?>
                    <li><a class="messages" href="#tab_2" data-toggle="tab"><i class="icon-envelope-alt"></i>Facturacion Electrónica</a></li>
                  <?php } ?>
                  <li><a class="settings" href="#" data-toggle="tab"><i class="icon-cog"></i>Base de datos</a></li>
                  <li><a class="logout" href="#tab_3" data-toggle="tab"><i class="icon-signout"></i>Otros</a></li>
                </ul>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /. box -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">

              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="contact100-map" id="google_map" data-map-x="40.722047" data-map-y="-73.986422" data-pin="images/icons/map-marker.png" data-scrollwhell="0" data-draggable="1"></div>

                  <div class="wrap-contact100">

                    <?php
                    $configuracion = ConfiguracionData::getAllConfiguracion();
                    //Convertimos el objeto en array con el fin de que podamos realizar dicha condicion
                    $object = (array) $configuracion;
                    if (count($object) > 0) {
                      $nombre = $configuracion->nombre;
                      $direccion = $configuracion->direccion;
                      $estado = $configuracion->estado;
                      $telefono = $configuracion->telefono;
                      $fax = $configuracion->fax;
                      $rnc = $configuracion->rnc;
                      $registro_empresarial = $configuracion->registro_empresarial;
                      $ciudad = $configuracion->ciudad;

                      $id = $configuracion->id; ?>

                      <form class="contact100-form validate-form flex-sb flex-w" method="post" id="addproduct" action="index.php?view=updateconfiguracion" role="form">

                      <?php } else {
                      $nombre = '';
                      $direccion = '';
                      $estado = '';
                      $telefono = '';
                      $fax = '';
                      $rnc = '';
                      $registro_empresarial = '';
                      $ciudad = '';
                      $id = 0; ?>
                        <form class="contact100-form validate-form flex-sb flex-w" method="post" id="addproduct" action="index.php?view=addconfiguracion">
                        <?php }
                        ?>
                        <span class="contact100-form-title">
                          Información de la Empresa
                        </span>

                        <div class="wrap-input100 rs1 validate-input" data-validate="Name is required">
                          <input class="input100" type="text" name="nombre" placeholder="Nombre empresa" value="<?php if ($nombre != 'NULL') {
                                                                                                                  echo $nombre;
                                                                                                                } ?>">
                          <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 rs1 validate-input">
                          <input class="input100" type="text" name="direccion" placeholder="Dirección" value="<?php if ($direccion != 'NULL') {
                                                                                                                echo $direccion;
                                                                                                              } ?>">
                          <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 rs1 validate-input">
                          <input class="input100" type="text" name="estado" placeholder="Estado o provincia" value="<?php if ($estado != 'NULL') {
                                                                                                                      echo $estado;
                                                                                                                    } ?>">
                          <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 rs1 validate-input">
                          <input class="input100" type="text" name="telefono" placeholder="Teléfono" value="<?php if ($telefono != 'NULL') {
                                                                                                              echo $telefono;
                                                                                                            } ?>">
                          <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 rs1 validate-input">
                          <input class="input100" type="text" name="rnc" placeholder="RUC" value="<?php if ($rnc != 'NULL') {
                                                                                                    echo $rnc;
                                                                                                  } ?>">
                          <span class="focus-input100"></span>
                        </div>


                        <div class="wrap-input100 rs1 validate-input">
                          <input class="input100" type="text" name="ciudad" placeholder="Ciudad" value="<?php if ($ciudad != 'NULL') {
                                                                                                          echo $ciudad;
                                                                                                        } ?>">
                          <span class="focus-input100"></span>
                        </div>

                        <div class="container-contact100-form-btn">
                          <input type="hidden" name="id_configuracion" value="<?php echo $id; ?>">
                          <button type="submit" class="contact100-form-btn">
                            Actualizar
                          </button>
                        </div>
                        </form>
                  </div>
                </div>

                <script>
                  window.dataLayer = window.dataLayer || [];

                  function gtag() {
                    dataLayer.push(arguments);
                  }
                  gtag('js', new Date());

                  gtag('config', 'UA-23581568-13');
                </script>

                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                  <div class="box box-primary">
                    <?php
                    $config = ConfiguracionData::getAllConfiguracion();
                    $object_logo = (array) $config;
                    if (count($object_logo) > 0) {
                      $logo = $config->logo;
                      $id_config = $config->id; ?>


                      <!-- /.box-header -->
                      <form class="form-horizontal" method="post" id="addproduct" action="index.php?view=updatelogo" enctype="multipart/form-data" role="form">
                        <div class="box-body no-padding">



                          <div class="row">
                            <div class="col-md-offset-1 col-md-10">

                              <div class="form-group">
                                <label>Logo</label>
                                <div class="input-group">
                                  <span class="input-group-addon">Subir imagen </span>
                                  <input type="file" class="form-control col-md-12" name="logo">
                                </div>
                              </div>

                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-offset-1 col-md-10">

                              <?php if ($logo != "" and $logo != "NULL" and $logo != NULL) : ?>
                                <img src="img/<?php echo $configuracion->logo; ?>" style="width:74px;">
                              <?php endif; ?>

                            </div>
                          </div>


                          <div class="box-footer" style="margin:80px">

                            <input type="hidden" name="id_config" value="<?php echo $id_config; ?>">
                            <button type="submit" class="btn btn-primary">Actualizar logo</button>

                          </div>


                        </div>
                        <!-- /.box-body -->
                      </form>
                    <?php } ?>

                  </div>



                </div>
                <!-- /.tab-pane -->


                <!-- ----------------------------------------------------------- TAB FACTURA ELECTRONICA ---------------------------------------------------------------------- -->

                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                  <div class="box box-primary">

                    <!-- /.box-header -->
                    <div class="contact100-map" id="google_map" data-map-x="40.722047" data-map-y="-73.986422" data-pin="images/icons/map-marker.png" data-scrollwhell="0" data-draggable="1"></div>
                    <div class="wrap-contact100">
                      <?php
                      $config = ConfiguracionData::getAllConfiguracion();
                      $object_logo = (array) $config;
                      if (count($object_logo) > 0) {
                        $logo = $config->logo;
                        $id_config = $config->id;
                        
                        $usuario_hacienda = $config->usuario_hacienda;
                        $contrasena_hacienda = $config->contrasena_hacienda;
                        $llave_criptografica = $config->llave_criptografica;
                        $pin = $config->pin;
                        $cod_moneda =  $config->cod_moneda;
                        $nombre_hac =  $config->nombre_hac;
                        $numero_id =  $config->numero_id;
                        $provincia =  $config->provincia;
                        $canton =  $config->canton;
                        $distrito =  $config->distrito;
                        $barrio =  $config->barrio;
                        $cod_pais_tel_hc =  $config->cod_pais_tel_hc;
                        $tel_hac =  $config->tel_hac;

                        
                      ?>

                        <div class="contact100-form validate-form flex-sb flex-w">


                          <span class="contact100-form-title">
                            Datos para Facturacion Electrónica
                          </span>

                          <div class="wrap-input100 rs1 validate-input" data-validate="Name is required">
                            <input class="input100" type="text" id="usuario_hacienda" name="usuario_hacienda" placeholder="Usuario Hacienda" value="<?php if ($usuario_hacienda != 'NULL') {
                                                                                                                                                      echo $usuario_hacienda;
                                                                                                                                                    } ?>" required>
                            <span class="focus-input100"></span>
                          </div>

                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="password" name="contrasena_hacienda" id="contrasena_hacienda" placeholder="Contraseña" value="<?php if ($contrasena_hacienda != 'NULL') {
                                                                                                                                                          echo $contrasena_hacienda;
                                                                                                                                                        } ?>" required>
                            <span class="focus-input100"></span>
                          </div>

                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="file" name="llave_criptografica" id="llave_criptografica" placeholder="Llave Criptografica" onchange="file(event)" accept=".p12">
                            <span class="focus-input100"></span>
                          </div>

                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="password" name="pin" id="pin" placeholder="PIN" value="<?php if ($pin != 'NULL') {
                                                                                                                    echo $pin;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>



                          <div class="wrap-input100  validate-input">
                            <select class="input100" name="cod_moneda" id="cod_moneda">
                              <option value="CRC" selected>CRC</option>
                            </select>
                            <span class="focus-input100"></span>
                          </div>



                          
                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="text" name="nombre_hac" id="nombre_hac" placeholder="Nombre Hacienda" value="<?php if ($nombre_hac != 'NULL') {
                                                                                                                    echo $nombre_hac;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>


                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="number" name="numero_id" id="numero_id" placeholder="Identificación" value="<?php if ($numero_id != 'NULL') {
                                                                                                                    echo $numero_id;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>


                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="text" name="provincia" id="provincia" placeholder="Provincia" value="<?php if ($provincia != 'NULL') {
                                                                                                                    echo $provincia;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>


                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="text" name="canton" id="canton" placeholder="Cantón" value="<?php if ($canton != 'NULL') {
                                                                                                                    echo $canton;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>

                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="text" name="distrito" id="distrito" placeholder="Distrito" value="<?php if ($distrito != 'NULL') {
                                                                                                                    echo $distrito;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>

                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="text" name="barrio" id="barrio" placeholder="Barrio" value="<?php if ($barrio != 'NULL') {
                                                                                                                    echo $barrio;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>

                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="text" name="cod_pais_tel_hc" id="cod_pais_tel_hc" placeholder="Código de Teléfono" value="<?php if ($cod_pais_tel_hc != 'NULL') {
                                                                                                                    echo $cod_pais_tel_hc;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>

                          <div class="wrap-input100 rs1 validate-input">
                            <input class="input100" type="number" name="tel_hac" id="tel_hac" placeholder="Teléfono" value="<?php if ($tel_hac != 'NULL') {
                                                                                                                    echo $tel_hac;
                                                                                                                  } ?>" required>
                            <span class="focus-input100"></span>
                          </div>

                          <input type="hidden" name="tipo_id" id="tipo_id" value="0">


                          <div class="container-contact100-form-btn">
                            <input type="hidden" id="id_config" name="id_config" value="<?php echo $id_config; ?>">
                            <button name="submit_fac" id="submit_fac" class="contact100-form-btn">
                              Actualizar
                            </button>
                          </div>
                        </div>
                      <?php } ?>
                    </div>

                    <script>
                      window.dataLayer = window.dataLayer || [];

                      function gtag() {
                        dataLayer.push(arguments);
                      }
                      gtag('js', new Date());

                      gtag('config', 'UA-23581568-13');
                    </script>
                  </div>



                </div>
                <!------------------------------------------------------------- FIN TAB FACTURA ELECTRONICA ---------------------------------------------------------------------- -->
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>

  </body>

  </html>

  <script src="js/vista_configuraciones/script.js"></script>