
<?php 
    date_default_timezone_set('America/Costa_Rica');
    $hoy = date("Y-m-d");
    $hora = date("H:i:s"); 
    $anio= date("Y");   
    $inicio=date("Y-01-01"); 
    $fin=date("Y-12-31") ;    

    $primer_dia = date("01-m-d");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vista_reportes/reportes_graficos_chartjs.scss">
    <!-- Favicon -->
    <link rel="stylesheet" href="css/vista_principal/vendor/nucleo/css/nucleo.css" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="css/vista_principal/css/argon.css?v=1.2.0" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>
    <script src="js/jquery-2.2.3.min.js"></script>

</head>
<body>
    <br>
    <br>
    <br>
    <div style="margin-left:1000px; margin-right:45px;" class="form-group">
                <div class="row">
                  <div class="col-sm-12">
                  <label style="font-size: 14px;">Categoria de gráficos</label>
                    <select style="font-size: 14px;" class="form-control select2" required  name="tipo_id_cliente" onchange="Cargar(this.value);">  
                      <option value="1" >Clientes</option>
                      <option value="2" >Productos</option>
                      <option value="3" >Cajas</option>
                    </select>
                  </div>
                </div>
     </div>
    <div id="padre">
        <div id="graficos_resultado">
        <!-- Inicio de los graficos -->
        


<div class="col-xl-12">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 style="font-size: 14px;" class="mb-0">Datos generales de Clientes</h3>
                </div>
                <div class="col text-right">
                  <a style="font-size: 12px;" href="#" class="btn btn-sm btn-primary">Seleccionar</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <!-- INICIA FOREACH -->
            <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" style="font-size: 11px;">
                <thead class="thead-light">
                  <tr>
                    <th style="font-size: 12px;" scope="col">Cliente</th>
                    <th style="font-size: 12px;" scope="col">Tipo de Identificación</th>
                    <th style="font-size: 12px;" scope="col">ID</th>
                    <th style="font-size: 12px;" scope="col"># Hospedajes</th>
                    <th style="font-size: 12px;" scope="col">Ganancia por Hospedaje</th>
                    <th style="font-size: 12px;" scope="col">Ganancia por Productos</th>
                    <th style="font-size: 12px;" scope="col">Email</th>
                    <th style="font-size: 12px;" scope="col">Teléfono</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $Datos_usuario = PersonaData::get_datos_generales();
                  foreach($Datos_usuario as $Datos_usuarios):
                  ?>
                <tr>
                  <td style="font-size: 12px;"><?php echo $Datos_usuarios->cliente ?></td>
                  <td style="font-size: 12px;"><?php echo $Datos_usuarios->documento ?></td>
                  <td style="font-size: 12px;"><?php echo $Datos_usuarios->id ?></td>
                  <td style="font-size: 12px;"><?php echo $Datos_usuarios->catidad_hospedajes ?></td>
                  <td style="font-size: 12px;"><?php echo $Datos_usuarios->ganancia_recepcion ." ₡"; ?></td>
                  <td style="font-size: 12px;">
                  <?php if($Datos_usuarios->ganancia_ventas != null) {
                    echo $Datos_usuarios->ganancia_ventas." ₡";
                   }else{
                    echo "0 ₡";
                   } 
                   ?></td>
                  <td style="font-size: 12px;"><?php echo $Datos_usuarios->email ?></td>
                  <td style="font-size: 12px;"><?php echo $Datos_usuarios->telefono ?></td>


                </tr>
                  <?php endforeach; ?>
              </tbody>
              <tfoot class="hide-if-no-paging" >
                        <tr>
                          <td colspan="9" class="text-center">
                            <ul class="pagination"></ul>
                          </td>
                        </tr>
                      </tfoot>
              </table>
            </div>
          </div>
        </div>



        <div class="row">
        <div class="col-xl-7.2">
          <div style="margin-left:25px; box-shadow: 0 1px 10px rgb(80 93 80); font-size: 14px;" class="card">
           <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Crecimiento de clientes</h3>
                  <div name="container">
                  <select name="select_gc" id="select_gc" style="font-size: 14px; width: 24%;" class="form-control select2" >  
                      <option value="2" >Cantidad</option>
                      <option value="1" >Ganancia</option>
                    </select>
                  <select name="fecha_gc" id="fecha_gc" style="font-size: 14px; width: 24%;  margin-left: 227px; margin-top: -38px;" class="form-control select2" >  
                      <option value="1" >Día</option>
                      <option value="2" >Mes</option>
                      <option value="3" >Año</option>
                    </select>
                  </div>

                      <div class="tile-body  col-xl-4" style="position:relative; left: 550px;  margin-top:-42px;">

                                <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" id="start_gc" name="start_gc" value="<?php echo date("Y-m-01");    ?>">

                                <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" id="end_gc" name="end_gc" value="<?php echo $hoy; ?>">
                            </div>
                            <!-- /tile body -->
                            <div class="tile-footer">
                                <div class="form-group text-center">
                                    <button  style="margin-top:15px; font-size: 14px; visibility:hidden; position:relative; left: 340px;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                </div>
                            </div>
                          <div style="margin-bottom:-20px;" id="buyersPadre" class="chart">
                            <canvas id="buyers" width="800" height="330"></canvas>
                          </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div style="margin-right:-75px;
          box-shadow: 0 1px 10px rgb(80 93 80);" class="card">
            <div  class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Variantes</h3>
                <br>
                <select name="select_variantes" id="select_variantes" style="font-size: 14px; width: 34%;" class="form-control select2" required  >  
                      <option value="1" >Medio de pago</option>
                      <option value="2" >Estado de pago</option>
                      <option value="3" >Tipo de Documento</option>
                      <option value="4" >Tipo de Comprobante</option>
                </select>
                      <div class="tile-body col-xl-5" style="position:relative; left: 280px; margin-top:-55px;">

                                <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" id="start_var" name="start_var" value="<?php echo date("Y-m-01");  ?>">

                                <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" id="end_var"name="end_var" value="<?php echo $hoy; ?>">
                            </div>
                            <!-- /tile body -->
                            <div class="tile-footer">
                                <div class="form-group text-center">
                                    <button style="margin-top:15px; font-size: 14px;  position:relative; left: 170px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                </div>
                            </div>
                            <div style="margin-top:-40px;" id="oilChartPadre" name="oilChartPadre">
                            <canvas id="oilChart"  name="oilChart" width="530" height="400"></canvas>          
                            </div>    
                          </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-12">
          <div style="margin-left:10px; margin-right:20px; box-shadow: 0 1px 10px rgb(80 93 80); font-size: 14px;" class="card">
           <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Ganancias por productos</h3>
                  <br>
                  <?php 
                  $productos = ProductoData::getAll();
                  ?>
                  <select id="select_producto" name="select_producto" style="font-size: 14px; width: 24%;" class="form-control select2"  required>  
                  <option value="1" >Todos</option>
                  <?php foreach($productos as $producto):?> 
                      <option value="<?php echo $producto->id ?>" ><?php echo $producto->nombre ?></option>
                  <?php endforeach; ?>
                  </select>
                 
                      <div class="tile-body  col-xl-3" style="position:relative; left:970px; margin-top:-50px;">

                                <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" name="start_prod" id="start_prod" value="<?php echo date("Y-m-01"); ?>">

                                <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" name="end_prod" id="end_prod" value="<?php echo $hoy; ?>">
                            </div>
                            <!-- /tile body -->
                            <div class="tile-footer">
                                <div class="form-group text-center">
                                    <button style="margin-top:15px; font-size: 14px;  position:relative; left: 567px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                </div>
                            </div>
                          <div id="incomePadre">
                          <canvas id="income" width="1200" height="300"></canvas>
                          </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        </div>
    </div> 
    <div class="col-xl-12">
          <div style="margin-left:-5px; margin-right:5px; box-shadow: 0 1px 10px rgb(80 93 80); font-size: 14px;" class="card">
           <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Ganancias por productos</h3>
                  <br>
                  <select id="select_tipo" name="select_tipo" style="font-size: 14px; width: 24%;" class="form-control select2"  required>  
                  <option value="1" >Todos</option>
                      <option value="1" >Ganancias</option>
                      <option value="2" >Cantidad</option>
                  </select>
                  <select id="producto1" name="producto1" style="font-size: 14px; width: 15%; margin-left: 367px; margin-top: -38px;" class="form-control select2"  required>  
                  <?php foreach($productos as $producto):?> 
                      <option value="<?php echo $producto->id ?>" ><?php echo $producto->nombre ?></option>
                  <?php endforeach; ?>
                  </select>
                  <select id="producto2" name="producto2" style="font-size: 14px; width: 15%; margin-left: 597px; margin-top: -38px;" class="form-control select2"  required>  
                  <?php foreach($productos as $producto):?> 
                      <option value="<?php echo $producto->id ?>" ><?php echo $producto->nombre ?></option>
                  <?php endforeach; ?>
                  </select>
                      <div class="tile-body  col-xl-3" style="position:relative; left:970px; margin-top:-50px;">

                                <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" name="start_prod2" id="start_prod2" value="<?php echo date("Y-m-01"); ?>">

                                <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" name="end_prod2" id="end_prod2" value="<?php echo $hoy; ?>">
                            </div>
                            <!-- /tile body -->
                            <div class="tile-footer">
                                <div class="form-group text-center">
                                    <button style="margin-top:15px; font-size: 14px;  position:relative; left: 567px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                </div>
                            </div>
                          <div id="mylineGraphPadre">
                          <canvas id="mylineGraph" style="margin-top:-65px;" width="80" height="29px"></canvas>
                          </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        </div>


        <div class="col-xl-4">
          <div style="margin-right:-275px;
          box-shadow: 0 1px 10px rgb(80 93 80);" class="card">
            <div  class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                <h3 style="font-size: 14px;" class="text-uppercase text-muted mb-1 ">Datos Generales</h3>
                <br>
                            <div class="tile-body col-xl-3" style="position:relative; left: 480px; margin-top:-35px;">

                                <h4 class="custom-font"><strong>Fecha</strong> Inicio</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" id="start_generales" name="start_generales" value="<?php echo date("Y-m-01");  ?>">

                                <h4 class="custom-font"><strong>Fecha</strong> Fin</h4>
                                <input style="font-size: 14px;" type="date" class="typeahead form-control" id="end_generales"name="end_generales" value="<?php echo $hoy; ?>">
                            </div>
                            <!-- /tile body -->
                            <div class="tile-footer">
                                <div class="form-group text-center">
                                    <button style="margin-top:15px; font-size: 14px;  position:relative; left: 170px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                </div>
                            </div>
                            <div id="myRadioGraphPadre">
                          <canvas id="myRadioGraph" style="margin-left:-90px; margin-top:-130px;" width="800" height="400"></canvas>
                          </div>  
                          </div>
            </div>
          </div>
        </div>
      </div>


              <!-- valores estaticos -->
          <?php 
           
            $Lista_fecha = [];
            $Lista_cantidad = [];
            $Lista_ganancia = [];


            $opcion = 1;        
            $grafico = ProcesoData::getFechaDia_mesActual($opcion);
            foreach($grafico as $graficos):
              array_push($Lista_fecha,$graficos->fecha);
              array_push($Lista_cantidad, $graficos->cantidad);
              array_push($Lista_ganancia, $graficos->ganancia);
            endforeach;
            ?>


            <script>


              var lista = <?php echo json_encode($Lista_fecha);?>;
              var lista2 = <?php echo json_encode($Lista_cantidad);?>;
              var lista3 = <?php echo json_encode($Lista_ganancia);?>;

            // line chart data
            var ctx = document.getElementById('buyers').getContext('2d');

              var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line', // also try bar or other graph types

                // The data for our dataset
                data: {
                  labels: lista,
                  // Information about the dataset
                  datasets: [{
                    label: ["Cantidad"],
                    backgroundColor: 'lightblue',
                    borderColor: 'royalblue',
                    data: lista2,
                  }]

                }

              });
            </script>
<?php
 //-----------------------------Grafico 4 estatico ---------------------

$Lista_fecha_estatic = [];
$Lista_cantidad_estatic = [];
$Lista_ganancia1_estatic = [];
$Lista_ganancia2_estatic = [];
$nombres_estatic = [];
$productos_lista_estatic = [];
foreach($productos as $producto_estatic):
  array_push($productos_lista_estatic,$producto_estatic->id);
endforeach; 

$fecha_ini_estatic = date("Y-m-01"); 
$fecha_fin_estatic = $hoy;
$grafico4_estatic = ProcesoData::get_ganancia_productos($fecha_ini_estatic,$fecha_fin_estatic,$productos_lista_estatic[0]);
foreach($grafico4_estatic as $graficos):
array_push($Lista_fecha_estatic,$graficos->fecha);
array_push($Lista_ganancia1_estatic, $graficos->ganancia);
array_push($nombres_estatic, $graficos->nombre);
endforeach;
?>
<script>
let nombre_producto1_estatic = <?php echo json_encode($nombres_estatic);?>;
let total_producto1_estatic  = <?php echo json_encode($Lista_ganancia1_estatic);?>;
let fecha_rango_estatic  = <?php echo json_encode($Lista_fecha_estatic);?>;

var line = document.getElementById("mylineGraph").getContext("2d");

var mylineGraph = new Chart(line, {
  type: "line",
  data: {
    labels: fecha_rango_estatic,
    datasets: [
      {
        label: nombre_producto1_estatic[0],
        data: total_producto1_estatic,
        fill: true,
        borderColor: "rgba(55, 130, 220, .65)",
        backgroundColor: "rgba(55, 130, 220, 0.1)",
        lineTension: 0,
        pointBorderWidth: 2,
        borderDash: [5, 5],
        pointStyle: "rectRounded"
      }/*
      {
        label: nombre_producto2,
        data: total_producto2,
        fill: false,
        borderColor: "rgba(245, 35, 56, .65)",
        lineTension: 0,
        pointBorderWidth: 2,
        pointStyle: "rectRounded"
      }*/
    ]
  },
  options: {
    title: {
      display: true,
      text: "Gráfico de ventas"
    },
    plugins: {
     datalabels: {
       display: false
     }
    }
  }
});




// -----------------------------------------Fin -------------------------

</script>
<?php
 //-----------------------------Grafico 5 estatico ---------------------

$Lista_fecha_estatic = [];
$Lista_cantidad_estatic = [];
$Lista_ganancia1_estatic = [];
$Lista_ganancia2_estatic = [];
$nombres_estatic = [];
$lista_DatosGenerales = [];


$fecha_ini_estatic = date("Y-m-01"); 
$fecha_fin_estatic = $hoy;
$grafico4_estatic = ProcesoData::get_generales($fecha_ini_estatic,$fecha_fin_estatic);
foreach($grafico4_estatic as $graficos):
array_push($lista_DatosGenerales,$graficos->ventas);
array_push($lista_DatosGenerales,$graficos->recepcion);
array_push($lista_DatosGenerales,$graficos->dinero_dejado);
array_push($lista_DatosGenerales,$graficos->compra);
array_push($lista_DatosGenerales,$graficos->egreso);
endforeach;
?>
<script>
let lista_DatosGenerales = <?php echo json_encode($lista_DatosGenerales);?>;


var radio = document.getElementById("myRadioGraph").getContext("2d");

var myRadioGraph = new Chart(radio, {
  type: "radar",
  data: {
    labels: [
      "Ventas",
      "Recepcion",
      "Dinero obsejeado",
      "Egreso",
      "Compra"
    ],
    datasets: [
      /*{
        label: "2018",
        data: [15, 39, 55, 35, 41, 15, 20],
        fill: true,
        backgroundColor: "rgba(54, 162, 235, 0.2)",
        borderColor: "rgb(54, 162, 235)",
        pointBackgroundColor: "rgb(54, 162, 235)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgb(54, 162, 235)"
      },*/
      {
        label: "Movimientos",
        data: lista_DatosGenerales,
        fill: true,
        backgroundColor: "rgba(255, 99, 132, 0.2)",
        borderColor: "rgb(255, 99, 132)",
        pointBackgroundColor: "rgb(255, 99, 132)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgb(255, 99, 132)"
      }
    ]
  },
  options: { 
    elements: { 
      line: { 
        tension: 0, 
        borderWidth: 3 
      } 
    },
    plugins: {
     datalabels: {
       display: false
     }
    }
  }
});
// -----------------------------------------Fin -------------------------
</script>     
<?php 


$Lista_fecha_tipo = [];
$Lista_cantidad_tipo = [];
    
$grafico = ProcesoData::get_medio_pago();
foreach($grafico as $graficos):
  array_push($Lista_fecha_tipo,$graficos->nombre);
  array_push($Lista_cantidad_tipo, $graficos->cantidad);
endforeach;
?>


<script>
  var Lista_fecha_tipo = <?php echo json_encode($Lista_fecha_tipo);?>;
  var Lista_cantidad_tipo = <?php echo json_encode($Lista_cantidad_tipo);?>;

 // Iniciamos con el grafico pie 
 new Chart(document.getElementById("oilChart"), {
  type: 'pie',
  data: {
    labels: Lista_fecha_tipo,
    datasets: [{
      label: "Population (millions)",
      backgroundColor: ["#122f50", "#20b9d8","#20dab3","#2a5d9d","#f5eb8f"],

      data: Lista_cantidad_tipo
    }]
  },
  options: {
    title: {
      display: true,
    }
  }
});



</script>


            <!-- valores estaticos -->
            <?php 


            $ganancia_ventas = [];
            $cantidad_ventas = [];
            $fecha = [];



            $grafico_venta = VentaData::get_grafica_ventas();
            foreach($grafico_venta as $graficos):
              array_push($ganancia_ventas,$graficos->ganancia_total);
              array_push($cantidad_ventas, $graficos->cantidad);
              array_push($fecha, $graficos->dia);

            endforeach;
            ?>


            <script>
              var cantidad_ventas = <?php echo json_encode($cantidad_ventas);?>;
              var ganancia_ventas = <?php echo json_encode($ganancia_ventas);?>;
              var fecha = <?php echo json_encode($fecha);?>;


            // bar chart data
            var ctx = document.getElementById("income").getContext('2d');
            var barChart = new Chart(ctx, {
              type: 'bar',
              data: {
                labels: fecha,
                datasets: [{
                  label: 'Ganancias',
                  data: ganancia_ventas,
                  backgroundColor: "#829de5"
                }, {
                  label: 'Cantidad',
                  data: cantidad_ventas,
                  backgroundColor: "#bedaed"
                }]
              }
            });
        </script>


<!-------------------------------------------------------------- Creamos la extraccion y envio de datos en el segundo grafico  ----------------------------------------------------------------->

<script type="text/javascript">
//Instanciamos y limpiamos los arrays a utilizar

// Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion

$('#end_var').change(function(){ 
    var value = $(this).val();
    tiempoReal_variantes(value);
});
$('#select_variantes').change(function(){ 
    var value = $(this).val();
    tiempoReal_variantes(value);
});
$('#start_var').change(function(){ 
    var value = $(this).val();
    tiempoReal_variantes(value);
});

let fecha_variante = [];
var total_variante = [];

function tiempoReal_variantes(value)
{

  // Extraemos valores para el cambio de datos en el grafico

  var dato = document.getElementById("select_variantes").value;
  var fecha_init = document.getElementById('start_var').value;
  var fecha_fin = document.getElementById('end_var').value;

  document.getElementById('oilChart').remove();
  document.getElementById('oilChartPadre').innerHTML = '<canvas id="oilChart" name="oilChart" width="530" height="400"></canvas>';

  var paso = "2";

	$.ajax({
		url : 'index.php?action=graficos_estadistica',
		type : 'POST',
		dataType : 'html',
		data : { fecha_fin: fecha_fin, dato: dato, fecha_init: fecha_init,  paso: paso },
		})
	.done(function(resultado){
    console.log(resultado);
    fecha_variante = [];
    total_variante = [];
    resultado1 = JSON.parse(resultado);
    resultado1.forEach(element => {
      total_variante.push(element.cantidad);
      fecha_variante.push(element.nombre);
    });

  // Iniciamos con el grafico pie 
  pai_variante(fecha_variante,total_variante);// iniciamos chart nuevamente
    
  function pai_variante(fecha_variante,total_variante) {
    new Chart(document.getElementById("oilChart"), {
        type: 'pie',
        data: {
          labels: fecha_variante,
          datasets: [{
            label: "Population (millions)",
            backgroundColor: ["#122f50", "#20b9d8","#20dab3","#2a5d9d","#f5eb8f"],

            data: total_variante
          }]
        },
        options: {
          title: {
            display: true,
          }
        }
      });
  }



})
  

}


</script>




<!-------------------------------------------------------------- Creamos la extraccion y envio de datos en el primer grafico  ----------------------------------------------------------------->
<script type="text/javascript">
//Instanciamos y limpiamos los arrays a utilizar

// Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion
$('#select_gc').change(function(){ 
    var value = $(this).val();
    tiempoReal(value);
});
$('#start_gc').change(function(){ 
    var value = $(this).val();
    tiempoReal(value);
});
$('#end_gc').change(function(){ 
    var value = $(this).val();
    tiempoReal(value);
});
$('#fecha_gc').change(function(){ 
    var value = $(this).val();
    tiempoReal(value);
});

let fecha_dato = [];
var total_dato = [];

function tiempoReal(value)
{
  
  document.getElementById('buyers').remove();
  document.getElementById('buyersPadre').innerHTML = '<canvas id="buyers" width="800" height="330"></canvas>';
  
  // Extraemos valores para el cambio de datos en el grafico
  var dato = document.getElementById("select_gc").value;
  var fecha_init = document.getElementById('start_gc').value;
  var fecha_fin = document.getElementById('end_gc').value;
  var fecha_gc = document.getElementById('fecha_gc').value;
  var paso = "1";
  
	$.ajax({
		url : 'index.php?action=graficos_estadistica',
		type : 'POST',
		dataType : 'html',
		data : { dato: dato, fecha_init: fecha_init, fecha_fin: fecha_fin, fecha_gc: fecha_gc, paso:paso },
		})
	.done(function(resultado){
    fecha_dato = [];
    total_dato = [];
    resultado1 = JSON.parse(resultado);
    resultado1.forEach(element => {
      total_dato.push(element.total);
      fecha_dato.push(element.fecha);
    });
    //alert(resultado);
		//alert(fecha_dato);
    //alert(total_dato);
    var valor;
    var dato = document.getElementById("select_gc").value;
    //alert(dato);
    if(dato == '1'){
      valor = 'Ganancia';
    }else if(dato == '2'){
      valor = 'Cantidad';
    }

function createMainChart(total_dato,fecha_dato) {
     // line chart data
     var ctx = document.getElementById('buyers').getContext('2d');
              var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line', // also try bar or other graph types

                // The data for our dataset
                data: {
                  labels: fecha_dato,
                  // Information about the dataset
                  datasets: [{
                    label: valor,
                    backgroundColor: 'lightblue',
                    borderColor: 'royalblue',
                    data: total_dato,
                  }]

                }

              });
}

createMainChart(total_dato,fecha_dato);// iniciamos chart nuevamente

})
  

}


</script>
</script>


<!-------------------------------------------------------------- Creamos la extraccion y envio de datos en el Tercer grafico  ----------------------------------------------------------------->

<script type="text/javascript">
//Instanciamos y limpiamos los arrays a utilizar

// Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion
$('#select_producto').change(function(){ 
  var value = $(this).val();
    tiempoReal_productos(value);
});
$('#start_prod').change(function(){ 
    var value = $(this).val();
    tiempoReal_productos(value);
});
$('#end_prod').change(function(){ 
    var value = $(this).val();
    tiempoReal_productos(value);
});



let fecha_producto = [];
var total_producto = [];
var cantidad_producto = [];

function tiempoReal_productos(value)
{

  // Extraemos valores para el cambio de datos en el grafico

  var dato = document.getElementById("select_producto").value;
  var fecha_init = document.getElementById('start_prod').value;
  var fecha_fin = document.getElementById('end_prod').value;

  document.getElementById('income').remove();
  document.getElementById('incomePadre').innerHTML = '<canvas id="income" name="income" width="1200" height="300"></canvas>';
 

  var paso = "3";

	$.ajax({
		url : 'index.php?action=graficos_estadistica',
		type : 'POST',
		dataType : 'html',
		data : { dato: dato, fecha_init: fecha_init, fecha_fin: fecha_fin, paso: paso },
		})
	.done(function(resultado){
    fecha_producto = [];
    total_producto = [];
    cantidad_producto = [];
    resultado1 = JSON.parse(resultado);
    resultado1.forEach(element => {
      total_producto.push(element.total);
      fecha_producto.push(element.fecha);
      cantidad_producto.push(element.cantidad);
    });
    

  // Iniciamos con el grafico nuevamente
    
function lista_Produtos(fecha_producto,total_producto,cantidad_producto) {
  var ctx = document.getElementById("income").getContext('2d');
            var barChart = new Chart(ctx, {
              type: 'bar',
              data: {
                labels: fecha_producto,
                datasets: [{
                  label: 'Ganancias',
                  data: total_producto,
                  backgroundColor: "#829de5"
                }, {
                  label: 'Cantidad',
                  data: cantidad_producto,
                  backgroundColor: "#bedaed",
                }]
              }
            });
}

lista_Produtos(fecha_producto,total_producto,cantidad_producto);// iniciamos chart nuevamente


})
  

}

</script>
<script type="text/javascript">

//---------------------------------------------------------------------------- Grafico #4 ------------------------------------------------------------------------------------------------
// Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion
$('#select_tipo').change(function(){ 
  var value = $(this).val();
  tiempoReal_gc(value);
});
$('#start_prod2').change(function(){ 
    var value = $(this).val();
    tiempoReal_gc(value);
});
$('#end_prod2').change(function(){ 
    var value = $(this).val();
    tiempoReal_gc(value);
});
$('#producto1').change(function(){ 
    var value = $(this).val();
    tiempoReal_gc(value);
});
$('#producto2').change(function(){ 
    var value = $(this).val();
    tiempoReal_gc(value);
});

var nombre_producto1;
var nombre_producto2;
var total_producto1 = [];
var total_producto2 = [];
let fecha_rango = [];
function tiempoReal_gc(value)
{
  // Extraemos valores para el cambio de datos en el grafico
  var dato = document.getElementById("select_tipo").value;
  var fecha_init = document.getElementById('start_prod2').value;
  var fecha_fin = document.getElementById('end_prod2').value;
  var producto1 = document.getElementById('producto1').value;
  var producto2 = document.getElementById('producto2').value;
  document.getElementById('mylineGraph').remove();
  document.getElementById('mylineGraphPadre').innerHTML = '<canvas id="mylineGraph" name="mylineGraph" style="margin-top:-65px;" width="80" height="29px"></canvas>';
 

  var paso = "4";

	$.ajax({
		url : 'index.php?action=graficos_estadistica',
		type : 'POST',
		dataType : 'html',
		data : {producto1:producto1,producto2:producto2,fecha_init:fecha_init,fecha_fin:fecha_fin,paso:paso},
		})
	.done(function(resultado){
    total_producto1 = [];
    total_producto2 = [];
    fecha_rango = [];
    resultado1 = JSON.parse(resultado);
   
    resultado1.forEach(element => {
      if(element.id == producto1){
        
        nombre_producto1 = element.nombre;
        if(dato == 1){
          total_producto1.push(element.ganancia);
        }else if(dato == 2){
          total_producto1.push(element.cantidad);
        }
      }else if(element.id == producto2){
        nombre_producto2 = element.nombre;
        if(dato == 1){
          total_producto2.push(element.ganancia);
        }else if(dato == 2){
          total_producto2.push(element.cantidad);
        }
      }
      fecha_rango.push(element.fecha);
    });
    

// Iniciamos con el grafico nuevamente
    
function lista_Productos(nombre_producto1,nombre_producto2,total_producto1,total_producto2,fecha_rango) {

var line = document.getElementById("mylineGraph").getContext("2d");
var mylineGraph = new Chart(line, {
  type: "line",
  data: {
    labels: fecha_rango,
    datasets: [
      {
        label: nombre_producto1,
        data: total_producto1,
        fill: true,
        borderColor: "rgba(55, 130, 220, .65)",
        backgroundColor: "rgba(55, 130, 220, 0.1)",
        lineTension: 0,
        pointBorderWidth: 2,
        borderDash: [5, 5],
        pointStyle: "rectRounded"
      },
      {
        label: nombre_producto2,
        data: total_producto2,
        fill: true,
        borderColor: "rgba(55, 130, 220, .65)",
        backgroundColor: "rgba(55, 130, 220, 0.1)",
        lineTension: 0,
        pointBorderWidth: 2,
        borderDash: [5, 5],
        pointStyle: "rectRounded"
      }

    ]
  },
  options: {
    title: {
      display: true,
      text: "Gráfico de ventas"
    },
    plugins: {
     datalabels: {
       display: false
     }
    }
  }
});

}

lista_Productos(nombre_producto1,nombre_producto2,total_producto1,total_producto2,fecha_rango);// iniciamos chart nuevamente

})
  

}

</script>

 <!---------------------------------------------------------------------------- final ----------------------------------------------------------------------------->
 <script type="text/javascript">

//---------------------------------------------------------------------------- Grafico #5 ------------------------------------------------------------------------------------------------
// Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion

$('#start_generales').change(function(){ 
    var value = $(this).val();
    tiempoReal_gc2(value);
});
$('#end_generales').change(function(){ 
    var value = $(this).val();
    tiempoReal_gc2(value);
});

let movimientos_generales = [];

function tiempoReal_gc2(value)
{
  // Extraemos valores para el cambio de datos en el grafico
  var fecha_init = document.getElementById('start_generales').value;
  var fecha_fin = document.getElementById('end_generales').value;

  document.getElementById('myRadioGraph').remove();
  document.getElementById('myRadioGraphPadre').innerHTML = '<canvas id="myRadioGraph" style="margin-left:-90px; margin-top:-130px;" width="800" height="400"></canvas>';
 

  var paso = "5";

	$.ajax({
		url : 'index.php?action=graficos_estadistica',
		type : 'POST',
		dataType : 'html',
		data : {fecha_init:fecha_init,fecha_fin:fecha_fin,paso:paso},
		})
	.done(function(resultado){
    movimientos_generales = [];
    resultado1 = JSON.parse(resultado);
   
    resultado1.forEach(element => {
      movimientos_generales.push(element.ventas);
      movimientos_generales.push(element.recepcion);
      movimientos_generales.push(element.dinero_dejado);
      movimientos_generales.push(element.egreso);
      movimientos_generales.push(element.compra);
    });
    

// Iniciamos con el grafico nuevamente
function lista_Productos2(movimientos_generales) {
  var radio = document.getElementById("myRadioGraph").getContext("2d");
  var myRadioGraph = new Chart(radio, {
  type: "radar",
  data: {
    labels: [
      "Ventas",
      "Recepcion",
      "Dinero obsejeado",
      "Egreso",
      "Compra"
    ],
    datasets: [
      /*{
        label: "Dinero",
        data: movimientos_generales,
        fill: true,
        backgroundColor: "rgba(54, 162, 235, 0.2)",
        borderColor: "rgb(54, 162, 235)",
        pointBackgroundColor: "rgb(54, 162, 235)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgb(54, 162, 235)"
      },*/
      {
        label: "Dinero",
        data: movimientos_generales,
        fill: true,
        backgroundColor: "rgba(255, 99, 132, 0.2)",
        borderColor: "rgb(255, 99, 132)",
        pointBackgroundColor: "rgb(255, 99, 132)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgb(255, 99, 132)"
      }
    ]
  },
  options: { 
    elements: { 
      line: { 
        tension: 0, 
        borderWidth: 3 
      } 
    },
    plugins: {
     datalabels: {
       display: false
     }
    }
  }
});


}

lista_Productos2(movimientos_generales); // iniciamos chart nuevamente

})
  

}

</script>

 <!---------------------------------------------------------------------------- final ----------------------------------------------------------------------------->
         </div>
        </div>


    </html>




    <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
        
        <script src="assets/js/vendor/footable/footable.all.min.js"></script>
      
<script>
$(window).load(function(){

   $('.footable').footable();
   
 });

    function Cargar(val)
    {   

        $('#graficos_resultado').html("Por favor espera un momento");    
        $.ajax({
            type: "POST",
            url: './?action=graficos_reportes',
            data: 'id_mostrar_cliente='+val,
            success: function(resp){
                document.getElementById('graficos_resultado').remove();
                document.getElementById('padre').innerHTML = '<div id="graficos_resultado"></div>';
                $('#graficos_resultado').html(resp);
            }
        });
    };

</script>






















