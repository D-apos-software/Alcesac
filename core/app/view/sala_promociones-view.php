<head>
    <link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
    <link rel="stylesheet" href="css/tablas/tablas.css" type="text/css">
    <link rel="stylesheet" href="css/vista_cliente/style.css">
    <link rel="stylesheet" href="css/vista_promociones/style.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>

<body id="minovate" class="appWrapper sidebar-sm-forced">
    <div class="row">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="index.php?view=reserva"><i class="fa fa-home"></i> Inicio</a></li>
                <li><a href="#">Configuración</a></li>
                <li class="active">Documento de identificación</li>
            </ol>
        </section>
    </div>
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <section class="tile">
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font"><strong>Promociones</strong></h1>
                    <ul class="controls">
                        <li class="remove">
                            <div style="paddin: 8px" class="btn-1">
                                <a href="?view=categoria_clientes_p" style="paddin: 8px"> <span class="fa fa-user">&nbsp;<span>Categorias</span></a>
                                <a href="#" style="paddin: 8px" data-toggle="modal" data-target="#myModal"> <span class="fa fa-user">&nbsp;<span>Registrar</span></a>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                <i class="fa fa-cog"></i><i class="fa fa-spinner fa-spin"></i>
                            </a>
                            <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                <li>
                                    <a role="button" tabindex="0" class="tile-toggle">
                                        <span class="minimize"><i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Minimize</span>
                                        <span class="expand"><i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-refresh">
                                        <i class="fa fa-refresh"></i> Refresh
                                    </a>
                                </li>
                                <li>
                                    <a role="button" tabindex="0" class="tile-fullscreen">
                                        <i class="fa fa-expand"></i> Fullscreen
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="remove"><a role="button" tabindex="0" class="tile-close"><i class="fa fa-times"></i></a></li>
                    </ul>
                </div>
                <!-- tile body -->
                <div class="tile-body">
                    <div class="form-group">
                        <label for="filter" style="padding-top: 5px">Buscar:</label>
                        <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
                    </div>



                    <?php $Documentos = Promociones::show_data();
                    if ($Documentos != null) {
                        // si hay usuarios
                    ?>
                        <!-- Muestra de la tabla -->
                        <table>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-plain">
                                        <div class="card-header card-header-primary">
                                            <center>
                                                <h4 class="card-title mt-0">Promociones</h4>
                                            </center>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" class="table table-hover">
                                                    <thead class="">
                                                        <tr>
                                                            <th>
                                                                Categoria
                                                            </th>
                                                            <th>
                                                                Valor
                                                            </th>
                                                            <th>
                                                                Descripcion
                                                            </th>
                                                            <th>
                                                                Estado
                                                            </th>
                                                            <th>
                                                                Actualizar
                                                            </th>
                                                            <th>
                                                                Eliminar
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($Documentos as $Documento) : ?>
                                                            <tr>
                                                                <td><?php echo $Documento->nombre; ?></td>
                                                                <td><?php echo $Documento->valor . '%'; ?></td>
                                                                <td><?php echo $Documento->descripcion ?></td>
                                                                <td>
                                                                    <input onclick="actualizar(<?php echo $Documento->id; ?>)" type="checkbox" class="checkbox" name="checkbox" id="checkbox" value="1" style="height: 26px; margin:-2px;" <?php if ($Documento->estado == 1) echo "checked"  ?>>
                                                                </td>
                                                                <td>
                                                                    <a style="margin-top:5px; margin-bottom:-2px;" href="" data-toggle="modal" data-target="#myModal_edit<?php echo $Documento->id; ?>" class="btn btn-primary btn-block2"><i class="glyphicon glyphicon-edit"></i> Editar</a>
                                                                </td>
                                                                <td>
                                                                    <a style="margin-top:5px; margin-bottom:-2px;" onclick="del(<?php echo $Documento->id; ?>)" class="btn btn-danger btn-block2"><i class="glyphicon glyphicon-edit"></i> Eliminar</a>
                                                                </td>
                                                            </tr>

                                                            <!-- Modal Actualizar -->
                                                            <div class="modal fade bs-example-modal-xm" id="myModal_edit<?php echo $Documento->id; ?>" role="dialog" aria-labelledby="myModalLabel">
                                                                <div class="modal-dialog modal-success">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <form class="form-horizontal" method="post" id="addproduct" action="index.php?action=actions_promociones" role="form">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span></button>
                                                                                    <h4 class="modal-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-file-text">&nbsp;&nbsp;</span>Crear Promoción</h4>
                                                                                </div>
                                                                                <div class="modal-body" style="background-color:#fff !important;">
                                                                                    <div class="row">
                                                                                        <div class="col-md-offset-1 col-md-10">
                                                                                            <div class="form-group">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon">Categoria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                    <?php $categoria =  Categoria_Clientes_P::getActivos(); ?>
                                                                                                    <?php $categoria_id = Tipo_r_promociones::getByID_Promo($Documento->id); ?>
                                                                                                    <select class="form-control select2" name="categoria" id="categoria" style="width: 93%!important;" required>
                                                                                                        <?php foreach ($categoria as $datos_c) : ?>
                                                                                                            <option value="<?php echo $datos_c->id ?> " <?php if ($datos_c->id == $categoria_id->id_categoria) {
                                                                                                                                                            echo "selected";
                                                                                                                                                        } ?>><?php echo $datos_c->nombre ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon">Porcentaje &nbsp;&nbsp;&nbsp;</span>
                                                                                                    <input type="text" class="form-control col-md-8" name="valor" required value="<?php echo $Documento->valor ?>">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon">Descripcion&nbsp;&nbsp;</span>
                                                                                                    <input type="text" class="form-control col-md-8" style="width: 93%!important;" name="descripcion" required value="<?php echo $Documento->descripcion ?>">
                                                                                                </div>
                                                                                            </div>
                                                                                            <!-- ----------------------------------------------------------------------- PRUEBAS -------------------------------------------------------------------------------------------------------------------------->
                                                                                            <!------------------------------ Habitaciones ------------------------->
                                                                                            <div class="form-group">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon"> Habitaciones</span>
                                                                                                    <div class="dropdown" data-control="checkbox-dropdown">
                                                                                                        <div class="iz">
                                                                                                            <label class="form-control dropdown-label">Seleccionar</label>
                                                                                                            <div class="dropdown-list">
                                                                                                                <?php $lista = array(); ?>
                                                                                                                <?php $habitaciones = HabitacionData::getActivos(); ?>
                                                                                                                <?php
                                                                                                                $Relaciones = Tipo_r_promociones::getByID_h($Documento->id);
                                                                                                                $ids_habitaciones = array();
                                                                                                                foreach ($Relaciones as $idsh) {
                                                                                                                    array_push($ids_habitaciones, $idsh->id_habitacion);
                                                                                                                }
                                                                                                                ?>
                                                                                                                <?php foreach ($habitaciones as $habitacione) : ?>
                                                                                                                    <label class="dropdown-option">
                                                                                                                        <input type="checkbox" name="dropdown-group[]" value="<?php echo $habitacione->id; ?>" <?php
                                                                                                                                                                                                                foreach ($ids_habitaciones as $ids) {
                                                                                                                                                                                                                    if ($habitacione->id == $ids) {
                                                                                                                                                                                                                        echo "checked";
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }
                                                                                                                                                                                                                ?> />
                                                                                                                        <?php
                                                                                                                        echo $habitacione->nombre;
                                                                                                                        array_push($lista, $habitacione->id);
                                                                                                                        ?>
                                                                                                                    </label>
                                                                                                                <?php endforeach; ?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!------------------------------ Productos ------------------------->
                                                                                                    <span class="input-group-addon"> Productos</span>
                                                                                                    <div class="dropdown" data-control="checkbox-dropdown">
                                                                                                        <div class="dr">
                                                                                                            <label class="form-control dropdown-label">Seleccionar</label>
                                                                                                            <div class="dropdown-list">
                                                                                                                <?php $lista = array(); ?>
                                                                                                                <?php $productos = ProductoData::getActivos(); ?>
                                                                                                                <?php
                                                                                                                $RelacionesP = Tipo_r_promociones::getByID_h($Documento->id);
                                                                                                                $ids_productos = array();
                                                                                                                foreach ($RelacionesP as $idsp) {
                                                                                                                    array_push($ids_habitaciones, $idsp->id_producto);
                                                                                                                }
                                                                                                                ?>
                                                                                                                <?php foreach ($productos as $producto) : ?>
                                                                                                                    <label class="dropdown-option">
                                                                                                                        <input type="checkbox" name="dropdown2-group[]" value="<?php echo $producto->id; ?>" 
                                                                                                                        <?php
                                                                                                                        foreach($ids_habitaciones as $ids){
                                                                                                                            if($producto->id == $ids){
                                                                                                                               echo "checked"; 
                                                                                                                            }
                                                                                                                        }


                                                                                                                        ?>
                                                                                                                        />
                                                                                                                        <?php
                                                                                                                        echo $producto->nombre;
                                                                                                                        array_push($lista, $producto->id);
                                                                                                                        ?>
                                                                                                                    </label>
                                                                                                                <?php endforeach; ?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>

                                                                                        </div>

                                                                                        <!-- ----------------------------------------------------------------------- PRUEBAS FINAL  -------------------------------------------------------------------------------------------------------------------------->

                                                                                        <input type="hidden" name="id" id="id" value="<?php echo $Documento->id; ?>" >

                                                                                        
                                                                                        <div class="modal-footer">
                                                                                            <button style="margin-left: 25px;" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                                                                                            <input type="hidden" class="form-control" name="estado" value="editar">
                                                                                            <button style="margin-left: 367px;" type="submit" class="btn btn-outline">Actualizar</button>
                                                                                        </div>

                                                                            </form>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                                <!-- /.modal -->
                                                            </div>





                                                        <?php endforeach; ?>
                                                    <tfoot class="hide-if-no-paging" style="left: -20px;">
                                                        <tr>
                                                            <td colspan="5" class="text-center">
                                                                <ul class="pagination"></ul>
                                                            </td>
                                                        </tr>
                                                    </tfoot>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </table>
                    <?php } else {
                        echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
                    };
                    ?>

                </div>
            </section>
        </div>
    </div>

    <!-- Modal Agregar -->
    <div class="modal fade bs-example-modal-xm" id="myModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-success">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" method="post" id="addproduct" action="index.php?action=actions_promociones" role="form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">&nbsp;&nbsp;&nbsp;<span class="fa fa-file-text">&nbsp;&nbsp;</span>Crear Promoción</h4>
                        </div>
                        <div class="modal-body" style="background-color:#fff !important;">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Categoria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <?php $categoria =  Categoria_Clientes_P::getActivos(); ?>
                                            <select class="form-control select2" name="categoria" id="categoria" style="width: 93%!important;" required>
                                                <?php foreach ($categoria as $datos_c) : ?>
                                                    <option value="<?php echo $datos_c->id ?>"><?php echo $datos_c->nombre ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Porcentaje &nbsp;&nbsp;&nbsp;</span>
                                            <input type="text" class="form-control col-md-8" name="valor" required placeholder="Ingrese el valor">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Descripcion&nbsp;&nbsp;</span>
                                            <input type="text" class="form-control col-md-8" style="width: 93%!important;" name="descripcion" required placeholder="Ingrese la descripcion">
                                        </div>
                                    </div>
                                    <!-- ----------------------------------------------------------------------- PRUEBAS -------------------------------------------------------------------------------------------------------------------------->
                                    <!------------------------------ Habitaciones ------------------------->
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"> Habitaciones</span>
                                            <div class="dropdown" data-control="checkbox-dropdown">
                                                <div class="iz">
                                                    <label class="form-control dropdown-label">Seleccionar</label>
                                                    <div class="dropdown-list">
                                                        <?php $lista = array(); ?>
                                                        <?php $habitaciones = HabitacionData::getActivos(); ?>
                                                        <?php foreach ($habitaciones as $habitacione) : ?>
                                                            <label class="dropdown-option">
                                                                <input type="checkbox" name="dropdown-group[]" value="<?php echo $habitacione->id; ?>" />
                                                                <?php
                                                                echo $habitacione->nombre;
                                                                array_push($lista, $habitacione->id);
                                                                ?>
                                                            </label>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!------------------------------ Productos ------------------------->
                                            <span class="input-group-addon"> Productos</span>
                                            <div class="dropdown" data-control="checkbox-dropdown">
                                                <div class="dr">
                                                    <label class="form-control dropdown-label">Seleccionar</label>
                                                    <div class="dropdown-list">
                                                        <?php $lista = array(); ?>
                                                        <?php $productos = ProductoData::getActivos(); ?>
                                                        <?php foreach ($productos as $producto) : ?>
                                                            <label class="dropdown-option">
                                                                <input type="checkbox" name="dropdown2-group[]" value="<?php echo $producto->id; ?>" />
                                                                <?php
                                                                echo $producto->nombre;
                                                                array_push($lista, $producto->id);
                                                                ?>
                                                            </label>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <!-- ----------------------------------------------------------------------- PRUEBAS FINAL  -------------------------------------------------------------------------------------------------------------------------->

                                <input type="text" name="estado" id="estado" value="agregar" hidden>

                                <div class="modal-footer">
                                    <button style="margin-left: 12px;" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                                    <input type="hidden" class="form-control" name="id_estado" value="add">
                                    <button style="margin-left: 393px;" type="submit" class="btn btn-outline">Agregar</button>
                                </div>

                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>


    <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>
    <script src="assets/js/vendor/footable/footable.all.min.js"></script>
    <script src="js/vista_productos/function.js"></script>

    <script>
        $(window).load(function() {

            $('.footable').footable();

        });

        function actualizar(id) {
            var parametros = {
                "id": id,
                "estado": "estado"
            };
            $.ajax({
                type: "POST",
                url: 'index.php?action=actions_promociones',
                data: parametros,
                success: function(data) {

                }
            });
        }

        function del(id) {
            Swal.fire({
                title: 'Estas seguro?',
                text: "",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar'
            }).then((result) => {
                if (result.isConfirmed) {
                    var parametros = {
                        "id": id,
                        "estado": "eliminar"
                    };
                    $('#tipo_cliente').html("Por favor espera un momento");
                    $.ajax({
                        type: "POST",
                        url: 'index.php?action=actions_promociones',
                        data: parametros,
                        success: function(data) {
                            window.location.reload(); // Recargar página
                        }
                    });
                }

            })
        }
    </script>