<!DOCTYPE html>
<html>
<script src="js/jquery-2.2.3.min.js"></script>
<script src="https://npmcdn.com/chart.js@latest/dist/chart.min.js"></script>

<head>
  <!-- Favicon -->
  <link rel="stylesheet" href="css/vista_principal/vendor/nucleo/css/nucleo.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="css/vista_principal/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="css/vista_principal/css/style.css" type="text/css">
</head>

<body>
  <!-- Main content -->
  <div style="margin-top:50px;" class="main-content" id="panel">
    <!-- Topnav -->

    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 style="font-size: 18px;" class="h2 text-white d-inline-block mb-0">Estadisticas Generales
                <?php
                $locale = date_default_timezone_set('America/Costa_Rica');
                $locale = setlocale(LC_TIME, "spanish", $locale);
                $mi_fecha = date("F");
                $mi_fecha = str_replace("/", "-", $mi_fecha);
                $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
                $Mes_Anyo = strftime("%A, %d de %B del %Y", strtotime($Nueva_Fecha));
                //devuelve: Mes actual y año actual
                echo $Mes_Anyo; ?>

              </h6>

              <!--<nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Default</li>
                </ol>
              </nav>-->
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a style="font-size: 18px; color: rgb(255 255 255);background-color: #0000001c;" href="?view=sala_reportes" class="btn btn-sm btn-neutral">Más Estadisticas</a>
            </div>
          </div>
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <?php

                      /*$ganancia_del_mes = CajasAperturas::getCrecimiento_de_ventas();
                      $COMPRAS_MES_ACTUAL = CajasAperturas::getTotalEgreso_compras_mes();
                      $EGRESO_MES_ACTUAL = CajasAperturas::getTotalEgreso_mes();
                      $TOTAL_DE_EGRESO_DEL_MES_ACTUAL = $EGRESO_MES_ACTUAL->egreso + $COMPRAS_MES_ACTUAL->total;
                      $TOTAL_DE_GANANCIA_MES_ACTUAL = $ganancia_del_mes->total - $TOTAL_DE_EGRESO_DEL_MES_ACTUAL;*/

                      $ingreso_recervas = CajasAperturas::get_habitaciones_ingresos_dia();
                      ?>
                       <div style="margin-bottom: 15px;"><h5 style="font-size: 12px;" class="card-title text-uppercase text-muted mb-0">Ganancia de recepcion </div>
                        <?php
                        // Mes actual en inglés, de January a Decemberprint();
                        setlocale(LC_TIME, "spanish");
                        $mi_fecha = date("F");
                        $mi_fecha = str_replace("/", "-", $mi_fecha);
                        $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
                        $Mes_Anyo = strftime("%B", strtotime($Nueva_Fecha));
                        //devuelve: Mes actual y año actual
                        echo $Mes_Anyo;
                        ?>
                      </h5>
                      <br>
                      <span class="h2 font-weight-bold mb-0"><?php print_r($ingreso_recervas[0]->monto . " ₡"); ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <?php
                      //Monto aperturado
                      $cajas = CajasAperturas::getMontoAperturado();  ?>
                      <div style="margin-bottom: 17px;"><h5 style="font-size: 12px;" class="card-title text-uppercase text-muted mb-0">Monto total aperturado</div>
                      <br><span class="h2 font-weight-bold mb-0">
                        
                          <?php
                          if ($cajas->monto_Aperturado != null) {
                            print_r($cajas->monto_Aperturado . " ₡");
                          } else {
                            print_r("0 ₡");
                          }
                          ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                        <i class="ni ni-chart-pie-35"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 5.18%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="col-xl-2 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <?php
                      //Monto aperturado
                      $total_egreso = CajasAperturas::getTotalEgreso();
                      $total_egreso_compra = CajasAperturas::getTotalEgreso_compras();
                      if ($total_egreso->egreso == null) {
                        $total_egreso->egreso = 0;
                      } else if ($total_egreso_compra->total == null) {
                        $total_egreso_compra->total = 0;
                      }
                      $perdida_total = $total_egreso->egreso + $total_egreso_compra->total;
                      ?>
                       <div style="margin-bottom: 15px;"><h5 style="font-size: 12px;" class="card-title text-uppercase text-muted mb-0">Monto total de Gastos </h5></div>
                      <span class="h2 font-weight-bold mb-0"><?php print_r($perdida_total . " ₡"); ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="ni ni-money-coins"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 1.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-2 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <?php $total_ingreso = CajasAperturas::getTotalIngreso(); ?>
                      <?php $total_ingreso_recepcion = CajasAperturas::getTotalIngreso_recepcion(); ?>
                      <div style="margin-bottom: 15px;"><h5 style="font-size: 12px;" class="card-title text-uppercase text-muted mb-0">Monto total de Ventas</h5></div>
                      <span class="h2 font-weight-bold mb-0"><?php


                                                              if ($total_ingreso->total != null) {
                                                                print_r($total_ingreso->total . " ₡");
                                                              } else {
                                                                print_r("0 ₡");
                                                              } ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="ni ni-chart-bar-32"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-2 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 style="font-size: 12px;" class="card-title text-uppercase text-muted mb-4">Ganancia del Dia</h5>
                      <span class="h2 font-weight-bold mb-0"><?php

                                                              print_r($total_ingreso->total + $total_ingreso_recepcion->total . " ₡");



                                                              ?></span>
                    </div>
                    <div class="col-auto">
                      <div style="background: linear-gradient(87deg, #3def11b5 0, #36bd15 100%) !important;" class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="fa fa-bar-chart"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 1.08%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid2 mt--3">
      <div class="row">
        <div class="col-xl-8">
          <div class="card bg-default">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 style="font-size: 15px;" class="text-light text-uppercase ls-1 mb-1">Crecimiento de Ventas</h6>
                </div>
                <div class="col">
                  <ul class="nav nav-pills justify-content-end">
                    <select name="select_fecha" id="select_fecha" style="font-size: 15px; background-color:#525f7f;" class="selectpicker">
                      <option value="dia">Dias</option>
                      <option value="mes">Mes</option>
                      <option value="año">Años</option>
                    </select>
                  </ul>
                </div>
              </div>
            </div>
            <div id="pieChartContent" style="padding:49px;" class="card-body">
              <!-- Chart -->
              <canvas id="publishedBooks"></canvas>
            </div>
          </div>
        </div>
        <div style="paddin:200px;" class="col-xl-4">
          <div class="card">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 style="font-size: 12px;" class="text-uppercase text-muted ls-1 mb-1">Ventas por Producto<br>
                    <?php
                    // Mes actual en inglés, de January a Decemberprint();
                    setlocale(LC_TIME, "spanish");
                    $mi_fecha = date("F");
                    $mi_fecha = str_replace("/", "-", $mi_fecha);
                    $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
                    $Mes_Anyo = strftime("%B del %Y", strtotime($Nueva_Fecha));
                    //devuelve: Mes actual y año actual
                    echo $Mes_Anyo;
                    ?>
                  </h6>
                </div>
              </div>
            </div>
            <div class="myChartDiv">
              <canvas id="myChart" width="600" height="545"></canvas>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xl-8">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 style="font-size: 14px;" class="mb-0">Cajas Habiertas el dia <?php

                                                                                    setlocale(LC_TIME, "spanish");
                                                                                    $mi_fecha = date("F");
                                                                                    $mi_fecha = str_replace("/", "-", $mi_fecha);
                                                                                    $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
                                                                                    $Mes_Anyo = strftime("%d de %B del %Y", strtotime($Nueva_Fecha));
                                                                                    //devuelve: Mes actual y año actual
                                                                                    echo $Mes_Anyo;
                                                                                    ?></h3>
                </div>
                <div class="col text-right">
                  <a style="font-size: 12px;" href="?view=sala_de_cajas" class="btn btn-sm btn-primary">Ver Cajas</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <!-- INICIA FOREACH -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th style="font-size: 12px;" scope="col">Usuario</th>
                    <th style="font-size: 12px;" scope="col">Caja</th>
                    <th style="font-size: 12px;" scope="col">Hora aperturado</th>
                    <th style="font-size: 12px;" scope="col">Monto Aperturado</th>
                    <th style="font-size: 12px;" scope="col">Hora Cierre</th>
                    <th style="font-size: 12px;" scope="col">Monto Cierre </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $Datos_usuario = CajasAperturas::get_usuarios_datos_dia();
                  foreach ($Datos_usuario as $Datos_usuarios) :
                  ?>
                    <tr>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->Usuario ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->numero ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->hora_inicial ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->monto_inicial . " ₡"; ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->hora_cierre ?></td>
                      <td style="font-size: 12px;"><?php echo $Datos_usuarios->monto_final . " ₡"; ?></td>

                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h6 style="font-size: 12px;" class="text-uppercase text-muted mb-1 ">Ganancia De Habitaciones<br>
                    <?php
                    // Mes actual en inglés, de January a Decemberprint();
                    setlocale(LC_TIME, "spanish");
                    $mi_fecha = date("F");
                    $mi_fecha = str_replace("/", "-", $mi_fecha);
                    $Nueva_Fecha = date("d-m-Y", strtotime($mi_fecha));
                    $Mes_Anyo = strftime("%B del %Y", strtotime($Nueva_Fecha));
                    //devuelve: Mes actual y año actual
                    echo $Mes_Anyo;
                    ?>
                  </h6>
                </div>
                <div class="col text-right">
                  <a href="?view=test" style="font-size: 12px;" class="btn btn-sm btn-primary">Ver más</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th style="font-size: 12px;" scope="col">Nombre</th>
                    <th style="font-size: 12px;" scope="col">Ganancia</th>
                    <th style="font-size: 12px;" scope="col">Veces Aperturada</th>
                    <th style="font-size: 12px;" scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $habitaciones = CajasAperturas::get_habitaciones_ingresos();
                  function color_Bar()
                  {
                    $random = rand(1, 5);
                    if ($random == 1) {
                      $random = "progress-bar bg-gradient-success";
                    } else if ($random == 2) {
                      $random = "progress-bar bg-gradient-danger";
                    } else if ($random == 3) {
                      $random = "progress-bar bg-gradient-primary";
                    } else if ($random == 4) {
                      $random = "progress-bar bg-gradient-warning";
                    } else if ($random == 5) {
                      $random = "progress-bar bg-gradient-info";
                    }
                    return $random;
                  }
                  foreach ($habitaciones as $habitacion) : ?>
                    <tr>
                      <th style="font-size: 10px;" scope="row">
                        <?php echo $habitacion->nombre; ?>
                      </th>
                      <td style="font-size: 10px;">
                        <?php echo $habitacion->ganancia . " ₡"; ?>
                      </td>
                      <td style="font-size: 10px;">

                        <div class="d-flex align-items-center">
                          <span class="mr-2"><?php echo $habitacion->cantidad . ""; ?></span>
                          <div>
                            <div class="progress">
                              <div class="<?php echo color_Bar(); ?>" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
                            </div>
                          </div>
                        </div>

                      </td>
                    </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
            &nbsp;&nbsp; &copy; 2022 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Principales funciones y procesos establecidos, Alcesac Version 0.1</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.d-apos.com/" class="nav-link" target="_blank">Etapas</a>
              </li>
              <li class="nav-item">
                <a href="https://www.d-apos.com/" class="nav-link" target="_blank">Desarrollo</a>
              </li>
              <li class="nav-item">
                <a href="https://www.d-apos.com/" class="nav-link" target="_blank">Alcesac</a>
              </li>
              <li class="nav-item">
                
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src='lib/locale/es.js'></script>
  <script src="css/vista_principal/vendor/js-cookie/js.cookie.js"></script>
  <script src="css/vista_principal/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="css/vista_principal/js/argon.js?v=1.2.0"></script>
</body>


<?php
$Lista_nombre = [];
$Lista_cantidad = [];


$grafico = CajasAperturas::get_GananciasProductos();
foreach ($grafico as $graficos) :
  array_push($Lista_nombre, $graficos->nombre);
  array_push($Lista_cantidad, $graficos->ganancias);
endforeach;

?>
<script>
  var ctx = document.getElementById('myChart');
  var lista = <?php echo json_encode($Lista_nombre); ?>;
  var lista2 = <?php echo json_encode($Lista_cantidad); ?>;
  console.log(lista);
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: lista,
      datasets: [{
        label: 'Ocultar',
        data: lista2,
        backgroundColor: '#fb6340',
        borderColor: '#FF000066'
      }, ],
    },
    options: {
      plugins: {
        legend: {
          labels: {
            generateLabels: (chart) => {
              const datasets = chart.data.datasets;
              return datasets.map((dataset, index) => {
                const color = dataset.backgroundColor ?
                  dataset.backgroundColor?.substring(0, 7) :
                  'transparent';
                return {
                  hidden: dataset.hidden,
                  datasetIndex: index,
                  text: dataset.label || '',
                  strokeStyle: color,
                  fillStyle: color,
                };
              });
            },
            usePointStyle: true,
          },
        },
      },
      scales: {
        x: {},
        y: {},
      },
    },
  });
</script>

<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ -->

<script>
  var e = $("#select_fecha").val();
</script>

<?php
//Inicio del primer recorrido - este recorrido se ejecuta una ves solo al inicio
$lista_fecha = [];
$lista_valor = [];
$dato = '<script>document.write(e)</script>';
$lista_ventas = CajasAperturas::get_grafico_ventas_crecimiento($dato);

foreach ($lista_ventas as $value) :
  array_push($lista_fecha, $value->fecha);
  array_push($lista_valor, $value->monto);
endforeach;
?>
<script>
  // Grafico por fechas anual,mensual, dias
  //Fake Datasets
  var lista_dia = <?php echo json_encode($lista_fecha); ?>;
  var lista_valor = <?php echo json_encode($lista_valor); ?>;
  var ctx = document.getElementById('publishedBooks');
  orderData = [{
    label: 'Monto de ventas',
    backgroundColor: 'rgba(98, 218, 252, 0.1)',
    borderColor: '#49bddd',
    data: lista_valor,
  }];

  function createMainChart(dataset) {


    new Chart(ctx, {
      // The type of chart we want to create
      type: 'line',

      // The data for our dataset
      data: {
        labels: lista_dia,
        datasets: dataset //Passed into function
      },

      // Chart Configuration options
      options: {
        responsive: true,
        aspectRatio: 1,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: '#9a9a9a'
            },
          }],
          xAxes: [{
            ticks: {
              fontColor: '#9a9a9a'
            },
          }]
        }
      }
    });
  }

  createMainChart(orderData); // Iniciamos el primer grafico

  //Fin del recorrido
</script>

</html>




<script type="text/javascript">
  //Instanciamos y limpiamos los arrays a utilizar
  let monto = [];
  let fecha = [];
  var orderData = [];

  function tiempoReal(value) {


    document.getElementById('publishedBooks').remove();
    document.getElementById('pieChartContent').innerHTML = '<canvas id="publishedBooks"></canvas>';

    var ctx = document.getElementById('publishedBooks');
    var dato = document.getElementById("select_fecha").value;
    //alert(dato);

    $.ajax({
        url: 'index.php?action=principal_chart',
        type: 'POST',
        dataType: 'html',
        data: {
          dato: dato
        },
      })
      .done(function(resultado) {
        monto = [];
        fecha = [];
        orderData = [];
        resultado1 = JSON.parse(resultado);
        resultado1.forEach(element => {
          monto.push(element.monto);
          fecha.push(element.fecha);
        });

        //alert(monto);
        //alert(fecha);

        orderData = [{
          label: 'Monto de ventas',
          backgroundColor: 'rgba(98, 218, 252, 0.1)',
          borderColor: '#49bddd',
          data: monto,
        }];

        function createMainChart(dataset, fecha) {


          new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
              labels: fecha,
              datasets: dataset //Passed into function
            },

            // Chart Configuration options
            options: {
              responsive: true,
              aspectRatio: 1,
              maintainAspectRatio: false,
              legend: {
                display: false
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true,
                    fontColor: '#9a9a9a'
                  },
                }],
                xAxes: [{
                  ticks: {
                    fontColor: '#9a9a9a'
                  },
                }]
              }
            }
          });
        }

        createMainChart(orderData, fecha); // Creates inital chart



      })

    //alert(tabla);
  }

  // Realizamos condicion para ejecutar las funciones cada que seleccionemos una opcion
  $('#select_fecha').change(function() {
    var value = $(this).val();
    tiempoReal(value);
    //alert(value);
  });
  //setInterval(tiempoReal, 2000);
</script>
</script>