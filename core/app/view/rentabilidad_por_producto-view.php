<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/tablas/tablas.css" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

<body id="minovate" class="appWrapper sidebar-sm-forced">
    <!-- row -->
    <br><br>
    <div class="row">
        <!-- col -->
        <div class="col-md-12">
            <section class="tile">
                <!-- tile body -->
                <div class="tile-body">
                    <div class="form-group">
                        <label for="filter" style="padding-top: 5px">Buscar:</label>
                        <input id="filter" type="text" class="form-control input-sm w-sm mb-12 inline-block" />
                    </div>
                    <div class="form-group">
                        <div style="margin-left:850px;">
                        <strong>Fecha Inicio</strong>
                        <input style="font-size: 14px;" type="date" class="typeahead form-control input-sm w-sm mb-12 inline-block" name="start_prod" id="start_prod" value="<?php echo date("Y-m-01"); ?>">
                        <strong>Fecha Fin</strong>
                        <input style="font-size: 14px;" type="date" class="typeahead form-control input-sm w-sm mb-12 inline-block" name="start_prod" id="start_prod" value="<?php echo date("Y-m-01"); ?>">
                        </div>
                    </div>
                    <?php $Documentos = ProductoData::get_ganancias_N_B();
                    if ($Documentos != null) {
                    ?>
                        <!-- Muestra de la tabla -->
                        <table>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-plain">
                                        <div class="card-header card-header-primary">
                                            <center>
                                            <h4 class="card-title mt-0"> Lista De Rentabilidad por Producto.</h4>
                                            </center>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="searchTextResults" data-filter="#filter" data-page-size="7" class="footable table table-custom" class="table table-hover">
                                                    <thead class="">
                                                        <tr>
                                                            <th>
                                                                Código
                                                            </th>
                                                            <th>
                                                                Nombre
                                                            </th>
                                                            <th>
                                                                Garantia Bruta
                                                            </th>
                                                            <th>
                                                                Garantia Neta
                                                            </th>
                                                            <th>
                                                                Gráfica
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($Documentos as $Documento) : ?>
                                                            <tr>
                                                                <td><?php echo $Documento->codigo; ?></td>
                                                                <td><?php echo $Documento->nombre; ?></td>
                                                                <td><?php echo 'valor neto'; ?></td>
                                                                <td><?php echo 'valor bruto'; ?></td>
                                                                <td>
                                                                    <a onclick="id_pro(<?php echo $Documento->id ?>)" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-block2">Ver Gráfica</a>
                                                                </td>
                                                            </tr>
                                            </div>
                                            <!-- /.Final del modal -->
                                        <?php endforeach; ?>
                                        </tbody>
                        </table>
                </div>
        </div>
    </div>
    </div>
    </div>
    </table>
    <!-- Creamos modal interno que mostrara el grafico -->
    <div class="modal fade bs-example-modal-xm" id="myModal" role="dialog" aria-labelledby="myModalLabel">
        <div id="padre">
            <div id="graficos_resultado">
                <!-- Inicio de los graficos -->
                <div class="col-xl-12">
                    <div style="margin-left:5px; margin-right:5px; box-shadow: 0 1px 10px rgb(80 93 80); font-size: 14px;" class="card">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <!-- /tile body -->
                                    <div class="tile-footer">
                                        <div class="form-group text-center">
                                            <button style="margin-top:15px; font-size: 14px;  position:relative; left: 567px; visibility:hidden;" class="btn btn-rounded btn-success ripple" type="submit"><i class="fa fa-open-eye"></i> Ver informe</button>
                                        </div>
                                    </div>
                                    <div id="incomePadre">
                                        <canvas id="myChart" width="1200" height="400"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var ctx = document.getElementById("myChart");
            var chart = new Chart(ctx, {
                type: "line",
                data: {
                    labels: ["2020/02/17", "", "2020/10/23", "", "2020/11/29", "2020/12/30"],
                    datasets: [{
                            type: "line",
                            backgroundColor: "rgba(54, 162, 235, 0.2)",
                            borderColor: "rgba(54, 162, 235, 1)",
                            borderWidth: 1,
                            label: "Bruta",
                            data: [60, 49, 72, 90, 100, 60]
                        },
                        {
                            type: "line",
                            label: "Neta",
                            data: [25, 13, 30, 35, 25, 40],
                            lineTension: 0,
                            fill: true
                        }
                    ]
                }
            });
        </script>

    <?php } else {
                        echo "<h4 class='alert alert-success'>NO HAY REGISTRO</h4>";
                    };
    ?>

    </div>


    </section>

    </div>
    </div>

    <script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"></script>

    <script src="assets/js/vendor/footable/footable.all.min.js"></script>


    <script>
        $(window).load(function() {

            $('.footable').footable();

        });



        function id_pro(val){
            alert(val);
        }

    </script>

    <style>
        .modal-footer {
            background-color: #ffffff !important;
        }

        .input-group .form-control {
            position: relative;
            z-index: 2;
            float: left;
            width: 70% !important;
            margin-bottom: 0;
        }

        .modal-footer {
            padding: 15px;
            text-align: right;
            border-top: 1px solid #ffffff;
        }

        .table {

            box-shadow: none;
        }

        button.close {

            padding: 15px !important;

        }

        b,
        strong {
            font-size: 20px;
        }
    </style>