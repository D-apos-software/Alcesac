<!DOCTYPE html>
<html>

<head>
    <!-- Favicon -->
    <link rel="stylesheet" href="css/vista_mantenimientos/mantenimientos.css" type="text/css">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    <!-- Vista de opciones a escoger el mantenimiento al cual redireccionarse-->
    <!-- flip-card-container -->
    <!-- row -->
    <br><br><br><br>
    <div class="col-md-12">
        <section class="tile">
            <div style="margin-left:1000px; margin-right:45px; padding-top:20px;" class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label style="font-size: 14px;">Categoria de mantenimientos</label>
                        <select style="font-size: 14px;" class="form-control select2" required name="tipo_id_cliente" onchange="Cargar_categorias(this.value);">
                            <option>Selecciona una Categoria</option>
                            <option value="1">Recepción</option>
                            <option value="2">Punto de venta</option>
                            <option value="3">Aperturas</option>
                            <option value="4">Notificaciones</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="padre">
                <div id="categoria_hijo">
                    <div class="tile-header dvd dvd-btm">
                        <!-- primer mantenimiento-->
<!--                         <div class="col-lg-1 col-xs-6" style="margin-right:230px; margin-left:-60px;">
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">

                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>

                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Documento</a>
                                        </ul>
                                    </div>

                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                        </figure>

                                        <a href="index.php?view=tipo_documento"><button>Abrir</button></a>

                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
 -->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;  margin-left:-60px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Estado de Pago</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="image-2">
                                        </figure>
                                        <a href="index.php?view=estado_de_pago"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!-- /flip-card-container -->
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Medio de Pago</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="image-2">
                                        </figure>
                                        <a href="index.php?view=tipo_de_pago"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /flip-card-container -->
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <!--<div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Tipo Cliente</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="image-2">
                                        </figure>
                                        <a href="index.php?view=tipo_cliente"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>-->
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Tipo Comprobante</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="image-2">
                                        </figure>
                                        <a href="index.php?view=tipo_comprobante"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Periodos de Pago</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="image-2">
                                        </figure>
                                        <a href="index.php?view=periodos_pago"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;  margin-left:-60px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="image-2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Proceso de Pago</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="image-2">
                                        </figure>
                                        <a href="index.php?view=tipo_proceso"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/1c/2a/ea/1c2aea7da3ca6eaef958d46dcefccf58.jpg" alt="Image 2">
                                            <figcaption>Aperturas</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Cantidad de cajas</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/1c/2a/ea/1c2aea7da3ca6eaef958d46dcefccf58.jpg" alt="image-2">
                                        </figure>
                                        <a href="index.php?view=cajas"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Bancos</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=tipo_banco"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Impuestos</a>
                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_impuestos"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;   margin-left:-60px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Impuestos de productos</a>
                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_impuestos_productos"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://image.freepik.com/foto-gratis/mensaje-conexion-comunicacion-correo-al-telefono-contactos-correo-concepto-letras-globales_36325-2940.jpg" style="width: 350px; height: 350px;" alt="Image 2">
                                            <figcaption>Notificaciones</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Emails Stocks</a>
                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://image.freepik.com/foto-gratis/mensaje-conexion-comunicacion-correo-al-telefono-contactos-correo-concepto-letras-globales_36325-2940.jpg" style="width: 350px; height: 350px;" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_notificaciones"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Productos</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=productos"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Subimpuestos de productos</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_subimpuestos_productos"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Bodegas</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_bodegas"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;   margin-left:-60px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Categoria Ventas</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_categoria_ventas"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Sub Categorias de Venta</a>
                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_tipo_categoria_ventas"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Unidad De Medida</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_unidad_de_medida"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->

                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Utilidad Producto</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_utilidad_productos"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;   margin-left:-60px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Descuento Producto</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_descuento_productos"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Categoria Habitación</a>

                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=categoria"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Habitaciones</a>
                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=habitacion"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Habitación Tarifas</a>
                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=tarifa"><button>Abrir</button></a>
                                        <div class="design-tarifa">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;   margin-left:-60px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Cliente</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=cliente"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Proveedor</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=proveedores"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                        <!-- Siguiente mantenimiento-->
                        <div class="col-lg-1 col-xs-6" style="margin-right:230px; ">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" height="300px" width="200px" alt="Image 2">
                                            <figcaption>Punto de venta</figcaption>
                                        </figure>
                                        <center>
                                            <ul>
                                                <a class="tamañotitulo" style="color:white;">Categoria Promociones</a>
                                            </ul>
                                        </center>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/a2/c8/35/a2c8352bd0a3b392feb88f29bb3b976a.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=categoria_clientes_p"><button>Abrir</button></a>
                                        <div class="design-container">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->
                         <!-- Siguiente mantenimiento-->
                         <div class="col-lg-1 col-xs-6" style="margin-right:230px;">
                            <!-- flip-card-container -->
                            <div class="flip-card-container" style="--hue: 170">
                                <div class="flip-card">
                                    <div class="card-front">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                            <figcaption>Recepción</figcaption>
                                        </figure>
                                        <ul>
                                            <a class="tamañotitulo" style="color:white;">Listado Cabys</a>
                                        </ul>
                                    </div>
                                    <div class="card-back">
                                        <figure>
                                            <div class="img-bg"></div>
                                            <img src="https://i.pinimg.com/564x/5f/ad/db/5faddb20b9d11bed0762a1faf5f6bee9.jpg" alt="Image 2">
                                        </figure>
                                        <a href="index.php?view=sala_auto_cabys"><button>Abrir</button></a>
                                        <div class="design-tarifa">
                                            <span class="design design--1"></span>
                                            <span class="design design--2"></span>
                                            <span class="design design--3"></span>
                                            <span class="design design--4"></span>
                                            <span class="design design--5"></span>
                                            <span class="design design--6"></span>
                                            <span class="design design--7"></span>
                                            <span class="design design--8"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--final del mantenimiento-->

                    </div>

                </div>
            </div>
    </div>
    </div>
    </section>
    </div>
</body>

</html>
<style>
    .tamañotitulo {
        font-size: 27px !important;
    }
</style>

<script>
    function Cargar_categorias(val) {
        $('#categorias_resultado').html("Por favor espera un momento");
        $.ajax({
            type: "POST",
            url: './?action=categoria_mantenimientos',
            data: 'categoria=' + val,
            success: function(resp) {
                document.getElementById('categoria_hijo').remove();
                document.getElementById('padre').innerHTML = '<div id="categoria_hijo"></div>';
                $('#categoria_hijo').html(resp);
            }
        });
    };
</script>