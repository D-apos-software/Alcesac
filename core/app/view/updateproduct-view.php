<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css" />
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>

<?php
require __DIR__ . '/../../../core/app/action/action-functions-products.php';

if (count($_POST) > 0) {

	if ($_POST['id_descuento'] != 0 && $_POST['id_descuento'] != "0") {
		$descuento = Descuento_Producto::getById($_POST["id_descuento"]);
		$valor_D = $descuento->valor;
	} else {
		$valor_D = 0;
	}
	$utilidad = $_POST["utilidad"];

	//print($_POST['id_descuento']);

	if ($valor_D > $utilidad) {
?>
		<script>
			swal({
				title: "¡ERROR!",
				text: "La Utilidad no puede ser mayor al Descuento",
				type: "error",
			}).then(function() {
				window.location = "index.php?view=productos";
			});
		</script>
	<?php
	} else if ($utilidad == 0 || $utilidad < 0) {

	?>
		<script>
			swal({
				title: "¡ERROR!",
				text: "Verificar el valor de utilidad ingresado",
				type: "error",
			}).then(function() {
				window.location = "index.php?view=productos";
			});
		</script>


		<?php
	} else {


		//Stock total del producto añadir
		$product_id = $_POST['id_pro'];
		$stock_m = $_POST['stock_m'];

		$stock = stock_productos($product_id, $stock_m);


		//Validamos que la cantidad del producto a registrar no sobrepase la cantidad maxima del almacenamiento de la bodega
		$bodega_stock = ProcesoVentaData::get_stock_bodega($_POST['id_bodega']);
		if (isset($bodega_stock)) {
			$total_stock = $bodega_stock->stock + $bodega_stock->compras;
			$total_stock = $total_stock - $bodega_stock->ventas;
			//print("<br><br><br>" . $total_stock);
			$total_stock = $total_stock + $stock;
			//print("<br><br><br>" . $total_stock);
			//print("<br><br><br>" . $bodega_stock->cantidad_maxima);
			$stock_val = $bodega_stock->cantidad_maxima;
		} else {

			$total_stock = 0;
			$stock_val = 1;
		}
		if ($stock_val < $total_stock) {

		?>

			<script>
				swal({
					title: "¡ERROR!",
					text: "La cantidad de la bodega seleccionada se encuentra al limite con el stock del producto que se añadirá.",
					type: "error",
				}).then(function() {
					window.location = "index.php?view=productos";
				});
			</script>

			<?php
		} else {


			try {

				$producto = ProductoData::getById($marca = $_POST["id_producto"]);

				//print("<br><br><br><br> id codigo_search".$_POST["codigo_sM".$_POST["id_producto"]]);
				if ($_POST["cd2" . $_POST["id_producto"]] == 2) {
					$producto->codigo = $_POST["codigoM" . $_POST["id_producto"]];
				} else if ($_POST["cd2" . $_POST["id_producto"]] == 3) {
					$producto->codigo = $_POST["codigo_sM" . $_POST["id_producto"]];
				}

				$producto->codigo_barra = $_POST["codigo_barra"];
				$producto->nombre = $_POST["nombre"];

				$marca = "NULL";
				if ($_POST["marca"] != "") {
					$marca = $_POST["marca"];
				}

				$presentacion = "NULL";
				if ($_POST["presentacion"] != "") {
					$presentacion = $_POST["presentacion"];
				}

				$descripcion = "NULL";
				if ($_POST["descripcion"] != "") {
					$descripcion = $_POST["descripcion"];
				}

				$precio_compra = "0";
				if ($_POST["precio_compra"] != "") {
					$precio_compra = $_POST["precio_compra"];
				}

				$id_acreditable = "0";
				if ($_POST["id_acreditable"] != "") {
					$id_acreditable = $_POST["id_acreditable"];
				}

				print_r('<br><br><br><br><br>');
				$dates_img = array();
				foreach ($_FILES['imagen_m'] as $value) {
					array_push($dates_img, $value);
				}
				//print($dates_img[0]);

				//Validando y Moviendo imagen 
				if ($_FILES['imagen_m']['name'] != Null) {
					$archivo = $_FILES['imagen_m']['tmp_name'];
					$destino = 'img/img_productos/' . $_FILES['imagen_m']['name'];
					move_uploaded_file($archivo, $destino);

					print_r($_FILES["imagen_m"]['name']);
					$condicion = 'img/img_productos/';
					//Eliminamos Imagen anterior
					//if ($_FILES['imagen_m'] != NULL || ) {
					$imagen_delete = $_POST["imagen_delete"];
					//if ($destino != $imagen_delete) {
					//print($_POST["imagen_delete"].'<br>'.$condicion);

					if ($imagen_delete != $condicion) {
						if ($imagen_delete == $_FILES['imagen_m']['name']) {
							//if (file_exists($imagen_delete) == 1) {
							unlink($imagen_delete);
							//}
						}
					}
				} else {
					$destino = $_POST["imagen_delete"];
				}


				//}
				//}



				$producto->presentacion = $presentacion;
				$producto->marca = $marca;
				$producto->descripcion = $descripcion;
				$producto->acreditable = $id_acreditable;
				$producto->precio_compra = $precio_compra;
				$producto->precio_venta = $_POST["precio_venta"];
				$producto->id_bodega = $_POST["id_bodega"];
				$producto->stock_max = $_POST["cantidad_maxima"];
				$producto->id_unidad_medida = $_POST["id_unidad_medida"];
				$producto->id_descuento = $_POST["id_descuento"];
				$producto->aviso_stock = $_POST["aviso_stock"];
				$producto->utilidad = $_POST["utilidad"];
				$producto->id_proveedor = $_POST["id_proveedor_m"];
				$producto->imagen = $destino;
				$data_product = $producto->update();


				//delete_utilidad($_POST["id_producto"]);

				//Actualizamos por medio de un insert y un delete las relaciones que hay entre las tablas productos,impuestos y r_impuestos_productos

				if (isset($_POST["dropdown-group"])) {
					
					delete($_POST["id_producto"]);
					$data = $_POST["dropdown-group"];

					foreach ($data as $datas) {
						//print($datas . 'insert <br>');
						insert($datas, $_POST["id_producto"]);
					} 
				}else{
					delete($_POST["id_producto"]);
				}

				//Actualizamos por medio de un insert y un delete las relaciones que hay entre las tablas productos,descuentos_productos y r_descuentos_productos
				//$datas_utilidad = $_POST["utilidad"];

				/*foreach ($datas_utilidad as $data_utilidad) {
						//print($datas . 'insert <br>');
						insert_utilidad($data_utilidad, $_POST["id_producto"]);
					}*/
			?>
				<script>
					swal({
						title: "¡Se Actualizo correctamente!",
						icon: 'success',
						text: "Se registro correctamente.<br> Causa : ".$th,
						type: "success",
					}).then(function() {
						window.location = "index.php?view=productos";
					});
				</script>

			<?php
			} catch (\Throwable $th) {
			?>
				<script>
					swal({
						title: "¡ERROR!",
						text: "No se ha podido registrar el producto.<br> Causa : ".$th,
						type: "error",
					}).then(function() {
						window.location = "index.php?view=productos";
					});
				</script>
<?php
			}
		}
	}
}
