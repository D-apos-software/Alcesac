<?php
 //Nos conectamos con el archivo notificaciones_stock-action.php para usar la funcion actualizar_stock_notificacion();
 //require __DIR__ . '/../../../core/app/action/notificaciones_stock-action.php';
 require __DIR__ . '/../../../core/app/action/action-functions-products.php';

?>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<link rel="stylesheet" href="assets/js/vendor/footable/css/footable.core.min.css">


<?php
$session_id= session_id();
$u=null;
$u = UserData::getById(Session::getUID());
$usuario = $u->is_admin;
$id_usuario = $u->id;

$tmps = TmpData::getAllTemporalCompra($session_id);
if(count($tmps)>0){


if(count($_POST)>0){
  
	$cajas = CajasAperturas::getCierreCaja($id_usuario); 
	$object = (array) $cajas;
	$object = count($object);
	if( $object  >0){ $id_caja=$cajas->id;
 	}else{$id_caja='NULL';}

 	$total=0;
 	$tmpes = TmpData::getAllTemporalCompra($session_id); 
	foreach($tmpes as $p): 
		$total=($p->precio_tmp*$p->cantidad_tmp)+$total;
	endforeach;
  




	$venta = new VentaData();
	$venta->id_tipo_comprobante= $_POST['id_tipo_comprobante'];
	$nro_comprobante="NULL";
	  if($_POST["nro_comprobante"]!=""){ $nro_comprobante=$_POST["nro_comprobante"];}
	$venta->nro_comprobante = $nro_comprobante;
	$venta->id_proveedor='NULL';
	$venta->id_tipo_pago=1;
	$venta->total= $total;
	$venta->id_usuario = $_SESSION["user_id"];
	$venta->id_caja = $id_caja;
	$v=$venta->addCompra();



	$productos_data = array();
	$tmps = TmpData::getAllTemporalCompra($session_id); 
	foreach($tmps as $p): 
		array_push($productos_data,$p->id_producto);
		$procesoventa = new ProcesoVentaData();  
		$procesoventa->id_producto=$p->id_producto;
		$procesoventa->id_caja=$id_caja; 
		$procesoventa->id_usuario=$_SESSION["user_id"];
		$procesoventa->id_venta=$v[1];
		$procesoventa->cantidad=$p->cantidad_tmp;
		$procesoventa->precio=$p->precio_tmp; 
		$procesoventa->tipo_operacion=$p->tipo_operacion; 
		$procesoventa->addCompra();

	endforeach;
	 
	$dels = TmpData::getAllTemporalCompra($session_id);
	foreach($dels as $del):
		$eliminar = TmpData::getById($del->id_tmp);
		$eliminar->del();
	endforeach;
	Actualizar_Apertura::actualizar();
	
	//actualizar_stock_notificacion();
	stock_producto_egreso($productos_data);
	?>
	<meta http-equiv="refresh" content="1; url=index.php?view=kardex">
	
	<script type="text/javascript">
		Swal.fire({
		icon: 'success',
		title: 'Compra satisfactoria',
		showConfirmButton: false,
		timer: 1700
		})
	</script>
	<?php 
//print "<script>window.location='index.php?view=kardex';</script>";

}


}else{ ?>
	<script type="text/javascript">
	Swal.fire('No se agrego ningun producto!')
</script>
<meta http-equiv="refresh" content="1; url=index.php?view=compra"> 

<?php 
//print "<script>window.location='index.php?view=compra';</script>";

}

?>

